#!/bin/bash

################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

MYDIR="$(dirname "$(readlink -f ${BASH_SOURCE[0]})")"

CLANG_TIDY_CACHE="${MYDIR}/clang-tidy-cache.py"
CLANG_TIDY="${CLANG_TIDY_CHECK_CLANG_TIDY:-clang-tidy}"

if [[ ! -d "$CLANG_TIDY_CHECK_BUILD_DIR" ]]; then
  echo "ERROR: Please set CLANG_TIDY_CHECK_BUILD_DIR to a valid build directory"
  exit 1
elif [[ ! -f "${CLANG_TIDY_CHECK_BUILD_DIR}/compile_commands.json" ]]; then
  echo "ERROR: compile_commands.json not found in build directory"
  exit 1
fi

# initialize to empty array if not already set externally
CLANG_TIDY_CHECK_INCLUDE_EXTS=("${CLANG_TIDY_CHECK_INCLUDE_EXTS[@]}")
CLANG_TIDY_CHECK_EXCLUDE_DIRS=("${CLANG_TIDY_CHECK_EXCLUDE_DIRS[@]}")
CLANG_TIDY_CHECK_EXCLUDE_FILES=("${CLANG_TIDY_CHECK_EXCLUDE_FILES[@]}")


################# BEGIN building find command arguments #################

if [[ ${#CLANG_TIDY_CHECK_INCLUDE_EXTS[@]} -eq 0 ]]; then
  echo "ERROR: At least one file extension has to be specified for clang-tidy source file scan via environment variable CLANG_TIDY_CHECK_INCLUDE_EXTS"
  exit 1
fi

FIND_ARGS=()

# exlude directories using path-prune-or schema
if [[ ${#CLANG_TIDY_CHECK_EXCLUDE_DIRS[@]} -gt 1 ]]; then
  FIND_ARGS+=("(")
fi

for EXCLUDE_DIR in "${CLANG_TIDY_CHECK_EXCLUDE_DIRS[@]}"; do
  FIND_ARGS+=("-path" "./$EXCLUDE_DIR" "-prune" "-o")
done

if [[ ${#CLANG_TIDY_CHECK_EXCLUDE_DIRS[@]} -gt 0 ]]; then
  FIND_ARGS_SIZE=${#FIND_ARGS[@]}
  unset FIND_ARGS[$(($FIND_ARGS_SIZE-1))]    # remove last "-o"
fi

if [[ ${#CLANG_TIDY_CHECK_EXCLUDE_DIRS[@]} -gt 1 ]]; then
  FIND_ARGS+=(")")
fi

if [[ ${#CLANG_TIDY_CHECK_EXCLUDE_DIRS[@]} -gt 0 ]]; then
  FIND_ARGS+=("-o")
fi

FIND_ARGS+=("-type" "f")

# exlude single files
for EXCLUDE_FILE in "${CLANG_TIDY_CHECK_EXCLUDE_FILES[@]}"; do
  FIND_ARGS+=("-not" "-path" "./$EXCLUDE_FILE")
done

# find files with specific extension(s)
FIND_ARGS+=("(")

for INCLUDE_EXT in "${CLANG_TIDY_CHECK_INCLUDE_EXTS[@]}"; do
  FIND_ARGS+=("-iname" "*.$INCLUDE_EXT" "-o")
done

FIND_ARGS_SIZE=${#FIND_ARGS[@]}
unset FIND_ARGS[$(($FIND_ARGS_SIZE-1))]    # remove last "-o"

FIND_ARGS+=(")" "-print0")

################# END building find command arguments #################

STDOUT_FILE="$CLANG_TIDY_CHECK_BUILD_DIR/clang-tidy_results.stdout"
STDERR_FILE="$CLANG_TIDY_CHECK_BUILD_DIR/clang-tidy_results.stderr"
LIST_FILE="${CLANG_TIDY_CHECK_BUILD_DIR}/ctc_list.txt"

cd "$CLANG_TIDY_CHECK_BASE_DIR"

rm -f "$STDOUT_FILE"*.log "$STDERR_FILE"*.log "$LIST_FILE"

CLANG_TIDY_CALL="$CLANG_TIDY_CACHE $CLANG_TIDY -p \"$CLANG_TIDY_CHECK_BUILD_DIR\" -extra-arg=-std=c++17"

find "${FIND_ARGS[@]}" > "$LIST_FILE"
echo "[CTCACHE] Checking $(cat "$LIST_FILE" | tr '\0' '\n' | wc -l) files..."

cat "$LIST_FILE" | sort -z | xargs -0 -P 2 -Isourcefilename \
  sh -c " \
  MYPID=\$\$; \
  $CLANG_TIDY_CALL sourcefilename > \"${STDOUT_FILE}.\${MYPID}.log\" 2> \"${STDERR_FILE}.\${MYPID}.log\"; \
  [ -f \"${STDOUT_FILE}.\${MYPID}.log\" ] && [ ! -s \"${STDOUT_FILE}.\${MYPID}.log\" ] \
    && echo \"[SUCCESS] sourcefilename\" \
    || echo \"[FAILURE] sourcefilename\" \
  "

awk 'FNR==1{print "=============================="}{print}' "$STDOUT_FILE".*.log > "$STDOUT_FILE".log
awk 'FNR==1{print "=============================="}{print}' "$STDERR_FILE".*.log > "$STDERR_FILE".log
rm -f "$STDOUT_FILE".*.log "$STDERR_FILE".*.log

if [[ -n "$CLANG_TIDY_CHECK_ARTIFACT_DIR" ]]; then
  mkdir -p "${CLANG_TIDY_CHECK_ARTIFACT_DIR}"
  mv "$STDOUT_FILE".log "$STDERR_FILE".log "$CLANG_TIDY_CHECK_ARTIFACT_DIR"
fi
