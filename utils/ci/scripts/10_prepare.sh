#!/bin/bash

################################################################################
# Copyright (c) 2021 in-tech GmbH
#               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script prepares building
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../.." || exit 1

if [ ! -d repo ]; then
  echo "repo folder doesn't exist as expected. exiting."
  exit 1
fi

# wipe build directories and pyOpenPASS results
if [[ -z "${CROSS_COMPILE_WINDOWS}" ]]; then
  rm -rf artifacts build dist build-fmi-library build-osi deps
  rm -f repo/sim/tests/endToEndTests/pyOpenPASS/result_*.xml
  # prepare
  mkdir build
else  
  rm -rf artifacts build-fmi-library build-osi deps
  rm -f repo/sim/tests/endToEndTests/pyOpenPASS/result_*.xml
fi

printenv

if [[ "${OSTYPE}" = "msys" ]]; then
  pacman -Q

  echo "list of native windows python packages"
  "${PYTHON_WINDOWS_EXE}" -m pip list -v
fi

exit 0

