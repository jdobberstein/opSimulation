#!/bin/bash

################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script builds a docker image
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"

apt-get -y install docker docker.io

systemctl show docker --property Environment

cd "$MYDIR/../../../../.." || exit 1


echo "Building docker image"
docker build -f ./repo/utils/Dockerfile \
             -t nightly_build \
             .

echo "Building & Testing openPASS"
docker run --rm -v $PWD:/workspace nightly_build:latest /workspace/repo/utils/ci/scripts/run_all.sh

echo "Packing openPASS artifacts"
docker run --rm -v $PWD:/workspace nightly_build:latest /workspace/repo/utils/ci/scripts/pack_artifacts.sh
