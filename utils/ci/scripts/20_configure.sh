#!/bin/bash

################################################################################
# Copyright (c) 2021 in-tech GmbH
#               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script configures cmake
################################################################################

# joins arguments using the cmake list separator (;)
function join_paths()
{
  local IFS=\;
  echo "$*"
}

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../build" || exit 1

# dependencies built previously
DEPS=(
  "$PWD/../deps/direct_deploy/fmilibrary"
  "$PWD/../deps/direct_deploy/open-simulation-interface"
  "$PWD/../deps/direct_deploy/protobuf"
  "$PWD/../deps/direct_deploy/protobuf-shared"
  "$PWD/../deps/direct_deploy/units"
  "$PWD/../deps/direct_deploy/mantleapi"
  "$PWD/../deps/direct_deploy/yase"
  "$PWD/../deps/direct_deploy/openscenario_api"
  "$PWD/../deps/direct_deploy/openscenario_engine"
  "$PWD/../deps/direct_deploy/gtest"
  "$PWD/../deps/direct_deploy/boost"
  "$PWD/../deps/direct_deploy/qt"
  "$PWD/../deps/direct_deploy/minizip"
  "$PWD/../deps/direct_deploy/libiconv"
  "$PWD/../deps/direct_deploy/libxml2"
  "$PWD/../deps/direct_deploy/zlib"
  "$PWD/../deps"
)

if [[ "${CROSS_COMPILE}" = true ]]; then
  CMAKE_C_COMPILER="-DCMAKE_C_COMPILER=/usr/bin/x86_64-w64-mingw32-gcc"
  CMAKE_CXX_COMPILER="-DCMAKE_CXX_COMPILER=/usr/bin/x86_64-w64-mingw32-g++"
  CMAKE_SYSTEM_NAME="-DCMAKE_SYSTEM_NAME=Windows"
  CMAKE_SYSTEM_VERSION="CMAKE_SYSTEM_VERSION=10.0"
  DEPS+=("/usr/lib/gcc/x86_64-w64-mingw32" "/usr/x86_64-w64-mingw32")
fi

# preparations for building on Windows/MSYS
if [[ "${OSTYPE}" = "msys" ]]; then
  # set the correct CMake generator
  CMAKE_GENERATOR_ARG="-GMSYS Makefiles"

  # set python command
  if [[ -n "${PYTHON_WINDOWS_EXE}" ]]; then
    CMAKE_PYTHON_COMMAND_ARG="-DPython3_EXECUTABLE=${PYTHON_WINDOWS_EXE}"
  fi

  # prepare dependency paths
  # it seems cmake doesn't like MSYS paths starting with drive letters (e.g. /c/thirdParty/...)
  # cygpath is used here to format the paths in "mixed format" (e.g. c:/thirdparty/...)
  # also, add the mingw64 base path to allow resolving of runtime dependencies during install
  OLD_DEPS=(${DEPS[@]})
  OLD_DEPS+=("/mingw64")
  DEPS=()
  for DEP in "${OLD_DEPS[@]}"; do
    DEPS+=("$(cygpath -a -m ${DEP})")
  done
fi

# generate version information
if [[ "${TAG_NAME}" =~ ^v[0-9]+.[0-9]+.[0-9]+ ]]; then
  MAJOR=$(echo ${TAG_NAME} | sed -e 's/v\([0-9]\+\)\.\([0-9]\+\)\.\([0-9]\+\)\(.*\)/\1/')
  MINOR=$(echo ${TAG_NAME} | sed -e 's/v\([0-9]\+\)\.\([0-9]\+\)\.\([0-9]\+\)\(.*\)/\2/')
  PATCH=$(echo ${TAG_NAME} | sed -e 's/v\([0-9]\+\)\.\([0-9]\+\)\.\([0-9]\+\)\(.*\)/\3/')
  CMAKE_VERSION_ARG="-DSIMCORE_VERSION_MAJOR=${MAJOR} -DSIMCORE_VERSION_MINOR=${MINOR} -DSIMCORE_VERSION_PATCH=${PATCH}"
elif [[ -n "${GIT_BRANCH}" || -n "${GIT_COMMIT}" ]]; then
  CMAKE_VERSION_ARG="-DSIMCORE_VERSION_TAG=${GIT_BRANCH:-no-branch}_${GIT_COMMIT}"
fi

cmake \
  "$CMAKE_GENERATOR_ARG" \
  "$CMAKE_PYTHON_COMMAND_ARG" \
  "$CMAKE_VERSION_ARG" \
  "$CMAKE_C_COMPILER" \
  "$CMAKE_CXX_COMPILER" \
  "$CMAKE_SYSTEM_NAME" \
  "$CMAKE_SYSTEM_VERSION" \
  -D CMAKE_PREFIX_PATH="$(join_paths ${DEPS[@]})" \
  -D CMAKE_MODULE_PATH="$PWD/../deps" \
  -D CMAKE_INSTALL_PREFIX="$PWD/../dist/opSimulation" \
  -D CMAKE_BUILD_TYPE=Release \
  -D INSTALL_BIN_DIR:STRING=. \
  -D INSTALL_EXTRA_RUNTIME_DEPS=ON \
  -D INSTALL_SYSTEM_RUNTIME_DEPS=ON \
  -D OPENPASS_ADJUST_OUTPUT=OFF \
  -D USE_CCACHE=OFF \
  -D WITH_COVERAGE=OFF \
  -D WITH_DEBUG_POSTFIX=OFF \
  -D WITH_DOC=ON \
  -D WITH_ENDTOEND_TESTS=ON \
  ../repo
