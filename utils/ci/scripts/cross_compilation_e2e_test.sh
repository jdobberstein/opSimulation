#!/bin/bash

################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script executes the build and installs files to destination directory
################################################################################
MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../.." || exit 1
REPO_ROOT="$PWD"

# Define variables
PYOPENPASS_CONFIGS=(
  "${REPO_ROOT}/sim/tests/endToEndTests/test_end_to_end.json"
  "${REPO_ROOT}/sim/tests/endToEndTests/test_spawner.json"
)

PYOPENPASS_RESOURCES=(
  "${REPO_ROOT}/sim/contrib/examples"
  "${REPO_ROOT}/sim/contrib/examples"
)

# Append externally set configs and resources (if any)
PYOPENPASS_CONFIGS+=("${PYOPENPASS_EXTRA_CONFIGS[@]}")
PYOPENPASS_RESOURCES+=("${PYOPENPASS_EXTRA_RESOURCES[@]}")

# Check if lists have the same length
if [ ${#PYOPENPASS_CONFIGS[@]} -ne ${#PYOPENPASS_RESOURCES[@]} ]; then
  echo "PYOPENPASS_CONFIGS and PYOPENPASS_RESOURCES must be the same length"
  exit 1
fi

# Set filter and executable name based on OS
if [[ "$OSTYPE" == "msys" ]]; then
  PYOPENPASS_TEST_FILTER="-k not linux64"
  OP_EXE_NAME="opSimulation.exe"
else
  PYOPENPASS_TEST_FILTER="-k not win64"
  OP_EXE_NAME="opSimulation"
fi

if [[ -f "$PYTHON_WINDOWS_EXE" ]]; then
  PYTHON3_EXECUTABLE="$PYTHON_WINDOWS_EXE"
else
  PYTHON3_EXECUTABLE=$(which python3)
fi

if [[ "$OSTYPE" == "msys" ]]; then
  # Check if the Python executable is valid
  "$PYTHON3_EXECUTABLE" --version > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Python executable at $PYTHON3_EXECUTABLE is not valid"
    exit 1
  fi
fi

# pyOpenPASS target
echo "pyOpenPASS target, executing all test files"

# List of files where the warnings are intended to be disregarded
FILES_WITH_IGNORED_WARNINGS=(
  "${REPO_ROOT}/sim/tests/endToEndTests/allowed_end_to_end_warnings.txt"
  "${REPO_ROOT}/sim/tests/endToEndTests/allowed_spawner_warnings.txt"
)

FILES_WITH_IGNORED_WARNINGS+=("${EXTRA_FILES_WITH_IGNORED_WARNINGS[@]}")

# Convert the list to a string with space-separated values
FILES_WITH_IGNORED_WARNINGS_STRING=$(printf ",--allowed-warnings=%s" "${FILES_WITH_IGNORED_WARNINGS[@]}")
FILES_WITH_IGNORED_WARNINGS_STRING=${FILES_WITH_IGNORED_WARNINGS_STRING:1}

echo $FILES_WITH_IGNORED_WARNINGS_STRING

# Loop through each config and resource
for i in "${!PYOPENPASS_CONFIGS[@]}"; do
  cd "${REPO_ROOT}/sim/tests/endToEndTests/pyOpenPASS" || exit 1
  CURRENT_CONFIG=${PYOPENPASS_CONFIGS[$i]}
  CURRENT_RESOURCES=${PYOPENPASS_RESOURCES[$i]}

  if [ ! -f "$CURRENT_CONFIG" ]; then
    echo "$CURRENT_CONFIG does not exist"
    exit 1
  fi

  CURRENT_TESTCASE=$(basename "$CURRENT_CONFIG" .json)

  # Copy the test file
  cp "$CURRENT_CONFIG" "${REPO_ROOT}/sim/tests/endToEndTests/pyOpenPASS"

  # Execute pytest
  "$PYTHON3_EXECUTABLE" -m pytest -vvv --simulation="${REPO_ROOT}/../dist/opSimulation/${OP_EXE_NAME}" --mutual="${CURRENT_RESOURCES}/Common" --resources="${CURRENT_RESOURCES}/Configurations/" --report-path=. --allowed-warnings="${REPO_ROOT}/sim/tests/endToEndTests/allowed_end_to_end_warnings.txt" --junitxml="result_${CURRENT_TESTCASE}.xml" "${REPO_ROOT}/sim/tests/endToEndTests/${CURRENT_TESTCASE}.json"  .

done

exit 0