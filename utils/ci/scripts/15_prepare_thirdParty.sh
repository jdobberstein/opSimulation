#!/bin/bash

################################################################################
# Copyright (c) 2021 ITK Engineering GmbH
#               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script prepares the thirdParty dependencies
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../.."

REPO_ROOT="$PWD"
file="$REPO_ROOT/utils/ci/conan/conanfile.txt" # Accessing the path of conanfile.txt

# requires by OSI conan build
if [[ -z "$WORKSPACE" ]]; then
  export WORKSPACE="$REPO_ROOT/.."
fi

# Set python command depending on OS
if [[ "${OSTYPE}" = "msys" ]]; then
  PYTHON_COMMAND="${PYTHON_WINDOWS_EXE}"
  export CONAN_CMAKE_GENERATOR="MSYS Makefiles"
  conanprofile="$REPO_ROOT/utils/ci/conan/recipe/conanprofile_windows"
  conanprofilehost="$REPO_ROOT/utils/ci/conan/recipe/conanprofile_windows"
else
  PYTHON_COMMAND=python3
  conanprofile="$REPO_ROOT/utils/ci/conan/recipe/conanprofile_linux"
  conanprofilehost="$REPO_ROOT/utils/ci/conan/recipe/conanprofile_linux"
fi

# required for cross compilation
if [[ "${CROSS_COMPILE}" = true ]]; then
  conanprofilehost="$REPO_ROOT/utils/ci/conan/recipe/conanprofile_crosscompilation"
fi

# Function to extract the option for the given package as mentioned in conanfile.txt
ExtractOptionsAndQueries() {

  for option in "${listOfOptions[@]}"; do
    # Check if the option belongs to the concerned package
    if [[ $option == ${packageName}* ]]; then
        # Append "-o " before the matched variable
        packageOptions+="-o ${option} "
        packageQueries+="$(echo "$option" | grep -oP "${packageName}.*" | cut -d':' -f2-) AND "
    fi
  done

  # Remove the trailing " AND "
  packageQueries="${packageQueries% AND }"
}

# Function to build package other than protobuf with the given options in conanfile.txt
BuildPackage() {

  # Extract version
  packageVersion=$(echo $each_requirement | awk -F'[@/]' '{print $2}')

  # Extract user
  packageUser=$(echo $each_requirement | awk -F'[@/]' '{print $3}')

  # Extract channel
  packageChannel=$(echo $each_requirement | awk -F'[@/]' '{print $4}')

  if [[ "$packageName" == "libxml2" ]]; then
    "$PYTHON_COMMAND" -m conans.conan create "$REPO_ROOT"/utils/ci/conan/recipe/"$packageName"/all --name $packageName --version $packageVersion --build=missing $packageOptions -pr:b "$conanprofile" -pr:h="$conanprofilehost"
    status=$?
    if [[ "$status" -gt 0 ]]; then
      echo "Error: Failed to create the $each_requirement $packageOptions"
      "$PYTHON_COMMAND" -m conans.conan remove -c "$each_requirement"
      exit 1
    fi
  else
    "$PYTHON_COMMAND" -m conans.conan create "$REPO_ROOT"/utils/ci/conan/recipe/"$packageName"/all --name $packageName --version $packageVersion --user $packageUser --channel $packageChannel --build=missing $packageOptions -pr:b "$conanprofile" -pr:h="$conanprofilehost"
    status=$?
    if [[ "$status" -gt 0 ]]; then
      echo "Error: Failed to create the $each_requirement $packageOptions"
      "$PYTHON_COMMAND" -m conans.conan remove -c "$each_requirement"
      exit 1
    fi
  fi
}

# Function to parse the conanfile.txt and list out requirements and options
ListOutRequirementsAndOptions() {
    local file="$1"
    local start_pattern="$2"
    local end_pattern="$3"
    local -n section_array="$4"
    local in_section=false

    while read -r line; do
      line=${line//[[:space:]]}   # trim all whitespace

      if [[ $line = '' ]]; then
        continue
      elif [[ $line =~ $start_pattern ]]; then
        in_section=true
        continue
      elif [[ $line =~ $end_pattern ]]; then
        in_section=false
        continue
      elif $in_section; then
        section_array+=("$line")
      fi
    done < "$file"
    section_array=("${section_array[@]}")
}

# List out requirements
listOfRequirements=()
ListOutRequirementsAndOptions "$file" "\[requires\]" "^\[" listOfRequirements

# List out options
listOfOptions=()
ListOutRequirementsAndOptions "$file" "\[options\]" "^\[" listOfOptions

echo "list of available packages"
"$PYTHON_COMMAND" -m conans.conan list *@openpass/testing
"$PYTHON_COMMAND" -m conans.conan list *@

# Note: If there is a change in recipe of the existing package and would want to
# reinstall the existing package. Then execute conan remove <package> command
# before executing the below "for" loop

# Iterate over the list of requirements
for each_requirement in "${listOfRequirements[@]}"; do

  # Name of the package
  packageName="${each_requirement%%/*}"
  # options command to create the package with the particular option as mentioned in conanfile.txt
  packageOptions=""
  # queries command to check the package with the particular option as mentioned in conanfile.txt is available in local or not
  packageQueries=""

  # If the requirement is protobuf, qt or boost, then we follow the different method,
  # as we are using conancenter to install and need both shared and static binaries
  if [[ "$packageName" == "protobuf" ]]; then
    continue
  fi

  if [[ "$packageName" == "boost" ]]; then
    continue
  fi

  if [[ "$packageName" == "qt" ]]; then
    continue
  fi

  if [[ "$packageName" == "zlib" ]]; then
    continue
  fi

  if [[ "$packageName" == "minizip" ]]; then
    continue
  fi

  if [[ "$packageName" == "gtest" ]]; then
    continue
  fi

  if [[ "$packageName" == "b2" ]]; then
    continue
  fi

  if [[ "$packageName" == "msys2" ]]; then
    continue
  fi

  if [[ "$packageName" == "libiconv" ]]; then
    continue
  fi

  # Extract options and queries for each package
  ExtractOptionsAndQueries

  # Check if the package is already available in the local
  # conan search results failure if the package is not at all available
  # and just informs that a particular package with the given option is not available, if the same package with another option is available.
  # Therefore, it is important to parse the output and check if the package is available or not
  searchResult=$("$PYTHON_COMMAND" -m conans.conan list -p "$packageQueries" $each_requirement 2>&1)

  # If requirement with the given option is already available, then just install them into deps folder.
  if ! echo "$searchResult" | grep -q -e "no packages for reference" -e "ERROR:"; then
    echo "Package $each_requirement $packageOptions is already available."
  # If requirement with the given option is not available, then create and install them into deps folder.
  else
    echo "Package $each_requirement $packageOptions is not available. Building..."
    BuildPackage
  fi

done

# Command to install all the packages into the required folder.
# --build=missing argument is necessary as at this point protobuf is checked and if not available it builds from conancenter

if [[ "${CROSS_COMPILE}" = true ]]; then
  "$PYTHON_COMMAND" -m conans.conan install $file -o qt/*:cross_compile=x86_64-w64-mingw32- -o qt/*:with_freetype=False --build=missing --deployer=direct_deploy -of="$REPO_ROOT/../deps" -pr:b "$conanprofile" -pr:h="$conanprofilehost" || exit 1
else
  "$PYTHON_COMMAND" -m conans.conan install $file --build=missing --deployer=direct_deploy -of="$REPO_ROOT/../deps" -pr:b "$conanprofile" -pr:h="$conanprofilehost" || exit 1
fi
mv $REPO_ROOT/../deps/direct_deploy/protobuf $REPO_ROOT/../deps/direct_deploy/protobuf-shared   # rename the deployed protobuf folder as protobuf-shared (to distinguish between protobuf static and shared)

"$PYTHON_COMMAND" -m conans.conan install --requires=protobuf/3.20.0@ --build=missing  --deployer=direct_deploy -of="$REPO_ROOT/../deps" -g CMakeDeps -o protobuf/*:shared=False -pr:b "$conanprofile" -pr:h="$conanprofilehost"
status=$?
if [[ "$status" -gt 0 ]]; then
  echo "Error: Failed to install and deploy the protobuf static"
  exit 1
fi

# Apply protobuf patch on port_def.inc and port_undef.inc files
# to avoid GetMessage name conflict on msys
if [[ "${OSTYPE}" = "msys" || "${CROSS_COMPILE}" = true ]]; then
  cd $REPO_ROOT/../deps/direct_deploy/protobuf
  patch -p0 < $REPO_ROOT/utils/ci/scripts/patches/protobuf.patch
  cd $REPO_ROOT/../deps/direct_deploy/protobuf-shared
  patch -p0 < $REPO_ROOT/utils/ci/scripts/patches/protobuf.patch
fi

if [[ "${CROSS_COMPILE}" = true ]]; then
  cd $REPO_ROOT/../deps
  sed -i 's/set(zlib_INCLUDE_DIRS_RELEASE )/set(zlib_INCLUDE_DIRS_RELEASE "${zlib_PACKAGE_FOLDER_RELEASE}\/include")/' ZLIB-release-x86_64-data.cmake
fi