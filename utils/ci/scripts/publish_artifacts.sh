#!/bin/bash

################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script stores the artifacts of the build when there is a tag
# Else, it removes all snapshot artifacts and stores the one of the 
# current build only.
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../.." || exit 1  

if [[ "${OSTYPE}" = "msys" ]]; then
  ARCHIVE_EXT="zip"
else
  ARCHIVE_EXT="tar.gz"
fi

if [[ ! -f artifacts/openPASS_SIM.${ARCHIVE_EXT} ]]; then
    echo "Artifact not found. Skipping deployment."
    exit 0
fi

BASE_PATH="/home/data/httpd/download.eclipse.org/openpass"
SSH_COMMAND="ssh -o BatchMode=yes genie.openpass@projects-storage.eclipse.org"
SCP_COMMAND="scp -o BatchMode=yes"
SCP_DEST_BASE="genie.openpass@projects-storage.eclipse.org:${BASE_PATH}"
DEST_DIR="."

if [ -z "${TAG_NAME}" ]; then
  ${SSH_COMMAND} "rm -rf ${BASE_PATH}/snapshots/opSimulation/openPASS_SIM_*.${ARCHIVE_EXT}"
  ${SSH_COMMAND} "mkdir -p ${BASE_PATH}/snapshots/opSimulation"
  ${SCP_COMMAND} artifacts/openPASS_SIM.${ARCHIVE_EXT} "${SCP_DEST_BASE}/snapshots/opSimulation/openPASS_SIM_$(date -u +%Y%m%d_%H%M%S)_${GIT_COMMIT:-unknown-commit}.${ARCHIVE_EXT}"
else
  ${SSH_COMMAND} mkdir -p ${BASE_PATH}/releases/opSimulation
  ${SCP_COMMAND} artifacts/openPASS_SIM.${ARCHIVE_EXT} "${SCP_DEST_BASE}/releases/opSimulation/openPASS_SIM_${TAG_NAME}.${ARCHIVE_EXT}"
fi