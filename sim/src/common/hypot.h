/********************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <cmath>
#include <units.h>

namespace openpass
{
// Borges, Carlos F. "An Improved Algorithm for hypot (a, b)." arXiv preprint arXiv:1904.09481 (2019).
[[nodiscard]] double constexpr hypot(double a, double b)
{
  if (a == 0.0) return std::abs(b);
  if (b == 0.0) return std::abs(a);
  const auto h = std::sqrt(std::fma(a, a, b * b));
  const auto h_sq = h * h;
  const auto a_sq = a * a;
  const auto x = std::fma(-b, b, h_sq - a_sq) + std::fma(h, h, -h_sq) - std::fma(a, a, -a_sq);
  return h - x / (2 * h);
}

template <typename T>
[[nodiscard]] T hypot(T a, T b)
{
  if (a.value() == 0.0) return units::math::abs(b);
  if (b.value() == 0.0) return units::math::abs(a);
  const auto h = units::math::sqrt(units::math::fma(a, a, b * b));
  const auto h_sq = h * h;
  const auto a_sq = a * a;
  const auto x = units::math::fma(-b, b, h_sq - a_sq) + units::math::fma(h, h, -h_sq) - units::math::fma(a, a, -a_sq);
  return h - x / (2 * h);
}
}  //namespace openpass