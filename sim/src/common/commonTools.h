/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2020 HLRS, University of Stuttgart
 *               2016-2020 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <algorithm>
#include <cmath>
#include <functional>
#include <iosfwd>
#include <limits>
#include <map>
#include <optional>
#include <string>
#include <string_view>
#include <type_traits>
#include <units.h>
#include <unordered_map>
#include <utility>
#include <vector>

#include "common/boostGeometryCommon.h"
#include "common/commonHelper.h"
#include "common/globalDefinitions.h"
#include "common/opExport.h"
#include "common/vector2d.h"
#include "common/worldDefinitions.h"
#include "include/agentInterface.h"
#include "include/worldInterface.h"
#include "include/worldObjectInterface.h"

using namespace units::literals;

namespace CommonHelper
{

[[maybe_unused]] static inline constexpr bool CheckPointValid(const Common::Vector2d<units::length::meter_t> *point)
{
  return ((point != nullptr) && (point->x != units::length::meter_t(INFINITY))
          && (point->y != units::length::meter_t(INFINITY)));
}

//! Calculate the absolute angle between two pcm points.
//!
//! @param[in]    firstPoint     firstPoint
//! @param[in]    secondPoint    secondPoint
//! @return       distance between two pcm points
//-----------------------------------------------------------------------------
[[maybe_unused]] static units::angle::radian_t CalcAngleBetweenPoints(
    const Common::Vector2d<units::length::meter_t> &firstPoint,
    const Common::Vector2d<units::length::meter_t> &secondPoint)
{
  if ((!CheckPointValid(&firstPoint)) || (!CheckPointValid(&secondPoint)))
  {
    return units::angle::radian_t(INFINITY);
  }

  return units::angle::radian_t((secondPoint - firstPoint).Angle());
}

//! Transform a pcm point to a vector in the coordination system of a
//! source point in a direction.
//!
//! @param[in]    point     point
//! @return                 vector
[[maybe_unused]] static Common::Vector2d<units::length::meter_t> TransformPointToSourcePointCoord(
    const Common::Vector2d<units::length::meter_t> *point,
    const Common::Vector2d<units::length::meter_t> *sourcePoint,
    units::angle::radian_t direction)
{
  Common::Vector2d<units::length::meter_t> newPoint = *point;  //(point->x, point->y);
  newPoint.Translate((*sourcePoint) * (-1));
  newPoint.Rotate(direction * (-1));

  return newPoint;
}

//-----------------------------------------------------------------------------
//! @brief Round doubles.
//!
//! Rounds doubles to a given amount of decimals.
//!
//! @param[in] value            Value which is rounded
//! @param[in] decimals         Amount of decimals.
//!
//! @return                     Rounded value.
//-----------------------------------------------------------------------------
static double roundDoubleWithDecimals(double value, int decimals)
{
  return std::floor((value * (std::pow(10, decimals))) + 0.5) / (std::pow(10.0, decimals));
}

//-----------------------------------------------------------------------------
//! @brief Round doubles.
//!
//! Rounds doubles of unit type to a given amount of decimals.
//!
//! @param[in] value            Value which is rounded
//! @param[in] decimals         Amount of decimals.
//!
//! @return                     Rounded value.
//-----------------------------------------------------------------------------
template <class T>
static T roundDoubleWithDecimals(T value, int decimals)
{
  return units::math::floor((value * (std::pow(10, decimals))) + T(0.5)) / (std::pow(10.0, decimals));
}

//! Estimate the inertial momentum for rotation around the vehicle's z-axis, assuming
//! a cuboid of homogeneous mass density. ( see .e.g. https://en.wikipedia.org/wiki/List_of_moments_of_inertia )
//!
//! @param[in] mass     Mass of the vehicle [kg]
//! @param[in] length   Length of the vehicle [m]
//! @param[in] width    Width of the vehicle [m]
//! @return             momentInertiaYaw [kg*m^2]
static units::inertia CalculateMomentInertiaYaw(units::mass::kilogram_t mass,
                                                units::length::meter_t length,
                                                units::length::meter_t width)
{
  return mass * (length * length + width * width) / units::dimensionless::scalar_t(12);
}

[[maybe_unused]] static std::optional<Common::Vector2d<units::length::meter_t>> CalculateIntersection(
    const Common::Vector2d<units::length::meter_t> &firstStartPoint,
    const Common::Vector2d<units::length::meter_t> &firstAxis,
    const Common::Vector2d<units::length::meter_t> &secondStartPoint,
    const Common::Vector2d<units::length::meter_t> &secondAxis)
{
  //Solve linear equation firstStartPoint + lambda * firstAxis = secondStart + kappa * secondAxis
  units::area::square_meter_t determinant
      = -firstAxis.x * secondAxis.y + firstAxis.y * secondAxis.x;  //Determinant of matrix (firstAxis -secondAxis)
  if (std::abs(determinant.value()) < EPSILON)
  {
    return std::nullopt;
  }
  units::dimensionless::scalar_t lambda = (-(secondStartPoint.x - firstStartPoint.x) * secondAxis.y
                                           + (secondStartPoint.y - firstStartPoint.y) * secondAxis.x)
                                        / determinant;
  units::length::meter_t intersectionPointX = firstStartPoint.x + lambda * firstAxis.x;
  units::length::meter_t intersectionPointY = firstStartPoint.y + lambda * firstAxis.y;
  return std::make_optional<Common::Vector2d<units::length::meter_t>>(intersectionPointX, intersectionPointY);
}

//! Calculates the net distance of the x and y coordinates of two bounding boxes
//!
//! \param ownBoundingBox       first bounding box
//! \param otherBoundingBox     second bounding box
//! \return net distance x, net distance y
[[maybe_unused]] static std::pair<double, double> GetCartesianNetDistance(const polygon_t &ownBoundingBox,
                                                                          const polygon_t &otherBoundingBox)
{
  double ownMaxX{std::numeric_limits<double>::lowest()};
  double ownMinX{std::numeric_limits<double>::max()};
  double ownMaxY{std::numeric_limits<double>::lowest()};
  double ownMinY{std::numeric_limits<double>::max()};
  for (const auto &point : ownBoundingBox.outer())
  {
    ownMaxX = std::max(ownMaxX, bg::get<0>(point));
    ownMinX = std::min(ownMinX, bg::get<0>(point));
    ownMaxY = std::max(ownMaxY, bg::get<1>(point));
    ownMinY = std::min(ownMinY, bg::get<1>(point));
  }
  double otherMaxX{std::numeric_limits<double>::lowest()};
  double otherMinX{std::numeric_limits<double>::max()};
  double otherMaxY{std::numeric_limits<double>::lowest()};
  double otherMinY{std::numeric_limits<double>::max()};
  for (const auto &point : otherBoundingBox.outer())
  {
    otherMaxX = std::max(otherMaxX, bg::get<0>(point));
    otherMinX = std::min(otherMinX, bg::get<0>(point));
    otherMaxY = std::max(otherMaxY, bg::get<1>(point));
    otherMinY = std::min(otherMinY, bg::get<1>(point));
  }
  double netX{0.0};
  if (ownMaxX < otherMinX)
  {
    netX = otherMinX - ownMaxX;
  }
  if (ownMinX > otherMaxX)
  {
    netX = otherMaxX - ownMinX;
  }
  double netY{0.0};
  if (ownMaxY < otherMinY)
  {
    netY = otherMinY - ownMaxY;
  }
  if (ownMinY > otherMaxY)
  {
    netY = otherMaxY - ownMinY;
  }
  return {netX, netY};
}

//! Calculates the net distance of the x and y coordinates of two bounding boxes
//!
//! \param vector       Vector which will be rotated
//! \param yaw          Angle of rotation
//! \return rotated vector
[[maybe_unused]] static mantle_api::Vec3<units::length::meter_t> RotateYaw(
    const mantle_api::Vec3<units::length::meter_t> &vector, units::angle::radian_t angle)
{
  auto cosValue = units::math::cos(angle);
  auto sinValue = units::math::sin(angle);
  units::length::meter_t newX = vector.x * cosValue - vector.y * sinValue;
  units::length::meter_t newY = vector.x * sinValue + vector.y * cosValue;

  return {newX, newY, vector.z};
}

/// Class representing calculation of intersection
class IntersectionCalculation
{
public:
  /**
   * @brief Checks whether a point lies within or on the edges
   *        of a convex quadrilateral.
   *
   * The quadrilateral must be convex and it points ordered clockwise:
   *              A
   *              #-------_______      B
   *             /   P           ------#
   *            /    #                  \
   *           /                         \
   *          #---------------------------#
   *          C                           D
   *
   * @param[in] A First corner of quadrilateral
   * @param[in] B Second corner of quadrilateral
   * @param[in] C Third corner of quadrilateral
   * @param[in] D Forth corner of quadrilateral
   * @param[in] P Point to be queried
   * @returns true if point is within on the edges of the quadrilateral
   */
  static OPENPASSCOMMONEXPORT bool IsWithin(const Common::Vector2d<units::length::meter_t> &A,
                                            const Common::Vector2d<units::length::meter_t> &B,
                                            const Common::Vector2d<units::length::meter_t> &C,
                                            const Common::Vector2d<units::length::meter_t> &D,
                                            const Common::Vector2d<units::length::meter_t> &P);

  //! \brief Calculates the intersection polygon of two quadrangles.
  //!
  //! This method calculates all points of the intersection polygon of two quadrangles.
  //! It is assumed, that both quadrangles are convex and that the points are given in clockwise order.
  //! If one or both are rectangles then a faster calculation is used
  //! Solve the linear equation "first point + lambda * first edge = second point + kappa * second edge" for each pair
  //! of edges to get the intersection of the edges. If both lamda and kappa are between 0 and 1, then the intersection
  //! lies on both edges. If the determinat is 0, then the two egdes are parallel.
  //!
  //! \param firstPoints          corner points of the first quadrangle in clockwise order
  //! \param secondPoints         corner points of the second quadrangle in clockwise order
  //! \param firstIsRectangular   specify that first quadrangele is rectangular
  //! \param secondIsRectangular  specify that second quadrangele is rectangular
  //! \return points of the intersection polygon
  static OPENPASSCOMMONEXPORT std::vector<Common::Vector2d<units::length::meter_t>> GetIntersectionPoints(
      const std::vector<Common::Vector2d<units::length::meter_t>> &firstPoints,
      const std::vector<Common::Vector2d<units::length::meter_t>> &secondPoints,
      bool firstIsRectangular = true,
      bool secondIsRectangular = true);

  //! \brief Calculates the intersection polygon of two quadrangles.
  //!
  //! This method calculates all points of the intersection polygon of two quadrangles.
  //! It is assumed, that both quadrangles are convex and that the points are given in clockwise order.
  //! If one or both are rectangles then a faster calculation is used
  //!
  //! \param firstPolygon         first quadrangle
  //! \param secondPolygon        second quadrangle
  //! \param firstIsRectangular   specify that first quadrangele is rectangular
  //! \param secondIsRectangular  specify that second quadrangele is rectangular
  //! \return points of the intersection polygon
  static OPENPASSCOMMONEXPORT std::vector<Common::Vector2d<units::length::meter_t>> GetIntersectionPoints(
      const polygon_t &firstPolygon,
      const polygon_t &secondPolygon,
      bool firstIsRectangular = true,
      bool secondIsRectangular = true);

private:
  static bool OnEdge(const Common::Vector2d<units::length::meter_t> &A,
                     const Common::Vector2d<units::length::meter_t> &B,
                     const Common::Vector2d<units::length::meter_t> &P);

  static bool WithinBarycentricCoords(units::area::square_meter_t dot00,
                                      units::area::square_meter_t dot02,
                                      units::area::square_meter_t dot01,
                                      units::area::square_meter_t dot11,
                                      units::area::square_meter_t dot12);
};

//-----------------------------------------------------------------------------
//! @brief Tokenizes string by delimiter.
//!
//! @param [in] str             String to be tokenized
//! @param [in] delimiter       Delimiter by which string gets tokenized
//!
//! @return                     Vector of trimmed tokens
//-----------------------------------------------------------------------------
[[maybe_unused]] static std::vector<std::string> TokenizeString(const std::string &str, const char delimiter = ',')
{
  constexpr const char *WHITESPACE{" \t\n\v\f\r"};
  std::vector<std::string> tokens;

  if (str.empty())
  {
    return tokens;
  }

  std::string_view remaining_input_view{str};
  auto tokenlength = static_cast<std::string_view::size_type>(-1);

  do
  {
    remaining_input_view.remove_prefix(tokenlength + 1);  // remove previous token from view
    tokenlength = remaining_input_view.find(delimiter);

    // if no delimiter is left, use whole string
    tokenlength = tokenlength != std::string::npos ? tokenlength : remaining_input_view.length();

    // untrimmed view on token
    std::string_view token{remaining_input_view.data(), tokenlength};
    auto trim_start = token.find_first_not_of(WHITESPACE);

    if (trim_start == std::string::npos)
    {
      // only whitespace charactes in current token
      tokens.push_back("");
      continue;
    }

    auto trim_end = token.find_last_not_of(WHITESPACE);
    tokens.emplace_back(token.substr(trim_start, trim_end - trim_start + 1));
  } while (tokenlength < remaining_input_view.length());

  return tokens;
}

//! Returns the directional road for which the heading is the lowest for a given position
//!
//! \param roadPositions    all possible road positions as calculated by the localization
//! \param world            world
//! \return road and direction with lowest heading
static RouteElement GetRoadWithLowestHeading(const GlobalRoadPositions &roadPositions, const WorldInterface &world)
{
  RouteElement bestFitting;
  units::angle::radian_t minHeading{std::numeric_limits<double>::max()};
  for (const auto [roadId, position] : roadPositions)
  {
    const auto absHeadingInOdDirection = units::math::abs(position.roadPosition.hdg);
    if (absHeadingInOdDirection < minHeading && world.IsDirectionalRoadExisting(roadId, true))
    {
      bestFitting = {roadId, true};
      minHeading = absHeadingInOdDirection;
    }
    const auto absHeadingAgainstOdDirection
        = units::math::abs(SetAngleToValidRange(position.roadPosition.hdg + units::angle::radian_t(M_PI)));
    if (absHeadingAgainstOdDirection < minHeading && world.IsDirectionalRoadExisting(roadId, false))
    {
      bestFitting = {roadId, false};
      minHeading = absHeadingAgainstOdDirection;
    }
  }
  return bestFitting;
}
};  // namespace CommonHelper

//-----------------------------------------------------------------------------
//! @brief Containing general static functions to evaluate traffic situations
//-----------------------------------------------------------------------------
class TrafficHelperFunctions
{
public:
  //! @brief Calculates the net time gap between two (moving) objects
  //!
  //! /details Time gap is the time until two objects collide if the front object stands
  //! still and the velocity of the rear object remains unchanged.
  //!
  //! Net in this context means that the carlengths are already substracted from
  //! the caller, i.e. netDistance = |sDist| - c
  //!   where c REK-usually in {carlengthEffective, carlength}
  //! More sophisticated would be c = dist(refPointRear, FrontEdgeRear) +
  //!  dist(refPointFront, RearEdgeFront) + MinGap
  //! where MinGap = 0 for true TTC, >0 for estimated TTC with a safety gap
  //!
  //! Default values of the function:
  //! -1 | netDistance < 0
  //! 0  | netDistance = 0
  //! netDistance | vRear <= 1
  //!
  //! Java: computeNetGap
  //!
  //! @param [in]   vRear         velocity of the rear object
  //! @param [in]   netDistance   distance between the objects
  //!
  //! @return time gap between the two objects
  static double CalculateNetTimeGap(double vRear, double netDistance)
  {
    if (netDistance < 0.0)
    {
      return -1.0;
    }

    return (netDistance <= 0.0) ? 0.0 : (vRear <= 1) ? netDistance : netDistance / vRear;
  }

  //-----------------------------------------------------------------------------
  //! @brief Calculates if two agents will collide during braking
  //!
  //! /details    validates if cars crash during brake phase
  //!             if velocity and acceleration of agents will be the same.
  //!
  //! @param [in]   sDelta     distance between both agents
  //! @param [in]   vEgo       velocity ego
  //! @param [in]   aEgo       acceleration ego
  //! @param [in]   vFront     velocity front
  //! @param [in]   aFront     acceleration front
  //!
  //! @return true if collision would happen
  //-----------------------------------------------------------------------------
  static bool WillCrashDuringBrake(units::length::meter_t sDelta,
                                   units::velocity::meters_per_second_t vEgo,
                                   units::acceleration::meters_per_second_squared_t aEgo,
                                   units::velocity::meters_per_second_t vFront,
                                   units::acceleration::meters_per_second_squared_t aFront)
  {
    //Calculate the intersection of the vehicles trajactory (quadratic functions)

    units::time::second_t stopTime = -vEgo / aEgo;  //timepoint where ego stops
    bool frontStopsEarlier = false;
    units::length::meter_t frontTravelDistance;

    if (aFront < 0_mps_sq)
    {
      auto stopTimeFront = -vFront / aFront;
      if (stopTimeFront < stopTime)  //special case: FrontVehicle stops earlier than Ego
      {
        frontStopsEarlier = true;
        frontTravelDistance = -0.5 * vFront * vFront / aFront;
      }
    }

    // acceleration equal -> difference of trajectories is linear
    if (aEgo == aFront)
    {
      // velocity is equal: distance constant -> no crash
      if (vEgo == vFront)
      {
        return false;
      }

      // intersection of lines
      units::time::second_t intersectionTime = sDelta / (vEgo - vFront);
      if (0_s <= intersectionTime && intersectionTime <= stopTime)
      {
        if (frontStopsEarlier)
        {
          auto travelDistance = -0.5 * stopTime * stopTime * aEgo;
          if (travelDistance < sDelta + frontTravelDistance)
          {
            return false;
          }
        }

        return true;
      }
    }

    // intersection of quadratic functions
    units::unit_t<units::squared<units::velocity::meters_per_second>> discriminant
        = (vEgo - vFront) * (vEgo - vFront) + 2 * (aEgo - aFront) * sDelta;

    if (discriminant < units::unit_t<units::squared<units::velocity::meters_per_second>>(0))
    {
      return false;
    }

    units::time::second_t intersectionPoint1 = ((vFront - vEgo) + units::math::sqrt(discriminant)) / (aEgo - aFront);
    units::time::second_t intersectionPoint2 = ((vFront - vEgo) - units::math::sqrt(discriminant)) / (aEgo - aFront);

    if ((0_s <= intersectionPoint1 && intersectionPoint1 <= stopTime)
        || (0_s <= intersectionPoint2 && intersectionPoint2 <= stopTime))
    {
      if (frontStopsEarlier)
      {
        units::length::meter_t travelDistance = -0.5 * stopTime * stopTime * aEgo;
        if (travelDistance < sDelta + frontTravelDistance)
        {
          return false;
        }
      }

      return true;
    }

    return false;
  }

  //-----------------------------------------------------------------------------
  //! @brief Calculates if two agents will collide during braking
  //!
  //! \details validates if two cars crash within time to brake
  //!             if velocity and acceleration of agents will be the same.
  //!             Triggers WillCrashDuringBrake
  //!
  //! @param [in]   sDelta                                distance between both agents
  //! @param [in]   vEgo                                  velocity rear agent (ego)
  //! @param [in]   assumedBrakeAccelerationEgo           acceleration rear agent (ego)
  //! @param [in]   vFront                                velocity front
  //! @param [in]   aFront                                acceleratin front
  //! @param [in]   assumedTtb                            assumed time to brake
  //!
  //! @return true if collision would happen
  //-----------------------------------------------------------------------------
  static bool WillCrash(units::length::meter_t sDelta,
                        units::velocity::meters_per_second_t vEgo,
                        units::acceleration::meters_per_second_squared_t assumedBrakeAccelerationEgo,
                        units::velocity::meters_per_second_t vFront,
                        units::acceleration::meters_per_second_squared_t aFront,
                        units::time::second_t assumedTtb)
  {
    auto sEgoAtTtb = vEgo * assumedTtb;

    // estimate values for front vehicle at t=ttb
    auto stopTimeFront
        = aFront < 0_mps_sq ? -vFront / aFront : units::time::second_t(std::numeric_limits<double>::max());
    auto tFront = units::math::min(stopTimeFront, assumedTtb);
    auto sFrontAtTtb = sDelta + vFront * tFront + aFront * tFront * tFront / 2;
    auto vFrontAtTtb = stopTimeFront < assumedTtb ? 0_mps : vFront + aFront * assumedTtb;
    auto aFrontAtTtb = stopTimeFront < assumedTtb ? 0_mps_sq : aFront;

    if (sFrontAtTtb <= sEgoAtTtb)
    {
      return true;
    }

    return WillCrashDuringBrake(sFrontAtTtb - sEgoAtTtb, vEgo, assumedBrakeAccelerationEgo, vFrontAtTtb, aFrontAtTtb);
  }

  /**
   * @brief Calculate the width left of the reference point of a leaning object
   *
   * @param width     width of the object
   * @param height    height of the object
   * @param roll      rolling angle
   * @return the width left of the reference point of a leaning object
   */
  static units::length::meter_t GetWidthLeft(units::length::meter_t width,
                                             units::length::meter_t height,
                                             units::angle::radian_t roll)
  {
    return 0.5 * width * units::math::cos(roll) + (roll < 0_rad ? height * units::math::sin(-roll) : 0_m);
  }

  /**
   * @brief Calculate the width right of the reference point of a leaning object
   *
   * @param width     width of the object
   * @param height    height of the object
   * @param roll      rolling angle
   * @return the width right of the reference point of a leaning object
   */
  static units::length::meter_t GetWidthRight(units::length::meter_t width,
                                              units::length::meter_t height,
                                              units::angle::radian_t roll)
  {
    return 0.5 * width * units::math::cos(roll) + (roll > 0_rad ? height * units::math::sin(roll) : 0_m);
  }
};

namespace helper::vector
{

/// \brief  convenience function to convert a vector into a string using a constom delimiter
/// \remark Please refer to the standard, how decimals are converted
/// \param  values     vector of strings
/// \param  delimiter  string in between the individual values
/// \return delimiter seperated list as string
template <typename T>
std::string to_string(const std::vector<T> &values, const std::string &delimiter = ",")
{
  if (values.empty())
  {
    return "";
  }

  std::ostringstream oss;
  std::copy(values.begin(), values.end(), std::ostream_iterator<T>(oss, delimiter.c_str()));

  return {oss.str(), 0, oss.str().length() - delimiter.size()};
}
}  // namespace helper::vector

namespace helper::map
{
/// @brief queries a map for a given key and returns the value if available
template <typename KeyType, typename ValueType>
std::optional<std::reference_wrapper<const ValueType>> query(const std::map<KeyType, ValueType> &the_map,
                                                             KeyType the_value)
{
  const auto &iter = the_map.find(the_value);
  return iter == the_map.end()
           ? std::nullopt
           : std::optional<std::reference_wrapper<const ValueType>>{std::cref<ValueType>(iter->second)};
}

template <typename ValueType>
std::optional<ValueType> query(const std::map<std::string, ValueType> &the_map, std::string the_value)
{
  const auto &iter = the_map.find(the_value);
  return iter == the_map.end() ? std::nullopt : std::make_optional(iter->second);
}

template <typename KeyType, typename ValueType>
std::optional<ValueType> query(const std::unordered_map<KeyType, ValueType> &the_map, KeyType the_value)
{
  const auto &iter = the_map.find(the_value);
  return iter == the_map.end() ? std::nullopt : std::make_optional(iter->second);
}

template <typename ValueType>
std::optional<ValueType> query(const std::unordered_map<std::string, ValueType> &the_map, std::string the_value)
{
  const auto &iter = the_map.find(the_value);
  return iter == the_map.end() ? std::nullopt : std::make_optional(iter->second);
}
}  // namespace helper::map

//-----------------------------------------------------------------------------
//! @brief Containing functions to calculate time to collision of two objects
//-----------------------------------------------------------------------------
class TtcCalculations
{
public:
  //-----------------------------------------------------------------------------
  //! @brief Structure to hold all necessary world object parameters for TTC calculation
  //-----------------------------------------------------------------------------
  struct TtcParameters
  {
    units::length::meter_t length;       ///< length of the object
    units::length::meter_t widthLeft;    ///< left width of the object
    units::length::meter_t widthRight;   ///< right width of the object
    units::length::meter_t frontLength;  ///< distance from reference point of object to leading edge of object
    units::length::meter_t backLength;   ///< distance from reference point to back edge of object
    point_t position;                    ///< position of the object
    units::velocity::meters_per_second_t velocityX;                             ///< velocity in x direction
    units::velocity::meters_per_second_t velocityY;                             ///< velocity in y direction
    units::acceleration::meters_per_second_squared_t accelerationX;             ///< acceleration in x direction
    units::acceleration::meters_per_second_squared_t accelerationY;             ///< acceleration in y direction
    units::angle::radian_t yaw;                                                 ///< yaw angle
    units::angular_velocity::radians_per_second_t yawRate;                      ///< yaw rate
    units::angular_acceleration::radians_per_second_squared_t yawAcceleration;  ///< yaw acceleration
  };
  //-----------------------------------------------------------------------------
  //! @brief Calculates the time to collision between two (moving) objects
  //! Time to collision (TTC) is the time until two objects collide if their
  //! accelerations remain unchanged.
  //!
  //! @param [in]   agent                                     Collided agent
  //! @param [in]   detectedObject                            Collided world object
  //! @param [in]   maxTtc                                    Maximum time to collision
  //! @param [in]   collisionDetectionLongitudinalBoundary    Longitudinal padding for collision detection
  //! @param [in]   collisionDetectionLateralBoundary         Lateral padding for collision detection
  //! @param [in]   cycleTime                                 Cycle time
  //!
  //! @return time to collsion between the two objects
  //-----------------------------------------------------------------------------
  static units::time::second_t CalculateObjectTTC(const AgentInterface &agent,
                                                  const WorldObjectInterface &detectedObject,
                                                  units::time::second_t maxTtc,
                                                  units::length::meter_t collisionDetectionLongitudinalBoundary,
                                                  units::length::meter_t collisionDetectionLateralBoundary,
                                                  double cycleTime)
  {
    TtcParameters agentParameters
        = GetTTCObjectParameters(&agent, collisionDetectionLongitudinalBoundary, collisionDetectionLateralBoundary);
    TtcParameters objectParameters = GetTTCObjectParameters(
        &detectedObject, collisionDetectionLongitudinalBoundary, collisionDetectionLateralBoundary);

    return CalculateObjectTTC(agentParameters, objectParameters, maxTtc, cycleTime);
  }

  /**
   * @brief Calculates the time to collision between two (moving) objects
   * Time to collision (TTC) is the time until two objects collide if their
   * accelerations remain unchanged.
   *
   * @param agentParameters   parameters of the agent
   * @param objectParameters  parameters of the moving object
   * @param maxTtc            maximum Time to collision
   * @param cycleTime         cycle time
   * @return time to collision
   */
  static units::time::second_t CalculateObjectTTC(TtcParameters agentParameters,
                                                  TtcParameters objectParameters,
                                                  units::time::second_t maxTtc,
                                                  double cycleTime)
  {
    const units::time::second_t timestep{cycleTime / 1000.0};
    units::time::second_t ttc{0.0};

    while (ttc <= maxTtc)
    {
      ttc += timestep;
      // Propagate parameters 1 timestep ahead
      PropagateParametersForward(agentParameters, timestep);
      PropagateParametersForward(objectParameters, timestep);
      // Calculate the new bounding boxes
      polygon_t agentBoundingBox = CalculateBoundingBox(agentParameters);
      polygon_t objectBoundingBox = CalculateBoundingBox(objectParameters);
      // Check for collision
      if (bg::intersects(agentBoundingBox, objectBoundingBox))
      {
        return ttc;
      }
    }
    // No collision detected
    return units::time::second_t(std::numeric_limits<double>::max());
  }

private:
  //-----------------------------------------------------------------------------
  //! @brief Retrieves the parameters to calculate the TTC for a world object
  //!
  //! @param [in]   object                                    world object
  //! @param [in]   collisionDetectionLongitudinalBoundary    longitudinal padding for collision detection
  //! @param [in]   collisionDetectionLateralBoundary         lateral padding for collision detection
  //!
  //! @return parameters needed for TTC calculation
  //-----------------------------------------------------------------------------
  static TtcParameters GetTTCObjectParameters(const WorldObjectInterface *object,
                                              units::length::meter_t collisionDetectionLongitudinalBoundary,
                                              units::length::meter_t collisionDetectionLateralBoundary)
  {
    TtcParameters parameters;

    // Initial bounding box in local coordinate system
    parameters.length = object->GetLength() + collisionDetectionLongitudinalBoundary;
    parameters.widthLeft
        = TrafficHelperFunctions::GetWidthLeft(object->GetWidth(), object->GetHeight(), object->GetRoll())
        + 0.5 * collisionDetectionLateralBoundary;
    parameters.widthRight
        = TrafficHelperFunctions::GetWidthRight(object->GetWidth(), object->GetHeight(), object->GetRoll())
        + 0.5 * collisionDetectionLateralBoundary;
    parameters.frontLength = object->GetDistanceReferencePointToLeadingEdge()
                           + 0.5 * collisionDetectionLongitudinalBoundary;  //returns the distance from reference point
                                                                            //of object to leading edge of object
    parameters.backLength
        = parameters.length - parameters.frontLength;  // distance from reference point to back edge of object

    // inital object values at current position
    parameters.position
        = {units::unit_cast<double>(object->GetPositionX()), units::unit_cast<double>(object->GetPositionY())};
    parameters.velocityX = object->GetVelocity().x;
    parameters.velocityY = object->GetVelocity().y;
    parameters.accelerationX = object->GetAcceleration().x;
    parameters.accelerationY = object->GetAcceleration().y;
    parameters.yaw = object->GetYaw();
    parameters.yawRate = 0.0_rad_per_s;
    parameters.yawAcceleration = 0.0_rad_per_s_sq;

    if (const AgentInterface *objectAsAgent
        = dynamic_cast<const AgentInterface *>(object))  // cast returns nullptr if unsuccessful
    {
      // object is movable -> get acceleration, yawRate and yawAcceleration
      parameters.yawRate = objectAsAgent->GetYawRate();
      // Not implemented yet:
      //parameters.yawAcceleration =  objectAsAgent->GetYawAcceleration();
    }

    return parameters;
  }

  //-----------------------------------------------------------------------------
  //! @brief Calculates the bounding box of world objects in world coordinates
  //!
  //! @param [in]   parameters    current state of the world objects reference point
  //!
  //! @return bounding box in world coordinates
  //-----------------------------------------------------------------------------
  static polygon_t CalculateBoundingBox(const TtcParameters parameters)
  {
    // construct corner points from reference point position and current yaw angle
    polygon_t box;
    bg::append(box,
               point_t{units::unit_cast<double>(units::length::meter_t(parameters.position.get<0>())
                                                - units::math::cos(parameters.yaw) * parameters.backLength
                                                + units::math::sin(parameters.yaw) * parameters.widthRight),
                       units::unit_cast<double>(units::length::meter_t(parameters.position.get<1>())
                                                - units::math::sin(parameters.yaw) * parameters.backLength
                                                - units::math::cos(parameters.yaw)
                                                      * parameters.widthRight)});  // back right corner
    bg::append(box,
               point_t{units::unit_cast<double>(units::length::meter_t(parameters.position.get<0>())
                                                - units::math::cos(parameters.yaw) * parameters.backLength
                                                - units::math::sin(parameters.yaw) * parameters.widthLeft),
                       units::unit_cast<double>(units::length::meter_t(parameters.position.get<1>())
                                                - units::math::sin(parameters.yaw) * parameters.backLength
                                                + units::math::cos(parameters.yaw)
                                                      * parameters.widthLeft)});  // back left corner
    bg::append(box,
               point_t{units::unit_cast<double>(units::length::meter_t(parameters.position.get<0>())
                                                + units::math::cos(parameters.yaw) * parameters.frontLength
                                                - units::math::sin(parameters.yaw) * parameters.widthLeft),
                       units::unit_cast<double>(units::length::meter_t(parameters.position.get<1>())
                                                + units::math::sin(parameters.yaw) * parameters.frontLength
                                                + units::math::cos(parameters.yaw)
                                                      * parameters.widthLeft)});  // front left corner
    bg::append(box,
               point_t{units::unit_cast<double>(units::length::meter_t(parameters.position.get<0>())
                                                + units::math::cos(parameters.yaw) * parameters.frontLength
                                                + units::math::sin(parameters.yaw) * parameters.widthRight),
                       units::unit_cast<double>(units::length::meter_t(parameters.position.get<1>())
                                                + units::math::sin(parameters.yaw) * parameters.frontLength
                                                - units::math::cos(parameters.yaw)
                                                      * parameters.widthRight)});  // front right corner
    bg::append(box,
               point_t{units::unit_cast<double>(units::length::meter_t(parameters.position.get<0>())
                                                - units::math::cos(parameters.yaw) * parameters.backLength
                                                + units::math::sin(parameters.yaw) * parameters.widthRight),
                       units::unit_cast<double>(units::length::meter_t(parameters.position.get<1>())
                                                - units::math::sin(parameters.yaw) * parameters.backLength
                                                - units::math::cos(parameters.yaw)
                                                      * parameters.widthRight)});  // back right corner
    return box;
  }
  //-----------------------------------------------------------------------------
  //! @brief Propagates a world object forward by one timestep
  //!
  //! @param [in]   parameters    current state of the world objects reference point
  //! @param [in]   timestep      time resolution of the TTC simulation
  //!
  //-----------------------------------------------------------------------------
  static void PropagateParametersForward(TtcParameters &parameters, const units::time::second_t timestep)
  {
    const units::length::meter_t positionX(units::length::meter_t(parameters.position.get<0>())
                                           + 0.5 * parameters.accelerationX * timestep * timestep
                                           + parameters.velocityX * timestep);
    const units::length::meter_t positionY(units::length::meter_t(parameters.position.get<1>())
                                           + 0.5 * parameters.accelerationY * timestep * timestep
                                           + parameters.velocityY * timestep);

    parameters.position.set<0>(units::unit_cast<double>(positionX));  // x-coordinate
    parameters.position.set<1>(units::unit_cast<double>(positionY));  // y-coordinate
    const units::angle::radian_t deltaYaw
        = 0.5 * parameters.yawAcceleration * timestep * timestep + parameters.yawRate * timestep;
    parameters.yaw += deltaYaw;
    parameters.velocityX
        = units::math::cos(deltaYaw) * parameters.velocityX - units::math::sin(deltaYaw) * parameters.velocityY;
    parameters.velocityY
        = units::math::sin(deltaYaw) * parameters.velocityX + units::math::cos(deltaYaw) * parameters.velocityY;
    parameters.accelerationX
        = units::math::cos(deltaYaw) * parameters.accelerationX - units::math::sin(deltaYaw) * parameters.accelerationY;
    parameters.accelerationY
        = units::math::sin(deltaYaw) * parameters.accelerationX + units::math::cos(deltaYaw) * parameters.accelerationY;
    parameters.velocityX += parameters.accelerationX * timestep;
    parameters.velocityY += parameters.accelerationY * timestep;
    parameters.yawRate += parameters.yawAcceleration * timestep;
  }
};
