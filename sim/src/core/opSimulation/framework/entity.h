/*******************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <memory>
#include <stdint.h>
#include <string>
#include <units.h>
#include <vector>

#include "common/globalDefinitions.h"
#include "common/opExport.h"
#include "include/agentBlueprintInterface.h"

class ScenarioControl;
class AgentInterface;
class RouteSamplerInterface;
class ScenarioControlInterface;
namespace mantle_api
{
struct EntityProperties;
struct RouteDefinition;
}  // namespace mantle_api

namespace core
{

//! This class represents an entity in a scenario
class SIMULATIONCOREEXPORT Entity : public virtual mantle_api::IEntity
{
public:
  //! Vehicle constructor
  //!
  //! @param[in] id               Unique id of the entity
  //! @param[in] name             Name of the entity
  //! @param[in] routeSampler     Route sampler
  //! @param[in] properties       Basic properties that describe scenario entities
  //! @param[in] agentCategory    Agent category classification
  Entity(mantle_api::UniqueId id,
         std::string name,
         const RouteSamplerInterface *routeSampler,
         std::shared_ptr<mantle_api::EntityProperties> properties,
         const AgentCategory agentCategory);

  mantle_api::UniqueId GetUniqueId() const override;

  void SetName(const std::string &name) override;
  const std::string &GetName() const override;

  void SetPosition(const mantle_api::Vec3<units::length::meter_t> &inert_pos) override;
  mantle_api::Vec3<units::length::meter_t> GetPosition() const override;

  void SetVelocity(const mantle_api::Vec3<units::velocity::meters_per_second_t> &velocity) override;
  mantle_api::Vec3<units::velocity::meters_per_second_t> GetVelocity() const override;

  void SetAcceleration(const mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> &acceleration) override;
  mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> GetAcceleration() const override;

  void SetOrientation(const mantle_api::Orientation3<units::angle::radian_t> &orientation) override;
  mantle_api::Orientation3<units::angle::radian_t> GetOrientation() const override;

  void SetOrientationRate(
      const mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> &orientation_rate) override;
  mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> GetOrientationRate() const override;

  void SetOrientationAcceleration(
      const mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>
          &orientation_acceleration) override;
  mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t> GetOrientationAcceleration()
      const override;

  void SetAssignedLaneIds(const std::vector<std::uint64_t> &assigned_lane_ids) override;
  std::vector<std::uint64_t> GetAssignedLaneIds() const override;

  void SetVisibility(const mantle_api::EntityVisibilityConfig &visibility) override;
  mantle_api::EntityVisibilityConfig GetVisibility() const override;

  //! @brief Method which assigns a route to an entity
  //!
  //! @param[in] route    Specifies how the route shall be constructed
  void AssignRoute(const mantle_api::RouteDefinition &route);

  //! @brief Method which sets the current system to the one passed as an argument
  //!
  //! @param[in] system   The new system to be used
  void SetSystem(const System &system);

  //! @brief Method which returns the components of an agent (name, category, etc.)
  //!
  //! @return The components of an agent
  AgentBuildInstructions GetAgentBuildInstructions();

  //! @brief Method which sets the current agent to the one passed as an argument
  //!
  //! @param[in] agent    The new agent to be used
  void SetAgent(AgentInterface *agent);

  //! Returns true if agent is still in World located.
  //!
  //! @return true if agent is in world
  bool IsAgentInWorld();

  //! @brief Method which retrieves the controll strategies
  //!
  //! @return Controll strategies
  std::shared_ptr<ScenarioControlInterface> const GetScenarioControl() const;

  //! @brief Method which returns the "shouldBeSpawned" flag
  //!
  //! @return "shouldBeSpawned" flag
  bool ShouldBeSpawned();

private:
  mantle_api::UniqueId id;
  std::string name;
  AgentCategory agentCategory;
  SpawnParameter spawnParameter;
  System system;
  AgentInterface *agent{nullptr};
  const RouteSamplerInterface *routeSampler;
  std::shared_ptr<mantle_api::EntityProperties> properties;
  bool shouldBeSpawned{false};
  std::shared_ptr<ScenarioControl> const scenarioControl;
};
}  // namespace core
