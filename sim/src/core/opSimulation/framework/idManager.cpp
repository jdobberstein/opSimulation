/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "idManager.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <algorithm>

namespace core
{

IdManager::IdManager()
{
  RegisterGroup(EntityType::kVehicle);
  RegisterGroup(EntityType::kController);
  RegisterGroup(EntityType::kObject);
  RegisterGroup(EntityType::kOther);
}

IdManager::~IdManager() = default;

mantle_api::UniqueId IdManager::Generate()
{
  return Generate(EntityType::kOther);
}

mantle_api::UniqueId IdManager::Generate(EntityType entityType)
{
  return _repository.at(entityType).GetNextIndex();
}

void IdManager::Reset(EntityType entityType)
{
  _repository.at(entityType).Reset();
}

void IdManager::RegisterGroup(EntityType entityType)
{
  _repository.emplace(entityType, EntityGroup(MAX_ENTITIES_PER_GROUP, MAX_ENTITIES_PER_GROUP * _repository.size()));
}

}  // namespace core