/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "commandLineParser.h"

#include <algorithm>
#include <iostream>

CommandLineArguments CommandLineParser::Parse(
    int argc,
    char* argv[],  // NOLINT(cppcoreguidelines-avoid-c-arrays, modernize-avoid-c-arrays)
    const std::string& version)
{
  // see struct declaration for defaults
  CommandLineArguments arguments{};

  po::options_description description("Options");
  // clang-format off
    description.add_options()(
        "help,h",
          "Print help message")(
        "version,v",
          "Show version information")(
        "logLevel",
          po::value<int>(&arguments.logLevel)->default_value(arguments.logLevel),
          "Log Level (0 - 5)")(
        "logFile",
          po::value<std::string>(&arguments.logFile)->default_value(arguments.logFile),
          "Path to the log file")(
        "lib",
          po::value<std::string>(&arguments.libPath)->default_value(arguments.libPath),
          "Root path of the libraries")(
        "configs",
          po::value<std::string>(&arguments.configsPath)->default_value(arguments.configsPath),
          "Path where to retrieve configuration files")(
        "results",
          po::value<std::string>(&arguments.resultsPath)->default_value(arguments.resultsPath),
          "Path where to put result files");
  // clang-format on

  po::variables_map variables_map;
  try
  {
    po::store(po::parse_command_line(argc, argv, description), variables_map);
    po::notify(variables_map);
  }
  catch (const po::error& e)
  {
    std::cerr << e.what() << std::endl;
    exit(EXIT_FAILURE);
  }

  if (variables_map.count("version"))
  {
    std::cout << argv[0] << " " << version << std::endl;  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    exit(EXIT_SUCCESS);
  }

  if (variables_map.count("help"))
  {
    std::cout << description << std::endl;
    exit(EXIT_SUCCESS);
  }

  arguments.logLevel = std::clamp(arguments.logLevel, 0, 5);
  return arguments;
};
