/********************************************************************************
 * Copyright (c) 2019-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
                 2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

//-----------------------------------------------------------------------------
/** \file  tasks.h
 *   \brief This file contains TaskItem and Tasks
 *   \details TaskItems are the data types to store in task set.
 *           Each task has a function for execution.
 */
//-----------------------------------------------------------------------------

#include <functional>
#include <set>

namespace core::scheduling
{

/// Type of task
enum TaskType
{
  Trigger,
  Update,
  Spawning,
  CollisionDetector,
  Observation,
  UpdateGlobalDrivingView,
  SyncGlobalData,
  Clear
};

//-----------------------------------------------------------------------------
/** \brief handles data to store scheduler tasks
 *
 *   \ingroup opSimulation
 */
//-----------------------------------------------------------------------------

class TaskItem
{
public:
  int agentId;                 ///< Id of the agent
  int priority;                ///< Priority of the task
  int cycletime;               ///< Cycle time
  int delay;                   ///< Delay/offset of task
  TaskType taskType;           ///< Type of task
  std::function<bool()> func;  ///< Funktion of task

  /**
   * @brief TaskItem constructor
   *
   * @param[in] agentId       Id of the agent
   * @param[in] priority      Priority of the task
   * @param[in] cycleTime     Cycle time
   * @param[in] delay         Delay/offset of task
   * @param[in] taskType      Type of task
   * @param[in] func          Funktion of task
   */
  TaskItem(int agentId, int priority, int cycleTime, int delay, TaskType taskType, std::function<bool()> func)
      : agentId(agentId), priority(priority), cycletime(cycleTime), delay(delay), taskType(taskType), func(func)
  {
  }
  virtual ~TaskItem() = default;

  static constexpr int VALID_FOR_ALL_AGENTS = -1;  ///< Task valid for all agents
  static constexpr int NO_DELAY = 0;               ///< No delay/offset of task

  static constexpr int PRIORITY_SPAWNING = 5;           ///< The priority of spawning
  static constexpr int PRIORITY_COLLISIONDETECTOR = 4;  ///< The priority of the collision detection
  static constexpr int PRIORITY_SYNCGLOBALDATA = 3;     ///< The priority of synchronizing the global data
  static constexpr int PRIORITY_OBSERVATION = 2;        ///< The priority of the observation
  static constexpr int PRIORITY_CLEAR = 1;              ///< The priority of the clear task

  /**
   * @brief Comparison operator. Needed for map sorting
   *
   * @param[in]   rhs TaskItem to compare to
   *
   * @return   true if the priority of the compared TaskItem is less than this priority
   */
  bool operator<(const TaskItem &rhs) const;

  /**
   * @brief Comparison operator
   *
   * @param[in]   rhs TaskItem to compare to
   *
   * @return   true if TaskItems are considered equal, false otherwise
   */
  bool operator==(const TaskItem &rhs) const;
};

//-----------------------------------------------------------------------------
/** \brief taskItem for triggering task */
//-----------------------------------------------------------------------------

class TriggerTaskItem : public TaskItem
{
public:
  /**
   * @brief TriggerTaskItem constructor
   *
   * @param[in] agentId       Id of the agent
   * @param[in] priority      Priority of the task
   * @param[in] cycleTime     Cycle time
   * @param[in] delay         Delay/offset of task
   * @param[in] task          Funktion of task
   */
  TriggerTaskItem(int agentId, int priority, int cycleTime, int delay, std::function<bool()> task)
      : TaskItem(agentId, priority, cycleTime, delay, TaskType::Trigger, task)
  {
  }
};

//-----------------------------------------------------------------------------
/** \brief taskItem for update task */
//-----------------------------------------------------------------------------

class UpdateTaskItem : public TaskItem
{
public:
  /**
   * @brief UpdateTaskItem constructor
   *
   * @param[in] agentId       Id of the agent
   * @param[in] priority      Priority of the task
   * @param[in] cycleTime     Cycle time
   * @param[in] delay         Delay/offset of task
   * @param[in] task          Funktion of task
   */
  UpdateTaskItem(int agentId, int priority, int cycleTime, int delay, std::function<bool()> task)
      : TaskItem(agentId, priority, cycleTime, delay, TaskType::Update, task)
  {
  }
};

//-----------------------------------------------------------------------------
/** \brief taskItem for spawning task */
//-----------------------------------------------------------------------------

class SpawningTaskItem : public TaskItem
{
public:
  /**
   * @brief SpawningTaskItem constructor
   *
   * @param[in] cycleTime     Cycle time
   * @param[in] task          Funktion of task
   */
  SpawningTaskItem(int cycleTime, std::function<bool()> task)
      : TaskItem(VALID_FOR_ALL_AGENTS, PRIORITY_SPAWNING, cycleTime, NO_DELAY, TaskType::Spawning, task)
  {
  }
};

//-----------------------------------------------------------------------------
/** \brief taskItem for collisionDetector task */
//-----------------------------------------------------------------------------

class CollisionDetectorTaskItem : public TaskItem
{
public:
  /**
   * @brief CollisionDetectorTaskItem constructor
   *
   * @param[in] cycleTime     Cycle time
   * @param[in] task          Funktion of task
   */
  CollisionDetectorTaskItem(int cycleTime, std::function<void()> task)
      : TaskItem(VALID_FOR_ALL_AGENTS,
                 PRIORITY_COLLISIONDETECTOR,
                 cycleTime,
                 NO_DELAY,
                 TaskType::CollisionDetector,
                 [task]
                 {
                   task();
                   return true;
                 })
  {
  }
};

//-----------------------------------------------------------------------------
/** \brief taskItem for observation task */
//-----------------------------------------------------------------------------

class ObservationTaskItem : public TaskItem
{
public:
  /**
   * @brief ObservationTaskItem constructor
   *
   * @param[in] cycleTime     Cycle time
   * @param[in] task          Funktion of task
   */
  ObservationTaskItem(int cycleTime, std::function<bool()> task)
      : TaskItem(VALID_FOR_ALL_AGENTS, PRIORITY_OBSERVATION, cycleTime, NO_DELAY, TaskType::Observation, task)
  {
  }
};

//-----------------------------------------------------------------------------
/** \brief taskItem for clear task */
//-----------------------------------------------------------------------------

class ClearTaskItem : public TaskItem
{
public:
  /**
   * @brief ClearTaskItem constructor
   *
   * @param[in] cycleTime     Cycle time
   * @param[in] task          Funktion of task
   */
  ClearTaskItem(int cycleTime, std::function<void()> task)
      : TaskItem(VALID_FOR_ALL_AGENTS,
                 PRIORITY_CLEAR,
                 cycleTime,
                 NO_DELAY,
                 TaskType::Clear,
                 [task]
                 {
                   task();
                   return true;
                 })
  {
  }
};

//-----------------------------------------------------------------------------
/** \brief Synchronize world state and reset controllers */
//-----------------------------------------------------------------------------

class SyncEnvironmentTaskItem : public TaskItem
{
public:
  /**
   * @brief SyncEnvironmentTaskItem constructor
   *
   * @param[in] cycleTime     Cycle time
   * @param[in] task          Funktion of task
   */
  SyncEnvironmentTaskItem(int cycleTime, std::function<void()> task)
      : TaskItem(VALID_FOR_ALL_AGENTS,
                 PRIORITY_SYNCGLOBALDATA,
                 cycleTime,
                 NO_DELAY,
                 TaskType::Observation,
                 [task]
                 {
                   task();
                   return true;
                 })
  {
  }
};

//-----------------------------------------------------------------------------
/** \brief taskItem for synchronize world task */
//-----------------------------------------------------------------------------

class SyncWorldTaskItem : public TaskItem
{
public:
  /**
   * @brief SyncWorldTaskItem constructor
   *
   * @param[in] cycleTime     Cycle time
   * @param[in] task          Funktion of task
   */
  SyncWorldTaskItem(int cycleTime, std::function<void()> task)
      : TaskItem(VALID_FOR_ALL_AGENTS,
                 PRIORITY_SYNCGLOBALDATA,
                 cycleTime,
                 NO_DELAY,
                 TaskType::SyncGlobalData,
                 [task]
                 {
                   task();
                   return true;
                 })
  {
  }
};

//-----------------------------------------------------------------------------
/** \brief stores taskItems in multiset
 *
 *   \ingroup opSimulation
 */
//-----------------------------------------------------------------------------

class Tasks
{
public:
  /**
   * @brief AddTask
   *
   * @details add given taskItem to intern multiset tasks
   *
   *
   * @param[in]     newTask    Subclass of taskItem
   */
  void AddTask(const TaskItem &newTask);

  /**
   * @brief DeleteTasks
   *
   * @details add given taskItem to intern multiset tasks
   *
   *
   * @param[in]     agentId    Id of removed agent to filter it out of tasks
   */
  void DeleteTasks(int agentId);

  /// List of TaskItems (all tasks)
  std::multiset<TaskItem> tasks;
};

}  //namespace core::scheduling