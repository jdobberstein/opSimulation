/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "pedestrian.h"

#include "common/globalDefinitions.h"

class RouteSamplerInterface;

namespace core
{
Pedestrian::Pedestrian(mantle_api::UniqueId id,
                       const std::string &name,
                       const std::shared_ptr<mantle_api::PedestrianProperties> &properties,
                       const RouteSamplerInterface *routeSampler)
    : Entity(id, name, routeSampler, properties, AgentCategory::Scenario), properties(properties)
{
}

void Pedestrian::SetProperties(std::unique_ptr<mantle_api::EntityProperties> properties) {}

mantle_api::PedestrianProperties *Pedestrian::GetProperties() const
{
  return properties.get();
}
}  // namespace core
