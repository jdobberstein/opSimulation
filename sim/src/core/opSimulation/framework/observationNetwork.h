/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  observationNetwork.h
//! @brief This file implements the container of all observation modules.
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <memory>
#include <string>

#include "common/observationLibraryDefinitions.h"
#include "common/opExport.h"
#include "include/observationNetworkInterface.h"

class DataBufferReadInterface;
class StochasticsInterface;
class WorldInterface;

namespace core
{

class ObservationBinding;
class ObservationModule;
class RunResult;

//! This class represents the container of all observation modules
class SIMULATIONCOREEXPORT ObservationNetwork : public ObservationNetworkInterface
{
public:
  /**
   * @brief ObservationNetwork constructor
   *
   * @param[in] bindings  Observation bindings
   */
  ObservationNetwork(std::map<std::string, ObservationBinding>* bindings) : bindings(bindings) {}
  ObservationNetwork(const ObservationNetwork&) = delete;
  ObservationNetwork(ObservationNetwork&&) = delete;
  ObservationNetwork& operator=(const ObservationNetwork&) = delete;
  ObservationNetwork& operator=(ObservationNetwork&&) = delete;

  virtual bool Instantiate(const ObservationInstanceCollection& observationInstances,
                           StochasticsInterface* stochastics,
                           WorldInterface* world,
                           DataBufferReadInterface* dataBuffer) override;
  const std::map<int, std::unique_ptr<ObservationModule>>& GetObservationModules() const override;

  bool InitAll() override;
  bool InitRun() override;
  bool UpdateTimeStep(int time, RunResult& runResult) override;
  bool FinalizeRun(const RunResult& result) override;
  bool FinalizeAll() override;

private:
  std::map<std::string, ObservationBinding>* bindings;
  std::map<int, std::unique_ptr<ObservationModule>> modules;
};

}  // namespace core
