/********************************************************************************
 * Copyright (c) 2017-2020 ITK Engineering GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "collisionDetection_Impact_implementation.h"

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <algorithm>
#include <cmath>
#include <limits>
#include <memory>
#include <ostream>

#include "common/commonTools.h"
#include "common/postCrashDynamic.h"
#include "include/agentInterface.h"
#include "polygon.h"

namespace
{

// counter-clockwise for Sutherland-Hodgman Algorithm
using CornerType = enum { UpperLeft = 0, LowerLeft, LowerRight, UpperRight, NumberCorners };

}  // namespace

std::vector<Common::Vector2d<units::length::meter_t>> CollisionDetectionPostCrash::GetAgentCorners(
    const AgentInterface *agent)
{
  Common::Vector2d<units::length::meter_t> agentPosition(
      agent->GetPositionX(), agent->GetPositionY());  // reference point (rear axle) in glabal CS
  const auto agentAngle = agent->GetYaw();

  const auto offSetGeometricCenter = agent->GetVehicleModelParameters()->bounding_box.geometric_center.x;
  agentPosition.x
      += units::math::cos(agentAngle) * offSetGeometricCenter;  // geometrical center of vehicle in global CS
  agentPosition.y
      += units::math::sin(agentAngle) * offSetGeometricCenter;  // geometrical center of vehicle in global CS

  const auto agentLength = agent->GetLength();
  const auto agentWidthHalf = agent->GetWidth() / 2;
  const auto agentDistanceCenter = agent->GetDistanceReferencePointToLeadingEdge();

  std::vector<Common::Vector2d<units::length::meter_t>> resultCorners;
  resultCorners.reserve(NumberCorners);
  for (int i = 0; i < NumberCorners; ++i)
  {
    resultCorners.emplace_back(0_m, 0_m);
  }

  // upper left corner for angle == 0
  resultCorners[UpperLeft].x = agentDistanceCenter - agentLength;
  resultCorners[UpperLeft].y = agentWidthHalf;

  // upper right corner for angle == 0
  resultCorners[UpperRight].x = agentDistanceCenter;
  resultCorners[UpperRight].y = agentWidthHalf;

  // lower right corner for angle == 0
  resultCorners[LowerRight].x = agentDistanceCenter;
  resultCorners[LowerRight].y = -agentWidthHalf;

  // lower left corner for angle == 0
  resultCorners[LowerLeft].x = agentDistanceCenter - agentLength;
  resultCorners[LowerLeft].y = -agentWidthHalf;

  // transform corners with actual angle and translate to position
  for (int i = 0; i < NumberCorners; ++i)
  {
    resultCorners[i].Rotate(agentAngle);
    resultCorners[i].Translate(agentPosition.x, agentPosition.y);
  }

  return resultCorners;
}

std::vector<Common::Vector2d<units::length::meter_t>> CollisionDetectionPostCrash::CalculateAllIntersectionPoints(
    std::vector<Common::Vector2d<units::length::meter_t>> corners1,
    std::vector<Common::Vector2d<units::length::meter_t>> corners2)
{
  // Sutherland-Hodgman-Algorith for polygon clipping
  for (size_t i1 = 0; i1 < corners1.size(); i1++)
  {  // loop over 1st polygon, must be convex

    // resulting polygon
    std::vector<Common::Vector2d<units::length::meter_t>> cornersIntersection;
    // indices of neighboring corners of polygon1
    auto indices11 = i1;                          // 1st polygon, 1st corner
    auto indices12 = (i1 + 1) % corners1.size();  // 1st polygon, 2nd corner

    // calculate normalized normal on polygon edge and distance for Hesse normal form
    Common::Vector2d<units::length::meter_t> normal1;
    normal1.x = corners1[indices12].y - corners1[indices11].y;
    normal1.y = -(corners1[indices12].x - corners1[indices11].x);

    auto tmpNormalizedVector = normal1.Norm();
    normal1.x = units::length::meter_t(tmpNormalizedVector.x.value());
    normal1.y = units::length::meter_t(tmpNormalizedVector.y.value());

    auto distance1 = normal1.Dot(corners1[indices11]);

    for (size_t i2 = 0; i2 < corners2.size(); i2++)
    {                                               // loop over 2nd polygon
      auto indices21 = i2;                          // 2nd polygon, 1st corner
      auto indices22 = (i2 + 1) % corners2.size();  // 2nd polygon, 2nd corner

      // calculate normalized normal on polygon edge and distance for Hesse normal form
      Common::Vector2d<units::length::meter_t> normal2;
      normal2.x = corners2[indices22].y - corners2[indices21].y;
      normal2.y = -(corners2[indices22].x - corners2[indices21].x);
      normal2.Norm();
      auto distance2 = normal2.Dot(corners2[indices21]);

      // check if edges are parallel
      bool areParallel
          = units::math::fabs(normal1.x - normal2.x) < units::length::meter_t(std::numeric_limits<double>::epsilon())
         && units::math::fabs(normal1.y - normal2.y) < units::length::meter_t(std::numeric_limits<double>::epsilon());
      bool areAntiParallel
          = units::math::fabs(normal1.x + normal2.x) < units::length::meter_t(std::numeric_limits<double>::epsilon())
         && units::math::fabs(normal1.y + normal2.y) < units::length::meter_t(std::numeric_limits<double>::epsilon());
      if (areParallel || areAntiParallel)
      {
        if (normal1.Dot(corners2[indices21]) <= distance1)
        {
          // first point of the edge of polygon2 is inside the edge of polygon1
          // => add first point of polygon2
          cornersIntersection.push_back(corners2[indices21]);
          continue;
        }
      }

      Common::Vector2d<units::length::meter_t> intersectionPoint;
      GetIntersectionPoint(normal1, normal2, distance1, distance2, intersectionPoint);

      auto dist1 = normal1.Dot(corners2[indices21]) - distance1;
      auto dist2 = normal1.Dot(corners2[indices22]) - distance1;
      if (dist1 <= 0_sq_m && dist2 > 0_sq_m)
      {
        // inside -> outside
        cornersIntersection.push_back(corners2[indices21]);
        cornersIntersection.push_back(intersectionPoint);
      }
      else if (dist1 > 0_sq_m && dist2 > 0_sq_m)
      {
        // outside -> outside
        continue;
      }
      else if (dist1 > 0_sq_m && dist2 <= 0_sq_m)
      {
        // outside -> inside
        cornersIntersection.push_back(intersectionPoint);
      }
      else
      {
        // inside -> inside
        cornersIntersection.push_back(corners2[indices21]);
      }
    }
    corners2 = cornersIntersection;
  }

  return corners2;
}

bool CollisionDetectionPostCrash::CalculatePlaneOfContact(Polygon intersection,
                                                          std::vector<int> vertexTypes,
                                                          Common::Vector2d<units::length::meter_t> &pointOfImpact,
                                                          units::angle::radian_t &phi)
{
  if (!intersection.CalculateCentroid(pointOfImpact))
  {
    return false;
  }

  if (intersection.GetNumberOfVertices() != vertexTypes.size())
  {
    return false;
  }

  std::vector<size_t> indices2;  // indices of vertices with vertexType = 2
  std::vector<size_t> indices3;  // indices of vertices with vertexType = 3

  for (size_t iV = 0; iV < intersection.GetNumberOfVertices(); iV++)
  {
    if (vertexTypes[iV] == 2)
    {
      indices2.push_back(iV);
    }
    else if (vertexTypes[iV] == 3)
    {
      indices3.push_back(iV);
    }
  }

  std::vector<Common::Vector2d<units::length::meter_t>> vertices = intersection.GetVertices();
  Common::Vector2d<units::length::meter_t> vecPlane;
  if (indices2.size() == 2)
  {
    vecPlane = vertices[indices2[0]] - (vertices[indices2[1]]);
  }
  else if (indices3.size() > 1)
  {
    vecPlane = vertices[indices3[0]] - (vertices[indices3[1]]);
  }
  else
  {
    return false;
  }

  if (vecPlane.x >= 0_m)
  {
    phi = units::math::atan(vecPlane.y / vecPlane.x);
  }
  else if (vecPlane.x < 0_m && vecPlane.y >= 0_m)
  {
    phi = units::math::atan(vecPlane.y / vecPlane.x) + units::angle::radian_t(M_PI);
  }
  else if (vecPlane.x < 0_m && vecPlane.y < 0_m)
  {
    phi = units::math::atan(vecPlane.y / vecPlane.x) - units::angle::radian_t(M_PI);
  }
  if (phi < 0_rad)
  {
    phi = phi + units::angle::radian_t(M_PI);
  }

  return true;
}

bool CollisionDetectionPostCrash::CalculatePostCrashDynamic(Common::Vector2d<units::length::meter_t> cog1,
                                                            const AgentInterface *agent1,
                                                            Common::Vector2d<units::length::meter_t> cog2,
                                                            const AgentInterface *agent2,
                                                            Common::Vector2d<units::length::meter_t> poi,
                                                            units::angle::radian_t phi,
                                                            PostCrashDynamic *postCrashDynamic1,
                                                            PostCrashDynamic *postCrashDynamic2)
{
  // input parameters of agent 1
  auto yaw1 = agent1->GetYaw();                                  // yaw angle
  units::angular_velocity::radians_per_second_t yawVel1{0};      // pre crash yaw velocity [rad/s]
  const auto vel1 = agent1->GetVelocity().Length();              // absolute velocity of first vehicle [m/s]
  auto velDir1 = yaw1;                                           // velocity direction of first vehicle
  const auto mass1 = agent1->GetVehicleModelParameters()->mass;  // mass of first vehicle [kg]

  auto length1 = agent1->GetLength();
  auto width1 = agent1->GetWidth();
  const auto momentIntertiaYaw1
      = CommonHelper::CalculateMomentInertiaYaw(mass1, length1, width1);  // moment of inertia 1st vehicle [kg*m^2]

  // input parameters of agent 2
  auto yaw2 = agent2->GetYaw();                                  // yaw angle [rad]
  units::angular_velocity::radians_per_second_t yawVel2{0};      // pre crash yaw velocity [rad/s]
  const auto vel2 = agent2->GetVelocity().Length();              // absolute velocity of 2nd vehicle [m/s]
  auto velDir2 = yaw2;                                           // velocity direction of 2nd vehicle [rad]
  const auto mass2 = agent2->GetVehicleModelParameters()->mass;  // mass of 2nd vehicle [kg]
  auto length2 = agent2->GetLength();
  auto width2 = agent2->GetWidth();
  const auto momentIntertiaYaw2
      = CommonHelper::CalculateMomentInertiaYaw(mass2, length2, width2);  // moment of inertia 2nd vehicle [kg*m^2]

  // new coordinate system axis: tangent and normal to contact plane
  Common::Vector2d<units::dimensionless::scalar_t> tang
      = Common::Vector2d<units::dimensionless::scalar_t>(units::math::cos(phi), units::math::sin(phi));
  Common::Vector2d<units::dimensionless::scalar_t> normal
      = Common::Vector2d<units::dimensionless::scalar_t>(-units::math::sin(phi), units::math::cos(phi));
  // distance of centers of gravity from point of impact in normal and
  // tangential direction: coordinates in new coordinate system
  units::length::meter_t normal1 = normal.x * cog1.x + normal.y * cog1.y - normal.x * poi.x - normal.y * poi.y;
  units::length::meter_t normal2 = normal.x * cog2.x + normal.y * cog2.y - normal.x * poi.x - normal.y * poi.y;
  units::length::meter_t tang1 = tang.x * cog1.x + tang.y * cog1.y - tang.x * poi.x - tang.y * poi.y;
  units::length::meter_t tang2 = tang.x * cog2.x + tang.y * cog2.y - tang.x * poi.x - tang.y * poi.y;
  // pre crash velocity vector, global coordinate system
  Common::Vector2d<units::velocity::meters_per_second_t> velocity1
      = Common::Vector2d<units::velocity::meters_per_second_t>(vel1 * units::math::cos(velDir1),
                                                               vel1 * units::math::sin(velDir1));
  Common::Vector2d<units::velocity::meters_per_second_t> velocity2
      = Common::Vector2d<units::velocity::meters_per_second_t>(vel2 * units::math::cos(velDir2),
                                                               vel2 * units::math::sin(velDir2));

  // COG-velocity in normal direction
  units::velocity::meters_per_second_t v1normCog = normal.x * velocity1.x + normal.y * velocity1.y;
  units::velocity::meters_per_second_t v2normCog = normal.x * velocity2.x + normal.y * velocity2.y;
  // velocity of point of impact in normal direction
  units::velocity::meters_per_second_t v1norm = v1normCog - yawVel1 * tang1 * units::inverse_radian(1);
  units::velocity::meters_per_second_t v2norm = v2normCog - yawVel2 * tang2 * units::inverse_radian(1);
  // relative velocity pre crash in normal direction
  auto vrelPreNorm = v1norm - v2norm;

  // COG-velocity in tangential direction
  units::velocity::meters_per_second_t v1tangCog = tang.x * velocity1.x + tang.y * velocity1.y;
  units::velocity::meters_per_second_t v2tangCog = tang.x * velocity2.x + tang.y * velocity2.y;
  // velocity of point of impact in tangential direction
  units::velocity::meters_per_second_t v1tang = v1tangCog + yawVel1 * normal1 * units::inverse_radian(1);
  units::velocity::meters_per_second_t v2tang = v2tangCog + yawVel2 * normal2 * units::inverse_radian(1);
  // relative velocity pre crash in tangential direction
  auto vrelPreTang = v1tang - v2tang;

  // auxiliary parameters
  units::unit_t<units::inverse<units::mass::kilogram>> param1
      = 1 / mass1 + 1 / mass2 + normal1 * normal1 / momentIntertiaYaw1 + normal2 * normal2 / momentIntertiaYaw2;
  units::unit_t<units::inverse<units::mass::kilogram>> param2
      = 1 / mass1 + 1 / mass2 + tang1 * tang1 / momentIntertiaYaw1 + tang2 * tang2 / momentIntertiaYaw2;
  units::unit_t<units::inverse<units::mass::kilogram>> param3
      = tang1 * normal1 / momentIntertiaYaw1 + tang2 * normal2 / momentIntertiaYaw2;
  // compute pulse vector in compression phase
  units::impulse pulseTc = (param3 * vrelPreNorm + param2 * vrelPreTang) / (param3 * param3 - param1 * param2);
  units::impulse pulseNc = (param1 * vrelPreNorm + param3 * vrelPreTang) / (param3 * param3 - param1 * param2);

  bool out1Sliding = false;
  bool out2Sliding = false;
  if (units::math::fabs(pulseTc) > units::math::fabs(interFriction * pulseNc))
  {
    out1Sliding = true;
    out2Sliding = true;
    // LOG(CbkLogLevel::Warning,
    //     "Sliding collision detected. Calculation of post-crash dynamics not valid for sliding collisions.");
  }
  // vector total pulse
  units::impulse totalPulseT = pulseTc * (1 + coeffRest);
  units::impulse totalPulseN = pulseNc * (1 + coeffRest);

  // relative post crash velocities: should be (very close to) ZERO! Just a check
  units::velocity::meters_per_second_t vrelPostTang = vrelPreTang + param1 * pulseTc - param3 * pulseNc;
  units::velocity::meters_per_second_t vrelPostNorm = vrelPreNorm - param3 * pulseTc + param2 * pulseNc;

  if ((units::math::fabs(vrelPostNorm) > 1E-3_mps) || (units::math::fabs(vrelPostTang) > 1E-3_mps))
  {
    std::stringstream msg;
    msg << "PostCrasDynamic Check: "
        << "Relative post crash velocities are too high: "
        << "tang: " << vrelPostTang << "norm: " << vrelPostNorm;
    LOG(CbkLogLevel::Warning, msg.str());
  }

  // compute post crash velocities in COG with momentum balance equations
  units::velocity::meters_per_second_t v1tangPostCog = totalPulseT / mass1 + v1tangCog;
  units::velocity::meters_per_second_t v1normPostCog = totalPulseN / mass1 + v1normCog;
  units::velocity::meters_per_second_t v2tangPostCog = -totalPulseT / mass2 + v2tangCog;
  units::velocity::meters_per_second_t v2normPostCog = -totalPulseN / mass2 + v2normCog;

  // matrix for coordinate transformation between global and local
  // POI-coordinates
  // pulse vector in global system
  Common::Vector2d<units::impulse> pulse{totalPulseT, totalPulseN};
  pulse.Rotate(phi);
  // Pulse direction
  units::angle::radian_t out1PulseDir{0.0};
  if (pulse.x >= units::impulse(0))
  {
    out1PulseDir = units::math::atan(pulse.y / pulse.x);
  }
  else if (pulse.x < units::impulse(0) && pulse.y >= units::impulse(0))
  {
    out1PulseDir = units::angle::radian_t(units::math::atan(pulse.y / pulse.x) + units::angle::radian_t(M_PI));
  }
  else if (pulse.x < units::impulse(0) && pulse.y < units::impulse(0))
  {
    out1PulseDir = units::angle::radian_t(units::math::atan(pulse.y / pulse.x) - units::angle::radian_t(M_PI));
  }
  auto out2PulseDir = out1PulseDir;

  auto pulseLength = pulse.Length();

  Common::Vector2d<units::impulse> out1PulseLocal = Common::Vector2d<units::impulse>(
      pulseLength * units::math::cos(out1PulseDir - yaw1), pulseLength * units::math::sin(out1PulseDir - yaw1));
  Common::Vector2d<units::impulse> out2PulseLocal = Common::Vector2d<units::impulse>(
      -pulseLength * units::math::cos(out2PulseDir - yaw2), -pulseLength * units::math::sin(out2PulseDir - yaw2));

  // --- compute output for vehicle 1
  // post crash velocity vector
  Common::Vector2d<units::velocity::meters_per_second_t> v1Post
      = Common::Vector2d<units::velocity::meters_per_second_t>(v1tangPostCog, v1normPostCog);
  v1Post.Rotate(phi);
  // absolute velocity
  units::velocity::meters_per_second_t out1Vel{v1Post.Length()};
  // velocity change delta-v
  units::velocity::meters_per_second_t out1DeltaV{(velocity1 - v1Post).Length()};
  // velocity direction
  units::angle::radian_t out1VelDir{v1Post.Angle()};
  // yaw velocity from angular momentum conservation equation
  units::angular_velocity::radians_per_second_t out1YawVel
      = 1_rad * (totalPulseT * normal1 - totalPulseN * tang1) / momentIntertiaYaw1 + yawVel1;
  Common::Vector2d out1Pulse = pulse;
  // rotation of poi from global to local
  Common::Vector2d out1PoiLocal = poi - cog1;
  out1PoiLocal.Rotate(-yaw1);
  auto out1CollVel = vel1;

  // --- compute output for vehicle 2
  // post crash velocity vector
  Common::Vector2d<units::velocity::meters_per_second_t> v2Post
      = Common::Vector2d<units::velocity::meters_per_second_t>(v2tangPostCog, v2normPostCog);
  v2Post.Rotate(phi);
  // absolute velocity
  units::velocity::meters_per_second_t out2Vel{v2Post.Length()};
  // velocity change delta-v
  units::velocity::meters_per_second_t out2DeltaV{(velocity2 - v2Post).Length()};
  // velocity direction
  units::angle::radian_t out2VelDir{v2Post.Angle()};
  // yaw velocity from angular momentum conservation equation
  units::angular_velocity::radians_per_second_t out2YawVel
      = 1_rad * (-totalPulseT * normal2 + totalPulseN * tang2) / momentIntertiaYaw2 + yawVel2;
  Common::Vector2d out2Pulse = pulse;
  // rotation of poi from global to local
  Common::Vector2d out2PoiLocal = poi - cog2;
  out2PoiLocal.Rotate(-yaw2);
  const auto out2CollVel = vel2;

  *postCrashDynamic1 = PostCrashDynamic(out1Vel,
                                        out1DeltaV,
                                        out1VelDir,
                                        out1YawVel,
                                        out1Pulse,
                                        out1PulseDir,
                                        out1PulseLocal,
                                        out1PoiLocal,
                                        out1CollVel,
                                        out1Sliding);

  *postCrashDynamic2 = PostCrashDynamic(out2Vel,
                                        out2DeltaV,
                                        out2VelDir,
                                        out2YawVel,
                                        out2Pulse,
                                        out2PulseDir,
                                        out2PulseLocal,
                                        out2PoiLocal,
                                        out2CollVel,
                                        out2Sliding);

  //for debug purpose
  // LogPostCrashDynamic(postCrashDynamic1, agent1->GetId());
  // LogPostCrashDynamic(postCrashDynamic2, agent2->GetId());

  return true;  // Calculation successful
}

bool CollisionDetectionPostCrash::GetIntersectionPoint(Common::Vector2d<units::length::meter_t> normalVector1,
                                                       Common::Vector2d<units::length::meter_t> normalVector2,
                                                       units::area::square_meter_t distance1,
                                                       units::area::square_meter_t distance2,
                                                       Common::Vector2d<units::length::meter_t> &intersectionPoint)
{
  units::area::square_meter_t det = (normalVector1.x * normalVector2.y - normalVector1.y * normalVector2.x);

  if (fabs(det.value()) < std::numeric_limits<double>::epsilon())
  {
    return false;
  }

  intersectionPoint.x = (distance1 * normalVector2.y - distance2 * normalVector1.y) / det;

  intersectionPoint.y = (distance2 * normalVector1.x - distance1 * normalVector2.x) / det;

  return true;
}

bool CollisionDetectionPostCrash::GetFirstContact(const AgentInterface *agent1,
                                                  const AgentInterface *agent2,
                                                  int &timeFirstContact)
{
  std::vector<Common::Vector2d<units::length::meter_t>> agent1Corners = GetAgentCorners(agent1);
  std::vector<Common::Vector2d<units::length::meter_t>> agent2Corners = GetAgentCorners(agent2);

  Polygon agent1Polygon(agent1Corners);

  Polygon agent2Polygon(agent2Corners);

  Common::Vector2d<units::velocity::meters_per_second_t> agent1VelocityVector = GetAgentVelocityVector(agent1);
  Common::Vector2d<units::velocity::meters_per_second_t> agent2VelocityVector = GetAgentVelocityVector(agent2);

  int cycleTime = 100;  // assumption

  timeFirstContact = 0;       //start of range
  int lastTimeNoContact = 0;  //end of range

  // Check if velocities are nearly same. If true, time of first contact will be very high
  // (infinity if velocities are exactly the same)
  if ((agent1VelocityVector - agent2VelocityVector).Length() < units::velocity::meters_per_second_t(1E-5))
  {
    return false;
  }

  //get last time with no contact --> calculate end of range
  bool intersected = true;
  while (intersected)
  {
    timeFirstContact = lastTimeNoContact;
    lastTimeNoContact -= cycleTime;  // one cycleTime to the past --> negative

    const auto time = units::make_unit<units::time::millisecond_t>(lastTimeNoContact);
    Common::Vector2d<units::length::meter_t> shiftVector1{agent1VelocityVector.x * time, agent1VelocityVector.y * time};
    Common::Vector2d<units::length::meter_t> shiftVector2{agent2VelocityVector.x * time, agent2VelocityVector.y * time};

    intersected = ShiftPolygonsAndCheckIntersection(agent1Polygon, agent2Polygon, shiftVector1, shiftVector2);
  }

  bool everIntersected = false;
  //calculate first time of contact with range getting smaller
  while (std::abs(timeFirstContact - lastTimeNoContact) > 1)
  {
    int nextTime = lastTimeNoContact - (lastTimeNoContact - timeFirstContact) / 2;
    const auto time = units::make_unit<units::time::millisecond_t>(nextTime);

    Common::Vector2d<units::length::meter_t> shiftVector1{agent1VelocityVector.x * time, agent1VelocityVector.y * time};
    Common::Vector2d<units::length::meter_t> shiftVector2{agent2VelocityVector.x * time, agent2VelocityVector.y * time};

    intersected = ShiftPolygonsAndCheckIntersection(agent1Polygon, agent2Polygon, shiftVector1, shiftVector2);

    if (intersected)
    {
      timeFirstContact = nextTime;
      everIntersected = true;
    }
    else
    {
      lastTimeNoContact = nextTime;
    }
  }

  return everIntersected;
}

std::vector<int> CollisionDetectionPostCrash::GetVertexTypes(
    const std::vector<Common::Vector2d<units::length::meter_t>> &vertices1,
    std::vector<Common::Vector2d<units::length::meter_t>> vertices2,
    std::vector<Common::Vector2d<units::length::meter_t>> verticesIntersection)
{
  std::vector<int> vertexTypes;

  if (verticesIntersection.empty())
  {
    return vertexTypes;
  }

  size_t numberOfType2 = verticesIntersection.size();
  for (unsigned int iIntersection = 0; iIntersection < verticesIntersection.size(); ++iIntersection)
  {
    vertexTypes.push_back(2);

    for (const auto &iPoly1 : vertices1)
    {
      if ((verticesIntersection[iIntersection] - iPoly1).Length() < 1E-12_m)
      {
        vertexTypes[iIntersection] = 1;
        numberOfType2--;
        continue;
      }
    }
    for (unsigned int iPoly2 = 0; iPoly2 < vertices1.size(); ++iPoly2)
    {
      if ((verticesIntersection[iIntersection] - vertices2[iPoly2]).Length() < 1E-12_m)
      {
        vertexTypes[iIntersection] = 1;
        numberOfType2--;
        continue;
      }
    }
  }
  if (numberOfType2 == 0)
  {
    // special case: perfectly straight impact
    for (unsigned int iIntersection = 0; iIntersection < verticesIntersection.size(); ++iIntersection)
    {
      for (const auto &iPoly1 : vertices1)
      {
        if ((verticesIntersection[iIntersection] - iPoly1).Length() < 1E-12_m)
        {
          vertexTypes[iIntersection] = 3;
        }
      }
    }
  }
  return vertexTypes;
}

Common::Vector2d<units::velocity::meters_per_second_t> CollisionDetectionPostCrash::GetAgentVelocityVector(
    const AgentInterface *agent)
{
  return agent->GetVelocity();
}

bool CollisionDetectionPostCrash::ShiftPolygonsAndCheckIntersection(
    Polygon polygon1,
    Polygon polygon2,
    Common::Vector2d<units::length::meter_t> shiftVector1,
    Common::Vector2d<units::length::meter_t> shiftVector2)
{
  polygon1.Translate(shiftVector1);
  polygon2.Translate(shiftVector2);

  std::vector<Common::Vector2d<units::length::meter_t>> intersectionPoints
      = CalculateAllIntersectionPoints(polygon1.GetVertices(), polygon2.GetVertices());

  return !intersectionPoints.empty();
}

void CollisionDetectionPostCrash::CalculateCollisionAngles(const AgentInterface *agent1,
                                                           const AgentInterface *agent2,
                                                           int timeShift)
{
  std::vector<Common::Vector2d<units::length::meter_t>> agentCorners1 = GetAgentCorners(agent1);
  std::vector<Common::Vector2d<units::length::meter_t>> agentCorners2 = GetAgentCorners(agent2);
  Polygon polygon1(agentCorners1);
  Polygon polygon2(agentCorners2);

  const auto time = units::make_unit<units::time::millisecond_t>(timeShift);

  Common::Vector2d<units::length::meter_t> translationVector1{GetAgentVelocityVector(agent1).x * time,
                                                              GetAgentVelocityVector(agent1).y * time};
  Common::Vector2d<units::length::meter_t> translationVector2{GetAgentVelocityVector(agent2).x * time,
                                                              GetAgentVelocityVector(agent2).y * time};

  polygon1.Translate(translationVector1);
  polygon2.Translate(translationVector2);

  Common::Vector2d<units::length::meter_t> centroid1;
  Common::Vector2d<units::length::meter_t> centroid2;

  polygon1.CalculateCentroid(centroid1);
  polygon2.CalculateCentroid(centroid2);

  std::vector<Common::Vector2d<units::length::meter_t>> intersectionPoints
      = CalculateAllIntersectionPoints(polygon1.GetVertices(), polygon2.GetVertices());
  Polygon intersectionPolygon(intersectionPoints);

  Common::Vector2d<units::length::meter_t> firstPointOfContact;
  intersectionPolygon.CalculateCentroid(firstPointOfContact);

  const auto hostYaw = agent1->GetYaw();
  const auto oppYaw = agent2->GetYaw();
  units::angle::radian_t oya = (oppYaw - hostYaw) * 180 / M_PI;

  Common::Vector2d<units::length::meter_t> opponentCenter
      = firstPointOfContact - centroid2;  // opponent center to first point of contact
  opponentCenter.Rotate(-oppYaw);
  units::angle::radian_t ocpaOrig = opponentCenter.Angle() * 180 / M_PI;
  units::angle::radian_t ocpaTrans
      = units::math::atan2(opponentCenter.y / agent2->GetWidth(), opponentCenter.x / agent2->GetLength()) * 180 / M_PI;

  Common::Vector2d<units::length::meter_t> hostCenter
      = firstPointOfContact - centroid1;  // host center to first point of contact
  hostCenter.Rotate(-hostYaw);
  units::angle::radian_t hcpaOrig = hostCenter.Angle() * 180 / M_PI;
  units::angle::radian_t hcpaTrans
      = units::math::atan2(hostCenter.y / agent1->GetWidth(), hostCenter.x / agent1->GetLength()) * 180 / M_PI;

  SetCollisionAngles(oya, hcpaOrig, ocpaOrig, hcpaTrans, ocpaTrans);
}

bool CollisionDetectionPostCrash::GetCollisionPosition(const AgentInterface *agent1,
                                                       const AgentInterface *agent2,
                                                       Common::Vector2d<units::length::meter_t> &cog1,
                                                       Common::Vector2d<units::length::meter_t> &cog2,
                                                       Common::Vector2d<units::length::meter_t> &pointOfImpact,
                                                       units::angle::radian_t &phi,
                                                       int timeShift)
{
  std::vector<Common::Vector2d<units::length::meter_t>> agentCorners1 = GetAgentCorners(agent1);
  std::vector<Common::Vector2d<units::length::meter_t>> agentCorners2 = GetAgentCorners(agent2);
  Polygon agent1Polygon(agentCorners1);
  Polygon agent2Polygon(agentCorners2);

  const auto time = units::make_unit<units::time::millisecond_t>(timeShift);

  Common::Vector2d<units::length::meter_t> translationVector1{GetAgentVelocityVector(agent1).x * time,
                                                              GetAgentVelocityVector(agent1).y * time};
  Common::Vector2d<units::length::meter_t> translationVector2{GetAgentVelocityVector(agent2).x * time,
                                                              GetAgentVelocityVector(agent2).y * time};

  agent1Polygon.Translate(translationVector1);
  agent2Polygon.Translate(translationVector2);

  agent1Polygon.CalculateCentroid(cog1);
  agent2Polygon.CalculateCentroid(cog2);

  std::vector<Common::Vector2d<units::length::meter_t>> intersectionPoints
      = CalculateAllIntersectionPoints(agent1Polygon.GetVertices(), agent2Polygon.GetVertices());
  Polygon intersectionPolygon(intersectionPoints);

  std::vector<int> vertexTypes
      = GetVertexTypes(agent1Polygon.GetVertices(), agent2Polygon.GetVertices(), intersectionPoints);

  CalculatePlaneOfContact(intersectionPolygon, vertexTypes, pointOfImpact, phi);

  return true;
}

bool CollisionDetectionPostCrash::CreatePostCrashDynamics(const AgentInterface *agent1,
                                                          const AgentInterface *agent2,
                                                          PostCrashDynamic *postCrashDynamic1,
                                                          PostCrashDynamic *postCrashDynamic2,
                                                          int &timeOfFirstContact)
{
  timeOfFirstContact = 0;
  if (!GetFirstContact(agent1, agent2, timeOfFirstContact))
  {
    return false;
  }
  //
  CalculateCollisionAngles(agent1, agent2, timeOfFirstContact);

  Common::Vector2d<units::length::meter_t> resultAgent1COG = Common::Vector2d<units::length::meter_t>(-1_m, -1_m);
  Common::Vector2d<units::length::meter_t> resultAgent2COG = Common::Vector2d<units::length::meter_t>(-1_m, -1_m);
  Common::Vector2d<units::length::meter_t> pointOfImpact;
  units::angle::radian_t phi;

  // position of agents after penetrationTime given by algorithm
  if (!GetCollisionPosition(
          agent1, agent2, resultAgent1COG, resultAgent2COG, pointOfImpact, phi, timeOfFirstContact + penetrationTime))
  {
    LOG(CbkLogLevel::Error, "Could not get collision position parameters.");
    return false;
  }

  if (!CalculatePostCrashDynamic(
          resultAgent1COG, agent1, resultAgent2COG, agent2, pointOfImpact, phi, postCrashDynamic1, postCrashDynamic2))
  {
    LOG(CbkLogLevel::Error, "Could not calculate post crash dynamic");
    return false;
  }
  return true;
}

CollisionAngles CollisionDetectionPostCrash::GetCollisionAngles()
{
  return collAngles;
}

void CollisionDetectionPostCrash::SetCollisionAngles(units::angle::radian_t opponentYawAngle,
                                                     units::angle::radian_t hostCollisionPointAngleOriginal,
                                                     units::angle::radian_t opponentCollisionPointAngleOriginal,
                                                     units::angle::radian_t hostCollisionPointAngle,
                                                     units::angle::radian_t opponentCollisionPointAngle)
{
  this->collAngles.OYA = opponentYawAngle;
  this->collAngles.HCPAo = hostCollisionPointAngleOriginal;
  this->collAngles.OCPAo = opponentCollisionPointAngleOriginal;
  this->collAngles.HCPA = hostCollisionPointAngle;
  this->collAngles.OCPA = opponentCollisionPointAngle;
}
