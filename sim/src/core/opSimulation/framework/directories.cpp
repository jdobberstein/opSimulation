/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "directories.h"

#include <algorithm>
#include <chrono>
#include <filesystem>
#include <string>

namespace openpass::core
{

Directories::Directories(const std::string& applicationDir,
                         const std::string& libraryDir,
                         const std::string& configurationDir,
                         const std::string& outputDir)
    : baseDir{Directories::Resolve(applicationDir, ".")},
      configurationDir{Directories::Resolve(applicationDir, configurationDir)},
      libraryDir{Directories::Resolve(applicationDir, libraryDir)},
      outputDir{Directories::Resolve(applicationDir, outputDir)}
{
}

namespace fs = std::filesystem;

const std::string Directories::Resolve(const std::string& applicationPath, const std::string& path)
{
  const auto absolutePath = fs::path(path);
  return absolutePath.is_absolute() ? absolutePath.string() : (fs::path(applicationPath) / absolutePath).string();
}

const std::string Directories::Concat(const std::string& path, const std::string& file)
{
  return (fs::path(path) / fs::path(file)).string();
}

const std::vector<std::string> Directories::Concat(const std::string& path, const std::vector<std::string>& filenames)
{
  std::vector<std::string> result{};

  for (const auto& filename : filenames)
  {
    auto fullPath = Directories::Concat(path, filename);
    result.push_back(fullPath);
  }

  return result;
}

const std::string Directories::StripFile(const std::string& path)
{
  fs::path fileInfo = fs::path(path);
  return fileInfo.parent_path().string();
}

bool Directories::IsRelative(const std::string& path)
{
  fs::path fileInfo = fs::path(path);
  return fileInfo.is_relative();
}

}  // namespace openpass::core
