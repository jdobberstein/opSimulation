/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "runInstantiator.h"

#include <MantleAPI/Execution/i_scenario_engine.h>
#include <MantleAPI/Execution/scenario_info.h>
#include <OpenScenarioEngine/OpenScenarioEngine.h>
#include <algorithm>
#include <chrono>
#include <cstdint>
#include <exception>
#include <filesystem>
#include <map>
#include <optional>
#include <sstream>
#include <unordered_map>
#include <utility>
#include <vector>

#include "common/commonTools.h"
#include "common/log.h"
#include "common/parameter.h"
#include "common/sampler.h"
#include "framework/parameterbuilder.h"
#include "importer/entityPropertiesImporter.h"
#include "include/agentFactoryInterface.h"
#include "include/dataBufferInterface.h"
#include "include/observationNetworkInterface.h"
#include "include/spawnPointNetworkInterface.h"
#include "include/stochasticsInterface.h"
#include "include/worldInterface.h"
#include "modelElements/parameters.h"
#include "scheduler/runResult.h"
#include "scheduler/scheduler.h"

namespace fs = std::filesystem;

const std::string SCENERYPATH = "full_map_path";
const std::string VEHICLEMODELSFILENAME = "VehicleModelsCatalog.xosc";

constexpr const char *SPAWNER{"Spawner"};

namespace core
{

bool RunInstantiator::ExecuteRun()
{
  LOG_INTERN(LogLevel::DebugCore) << std::endl << "### execute run ###";

  stopMutex.lock();
  stopped = false;
  stopMutex.unlock();

  const auto &simulationConfig = *configurationContainer.GetSimulationConfig();
  const auto &profiles = *configurationContainer.GetProfiles();
  const auto &experimentConfig = simulationConfig.GetExperimentConfig();
  const auto &environmentConfig = simulationConfig.GetEnvironmentConfig();

  ThrowIfFalse(dataBuffer.Instantiate(), "Failed to instantiate DataBuffer");

  ThrowIfFalse(stochastics.Instantiate(frameworkModules.stochasticsLibrary), "Failed to instantiate Stochastics");

  agentBlueprintProvider.Init(&configurationContainer, &stochastics);

  OpenScenarioEngine::v1_2::OpenScenarioEngine scenarioEngine{
      configurationContainer.GetSimulationConfig()->GetScenarioConfig().scenarioPath, environment};

  scenarioEngine.Init();

  const fs::path configurationDir = configurationContainer.GetRuntimeInformation().directories.configuration;
  const auto &relativeSceneryPath = scenarioEngine.GetScenarioInfo().full_map_path;
  const fs::path vehicleCatalogFilepath
      = configurationDir / fs::path(scenarioEngine.GetScenarioInfo().additional_information.at("vehicle_catalog_path"))
      / fs::path(VEHICLEMODELSFILENAME);

  environment->CreateMap(relativeSceneryPath, *scenarioEngine.GetScenarioInfo().map_details);

  Importer::EntityPropertiesImporter entityPropertiesImporter;
  entityPropertiesImporter.Import(vehicleCatalogFilepath.string());
  vehicles = entityPropertiesImporter.GetVehicleProperties();

  if (!InitPreRun())
  {
    LOG_INTERN(LogLevel::DebugCore) << std::endl << "### initialization failed ###";
    return false;
  }

  dataBuffer.PutStatic("SceneryFile", relativeSceneryPath, true);
  ThrowIfFalse(observationNetwork.InitAll(), "Failed to initialize ObservationNetwork");

  core::scheduling::Scheduler scheduler(
      world, spawnPointNetwork, collisionDetection, observationNetwork, dataBuffer, *environment);
  bool scheduler_state{false};

  setTotalRuns(experimentConfig.numberOfInvocations);

  for (auto invocation = 0; invocation < experimentConfig.numberOfInvocations; invocation++)
  {
    RunResult runResult;
    setCurrentRun(invocation);

    LOG_INTERN(LogLevel::DebugCore) << std::endl << "### run number: " << invocation << " ###";
    stochastics.InitGenerator(static_cast<std::uint32_t>(experimentConfig.randomSeed + invocation));
    if (!InitRun(environmentConfig, profiles))
    {
      LOG_INTERN(LogLevel::DebugCore) << std::endl << "### run initialization failed ###";
      break;
    }

    LOG_INTERN(LogLevel::DebugCore) << std::endl << "### run started ###";
    scheduler_state = scheduler.Run(0, runResult, &scenarioEngine);

    if (scheduler_state == core::scheduling::Scheduler::FAILURE)
    {
      LOG_INTERN(LogLevel::DebugCore) << std::endl << "### run aborted ###";
      break;
    }
    LOG_INTERN(LogLevel::DebugCore) << std::endl << "### run successful ###";

    observationNetwork.FinalizeRun(runResult);
    ClearRun();
  }

  LOG_INTERN(LogLevel::DebugCore) << std::endl << "### end of all runs ###";
  bool observations_state = observationNetwork.FinalizeAll();

  return (scheduler_state && observations_state);
}

bool RunInstantiator::InitPreRun()
{
  try
  {
    ThrowIfFalse(
        observationNetwork.Instantiate(frameworkModules.observationLibraries, &stochastics, &world, &dataBuffer),
        "Failed to instantiate ObservationNetwork");

    return true;
  }
  catch (const std::exception &error)
  {
    LOG_INTERN(LogLevel::Error) << std::endl << "### could not init: " << error.what() << "###";
    return false;
  }

  LOG_INTERN(LogLevel::Error) << std::endl << "### exception caught, which is not of type std::exception! ###";
  return false;
}

void RunInstantiator::InitializeSpawnPointNetwork()
{
  const auto &profileGroups = configurationContainer.GetProfiles()->GetProfileGroups();
  bool existingSpawnProfiles = profileGroups.find(SPAWNER) != profileGroups.end();

  ThrowIfFalse(spawnPointNetwork.Instantiate(
                   frameworkModules.spawnPointLibraries,
                   &agentFactory,
                   &stochastics,
                   existingSpawnProfiles ? std::make_optional(profileGroups.at(SPAWNER)) : std::nullopt,
                   &configurationContainer,
                   environment,
                   vehicles),
               "Failed to instantiate SpawnPointNetwork");
}

std::unique_ptr<ParameterInterface> RunInstantiator::SampleWorldParameters(
    const EnvironmentConfig &environmentConfig,
    const ProfileGroup &trafficRules,
    StochasticsInterface *stochastics,
    const openpass::common::RuntimeInformation &runtimeInformation)
{
  auto trafficRule = helper::map::query(trafficRules, environmentConfig.trafficRules);
  ThrowIfFalse(trafficRule.has_value(),
               "No traffic rule set with name " + environmentConfig.trafficRules + " defined in ProfilesCatalog");
  auto parameters = trafficRule.value();
  parameters.emplace_back("TimeOfDay", Sampler::Sample(environmentConfig.timeOfDays, stochastics));
  parameters.emplace_back("VisibilityDistance", Sampler::Sample(environmentConfig.visibilityDistances, stochastics));
  parameters.emplace_back("Friction", Sampler::Sample(environmentConfig.frictions, stochastics));
  parameters.emplace_back("Weather", Sampler::Sample(environmentConfig.weathers, stochastics));

  return openpass::parameter::make<SimulationCommon::Parameters>(runtimeInformation, parameters);
}
bool RunInstantiator::InitRun(const EnvironmentConfig &environmentConfig, const ProfilesInterface &profiles)
{
  try
  {
    auto trafficRules = helper::map::query(profiles.GetProfileGroups(), "TrafficRules");
    ThrowIfFalse(trafficRules.has_value(), "No traffic rules defined in ProfilesCatalog");
    worldParameter = SampleWorldParameters(
        environmentConfig, trafficRules.value(), &stochastics, configurationContainer.GetRuntimeInformation());
    world.ExtractParameter(worldParameter.get());

    observationNetwork.InitRun();
    InitializeSpawnPointNetwork();

    return true;
  }
  catch (const std::exception &error)
  {
    LOG_INTERN(LogLevel::Error) << std::endl << "### could not init run: " << error.what() << "###";
    return false;
  }

  LOG_INTERN(LogLevel::Error) << std::endl << "### exception caught, which is not of type std::exception! ###";
  return false;
}

void RunInstantiator::ClearRun()
{
  agentFactory.Clear();
  spawnPointNetwork.Clear();
  dataBuffer.ClearRun();
}

}  // namespace core