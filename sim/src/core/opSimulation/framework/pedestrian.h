/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <memory>
#include <string>

#include "framework/entity.h"

class RouteSamplerInterface;

namespace core
{

//! This class represents a pedestrian in a scenario
class Pedestrian : public Entity, public mantle_api::IPedestrian
{
public:
  //! Pedestrian constructor
  //!
  //! @param[in] id           Unique id
  //! @param[in] name         Name of the pedestrian
  //! @param[in] properties   Additional properties that describe scenario pedestrian
  //! @param[in] routeSampler Route sampler
  Pedestrian(mantle_api::UniqueId id,
             const std::string &name,
             const std::shared_ptr<mantle_api::PedestrianProperties> &properties,
             const RouteSamplerInterface *routeSampler);

  void SetProperties(std::unique_ptr<mantle_api::EntityProperties> properties) override;
  mantle_api::PedestrianProperties *GetProperties() const override;

private:
  std::shared_ptr<mantle_api::PedestrianProperties> properties;
};
}  // namespace core
