/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Traffic/i_controller.h>
#include <MantleAPI/Traffic/i_controller_repository.h>
#include <functional>
#include <map>
#include <memory>
#include <optional>
#include <stdexcept>

#include "common/opExport.h"
#include "include/idManagerInterface.h"

class AgentBlueprintProvider;
namespace mantle_api
{
struct IControllerConfig;
}  // namespace mantle_api

namespace core
{
class Controller;

//! This class provides CRUD functionality for controllers
class SIMULATIONCOREEXPORT ControllerRepository final : public mantle_api::IControllerRepository
{
public:
  //! ControllerRepository constructor
  //!
  //! @param[in] idManager                References the idManager that handles unique IDs
  //! @param[in] agentBlueprintProvider   References the agent blueprint provider
  ControllerRepository(IdManagerInterface &idManager, const AgentBlueprintProvider &agentBlueprintProvider)
      : idManager(idManager), agentBlueprintProvider(agentBlueprintProvider)
  {
  }

  mantle_api::IController &Create(std::unique_ptr<mantle_api::IControllerConfig> config) override final;
  mantle_api::IController &Create(mantle_api::UniqueId id,
                                  std::unique_ptr<mantle_api::IControllerConfig> config) override final
  {
    throw std::runtime_error("ControllerRepository: Use of unsupported deprecated function \"Create(id, config)\"");
  }

  std::optional<std::reference_wrapper<mantle_api::IController>> Get(mantle_api::UniqueId id) override final;
  bool Contains(mantle_api::UniqueId id) const override final;

  void Delete(mantle_api::UniqueId id) override final;

  void Reset() override;

  std::map<mantle_api::UniqueId, std::shared_ptr<Controller>>
      controllers;  //!< The map of all controllers with their UniqueId

private:
  IdManagerInterface &idManager;  //!< References the idManager that handles unique IDs
  std::reference_wrapper<const AgentBlueprintProvider> agentBlueprintProvider;
};
}  // namespace core
