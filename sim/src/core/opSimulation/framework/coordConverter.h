/*******************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Map/i_coord_converter.h>

#include "include/callbackInterface.h"
#include "include/worldInterface.h"

using namespace units::literals;

namespace core
{

//! Thic class provides functionality to query the underlying map with regard to transformation of different position
//! types, curvature, elevation etc.
class SIMULATIONCOREEXPORT CoordConverter final : public mantle_api::ICoordConverter
{
public:
  CoordConverter(const CallbackInterface *callbacks) : callbacks(callbacks) {}

  ~CoordConverter() = default;

  //! @brief A method that initializes the world
  //!
  //! @param[in] world    Pointer to the world
  void Init(WorldInterface *world) { this->world = world; }

  mantle_api::Vec3<units::length::meter_t> Convert(mantle_api::Position position) const final
  {
    if (std::holds_alternative<mantle_api::OpenDriveLanePosition>(position))
    {
      const auto openDrivePosition = std::get<mantle_api::OpenDriveLanePosition>(position);
      const auto worldPosition = world->LaneCoord2WorldCoord(
          openDrivePosition.s_offset, openDrivePosition.t_offset, openDrivePosition.road, openDrivePosition.lane);
      THROWIFFALSE(worldPosition.has_value(),
                   "Cannot convert LaneCoordinates, No lane " + std::to_string(openDrivePosition.lane) + " for Road \""
                       + openDrivePosition.road + "\" and s=" + std::to_string(openDrivePosition.s_offset.value()));
      return {worldPosition->xPos, worldPosition->yPos, 0.0_m};
    }
    if (std::holds_alternative<mantle_api::OpenDriveRoadPosition>(position))
    {
      const auto openDrivePosition = std::get<mantle_api::OpenDriveRoadPosition>(position);
      const auto worldPosition = world->RoadCoord2WorldCoord(
          {openDrivePosition.s_offset, openDrivePosition.t_offset, 0_rad}, openDrivePosition.road);
      THROWIFFALSE(worldPosition.has_value(),
                   "Cannot convert RoadCoordinates, No lane for Road \"" + openDrivePosition.road
                       + "\" and s=" + std::to_string(openDrivePosition.s_offset.value())
                       + " t=" + std::to_string(openDrivePosition.t_offset.value()));
      return {worldPosition->xPos, worldPosition->yPos, 0.0_m};
    }

    return {};
  }

private:
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
  {
    if (callbacks)
    {
      callbacks->Log(logLevel, file, line, message);
    }
  }

  WorldInterface *world;

  const CallbackInterface *callbacks;
};
}  //namespace core
