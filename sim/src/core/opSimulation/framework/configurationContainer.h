/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2018-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  configurationContainer.h
 *   \brief This file stores all configurations
 */
//-----------------------------------------------------------------------------

#pragma once

#include <filesystem>
#include <map>
#include <memory>
#include <string>

#include "common/opExport.h"
#include "common/runtimeInformation.h"
#include "importer/configurationFiles.h"
#include "importer/profiles.h"
#include "importer/simulationConfig.h"
#include "include/configurationContainerInterface.h"

class ProfilesInterface;
class SimulationConfigInterface;
class SystemConfigInterface;

namespace Configuration
{
class SystemConfig;

//-----------------------------------------------------------------------------
/** \brief This class imports and stores all configurations
 *   \details
 */
//-----------------------------------------------------------------------------
class SIMULATIONCOREEXPORT ConfigurationContainer : public ConfigurationContainerInterface
{
public:
  /**
   * @brief ConfigurationContainer constructor
   *
   * @param[in] configurationFiles    Configuration files
   * @param[in] runtimeInformation    Common runtimeInformation
   */
  ConfigurationContainer(const ConfigurationFiles &configurationFiles,
                         const openpass::common::RuntimeInformation &runtimeInformation)
      : configurationFiles{configurationFiles}, runtimeInformation(runtimeInformation)
  {
  }

  ~ConfigurationContainer() override = default;

  /*!
   * \brief This functions imports all confiugrations
   *
   * \details This function imports all configurations and stores them.
   *
   * @param executionPath Target of the /proc/self/exe symlink on Linux, canonical path of executable on Windows
   * @return        true, if successful
   */
  bool ImportAllConfigurations(const std::filesystem::path &executionPath) override;

  /*!
   * \brief Returns a pointer to the SystemConfigBlueprint
   *
   * @return        SystemConfigBlueprint pointer
   */
  std::shared_ptr<SystemConfigInterface> GetSystemConfigBlueprint() const override;

  /*!
   * \brief Returns a pointer to the SimulationConfig
   *
   * @return        SimulationConfig pointer
   */
  const SimulationConfigInterface *GetSimulationConfig() const override;

  /*!
   * \brief Returns a pointer to the Profiles
   *
   * @return        Profiles pointer
   */
  const ProfilesInterface *GetProfiles() const override;

  /*!
   * \brief Returns imported systemConfigs
   *
   * @return        systemConfigs map
   */
  const std::map<std::string, std::shared_ptr<SystemConfigInterface>> &GetSystemConfigs() const override;

  /*!
   * \brief Returns the RunTimeInformation
   *
   * @return        RunTimeInformation
   */
  const openpass::common::RuntimeInformation &GetRuntimeInformation() const override;

private:
  const ConfigurationFiles &configurationFiles;

  std::shared_ptr<SystemConfig> systemConfigBlueprint;
  SimulationConfig simulationConfig;
  std::map<std::string, std::shared_ptr<SystemConfigInterface>> systemConfigs;
  Profiles profiles;
  openpass::common::RuntimeInformation runtimeInformation;
};

}  //namespace Configuration
