/********************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  collisionDetector.h
 *	@brief Detects whether agents collided.
 *
 *   This manipulator detects whether agents collided with either other agents or traffic objects. */
//-----------------------------------------------------------------------------

#pragma once

#include <vector>

#include "common/globalDefinitions.h"
#include "common/opExport.h"
#include "common/postCrashDynamic.h"
#include "include/collisionDetectionInterface.h"
#include "srcCollisionPostCrash/collisionDetection_Impact_implementation.h"

class AgentInterface;
class WorldObjectInterface;
class PublisherInterface;
class RunResultInterface;
class WorldInterface;

/// Representing different corner types
typedef enum
{
  UpperLeft = 0,
  UpperRight,
  LowerRight,
  LowerLeft,
  NumberCorners
} CornerType;

/// Representing different edge types
typedef enum
{
  UpperEdge = 0,
  RightEdge,
  LowerEdge,
  LeftEdge,
  NumberEdges
} EdgeType;

/// Representing different normal types
typedef enum
{
  Right = 0,
  Up,
  NumberNormals
} NormalType;

/// Representing different projected types
typedef enum
{
  ProjectedFirst = 0,
  ProjectedSecond,
  NumberProjectedOwnAxis,
  ProjectedThird = 2,
  ProjectedFourth,
  NumberProjectedOpponentAxis
} ProjectedType;

/// Info about who if and how collided
struct CollisionInfo
{
  /// true if agent to agent collision, false if agent to object collision
  bool collisionWithAgent;
  /// primary collision partner (always an agent)
  int collisionAgentId;
  /// secondary collision partner (could be also static object)
  int collisionOpponentId;
};

/// comparision epsilon for rotations
static inline constexpr double ROTATION_EPS{0.0001};

/// @brief This class detectes whether a collision happen in the simulation.
/// @details This class detects whether an agent collided with either another agent or a traffic object.
class SIMULATIONCOREEXPORT CollisionDetector : public core::CollisionDetectionInterface
{
public:
  /**
   * @brief CollisionDetector constructor
   *
   * @param[in]   world                 Pointer to the world
   * @param[in]   publisher             Pointer to the publisher
   */
  CollisionDetector(WorldInterface *world, PublisherInterface *publisher);

  ~CollisionDetector() override = default;
  void Trigger(int time) override;
  void Initialize(RunResultInterface *runResult) override;

private:
  /// Crash information
  struct CrashInfo
  {
    /// collision angles
    CollisionAngles angles;
    /// info about primary partner
    PostCrashDynamic postCrashDynamic1;
    /// info about secondary partner
    PostCrashDynamic postCrashDynamic2;
  };

  /// @brief Check For Collision between worldObjects
  ///
  /// @param  other   pointer to worldObject
  /// @param  agent   pointer to agent
  /// @return true when collision detected
  bool DetectCollision(const WorldObjectInterface *other, AgentInterface *agent);

  /// @brief Creates a CollisionInfo and inserts it into the collision list
  ///
  /// @param  agent   pointer to agent
  /// @param  other   pointer to other world object
  void DetectedCollisionWithObject(AgentInterface *agent, const WorldObjectInterface *other);

  /// @brief Creates a CollisionInfo and inserts it into the collision list
  ///
  /// @param  agent   pointer to agent
  /// @param  other   pointer to other agent
  void DetectedCollisionWithAgent(AgentInterface *agent, AgentInterface *other);

  /// @brief This function is responsible for updating, adding and publishing collisions
  virtual void UpdateCollisions();

  /// @brief This function updates the collisionPartners of the agent, the opponent and recursively of all existing
  /// collisionPartners of both.
  void UpdateCollision(AgentInterface *agent, AgentInterface *opponent);

  /// Calculates the crash info for two collision partners
  /// @param agent     primary partner
  /// @param opponent  secondary partner
  CrashInfo CalculateCrash(AgentInterface *agent, AgentInterface *opponent);

  /// Publish information about the crash (e.g. for simulation output)
  /// @param collisionInfo     collision info
  /// @param crashInfo         crash info
  void PublishCrash(const CollisionInfo &collisionInfo, const CrashInfo &crashInfo);

  /// Publish information about the crash (e.g. for simulation output)
  /// @param collisionInfo     collision info
  void PublishCrash(const CollisionInfo &collisionInfo);

  /// Adds a collision id to the RunResult
  /// @param     agentId     Id of the collision agent
  void AddCollision(const int agentId);

  CollisionDetectionPostCrash collisionPostCrash;
  PublisherInterface *publisher;
  WorldInterface *world = nullptr;
  RunResultInterface *runResult{nullptr};
  std::vector<CollisionInfo> collisions;
};
