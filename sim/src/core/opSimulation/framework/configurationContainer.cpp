/********************************************************************************
 * Copyright (c) 2021 ITK Engineering GmbH
 *               2018-2019 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  configurationContainer.cpp */
//-----------------------------------------------------------------------------

#include "configurationContainer.h"

#include <algorithm>
#include <ostream>
#include <set>
#include <type_traits>
#include <unordered_map>
#include <utility>

#include "common/log.h"
#include "framework/directories.h"
#include "importer/profilesImporter.h"
#include "importer/simulationConfigImporter.h"
#include "importer/systemConfig.h"
#include "importer/systemConfigImporter.h"

class ProfilesInterface;
class SimulationConfigInterface;
class SystemConfigInterface;

using namespace Importer;

bool ConfigurationContainer::ImportAllConfigurations(const std::filesystem::path &executionPath)
{
  //Import SystemConfigBlueprint
  systemConfigBlueprint = std::make_shared<SystemConfig>();
  if (!SystemConfigImporter::Import(configurationFiles.systemConfigBlueprintFile, systemConfigBlueprint, executionPath))
  {
    LOG_INTERN(LogLevel::Info) << "could not import systemConfigBlueprint.";
    systemConfigBlueprint = nullptr;
  }

  //Import SimulationConfig
  if (!SimulationConfigImporter::Import(configurationFiles.configurationDir,
                                        configurationFiles.simulationConfigFile,
                                        simulationConfig,
                                        executionPath))
  {
    LOG_INTERN(LogLevel::Error) << "could not import simulation configuration '"
                                << configurationFiles.simulationConfigFile << "'";
    return false;
  }

  //Import ProfilesCatalog
  if (!ProfilesImporter::Import(simulationConfig.GetProfilesCatalog(), profiles, executionPath))
  {
    LOG_INTERN(LogLevel::Error) << "could not import profiles catalog";
    return false;
  }

  std::set<std::string> systemConfigFiles;
  std::transform(profiles.GetAgentProfiles().begin(),
                 profiles.GetAgentProfiles().end(),
                 std::inserter(systemConfigFiles, systemConfigFiles.begin()),
                 [](const auto &agentProfile) { return agentProfile.second.systemConfigFile; });

  //Import SystemConfigs
  for (const auto &systemConfigFile : systemConfigFiles)
  {
    if (systemConfigFile == "")
    {
      continue;
    }
    auto systemConfig = std::make_shared<SystemConfig>();

    if (!SystemConfigImporter::Import(
            openpass::core::Directories::Concat(configurationFiles.configurationDir, systemConfigFile),
            systemConfig,
            executionPath))
    {
      LOG_INTERN(LogLevel::Error) << "could not import system configs";
      return false;
    }
    systemConfigs.insert(std::make_pair(systemConfigFile, systemConfig));
  }

  return true;
}

std::shared_ptr<SystemConfigInterface> ConfigurationContainer::GetSystemConfigBlueprint() const
{
  return systemConfigBlueprint;
}

const SimulationConfigInterface *ConfigurationContainer::GetSimulationConfig() const
{
  return &simulationConfig;
}

const ProfilesInterface *ConfigurationContainer::GetProfiles() const
{
  return &profiles;
}

const std::map<std::string, std::shared_ptr<SystemConfigInterface>> &ConfigurationContainer::GetSystemConfigs() const
{
  return systemConfigs;
}

const openpass::common::RuntimeInformation &ConfigurationContainer::GetRuntimeInformation() const
{
  return runtimeInformation;
}
