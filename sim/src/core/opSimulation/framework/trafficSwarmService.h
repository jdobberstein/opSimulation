/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Traffic/i_traffic_swarm_service.h>

#include "common/log.h"

namespace core
{

//! Thic class provides functionality to query the underlying map with regard to transformation of different position
//! types, curvature, elevation etc.
class SIMULATIONCOREEXPORT TrafficSwarmService final : public mantle_api::ITrafficSwarmService
{
public:
  std::vector<SpawningPosition> GetAvailableSpawningPoses() const override
  {
    LogErrorAndThrow("Method GetAvailableSpawningPoses in TrafficSwarmService is not yet implemented");
  }
  mantle_api::VehicleProperties GetVehicleProperties(mantle_api::VehicleClass vehicle_class) const override
  {
    LogErrorAndThrow("Method GetVehicleProperties in TrafficSwarmService is not yet implemented");
  }
  void UpdateControllerConfig(std::unique_ptr<mantle_api::ExternalControllerConfig>& config,
                              units::velocity::meters_per_second_t speed) override
  {
    LogErrorAndThrow("Method UpdateControllerParameters in TrafficSwarmService is not yet implemented");
  }
  void SetSwarmEntitiesCount(size_t count) override
  {
    LogErrorAndThrow("Method SetSwarmEntitiesCount in TrafficSwarmService is not yet implemented");
  }

private:
  std::vector<mantle_api::Pose> pose{};
  mantle_api::VehicleProperties vehicleProperties;
};
}  //namespace core
