/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "spawnPointNetwork.h"

#include <algorithm>
#include <ostream>
#include <stdexcept>
#include <unordered_map>
#include <utility>
#include <vector>

#include "bindings/spawnPointBinding.h"
#include "common/log.h"
#include "common/parameter.h"
#include "framework/parameterbuilder.h"
#include "include/parameterInterface.h"
#include "modelElements/parameters.h"
#include "modelElements/spawnPoint.h"

class ConfigurationContainerInterface;
class StochasticsInterface;
class WorldInterface;

namespace mantle_api
{
class IEnvironment;
}  // namespace mantle_api

namespace openpass::common
{
struct RuntimeInformation;
}  // namespace openpass::common

namespace core
{
class AgentFactoryInterface;

SpawnPointNetwork::SpawnPointNetwork(std::map<std::string, SpawnPointBinding>* spawnPointBindings,
                                     WorldInterface* world,
                                     const openpass::common::RuntimeInformation& runtimeInformation)
    : spawnPointBindings{spawnPointBindings}, world{world}, runtimeInformation{runtimeInformation}
{
}

void SpawnPointNetwork::Clear()
{
  preRunSpawnZones.clear();
  runtimeSpawnPoints.clear();
}

bool SpawnPointNetwork::Instantiate(const SpawnPointLibraryInfoCollection& libraryInfos,
                                    AgentFactoryInterface* agentFactory,
                                    StochasticsInterface* stochastics,
                                    const std::optional<ProfileGroup>& spawnPointProfiles,
                                    ConfigurationContainerInterface* configurationContainer,
                                    std::shared_ptr<mantle_api::IEnvironment> environment,
                                    std::shared_ptr<Vehicles> vehicles)
{
  agentBlueprintProvider.Init(configurationContainer, stochastics);

  for (const auto& libraryInfo : libraryInfos)
  {
    const auto bindingIter = spawnPointBindings->find(libraryInfo.libraryName);
    if (bindingIter == spawnPointBindings->end())
    {
      return false;
    }

    auto* binding = &bindingIter->second;
    SpawnPointDependencies dependencies(
        agentFactory, world, &agentBlueprintProvider, stochastics, environment, vehicles);

    if (libraryInfo.profileName.has_value())
    {
      ThrowIfFalse(spawnPointProfiles.has_value(), "No Spawner ProfileGroup defined in ProfilesCatalog");
      try
      {
        auto parameter = openpass::parameter::make<SimulationCommon::Parameters>(
            runtimeInformation, spawnPointProfiles.value().at(libraryInfo.profileName.value()));

        dependencies.parameters = parameter.get();    // get spawnpoint access to the parameters
        binding->SetParameter(std::move(parameter));  // the transfer ownership away from network
      }
      catch (const std::out_of_range&)
      {
        LOG_INTERN(LogLevel::Error) << "Could not load spawn point profile \"" << libraryInfo.profileName.value()
                                    << "\"";
        return false;
      }
    }
    else
    {
      // dependencies.scenario = scenario;
    }

    auto spawnPoint = binding->Instantiate(libraryInfo.libraryName, dependencies);
    if (!spawnPoint)
    {
      LOG_INTERN(LogLevel::DebugCore) << "could not initialize spawn point with library path\""
                                      << libraryInfo.libraryName << "\"";
      return false;
    }

    switch (libraryInfo.type)
    {
      case SpawnPointType::PreRun:
        preRunSpawnZones.emplace(libraryInfo.priority, std::move(spawnPoint));
        break;
      case SpawnPointType::Runtime:
        runtimeSpawnPoints.emplace(libraryInfo.priority, std::move(spawnPoint));
        break;
    }
  }

  return true;
}

bool SpawnPointNetwork::TriggerPreRunSpawnZones()
{
  try
  {
    std::for_each(preRunSpawnZones.crbegin(),
                  preRunSpawnZones.crend(),
                  [&](auto const& element) { element.second->GetImplementation()->Trigger(0); });
  }
  catch (const std::runtime_error& error)
  {
    LOG_INTERN(LogLevel::Error) << error.what();
    return false;
  }

  return true;
}

bool SpawnPointNetwork::TriggerRuntimeSpawnPoints([[maybe_unused]] const int timestamp)
{
  try
  {
    std::for_each(runtimeSpawnPoints.crbegin(),
                  runtimeSpawnPoints.crend(),
                  [&](auto const& element) { element.second->GetImplementation()->Trigger(timestamp); });
  }
  catch (const std::runtime_error& error)
  {
    LOG_INTERN(LogLevel::Error) << error.what();
    return false;
  }

  return true;
}

}  // namespace core
