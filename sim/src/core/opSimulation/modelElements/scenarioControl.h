/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  scenarioControl.h
//! @brief This class holds all control strategies for a single entity
//-----------------------------------------------------------------------------

#pragma once

#include <MantleAPI/Traffic/control_strategy.h>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include "include/scenarioControlInterface.h"

class ScenarioControl : public ScenarioControlInterface  ///< Class representing scenario control
{
public:
  ScenarioControl() = default;
  ~ScenarioControl() override = default;

  /// @brief Function to get strategies
  /// @param type Control strategy type
  /// @return list of pointers to the control strategy
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> GetStrategies(
      mantle_api::ControlStrategyType type) override;

  /// @brief Function to get strategies
  /// @param domain Movement domain
  /// @return list of pointers to the control strategy
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> GetStrategies(mantle_api::MovementDomain domain) override;

  /// @brief Function to set strategies
  /// @param strategies list of pointers of control strategies to set
  void SetStrategies(std::vector<std::shared_ptr<mantle_api::ControlStrategy>> strategies) override;

  /// @return Returns true, if control strategy has a new longitudinal strategy
  bool HasNewLongitudinalStrategy() const override { return newLongitudinalStrategy; }

  /// @return Returns true, if control strategy has a new lateral strategy
  bool HasNewLateralStrategy() const override { return newLateralStrategy; }

  /// @brief Setter function to set control strategy goal
  /// @param type Type of control strategy
  void SetControlStrategyGoalReached(mantle_api::ControlStrategyType type) override;

  /// @brief Function to check if the given control strategy goal has been reached
  /// @param type Type of control strategy
  /// @return Returns true, if the control strategy goal has been reached
  bool HasControlStrategyGoalBeenReached(mantle_api::ControlStrategyType type) const override;

  /// @return Returns true, if custom controller is used
  bool UseCustomController() const override { return useCustomController; }

  /// @brief Setter function to use custom controller
  /// @param useCustomController boolean, if customcontroller has to be used
  void SetCustomController(bool useCustomController) override { this->useCustomController = useCustomController; }

  /// @return Function to get the list of custom commands
  const std::vector<std::string> &GetCustomCommands() override;

  /// @brief Function to add the custom command
  /// @param command custom command to add
  void AddCustomCommand(const std::string &command) override;

  /// @brief Function to update for the next time step
  void UpdateForNextTimestep() override;

  /// Add a scenario command to the controller
  ///
  /// @param[in] command to be published
  void AddCommand(ScenarioCommand::Any command);

  /// Get all published scenario commands
  ///
  /// @returns list of available commands
  const ScenarioCommands &GetCommands() override;

private:
  bool CorrespondentMovementDomain(mantle_api::MovementDomain lhs, mantle_api::MovementDomain rhs);

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> strategies{};
  std::vector<std::string> customCommands;

  bool newLongitudinalStrategy{false};
  bool newLateralStrategy{false};
  bool useCustomController{false};

  std::set<mantle_api::ControlStrategyType> reachedStrategies;
  ScenarioCommands commands;
};
