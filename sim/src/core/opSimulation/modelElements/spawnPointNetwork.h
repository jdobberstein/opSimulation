/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  spawnPointNetwork.h
//! @brief This file implements a container of the spawn points during a
//!        simulation run.
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <memory>
#include <optional>
#include <string>

#include "common/opExport.h"
#include "common/spawnPointLibraryDefinitions.h"
#include "framework/agentBlueprintProvider.h"
#include "include/profilesInterface.h"
#include "include/spawnPointNetworkInterface.h"

class ConfigurationContainerInterface;
class StochasticsInterface;
class WorldInterface;
namespace mantle_api
{
class IEnvironment;
}  // namespace mantle_api
namespace openpass
{
namespace common
{
struct RuntimeInformation;
}  // namespace common
}  // namespace openpass

namespace core
{
class SpawnPointBinding;
class AgentFactoryInterface;
class SpawnPoint;

//! This class represents a container of the spawn points during a simulation run
class SIMULATIONCOREEXPORT SpawnPointNetwork : public SpawnPointNetworkInterface
{
public:
  /**
   * @brief SpawnPointNetwork constructor
   *
   * @param[in] spawnPointBindings    Pointer to the spawn point bindings
   * @param[in] world                 The world interface
   * @param[in] runtimeInformation    Common runtimeInformation
   */
  SpawnPointNetwork(std::map<std::string, SpawnPointBinding>* spawnPointBindings,
                    WorldInterface* world,
                    const openpass::common::RuntimeInformation& runtimeInformation);
  SpawnPointNetwork(const SpawnPointNetwork&) = delete;
  SpawnPointNetwork(SpawnPointNetwork&&) = delete;
  SpawnPointNetwork& operator=(const SpawnPointNetwork&) = delete;
  SpawnPointNetwork& operator=(SpawnPointNetwork&&) = delete;
  ~SpawnPointNetwork() override = default;

  bool Instantiate(const SpawnPointLibraryInfoCollection& libraryInfos,
                   AgentFactoryInterface* agentFactory,
                   StochasticsInterface* stochastics,
                   const std::optional<ProfileGroup>& spawnPointProfiles,
                   ConfigurationContainerInterface* configurationContainer,
                   std::shared_ptr<mantle_api::IEnvironment> environment,
                   std::shared_ptr<Vehicles> vehicles) override;

  bool TriggerPreRunSpawnZones() override;

  bool TriggerRuntimeSpawnPoints(const int timestamp) override;

  void Clear() override;

private:
  std::map<std::string, SpawnPointBinding>* spawnPointBindings;
  WorldInterface* world;
  const openpass::common::RuntimeInformation& runtimeInformation;
  std::multimap<int, std::unique_ptr<SpawnPoint>> preRunSpawnZones;
  std::multimap<int, std::unique_ptr<SpawnPoint>> runtimeSpawnPoints;

  AgentBlueprintProvider agentBlueprintProvider;
};

}  // namespace core
