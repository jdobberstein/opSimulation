/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "agent.h"

#include <iostream>
#include <map>
#include <memory>
#include <new>
#include <stdexcept>
#include <type_traits>
#include <utility>
#include <vector>

#include "bindings/modelBinding.h"
#include "common/log.h"
#include "framework/agentDataPublisher.h"
#include "include/agentBlueprintInterface.h"
#include "include/agentInterface.h"
#include "include/agentTypeInterface.h"
#include "include/componentInterface.h"
#include "include/publisherInterface.h"
#include "include/worldInterface.h"
#include "modelElements/agentType.h"
#include "modelElements/channel.h"
#include "modelElements/componentType.h"

class DataBufferWriteInterface;
class StochasticsInterface;

namespace core
{

Agent::Agent(mantle_api::UniqueId id, WorldInterface *world, const AgentBuildInstructions &agentBuildInstructions)
    : agentInterface(world->CreateAgentAdapter(id, agentBuildInstructions)), id(id), world(world)
{
}

Agent::~Agent() = default;

bool Agent::Instantiate(const AgentBuildInstructions &agentBuildInstructions,
                        ModelBinding *modelBinding,
                        StochasticsInterface *stochastics,
                        ObservationNetworkInterface *observationNetwork,
                        DataBufferWriteInterface *dataBuffer)
{
  // instantiate channels
  for (int channelId : agentBuildInstructions.system.agentType->GetChannels())
  {
    LOG_INTERN(LogLevel::DebugCore) << "- instantiate channel " << channelId;

    auto channel = std::make_unique<Channel>(channelId);
    if (!channel)
    {
      LOG_INTERN(LogLevel::Error) << "agent could not be instantiated";
      return false;
    }

    channel->SetAgent(this);

    if (!AddChannel(channelId, std::move(channel)))
    {
      LOG_INTERN(LogLevel::Error) << "agent could not be instantiated";
      return false;
    }
  }

  // instantiate components
  publisher = std::make_unique<openpass::publisher::AgentDataPublisher>(dataBuffer, id);

  auto instantiateCompontent = [&](const auto &componentName,
                                   const auto &componentType) -> std::unique_ptr<ComponentInterface, ComponentDeleter> &
  {
    LOG_INTERN(LogLevel::DebugCore) << "- instantiate component " << componentName;
    std::unique_ptr<ComponentInterface, ComponentDeleter> component(
        modelBinding->Instantiate(componentType,
                                  componentName,
                                  stochastics,
                                  world,
                                  observationNetwork,
                                  this,
                                  agentBuildInstructions.scenarioControl,
                                  publisher.get()));

    if (!component)
    {
      LogErrorAndThrow("agent.cpp: Failed ModelBinding for '" + componentName
                       + "'. Ensure library availability and correct rpath setting for further dependencies.");
    }

    return AddComponent(componentName, std::move(component));
  };

  for (auto [componentName, componentType] : agentBuildInstructions.system.agentType->GetComponents())
  {
    auto &component = instantiateCompontent(componentName, componentType);
    for (const auto &[linkId, channelRef] : componentType->GetInputLinks())
    {
      LOG_INTERN(LogLevel::DebugCore) << "  * instantiate input channel " << channelRef << " (linkId " << linkId << ")";

      if (auto *channel = GetChannel(channelRef))
      {
        if (!component->AddInputLink(channel, linkId) || !channel->AddTarget(component.get(), linkId))
        {
          return false;
        }
      }
    }

    for (const auto &[linkId, channelRef] : componentType->GetOutputLinks())
    {
      LOG_INTERN(LogLevel::DebugCore) << "  * instantiate output channel " << channelRef << " (linkId " << linkId
                                      << ")";

      if (auto *channel = GetChannel(channelRef))
      {
        if (!component->AddOutputLink(channel, linkId) || !channel->SetSource(component.get(), linkId))
        {
          return false;
        }
      }
    }
  }

  return true;
}

AgentInterface *Agent::GetAgentAdapter() const
{
  return &agentInterface;
}

std::unique_ptr<ComponentInterface, ComponentDeleter> &Agent::AddComponent(
    const std::string &componentName, std::unique_ptr<ComponentInterface, ComponentDeleter> component)
{
  if (auto [nameAndComponent, success] = components.try_emplace(componentName, std::move(component)); success)
  {
    return nameAndComponent->second;
  }
  LogErrorAndThrow("agent.cpp: Could not add component  \"" + componentName + "\" . Components must be unique.");
}

bool Agent::AddChannel(int id, std::unique_ptr<Channel> channel)
{
  if (!channels.try_emplace(id, std::move(channel)).second)
  {
    LOG_INTERN(LogLevel::Warning) << "channels must be unique";
    return false;
  }

  return true;
}

Channel *Agent::GetChannel(int id) const
{
  const auto iter{channels.find(id)};
  return (iter == channels.end()) ? nullptr : iter->second.get();
}

std::vector<std::pair<std::string, ComponentInterface *>> Agent::GetComponents() const
{
  std::vector<std::pair<std::string, ComponentInterface *>> componentsVector;
  componentsVector.reserve(components.size());
  std::transform(std::begin(components),
                 std::end(components),
                 std::back_inserter(componentsVector),
                 [](const auto &item) { return std::make_pair(item.first, item.second.get()); });
  return componentsVector;
}

void Agent::LinkSchedulerTime(int *const schedulerTime)
{
  currentTime = schedulerTime;
}

}  // namespace core
