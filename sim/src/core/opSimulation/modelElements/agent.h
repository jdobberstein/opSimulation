/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  agent.h
//! @brief This file contains the internal representation of an agent instance
//!        during a simulation run.
//-----------------------------------------------------------------------------

#pragma once

#include <MantleAPI/Common/i_identifiable.h>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "include/componentInterface.h"

class DataBufferWriteInterface;
class AgentInterface;
class PublisherInterface;
class StochasticsInterface;
class WorldInterface;
struct AgentBuildInstructions;

namespace core
{

class Channel;
class ModelBinding;
class ObservationNetworkInterface;

/// @brief Takes care or releasing a component, when destroyed or reset
struct ComponentDeleter
{
  /// @brief Releases a component. Used as a deleter in smart pointers for automatic management of component release
  ///
  /// @param component Pointer to a ComponentInterface-derived object to be released
  void operator()(ComponentInterface *component) const { component->ReleaseFromLibrary(); }
};

//! This class represents an agent instance during a simulation run
class Agent
{
public:
  //! Agent constructor
  //!
  //! @param[in] id                       agent id
  //! @param[in] world                    The world interface
  //! @param[in] agentBuildInstructions   Contains parameters of the agent
  Agent(mantle_api::UniqueId id, WorldInterface *world, const AgentBuildInstructions &agentBuildInstructions);
  Agent(const Agent &) = delete;
  Agent(Agent &&) = delete;
  Agent &operator=(const Agent &) = delete;
  Agent &operator=(Agent &&) = delete;
  SIMULATIONCOREEXPORT ~Agent();

  /// @brief Adds a component to the agent.
  ///
  /// @param componentName The name of the component.
  /// @param component The unique pointer to the component.
  /// @return A reference to the added component.
  std::unique_ptr<ComponentInterface, ComponentDeleter> &AddComponent(
      const std::string &componentName, std::unique_ptr<ComponentInterface, ComponentDeleter> component);

  /// @brief Add channel to the agent
  /// @param id       Id of the channel
  /// @param channel  Pointer to the channel
  /// @return True, if successfull
  bool AddChannel(int id, std::unique_ptr<Channel> channel);

  /// @brief Getter function to return channel information
  /// @param id Id of the channel
  /// @return Returns channel
  Channel *GetChannel(int id) const;

  /// @brief Getter function to return all components
  /// @return Returns the name of the component with corresponding pointer to these components
  std::vector<std::pair<std::string, ComponentInterface *>> GetComponents() const;

  //! Returns the agent Id
  //!
  //! @return Id of the agent
  int GetId() const { return id; }

  /// @brief Instantiate the agent
  /// @param agentBuildInstructions   Reference to the parameters of the agent
  /// @param modelBinding             Pointer to the model binding
  /// @param stochastics              Pointer to the stochastics interface
  /// @param observationNetwork       Pointer to the observation network interface
  /// @param dataBuffer               Pointer to the data buffer writer interface
  /// @return True, if the agent is successfully instantiated
  bool Instantiate(const AgentBuildInstructions &agentBuildInstructions,
                   ModelBinding *modelBinding,
                   StochasticsInterface *stochastics,
                   core::ObservationNetworkInterface *observationNetwork,
                   DataBufferWriteInterface *dataBuffer);

  /// @return Getter function to return the pointer to the agent interface
  AgentInterface *GetAgentAdapter() const;

  /// @brief Link the scheduler time to the agent
  /// @param schedulerTime Scheduler time
  void LinkSchedulerTime(int *const schedulerTime);

private:
  AgentInterface &agentInterface;
  mantle_api::UniqueId id;
  WorldInterface *world = nullptr;

  std::vector<int> idsCollisionPartners;
  std::map<int, std::unique_ptr<Channel>> channels;
  std::map<std::string, std::unique_ptr<ComponentInterface, ComponentDeleter>> components;

  std::unique_ptr<PublisherInterface> publisher;

  int *currentTime = nullptr;
};

}  // namespace core
