/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "profilesImporter.h"

#include <algorithm>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <locale>
#include <stdexcept>

#include "common/log.h"
#include "common/parameter.h"
#include "common/sensorDefinitions.h"
#include "common/xmlParser.h"
#include "importer/importerCommon.h"
#include "importer/importerLoggingHelper.h"
#include "importer/parameterImporter.h"
#include "importer/profiles.h"
#include "importerCommon.h"
#include "parameterImporter.h"
#include "profiles.h"

using namespace Importer;
using namespace SimulationCommon;

namespace TAG = openpass::importer::xml::profilesImporter::tag;
namespace ATTRIBUTE = openpass::importer::xml::profilesImporter::attribute;

void ProfilesImporter::ImportAgentProfiles(xmlNodePtr agentProfilesElement, Profiles& profiles)
{
  xmlNodePtr agentProfileElement = GetFirstChildElement(agentProfilesElement, TAG::agentProfile);
  ThrowIfFalse(agentProfileElement, agentProfilesElement, "Tag " + std::string(TAG::agentProfile) + " is missing.");

  while (agentProfileElement)
  {
    if (xmlStrEqual(agentProfileElement->name, toXmlChar(TAG::agentProfile)))
    {
      std::string agentProfileName;
      ThrowIfFalse(ParseAttributeString(agentProfileElement, ATTRIBUTE::name, agentProfileName),
                   agentProfileElement,
                   "Attribute " + std::string(ATTRIBUTE::name) + " is missing.");

      std::string profileType;
      ThrowIfFalse(ParseAttributeString(agentProfileElement, ATTRIBUTE::type, profileType),
                   agentProfileElement,
                   "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");

      AgentProfile agentProfile;

      if (profileType == "Dynamic")
      {
        agentProfile.type = AgentProfileType::Dynamic;

        //Parses all driver profiles
        xmlNodePtr driverProfilesElement = GetFirstChildElement(agentProfileElement, TAG::driverProfiles);
        ThrowIfFalse(
            driverProfilesElement, agentProfileElement, "Tag " + std::string(TAG::driverProfiles) + " is missing.");
        ThrowIfFalse(ImportProbabilityMap(driverProfilesElement,
                                          ATTRIBUTE::name,
                                          TAG::driverProfile,
                                          agentProfile.driverProfiles,
                                          LogErrorAndThrow),
                     driverProfilesElement,
                     "Invalid probalities");

        //Parses all vehicle profiles
        xmlNodePtr systemProfilesElement = GetFirstChildElement(agentProfileElement, TAG::systemProfiles);
        ThrowIfFalse(
            systemProfilesElement, agentProfileElement, "Tag " + std::string(TAG::systemProfiles) + " is missing.");
        ThrowIfFalse(ImportProbabilityMap(systemProfilesElement,
                                          ATTRIBUTE::name,
                                          TAG::systemProfile,
                                          agentProfile.systemProfiles,
                                          LogErrorAndThrow),
                     systemProfilesElement,
                     "Invalid probalities");

        //Parses all vehicle profiles
        xmlNodePtr vehicleModelsElement = GetFirstChildElement(agentProfileElement, TAG::vehicleModels);
        ThrowIfFalse(
            vehicleModelsElement, agentProfileElement, "Tag " + std::string(TAG::vehicleModels) + " is missing.");
        ThrowIfFalse(
            ImportProbabilityMap(
                vehicleModelsElement, ATTRIBUTE::name, TAG::vehicleModel, agentProfile.vehicleModels, LogErrorAndThrow),
            vehicleModelsElement,
            "Invalid probalities");
      }
      else
      {
        ThrowIfFalse(profileType == "Static", "Invalid agent profile type.");

        agentProfile.type = AgentProfileType::Static;
        xmlNodePtr systemElement = GetFirstChildElement(agentProfileElement, TAG::system);
        ThrowIfFalse(systemElement, agentProfileElement, "Tag " + std::string(TAG::system) + " is missing.");

        std::string systemConfigFile;
        ThrowIfFalse(ParseString(systemElement, ATTRIBUTE::file, systemConfigFile),
                     systemElement,
                     "Attribute " + std::string(ATTRIBUTE::file) + " is missing.");

        agentProfile.systemConfigFile = systemConfigFile;

        int systemId = 0;
        ThrowIfFalse(ParseInt(systemElement, ATTRIBUTE::id, systemId),
                     systemElement,
                     "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");

        agentProfile.systemId = systemId;

        std::string vehicleModel;
        ThrowIfFalse(ParseString(agentProfileElement, ATTRIBUTE::vehicleModel, vehicleModel),
                     agentProfileElement,
                     "Attribute " + std::string(ATTRIBUTE::vehicleModel) + " is missing.");

        agentProfile.vehicleModel = vehicleModel;
      }

      ThrowIfFalse(profiles.AddAgentProfile(agentProfileName, agentProfile),
                   agentProfileElement,
                   "AgentProfile names need to be unique.");
    }

    agentProfileElement = xmlNextElementSibling(agentProfileElement);
  }
}

void ProfilesImporter::ImportProfileGroups(Profiles& profiles, xmlNodePtr profilesElement)
{
  xmlNodePtr profileGroupElement = GetFirstChildElement(profilesElement, TAG::profileGroup);
  ThrowIfFalse(profileGroupElement, "ProfileGroup element is missing.");

  while (profileGroupElement)
  {
    if (xmlStrEqual(profileGroupElement->name, toXmlChar(TAG::profileGroup)))
    {
      std::string profileType;
      ThrowIfFalse(ParseAttributeString(profileGroupElement, ATTRIBUTE::type, profileType),
                   profileGroupElement,
                   "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");

      xmlNodePtr profileElement = GetFirstChildElement(profileGroupElement, TAG::profile);

      while (profileElement)
      {
        if (xmlStrEqual(profileElement->name, toXmlChar(TAG::profile)))
        {
          std::string profileName;
          ThrowIfFalse(ParseAttributeString(profileElement, ATTRIBUTE::name, profileName),
                       profileElement,
                       "Attribute " + std::string(ATTRIBUTE::name) + " is missing.");

          openpass::parameter::ParameterSetLevel1 parameters{};
          try
          {
            parameters = openpass::parameter::Import(profileElement, profilesElement);
          }
          catch (const std::runtime_error& error)
          {
            LogErrorAndThrow("Could not import spawner profile parameters: " + std::string(error.what()));
          }

          ThrowIfFalse(profiles.AddProfileGroup(profileType, profileName, parameters),
                       profileElement,
                       "Profile names need to be unique within a ProfileGroup");
        }
        profileElement = xmlNextElementSibling(profileElement);
      }
    }
    profileGroupElement = xmlNextElementSibling(profileGroupElement);
  }
}

void ProfilesImporter::ImportSensorLinksOfComponent(xmlNodePtr sensorLinksElement, std::vector<SensorLink>& sensorLinks)
{
  xmlNodePtr sensorLinkElement = GetFirstChildElement(sensorLinksElement, TAG::sensorLink);

  while (sensorLinkElement)
  {
    if (xmlStrEqual(sensorLinkElement->name, toXmlChar(TAG::sensorLink)))
    {
      int sensorId = 0;
      std::string inputId;

      ThrowIfFalse(ParseAttributeInt(sensorLinkElement, ATTRIBUTE::sensorId, sensorId),
                   sensorLinkElement,
                   "Attribute " + std::string(ATTRIBUTE::sensorId) + " is missing.");
      ThrowIfFalse(ParseAttributeString(sensorLinkElement, ATTRIBUTE::inputId, inputId),
                   sensorLinkElement,
                   "Attribute " + std::string(ATTRIBUTE::inputId) + " is missing.");

      SensorLink sensorLink{};
      sensorLink.sensorId = sensorId;
      sensorLink.inputId = inputId;
      sensorLinks.push_back(sensorLink);
    }

    sensorLinkElement = xmlNextElementSibling(sensorLinkElement);
  }
}

void ProfilesImporter::ImportVehicleComponent(xmlNodePtr vehicleComponentElement, VehicleComponent& vehicleComponent)
{
  ThrowIfFalse(ParseAttributeString(vehicleComponentElement, ATTRIBUTE::type, vehicleComponent.type),
               vehicleComponentElement,
               "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");

  xmlNodePtr profilesElement = GetFirstChildElement(vehicleComponentElement, TAG::profiles);
  ThrowIfFalse(profilesElement, vehicleComponentElement, "Tag " + std::string(TAG::profiles) + " is missing.");
  ThrowIfFalse(
      ImportProbabilityMap(
          profilesElement, ATTRIBUTE::name, "Profile", vehicleComponent.componentProfiles, LogErrorAndThrow, false),
      profilesElement,
      "Attribute " + std::string(ATTRIBUTE::name) + " is missing.");

  xmlNodePtr sensorLinksElement = GetFirstChildElement(vehicleComponentElement, TAG::sensorLinks);
  ImportSensorLinksOfComponent(sensorLinksElement, vehicleComponent.sensorLinks);
}

void ProfilesImporter::ImportAllVehicleComponentsOfSystemProfile(xmlNodePtr systemProfileElement,
                                                                 SystemProfile& systemProfile)
{
  xmlNodePtr vehicleComponentsElement = GetFirstChildElement(systemProfileElement, TAG::components);
  ThrowIfFalse(vehicleComponentsElement, systemProfileElement, "Tag " + std::string(TAG::components) + " is missing.");

  xmlNodePtr componentElement = GetFirstChildElement(vehicleComponentsElement, TAG::component);

  while (componentElement)
  {
    if (xmlStrEqual(componentElement->name, toXmlChar(TAG::component)))
    {
      VehicleComponent vehicleComponent;
      ImportVehicleComponent(componentElement, vehicleComponent);

      systemProfile.vehicleComponents.push_back(vehicleComponent);
    }
    componentElement = xmlNextElementSibling(componentElement);
  }
}

void ProfilesImporter::ImportSensorParameters(xmlNodePtr sensorElement, openpass::sensors::Parameter& sensor)
{
  ThrowIfFalse(ParseAttributeInt(sensorElement, ATTRIBUTE::id, sensor.id),
               sensorElement,
               "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");
  ThrowIfFalse(ParseAttributeString(sensorElement, ATTRIBUTE::position, sensor.positionName),
               sensorElement,
               "Attribute " + std::string(ATTRIBUTE::position) + " is missing.");

  xmlNodePtr profileElement = GetFirstChildElement(sensorElement, TAG::profile);
  ThrowIfFalse(profileElement, sensorElement, "Tag " + std::string(TAG::profile) + " is missing.");
  ThrowIfFalse(ParseAttributeString(profileElement, ATTRIBUTE::type, sensor.profile.type),
               profileElement,
               "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");
  ThrowIfFalse(ParseAttributeString(profileElement, ATTRIBUTE::name, sensor.profile.name),
               profileElement,
               "Attribute " + std::string(ATTRIBUTE::name) + " is missing.");
}

void ProfilesImporter::ImportAllSensorsOfSystemProfile(xmlNodePtr systemProfileElement, SystemProfile& systemProfile)
{
  xmlNodePtr sensorsElement = GetFirstChildElement(systemProfileElement, TAG::sensors);
  ThrowIfFalse(sensorsElement, systemProfileElement, "Tag " + std::string(TAG::sensors) + " is missing.");

  xmlNodePtr sensorElement = GetFirstChildElement(sensorsElement, TAG::sensor);

  while (sensorElement)
  {
    if (xmlStrEqual(sensorElement->name, toXmlChar(TAG::sensor)))
    {
      openpass::sensors::Parameter sensor;
      ImportSensorParameters(sensorElement, sensor);

      systemProfile.sensors.push_back(sensor);
    }
    sensorElement = xmlNextElementSibling(sensorElement);
  }
}

SystemProfile ProfilesImporter::ImportSystemProfile(xmlNodePtr systemProfileElement)
{
  SystemProfile systemProfile;

  ImportAllVehicleComponentsOfSystemProfile(systemProfileElement, systemProfile);
  ImportAllSensorsOfSystemProfile(systemProfileElement, systemProfile);

  return systemProfile;
}

void ProfilesImporter::ImportSystemProfiles(xmlNodePtr systemProfilesElement, Profiles& profiles)
{
  xmlNodePtr systemProfileElement = GetFirstChildElement(systemProfilesElement, TAG::systemProfile);

  while (systemProfileElement)
  {
    if (xmlStrEqual(systemProfileElement->name, toXmlChar(TAG::systemProfile)))
    {
      std::string profileName;
      ThrowIfFalse(ParseAttributeString(systemProfileElement, ATTRIBUTE::name, profileName),
                   systemProfileElement,
                   "Attribute " + std::string(ATTRIBUTE::name) + " is missing.");

      auto systemProfile = ImportSystemProfile(systemProfileElement);
      profiles.AddSystemProfile(profileName, systemProfile);
    }

    systemProfileElement = xmlNextElementSibling(systemProfileElement);
  }
}

bool ProfilesImporter::Import(const std::string& filename,
                              Profiles& profiles,
                              const std::filesystem::path& executionPath)
{
  try
  {
    xmlInitParser();

    xmlDocPtr doc
        = xmlReadFile(filename.c_str(),
                      NULL,  // NOLINT(modernize-use-nullptr)
                             // https://gnome.pages.gitlab.gnome.org/libxml2/devhelp/libxml2-parser.html#xmlReadFile
                      0);
    ThrowIfFalse(doc != nullptr,
                 "an error occurred during profilesCatalog import. Invalid xml file format of file " + filename);

    xmlNodePtr documentRoot = xmlDocGetRootElement(doc);
    ThrowIfFalse(documentRoot != nullptr, "invalid document root " + filename);

    ImporterCommon::validateXMLSchema(filename, "ProfilesCatalog.xsd", doc, executionPath);

    std::string configVersion;
    ParseAttributeString(documentRoot, ATTRIBUTE::schemaVersion, configVersion);
    ThrowIfFalse(
        configVersion.compare(supportedConfigVersion) == 0,
        "ProfilesCatalog version is not supported. Supported version is " + std::string(supportedConfigVersion));

    xmlNodePtr agentProfilesElement = GetFirstChildElement(documentRoot, TAG::agentProfiles);
    ThrowIfFalse(agentProfilesElement, "AgentProfiles element is missing.");
    ImportAgentProfiles(agentProfilesElement, profiles);

    xmlNodePtr systemProfilesElement = GetFirstChildElement(documentRoot, TAG::systemProfiles);
    if (systemProfilesElement)
    {
      ImportSystemProfiles(systemProfilesElement, profiles);
    }

    ImportProfileGroups(profiles, documentRoot);

    xmlFreeDoc(doc);
    xmlCleanupParser();

    return true;
  }
  catch (std::runtime_error& e)
  {
    LOG_INTERN(LogLevel::Error) << "ProfilesCatalog import failed: " + std::string(e.what());
    return false;
  }
}
