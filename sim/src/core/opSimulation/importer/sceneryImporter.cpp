/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "sceneryImporter.h"

#include <algorithm>
#include <cstddef>
#include <iostream>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <limits>
#include <locale>
#include <map>
#include <optional>
#include <regex>
#include <stdexcept>
#include <string>
#include <units.h>
#include <utility>

#include "common/commonHelper.h"
#include "common/commonTools.h"
#include "common/globalDefinitions.h"
#include "common/log.h"
#include "common/xmlParser.h"
#include "importer/importerCommon.h"
#include "importer/importerLoggingHelper.h"
#include "importer/scenery.h"
#include "include/roadInterface/connectionInterface.h"
#include "include/roadInterface/junctionInterface.h"
#include "include/roadInterface/roadInterface.h"
#include "include/roadInterface/roadLaneInterface.h"
#include "include/roadInterface/roadLaneSectionInterface.h"

using namespace Configuration;
using namespace SimulationCommon;

namespace TAG = openpass::importer::xml::sceneryImporter::tag;
namespace ATTRIBUTE = openpass::importer::xml::sceneryImporter::attribute;

namespace Importer
{
const std::map<std::string, RoadSignalUnit> roadSignalUnitConversionMap = {{"m", RoadSignalUnit::Meter},
                                                                           {"km", RoadSignalUnit::Kilometer},
                                                                           {"ft", RoadSignalUnit::Feet},
                                                                           {"mile", RoadSignalUnit::LandMile},
                                                                           {"m/s", RoadSignalUnit::MetersPerSecond},
                                                                           {"km/h", RoadSignalUnit::KilometersPerHour},
                                                                           {"mph", RoadSignalUnit::MilesPerHour},
                                                                           {"kg", RoadSignalUnit::Kilogram},
                                                                           {"t", RoadSignalUnit::MetricTons},
                                                                           {"%", RoadSignalUnit::Percent}};

const std::map<std::string, RoadLaneRoadMarkType> roadLaneRoadMarkTypeConversionMap
    = {{"none", RoadLaneRoadMarkType::None},
       {"solid", RoadLaneRoadMarkType::Solid},
       {"broken", RoadLaneRoadMarkType::Broken},
       {"solid solid", RoadLaneRoadMarkType::Solid_Solid},
       {"solid broken", RoadLaneRoadMarkType::Solid_Broken},
       {"broken solid", RoadLaneRoadMarkType::Broken_Solid},
       {"broken broken", RoadLaneRoadMarkType::Broken_Broken},
       {"botts dots", RoadLaneRoadMarkType::Botts_Dots},
       {"grass", RoadLaneRoadMarkType::Grass},
       {"curb", RoadLaneRoadMarkType::Curb}};

const std::map<std::string, RoadLaneRoadMarkColor> roadLaneRoadMarkColorConversionMap
    = {{"blue", RoadLaneRoadMarkColor::Blue},
       {"green", RoadLaneRoadMarkColor::Green},
       {"red", RoadLaneRoadMarkColor::Red},
       {"yellow", RoadLaneRoadMarkColor::Yellow},
       {"standard", RoadLaneRoadMarkColor::White},
       {"white", RoadLaneRoadMarkColor::White},
       {"orange", RoadLaneRoadMarkColor::Orange}};

template <typename T>
bool assignIfMatching(const std::string& element, T& enumeration, const std::string& match, const T& value)
{
  if (element == match)
  {
    enumeration = value;
    return true;
  }
  return false;
}

bool ParseType(const std::string& element, RoadElementOrientation& orientation)
{
  return assignIfMatching(element, orientation, "none", RoadElementOrientation::both)
      || assignIfMatching(element, orientation, "+", RoadElementOrientation::positive)
      || assignIfMatching(element, orientation, "-", RoadElementOrientation::negative);
}

bool ParseType(const std::string& element, RoadObjectType& objectType)
{
  return assignIfMatching(element, objectType, "none", RoadObjectType::none)
      || assignIfMatching(element, objectType, "obstacle", RoadObjectType::obstacle)
      || assignIfMatching(element, objectType, "car", RoadObjectType::car)
      || assignIfMatching(element, objectType, "pole", RoadObjectType::pole)
      || assignIfMatching(element, objectType, "tree", RoadObjectType::tree)
      || assignIfMatching(element, objectType, "vegetation", RoadObjectType::vegetation)
      || assignIfMatching(element, objectType, "barrier", RoadObjectType::barrier)
      || assignIfMatching(element, objectType, "building", RoadObjectType::building)
      || assignIfMatching(element, objectType, "parkingSpace", RoadObjectType::parkingSpace)
      || assignIfMatching(element, objectType, "patch", RoadObjectType::patch)
      || assignIfMatching(element, objectType, "railing", RoadObjectType::railing)
      || assignIfMatching(element, objectType, "trafficIsland", RoadObjectType::trafficIsland)
      || assignIfMatching(element, objectType, "crosswalk", RoadObjectType::crosswalk)
      || assignIfMatching(element, objectType, "streetlamp", RoadObjectType::streetlamp)
      || assignIfMatching(element, objectType, "gantry", RoadObjectType::gantry)
      || assignIfMatching(element, objectType, "soundBarrier", RoadObjectType::soundBarrier)
      || assignIfMatching(element, objectType, "van", RoadObjectType::van)
      || assignIfMatching(element, objectType, "bus", RoadObjectType::bus)
      || assignIfMatching(element, objectType, "trailer", RoadObjectType::trailer)
      || assignIfMatching(element, objectType, "bike", RoadObjectType::bike)
      || assignIfMatching(element, objectType, "motorbike", RoadObjectType::motorbike)
      || assignIfMatching(element, objectType, "tram", RoadObjectType::tram)
      || assignIfMatching(element, objectType, "train", RoadObjectType::train)
      || assignIfMatching(element, objectType, "pedestrian", RoadObjectType::pedestrian)
      || assignIfMatching(element, objectType, "wind", RoadObjectType::wind)
      || assignIfMatching(element, objectType, "roadMark", RoadObjectType::roadMark);
}

template <typename T>
bool ParseAttributeType(xmlNodePtr element, const std::string& attributeName, T& result)
{
  if (element == nullptr || attributeName.empty())
  {
    return false;
  }

  xmlChar* attribute = xmlGetProp(element, toXmlChar(attributeName));
  if (attribute == nullptr)
  {
    return false;
  }

  auto type = ParseType(toString(attribute), result);
  xmlFree(attribute);
  return type;
}

void SceneryImporter::ParseLanes(xmlNodePtr rootElement, RoadLaneSectionInterface* laneSection)
{
  xmlNodePtr roadLaneElement = GetFirstChildElement(rootElement, TAG::lane);

  while (roadLaneElement)
  {
    if (xmlStrEqual(roadLaneElement->name, toXmlChar(TAG::lane)))
    {
      int roadLaneId{};
      ThrowIfFalse(ParseAttributeInt(roadLaneElement, ATTRIBUTE::id, roadLaneId),
                   roadLaneElement,
                   "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");

      std::string roadLaneTypeStr;
      ThrowIfFalse(ParseAttributeString(roadLaneElement, ATTRIBUTE::type, roadLaneTypeStr),
                   roadLaneElement,
                   "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");

      RoadLaneType roadLaneType = roadLaneTypeConversionMap.at(roadLaneTypeStr);

      RoadLaneInterface* roadLane = laneSection->AddRoadLane(roadLaneId, roadLaneType);
      ThrowIfFalse(roadLane, roadLaneElement, "Id must be unique");
      LOG_INTERN(LogLevel::DebugCore) << "lane: type " << static_cast<int>(roadLaneType) << ", id=" << roadLaneId;

      if (0 != roadLaneId)  // skip center lanes
      {
        xmlNodePtr widthElement = GetFirstChildElement(roadLaneElement, TAG::width);
        xmlNodePtr borderElement = GetFirstChildElement(roadLaneElement, TAG::border);
        // at least one width element necessary
        if (widthElement)
        {
          while (widthElement)
          {
            if (xmlStrEqual(widthElement->name, toXmlChar(TAG::width)))
            {
              units::length::meter_t sOffset;
              units::length::meter_t a;
              double b{};
              units::unit_t<units::inverse<units::length::meter>> c;
              units::unit_t<units::inverse<units::squared<units::length::meter>>> d;

              ThrowIfFalse(ParseAttributeDouble(widthElement, ATTRIBUTE::sOffset, sOffset),
                           widthElement,
                           "Attribute " + std::string(ATTRIBUTE::sOffset) + " is missing.");
              ThrowIfFalse(ParseAttributeDouble(widthElement, ATTRIBUTE::a, a),
                           widthElement,
                           "Attribute " + std::string(ATTRIBUTE::a) + " is missing.");
              ThrowIfFalse(ParseAttributeDouble(widthElement, ATTRIBUTE::b, b),
                           widthElement,
                           "Attribute " + std::string(ATTRIBUTE::b) + " is missing.");
              ThrowIfFalse(ParseAttributeDouble(widthElement, ATTRIBUTE::c, c),
                           widthElement,
                           "Attribute " + std::string(ATTRIBUTE::c) + " is missing.");
              ThrowIfFalse(ParseAttributeDouble(widthElement, ATTRIBUTE::d, d),
                           widthElement,
                           "Attribute " + std::string(ATTRIBUTE::d) + " is missing.");

              roadLane->AddWidth(sOffset, a, b, c, d);
            }

            widthElement = xmlNextElementSibling(widthElement);
          }
        }
        else if (borderElement)
        {
          while (borderElement)
          {
            if (xmlStrEqual(borderElement->name, toXmlChar(TAG::border)))
            {
              units::length::meter_t sOffset;
              units::length::meter_t a;
              double b{};
              units::unit_t<units::inverse<units::length::meter>> c;
              units::unit_t<units::inverse<units::squared<units::length::meter>>> d;

              ThrowIfFalse(ParseAttributeDouble(borderElement, ATTRIBUTE::sOffset, sOffset),
                           borderElement,
                           "Attribute " + std::string(ATTRIBUTE::sOffset) + " is missing.");
              ThrowIfFalse(ParseAttributeDouble(borderElement, ATTRIBUTE::a, a),
                           borderElement,
                           "Attribute " + std::string(ATTRIBUTE::a) + " is missing.");
              ThrowIfFalse(ParseAttributeDouble(borderElement, ATTRIBUTE::b, b),
                           borderElement,
                           "Attribute " + std::string(ATTRIBUTE::b) + " is missing.");
              ThrowIfFalse(ParseAttributeDouble(borderElement, ATTRIBUTE::c, c),
                           borderElement,
                           "Attribute " + std::string(ATTRIBUTE::c) + " is missing.");
              ThrowIfFalse(ParseAttributeDouble(borderElement, ATTRIBUTE::d, d),
                           borderElement,
                           "Attribute " + std::string(ATTRIBUTE::d) + " is missing.");

              roadLane->AddBorder(sOffset, a, b, c, d);
            }
            borderElement = xmlNextElementSibling(borderElement);
          }
        }
      }

      xmlNodePtr roadLaneLinkElement = GetFirstChildElement(roadLaneElement, TAG::link);
      if (roadLaneLinkElement)
      {
        int roadLaneLinkItemId{};
        xmlNodePtr roadLaneLinkSuccessorElement = GetFirstChildElement(roadLaneLinkElement, TAG::successor);
        if (roadLaneLinkSuccessorElement)
        {
          ThrowIfFalse(ParseAttributeInt(roadLaneLinkSuccessorElement, ATTRIBUTE::id, roadLaneLinkItemId),
                       roadLaneLinkSuccessorElement,
                       "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");
          roadLane->AddSuccessor(roadLaneLinkItemId);
        }

        xmlNodePtr roadLaneLinkPredecessorElement = GetFirstChildElement(roadLaneLinkElement, TAG::predecessor);
        if (roadLaneLinkPredecessorElement)
        {
          ThrowIfFalse(ParseAttributeInt(roadLaneLinkPredecessorElement, ATTRIBUTE::id, roadLaneLinkItemId),
                       roadLaneLinkPredecessorElement,
                       "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");
          roadLane->AddPredecessor(roadLaneLinkItemId);
        }
      }

      ParseLaneRoadMark(toString(rootElement->name), roadLaneElement, roadLane);
    }
    roadLaneElement = xmlNextElementSibling(roadLaneElement);
  }  // lane loop
}

void SceneryImporter::ParseLaneRoadMark(const std::string& leftCenterRight,
                                        xmlNodePtr roadLaneElement,
                                        RoadLaneInterface* roadLane)
{
  RoadLaneRoadDescriptionType descType = RoadLaneRoadDescriptionType::Center;

  if (0 == leftCenterRight.compare("left"))
  {
    descType = RoadLaneRoadDescriptionType::Left;
  }
  else if (0 == leftCenterRight.compare("right"))
  {
    descType = RoadLaneRoadDescriptionType::Right;
  }

  xmlNodePtr roadLaneRoadMarkElement = GetFirstChildElement(roadLaneElement, TAG::roadMark);

  while (roadLaneRoadMarkElement)
  {
    if (xmlStrEqual(roadLaneRoadMarkElement->name, toXmlChar(TAG::roadMark)))
    {
      units::length::meter_t roadLaneSOffset;

      ThrowIfFalse(ParseAttributeDouble(roadLaneRoadMarkElement, ATTRIBUTE::sOffset, roadLaneSOffset, {0.0_m}),
                   roadLaneRoadMarkElement,
                   "Attribute " + std::string(ATTRIBUTE::sOffset) + " is missing.");

      std::string roadMarkTypeStr;
      ThrowIfFalse(ParseAttributeString(roadLaneRoadMarkElement, ATTRIBUTE::type, roadMarkTypeStr, "none"),
                   roadLaneRoadMarkElement,
                   "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");
      RoadLaneRoadMarkType roadMarkType = RoadLaneRoadMarkType::Undefined;
      if (auto iter = roadLaneRoadMarkTypeConversionMap.find(roadMarkTypeStr);
          iter != roadLaneRoadMarkTypeConversionMap.end())
      {
        roadMarkType = iter->second;
      }
      std::string roadMarkColorStr;
      ThrowIfFalse(ParseAttributeString(roadLaneRoadMarkElement, ATTRIBUTE::color, roadMarkColorStr, "standard"),
                   roadLaneRoadMarkElement,
                   "Attribute " + std::string(ATTRIBUTE::color) + " is missing.");
      RoadLaneRoadMarkColor color = RoadLaneRoadMarkColor::Undefined;
      if (auto iter = roadLaneRoadMarkColorConversionMap.find(roadMarkColorStr);
          iter != roadLaneRoadMarkColorConversionMap.end())
      {
        color = iter->second;
      }
      RoadLaneRoadMarkLaneChange roadChange = RoadLaneRoadMarkLaneChange::Undefined;

      std::string weightStr;
      ThrowIfFalse(ParseAttributeString(roadLaneRoadMarkElement, ATTRIBUTE::weight, weightStr, "standard"),
                   roadLaneRoadMarkElement,
                   "Attribute " + std::string(ATTRIBUTE::weight) + " is missing.");
      RoadLaneRoadMarkWeight weight = RoadLaneRoadMarkWeight::Undefined;
      if (weightStr == "standard")
      {
        weight = RoadLaneRoadMarkWeight::Standard;
      }
      else if (weightStr == "bold")
      {
        weight = RoadLaneRoadMarkWeight::Bold;
      }

      roadLane->AddRoadMark(roadLaneSOffset, descType, roadMarkType, color, roadChange, weight);
    }
    roadLaneRoadMarkElement = xmlNextElementSibling(roadLaneRoadMarkElement);
  }
}

void SceneryImporter::ParseGeometries(xmlNodePtr roadElement, RoadInterface* road)
{
  // road plan view
  xmlNodePtr roadPlanView = GetFirstChildElement(roadElement, TAG::planView);
  ThrowIfFalse(roadPlanView, roadElement, "Tag " + std::string(TAG::planView) + " is missing.");

  xmlNodePtr roadGeometryHeaderElement = GetFirstChildElement(roadPlanView, TAG::geometry);
  ThrowIfFalse(roadGeometryHeaderElement, roadPlanView, "Tag " + std::string(TAG::geometry) + " is missing.");
  while (roadGeometryHeaderElement)
  {
    if (xmlStrEqual(roadGeometryHeaderElement->name, toXmlChar(TAG::geometry)))
    {
      units::length::meter_t roadGeometryS;
      ThrowIfFalse(ParseAttributeDouble(roadGeometryHeaderElement, ATTRIBUTE::s, roadGeometryS),
                   roadGeometryHeaderElement,
                   "Attribute " + std::string(ATTRIBUTE::s) + " is missing.");
      roadGeometryS = CommonHelper::roundDoubleWithDecimals(roadGeometryS, 3);

      units::length::meter_t roadGeometryX;
      ThrowIfFalse(ParseAttributeDouble(roadGeometryHeaderElement, ATTRIBUTE::x, roadGeometryX),
                   roadGeometryHeaderElement,
                   "Attribute " + std::string(ATTRIBUTE::x) + " is missing.");
      roadGeometryX = CommonHelper::roundDoubleWithDecimals(roadGeometryX, 3);

      units::length::meter_t roadGeometryY;
      ThrowIfFalse(ParseAttributeDouble(roadGeometryHeaderElement, ATTRIBUTE::y, roadGeometryY),
                   roadGeometryHeaderElement,
                   "Attribute " + std::string(ATTRIBUTE::y) + " is missing.");
      roadGeometryY = CommonHelper::roundDoubleWithDecimals(roadGeometryY, 3);

      units::angle::radian_t roadGeometryHdg;
      ThrowIfFalse(ParseAttributeDouble(roadGeometryHeaderElement, ATTRIBUTE::hdg, roadGeometryHdg),
                   roadGeometryHeaderElement,
                   "Attribute " + std::string(ATTRIBUTE::hdg) + " is missing.");
      roadGeometryHdg = CommonHelper::roundDoubleWithDecimals(roadGeometryHdg, 6);

      units::length::meter_t roadGeometryLength;
      ThrowIfFalse(ParseAttributeDouble(roadGeometryHeaderElement, ATTRIBUTE::length, roadGeometryLength),
                   roadGeometryHeaderElement,
                   "Attribute " + std::string(ATTRIBUTE::length) + " is missing.");
      roadGeometryLength = CommonHelper::roundDoubleWithDecimals(roadGeometryLength, 3);

      xmlNodePtr roadGeometryLineElement = GetFirstChildElement(roadGeometryHeaderElement, TAG::line);
      xmlNodePtr roadGeometryArcElement = GetFirstChildElement(roadGeometryHeaderElement, TAG::arc);
      xmlNodePtr roadGeometrySpiralElement = GetFirstChildElement(roadGeometryHeaderElement, TAG::spiral);
      xmlNodePtr roadGeometryPoly3Element = GetFirstChildElement(roadGeometryHeaderElement, TAG::poly3);
      xmlNodePtr roadGeometryParamPoly3Element = GetFirstChildElement(roadGeometryHeaderElement, TAG::paramPoly3);
      if (roadGeometryLineElement)
      {
        road->AddGeometryLine(roadGeometryS, roadGeometryX, roadGeometryY, roadGeometryHdg, roadGeometryLength);
      }
      else if (roadGeometryArcElement)
      {
        units::curvature::inverse_meter_t roadGeometryCurvature;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryArcElement, ATTRIBUTE::curvature, roadGeometryCurvature),
                     roadGeometryArcElement,
                     "Attribute " + std::string(ATTRIBUTE::curvature) + " is missing.");
        road->AddGeometryArc(
            roadGeometryS, roadGeometryX, roadGeometryY, roadGeometryHdg, roadGeometryLength, roadGeometryCurvature);
      }
      else if (roadGeometrySpiralElement)
      {
        units::curvature::inverse_meter_t roadGeometryCurvStart;
        ThrowIfFalse(ParseAttributeDouble(roadGeometrySpiralElement, ATTRIBUTE::curvStart, roadGeometryCurvStart),
                     roadGeometrySpiralElement,
                     "Attribute " + std::string(ATTRIBUTE::curvStart) + " is missing.");

        units::curvature::inverse_meter_t roadGeometryCurvEnd;
        ThrowIfFalse(ParseAttributeDouble(roadGeometrySpiralElement, ATTRIBUTE::curvEnd, roadGeometryCurvEnd),
                     roadGeometrySpiralElement,
                     "Attribute " + std::string(ATTRIBUTE::curvEnd) + " is missing.");

        if (roadGeometryCurvStart < 1e-7_i_m && roadGeometryCurvStart > -1e-7_i_m)
        {
          roadGeometryCurvStart = 0_i_m;
        }

        if (roadGeometryCurvEnd < 1e-7_i_m && roadGeometryCurvEnd > -1e-7_i_m)
        {
          roadGeometryCurvEnd = 0_i_m;
        }
        road->AddGeometrySpiral(roadGeometryS,
                                roadGeometryX,
                                roadGeometryY,
                                roadGeometryHdg,
                                roadGeometryLength,
                                roadGeometryCurvStart,
                                roadGeometryCurvEnd);
      }
      else if (roadGeometryPoly3Element)
      {
        units::length::meter_t roadGeometryA;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryPoly3Element, ATTRIBUTE::a, roadGeometryA),
                     roadGeometryPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::a) + " is missing.");

        double roadGeometryB{};
        ThrowIfFalse(ParseAttributeDouble(roadGeometryPoly3Element, ATTRIBUTE::b, roadGeometryB),
                     roadGeometryPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::b) + " is missing.");

        units::unit_t<units::inverse<units::length::meter>> roadGeometryC;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryPoly3Element, ATTRIBUTE::c, roadGeometryC),
                     roadGeometryPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::c) + " is missing.");

        units::unit_t<units::inverse<units::squared<units::length::meter>>> roadGeometryD;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryPoly3Element, ATTRIBUTE::d, roadGeometryD),
                     roadGeometryPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::d) + " is missing.");
        road->AddGeometryPoly3(roadGeometryS,
                               roadGeometryX,
                               roadGeometryY,
                               roadGeometryHdg,
                               roadGeometryLength,
                               roadGeometryA,
                               roadGeometryB,
                               roadGeometryC,
                               roadGeometryD);
      }
      else if (roadGeometryParamPoly3Element)
      {
        units::length::meter_t roadGeometryaU;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryParamPoly3Element, ATTRIBUTE::aU, roadGeometryaU),
                     roadGeometryParamPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::aU) + " is missing.");

        units::dimensionless::scalar_t roadGeometrybU;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryParamPoly3Element, ATTRIBUTE::bU, roadGeometrybU),
                     roadGeometryParamPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::bU) + " is missing.");

        units::curvature::inverse_meter_t roadGeometrycU;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryParamPoly3Element, ATTRIBUTE::cU, roadGeometrycU),
                     roadGeometryParamPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::cU) + " is missing.");

        units::unit_t<units::inverse<units::squared<units::length::meter>>> roadGeometrydU;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryParamPoly3Element, ATTRIBUTE::dU, roadGeometrydU),
                     roadGeometryParamPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::dU) + " is missing.");

        units::length::meter_t roadGeometryaV;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryParamPoly3Element, ATTRIBUTE::aV, roadGeometryaV),
                     roadGeometryParamPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::aV) + " is missing.");

        units::dimensionless::scalar_t roadGeometrybV;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryParamPoly3Element, ATTRIBUTE::bV, roadGeometrybV),
                     roadGeometryParamPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::bV) + " is missing.");

        units::curvature::inverse_meter_t roadGeometrycV;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryParamPoly3Element, ATTRIBUTE::cV, roadGeometrycV),
                     roadGeometryParamPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::cV) + " is missing.");

        units::unit_t<units::inverse<units::squared<units::length::meter>>> roadGeometrydV;
        ThrowIfFalse(ParseAttributeDouble(roadGeometryParamPoly3Element, ATTRIBUTE::dV, roadGeometrydV),
                     roadGeometryParamPoly3Element,
                     "Attribute " + std::string(ATTRIBUTE::dV) + " is missing.");

        ParamPoly3Parameters parameters = {roadGeometryaU,
                                           roadGeometrybU,
                                           roadGeometrycU,
                                           roadGeometrydU,
                                           roadGeometryaV,
                                           roadGeometrybV,
                                           roadGeometrycV,
                                           roadGeometrydV};
        road->AddGeometryParamPoly3(
            roadGeometryS, roadGeometryX, roadGeometryY, roadGeometryHdg, roadGeometryLength, parameters);
      }
      else
      {
        LogErrorAndThrow("invalid geometry");
      }

      LOG_INTERN(LogLevel::DebugCore) << "road geometry: s: " << roadGeometryS << ", x: " << roadGeometryX
                                      << ", y: " << roadGeometryY << ", hdg: " << roadGeometryHdg
                                      << ", length: " << roadGeometryLength;
    }
    roadGeometryHeaderElement = xmlNextElementSibling(roadGeometryHeaderElement);
  }  // road geometry loop
}

void SceneryImporter::ParseElevationProfile(xmlNodePtr roadElement, RoadInterface* road)
{
  xmlNodePtr elevationProfileElement = GetFirstChildElement(roadElement, TAG::elevationProfile);
  if (elevationProfileElement)
  {
    xmlNodePtr elevationElement = GetFirstChildElement(elevationProfileElement, TAG::elevation);
    while (elevationElement)
    {
      if (xmlStrEqual(elevationElement->name, toXmlChar(TAG::elevation)))
      {
        units::length::meter_t elevationS;
        units::length::meter_t elevationA;
        double elevationB{};
        units::unit_t<units::inverse<units::length::meter>> elevationC;
        units::unit_t<units::inverse<units::squared<units::length::meter>>> elevationD;
        ThrowIfFalse(ParseAttributeDouble(elevationElement, ATTRIBUTE::s, elevationS),
                     elevationElement,
                     "Attribute " + std::string(ATTRIBUTE::s) + " is missing.");

        ThrowIfFalse(ParseAttributeDouble(elevationElement, ATTRIBUTE::a, elevationA),
                     elevationElement,
                     "Attribute " + std::string(ATTRIBUTE::a) + " is missing.");

        ThrowIfFalse(ParseAttributeDouble(elevationElement, ATTRIBUTE::b, elevationB),
                     elevationElement,
                     "Attribute " + std::string(ATTRIBUTE::b) + " is missing.");

        ThrowIfFalse(ParseAttributeDouble(elevationElement, ATTRIBUTE::c, elevationC),
                     elevationElement,
                     "Attribute " + std::string(ATTRIBUTE::c) + " is missing.");

        ThrowIfFalse(ParseAttributeDouble(elevationElement, ATTRIBUTE::d, elevationD),
                     elevationElement,
                     "Attribute " + std::string(ATTRIBUTE::d) + " is missing.");

        road->AddElevation(elevationS, elevationA, elevationB, elevationC, elevationD);
      }
      elevationElement = xmlNextElementSibling(elevationElement);
    }  // if elevations exist
  }    // if elevation profiles exist
}

void SceneryImporter::ParseRoadLinks(xmlNodePtr roadElement, RoadInterface* road)
{
  xmlNodePtr roadLinkElement = GetFirstChildElement(roadElement, TAG::link);
  if (roadLinkElement)
  {
    xmlNodePtr roadLinkItemElement = GetFirstChild(roadLinkElement);
    while (roadLinkItemElement)
    {
      RoadLinkType roadLinkType = RoadLinkType::Undefined;
      RoadLinkElementType roadLinkElementType = RoadLinkElementType::Undefined;
      std::string roadLinkElementId;
      ContactPointType roadLinkContactPoint = ContactPointType::Undefined;

      if (1 == xmlStrEqual(roadLinkItemElement->name, toXmlChar(TAG::predecessor)))
      {
        roadLinkType = RoadLinkType::Predecessor;
      }
      else if (1 == xmlStrEqual(roadLinkItemElement->name, toXmlChar(TAG::successor)))
      {
        roadLinkType = RoadLinkType::Successor;
      }
      else
      {
        LogErrorAndThrow("invalid road link type");
      }

      std::string roadLinkItem_ElementTypeAttribute;
      ThrowIfFalse(ParseAttributeString(roadLinkItemElement, ATTRIBUTE::elementType, roadLinkItem_ElementTypeAttribute),
                   roadLinkItemElement,
                   "Attribute " + std::string(ATTRIBUTE::elementType) + " is missing.");

      if (0 == roadLinkItem_ElementTypeAttribute.compare(ATTRIBUTE::road))
      {
        roadLinkElementType = RoadLinkElementType::Road;
      }
      else if (0 == roadLinkItem_ElementTypeAttribute.compare(ATTRIBUTE::junction))
      {
        roadLinkElementType = RoadLinkElementType::Junction;
      }
      else
      {
        LogErrorAndThrow("invalid road link attribute");
      }

      ThrowIfFalse(ParseAttributeString(roadLinkItemElement, ATTRIBUTE::elementId, roadLinkElementId),
                   roadLinkItemElement,
                   "Attribute " + std::string(ATTRIBUTE::elementId) + " is missing.");

      if (RoadLinkElementType::Road == roadLinkElementType)
      {
        std::string roadLinkItem_ContactPointAttribute;
        ThrowIfFalse(
            ParseAttributeString(roadLinkItemElement, ATTRIBUTE::contactPoint, roadLinkItem_ContactPointAttribute),
            roadLinkItemElement,
            "Attribute " + std::string(ATTRIBUTE::contactPoint) + " is missing.");

        if (0 == roadLinkItem_ContactPointAttribute.compare(ATTRIBUTE::start))
        {
          roadLinkContactPoint = ContactPointType::Start;
        }
        else if (0 == roadLinkItem_ContactPointAttribute.compare(ATTRIBUTE::end))
        {
          roadLinkContactPoint = ContactPointType::End;
        }
        else
        {
          LogErrorAndThrow("invalid road link attribute");
        }
      }

      LOG_INTERN(LogLevel::DebugCore) << "roadlink: " << toString(roadLinkItemElement->name) << " ("
                                      << static_cast<int>(roadLinkElementType) << ", " << roadLinkElementId << ", "
                                      << static_cast<int>(roadLinkContactPoint);

      road->AddLink(roadLinkType, roadLinkElementType, roadLinkElementId, roadLinkContactPoint);

      roadLinkItemElement = xmlNextElementSibling(roadLinkItemElement);  // any type of road link element
    }                                                                    // if road link item exists
  }                                                                      // if road links exist
}

void SceneryImporter::checkRoadSignalBoundaries(const RoadSignalSpecification& signal)
{
  ThrowIfFalse((signal.s >= 0_m && (signal.dynamic == "yes" || signal.dynamic == "no")
                && (signal.orientation == "+" || signal.orientation == "-" || signal.orientation == "none")
                && signal.height >= 0_m && signal.width >= 0_m),
               "Invalid road signal boundaries.");
}

void SceneryImporter::ParseSignals(xmlNodePtr roadElement, RoadInterface* road)
{
  using namespace SimulationCommon;

  xmlNodePtr signalsElement = GetFirstChildElement(roadElement, TAG::Signals);
  if (signalsElement)
  {
    xmlNodePtr signalElement = GetFirstChildElement(signalsElement, TAG::signal);
    if (signalElement)
    {
      while (signalElement)
      {
        if (xmlStrEqual(signalElement->name, toXmlChar(TAG::signal)))
        {
          RoadSignalSpecification signal;

          ThrowIfFalse(ParseAttributeDouble(signalElement, ATTRIBUTE::s, signal.s),
                       signalElement,
                       "Attribute " + std::string(ATTRIBUTE::s) + " is missing.");
          ThrowIfFalse(ParseAttributeDouble(signalElement, ATTRIBUTE::t, signal.t),
                       signalElement,
                       "Attribute " + std::string(ATTRIBUTE::t) + " is missing.");
          ThrowIfFalse(ParseAttributeDouble(signalElement, ATTRIBUTE::zOffset, signal.zOffset),
                       signalElement,
                       "Attribute " + std::string(ATTRIBUTE::zOffset) + " is missing.");
          ThrowIfFalse(ParseAttributeString(signalElement, ATTRIBUTE::id, signal.id),
                       signalElement,
                       "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");
          ThrowIfFalse(ParseAttributeString(signalElement, ATTRIBUTE::dynamic, signal.dynamic, "no"),
                       signalElement,
                       "Attribute " + std::string(ATTRIBUTE::dynamic) + " is missing.");
          ThrowIfFalse(ParseAttributeString(signalElement, ATTRIBUTE::orientation, signal.orientation),
                       signalElement,
                       "Attribute " + std::string(ATTRIBUTE::orientation) + " is missing.");
          ThrowIfFalse(ParseAttributeString(signalElement, ATTRIBUTE::type, signal.type),
                       signalElement,
                       "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");
          ThrowIfFalse(ParseAttributeString(signalElement, ATTRIBUTE::subtype, signal.subtype),
                       signalElement,
                       "Attribute " + std::string(ATTRIBUTE::subtype) + " is missing.");

          //OpenDRIVE 1.6 standard defines subtypes to be "-1". Maps until now use subtype = "".
          if (signal.subtype == "" || signal.subtype == " ")
          {
            LOG_INTERN(LogLevel::Info) << "Encountered signal with empty subtype definition. That is not supported in "
                                          "openDRIVE 1.6 -> interpreting empty subtype as \"-1\"";
            signal.subtype = "-1";
          }
          else if (signal.subtype == "none")
          {
            LOG_INTERN(LogLevel::Warning)
                << R"(Encountered signal with typedefinition "none". Translated that to "-1")";
            signal.subtype = "-1";
          }

          // optional
          std::string signalUnit;
          double signalValue{};
          if (ParseAttributeDouble(signalElement, ATTRIBUTE::value, signalValue))
          {
            signal.value = signalValue;
          }
          ParseAttributeString(signalElement, ATTRIBUTE::unit, signalUnit);
          ParseSignalUnit(signalUnit, signal.unit);
          ParseAttributeDouble(signalElement, ATTRIBUTE::hOffset, signal.hOffset);
          ParseAttributeDouble(signalElement, ATTRIBUTE::pitch, signal.pitch);
          ParseAttributeDouble(signalElement, ATTRIBUTE::roll, signal.roll);
          ParseAttributeString(signalElement, ATTRIBUTE::name, signal.name);
          ParseAttributeDouble(signalElement, ATTRIBUTE::height, signal.height);
          ParseAttributeDouble(signalElement, ATTRIBUTE::width, signal.width);
          ParseAttributeString(signalElement, ATTRIBUTE::text, signal.text);
          ParseAttributeString(signalElement, ATTRIBUTE::country, signal.country);
          ParseAttributeString(signalElement, ATTRIBUTE::countryRevision, signal.countryRevision);

          // check validity subtag
          ParseElementValidity(signalElement, signal.validity);

          // check other parameters
          checkRoadSignalBoundaries(signal);

          // check dependencies
          xmlNodePtr dependencyElement = GetFirstChildElement(signalElement, TAG::dependency);
          if (dependencyElement)
          {
            std::string dependencyId{};

            while (dependencyElement)
            {
              if (xmlStrEqual(dependencyElement->name, toXmlChar(TAG::dependency)))
              {
                ThrowIfFalse(ParseAttributeString(dependencyElement, ATTRIBUTE::id, dependencyId),
                             dependencyElement,
                             "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");
                signal.dependencyIds.push_back(dependencyId);
              }
              dependencyElement = xmlNextElementSibling(dependencyElement);
            }
          }

          road->AddRoadSignal(signal);
        }
        signalElement = xmlNextElementSibling(signalElement);
      }
    }
  }
}

void SceneryImporter::ParseObjects(xmlNodePtr roadElement, RoadInterface* road)
{
  using namespace SimulationCommon;

  xmlNodePtr objectsElement = GetFirstChildElement(roadElement, TAG::objects);
  if (objectsElement)
  {
    xmlNodePtr objectElement = GetFirstChildElement(objectsElement, TAG::object);
    if (objectElement)
    {
      while (objectElement)
      {
        if (xmlStrEqual(objectElement->name, toXmlChar(TAG::object)))
        {
          ParseObject(objectElement, road);
        }
        objectElement = xmlNextElementSibling(objectElement);
      }
    }
  }
}

void SceneryImporter::ParseObject(xmlNodePtr objectElement, RoadInterface* road)
{
  using namespace SimulationCommon;

  RoadObjectSpecification object;

  ThrowIfFalse(ParseAttributeString(objectElement, ATTRIBUTE::id, object.id),
               objectElement,
               "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");
  ThrowIfFalse(ParseAttributeDouble(objectElement, ATTRIBUTE::s, object.s),
               objectElement,
               "Attribute " + std::string(ATTRIBUTE::s) + " is missing.");
  ThrowIfFalse(ParseAttributeDouble(objectElement, ATTRIBUTE::t, object.t),
               objectElement,
               "Attribute " + std::string(ATTRIBUTE::t) + " is missing.");
  ThrowIfFalse(ParseAttributeDouble(objectElement, ATTRIBUTE::zOffset, object.zOffset),
               objectElement,
               "Attribute " + std::string(ATTRIBUTE::zOffset) + " is missing.");
  // optional
  ParseAttributeType(objectElement, ATTRIBUTE::type, object.type);
  ParseAttributeString(objectElement, ATTRIBUTE::name, object.name);
  ParseAttributeDouble(objectElement, ATTRIBUTE::validLength, object.validLength);
  ParseAttributeType(objectElement, ATTRIBUTE::orientation, object.orientation);
  ParseAttributeDouble(objectElement, ATTRIBUTE::width, object.width);
  ParseAttributeDouble(objectElement, ATTRIBUTE::length, object.length);
  ParseAttributeDouble(objectElement, ATTRIBUTE::height, object.height);
  ParseAttributeDouble(objectElement, ATTRIBUTE::hdg, object.hdg);
  ParseAttributeDouble(objectElement, ATTRIBUTE::pitch, object.pitch);
  ParseAttributeDouble(objectElement, ATTRIBUTE::roll, object.roll);
  ParseAttributeDouble(objectElement, "radius", object.radius);

  ThrowIfFalse(
      object.checkStandardCompliance(), objectElement, "limits of object are not valid for OpenDRIVE standard");

  if (object.radius > 0_m)
  {
    ConvertRadius(object);
  }

  if (object.checkSimulatorCompliance())
  {
    ParseElementValidity(objectElement, object.validity);
    auto objects = ParseObjectRepeat(objectElement, object);
    AddParsedObjectsToRoad(objects, road);
  }
  else
  {
    LOG_INTERN(LogLevel::Warning) << "Limits of object " << object.name << "with id: " << object.id
                                  << " are not valid for the simulation. The Object will be ignored.";
  }
}

void SceneryImporter::ConvertRadius(RoadObjectSpecification& object)
{
  object.width = 2.0 * object.radius;
  object.length = 2.0 * object.radius;
  object.radius = 0.0_m;
}

void SceneryImporter::AddParsedObjectsToRoad(const std::vector<RoadObjectSpecification>& parsedObjects,
                                             RoadInterface* road)
{
  for (const auto& object : parsedObjects)
  {
    road->AddRoadObject(object);
  }
}

std::vector<RoadObjectSpecification> SceneryImporter::ParseObjectRepeat(xmlNodePtr objectElement,
                                                                        const RoadObjectSpecification& object)
{
  using namespace SimulationCommon;

  std::vector<RoadObjectSpecification> objectRepetitions;

  xmlNodePtr repeatElement = GetFirstChildElement(objectElement, TAG::repeat);
  if (repeatElement)
  {
    while (repeatElement)
    {
      if (xmlStrEqual(repeatElement->name, toXmlChar(TAG::repeat)))
      {
        ParseRepeat(repeatElement, object, objectRepetitions);
      }
      repeatElement = xmlNextElementSibling(repeatElement);
    }
  }
  else
  {
    objectRepetitions.push_back(object);
  }
  return objectRepetitions;
}

void SceneryImporter::ParseOptionalInterval(xmlNodePtr repeatElement,
                                            const std::string& startAttribute,
                                            const std::string& endAttribute,
                                            OptionalInterval& interval)
{
  bool checkStartIsSet = ParseAttributeDouble(repeatElement, startAttribute, interval.start);
  bool checkEndIsSet = ParseAttributeDouble(repeatElement, endAttribute, interval.end);

  if (checkStartIsSet && checkEndIsSet)
  {
    interval.isSet = true;
    return;
  }
  ThrowIfFalse(!checkStartIsSet && !checkEndIsSet, repeatElement, "Missing intervall parameter in scenery import");
  return;
}

void SceneryImporter::ParseRepeat(xmlNodePtr repeatElement,
                                  RoadObjectSpecification object,
                                  std::vector<RoadObjectSpecification>& objectRepitions)
{
  using namespace SimulationCommon;
  ObjectRepeat repeat;

  ThrowIfFalse(ParseAttributeDouble(repeatElement, ATTRIBUTE::s, repeat.s),
               repeatElement,
               "Attribute " + std::string(ATTRIBUTE::s) + " is missing.");
  ThrowIfFalse(ParseAttributeDouble(repeatElement, ATTRIBUTE::length, repeat.length),
               repeatElement,
               "Attribute " + std::string(ATTRIBUTE::length) + " is missing.");
  ThrowIfFalse(ParseAttributeDouble(repeatElement, ATTRIBUTE::distance, repeat.distance),
               repeatElement,
               "Attribute " + std::string(ATTRIBUTE::distance) + " is missing.");

  ParseOptionalInterval(repeatElement, "tStart", "tEnd", repeat.t);
  ParseOptionalInterval(repeatElement, "widthStart", "widthEnd", repeat.width);
  ParseOptionalInterval(repeatElement, "heightStart", "heightEnd", repeat.height);
  ParseOptionalInterval(repeatElement, "zOffsetStart", "zOffsetEnd", repeat.zOffset);

  ThrowIfFalse(repeat.checkLimits(), repeatElement, "Invalid limits.");

  return ApplyRepeat(repeat, std::move(object), objectRepitions);
}

void SceneryImporter::ApplyRepeat(ObjectRepeat repeat,
                                  RoadObjectSpecification object,
                                  std::vector<RoadObjectSpecification>& objectRepitions)
{
  const auto isRepeating = (repeat.distance == 0_m);

  if (isRepeating)
  {
    object.length = repeat.length;
  }
  size_t objectCount = isRepeating ? 1 : static_cast<size_t>(repeat.length / repeat.distance);

  std::vector<units::length::meter_t> interpolatedT, interpolatedHeight, interpolatedWidth, interpolatedZOffset;

  if (repeat.t.isSet)
  {
    interpolatedT = CommonHelper::InterpolateLinear(repeat.t.start, repeat.t.end, objectCount);
  }
  if (repeat.height.isSet)
  {
    interpolatedHeight = CommonHelper::InterpolateLinear(repeat.height.start, repeat.height.end, objectCount);
  }
  if (repeat.width.isSet)
  {
    interpolatedWidth = CommonHelper::InterpolateLinear(repeat.width.start, repeat.width.end, objectCount);
  }
  if (repeat.zOffset.isSet)
  {
    interpolatedZOffset = CommonHelper::InterpolateLinear(repeat.zOffset.start, repeat.zOffset.end, objectCount);
  }

  for (size_t i = 0; i < objectCount; i++)
  {
    RoadObjectSpecification repeatingObject = object;
    repeatingObject.s = repeat.s + (i * repeat.distance);

    if (repeat.t.isSet)
    {
      repeatingObject.t = interpolatedT.at(i);
    }
    if (repeat.height.isSet)
    {
      repeatingObject.height = interpolatedHeight.at(i);
    }
    if (repeat.width.isSet)
    {
      repeatingObject.width = interpolatedWidth.at(i);
    }
    if (repeat.zOffset.isSet)
    {
      repeatingObject.zOffset = interpolatedZOffset.at(i);
    }
    repeatingObject.continuous = (repeat.distance == 0_m);

    ThrowIfFalse(repeatingObject.checkStandardCompliance(), "Standard compliance invalid.");

    ThrowIfFalse(repeatingObject.checkSimulatorCompliance(),
                 "Limits of repeating object are not valid for the simulation. The Object will be ignored.");

    objectRepitions.push_back(repeatingObject);
  }
}

void SceneryImporter::ParseElementValidity(xmlNodePtr rootElement, RoadElementValidity& validity)
{
  using namespace SimulationCommon;

  xmlNodePtr validityElement = GetFirstChildElement(rootElement, TAG::validity);
  if (validityElement)
  {
    int fromLane{};
    int toLane{};

    ThrowIfFalse(ParseAttributeInt(validityElement, ATTRIBUTE::fromLane, fromLane),
                 validityElement,
                 "Attribute " + std::string(ATTRIBUTE::fromLane) + " is missing.");
    ThrowIfFalse(ParseAttributeInt(validityElement, ATTRIBUTE::toLane, toLane),
                 validityElement,
                 "Attribute " + std::string(ATTRIBUTE::toLane) + " is missing.");

    if (fromLane > toLane)
    {
      std::swap(fromLane, toLane);
    }

    for (int laneId = fromLane; laneId <= toLane; laneId++)
    {
      validity.lanes.push_back(laneId);
    }
  }
  else
  {
    validity.all = true;
  }
}

void SceneryImporter::ParseRoadTypes(xmlNodePtr roadElement, RoadInterface* road)
{
  using namespace SimulationCommon;

  xmlNodePtr typeElement = GetFirstChildElement(roadElement, TAG::type);
  if (typeElement)
  {
    while (typeElement)
    {
      if (xmlStrEqual(typeElement->name, toXmlChar(TAG::type)))
      {
        RoadTypeSpecification roadType;
        ThrowIfFalse(ParseAttributeDouble(typeElement, ATTRIBUTE::s, roadType.s),
                     typeElement,
                     "Attribute " + std::string(ATTRIBUTE::s) + " is missing.");
        std::string roadTypeStr;
        ThrowIfFalse(ParseAttributeString(typeElement, ATTRIBUTE::type, roadTypeStr),
                     typeElement,
                     "Attribute " + std::string(ATTRIBUTE::type) + " is missing.");

        roadType.roadType = roadTypeConversionMap.at(roadTypeStr);

        road->AddRoadType(roadType);
      }

      typeElement = xmlNextElementSibling(typeElement);
    }
  }
}

void SceneryImporter::ParseSignalUnit(const std::string& element, RoadSignalUnit& signalUnit)
{
  // empty is a valid state!
  if (element.empty())
  {
    signalUnit = RoadSignalUnit::Undefined;
  }
  else
  {
    signalUnit = roadSignalUnitConversionMap.at(element);
  }
}

void SceneryImporter::ParseRoadLanes(xmlNodePtr roadElement, RoadInterface* road)
{
  xmlNodePtr roadLanesElement = GetFirstChildElement(roadElement, TAG::lanes);
  ThrowIfFalse(roadLanesElement, roadElement, "Tag + " + std::string(TAG::lanes) + " is missing.");

  // parse lane offsets
  xmlNodePtr laneOffsetElement = GetFirstChildElement(roadLanesElement, TAG::laneOffset);
  if (laneOffsetElement)
  {
    while (laneOffsetElement)
    {
      if (xmlStrEqual(laneOffsetElement->name, toXmlChar(TAG::laneOffset)))
      {
        units::length::meter_t laneOffsetS;
        units::length::meter_t laneOffsetA;
        double laneOffsetB{};
        units::unit_t<units::inverse<units::length::meter>> laneOffsetC;
        units::unit_t<units::inverse<units::squared<units::length::meter>>> laneOffsetD;
        ThrowIfFalse(ParseAttributeDouble(laneOffsetElement, ATTRIBUTE::s, laneOffsetS, {0.0_m}),
                     laneOffsetElement,
                     "Attribute " + std::string(ATTRIBUTE::s) + " is missing.");

        ThrowIfFalse(ParseAttributeDouble(laneOffsetElement, ATTRIBUTE::a, laneOffsetA, {0.0_m}),
                     laneOffsetElement,
                     "Attribute " + std::string(ATTRIBUTE::a) + " is missing.");

        ThrowIfFalse(ParseAttributeDouble(laneOffsetElement, ATTRIBUTE::b, laneOffsetB, 0.0),
                     laneOffsetElement,
                     "Attribute " + std::string(ATTRIBUTE::b) + " is missing.");

        ThrowIfFalse(ParseAttributeDouble(laneOffsetElement,
                                          ATTRIBUTE::c,
                                          laneOffsetC,
                                          {units::unit_t<units::inverse<units::length::meter>>(0.0)}),
                     laneOffsetElement,
                     "Attribute " + std::string(ATTRIBUTE::c) + " is missing.");

        ThrowIfFalse(ParseAttributeDouble(laneOffsetElement,
                                          ATTRIBUTE::d,
                                          laneOffsetD,
                                          {units::unit_t<units::inverse<units::squared<units::length::meter>>>(0.0)}),
                     laneOffsetElement,
                     "Attribute " + std::string(ATTRIBUTE::d) + " is missing.");

        road->AddLaneOffset(laneOffsetS, laneOffsetA, laneOffsetB, laneOffsetC, laneOffsetD);
      }

      laneOffsetElement = xmlNextElementSibling(laneOffsetElement);
    }  // laneOffset loop
  }    // if laneOffsets exist

  // parse lane sections
  xmlNodePtr roadLaneSectionElement = GetFirstChildElement(roadLanesElement, TAG::laneSection);
  ThrowIfFalse(roadLaneSectionElement, roadLanesElement, "Tag " + std::string(TAG::laneSection) + " is missing.");
  while (roadLaneSectionElement)
  {
    if (xmlStrEqual(roadLaneSectionElement->name, toXmlChar(TAG::laneSection)))
    {
      units::length::meter_t roadLaneSectionStart;
      ThrowIfFalse(ParseAttributeDouble(roadLaneSectionElement, ATTRIBUTE::s, roadLaneSectionStart),
                   roadLaneSectionElement,
                   "Attribute " + std::string(ATTRIBUTE::s) + " is missing.");

      // add OpenDRIVE lane section in ascending order
      road->AddRoadLaneSection(roadLaneSectionStart);
      auto* laneSection = road->GetLaneSections().back().get();
      ThrowIfFalse(laneSection != nullptr, roadLanesElement, "Could not add Section");

      LOG_INTERN(LogLevel::DebugCore) << "lane section (s=" << roadLaneSectionStart << ")";

      // left lanes
      xmlNodePtr roadLaneSectionLeftSideElement = GetFirstChildElement(roadLaneSectionElement, TAG::left);

      if (roadLaneSectionLeftSideElement)
      {
        ParseLanes(roadLaneSectionLeftSideElement, laneSection);
      }

      // center lanes
      xmlNodePtr roadLaneSectionCenterSideElement = GetFirstChildElement(roadLaneSectionElement, TAG::center);
      if (roadLaneSectionCenterSideElement)
      {
        ParseLanes(roadLaneSectionCenterSideElement, laneSection);
      }

      // right lanes
      xmlNodePtr roadLaneSectionRightSideElement = GetFirstChildElement(roadLaneSectionElement, TAG::right);
      if (roadLaneSectionRightSideElement)
      {
        ParseLanes(roadLaneSectionRightSideElement, laneSection);
      }
    }
    roadLaneSectionElement = xmlNextElementSibling(roadLaneSectionElement);
  }  // road lane section loop
}

void SceneryImporter::ParseJunctionConnections(xmlNodePtr junctionElement, JunctionInterface* junction)
{
  xmlNodePtr connectionElement = GetFirstChildElement(junctionElement, TAG::connection);
  ThrowIfFalse(connectionElement, junctionElement, "Tag " + std::string(TAG::connection) + " is missing.");
  while (connectionElement)
  {
    if (xmlStrEqual(connectionElement->name, toXmlChar(TAG::connection)))
    {
      std::string id;
      ThrowIfFalse(ParseAttributeString(connectionElement, ATTRIBUTE::id, id),
                   connectionElement,
                   "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");
      std::string incomingRoad;
      ThrowIfFalse(ParseAttributeString(connectionElement, ATTRIBUTE::incomingRoad, incomingRoad),
                   connectionElement,
                   "Attribute " + std::string(ATTRIBUTE::incomingRoad) + " is missing.");
      std::string connectingRoad;
      ThrowIfFalse(ParseAttributeString(connectionElement, ATTRIBUTE::connectingRoad, connectingRoad),
                   connectionElement,
                   "Attribute " + std::string(ATTRIBUTE::connectingRoad) + " is missing.");
      std::string contactPoint;
      ThrowIfFalse(ParseAttributeString(connectionElement, ATTRIBUTE::contactPoint, contactPoint),
                   connectionElement,
                   "Attribute " + std::string(ATTRIBUTE::contactPoint) + " is missing.");

      ContactPointType roadLinkContactPoint = ContactPointType::Undefined;

      if (0 == contactPoint.compare("start"))
      {
        roadLinkContactPoint = ContactPointType::Start;
      }
      else if (0 == contactPoint.compare("end"))
      {
        roadLinkContactPoint = ContactPointType::End;
      }

      ConnectionInterface* connection = junction->AddConnection(id, incomingRoad, connectingRoad, roadLinkContactPoint);

      ParseJunctionConnectionLinks(connectionElement, connection);
    }
    connectionElement = xmlNextElementSibling(connectionElement);
  }
}

void SceneryImporter::ParseJunctionConnectionLinks(xmlNodePtr connectionElement, ConnectionInterface* connection)
{
  xmlNodePtr linkElement = GetFirstChildElement(connectionElement, TAG::laneLink);
  if (linkElement)
  {
    while (linkElement)
    {
      if (xmlStrEqual(linkElement->name, toXmlChar(TAG::laneLink)))
      {
        int from{};
        ThrowIfFalse(ParseAttributeInt(linkElement, ATTRIBUTE::from, from),
                     linkElement,
                     "Attribute " + std::string(ATTRIBUTE::from) + " is missing.");

        int to{};
        ThrowIfFalse(ParseAttributeInt(linkElement, ATTRIBUTE::to, to),
                     linkElement,
                     "Attribute " + std::string(ATTRIBUTE::to) + " is missing.");
        connection->AddLink(from, to);
      }

      linkElement = xmlNextElementSibling(linkElement);
    }
  }
}

void SceneryImporter::ParseJunctionPriorities(xmlNodePtr junctionElement, JunctionInterface* junction)
{
  xmlNodePtr priorityElement = GetFirstChildElement(junctionElement, TAG::priority);
  if (priorityElement)
  {
    while (priorityElement)
    {
      if (xmlStrEqual(priorityElement->name, toXmlChar(TAG::priority)))
      {
        Priority priority;
        ThrowIfFalse(ParseAttributeString(priorityElement, ATTRIBUTE::high, priority.high),
                     priorityElement,
                     "Attribute " + std::string(ATTRIBUTE::high) + " is missing.");
        ThrowIfFalse(ParseAttributeString(priorityElement, ATTRIBUTE::low, priority.low),
                     priorityElement,
                     "Attribute " + std::string(ATTRIBUTE::low) + " is missing.");

        junction->AddPriority(priority);
      }

      priorityElement = xmlNextElementSibling(priorityElement);
    }
  }
}

void SceneryImporter::ParseRoads(xmlNodePtr documentRoot, Scenery* scenery)
{
  xmlNodePtr roadElement = GetFirstChildElement(documentRoot, TAG::road);
  ThrowIfFalse(roadElement, documentRoot, "Tag " + std::string(TAG::road) + " is missing.");
  while (roadElement)
  {
    if (xmlStrEqual(roadElement->name, toXmlChar(TAG::road)))
    {
      std::string id;
      ThrowIfFalse(ParseAttributeString(roadElement, ATTRIBUTE::id, id),
                   roadElement,
                   "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");

      double length{};
      ThrowIfFalse(ParseAttributeDouble(roadElement, ATTRIBUTE::length, length),
                   roadElement,
                   "Attribute " + std::string(ATTRIBUTE::length) + " is missing.");

      RoadInterface* road = scenery->AddRoad(id, length);
      ThrowIfFalse(road != nullptr, roadElement, "Could not add Road");

      std::string junctionId;
      if (!ParseAttributeString(roadElement, ATTRIBUTE::junction, junctionId))
      {
        junctionId = "-1";
      }
      road->SetJunctionId(junctionId);

      LOG_INTERN(LogLevel::DebugCore) << "road: id: " << id;

      ParseGeometries(roadElement, road);

      ParseElevationProfile(roadElement, road);

      ParseRoadLinks(roadElement, road);

      ParseRoadLanes(roadElement, road);  // parsing laneOffset is included here

      ParseObjects(roadElement, road);

      ParseSignals(roadElement, road);

      ParseRoadTypes(roadElement, road);
    }

    roadElement = xmlNextElementSibling(roadElement);
  }  // road loop
}

void SceneryImporter::ParseJunctions(xmlNodePtr documentRoot, Scenery* scenery)
{
  xmlNodePtr junctionElement = GetFirstChildElement(documentRoot, TAG::junction);
  if (junctionElement)
  {
    while (junctionElement)
    {
      if (xmlStrEqual(junctionElement->name, toXmlChar(TAG::junction)))
      {
        std::string id;
        ThrowIfFalse(ParseAttributeString(junctionElement, ATTRIBUTE::id, id),
                     junctionElement,
                     "Attribute " + std::string(ATTRIBUTE::id) + " is missing.");

        JunctionInterface* junction = scenery->AddJunction(id);

        ParseJunctionConnections(junctionElement, junction);
        ParseJunctionPriorities(junctionElement, junction);
      }

      junctionElement = xmlNextElementSibling(junctionElement);
    }
  }
}

void SceneryImporter::ParseGeoReference(xmlNodePtr headerElement, Scenery* scenery)
{
  xmlNodePtr geoReferenceElement = GetFirstChildElement(headerElement, TAG::geoReference);
  if (geoReferenceElement)
  {
    xmlChar* content = xmlNodeGetContent(geoReferenceElement);
    std::string str(reinterpret_cast<const char*>(content));  // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
    std::string cleanStr = std::regex_replace(str, std::regex("^( |\\n)+|( |\\n)+$"), "");
    scenery->SetGeoReference(cleanStr);
  }
}

void SceneryImporter::ParseHeader(xmlNodePtr documentRoot, Scenery* scenery)
{
  xmlNodePtr headerElement = GetFirstChildElement(documentRoot, TAG::header);
  if (headerElement)
  {
    ParseGeoReference(headerElement, scenery);
  }
}

bool SceneryImporter::Import(const std::string& filename, Scenery* scenery)
{
  try
  {
    xmlInitParser();

    xmlDocPtr doc
        = xmlReadFile(filename.c_str(),
                      NULL,  // NOLINT(modernize-use-nullptr)
                             // https://gnome.pages.gitlab.gnome.org/libxml2/devhelp/libxml2-parser.html#xmlReadFile
                      0);
    ThrowIfFalse(doc != nullptr,
                 "an error occurred during scenery import. Invalid xml file format of file " + filename);

    xmlNodePtr documentRoot = xmlDocGetRootElement(doc);
    if (documentRoot == nullptr)
    {
      return false;
    }

    // parse header
    ParseHeader(documentRoot, scenery);

    // parse junctions
    ParseJunctions(documentRoot, scenery);

    // parse roads
    ParseRoads(documentRoot, scenery);

    xmlFreeDoc(doc);
    xmlCleanupParser();

    return true;
  }
  catch (std::runtime_error& e)
  {
    LOG_INTERN(LogLevel::Error) << "Scenery import failed: " + std::string(e.what());
    return false;
  }
}

}  // namespace Importer
