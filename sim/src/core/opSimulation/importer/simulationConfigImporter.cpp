/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2018 ITK Engineering GmbH
 *               2018-2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  simulationConfigImporter.cpp */
//-----------------------------------------------------------------------------

#include "simulationConfigImporter.h"

#include <algorithm>
#include <cstdint>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <locale>
#include <optional>
#include <ostream>
#include <stdexcept>
#include <unordered_map>
#include <utility>

#include "common/log.h"
#include "common/observationLibraryDefinitions.h"
#include "common/parameter.h"
#include "common/spawnPointLibraryDefinitions.h"
#include "common/xmlParser.h"
#include "framework/directories.h"
#include "importer/importerCommon.h"
#include "importer/importerLoggingHelper.h"
#include "importer/parameterImporter.h"
#include "importer/simulationConfig.h"
#include "modelElements/parameters.h"

namespace TAG = openpass::importer::xml::simulationConfigImporter::tag;
namespace ATTRIBUTE = openpass::importer::xml::simulationConfigImporter::attribute;

using namespace Importer;
using namespace SimulationCommon;

std::string SimulationConfigImporter::GetLibrary(xmlNodePtr root,
                                                 const std::string& tag,
                                                 const std::string& defaultValue)
{
  std::string parsedName;
  if (!Parse(root, tag, parsedName))
  {
    LOG_INTERN(LogLevel::Info) << "Library " + tag << " undefined, falling back to default value " + defaultValue;
    return defaultValue;
  }
  return parsedName;
}

ExperimentConfig::Libraries SimulationConfigImporter::ImportLibraries(xmlNodePtr rootElement)
{
  xmlNodePtr libsRoot = GetFirstChildElement(rootElement, TAG::libraries);
  if (!libsRoot)
  {
    LOG_INTERN(LogLevel::Warning) << "No libraries found. Falling back to default values";
    return defaultLibraryMapping;
  }

  ExperimentConfig::Libraries libs;
  for (const auto& [tagName, defaultValue] : defaultLibraryMapping)
  {
    libs.try_emplace(tagName, SimulationConfigImporter::GetLibrary(libsRoot, tagName, defaultValue));
  }
  return libs;
}

void SimulationConfigImporter::ImportExperiment(xmlNodePtr experimentElement,
                                                Configuration::SimulationConfig& simulationConfig)
{
  ExperimentConfig experimentConfig;

  ThrowIfFalse(ParseInt(experimentElement, "ExperimentID", experimentConfig.experimentId),
               experimentElement,
               "ExperimentID not valid.");

  ThrowIfFalse(ParseInt(experimentElement, "NumberOfInvocations", experimentConfig.numberOfInvocations),
               experimentElement,
               "NumberOfInvocations not valid.");

  std::uint64_t randomSeed = 0;
  ThrowIfFalse(ParseULong(experimentElement, "RandomSeed", randomSeed), experimentElement, "RandomSeed not valid.");

  experimentConfig.randomSeed = static_cast<std::uint32_t>(randomSeed);

  experimentConfig.libraries = ImportLibraries(experimentElement);

  simulationConfig.SetExperimentConfig(experimentConfig);
}

void SimulationConfigImporter::ImportScenario(xmlNodePtr scenarioElement,
                                              const std::string& configurationDir,
                                              Configuration::SimulationConfig& simulationConfig)
{
  ScenarioConfig scenarioConfig;
  std::string scenarioFilename;
  ThrowIfFalse(ParseString(scenarioElement, "OpenScenarioFile", scenarioFilename),
               scenarioElement,
               "OpenScenarioFile not valid.");

  scenarioConfig.scenarioPath = openpass::core::Directories::Concat(configurationDir, scenarioFilename);
  simulationConfig.SetScenarioConfig(scenarioConfig);
}

void SimulationConfigImporter::ImportEnvironment(xmlNodePtr environmentElement,
                                                 Configuration::SimulationConfig& simulationConfig)
{
  EnvironmentConfig environmentConfig;
  //Parse all time of days
  xmlNodePtr timeOfDaysElement = GetFirstChildElement(environmentElement, TAG::timeOfDays);
  ThrowIfFalse(timeOfDaysElement, environmentElement, "Tag " + std::string(TAG::timeOfDays) + " is missing.");
  ThrowIfFalse(
      ImportProbabilityMap(timeOfDaysElement, "Value", TAG::timeOfDay, environmentConfig.timeOfDays, LogErrorAndThrow),
      timeOfDaysElement,
      "Could not import Probabilities.");

  //Parse all visibility distances
  xmlNodePtr visibilityDistancesElement = GetFirstChildElement(environmentElement, TAG::visibilityDistances);
  ThrowIfFalse(
      visibilityDistancesElement, environmentElement, "Tag " + std::string(TAG::visibilityDistances) + " is missing.");
  ThrowIfFalse(ImportProbabilityMap(visibilityDistancesElement,
                                    "Value",
                                    TAG::visibilityDistance,
                                    environmentConfig.visibilityDistances,
                                    LogErrorAndThrow),
               visibilityDistancesElement,
               "Could not import Probabilities.");

  //Parse all frictions
  xmlNodePtr frictionsElement = GetFirstChildElement(environmentElement, TAG::frictions);
  ThrowIfFalse(frictionsElement, environmentElement, "Tag " + std::string(TAG::frictions) + " is missing.");
  ThrowIfFalse(
      ImportProbabilityMap(frictionsElement, "Value", TAG::friction, environmentConfig.frictions, LogErrorAndThrow),
      frictionsElement,
      "Could not import Probabilities.");

  //Parse all weathers
  xmlNodePtr weathersElement = GetFirstChildElement(environmentElement, TAG::weathers);
  ThrowIfFalse(weathersElement, environmentElement, "Tag " + std::string(TAG::weathers) + " is missing.");
  ThrowIfFalse(
      ImportProbabilityMap(weathersElement, "Value", TAG::weather, environmentConfig.weathers, LogErrorAndThrow),
      weathersElement,
      "Could not import Probabilities.");

  //Parse traffic rules
  ThrowIfFalse(ParseString(environmentElement, TAG::trafficRules, environmentConfig.trafficRules),
               environmentElement,
               "Tag " + std::string(TAG::trafficRules) + " is missing.");

  xmlNodePtr turningRatesElement = GetFirstChildElement(environmentElement, TAG::turningRates);
  if (turningRatesElement)
  {
    ImportTurningRates(turningRatesElement, environmentConfig.turningRates);
  }
  simulationConfig.SetEnvironmentConfig(environmentConfig);
}

void SimulationConfigImporter::ImportSpawners(xmlNodePtr spawnersElement,
                                              Configuration::SimulationConfig& simulationConfig)
{
  xmlNodePtr spawnPointElement = GetFirstChildElement(spawnersElement, TAG::spawner);

  while (spawnPointElement)
  {
    if (xmlStrEqual(spawnPointElement->name, toXmlChar(TAG::spawner)))
    {
      SpawnPointLibraryInfo spawnPointInfo;

      ThrowIfFalse(ParseString(spawnPointElement, TAG::library, spawnPointInfo.libraryName),
                   spawnPointElement,
                   "Tag " + std::string(TAG::library) + " is missing.");

      std::string type;
      ThrowIfFalse(ParseString(spawnPointElement, TAG::type, type),
                   spawnPointElement,
                   "Tag " + std::string(TAG::type) + " is missing.");
      const auto& spawnPointTypeIter
          = std::find_if(spawnPointTypeMapping.cbegin(),
                         spawnPointTypeMapping.cend(),
                         [&type](const auto& spawnPointTypePair) -> bool { return spawnPointTypePair.second == type; });
      ThrowIfFalse(spawnPointTypeIter != spawnPointTypeMapping.cend(), spawnPointElement, "SpawnPoint Type invalid");
      spawnPointInfo.type = spawnPointTypeIter->first;

      ThrowIfFalse(ParseInt(spawnPointElement, TAG::priority, spawnPointInfo.priority),
                   spawnPointElement,
                   "Tag " + std::string(TAG::priority) + " is missing.");

      std::string spawnPointProfile;
      if (ParseString(spawnPointElement, "Profile", spawnPointProfile))
      {
        spawnPointInfo.profileName.emplace(spawnPointProfile);
      }

      simulationConfig.AddSpawnPointInfo(spawnPointInfo);
    }

    spawnPointElement = xmlNextElementSibling(spawnPointElement);
  }
}

void SimulationConfigImporter::ImportObservations(xmlNodePtr observationsElement,
                                                  Configuration::SimulationConfig& simulationConfig)
{
  xmlNodePtr observationElement = GetFirstChildElement(observationsElement, TAG::observation);
  ThrowIfFalse(observationElement, observationsElement, "Tag " + std::string(TAG::observation) + " is missing.");

  int id = 0;

  while (observationElement)
  {
    if (xmlStrEqual(observationElement->name, toXmlChar(TAG::observation)))
    {
      ObservationInstance observationInstance;
      observationInstance.id = id;
      ++id;

      ThrowIfFalse(ParseString(observationElement, TAG::library, observationInstance.libraryName),
                   observationElement,
                   "Tag " + std::string(TAG::library) + " is missing.");

      xmlNodePtr parametersElement = GetFirstChildElement(observationElement, TAG::parameters);
      ThrowIfFalse(parametersElement, observationsElement, "Tag " + std::string(TAG::parameters) + " is missing.");
      observationInstance.parameters = openpass::parameter::Import(parametersElement, observationsElement);

      simulationConfig.AddObservationInstance(observationInstance);
    }

    observationElement = xmlNextElementSibling(observationElement);
  }
}

void SimulationConfigImporter::ImportTurningRates(xmlNodePtr turningRatesElement, TurningRates& turningRates)
{
  xmlNodePtr turningRateElement = GetFirstChildElement(turningRatesElement, TAG::turningRate);
  ThrowIfFalse(turningRateElement, turningRatesElement, "Tag " + std::string(TAG::turningRate) + " is missing.");

  while (turningRateElement)
  {
    if (xmlStrEqual(turningRateElement->name, toXmlChar(TAG::turningRate)))
    {
      TurningRate turnigRate;

      ThrowIfFalse(ParseAttribute(turningRateElement, ATTRIBUTE::incoming, turnigRate.incoming),
                   turningRateElement,
                   "Attribute " + std::string(ATTRIBUTE::incoming) + " is missing.");
      ThrowIfFalse(ParseAttribute(turningRateElement, ATTRIBUTE::outgoing, turnigRate.outgoing),
                   turningRateElement,
                   "Attribute " + std::string(ATTRIBUTE::outgoing) + " is missing.");
      ThrowIfFalse(ParseAttribute(turningRateElement, ATTRIBUTE::weight, turnigRate.weight),
                   turningRateElement,
                   "Attribute " + std::string(ATTRIBUTE::weight) + " is missing.");

      turningRates.emplace_back(turnigRate);
    }

    turningRateElement = xmlNextElementSibling(turningRateElement);
  }
}

bool SimulationConfigImporter::Import(const std::string& configurationDir,
                                      const std::string& simulationConfigFile,
                                      Configuration::SimulationConfig& simulationConfig,
                                      const std::filesystem::path& executionPath)
{
  try
  {
    xmlInitParser();

    xmlDocPtr doc
        = xmlReadFile(simulationConfigFile.c_str(),
                      NULL,  // NOLINT(modernize-use-nullptr)
                             // https://gnome.pages.gitlab.gnome.org/libxml2/devhelp/libxml2-parser.html#xmlReadFile
                      0);
    ThrowIfFalse(doc != nullptr,
                 "an error occurred during simulation configuration import. Invalid xml file format of file "
                     + simulationConfigFile);

    xmlNodePtr documentRoot = xmlDocGetRootElement(doc);
    if (documentRoot == nullptr)
    {
      return false;
    }

    ImporterCommon::validateXMLSchema(simulationConfigFile, "simulationConfig.xsd", doc, executionPath);

    std::string configVersion;
    ParseAttributeString(documentRoot, ATTRIBUTE::schemaVersion, configVersion);
    ThrowIfFalse(configVersion.compare(supportedConfigVersion) == 0,
                 "SimulationConfig version not suppored. Supported version is " + std::string(supportedConfigVersion));

    std::string profilesCatalog;
    ThrowIfFalse(ParseString(documentRoot, "ProfilesCatalog", profilesCatalog), "Could not import Proifles Catalog.");

    simulationConfig.SetProfilesCatalog(openpass::core::Directories::Concat(configurationDir, profilesCatalog));

    xmlNodePtr experimentConfigElement = GetFirstChildElement(documentRoot, TAG::experiment);
    ThrowIfFalse(experimentConfigElement, "Tag " + std::string(TAG::experiment) + " is missing.");
    ImportExperiment(experimentConfigElement, simulationConfig);

    xmlNodePtr scenarioConfigElement = GetFirstChildElement(documentRoot, TAG::scenario);
    ThrowIfFalse(scenarioConfigElement, "Tag " + std::string(TAG::scenario) + " is missing.");
    ImportScenario(scenarioConfigElement, configurationDir, simulationConfig);

    xmlNodePtr environmentConfigElement = GetFirstChildElement(documentRoot, TAG::environment);
    ThrowIfFalse(environmentConfigElement, "Tag " + std::string(TAG::environment) + " is missing.");
    ImportEnvironment(environmentConfigElement, simulationConfig);

    xmlNodePtr observationConfigElement = GetFirstChildElement(documentRoot, TAG::observations);
    ThrowIfFalse(observationConfigElement, "Tag " + std::string(TAG::observations) + " is missing.");
    ImportObservations(observationConfigElement, simulationConfig);

    xmlNodePtr spawnPointsConfigElement = GetFirstChildElement(documentRoot, TAG::spawners);
    ThrowIfFalse(spawnPointsConfigElement, "Tag " + std::string(TAG::spawners) + " is missing.");
    ImportSpawners(spawnPointsConfigElement, simulationConfig);

    xmlFreeDoc(doc);
    xmlCleanupParser();

    return true;
  }
  catch (const std::runtime_error& e)
  {
    LOG_INTERN(LogLevel::Error) << "SimulationConfig import failed: " + std::string(e.what());
    return false;
  }
}
