/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "road.h"

#include <MantleAPI/Common/floating_point_helper.h>
#include <algorithm>
#include <cmath>
#include <limits>
#include <new>
#include <ostream>
#include <utility>

#include "common/log.h"
#include "importer/road/roadObject.h"
#include "importer/road/roadSignal.h"
#include "include/roadInterface/roadElevation.h"
#include "include/roadInterface/roadLaneOffset.h"
#include "include/roadInterface/roadLaneRoadMark.h"
#include "include/roadInterface/roadLaneWidth.h"
#include "include/roadInterface/roadObjectInterface.h"
#include "include/roadInterface/roadSignalInterface.h"

extern "C"
{
  extern int fresnl(double xxa, double *ssa, double *cca);
}

namespace
{
const double SQRT_PI = std::sqrt(M_PI);

}  // namespace

void RoadLane::AddWidth(units::length::meter_t sOffset,
                        units::length::meter_t a,
                        double b,
                        units::unit_t<units::inverse<units::length::meter>> c,
                        units::unit_t<units::inverse<units::squared<units::length::meter>>> d)
{
  widths.emplace_back(std::make_unique<RoadLaneWidth>(sOffset, a, b, c, d));
}

void RoadLane::AddBorder(units::length::meter_t sOffset,
                         units::length::meter_t a,
                         double b,
                         units::unit_t<units::inverse<units::length::meter>> c,
                         units::unit_t<units::inverse<units::squared<units::length::meter>>> d)
{
  borders.emplace_back(std::make_unique<RoadLaneWidth>(sOffset, a, b, c, d));
}

bool RoadLane::AddSuccessor(int id)
{
  ThrowIfFalse(successor.empty(), "added more than one successor to road lane.");
  successor.push_back(id);

  return true;
}

bool RoadLane::AddPredecessor(int id)
{
  ThrowIfFalse(predecessor.empty(), "added more than one predecessor to road lane.");
  predecessor.push_back(id);

  return true;
}

void RoadLane::AddRoadMark(units::length::meter_t sOffset,
                           RoadLaneRoadDescriptionType type,
                           RoadLaneRoadMarkType roadMark,
                           RoadLaneRoadMarkColor color,
                           RoadLaneRoadMarkLaneChange laneChange,
                           RoadLaneRoadMarkWeight weight)
{
  for (const auto &roadMark : roadMarks)
  {
    roadMark->LimitSEnd(sOffset);
  }
  roadMarks.emplace_back(std::make_unique<RoadLaneRoadMark>(sOffset, type, roadMark, color, laneChange, weight));
}

RoadLaneInterface *RoadLaneSection::AddRoadLane(int id, RoadLaneType type)
{
  if (auto lane = lanes.try_emplace(id, std::make_unique<RoadLane>(this, id, type)); lane.second)
  {
    return lane.first->second.get();
  }

  return nullptr;
}

Common::Vector2d<units::length::meter_t> RoadGeometry::GetCoordLine(units::length::meter_t sOffset,
                                                                    units::length::meter_t tOffset) const
{
  if (sOffset > length)
  {
    if (!mantle_api::AlmostEqual(sOffset, length))
    {
      LOG_INTERN(LogLevel::Warning) << "cummulative s-offset exceeds length of line geometry by " << (sOffset - length)
                                    << " m. "
                                    << "Setting sOffset to length.";
    }
    sOffset = length;
  }

  // unrotated road initially pointing to east
  Common::Vector2d<units::length::meter_t> offset(sOffset, tOffset);

  offset.Rotate(hdg);

  offset.x += x;
  offset.y += y;

  return offset;
}

units::angle::radian_t RoadGeometry::GetDirLine([[maybe_unused]] units::length::meter_t sOffset) const
{
  return hdg;
}

Common::Vector2d<units::length::meter_t> RoadGeometry::GetCoordArc(units::length::meter_t sOffset,
                                                                   units::length::meter_t tOffset,
                                                                   units::curvature::inverse_meter_t curvature) const
{
  if (sOffset > length)
  {
    if (!mantle_api::AlmostEqual(sOffset, length))
    {
      LOG_INTERN(LogLevel::Warning) << "cummulative s-offset exceeds length of arc geometry by " << (sOffset - length)
                                    << " m. "
                                    << "Setting sOffset to length.";
    }

    sOffset = length;
  }

  units::length::meter_t radius{1 / curvature};
  units::length::meter_t circumference{2 * M_PI / curvature};

  // account for sOffset beyond circumference
  // fractionRad = fractionCircumference * 2 * PI / circumference
  units::angle::radian_t fractionRad = 1_rad * units::math::fmod(sOffset, circumference) * curvature;

  // shift by radius to rotate around center at origin
  Common::Vector2d<units::length::meter_t> offset(0_m, -radius + tOffset);
  offset.Rotate(fractionRad);

  // shift back
  offset.y += radius;

  offset.Rotate(hdg);

  offset.x += x;
  offset.y += y;

  return offset;
}

units::angle::radian_t RoadGeometry::GetDirArc(units::length::meter_t sOffset,
                                               units::curvature::inverse_meter_t curvature) const
{
  units::length::meter_t circumference = (2 * M_PI) / curvature;

  // account for sOffset beyond circumference
  // fractionRad = fractionCircumference * 2 * PI / circumference
  units::angle::radian_t fractionRad{units::unit_cast<double>(units::math::fmod(sOffset, circumference) * curvature)};

  return hdg + fractionRad;
}

Common::Vector2d<units::length::meter_t> RoadGeometryLine::GetCoord(units::length::meter_t sOffset,
                                                                    units::length::meter_t tOffset) const
{
  return GetCoordLine(sOffset, tOffset);
}

units::angle::radian_t RoadGeometryLine::GetDir(units::length::meter_t sOffset) const
{
  return GetDirLine(sOffset);
}

Common::Vector2d<units::length::meter_t> RoadGeometryArc::GetCoord(units::length::meter_t sOffset,
                                                                   units::length::meter_t tOffset) const
{
  if (0.0_i_m == curvature)
  {
    return GetCoordLine(sOffset, tOffset);
  }

  return GetCoordArc(sOffset, tOffset, curvature);
}

units::angle::radian_t RoadGeometryArc::GetDir(units::length::meter_t sOffset) const
{
  if (0.0_i_m == curvature)
  {
    return GetDirLine(sOffset);
  }

  return GetDirArc(sOffset, curvature);
}

RoadGeometrySpiral::RoadGeometrySpiral(units::length::meter_t s,
                                       units::length::meter_t x,
                                       units::length::meter_t y,
                                       units::angle::radian_t hdg,
                                       units::length::meter_t length,
                                       units::curvature::inverse_meter_t curvStart,
                                       units::curvature::inverse_meter_t curvEnd)
    : RoadGeometry{s, x, y, hdg, length}, c_start{curvStart}, c_end{curvEnd}
{
  if (length != 0.0_m)
  {
    c_dot = (c_end - c_start) / length;
  }
  else
  {
    c_dot = units::unit_t<units::inverse<units::squared<units::length::meter>>>(0.0);
  }

  if (c_dot != units::unit_t<units::inverse<units::squared<units::length::meter>>>(0.0))
  {
    l_start = c_start / c_dot;
  }
  else
  {
    l_start = 0.0_m;
  }

  const auto l_end = l_start + length;
  units::area::square_meter_t rl;  // helper constant R * L

  if (c_start != 0.0_i_m)
  {
    rl = l_start / c_start;
  }
  else if (c_end != 0.0_i_m)
  {
    rl = l_end / c_end;
  }
  else
  {
    t_start = 0.0_rad;
    a = 0.0_m;
    sign = 0.0;
    return;
  }

  t_start = 1_rad * 0.5 * l_start * curvStart;
  a = units::math::sqrt(units::math::abs(rl));
  sign = std::signbit(rl.value()) ? -1.0 : 1.0;
}

Common::Vector2d<units::length::meter_t> RoadGeometrySpiral::FullCoord(units::length::meter_t sOffset,
                                                                       units::length::meter_t tOffset) const
{
  // curvature of the spiral at sOffset
  units::curvature::inverse_meter_t curvAtsOffet = c_start + c_dot * sOffset;

  // start and end coordinates of spiral
  Common::Vector2d<units::length::meter_t> start, end;

  double tmpYValue{};
  double tmpXValue{};

  // calculate x and y of spiral start, assuming sOffset = 0 means start of spiral with curvature curvStart
  (void)fresnl(l_start.value() / a.value() / SQRT_PI, &tmpYValue, &tmpXValue);
  start.y = units::length::meter_t(tmpYValue);
  start.x = units::length::meter_t(tmpXValue);
  start.Scale(a.value() * SQRT_PI);
  start.y *= sign;

  // calculate x and y of spiral end, assuming l_start + sOffset means end of spiral with curvature curvEnd
  (void)fresnl(units::unit_cast<double>(l_start + sOffset) / a.value() / SQRT_PI, &tmpYValue, &tmpXValue);
  end.y = units::length::meter_t(tmpYValue);
  end.x = units::length::meter_t(tmpXValue);
  end.Scale(a.value() * SQRT_PI);
  end.y *= sign;

  // delta x, y from start of spiral to end of spiral
  auto diff = end - start;

  // tangent angle at end of spiral
  units::angle::radian_t t_end{units::unit_cast<double>((l_start + sOffset) * curvAtsOffet / 2.0)};

  // heading change of spiral
  const auto dHdg = t_end - t_start;

  // rotate delta x, y to match starting direction and origin heading
  diff.Rotate(-t_start + hdg);

  // heading at end of spiral
  auto endHdg = hdg + dHdg;

  // calculate t-offset to spiral
  Common::Vector2d<units::length::meter_t> unit{1.0_m, 0_m};
  unit.Rotate(endHdg + units::angle::radian_t(M_PI_2));
  unit.Scale(tOffset.value());

  return diff + unit + Common::Vector2d<units::length::meter_t>(x, y);
}

units::curvature::inverse_meter_t RoadGeometrySpiral::FullCurvature(units::length::meter_t sOffset) const
{
  return c_start + c_dot * sOffset;
}

units::angle::radian_t RoadGeometrySpiral::FullDir(units::length::meter_t sOffset) const
{
  // tangent_angle = L / (2 * R) = 0.5 * L * curv
  // direction change in spiral = tangent_end - tangent_start

  const auto curvEnd = FullCurvature(sOffset);
  return hdg + 0.5_rad * (curvEnd * (l_start + sOffset) - c_start * l_start);
}

Common::Vector2d<units::length::meter_t> RoadGeometrySpiral::GetCoord(units::length::meter_t sOffset,
                                                                      units::length::meter_t tOffset) const
{
  if (0.0_i_m == c_start && 0.0_i_m == c_end)
  {
    return GetCoordLine(sOffset, tOffset);
  }

  if (mantle_api::AlmostEqual(c_start, c_end, static_cast<units::curvature::inverse_meter_t>(1e-6)))
  {
    return GetCoordArc(sOffset, tOffset, c_start);
  }

  return FullCoord(sOffset, tOffset);
}

units::angle::radian_t RoadGeometrySpiral::GetDir(units::length::meter_t sOffset) const
{
  if (0.0_i_m == c_start && 0.0_i_m == c_end)
  {
    return GetDirLine(sOffset);
  }

  if (mantle_api::AlmostEqual(c_start, c_end, static_cast<units::curvature::inverse_meter_t>(1e-6)))
  {
    return GetDirArc(sOffset, c_start);
  }

  return FullDir(sOffset);
}

Common::Vector2d<units::length::meter_t> RoadGeometryPoly3::GetCoord(units::length::meter_t sOffset,
                                                                     units::length::meter_t tOffset) const
{
  if (0.0_m == a && 0.0 == b && 0.0_i_m == c
      && units::unit_t<units::inverse<units::squared<units::length::meter>>>(0.0) == d)
  {
    return GetCoordLine(sOffset, tOffset);
  }

  units::length::meter_t s = 0.0_m;
  Common::Vector2d<units::length::meter_t> lastPos;
  Common::Vector2d<units::length::meter_t> delta;
  Common::Vector2d<units::length::meter_t> pos(0.0_m, a);

  while (s < sOffset)
  {
    lastPos = pos;
    pos.x += 1.0_m;
    pos.y = a + b * pos.x + c * pos.x * pos.x + d * pos.x * pos.x * pos.x;

    delta = pos - lastPos;
    units::length::meter_t deltaLength{delta.Length()};

    if (0.0_m == deltaLength)
    {
      LOG_INTERN(LogLevel::Warning) << "could not calculate road geometry correctly";
      return {};
    }

    if (s + deltaLength > sOffset)
    {
      // rescale last step
      units::dimensionless::scalar_t scale = (sOffset - s) / deltaLength;

      delta.Scale(scale);
      deltaLength = sOffset - s;
    }

    s += deltaLength;
  }

  Common::Vector2d<units::length::meter_t> offset(lastPos + delta);

  Common::Vector2d<units::length::meter_t> norm;
  if (0_m < sOffset)
  {
    norm = delta;
  }
  else  // account for start point
  {
    norm.x = 1.0_m;
  }

  norm.Rotate(units::angle::radian_t(-M_PI_2));  // pointing to right side

  Common::Vector2d<units::dimensionless::scalar_t> normalizedVector;

  try
  {
    normalizedVector = norm.Norm();
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "division by 0";
  }

  norm.x = normalizedVector.x * -tOffset;
  norm.y = normalizedVector.y * -tOffset;

  offset.Add(norm);

  offset.Rotate(hdg);

  offset.x += x;
  offset.y += y;
  return offset;
}

units::angle::radian_t RoadGeometryPoly3::GetDir(units::length::meter_t sOffset) const
{
  if (0.0_m == a && 0.0 == b && 0.0_i_m == c
      && units::unit_t<units::inverse<units::squared<units::length::meter>>>(0.0) == d)
  {
    return GetDirLine(sOffset);
  }

  units::length::meter_t s = 0.0_m;
  Common::Vector2d<units::length::meter_t> lastPos;
  Common::Vector2d<units::length::meter_t> delta;
  Common::Vector2d<units::length::meter_t> pos(0.0_m, a);

  while (s < sOffset)
  {
    lastPos = pos;
    pos.x += 1.0_m;
    pos.y = a + b * pos.x + c * pos.x * pos.x + d * pos.x * pos.x * pos.x;

    delta = pos - lastPos;
    units::length::meter_t deltaLength{delta.Length()};

    if (0.0_m == deltaLength)
    {
      LOG_INTERN(LogLevel::Warning) << "could not calculate road geometry correctly";
      return 0.0_rad;
    }

    if (s + deltaLength > sOffset)
    {
      // rescale last step
      units::dimensionless::scalar_t scale = (sOffset - s) / deltaLength;

      delta.Scale(scale);
      deltaLength = sOffset - s;
    }

    s += deltaLength;
  }

  Common::Vector2d<units::length::meter_t> direction;
  if (0_m < sOffset)
  {
    direction = delta;
  }
  else  // account for start point
  {
    direction.x = 1.0_m;
  }

  direction.Rotate(hdg);

  Common::Vector2d<units::dimensionless::scalar_t> normalizedDirection;

  try
  {
    normalizedDirection = direction.Norm();
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "division by 0";
  }

  if (1.0 < normalizedDirection.y)
  {
    normalizedDirection.y = 1.0;
  }
  else if (-1.0 > normalizedDirection.y)
  {
    normalizedDirection.y = -1.0;
  }

  direction.y = units::length::meter_t(normalizedDirection.y.value());

  auto angle = units::math::asin(normalizedDirection.y);

  if (0.0 <= normalizedDirection.x)
  {
    return angle;
  }
  return 1_rad * M_PI - angle;
}

//!Maximum distance between two steps for ParamPoly3 calculations
const units::length::meter_t MAXIMUM_DELTALENGTH{0.1};

Common::Vector2d<units::length::meter_t> RoadGeometryParamPoly3::GetCoord(units::length::meter_t sOffset,
                                                                          units::length::meter_t tOffset) const
{
  if (0.0_m == parameters.aV && 0.0 == parameters.bV && 0.0_i_m == parameters.cV
      && units::unit_t<units::inverse<units::squared<units::length::meter>>>(0.0) == parameters.dV)
  {
    return GetCoordLine(sOffset, tOffset);
  }

  units::length::meter_t s{0.0};
  Common::Vector2d<units::length::meter_t> lastPos;
  Common::Vector2d<units::length::meter_t> delta;
  units::length::meter_t p{0.0};
  Common::Vector2d<units::length::meter_t> pos(parameters.aU, parameters.aV);

  while (s < sOffset)
  {
    lastPos = pos;
    units::length::meter_t stepSize{1.0};
    units::length::meter_t deltaLength{std::numeric_limits<double>::max()};

    while (deltaLength > MAXIMUM_DELTALENGTH)
    {
      auto p_new = p + stepSize;
      pos.x = parameters.aU + parameters.bU * p_new + parameters.cU * p_new * p_new
            + parameters.dU * p_new * p_new * p_new;
      pos.y = parameters.aV + parameters.bV * p_new + parameters.cV * p_new * p_new
            + parameters.dV * p_new * p_new * p_new;

      delta = pos - lastPos;
      deltaLength = delta.Length();

      stepSize *= 0.5;
    }
    p += stepSize;

    if (0.0_m == deltaLength)
    {
      LOG_INTERN(LogLevel::Warning) << "could not calculate road geometry correctly";
      return {};
    }

    if (s + deltaLength > sOffset)
    {
      // rescale last step
      double scale = (sOffset - s) / deltaLength;

      delta.Scale(scale);
      deltaLength = sOffset - s;
    }

    s += deltaLength;
  }

  Common::Vector2d<units::length::meter_t> offset(lastPos + delta);

  Common::Vector2d<units::length::meter_t> norm;
  if (0_m < sOffset)
  {
    norm = delta;
  }
  else  // account for start point
  {
    norm.x = 1.0_m;
  }

  norm.Rotate(units::angle::radian_t(-M_PI_2));  // pointing to right side

  Common::Vector2d<units::dimensionless::scalar_t> normalizedVector;

  try
  {
    normalizedVector = norm.Norm();
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "division by 0";
  }

  norm.x = normalizedVector.x * -tOffset;
  norm.y = normalizedVector.y * -tOffset;

  offset.Add(norm);

  offset.Rotate(hdg);

  offset.x += x;
  offset.y += y;
  return offset;
}

units::angle::radian_t RoadGeometryParamPoly3::GetDir(units::length::meter_t sOffset) const
{
  units::length::meter_t s{0.0};
  Common::Vector2d<units::length::meter_t> lastPos;
  Common::Vector2d<units::length::meter_t> delta;
  units::length::meter_t p{0.0};
  Common::Vector2d<units::length::meter_t> pos(parameters.aU, parameters.aV);

  while (s < sOffset)
  {
    lastPos = pos;
    units::length::meter_t stepSize{1.0};
    units::length::meter_t deltaLength{std::numeric_limits<double>::max()};

    while (deltaLength > MAXIMUM_DELTALENGTH)
    {
      auto p_new = p + stepSize;
      pos.x = parameters.aU + parameters.bU * p_new + parameters.cU * p_new * p_new
            + parameters.dU * p_new * p_new * p_new;
      pos.y = parameters.aV + parameters.bV * p_new + parameters.cV * p_new * p_new
            + parameters.dV * p_new * p_new * p_new;

      delta = pos - lastPos;
      deltaLength = delta.Length();

      stepSize *= 0.5;
    }
    p += stepSize;

    if (0.0_m == deltaLength)
    {
      LOG_INTERN(LogLevel::Warning) << "could not calculate road geometry correctly";
      return 0.0_rad;
    }

    if (s + deltaLength > sOffset)
    {
      // rescale last step
      units::dimensionless::scalar_t scale = (sOffset - s) / deltaLength;

      delta.Scale(scale);
      deltaLength = sOffset - s;
    }

    s += deltaLength;
  }

  units::dimensionless::scalar_t dU = parameters.bU + 2 * parameters.cU * p + 3 * parameters.dU * p * p;
  units::dimensionless::scalar_t dV = parameters.bV + 2 * parameters.cV * p + 3 * parameters.dV * p * p;

  return GetHdg() + units::math::atan2(dV, dU);
}

void Road::AddGeometryLine(units::length::meter_t s,
                           units::length::meter_t x,
                           units::length::meter_t y,
                           units::angle::radian_t hdg,
                           units::length::meter_t length)
{
  geometries.emplace_back(std::make_unique<RoadGeometryLine>(s, x, y, hdg, length));
}

void Road::AddGeometryArc(units::length::meter_t s,
                          units::length::meter_t x,
                          units::length::meter_t y,
                          units::angle::radian_t hdg,
                          units::length::meter_t length,
                          units::curvature::inverse_meter_t curvature)
{
  geometries.emplace_back(std::make_unique<RoadGeometryArc>(s, x, y, hdg, length, curvature));
}

void Road::AddGeometrySpiral(units::length::meter_t s,
                             units::length::meter_t x,
                             units::length::meter_t y,
                             units::angle::radian_t hdg,
                             units::length::meter_t length,
                             units::curvature::inverse_meter_t curvStart,
                             units::curvature::inverse_meter_t curvEnd)
{
  geometries.emplace_back(std::make_unique<RoadGeometrySpiral>(s, x, y, hdg, length, curvStart, curvEnd));
}

void Road::AddGeometryPoly3(units::length::meter_t s,
                            units::length::meter_t x,
                            units::length::meter_t y,
                            units::angle::radian_t hdg,
                            units::length::meter_t length,
                            units::length::meter_t a,
                            double b,
                            units::unit_t<units::inverse<units::length::meter>> c,
                            units::unit_t<units::inverse<units::squared<units::length::meter>>> d)
{
  geometries.emplace_back(std::make_unique<RoadGeometryPoly3>(s, x, y, hdg, length, a, b, c, d));
}

void Road::AddGeometryParamPoly3(units::length::meter_t s,
                                 units::length::meter_t x,
                                 units::length::meter_t y,
                                 units::angle::radian_t hdg,
                                 units::length::meter_t length,
                                 ParamPoly3Parameters parameters)
{
  geometries.emplace_back(std::make_unique<RoadGeometryParamPoly3>(s, x, y, hdg, length, parameters));
}

void Road::AddElevation(units::length::meter_t s,
                        units::length::meter_t a,
                        double b,
                        units::unit_t<units::inverse<units::length::meter>> c,
                        units::unit_t<units::inverse<units::squared<units::length::meter>>> d)
{
  elevations.emplace_back(std::make_unique<RoadElevation>(s, a, b, c, d));
}

void Road::AddLaneOffset(units::length::meter_t s,
                         units::length::meter_t a,
                         double b,
                         units::unit_t<units::inverse<units::length::meter>> c,
                         units::unit_t<units::inverse<units::squared<units::length::meter>>> d)
{
  laneOffsets.emplace_back(std::make_unique<RoadLaneOffset>(s, a, b, c, d));
}

void Road::AddLink(RoadLinkType type,
                   RoadLinkElementType elementType,
                   const std::string &elementId,
                   ContactPointType contactPoint)
{
  links.emplace_back(std::make_unique<RoadLink>(type, elementType, elementId, contactPoint));
}

void Road::AddRoadLaneSection(units::length::meter_t start)
{
  laneSections.emplace_back(std::make_unique<RoadLaneSection>(this, start));
}

void Road::AddRoadSignal(const RoadSignalSpecification &signal)
{
  roadSignals.emplace_back(std::make_unique<RoadSignal>(this, signal));
}

void Road::AddRoadObject(const RoadObjectSpecification &object)
{
  roadObjects.emplace_back(std::make_unique<RoadObject>(this, object));
}

void Road::AddRoadType(const RoadTypeSpecification &info)
{
  roadTypes.push_back(info);
}

RoadTypeInformation Road::GetRoadType(units::length::meter_t start) const
{
  for (RoadTypeSpecification roadTypeSpec : roadTypes)
  {
    if (mantle_api::AlmostEqual(roadTypeSpec.s, start, static_cast<units::length::meter_t>(1e-6)))
    {
      return roadTypeSpec.roadType;
    }
  }

  return RoadTypeInformation::Undefined;
}
