/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "importerCommon.h"

#include <chrono>
#include <filesystem>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlschemas.h>
#include <ostream>

namespace fs = std::filesystem;

bool ImporterCommon::validateXMLSchema(const std::string &xmlFileName,
                                       const std::string &xmlSchemaFilename,
                                       xmlDocPtr doc,
                                       const std::filesystem::path &executionPath)
{
  fs::path schemaDirectory = executionPath / "schemas";
  if (!fs::exists(schemaDirectory))
  {
    auto application_directory = executionPath.parent_path();
    schemaDirectory = application_directory / "schemas";

    if (!fs::exists(schemaDirectory))
    {
      LOG_INTERN(LogLevel::Warning) << "schemas folder is missing in installation directory";
      return false;
    }
  }
  fs::path xmlSchemaFileLocation = schemaDirectory / xmlSchemaFilename;

  xmlSchemaParserCtxtPtr parserContext = xmlSchemaNewParserCtxt(xmlSchemaFileLocation.string().c_str());
  if (!parserContext)
  {
    LOG_INTERN(LogLevel::Warning) << "Error creating XML Schema parser context. XML Schema file: "
                                         + xmlSchemaFileLocation.string() + " is not a Schema file";
    return false;
  }

  xmlSchemaPtr schema = xmlSchemaParse(parserContext);
  xmlSchemaFreeParserCtxt(parserContext);
  if (!schema)
  {
    LOG_INTERN(LogLevel::Warning) << "Error parsing XML Schema file: " + xmlSchemaFileLocation.string();
    return false;
  }

  xmlSchemaValidCtxtPtr validContext = xmlSchemaNewValidCtxt(schema);
  if (!validContext)
  {
    LOG_INTERN(LogLevel::Warning) << "XML Schema file: " + xmlSchemaFileLocation.string()
                                         + " is not a valid Schema file";
    xmlSchemaFree(schema);
    return false;
  }

  if (xmlSchemaValidateDoc(validContext, doc) != 0)
  {
    LOG_INTERN(LogLevel::Warning) << "Schema file: " + xmlSchemaFileLocation.string()
                                         + " does not meet the XML Schema in " + xmlFileName;
    xmlSchemaFreeValidCtxt(validContext);
    xmlSchemaFree(schema);
    return false;
  }

  xmlSchemaFreeValidCtxt(validContext);
  xmlSchemaFree(schema);

  return true;
}
