/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

namespace openpass::importer::xml::parameterImporter::tag
{
constexpr const char* Bool{"Bool"};
constexpr const char* Double{"Double"};
constexpr const char* DoubleVector{"DoubleVector"};
constexpr const char* Int{"Int"};
constexpr const char* IntVector{"IntVector"};
constexpr const char* String{"String"};
constexpr const char* StringVector{"StringVector"};

constexpr const char* NormalDistribution{"NormalDistribution"};
constexpr const char* LogNormalDistribution{"LogNormalDistribution"};
constexpr const char* UniformDistribution{"UniformDistribution"};
constexpr const char* ExponentialDistribution{"ExponentialDistribution"};
constexpr const char* GammaDistribution{"GammaDistribution"};

constexpr const char* List{"List"};
constexpr const char* ListItem{"ListItem"};
constexpr const char* Reference{"Reference"};
}  //namespace openpass::importer::xml::parameterImporter::tag

namespace openpass::importer::xml::parameterImporter::attribute
{
constexpr const char* key{"Key"};
constexpr const char* lambda{"Lambda"};
constexpr const char* max{"Max"};
constexpr const char* mean{"Mean"};
constexpr const char* min{"Min"};
constexpr const char* mu{"Mu"};
constexpr const char* name{"Name"};
constexpr const char* scale{"Scale"};
constexpr const char* sd{"SD"};
constexpr const char* shape{"Shape"};
constexpr const char* sigma{"Sigma"};
constexpr const char* type{"Type"};
constexpr const char* value{"Value"};
}  //namespace openpass::importer::xml::parameterImporter::attribute

namespace openpass::importer::xml::profilesImporter::tag
{
constexpr const char* agentProfile{"AgentProfile"};
constexpr const char* agentProfiles{"AgentProfiles"};
constexpr const char* component{"Component"};
constexpr const char* components{"Components"};
constexpr const char* driverProfile{"DriverProfile"};
constexpr const char* driverProfiles{"DriverProfiles"};
constexpr const char* model{"Model"};
constexpr const char* position{"Position"};
constexpr const char* profile{"Profile"};
constexpr const char* profiles{"Profiles"};
constexpr const char* profileGroup{"ProfileGroup"};
constexpr const char* sensor{"Sensor"};
constexpr const char* sensors{"Sensors"};
constexpr const char* sensorLink{"SensorLink"};
constexpr const char* sensorLinks{"SensorLinks"};
constexpr const char* system{"System"};
constexpr const char* systemProfile{"SystemProfile"};
constexpr const char* systemProfiles{"SystemProfiles"};
constexpr const char* vehicleModel{"VehicleModel"};
constexpr const char* vehicleModels{"VehicleModels"};
}  //namespace openpass::importer::xml::profilesImporter::tag

namespace openpass::importer::xml::profilesImporter::attribute
{
constexpr const char* file{"File"};
constexpr const char* id{"Id"};
constexpr const char* inputId{"InputId"};
constexpr const char* name{"Name"};
constexpr const char* position{"Position"};
constexpr const char* schemaVersion{"SchemaVersion"};
constexpr const char* sensorId{"SensorId"};
constexpr const char* type{"Type"};
constexpr const char* vehicleModel{"VehicleModel"};
constexpr const char* latency{"Latency"};
}  //namespace openpass::importer::xml::profilesImporter::attribute

namespace openpass::importer::xml::sceneryImporter::tag
{
constexpr const char* arc{"arc"};
constexpr const char* border{"border"};
constexpr const char* center{"center"};
constexpr const char* connection{"connection"};
constexpr const char* dependency{"dependency"};
constexpr const char* elevation{"elevation"};
constexpr const char* elevationProfile{"elevationProfile"};
constexpr const char* geometry{"geometry"};
constexpr const char* geoReference{"geoReference"};
constexpr const char* header{"header"};
constexpr const char* junction{"junction"};
constexpr const char* lane{"lane"};
constexpr const char* laneLink{"laneLink"};
constexpr const char* laneOffset{"laneOffset"};
constexpr const char* lanes{"lanes"};
constexpr const char* laneSection{"laneSection"};
constexpr const char* left{"left"};
constexpr const char* line{"line"};
constexpr const char* link{"link"};
constexpr const char* neighbor{"neighbor"};
constexpr const char* object{"object"};
constexpr const char* objects{"objects"};
constexpr const char* paramPoly3{"paramPoly3"};
constexpr const char* planView{"planView"};
constexpr const char* poly3{"poly3"};
constexpr const char* predecessor{"predecessor"};
constexpr const char* priority{"priority"};
constexpr const char* repeat{"repeat"};
constexpr const char* right{"right"};
constexpr const char* road{"road"};
constexpr const char* roadMark{"roadMark"};
constexpr const char* signal{"signal"};
constexpr const char* Signals{"signals"};
constexpr const char* spiral{"spiral"};
constexpr const char* successor{"successor"};
constexpr const char* type{"type"};
constexpr const char* validity{"validity"};
constexpr const char* width{"width"};
}  //namespace openpass::importer::xml::sceneryImporter::tag

namespace openpass::importer::xml::sceneryImporter::attribute
{
constexpr const char* a{"a"};
constexpr const char* aU{"aU"};
constexpr const char* aV{"aV"};
constexpr const char* b{"b"};
constexpr const char* bU{"bU"};
constexpr const char* bV{"bV"};
constexpr const char* c{"c"};
constexpr const char* color{"color"};
constexpr const char* connectingRoad{"connectingRoad"};
constexpr const char* contactPoint{"contactPoint"};
constexpr const char* country{"country"};
constexpr const char* countryRevision{"countryRevision"};
constexpr const char* cU{"cU"};
constexpr const char* curvature{"curvature"};
constexpr const char* curvEnd{"curvEnd"};
constexpr const char* curvStart{"curvStart"};
constexpr const char* cV{"cV"};
constexpr const char* d{"d"};
constexpr const char* direction{"direction"};
constexpr const char* distance{"distance"};
constexpr const char* ds{"ds"};
constexpr const char* dU{"dU"};
constexpr const char* dV{"dV"};
constexpr const char* dynamic{"dynamic"};
constexpr const char* elementId{"elementId"};
constexpr const char* elementType{"elementType"};
constexpr const char* end{"end"};
constexpr const char* from{"from"};
constexpr const char* fromLane{"fromLane"};
constexpr const char* hdg{"hdg"};
constexpr const char* height{"height"};
constexpr const char* high{"high"};
constexpr const char* hOffset{"hOffset"};
constexpr const char* id{"id"};
constexpr const char* incomingRoad{"incomingRoad"};
constexpr const char* junction{"junction"};
constexpr const char* left{"left"};
constexpr const char* length{"length"};
constexpr const char* low{"low"};
constexpr const char* name{"name"};
constexpr const char* opposite{"opposite"};
constexpr const char* orientation{"orientation"};
constexpr const char* pitch{"pitch"};
constexpr const char* right{"right"};
constexpr const char* road{"road"};
constexpr const char* roll{"roll"};
constexpr const char* s{"s"};
constexpr const char* same{"same"};
constexpr const char* side{"side"};
constexpr const char* sOffset{"sOffset"};
constexpr const char* start{"start"};
constexpr const char* subtype{"subtype"};
constexpr const char* t{"t"};
constexpr const char* text{"text"};
constexpr const char* to{"to"};
constexpr const char* toLane{"toLane"};
constexpr const char* type{"type"};
constexpr const char* unit{"unit"};
constexpr const char* validLength{"validLength"};
constexpr const char* value{"value"};
constexpr const char* weight{"weight"};
constexpr const char* width{"width"};
constexpr const char* x{"x"};
constexpr const char* y{"y"};
constexpr const char* zOffset{"zOffset"};
}  //namespace openpass::importer::xml::sceneryImporter::attribute

namespace openpass::importer::xml::simulationConfigImporter::tag
{
constexpr const char* environment{"Environment"};
constexpr const char* experiment{"Experiment"};
constexpr const char* friction{"Friction"};
constexpr const char* frictions{"Frictions"};
constexpr const char* homogenities{"Homogenities"};
constexpr const char* homogenity{"Homogenity"};
constexpr const char* libraries{"Libraries"};
constexpr const char* library{"Library"};
constexpr const char* observation{"Observation"};
constexpr const char* observations{"Observations"};
constexpr const char* parameters{"Parameters"};
constexpr const char* priority{"Priority"};
constexpr const char* scenario{"Scenario"};
constexpr const char* spawner{"Spawner"};
constexpr const char* spawners{"Spawners"};
constexpr const char* timeOfDay{"TimeOfDay"};
constexpr const char* timeOfDays{"TimeOfDays"};
constexpr const char* trafficRules{"TrafficRules"};
constexpr const char* turningRate{"TurningRate"};
constexpr const char* turningRates{"TurningRates"};
constexpr const char* type{"Type"};
constexpr const char* visibilityDistance{"VisibilityDistance"};
constexpr const char* visibilityDistances{"VisibilityDistances"};
constexpr const char* weather{"Weather"};
constexpr const char* weathers{"Weathers"};
}  //namespace openpass::importer::xml::simulationConfigImporter::tag

namespace openpass::importer::xml::simulationConfigImporter::attribute
{
constexpr const char* incoming{"Incoming"};
constexpr const char* outgoing{"Outgoing"};
constexpr const char* schemaVersion{"SchemaVersion"};
constexpr const char* weight{"Weight"};
}  //namespace openpass::importer::xml::simulationConfigImporter::attribute

namespace openpass::importer::xml::systemConfigImporter::tag
{
constexpr const char* component{"component"};
constexpr const char* components{"components"};
constexpr const char* connection{"connection"};
constexpr const char* connections{"connections"};
constexpr const char* id{"id"};
constexpr const char* input{"input"};
constexpr const char* output{"output"};
constexpr const char* parameter{"parameter"};
constexpr const char* parameters{"parameters"};
constexpr const char* source{"source"};
constexpr const char* system{"system"};
constexpr const char* target{"target"};
}  //namespace openpass::importer::xml::systemConfigImporter::tag

namespace openpass::importer::xml::trajectoryImporter::tag
{
constexpr const char* trajectoryCoordinate{"TrajectoryCoordinate"};
}

namespace openpass::importer::xml::trajectoryImporter::attribute
{
constexpr const char* hdg{"Hdg"};
constexpr const char* s{"S"};
constexpr const char* t{"T"};
constexpr const char* time{"Time"};
constexpr const char* x{"X"};
constexpr const char* y{"Y"};
constexpr const char* yaw{"Yaw"};
}  //namespace openpass::importer::xml::trajectoryImporter::attribute

namespace openpass::importer::xml::vehicleModelsImporter::tag
{
constexpr const char* axles{"Axles"};
constexpr const char* boundingBox{"BoundingBox"};
constexpr const char* catalog{"Catalog"};
constexpr const char* center{"Center"};
constexpr const char* dimensions{"Dimensions"};
constexpr const char* frontAxle{"FrontAxle"};
constexpr const char* parameterDeclarations{"ParameterDeclarations"};
constexpr const char* pedestrian{"Pedestrian"};
constexpr const char* performance{"Performance"};
constexpr const char* properties{"Properties"};
constexpr const char* property{"Property"};
constexpr const char* rearAxle{"RearAxle"};
constexpr const char* vehicle{"Vehicle"};
}  //namespace openpass::importer::xml::vehicleModelsImporter::tag

namespace openpass::importer::xml::vehicleModelsImporter::attribute
{
constexpr const char* vehicleCategory{"vehicleCategory"};
constexpr const char* height{"height"};
constexpr const char* length{"length"};
constexpr const char* mass{"mass"};
constexpr const char* maxAcceleration{"maxAcceleration"};
constexpr const char* maxDeceleration{"maxDeceleration"};
constexpr const char* maxSpeed{"maxSpeed"};
constexpr const char* maxSteering{"maxSteering"};
constexpr const char* name{"name"};
constexpr const char* pedestrianCategory{"pedestrianCategory"};
constexpr const char* positionX{"positionX"};
constexpr const char* positionZ{"positionZ"};
constexpr const char* trackWidth{"trackWidth"};
constexpr const char* value{"value"};
constexpr const char* wheelDiameter{"wheelDiameter"};
constexpr const char* width{"width"};
constexpr const char* x{"x"};
constexpr const char* y{"y"};
constexpr const char* z{"z"};
}  //namespace openpass::importer::xml::vehicleModelsImporter::attribute
