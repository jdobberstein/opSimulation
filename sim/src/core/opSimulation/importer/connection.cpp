/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "connection.h"

#include <utility>

Connection::Connection(std::string id,
                       std::string incomingRoad,
                       std::string connectingRoad,
                       ContactPointType contactPoint)
    : id(std::move(id)),
      incommingRoadId(std::move(incomingRoad)),
      connectingRoadId(std::move(connectingRoad)),
      contactPoint(contactPoint)
{
}

Connection::~Connection() = default;

void Connection::AddLink(int from, int to)
{
  links[from] = to;
}

const std::string& Connection::GetConnectingRoadId() const
{
  return connectingRoadId;
}

const std::string& Connection::GetIncommingRoadId() const
{
  return incommingRoadId;
}

const std::map<int, int>& Connection::GetLinks() const
{
  return links;
}

ContactPointType Connection::GetContactPoint() const
{
  return contactPoint;
}
