/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  scenery.h
//! @brief This file contains the representation of the static objects of the
//!        scenery.
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <memory>
#include <stdexcept>
#include <string>

#include "common/opExport.h"
#include "include/sceneryInterface.h"

class JunctionInterface;
class RoadInterface;

namespace Configuration
{

//-----------------------------------------------------------------------------
//! Class representing a scenery as a list of roads.
//-----------------------------------------------------------------------------
class SIMULATIONCOREEXPORT Scenery : public SceneryInterface
{
public:
  //-----------------------------------------------------------------------------
  //! Adds a new road to a scenery by creating a new Road object and adding it
  //! via its ID to a stored mapping of roads.
  //!
  //! @param[in]  id                  ID of the road to add
  //! @param[in]  length              Length of the road
  //! @return                         created road
  //-----------------------------------------------------------------------------
  RoadInterface *AddRoad(const std::string &id, const double length) override;

  //-----------------------------------------------------------------------------
  //! Adds a new junction to a scenery by creating a new Junction object and adding it
  //! via its ID to a stored mapping of junctions.
  //!
  //! @param[in]  id                  ID of the junction to add
  //! @return                         created junction
  //-----------------------------------------------------------------------------
  JunctionInterface *AddJunction(const std::string &id) override;

  //-----------------------------------------------------------------------------
  //! Returns the stored list of roads.
  //!
  //! @return                         list of roads
  //-----------------------------------------------------------------------------
  const std::vector<std::pair<std::string, RoadInterface *>> GetRoads() const override
  {
    std::vector<std::pair<std::string, RoadInterface *>> roadsVector;
    roadsVector.reserve(roads.size());
    std::transform(std::begin(roads),
                   std::end(roads),
                   std::back_inserter(roadsVector),
                   [](const auto &item) { return std::make_pair(item.first, item.second.get()); });
    return roadsVector;
  }

  //-----------------------------------------------------------------------------
  //! Returns the stored list of junctions.
  //!
  //! @return                         list of junctions
  //-----------------------------------------------------------------------------
  const std::vector<std::pair<std::string, JunctionInterface *>> GetJunctions() const override
  {
    std::vector<std::pair<std::string, JunctionInterface *>> junctionsVector;
    junctionsVector.reserve(junctions.size());
    std::transform(std::begin(junctions),
                   std::end(junctions),
                   std::back_inserter(junctionsVector),
                   [](const auto &item) { return std::make_pair(item.first, item.second.get()); });
    return junctionsVector;
  }

  //-----------------------------------------------------------------------------
  //! Returns the junction with the provided ID from the scenery.
  //!
  //! @param[in]  id                  ID of the junction
  //! @return                         junction with the provided ID
  //-----------------------------------------------------------------------------
  const JunctionInterface *GetJunction(const std::string &id) const override
  {
    JunctionInterface *junction{};
    try
    {
      junction = junctions.at(id).get();
    }
    catch (const std::out_of_range &)
    {
      junction = nullptr;
    }

    return junction;
  }

  //-----------------------------------------------------------------------------
  //! Returns the road with the provided ID from the scenery.
  //!
  //! @param[in]  id                  ID of the road
  //! @return                         road with the provided ID
  //-----------------------------------------------------------------------------
  const RoadInterface *GetRoad(const std::string &id) const override
  {
    RoadInterface *road{};
    try
    {
      road = roads.at(id).get();
    }
    catch (const std::out_of_range &)
    {
      road = nullptr;
    }

    return road;
  }

  //-----------------------------------------------------------------------------
  //! Sets the geo reference (projection string) to the specified value
  //!
  //! @param[in]  ref                 geo reference / projection string
  //-----------------------------------------------------------------------------
  void SetGeoReference(const std::string &ref) { geoReference = ref; }

  //-----------------------------------------------------------------------------
  //! Returns the geo reference / projection string
  //!
  //! @return                         geo reference / projection string
  //-----------------------------------------------------------------------------
  const std::string &GetGeoReference() const { return geoReference; }

private:
  std::map<std::string, std::unique_ptr<RoadInterface>> roads;
  std::map<std::string, std::unique_ptr<JunctionInterface>> junctions;

  std::string geoReference;
};

}  //namespace Configuration
