/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2018 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  road.h
//! @brief This file contains the internal representation of a road in a
//!        scenery.
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <memory>
#include <string>
#include <units.h>
#include <utility>
#include <vector>

#include "common/globalDefinitions.h"
#include "common/vector2d.h"
#include "include/roadInterface/roadElementTypes.h"
#include "include/roadInterface/roadGeometryInterface.h"
#include "include/roadInterface/roadInterface.h"
#include "include/roadInterface/roadLaneInterface.h"
#include "include/roadInterface/roadLaneSectionInterface.h"
#include "include/roadInterface/roadLinkInterface.h"

class RoadElevation;
class RoadLaneOffset;
class RoadLaneRoadMark;
class RoadLaneWidth;
class RoadObjectInterface;
class RoadSignalInterface;

//-----------------------------------------------------------------------------
//! Class representing a link to a road.
//-----------------------------------------------------------------------------
class RoadLink : public RoadLinkInterface
{
public:
  /**
   * @brief RoadLink constructor
   *
   * @param[in] type          Type of the RoadLink
   * @param[in] elementType   Element type of the RoadLink
   * @param[in] elementId     Id of the RoadLink
   * @param[in] contactPoint  ContactPointType of the RoadLink
   */
  RoadLink(RoadLinkType type,
           RoadLinkElementType elementType,
           const std::string &elementId,
           ContactPointType contactPoint)
      : type(type), elementType(elementType), elementId(elementId), contactPoint(contactPoint)
  {
  }
  ~RoadLink() override = default;

  RoadLinkType GetType() const override { return type; }
  RoadLinkElementType GetElementType() const override { return elementType; }
  const std::string &GetElementId() const override { return elementId; }
  ContactPointType GetContactPoint() const override { return contactPoint; }

private:
  RoadLinkType type;
  RoadLinkElementType elementType;
  std::string elementId;
  ContactPointType contactPoint;
};

//-----------------------------------------------------------------------------
//! Class representing a road lane.
//-----------------------------------------------------------------------------
class RoadLane : public RoadLaneInterface
{
public:
  /**
   * @brief RoadLane constructor
   *
   * @param[in] laneSection   RoadLaneSection to which the road lane belongs
   * @param[in] id            Id of the road lane
   * @param[in] type          Type of the road lane
   */
  RoadLane(RoadLaneSectionInterface *laneSection, int id, RoadLaneType type)
      : laneSection(laneSection), id(id), type(type)
  {
  }

  void AddWidth(units::length::meter_t sOffset,
                units::length::meter_t a,
                double b,
                units::unit_t<units::inverse<units::length::meter>> c,
                units::unit_t<units::inverse<units::squared<units::length::meter>>> d) override;

  void AddBorder(units::length::meter_t sOffset,
                 units::length::meter_t a,
                 double b,
                 units::unit_t<units::inverse<units::length::meter>> c,
                 units::unit_t<units::inverse<units::squared<units::length::meter>>> d) override;

  bool AddSuccessor(int id) override;
  bool AddPredecessor(int id) override;
  int GetId() const override { return id; }
  RoadLaneType GetType() const override { return type; }
  const RoadLaneWidths &GetWidths() const override { return widths; }
  const RoadLaneWidths &GetBorders() const override { return borders; }
  const LaneIds &GetSuccessor() const override { return successor; }
  const LaneIds &GetPredecessor() const override { return predecessor; }
  void SetInDirection(bool inDirection) override { this->inDirection = inDirection; }
  bool GetInDirection() const override { return inDirection; }
  RoadLaneSectionInterface *GetLaneSection() const override { return laneSection; }

  void AddRoadMark(units::length::meter_t sOffset,
                   RoadLaneRoadDescriptionType type,
                   RoadLaneRoadMarkType roadMark,
                   RoadLaneRoadMarkColor color,
                   RoadLaneRoadMarkLaneChange laneChange,
                   RoadLaneRoadMarkWeight weight) override;

  const std::vector<std::unique_ptr<RoadLaneRoadMark>> &GetRoadMarks() const override { return roadMarks; }

private:
  RoadLaneSectionInterface *laneSection;
  int id;
  RoadLaneType type;
  // using lists to indicate empty predecessor/successor
  std::vector<std::unique_ptr<RoadLaneWidth>> widths;
  std::vector<std::unique_ptr<RoadLaneWidth>> borders;
  std::vector<int> predecessor;
  std::vector<int> successor;
  bool inDirection = true;

  RoadLaneRoadMarkType roadMarkType = RoadLaneRoadMarkType::Undefined;
  units::length::meter_t roadMarkTypeSOffset;

  std::vector<std::unique_ptr<RoadLaneRoadMark>> roadMarks;
};

//-----------------------------------------------------------------------------
//! Class representing a road lane section that can contain several road lanes.
//-----------------------------------------------------------------------------
class RoadLaneSection : public RoadLaneSectionInterface
{
public:
  /**
   * @brief RoadLaneSection constructor
   *
   * @param[in] road      Road from which this section is a part of
   * @param[in] start     Starting offset of the road lane section
   */
  RoadLaneSection(RoadInterface *road, units::length::meter_t start) : road(road), start(start) {}

  RoadLaneInterface *AddRoadLane(int id, RoadLaneType type) override;
  const std::map<int, std::unique_ptr<RoadLaneInterface>> &GetLanes() const override { return lanes; }
  units::length::meter_t GetStart() const override { return start; }
  void SetInDirection(bool inDirection) override { this->inDirection = inDirection; }
  void SetLaneIndexOffset(int laneIndexOffset) override { this->laneIndexOffset = laneIndexOffset; }
  void SetId(int Id) override { this->Id = Id; }
  bool GetInDirection() const override { return inDirection; }
  int GetLaneIndexOffset() const override { return laneIndexOffset; }
  int GetId() const override { return Id; }
  RoadInterface *GetRoad() const override { return road; }

private:
  RoadInterface *road;
  const units::length::meter_t start;
  std::map<int, std::unique_ptr<RoadLaneInterface>> lanes;  // use map for sorted ids
  bool inDirection = true;
  int laneIndexOffset = 0;
  int Id{0};
};

//-----------------------------------------------------------------------------
//! Abstract class representing the geometry of a road, i.e. the layout of the
//! road's reference line in the in the x/y plane.
//-----------------------------------------------------------------------------
class RoadGeometry : public RoadGeometryInterface
{
public:
  /**
   * @brief RoadGeometry constructor
   *
   * @param[in] s             Start position s-coordinate
   * @param[in] x             Start position x inertial
   * @param[in] y             Start position y inertial
   * @param[in] hdg           Start orientation (inertial heading)
   * @param[in] length        Length of the element's reference line
   */
  RoadGeometry(units::length::meter_t s,
               units::length::meter_t x,
               units::length::meter_t y,
               units::angle::radian_t hdg,
               units::length::meter_t length)
      : s{s}, x{x}, y{y}, hdg{hdg}, length{length}
  {
  }
  ~RoadGeometry() override = default;

  Common::Vector2d<units::length::meter_t> GetCoord(units::length::meter_t sOffset,
                                                    units::length::meter_t tOffset) const override
      = 0;

  units::angle::radian_t GetDir(units::length::meter_t sOffset) const override = 0;
  units::length::meter_t GetS() const override { return s; }
  units::angle::radian_t GetHdg() const override { return hdg; }
  units::length::meter_t GetLength() const override { return length; }

protected:
  //-----------------------------------------------------------------------------
  //! Gets the coordinate at the s and t offest if the road is linear.
  //!
  //! @param[in]  sOffset    s offset within geometry section
  //! @param[in]  tOffset    offset to the left
  //! @return                coordinates with the x/y coordinates
  //-----------------------------------------------------------------------------
  Common::Vector2d<units::length::meter_t> GetCoordLine(units::length::meter_t sOffset,
                                                        units::length::meter_t tOffset) const;

  //-----------------------------------------------------------------------------
  //! Returns the direction of the line, i.e. the start orientation.
  //!
  //! @param[in]  sOffset      offset within geometry section; unused
  //! @return                         direction
  //-----------------------------------------------------------------------------
  units::angle::radian_t GetDirLine(units::length::meter_t sOffset) const;

  //-----------------------------------------------------------------------------
  //! Gets the coordinate at the s and t offest if the road is an arc.
  //!
  //! @param[in]  sOffset    s offset within geometry section
  //! @param[in]  tOffset    offset to the left
  //! @param[in]  curvature  curvature of the arc
  //! @return                coordinates with the x/y coordinates
  //-----------------------------------------------------------------------------
  Common::Vector2d<units::length::meter_t> GetCoordArc(units::length::meter_t sOffset,
                                                       units::length::meter_t tOffset,
                                                       units::curvature::inverse_meter_t curvature) const;

  //-----------------------------------------------------------------------------
  //! Returns the heading of an arc, i.e. the initial heading plus the fraction
  //! of the radius due to the curvature.
  //!
  //! @param[in]  sOffset    s offset within geometry section
  //! @param[in]  curvature  curvature of the arc
  //! @return                heading of the arc
  //-----------------------------------------------------------------------------
  units::angle::radian_t GetDirArc(units::length::meter_t sOffset, units::curvature::inverse_meter_t curvature) const;

  /// Start position s-coordinate
  units::length::meter_t s;
  /// Start position x inertial
  units::length::meter_t x;
  /// Start position y inertial
  units::length::meter_t y;
  /// Start orientation (inertial heading)
  units::angle::radian_t hdg;
  /// Length of the element's reference line
  units::length::meter_t length;
};

//-----------------------------------------------------------------------------
//! Class representing a road line as a RoadGeometry.
//-----------------------------------------------------------------------------
class RoadGeometryLine : public RoadGeometry
{
public:
  /**
   * @brief RoadGeometryLine constructor
   *
   * @param[in] s             Start position s-coordinate
   * @param[in] x             Start position x inertial
   * @param[in] y             Start position y inertial
   * @param[in] hdg           Start orientation (inertial heading)
   * @param[in] length        Length of the element's reference line
   */
  RoadGeometryLine(units::length::meter_t s,
                   units::length::meter_t x,
                   units::length::meter_t y,
                   units::angle::radian_t hdg,
                   units::length::meter_t length)
      : RoadGeometry(s, x, y, hdg, length)
  {
  }
  ~RoadGeometryLine() override = default;
  Common::Vector2d<units::length::meter_t> GetCoord(units::length::meter_t sOffset,
                                                    units::length::meter_t tOffset) const override;
  units::angle::radian_t GetDir(units::length::meter_t sOffset) const override;
};

//-----------------------------------------------------------------------------
//! Class representing a road arc as a RoadGeometry.
//-----------------------------------------------------------------------------
class RoadGeometryArc : public RoadGeometry
{
public:
  /**
   * @brief RoadGeometryArc constructor
   *
   * @param[in] s             Start position s-coordinate
   * @param[in] x             Start position x inertial
   * @param[in] y             Start position y inertial
   * @param[in] hdg           Start orientation (inertial heading)
   * @param[in] length        Length of the element's reference line
   * @param[in] curvature     Constant curvature throughout the elemen
   */
  RoadGeometryArc(units::length::meter_t s,
                  units::length::meter_t x,
                  units::length::meter_t y,
                  units::angle::radian_t hdg,
                  units::length::meter_t length,
                  units::curvature::inverse_meter_t curvature)
      : RoadGeometry{s, x, y, hdg, length}, curvature{curvature}
  {
  }
  ~RoadGeometryArc() override = default;
  Common::Vector2d<units::length::meter_t> GetCoord(units::length::meter_t sOffset,
                                                    units::length::meter_t tOffset) const override;
  units::angle::radian_t GetDir(units::length::meter_t sOffset) const override;

  //-----------------------------------------------------------------------------
  //! Returns the stored curvature.
  //!
  //! @return                         curvature
  //-----------------------------------------------------------------------------
  units::curvature::inverse_meter_t GetCurvature() const { return curvature; }

private:
  units::curvature::inverse_meter_t curvature;
};

//-----------------------------------------------------------------------------
//! Class representing a spiral road (clothoid) as a RoadGeometry.
//-----------------------------------------------------------------------------
class RoadGeometrySpiral : public RoadGeometry
{
public:
  /**
   * @brief RoadGeometrySpiral constructor
   *
   * @param[in] s             Start position s-coordinate
   * @param[in] x             Start position x inertial
   * @param[in] y             Start position y inertial
   * @param[in] hdg           Start orientation (inertial heading)
   * @param[in] length        Length of the element's reference line
   * @param[in] curvStart     Curvature at the start of the element
   * @param[in] curvEnd       Curvature at the end of the element
   */
  RoadGeometrySpiral(units::length::meter_t s,
                     units::length::meter_t x,
                     units::length::meter_t y,
                     units::angle::radian_t hdg,
                     units::length::meter_t length,
                     units::curvature::inverse_meter_t curvStart,
                     units::curvature::inverse_meter_t curvEnd);
  ~RoadGeometrySpiral() override = default;
  Common::Vector2d<units::length::meter_t> GetCoord(units::length::meter_t sOffset,
                                                    units::length::meter_t tOffset) const override;
  units::angle::radian_t GetDir(units::length::meter_t sOffset) const override;

private:
  //-----------------------------------------------------------------------------
  //! Calculates the x/y coordinates as vector.
  //!
  //! @param[in]  sOffset    s offset within geometry section
  //! @param[in]  tOffset    offset to the left
  //! @return     vector with the x/y coordinates
  //-----------------------------------------------------------------------------
  Common::Vector2d<units::length::meter_t> FullCoord(units::length::meter_t sOffset,
                                                     units::length::meter_t tOffset) const;

  //-----------------------------------------------------------------------------
  //! Calculates the curvature.
  //!
  //! @param[in]  sOffset    s offset within geometry section
  //! @return                curvature
  //-----------------------------------------------------------------------------
  units::curvature::inverse_meter_t FullCurvature(units::length::meter_t sOffset) const;

  //-----------------------------------------------------------------------------
  //! Calculates the direction.
  //!
  //! @param[in]  sOffset    s offset within geometry section
  //! @return                direction
  //-----------------------------------------------------------------------------
  units::angle::radian_t FullDir(units::length::meter_t sOffset) const;

  units::curvature::inverse_meter_t c_start;  //!< spiral starting curvature
  units::curvature::inverse_meter_t c_end;    //!< spiral end curvature
  units::length::meter_t a{0.0};              //!< clothoid parameter of spiral
  double sign;  //!< direction of curvature change (needes to correct Fresnel integral results)
  units::unit_t<units::inverse<units::squared<units::length::meter>>> c_dot{0.0};  //!< change of curvature per unit
  units::length::meter_t l_start{0.0};  //!< offset of starting point along spiral
  units::angle::radian_t t_start{0.0};  //!< tangent angle at start point
};

//-----------------------------------------------------------------------------
//! Class representing a road form defined via a cubic polynomial as a RoadGeometry.
//-----------------------------------------------------------------------------
class RoadGeometryPoly3 : public RoadGeometry
{
public:
  /**
   * @brief RoadGeometryPoly3 constructor
   *
   * @param[in] s         Start position s-coordinate
   * @param[in] x         Start position x inertial
   * @param[in] y         Start position y inertial
   * @param[in] hdg       Start orientation (inertial heading)
   * @param[in] length    Length of the element's reference line
   * @param[in] a         Constant factor of the polynomial
   * @param[in] b         Linear factor of the polynomial
   * @param[in] c         Quadratic factor of the polynomial
   * @param[in] d         Cubic factor of the polynomial
   */
  RoadGeometryPoly3(units::length::meter_t s,
                    units::length::meter_t x,
                    units::length::meter_t y,
                    units::angle::radian_t hdg,
                    units::length::meter_t length,
                    units::length::meter_t a,
                    double b,
                    units::unit_t<units::inverse<units::length::meter>> c,
                    units::unit_t<units::inverse<units::squared<units::length::meter>>> d)
      : RoadGeometry(s, x, y, hdg, length), a(a), b(b), c(c), d(d)
  {
  }
  ~RoadGeometryPoly3() override = default;
  Common::Vector2d<units::length::meter_t> GetCoord(units::length::meter_t sOffset,
                                                    units::length::meter_t tOffset) const override;
  units::angle::radian_t GetDir(units::length::meter_t sOffset) const override;

  //-----------------------------------------------------------------------------
  //! Returns the constant factor of the polynomial.
  //!
  //! @return                         constant factor of the polynomial
  //-----------------------------------------------------------------------------
  units::length::meter_t GetA() const { return a; }

  //-----------------------------------------------------------------------------
  //! Returns the linear factor of the polynomial.
  //!
  //! @return                         linear factor of the polynomial
  //-----------------------------------------------------------------------------
  double GetB() const { return b; }

  //-----------------------------------------------------------------------------
  //! Returns the quadratic factor of the polynomial.
  //!
  //! @return                         quadratic factor of the polynomial
  //-----------------------------------------------------------------------------
  units::unit_t<units::inverse<units::length::meter>> GetC() const { return c; }

  //-----------------------------------------------------------------------------
  //! Returns the cubic factor of the polynomial.
  //!
  //! @return                         cubic factor of the polynomial
  //-----------------------------------------------------------------------------
  units::unit_t<units::inverse<units::squared<units::length::meter>>> GetD() const { return d; }

private:
  units::length::meter_t a;
  double b;
  units::unit_t<units::inverse<units::length::meter>> c;
  units::unit_t<units::inverse<units::squared<units::length::meter>>> d;
};

//-----------------------------------------------------------------------------
//! Class representing a road form defined via a parametric cubic polynomial as a RoadGeometry.
//-----------------------------------------------------------------------------
class RoadGeometryParamPoly3 : public RoadGeometry
{
public:
  /**
   * @brief RoadGeometryParamPoly3 constructor
   *
   * @param[in] s             Start position s-coordinate
   * @param[in] x             Start position x inertial
   * @param[in] y             Start position y inertial
   * @param[in] hdg           Start orientation (inertial heading)
   * @param[in] length        Length of the element's reference line
   * @param[in] parameters    Factors of the two polynomials describing the road
   */
  RoadGeometryParamPoly3(units::length::meter_t s,
                         units::length::meter_t x,
                         units::length::meter_t y,
                         units::angle::radian_t hdg,
                         units::length::meter_t length,
                         ParamPoly3Parameters parameters)
      : RoadGeometry(s, x, y, hdg, length), parameters(parameters)
  {
  }
  ~RoadGeometryParamPoly3() override = default;
  Common::Vector2d<units::length::meter_t> GetCoord(units::length::meter_t sOffset,
                                                    units::length::meter_t tOffset) const override;
  units::angle::radian_t GetDir(units::length::meter_t sOffset) const override;

  //-----------------------------------------------------------------------------
  //! Returns the constant factor of the polynomial for the u coordinate.
  //!
  //! @return                         constant factor of the polynomial
  //-----------------------------------------------------------------------------
  units::length::meter_t GetAU() const { return parameters.aU; }

  //-----------------------------------------------------------------------------
  //! Returns the linear factor of the polynomial for the u coordinate.
  //!
  //! @return                         linear factor of the polynomial
  //-----------------------------------------------------------------------------
  units::dimensionless::scalar_t GetBU() const { return parameters.bU; }

  //-----------------------------------------------------------------------------
  //! Returns the quadratic factor of the polynomial for the u coordinate.
  //!
  //! @return                         quadratic factor of the polynomial
  //-----------------------------------------------------------------------------
  units::curvature::inverse_meter_t GetCU() const { return parameters.cU; }

  //-----------------------------------------------------------------------------
  //! Returns the cubic factor of the polynomial for the u coordinate.
  //!
  //! @return                         cubic factor of the polynomial
  //-----------------------------------------------------------------------------
  units::unit_t<units::inverse<units::squared<units::length::meter>>> GetD() const { return parameters.dU; }

  //-----------------------------------------------------------------------------
  //! Returns the constant factor of the polynomial for the v coordinate.
  //!
  //! @return                         constant factor of the polynomial
  //-----------------------------------------------------------------------------
  units::length::meter_t GetAV() const { return parameters.aV; }

  //-----------------------------------------------------------------------------
  //! Returns the linear factor of the polynomial for the v coordinate.
  //!
  //! @return                         linear factor of the polynomial
  //-----------------------------------------------------------------------------
  units::dimensionless::scalar_t GetBV() const { return parameters.bV; }

  //-----------------------------------------------------------------------------
  //! Returns the quadratic factor of the polynomial for the v coordinate.
  //!
  //! @return                         quadratic factor of the polynomial
  //-----------------------------------------------------------------------------
  units::curvature::inverse_meter_t GetCV() const { return parameters.cV; }

  //-----------------------------------------------------------------------------
  //! Returns the cubic factor of the polynomial for the v coordinate.
  //!
  //! @return                         cubic factor of the polynomial
  //-----------------------------------------------------------------------------
  units::unit_t<units::inverse<units::squared<units::length::meter>>> GetDV() const { return parameters.dV; }

private:
  ParamPoly3Parameters parameters;
};

//-----------------------------------------------------------------------------
//! Class representing a road.
//-----------------------------------------------------------------------------
class Road : public RoadInterface
{
public:
  /// @brief Construct a road object
  /// @param id       Id of the road
  /// @param length   Length of the road
  Road(std::string id, const double length) : id(std::move(id)), length(length) {}

  void AddGeometryLine(units::length::meter_t s,
                       units::length::meter_t x,
                       units::length::meter_t y,
                       units::angle::radian_t hdg,
                       units::length::meter_t length) override;

  void AddGeometryArc(units::length::meter_t s,
                      units::length::meter_t x,
                      units::length::meter_t y,
                      units::angle::radian_t hdg,
                      units::length::meter_t length,
                      units::curvature::inverse_meter_t curvature) override;

  void AddGeometrySpiral(units::length::meter_t s,
                         units::length::meter_t x,
                         units::length::meter_t y,
                         units::angle::radian_t hdg,
                         units::length::meter_t length,
                         units::curvature::inverse_meter_t curvStart,
                         units::curvature::inverse_meter_t curvEnd) override;

  void AddGeometryPoly3(units::length::meter_t s,
                        units::length::meter_t x,
                        units::length::meter_t y,
                        units::angle::radian_t hdg,
                        units::length::meter_t length,
                        units::length::meter_t a,
                        double b,
                        units::unit_t<units::inverse<units::length::meter>> c,
                        units::unit_t<units::inverse<units::squared<units::length::meter>>> d) override;

  void AddGeometryParamPoly3(units::length::meter_t s,
                             units::length::meter_t x,
                             units::length::meter_t y,
                             units::angle::radian_t hdg,
                             units::length::meter_t length,
                             ParamPoly3Parameters parameters) override;

  void AddElevation(units::length::meter_t s,
                    units::length::meter_t a,
                    double b,
                    units::unit_t<units::inverse<units::length::meter>> c,
                    units::unit_t<units::inverse<units::squared<units::length::meter>>> d) override;

  void AddLaneOffset(units::length::meter_t s,
                     units::length::meter_t a,
                     double b,
                     units::unit_t<units::inverse<units::length::meter>> c,
                     units::unit_t<units::inverse<units::squared<units::length::meter>>> d) override;

  void AddLink(RoadLinkType type,
               RoadLinkElementType elementType,
               const std::string &elementId,
               ContactPointType contactPoint) override;

  void AddRoadLaneSection(units::length::meter_t start) override;
  void AddRoadSignal(const RoadSignalSpecification &signal) override;
  void AddRoadType(const RoadTypeSpecification &info) override;
  void AddRoadObject(const RoadObjectSpecification &object) override;
  RoadTypeInformation GetRoadType(units::length::meter_t start) const override;

  const std::string GetId() const override { return id; }
  double GetLength() const override { return length; }
  const std::vector<std::unique_ptr<RoadElevation>> &GetElevations() const override { return elevations; }
  const std::vector<std::unique_ptr<RoadLaneOffset>> &GetLaneOffsets() const override { return laneOffsets; }
  const std::vector<std::unique_ptr<RoadGeometryInterface>> &GetGeometries() const override { return geometries; }
  const std::vector<std::unique_ptr<RoadLinkInterface>> &GetRoadLinks() const override { return links; }

  const std::vector<std::unique_ptr<RoadLaneSectionInterface>> &GetLaneSections() const override
  {
    return laneSections;
  }

  const std::vector<std::unique_ptr<RoadSignalInterface>> &GetRoadSignals() const override { return roadSignals; }
  const std::vector<std::unique_ptr<RoadObjectInterface>> &GetRoadObjects() const override { return roadObjects; }
  void SetInDirection(bool inDirection) override { this->inDirection = inDirection; }
  bool GetInDirection() const override { return inDirection; }
  void SetJunctionId(const std::string &junctionId) override { this->junctionId = junctionId; }
  const std::string GetJunctionId() const override { return junctionId; }

private:
  const std::string id;
  const double length;
  std::vector<std::unique_ptr<RoadElevation>> elevations;
  std::vector<std::unique_ptr<RoadLaneOffset>> laneOffsets;
  std::vector<std::unique_ptr<RoadGeometryInterface>> geometries;
  std::vector<std::unique_ptr<RoadLinkInterface>> links;
  std::vector<std::unique_ptr<RoadLaneSectionInterface>> laneSections;
  std::vector<std::unique_ptr<RoadSignalInterface>> roadSignals;
  std::vector<std::unique_ptr<RoadObjectInterface>> roadObjects;
  std::vector<RoadTypeSpecification> roadTypes;
  bool inDirection = true;
  std::string junctionId;
};
