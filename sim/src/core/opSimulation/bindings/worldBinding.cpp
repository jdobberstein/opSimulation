/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "worldBinding.h"

#include <new>
#include <utility>

#include "bindings/worldLibrary.h"

class CallbackInterface;
class DataBufferWriteInterface;
class StochasticsInterface;
class WorldInterface;

namespace core
{

WorldBinding::WorldBinding(std::string libraryPath,
                           CallbackInterface* callbacks,
                           IdManagerInterface* idManager,
                           StochasticsInterface* stochastics,
                           DataBufferWriteInterface* dataBuffer)
    : libraryPath{std::move(libraryPath)},
      callbacks{callbacks},
      idManager{idManager},
      stochastics{stochastics},
      dataBuffer{dataBuffer}
{
}

WorldBinding::~WorldBinding()
{
  if (library)
  {
    library->ReleaseWorld();
  }
}

WorldInterface* WorldBinding::Instantiate()
{
  if (!library)
  {
    // create new world library
    library = std::make_unique<WorldLibrary>(libraryPath, callbacks, idManager, stochastics, dataBuffer);
  }

  // check if initialization is successful
  if (library->Init())
  {
    return library->CreateWorld();
  }

  // world library initialization failed
  return nullptr;
}

}  // namespace core
