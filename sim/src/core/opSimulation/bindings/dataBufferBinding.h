/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  dataBufferBinding.h
//! @brief This file contains the interface to the dataBuffer library.
//-----------------------------------------------------------------------------

#pragma once

#include <memory>
#include <string>

#include "common/opExport.h"

class CallbackInterface;
class DataBufferInterface;
namespace openpass
{
namespace common
{
struct RuntimeInformation;
}  // namespace common
}  // namespace openpass

namespace core
{

class DataBufferLibrary;

//! This class represents the interface to the dataBuffer library
class SIMULATIONCOREEXPORT DataBufferBinding
{
public:
  /**
   * @brief DataBufferBinding constructor
   *
   * @param[in] libraryPath           Path to the library
   * @param[in] runtimeInformation    Common runtimeInformation
   * @param[in] callbacks             Pointer to the callbacks
   */
  DataBufferBinding(std::string libraryPath,
                    const openpass::common::RuntimeInformation& runtimeInformation,
                    CallbackInterface* callbacks);
  DataBufferBinding(const DataBufferBinding&) = delete;
  DataBufferBinding(DataBufferBinding&&) = delete;
  DataBufferBinding& operator=(const DataBufferBinding&) = delete;
  DataBufferBinding& operator=(DataBufferBinding&&) = delete;
  virtual ~DataBufferBinding();

  //-----------------------------------------------------------------------------
  //! Gets the dataBuffer instance library and stores it,
  //! then creates a new dataBufferInterface of the library.
  //!
  //! @return   dataBufferInterface created from the library
  //-----------------------------------------------------------------------------
  DataBufferInterface* Instantiate();

private:
  const std::string libraryPath;
  std::unique_ptr<DataBufferLibrary> library;
  CallbackInterface* callbacks;
  const openpass::common::RuntimeInformation& runtimeInformation;
};

}  // namespace core
