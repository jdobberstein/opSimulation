/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "observationBinding.h"

#include <new>

#include "bindings/observationLibrary.h"
#include "framework/observationModule.h"
#include "include/observationInterface.h"

class CallbackInterface;
class DataBufferReadInterface;
class StochasticsInterface;
class WorldInterface;

namespace openpass::common
{
struct RuntimeInformation;
}  // namespace openpass::common

namespace core
{

ObservationBinding::ObservationBinding(const openpass::common::RuntimeInformation& runtimeInformation,
                                       CallbackInterface* callbacks)
    : runtimeInformation{runtimeInformation}, callbacks{callbacks}
{
}

std::unique_ptr<ObservationModule> ObservationBinding::Instantiate(
    const std::string& libraryPath,
    const openpass::parameter::ParameterSetLevel1& parameter,
    StochasticsInterface* stochastics,
    WorldInterface* world,
    DataBufferReadInterface* const dataBuffer)
{
  if (!library)
  {
    // create new observation library
    library = std::make_unique<ObservationLibrary>(libraryPath, callbacks);
  }

  // check if initialization is successful
  if (library->Init())
  {
    return library->CreateObservationModule(runtimeInformation, parameter, stochastics, world, dataBuffer);
  }

  // observation library initialization failed
  return nullptr;
}

}  // namespace core
