/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "modelLibrary.h"

#include <algorithm>
#include <exception>
#include <iostream>
#include <new>
#include <stdexcept>
#include <string>
#include <utility>

#include "common/log.h"
#include "common/parameter.h"
#include "framework/parameterbuilder.h"
#include "include/componentInterface.h"
#include "include/observationNetworkInterface.h"
#include "include/parameterInterface.h"
#include "modelElements/agent.h"
#include "modelElements/channelBuffer.h"
#include "modelElements/component.h"
#include "modelElements/componentType.h"
#include "modelElements/parameters.h"

class ModelInterface;
class ScenarioControlInterface;
class StochasticsInterface;
class WorldInterface;

namespace openpass::common
{
struct RuntimeInformation;
}  // namespace openpass::common

namespace core
{

bool ModelLibrary::Init()
{
#if defined(unix)
  const std::string path = modelLibraryPath + "/lib" + modelLibraryName;
#elif defined(WIN32)
  std::string suffix = DEBUG_POSTFIX;
  const std::string path = modelLibraryPath + "/" + modelLibraryName + suffix;
#else
#error Unsupported target platform. Either unix or WIN32 have to be set.
#endif

  library = boost::dll::shared_library(
      path, boost::dll::load_mode::append_decorations | boost::dll::load_mode::search_system_folders);
  try
  {
    if (!library.is_loaded())
    {
      LOG_INTERN(LogLevel::Error) << "Failed to load library: " << path;
      return false;
    }
  }
  catch (std::exception& e)
  {
    // ignore exceptions during dublicate initialization of libprotobuf
  }

  getVersionFunc = library.get<ModelInterface_GetVersionType>(DllGetVersionId);
  if (!getVersionFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllGetVersionId << " within the DLL located at "
                                << path;
    return false;
  }

  createInstanceFunc = library.get<ModelInterface_CreateInstanceType>(DllCreateInstanceId);
  if (!createInstanceFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllCreateInstanceId
                                << " within the DLL located at " << path;
    return false;
  }

  destroyInstanceFunc = library.get<ModelInterface_DestroyInstanceType>(DllDestroyInstanceId);
  if (!destroyInstanceFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllDestroyInstanceId
                                << " within the DLL located at " << path;
    return false;
  }

  updateInputFunc = library.get<ModelInterface_UpdateInputType>(DllUpdateInputId);
  if (!updateInputFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllUpdateInputId << " within the DLL located at "
                                << path;
    return false;
  }

  updateOutputFunc = library.get<ModelInterface_UpdateOutputType>(DllUpdateOutputId);
  if (!updateOutputFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllUpdateOutputId << " within the DLL located at "
                                << path;
    return false;
  }

  triggerFunc = library.get<ModelInterface_TriggerType>(DllTriggerId);
  if (!triggerFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllTriggerId << " within the DLL located at "
                                << path;
    return false;
  }

  try
  {
    LOG_INTERN(LogLevel::DebugCore) << "Loaded model library " << library.location().filename().string() << ", version "
                                    << getVersionFunc();

    return true;
  }
  catch (std::runtime_error const& ex)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to retrieve version information from the DLL: " << ex.what();
    return false;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to retrieve version information from the DLL";
    return false;
  }
}

ModelLibrary::~ModelLibrary()
{
  if (!(components.empty()))
  {
    LOG_INTERN(LogLevel::Warning) << "unloading library which is still in use";
  }

  if (library.is_loaded())
  {
    LOG_INTERN(LogLevel::DebugCore) << "unloading library " << modelLibraryName;
    library.unload();
  }
}

bool ModelLibrary::ReleaseComponent(ComponentInterface* component)
{
  if (!library)
  {
    return false;
  }

  auto findIter = std::find(components.begin(), components.end(), component);
  if (components.end() == findIter)
  {
    LOG_INTERN(LogLevel::Warning) << "model doesn't belong to library";
    return false;
  }

  try
  {
    destroyInstanceFunc(component->GetImplementation());
  }
  catch (std::runtime_error const& ex)
  {
    LOG_INTERN(LogLevel::Error) << "model could not be released: " << ex.what();
    return false;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "model could not be released";
    return false;
  }

  delete component;  // NOLINT(cppcoreguidelines-owning-memory)
  components.erase(findIter);

  // do not unload library here since shared_ptrs in other DLLs could still reference deleters within this DLL

  return true;
}

ComponentInterface* ModelLibrary::CreateComponent(const std::shared_ptr<ComponentType>& componentType,
                                                  const std::string& componentName,
                                                  const openpass::common::RuntimeInformation& runtimeInformation,
                                                  StochasticsInterface* stochastics,
                                                  WorldInterface* world,
                                                  ObservationNetworkInterface* observationNetwork,
                                                  Agent* agent,
                                                  std::shared_ptr<ScenarioControlInterface> const& scenarioControl,
                                                  PublisherInterface* publisher)
{
  if (!library)
  {
    return nullptr;
  }

  if (!library.is_loaded())
  {
    return nullptr;
  }

  std::unique_ptr<ComponentInterface> component(new (std::nothrow) Component(componentName, agent));
  if (!component)
  {
    return nullptr;
  }

  if (!component->SetModelLibrary(this))
  {
    return nullptr;
  }

  component->SetObservations(observationNetwork->GetObservationModules());

  ModelInterface* implementation = nullptr;
  auto parameter = openpass::parameter::make<SimulationCommon::Parameters>(runtimeInformation,
                                                                           componentType->GetModelParameters());

  try
  {
    const auto& version = getVersionFunc();

    /*if (version == "0.3.0")
    {
        implementation =
    reinterpret_cast<UnrestrictedEventPublishModelInterface_CreateInstanceType>(createInstanceFunc)( componentName,
            componentType->GetInit(),
            componentType->GetPriority(),
            componentType->GetOffsetTime(),
            componentType->GetResponseTime(),
            componentType->GetCycleTime(),
            stochastics,
            world,
            parameter.get(),
            &component->GetObservations(),
            agent->GetAgentAdapter(),
            callbacks,
            eventNetwork,
            publisher);
    }
    if (version == "0.2.0")
    {
        implementation = reinterpret_cast<UnrestrictedPublishModelInterface_CreateInstanceType>(createInstanceFunc)(
                             componentName,
                             componentType->GetInit(),
                             componentType->GetPriority(),
                             componentType->GetOffsetTime(),
                             componentType->GetResponseTime(),
                             componentType->GetCycleTime(),
                             stochastics,
                             world,
                             parameter.get(),
                             &component->GetObservations(),
                             agent->GetAgentAdapter(),
                             callbacks,
                             publisher);
    }
    else*/
    if (version == "0.1.0")
    {
      implementation = reinterpret_cast<  // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
          UnrestrictedControllStrategyModelInterface_CreateInstanceType>(createInstanceFunc)(
          componentName,
          componentType->GetInit(),
          componentType->GetPriority(),
          componentType->GetOffsetTime(),
          componentType->GetResponseTime(),
          componentType->GetCycleTime(),
          stochastics,
          world,
          parameter.get(),
          publisher,
          agent->GetAgentAdapter(),
          callbacks,
          scenarioControl);
    }
    else
    {
      implementation = createInstanceFunc(componentName,
                                          componentType->GetInit(),
                                          componentType->GetPriority(),
                                          componentType->GetOffsetTime(),
                                          componentType->GetResponseTime(),
                                          componentType->GetCycleTime(),
                                          stochastics,
                                          world,
                                          parameter.get(),
                                          publisher,
                                          agent->GetAgentAdapter(),
                                          callbacks);
    }
  }
  catch (std::runtime_error const& ex)
  {
    LOG_INTERN(LogLevel::Error) << "could not create model instance: " << ex.what();
    return nullptr;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "could not create model instance";
    return nullptr;
  }

  if (!implementation)
  {
    return nullptr;
  }

  component->SetImplementation(implementation);
  component->SetParameter(std::move(parameter));

  ComponentInterface* componentPtr = component.release();
  components.push_back(componentPtr);

  return componentPtr;
}

}  // namespace core
