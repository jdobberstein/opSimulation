/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  dataBufferLibrary.h
//! @brief This file contains the internal representation of the library of a
//!        dataBufferInterface.
//-----------------------------------------------------------------------------

#pragma once

#include <boost/dll.hpp>
#include <boost/function.hpp>
#include <string>

class CallbackInterface;
class DataBufferInterface;
namespace openpass
{
namespace common
{
struct RuntimeInformation;
}  // namespace common
}  // namespace openpass

namespace core
{

//! This class represents the library of a dataBufferInterface
class DataBufferLibrary
{
public:
  //! Type representing the signature of a dll-function for obtaining the version information of the module
  using DataBufferInterface_GetVersionType = const std::string&();
  //! Type representing the signature of a dll-function for creating an instance of the module
  using DataBufferInterface_CreateInstanceType
      = DataBufferInterface*(const openpass::common::RuntimeInformation* runtimeInformation,
                             CallbackInterface* callbacks);
  //! Type representing the signature of a dll-function for destroying an instance of DataBufferInterface
  using DataBufferInterface_DestroyInstanceType = void(DataBufferInterface* implementation);

  /**
   * @brief DataBufferLibrary constructor
   *
   * @param[in] dataBufferLibraryPath Path to the data buffer library
   * @param[in] callbacks             Pointer to the callbacks
   */
  DataBufferLibrary(std::string dataBufferLibraryPath, CallbackInterface* callbacks)
      : dataBufferLibraryPath(std::move(dataBufferLibraryPath)), callbacks(callbacks)
  {
  }

  DataBufferLibrary(const DataBufferLibrary&) = delete;
  DataBufferLibrary(DataBufferLibrary&&) = delete;
  DataBufferLibrary& operator=(const DataBufferLibrary&) = delete;
  DataBufferLibrary& operator=(DataBufferLibrary&&) = delete;

  //-----------------------------------------------------------------------------
  //! Destructor, deletes the stored library (unloads it if necessary)
  //-----------------------------------------------------------------------------
  virtual ~DataBufferLibrary();

  //-----------------------------------------------------------------------------
  //! Creates a boost::dll::shared_library based on the path from the constructor
  //! and stores boost::function for getting the library version, creating and
  //! destroying instances and setting the stochasticsInterface item
  //! (see typedefs for corresponding signatures).
  //!
  //! @return   true on successful operation, false otherwise
  //-----------------------------------------------------------------------------
  bool Init();

  //-----------------------------------------------------------------------------
  //! Delete the dataBufferInterface and the library
  //!
  //! @return   Flag if the release was successful
  //-----------------------------------------------------------------------------
  bool ReleaseDataBuffer();

  //-----------------------------------------------------------------------------
  //! Make sure that the library exists and is loaded, then call the "create instance"
  //! function pointer, which instantiates a DataBufferInterface. The created Interface
  //! is stored.
  //!
  //! @param[in]   runtimeInformation   Reference to the simulation runtime information
  //!
  //! @return   DataBufferInterface created
  //-----------------------------------------------------------------------------
  DataBufferInterface* CreateDataBuffer(const openpass::common::RuntimeInformation& runtimeInformation);

private:
  const std::string DllGetVersionId = "OpenPASS_GetVersion";
  const std::string DllCreateInstanceId = "OpenPASS_CreateInstance";
  const std::string DllDestroyInstanceId = "OpenPASS_DestroyInstance";

  const std::string dataBufferLibraryPath;
  DataBufferInterface* dataBufferInterface = nullptr;
  boost::dll::shared_library library;
  CallbackInterface* callbacks;
  boost::function<DataBufferInterface_GetVersionType> getVersionFunc;
  boost::function<DataBufferInterface_CreateInstanceType> createInstanceFunc;
  boost::function<DataBufferInterface_DestroyInstanceType> destroyInstanceFunc;
};

}  // namespace core
