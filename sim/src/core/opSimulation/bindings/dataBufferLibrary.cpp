/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "dataBufferLibrary.h"

#include <ostream>
#include <stdexcept>

#include "common/log.h"

class DataBufferInterface;

namespace openpass::common
{
struct RuntimeInformation;
}  // namespace openpass::common

namespace core
{

bool DataBufferLibrary::Init()
{
  library = boost::dll::shared_library(
      dataBufferLibraryPath, boost::dll::load_mode::append_decorations | boost::dll::load_mode::search_system_folders);

  if (!library.is_loaded())
  {
    LOG_INTERN(LogLevel::Error) << "Failed to load library: " << dataBufferLibraryPath;
    return false;
  }

  getVersionFunc = library.get<DataBufferInterface_GetVersionType>(DllGetVersionId);

  if (!getVersionFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllGetVersionId << " within the DLL located at "
                                << dataBufferLibraryPath;
    return false;
  }

  createInstanceFunc = library.get<DataBufferInterface_CreateInstanceType>(DllCreateInstanceId);
  if (!createInstanceFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllCreateInstanceId
                                << " within the DLL located at " << dataBufferLibraryPath;
    return false;
  }

  destroyInstanceFunc = library.get<DataBufferInterface_DestroyInstanceType>(DllDestroyInstanceId);
  if (!destroyInstanceFunc)
  {
    LOG_INTERN(LogLevel::Warning) << "Unable to resolve the symbol " << DllDestroyInstanceId
                                  << " within the DLL located at " << dataBufferLibraryPath;
    return false;
  }

  try
  {
    LOG_INTERN(LogLevel::DebugCore) << "Loaded dataBuffer library " << library.location().filename().string()
                                    << ", version " << getVersionFunc();
  }
  catch (std::runtime_error const &ex)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to retrieve version information from the DLL: " << ex.what();
    return false;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to retrieve version information from the DLL";
    return false;
  }

  return true;
}

DataBufferLibrary::~DataBufferLibrary()
{
  if (dataBufferInterface)
  {
    LOG_INTERN(LogLevel::Warning) << "unloading library which is still in use";
  }

  if (library.is_loaded())
  {
    LOG_INTERN(LogLevel::DebugCore) << "unloading dataBuffer library ";
    library.unload();
  }
}

bool DataBufferLibrary::ReleaseDataBuffer()
{
  if (!dataBufferInterface)
  {
    return true;
  }

  if (!library)
  {
    return false;
  }

  try
  {
    destroyInstanceFunc(dataBufferInterface);
  }
  catch (std::runtime_error const &ex)
  {
    LOG_INTERN(LogLevel::Error) << "dataBuffer could not be released: " << ex.what();
    return false;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "dataBuffer could not be released";
    return false;
  }

  dataBufferInterface = nullptr;

  return true;
}

DataBufferInterface *DataBufferLibrary::CreateDataBuffer(const openpass::common::RuntimeInformation &runtimeInformation)
{
  if (!library.is_loaded())
  {
    return nullptr;
  }

  dataBufferInterface = nullptr;

  try
  {
    dataBufferInterface = createInstanceFunc(&runtimeInformation, callbacks);
  }
  catch (std::runtime_error const &ex)
  {
    LOG_INTERN(LogLevel::Error) << "could not create stochastics instance: " << ex.what();
    return nullptr;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "could not create stochastics instance";
    return nullptr;
  }

  return dataBufferInterface;
}

}  // namespace core
