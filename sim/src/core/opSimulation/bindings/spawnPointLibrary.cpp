/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "spawnPointLibrary.h"

#include <iostream>
#include <stdexcept>
#include <string>

#include "common/log.h"
#include "common/spawnPointLibraryDefinitions.h"
#include "include/spawnPointInterface.h"
#include "modelElements/spawnPoint.h"

namespace core
{

bool SpawnPointLibrary::Init()
{
  std::string suffix = DEBUG_POSTFIX;
  library = boost::dll::shared_library(
      libraryPath + suffix, boost::dll::load_mode::append_decorations | boost::dll::load_mode::search_system_folders);

  if (!library.is_loaded())
  {
    LOG_INTERN(LogLevel::Error) << "Failed to load library: " << libraryPath + suffix;
    return false;
  }

  getVersionFunc = library.get<SpawnPointInterface_GetVersionType>(DllGetVersionId);
  if (!getVersionFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllGetVersionId << " within the DLL located at "
                                << libraryPath + suffix;
    return false;
  }

  createInstanceFunc = library.get<SpawnPointInterface_CreateInstanceType>(DllCreateInstanceId);
  if (!createInstanceFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllCreateInstanceId
                                << " within the DLL located at " << libraryPath + suffix;
    return false;
  }

  try
  {
    LOG_INTERN(LogLevel::DebugCore) << "Loaded spawn point library " << library.location().filename().string()
                                    << ", version " << getVersionFunc();
  }
  catch (std::runtime_error const &ex)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to retrieve version information from the DLL: " << ex.what();
    return false;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to retrieve version information from the DLL";
    return false;
  }

  return true;
}

SpawnPointLibrary::~SpawnPointLibrary()
{
  if (library.is_loaded())
  {
    LOG_INTERN(LogLevel::DebugCore) << "unloading library " << libraryPath;
    library.unload();
  }
}

std::unique_ptr<SpawnPoint> SpawnPointLibrary::CreateSpawnPoint(const SpawnPointDependencies &dependencies)
{
  if (!library.is_loaded())
  {
    return nullptr;
  }

  std::unique_ptr<SpawnPointInterface> spawnPoint = nullptr;
  try
  {
    spawnPoint = createInstanceFunc(&dependencies, callbacks);
  }
  catch (std::runtime_error const &ex)
  {
    LOG_INTERN(LogLevel::Error) << "could not create spawn point instance: " << ex.what();
    return nullptr;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "could not create spawn point instance";
    return nullptr;
  }

  if (!spawnPoint)
  {
    return nullptr;
  }

  return std::make_unique<SpawnPoint>(dependencies.agentFactory, std::move(spawnPoint), this);
}
}  // namespace core
