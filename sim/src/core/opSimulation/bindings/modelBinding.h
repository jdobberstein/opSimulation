/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  modelBinding.h
//! @brief This file contains the interface to the model libraries.
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <memory>
#include <string>

#include "common/opExport.h"

class PublisherInterface;
class CallbackInterface;
class ScenarioControlInterface;
class StochasticsInterface;
class WorldInterface;
namespace openpass
{
namespace common
{
struct RuntimeInformation;
}  // namespace common
}  // namespace openpass

namespace core
{

class ModelLibrary;
class Agent;
class ComponentType;
class ObservationNetworkInterface;
class ComponentInterface;

//! This class represents the interface to the model libraries
class SIMULATIONCOREEXPORT ModelBinding
{
public:
  /**
   * @brief ModelBinding constructor
   *
   * @param[in] libraryPath           Path to the library
   * @param[in] runtimeInformation    Common runtimeInformation
   * @param[in] callbacks             Pointer to the callbacks
   */
  ModelBinding(std::string libraryPath,
               const openpass::common::RuntimeInformation &runtimeInformation,
               CallbackInterface *callbacks);
  ModelBinding(const ModelBinding &) = delete;
  ModelBinding(ModelBinding &&) = delete;
  ModelBinding &operator=(const ModelBinding &) = delete;
  ModelBinding &operator=(ModelBinding &&) = delete;
  ~ModelBinding();

  //-----------------------------------------------------------------------------
  //! @brief Creates a new component from the given parameters using the respective
  //!         model library
  //!
  //! Gets the model library from the component type (instantiating and initializing
  //! it, if not already done), then creates the new component from the given
  //! parameters using this library.
  //!
  //! @param[in]  componentType       Type of the component to instantiate
  //! @param[in]  componentName       Name of the component to instantiate
  //! @param[in]  stochastics         Stochastics interface
  //! @param[in]  world               World representation
  //! @param[in]  observationNetwork  Network of the observation modules
  //! @param[in]  agent               Agent that the component type is a part of
  //! @param[in]  scenarioControl     scenarioControl of the entity
  //! @param[in]  publisher           Publisher instance
  //!
  //! @return                         The instantiated component
  //-----------------------------------------------------------------------------
  ComponentInterface *Instantiate(const std::shared_ptr<ComponentType> &componentType,
                                  const std::string &componentName,
                                  StochasticsInterface *stochastics,
                                  WorldInterface *world,
                                  ObservationNetworkInterface *observationNetwork,
                                  Agent *agent,
                                  std::shared_ptr<ScenarioControlInterface> const &scenarioControl,
                                  PublisherInterface *publisher);

private:
  const std::string libraryPath;
  const openpass::common::RuntimeInformation &runtimeInformation;
  CallbackInterface *callbacks{nullptr};
  std::map<std::string, std::unique_ptr<ModelLibrary>> modelLibraries;
};

}  // namespace core
