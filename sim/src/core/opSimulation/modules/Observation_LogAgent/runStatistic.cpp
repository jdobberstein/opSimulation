/********************************************************************************
 * Copyright (c) 2016-2020 ITK Engineering GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  runStatistic.cpp */
//-----------------------------------------------------------------------------

#include "runStatistic.h"

#include <libxml/xmlwriter.h>

#include "common/xmlParser.h"

using namespace SimulationCommon;

RunStatistic::RunStatistic(std::uint32_t randomSeed) : _randomSeed(randomSeed) {}

void RunStatistic::AddStopReason(int time, StopReason reason)
{
  _stopReasonIdx = static_cast<int>(reason);
  StopTime = time;
}

// ----------------------------- writing out --------------------- //
void RunStatistic::WriteStatistics(xmlTextWriterPtr fileStream)
{
  xmlTextWriterStartElement(fileStream, toXmlChar("RandomSeed"));
  xmlTextWriterWriteString(fileStream, toXmlChar(std::to_string(_randomSeed)));
  xmlTextWriterEndElement(fileStream);

  xmlTextWriterStartElement(fileStream, toXmlChar("VisibilityDistance"));
  xmlTextWriterWriteString(fileStream, toXmlChar(std::to_string(VisibilityDistance.value())));
  xmlTextWriterEndElement(fileStream);

  xmlTextWriterStartElement(fileStream, toXmlChar("StopReason"));
  xmlTextWriterWriteString(fileStream, toXmlChar(std::string(StopReasonsStrings.at(_stopReasonIdx))));
  xmlTextWriterEndElement(fileStream);

  xmlTextWriterStartElement(fileStream, toXmlChar("StopTime"));
  xmlTextWriterWriteString(fileStream, toXmlChar(std::to_string(StopTime)));
  xmlTextWriterEndElement(fileStream);

  xmlTextWriterStartElement(fileStream, toXmlChar("EgoAccident"));
  xmlTextWriterWriteString(fileStream, toXmlChar(BoolToString(EgoCollision)));
  xmlTextWriterEndElement(fileStream);

  double totalDistanceTraveled = 0;
  for (const auto& [agent, distance] : distanceTraveled)
  {
    totalDistanceTraveled += distance;
  }

  xmlTextWriterStartElement(fileStream, toXmlChar("TotalDistanceTraveled"));
  xmlTextWriterWriteString(fileStream, toXmlChar(std::to_string(totalDistanceTraveled)));
  xmlTextWriterEndElement(fileStream);

  xmlTextWriterStartElement(fileStream, toXmlChar("EgoDistanceTraveled"));
  xmlTextWriterWriteString(fileStream, toXmlChar(std::to_string(distanceTraveled.at("0"))));
  xmlTextWriterEndElement(fileStream);
}

std::string RunStatistic::BoolToString(bool b)
{
  return b ? "True" : "False";
}
