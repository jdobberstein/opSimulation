/********************************************************************************
 * Copyright (c) 2017-2020 ITK Engineering GmbH
 *               2019-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <filesystem>
#include <fstream>
#include <iostream>
#include <libxml/xmlwriter.h>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "common/openPassTypes.h"
#include "common/sensorDefinitions.h"
#include "include/dataBufferInterface.h"
#include "observationLogConstants.h"
#include "runStatistic.h"

class ObservationCyclics;
class RunResultInterface;

struct Event
{
  // required for gcc-9
  /// @brief Event constructor
  /// @param time     Time of event occurance
  /// @param dataRow  Reference to the Acyclic Row
  Event(const int& time, openpass::databuffer::AcyclicRow dataRow) : time(time), dataRow(std::move(dataRow)) {}

  int time;
  openpass::databuffer::AcyclicRow dataRow;
};

using Events = std::vector<Event>;

//-----------------------------------------------------------------------------
/** \brief Provides the basic logging and observer functionality.
 *   \details Provides the basic logging functionality.
 *            Writes the framework, events, agents and samples into the output.
 *            It has two logging modes either event based or as single output.
 *            This module also acts as an observer of agent modules.
 *
 *   \ingroup ObservationLog
 */
//-----------------------------------------------------------------------------
class ObservationFileHandler
{
public:
  const std::string COMPONENTNAME = "ObservationFileHandler";

  /// @brief Copy constructor
  /// @param dataBuffer Another data buffer read interface
  ObservationFileHandler(const DataBufferReadInterface& dataBuffer);
  ObservationFileHandler(const ObservationFileHandler&) = delete;
  ObservationFileHandler(ObservationFileHandler&&) = delete;
  ObservationFileHandler& operator=(const ObservationFileHandler&) = delete;
  ObservationFileHandler& operator=(ObservationFileHandler&&) = delete;
  ~ObservationFileHandler() = default;

  /*!
   * \brief Sets the directory where the output is written into
   *
   * \param outputDir     Path of output directory
   * \param outputFile    Path of output file
   */
  void SetOutputLocation(const std::string& outputDir, const std::string& outputFile)
  {
    folder = outputDir;
    finalFilename = outputFile;
    finalPath = folder / finalFilename;
  }

  /// @brief Setter function to set scenery file
  /// @param fileName Name of the scenery file
  void SetSceneryFile(const std::string& fileName) { sceneryFile = fileName; }

  /// @brief Setter function to set csv output
  /// @param writeCsv Boolean to write csv output
  void SetCsvOutput(bool writeCsv) { writeCyclicsToCsv = writeCsv; }

  /*!
   * \brief Creates the output file as simulationOutput.tmp and writes the basic header information
   *
   * \param[in] frameworkVersion  Framework version
   */
  void WriteStartOfFile(const std::string& frameworkVersion);

  /*!
   * \brief This function gets called after each run and writes all information about this run into the output file
   *
   * \param runResult
   * \param runStatistic
   * \param cyclics
   * \param events
   */
  void WriteRun([[maybe_unused]] const RunResultInterface& runResult,
                RunStatistic runStatistic,
                ObservationCyclics& cyclics,
                Events& events);

  /*!
   * \brief Closes the xml tags flushes the output file, closes it and renames it to simulationOutput.xlm
   */
  void WriteEndOfFile();

private:
  xmlTextWriterPtr xmlFileStream{};
  const DataBufferReadInterface& dataBuffer;

  int runNumber = 0;  //!< run number
  std::string sceneryFile;

  bool writeCyclicsToCsv{false};

  OutputAttributes outputAttributes;
  OutputTags outputTags;

  std::filesystem::path folder;
  std::filesystem::path tmpFilename;
  std::filesystem::path finalFilename;
  std::filesystem::path tmpFilePath;
  std::filesystem::path finalPath;
  std::ofstream xmlFile;
  std::ofstream csvFile;

  //add infos to the file stream
  /*!
   * \brief Returns whether the agent has sensors or not.
   *
   * @return       true if agent has sensors, otherwise false.
   */
  inline bool ContainsSensor(const openpass::sensors::Parameters& sensorParameters) const;

  /*!
   * \brief Writes the sensor information into the simulation output.
   *
   * @param[in]    fStream             Shared pointer of the stream writer.
   * @param[in]    sensorParameter    Parameters of the sensor.
   */
  void AddSensor(const std::string& agentId, const std::string& sensorId);

  /*!
   * \brief Writes the sensor information into the simulation output.
   *
   * @param[in]    fStream             Shared pointer of the stream writer.
   * @param[in]    vehicleModelParameters      Parameters of the vehicle.
   */
  void AddVehicleAttributes(const std::string& agentId);

  /*!
   * \brief Writes all sensor information of an agent into the simulation output.
   *
   * @param[in]     fStream    Shared pointer of the stream writer.
   * @param[in]     agent      Pointer to the agent.
   */
  void AddSensors(const std::string& agentId);

  /*!
   * \brief Writes the content of an agent into the simulation output.
   *
   * @param[in]     fStream    Shared pointer of the stream writer.
   * @param[in]     agent      Pointer to the agent.
   */
  void AddAgent(const std::string& agentId);

  /*!
   * \brief Writes the content of all agent into the simulation output.
   *
   * @param[in]     fStream    Shared pointer of the stream writer.
   */
  void AddAgents();

  /*!
   * \brief Writes all events into the simulation output.
   *
   * @param[in]     fStream            Shared pointer of the stream writer.
   */
  void AddEvents(Events& events);

  /*!
   * \brief Writes the header into the simulation output during full logging.
   *
   * @param[in]     fStream            Shared pointer of the stream writer.
   */
  void AddHeader(ObservationCyclics& cyclics);

  /*!
   * \brief Writes the samples into the simulation output during full logging.
   *
   * @param[in]     fStream            Shared pointer of the stream writer.
   */
  void AddSamples(ObservationCyclics& cyclics);

  /*!
   * \brief Writes the filename for the cyclics file into the simulation output during full logging.
   *
   * @param[in]     filename           Name of the file, where cyclics are written to.
   */
  void AddReference(const std::string& filename);

  /*!
   * \brief Writes the cyclics of one run to a csv.
   *
   * @param[in]    runId               Id of the current run
   * @param[in]    cyclics             Cyclics of the current run
   */
  void WriteCsvCyclics(const std::filesystem::path& runId, ObservationCyclics& cyclics);

  /*!
   * \brief Write entities to XML
   *
   * \param tag       tag name
   * \param entities  list of entities
   * \param mandatory if set, an emtpy tag is added if entities is empty (default false)
   */
  void WriteEntities(const char* tag, const openpass::type::EntityIds& entities, bool mandatory = false);

  /*!
   * \brief Write (event) parameter to XML
   *
   * \remark Might be used for generic parameters in the future - right now, only event parameters
   *
   * \param[in]    parameters  list of parameters
   * \param[in]    mandatory   if set, an emtpy tag is added if parameters are empty (default false)
   */
  void WriteParameter(const openpass::type::FlatParameter& parameters, bool mandatory = false);

private:
  static constexpr const char* outputFileVersion = "0.2.1";
};
