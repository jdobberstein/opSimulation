/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SpawnerPreRunCommon.h"

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/route_definition.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_entity_repository.h>
#include <cmath>
#include <ostream>
#include <stdexcept>
#include <utility>
#include <vector>

#include "PreRunCommon/SpawnerPreRunCommonDefinitions.h"
#include "SpawnerPreRunCommonParameterExtractor.h"
#include "common/WorldAnalyzer.h"
#include "common/commonHelper.h"
#include "common/commonTools.h"
#include "common/globalDefinitions.h"
#include "common/sampler.h"
#include "include/agentBlueprintInterface.h"
#include "include/agentBlueprintProviderInterface.h"
#include "include/callbackInterface.h"
#include "include/entityRepositoryInterface.h"
#include "include/streamInterface.h"
#include "include/worldInterface.h"

SpawnerPreRunCommon::SpawnerPreRunCommon(const SpawnPointDependencies *dependencies, const CallbackInterface *callbacks)
    : SpawnPointInterface(dependencies->world, callbacks),
      dependencies(*dependencies),
      parameters(ExtractSpawnZonesParameters(*(dependencies->parameters.value()), *dependencies->world, callbacks)),
      worldAnalyzer(dependencies->world)
{
}

void SpawnerPreRunCommon::Trigger([[maybe_unused]] int time)
{
  for (const auto &spawnArea : parameters.spawnAreas)
  {
    const auto validLaneSpawningRanges = worldAnalyzer.GetValidLaneSpawningRanges(
        spawnArea.laneStream, spawnArea.sStart, spawnArea.sEnd, supportedLaneTypes);

    for (const auto &validSpawningRange : validLaneSpawningRanges)
    {
      GenerateAgentsForRange(spawnArea.laneStream, validSpawningRange);
    }
  }
}

SpawningAgentProfile SpawnerPreRunCommon::SampleAgentProfile(bool rightLane)
{
  return Sampler::Sample(
      rightLane ? parameters.agentProfileLaneMaps.rightLanes : parameters.agentProfileLaneMaps.leftLanes,
      dependencies.stochastics);
}

void SpawnerPreRunCommon::GenerateAgentsForRange(const std::unique_ptr<LaneStreamInterface> &laneStream,
                                                 const Range &range)
{
  bool generating = true;

  size_t rightLaneCount = worldAnalyzer.GetRightLaneCount(laneStream, range.second);

  while (generating)
  {
    const auto agentProfile = SampleAgentProfile(rightLaneCount == 0);

    try
    {
      const auto vehicleModelName = dependencies.agentBlueprintProvider->SampleVehicleModelName(agentProfile.name);

      const auto vehicleProperties = helper::map::query(*dependencies.vehicles, vehicleModelName);
      THROWIFFALSE(vehicleProperties,
                   "Unkown VehicleModel \"" + vehicleModelName + "\" in AgentProfile \"" + agentProfile.name + "\"");

      const auto agentLength = vehicleProperties.value()->bounding_box.dimension.length;
      const auto agentFrontLength = agentLength * 0.5 + vehicleProperties.value()->bounding_box.geometric_center.x;
      const auto agentRearLength = agentLength - agentFrontLength;

      units::velocity::meters_per_second_t velocity{
          Sampler::RollForStochasticAttribute(agentProfile.velocity, dependencies.stochastics)};

      for (size_t iterator = 0; iterator < rightLaneCount; ++iterator)
      {
        double homogeneity = agentProfile.homogeneities.size() > iterator ? agentProfile.homogeneities[iterator] : 1.0;
        velocity *= 2.0 - homogeneity;
      }

      const units::time::second_t tGap{
          Sampler::RollForStochasticAttribute(agentProfile.tGap, dependencies.stochastics)};

      const auto spawnInfo = GetNextSpawnCarInfo(laneStream, range, tGap, velocity, agentFrontLength, agentRearLength);
      if (!spawnInfo.has_value())
      {
        generating = false;
        break;
      }
      if (!worldAnalyzer.AreSpawningCoordinatesValid(spawnInfo->roadId,
                                                     spawnInfo->laneId,
                                                     spawnInfo->s,
                                                     0_m,
                                                     units::length::meter_t(GetStochasticOrPredefinedValue(
                                                         parameters.minimumSeparationBuffer, dependencies.stochastics)),
                                                     spawnInfo->spawnParameter.route,
                                                     vehicleProperties.value())
          || worldAnalyzer.SpawnWillCauseCrash(
              laneStream, spawnInfo->streamPosition, agentFrontLength, agentRearLength, velocity, Direction::BACKWARD))
      {
        generating = false;
        break;
      }

      auto entityId = SpawnPointDefinitions::CreateSpawnReadyEntity(
          {agentProfile.name, "A", vehicleProperties.value(), spawnInfo->spawnParameter}, dependencies.environment);

      dependencies.environment->AssignRoute(entityId, spawnInfo->spawnParameter.routeDefinition);
      auto &entityRepository{
          dynamic_cast<core::EntityRepositoryInterface &>(dependencies.environment->GetEntityRepository())};

      if (!entityRepository.SpawnReadyAgents())
      {
        generating = false;
        break;
      }

      rightLaneCount = worldAnalyzer.GetRightLaneCount(laneStream, spawnInfo->streamPosition);
    }
    catch (const std::runtime_error &error)
    {
      LogError(error.what());
    }
  }
}

std::optional<SpawnInfo> SpawnerPreRunCommon::GetNextSpawnCarInfo(
    const std::unique_ptr<LaneStreamInterface> &laneStream,
    const Range &range,
    const units::time::second_t gapInSeconds,
    const units::velocity::meters_per_second_t velocity,
    const units::length::meter_t agentFrontLength,
    const units::length::meter_t agentRearLength) const
{
  auto spawnDistance
      = worldAnalyzer.GetNextSpawnPosition(laneStream,
                                           range,
                                           agentFrontLength,
                                           agentRearLength,
                                           velocity,
                                           gapInSeconds,
                                           units::length::meter_t(GetStochasticOrPredefinedValue(
                                               parameters.minimumSeparationBuffer, dependencies.stochastics)));

  if (!spawnDistance.has_value())
  {
    return {};
  }

  auto roadPosition = laneStream->GetRoadPosition({spawnDistance.value(), 0_m});

  if (roadPosition.roadId.empty())
  {
    return {};
  }

  // TODO: Align datatype of LaneId (int) with mantle_api::LaneId (int64_t))
  auto route
      = worldAnalyzer.SampleRoute(roadPosition.roadId, static_cast<int>(roadPosition.laneId), dependencies.stochastics);
  auto routeDefinition = worldAnalyzer.ConvertToMantleRouteWayPoints(route);

  //We don't want to spawn agents too close to the end of lane, so adjust the spawn position if neccessary
  spawnDistance = worldAnalyzer.CalculateAdjustedSpawnDistanceToEndOfLane(static_cast<int>(roadPosition.laneId),
                                                                          roadPosition.roadPosition.s,
                                                                          spawnDistance.value(),
                                                                          velocity,
                                                                          route,
                                                                          supportedLaneTypes);
  if (spawnDistance.value() < range.first)
  {
    return {};
  }
  roadPosition = laneStream->GetRoadPosition({spawnDistance.value(), 0_m});

  const auto adjustedVelocity = worldAnalyzer.CalculateSpawnVelocityToPreventCrashing(
      laneStream, *spawnDistance, agentFrontLength, agentRearLength, velocity);

  auto worldPosition = GetWorld()->LaneCoord2WorldCoord(
      roadPosition.roadPosition.s, roadPosition.roadPosition.t, roadPosition.roadId, roadPosition.laneId);
  if (!worldPosition)
  {
    return {};
  }

  //considers adjusted velocity in curvatures
  auto spawnV = adjustedVelocity;
  auto kappa = worldPosition->curvature.value();
  if (kappa != 0.0)
  {
    double curvatureVelocity = 160 * (1 / (std::sqrt((std::abs(kappa)) / 1000)));

    spawnV = units::math::min(spawnV, units::velocity::meters_per_second_t(curvatureVelocity));
  }

  SpawnInfo spawnInfo;

  spawnInfo.roadId = roadPosition.roadId;
  spawnInfo.laneId = static_cast<int>(roadPosition.laneId);
  spawnInfo.s = roadPosition.roadPosition.s;
  spawnInfo.streamPosition = spawnDistance.value();
  spawnInfo.spawnParameter.position = {worldPosition->xPos, worldPosition->yPos, 0_m};
  spawnInfo.spawnParameter.orientation
      = {CommonHelper::SetAngleToValidRange(worldPosition->yawAngle
                                            + (roadPosition.laneId < 0 ? 0_rad : units::angle::radian_t(M_PI))),
         0_rad,
         0_rad};
  spawnInfo.spawnParameter.velocity = spawnV;
  spawnInfo.spawnParameter.acceleration = 0.0_mps_sq;
  spawnInfo.spawnParameter.route = route;
  spawnInfo.spawnParameter.routeDefinition = routeDefinition;

  return spawnInfo;
}

void SpawnerPreRunCommon::LogError(const std::string &message)
{
  std::stringstream log;
  log.str(std::string());
  log << COMPONENTNAME << " " << message;
  LOG(CbkLogLevel::Error, log.str());
  throw std::runtime_error(log.str());
}
