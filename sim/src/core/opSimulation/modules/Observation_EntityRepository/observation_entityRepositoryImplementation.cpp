/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  observation_entityRepositoryImplementation.cpp */
//-----------------------------------------------------------------------------

#include "observation_entityRepositoryImplementation.h"

#include <algorithm>
#include <chrono>
#include <cstdint>
#include <exception>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <variant>
#include <vector>

#include "common/openPassUtils.h"
#include "common/runtimeInformation.h"
#include "include/callbackInterface.h"
#include "include/parameterInterface.h"

class RunResultInterface;
class StochasticsInterface;
class WorldInterface;

namespace fs = std::filesystem;

ObservationEntityRepositoryImplementation::ObservationEntityRepositoryImplementation(
    StochasticsInterface *stochastics,
    WorldInterface *world,
    const ParameterInterface *parameters,
    const CallbackInterface *callbacks,
    DataBufferReadInterface *dataBuffer)
    : ObservationInterface(stochastics, world, parameters, callbacks, dataBuffer),
      dataBuffer(dataBuffer),
      directory(parameters->GetRuntimeInformation().directories.output)
{
  const auto &parametersString = parameters->GetParametersString();
  const auto filenamePrefixParameter = parametersString.find("FilenamePrefix");
  if (filenamePrefixParameter != parametersString.cend())
  {
    filenamePrefix = filenamePrefixParameter->second;
  }

  const auto persistentParameter = parametersString.find("WritePersistentEntities");
  if (persistentParameter != parametersString.cend())
  {
    if (persistentParameter->second == "Consolidated")
    {
      writePersitent = true;
      persistentInSeparateFile = false;
    }
    else if (persistentParameter->second == "Separate")
    {
      writePersitent = true;
      persistentInSeparateFile = true;
    }
    else if (persistentParameter->second == "Skip")
    {
      writePersitent = false;
    }
    else
    {
      LOGERRORANDTHROW("Unknown parameter \"" + persistentParameter->second + "\"")
    }
  }
}

void ObservationEntityRepositoryImplementation::OpSimulationPreHook()
{
  for (const auto &entry : fs::directory_iterator(directory))
  {
    const fs::path &fileInfo = entry.path();
    if (fs::is_regular_file(fileInfo) && fileInfo.extension() == ".csv"
        && fileInfo.stem().string().find(filenamePrefix + "_") == 0)
    {
      fs::remove(entry);
    }
  }
}

void ObservationEntityRepositoryImplementation::OpSimulationPreRunHook() {}

void ObservationEntityRepositoryImplementation::OpSimulationPostRunHook(
    [[maybe_unused]] const RunResultInterface &runResult)
{
  LOGDEBUG("Getting non-persistent entities");
  const auto non_persistent = GetEntities(NON_PERSISTENT_ENTITIES);

  if (writePersitent && runNumber == 0)
  {
    LOGDEBUG("Getting persistent entities");
    persistentEntities = GetEntities(PERSISTENT_ENTITIES);
  }

  std::string runPrefix;
  if (runNumber < 10)
  {
    runPrefix = "00";
  }
  else if (runNumber < 100)
  {
    runPrefix = "0";
  }

  std::string filename = filenamePrefix + "_Run_" + runPrefix + std::to_string(runNumber) + ".csv";

  fs::path path = directory / filename;
  std::ofstream csvFile(path.string());
  if (!csvFile.is_open())
  {
    LOGERRORANDTHROW(COMPONENTNAME + " could not create file: " + path.string());
  }

  std::stringstream stream;

  const auto header = openpass::utils::vector::to_string(CSV_HEADERS, CSV_DELIMITER);

  LOGDEBUG("Writing non-persistent entities");
  stream << header.c_str() << CSV_DELIMITER << '\n';
  WriteEntities(non_persistent, stream);

  if (writePersitent)
  {
    if (persistentInSeparateFile)
    {
      if (runNumber == 0)
      {
        LOGDEBUG("Writing persistent entities (once only)");
        std::string filename = filenamePrefix + "_Persistent.csv";
        fs::path path = directory / filename;
        std::ofstream persistentFile(path.string());
        if (!persistentFile.is_open())
        {
          LOGERROR(COMPONENTNAME + " could not create file: " + filename);
        }
        std::stringstream persistentStream;

        persistentStream << header.c_str() << CSV_DELIMITER << '\n';
        WriteEntities(persistentEntities, persistentStream);
        persistentFile << persistentStream.str();
        persistentFile.flush();
        persistentFile.close();
      }
    }
    else
    {
      LOGDEBUG("Writing persistent entities");
      WriteEntities(persistentEntities, stream);
    }
  }

  csvFile << stream.str();
  csvFile.flush();
  csvFile.close();

  ++runNumber;
}

void ObservationEntityRepositoryImplementation::OpSimulationPostHook() {}

std::string ObservationEntityRepositoryImplementation::GetEntitySource(const std::string &source_key)
{
  try
  {
    return std::get<std::string>(dataBuffer->GetStatic(source_key).at(0));
  }
  catch (const std::exception &e)
  {
    LOGERROR("While parsing databuffer key " + source_key + ": " + e.what());
    return "<error>";
  }
}

Metainfo ObservationEntityRepositoryImplementation::GetEntityMetainfo(const std::string &metainfo_key)
{
  Metainfo metainfo;
  try
  {
    const auto metainfoKeys = dataBuffer->GetKeys("Statics/" + metainfo_key);
    for (const auto &subkey : metainfoKeys)
    {
      auto value = dataBuffer->GetStatic(std::string(metainfo_key).append("/").append(subkey));
      metainfo.emplace(subkey, to_string(value));
    }
  }
  catch (const std::exception &e)
  {
    LOGERROR("While parsing databuffer key " + metainfo_key + ": " + e.what());
  }
  return metainfo;
}

std::vector<CsvRow> ObservationEntityRepositoryImplementation::GetEntities(const std::string &entityBaseKey)
{
  const auto entityIdStrings = dataBuffer->GetKeys("Statics/" + entityBaseKey);

  // entries in databuffer are not sorted
  std::vector<uint64_t> entityIds;
  entityIds.reserve(entityIdStrings.size());
  std::transform(entityIdStrings.cbegin(),
                 entityIdStrings.cend(),
                 std::back_inserter(entityIds),
                 [](const std::string &s) { return stoi(s); });
  std::sort(entityIds.begin(), entityIds.end());

  std::vector<CsvRow> csvRows;

  for (const auto &entityId : entityIds)
  {
    const auto entityKey = entityBaseKey + "/" + std::to_string(entityId);
    csvRows.emplace_back(entityId, GetEntitySource(entityKey + "/Source"), GetEntityMetainfo(entityKey + "/Metainfo"));
  }

  return csvRows;
}

void ObservationEntityRepositoryImplementation::WriteEntities(const std::vector<CsvRow> &entities,
                                                              std::stringstream &stream)
{
  for (const auto &row : entities)
  {
    stream << std::to_string(row.id).c_str() << CSV_DELIMITER << row.source.c_str() << CSV_DELIMITER;

    for (const auto &columnMapping : CSV_METAINFO)
    {
      for (const auto &column : columnMapping)
      {
        if (const auto &iter = row.metainfo.find(column); iter != row.metainfo.cend())
        {
          stream << iter->second.c_str();
        }
        stream << CSV_DELIMITER;
      }
    }
    stream << '\n';
  }
}

const std::string ObservationEntityRepositoryImplementation::to_string(const openpass::databuffer::Values &values)
{
  if (values.empty())
  {
    LOGERROR("Retrieved value from databuffer contains no element");
    return "<error>";
  }

  if (values.size() > 1)
  {
    LOGWARN("Multiple values per databuffer request currently not supported");
  }

  return std::visit(openpass::utils::FlatParameter::to_string(), values.front());
}
