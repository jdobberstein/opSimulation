/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "MovingObject.h"

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <limits>
#include <osi3/osi_groundtruth.pb.h>
#include <stdexcept>
#include <utility>
#include <vector>

#include "OWL/DataTypes.h"
#include "common/commonHelper.h"
#include "common/hypot.h"

OWL::Implementation::MovingObject::MovingObject(osi3::MovingObject *osiMovingObject) : osiObject{osiMovingObject}
{
  DefaultMovingObjectFactory dmof;
  dmof.AssignDefaultValues(osiMovingObject);
  SetIndicatorState(IndicatorState::IndicatorState_Off);
  SetBrakeLightState(false);
  SetHeadLight(false);
  SetHighBeamLight(false);
}

void OWL::Implementation::MovingObject::CopyToGroundTruth(osi3::GroundTruth &target) const
{
  auto *newMovingObject = target.add_moving_object();
  newMovingObject->CopyFrom(*osiObject);
}

OWL::Id OWL::Implementation::MovingObject::GetId() const
{
  return osiObject->id().value();
}

mantle_api::Dimension3 OWL::Implementation::MovingObject::GetDimension() const
{
  return OWL::GetDimensionFromOsiObject(osiObject);
}

units::length::meter_t OWL::Implementation::MovingObject::GetDistanceReferencePointToLeadingEdge() const
{
  return units::length::meter_t(osiObject->base().dimension().length() * 0.5
                                - osiObject->vehicle_attributes().bbcenter_to_rear().x());
}

void OWL::Implementation::MovingObject::SetDimension(const mantle_api::Dimension3 &newDimension)
{
  osi3::Dimension3d *osiDimension = osiObject->mutable_base()->mutable_dimension();

  osiDimension->set_length(units::unit_cast<double>(newDimension.length));
  osiDimension->set_width(units::unit_cast<double>(newDimension.width));
  osiDimension->set_height(units::unit_cast<double>(newDimension.height));
}

void OWL::Implementation::MovingObject::SetLength(const units::length::meter_t newLength)
{
  osi3::Dimension3d *osiDimension = osiObject->mutable_base()->mutable_dimension();
  osiDimension->set_length(units::unit_cast<double>(newLength));
}

void OWL::Implementation::MovingObject::SetWidth(const units::length::meter_t newWidth)
{
  osi3::Dimension3d *osiDimension = osiObject->mutable_base()->mutable_dimension();
  osiDimension->set_width(units::unit_cast<double>(newWidth));
}

void OWL::Implementation::MovingObject::SetHeight(const units::length::meter_t newHeight)
{
  osi3::Dimension3d *osiDimension = osiObject->mutable_base()->mutable_dimension();
  osiDimension->set_height(units::unit_cast<double>(newHeight));
}

void OWL::Implementation::MovingObject::SetBoundingBoxCenterToRear(const units::length::meter_t distanceX,
                                                                   const units::length::meter_t distanceY,
                                                                   const units::length::meter_t distanceZ)
{
  osiObject->mutable_vehicle_attributes()->mutable_bbcenter_to_rear()->set_x(units::unit_cast<double>(distanceX));
  osiObject->mutable_vehicle_attributes()->mutable_bbcenter_to_rear()->set_y(units::unit_cast<double>(distanceY));
  osiObject->mutable_vehicle_attributes()->mutable_bbcenter_to_rear()->set_z(units::unit_cast<double>(distanceZ));
}

void OWL::Implementation::MovingObject::SetBoundingBoxCenterToFront(const units::length::meter_t distanceX,
                                                                    const units::length::meter_t distanceY,
                                                                    const units::length::meter_t distanceZ)
{
  osiObject->mutable_vehicle_attributes()->mutable_bbcenter_to_front()->set_x(units::unit_cast<double>(distanceX));
  osiObject->mutable_vehicle_attributes()->mutable_bbcenter_to_front()->set_y(units::unit_cast<double>(distanceY));
  osiObject->mutable_vehicle_attributes()->mutable_bbcenter_to_front()->set_z(units::unit_cast<double>(distanceZ));
}

mantle_api::Vec3<units::length::meter_t> OWL::Implementation::MovingObject::GetReferencePointPosition() const
{
  const osi3::Vector3d &osiPosition = osiObject->base().position();
  const double yaw = osiObject->base().orientation().yaw();
  const osi3::Vector3d &bbCenterToRear = osiObject->vehicle_attributes().bbcenter_to_rear();

  mantle_api::Vec3<units::length::meter_t> position;

  position.x = units::length::meter_t(osiPosition.x() + std::cos(yaw) * bbCenterToRear.x());
  position.y = units::length::meter_t(osiPosition.y() + std::sin(yaw) * bbCenterToRear.x());
  position.z = units::length::meter_t(osiPosition.z());

  return position;
}

void OWL::Implementation::MovingObject::SetReferencePointPosition(
    const mantle_api::Vec3<units::length::meter_t> &newPosition)
{
  osi3::Vector3d *osiPosition = osiObject->mutable_base()->mutable_position();
  const auto yaw = osiObject->base().orientation().yaw();
  const osi3::Vector3d &bbCenterToRear = osiObject->vehicle_attributes().bbcenter_to_rear();

  osiPosition->set_x(units::unit_cast<double>(newPosition.x) - std::cos(yaw) * bbCenterToRear.x());
  osiPosition->set_y(units::unit_cast<double>(newPosition.y) - std::sin(yaw) * bbCenterToRear.x());
  osiPosition->set_z(units::unit_cast<double>(newPosition.z));

  frontDistance.Invalidate();
  rearDistance.Invalidate();
}

void OWL::Implementation::MovingObject::SetX(const units::length::meter_t newX)
{
  osi3::Vector3d *osiPosition = osiObject->mutable_base()->mutable_position();
  const double yaw = osiObject->base().orientation().yaw();
  const osi3::Vector3d &bbCenterToRear = osiObject->vehicle_attributes().bbcenter_to_rear();

  osiPosition->set_x(units::unit_cast<double>(newX) - std::cos(yaw) * bbCenterToRear.x());
}

void OWL::Implementation::MovingObject::SetY(const units::length::meter_t newY)
{
  osi3::Vector3d *osiPosition = osiObject->mutable_base()->mutable_position();
  const double yaw = osiObject->base().orientation().yaw();
  const osi3::Vector3d &bbCenterToRear = osiObject->vehicle_attributes().bbcenter_to_rear();

  osiPosition->set_y(units::unit_cast<double>(newY) - std::sin(yaw) * bbCenterToRear.x());
}

void OWL::Implementation::MovingObject::SetZ(const units::length::meter_t newZ)
{
  osi3::Vector3d *osiPosition = osiObject->mutable_base()->mutable_position();
  osiPosition->set_z(units::unit_cast<double>(newZ));
}

void OWL::Implementation::MovingObject::SetTouchedRoads(const RoadIntervals &touchedRoads)
{
  this->touchedRoads = &touchedRoads;
}

mantle_api::Orientation3<units::angle::radian_t> OWL::Implementation::MovingObject::GetAbsOrientation() const
{
  osi3::Orientation3d osiOrientation = osiObject->base().orientation();

  return {units::angle::radian_t(osiOrientation.yaw()),
          units::angle::radian_t(osiOrientation.pitch()),
          units::angle::radian_t(osiOrientation.roll())};
}

void OWL::Implementation::MovingObject::SetAbsOrientation(
    const mantle_api::Orientation3<units::angle::radian_t> &newOrientation)
{
  osi3::Orientation3d *osiOrientation = osiObject->mutable_base()->mutable_orientation();
  const auto referencePosition = GetReferencePointPosition();  //AbsPosition needs to be evaluated with "old" yaw
  osiOrientation->set_yaw(units::unit_cast<double>(CommonHelper::SetAngleToValidRange(newOrientation.yaw)));
  osiOrientation->set_pitch(units::unit_cast<double>(CommonHelper::SetAngleToValidRange(newOrientation.pitch)));
  osiOrientation->set_roll(units::unit_cast<double>(CommonHelper::SetAngleToValidRange(newOrientation.roll)));
  frontDistance.Invalidate();
  rearDistance.Invalidate();
  SetReferencePointPosition(referencePosition);  //Changing yaw also changes position of the boundingBox center
}

void OWL::Implementation::MovingObject::SetYaw(const units::angle::radian_t newYaw)
{
  const auto referencePosition = GetReferencePointPosition();
  osi3::Orientation3d *osiOrientation = osiObject->mutable_base()->mutable_orientation();
  osiOrientation->set_yaw(units::unit_cast<double>(CommonHelper::SetAngleToValidRange(newYaw)));
  frontDistance.Invalidate();
  rearDistance.Invalidate();
  SetReferencePointPosition(referencePosition);  //Changing yaw also changes position of the boundingBox center
}

void OWL::Implementation::MovingObject::SetPitch(const units::angle::radian_t newPitch)
{
  osi3::Orientation3d *osiOrientation = osiObject->mutable_base()->mutable_orientation();
  osiOrientation->set_pitch(units::unit_cast<double>(CommonHelper::SetAngleToValidRange(newPitch)));
}

void OWL::Implementation::MovingObject::SetRoll(const units::angle::radian_t newRoll)
{
  osi3::Orientation3d *osiOrientation = osiObject->mutable_base()->mutable_orientation();
  osiOrientation->set_roll(units::unit_cast<double>(CommonHelper::SetAngleToValidRange(newRoll)));
}

void OWL::Implementation::MovingObject::SetIndicatorState(IndicatorState indicatorState)
{
  if (indicatorState == IndicatorState::IndicatorState_Off)
  {
    osiObject->mutable_vehicle_classification()->mutable_light_state()->set_indicator_state(
        osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_OFF);
    return;
  }
  if (indicatorState == IndicatorState::IndicatorState_Left)
  {
    osiObject->mutable_vehicle_classification()->mutable_light_state()->set_indicator_state(
        osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_LEFT);
    return;
  }
  if (indicatorState == IndicatorState::IndicatorState_Right)
  {
    osiObject->mutable_vehicle_classification()->mutable_light_state()->set_indicator_state(
        osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_RIGHT);
    return;
  }
  if (indicatorState == IndicatorState::IndicatorState_Warn)
  {
    osiObject->mutable_vehicle_classification()->mutable_light_state()->set_indicator_state(
        osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_WARNING);
    return;
  }

  throw std::invalid_argument("Indicator state is not supported");
}

IndicatorState OWL::Implementation::MovingObject::GetIndicatorState() const
{
  const auto &osiState = osiObject->vehicle_classification().light_state().indicator_state();

  if (osiState == osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_OFF)
  {
    return IndicatorState::IndicatorState_Off;
  }
  if (osiState == osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_LEFT)
  {
    return IndicatorState::IndicatorState_Left;
  }
  if (osiState == osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_RIGHT)
  {
    return IndicatorState::IndicatorState_Right;
  }
  if (osiState == osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_WARNING)
  {
    return IndicatorState::IndicatorState_Warn;
  }

  throw std::logic_error("Indicator state is not supported");
}

void OWL::Implementation::MovingObject::SetBrakeLightState(bool brakeLightState)
{
  if (brakeLightState)
  {
    osiObject->mutable_vehicle_classification()->mutable_light_state()->set_brake_light_state(
        osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_NORMAL);
  }
  else
  {
    osiObject->mutable_vehicle_classification()->mutable_light_state()->set_brake_light_state(
        osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_OFF);
  }
}

bool OWL::Implementation::MovingObject::GetBrakeLightState() const
{
  const auto &osiState = osiObject->vehicle_classification().light_state().brake_light_state();

  if (osiState == osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_NORMAL)
  {
    return true;
  }
  if (osiState == osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_OFF)
  {
    return false;
  }

  throw std::logic_error("BrakeLightState is not supported");
}

void OWL::Implementation::MovingObject::SetHeadLight(bool headLight)
{
  if (headLight)
  {
    osiObject->mutable_vehicle_classification()->mutable_light_state()->set_head_light(
        osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_ON);
  }
  else
  {
    osiObject->mutable_vehicle_classification()->mutable_light_state()->set_head_light(
        osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  }
}

bool OWL::Implementation::MovingObject::GetHeadLight() const
{
  const auto &osiState = osiObject->vehicle_classification().light_state().head_light();

  if (osiState == osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_ON)
  {
    return true;
  }
  if (osiState == osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF)
  {
    return false;
  }

  throw std::logic_error("HeadLightState is not supported");
}

void OWL::Implementation::MovingObject::SetHighBeamLight(bool highbeamLight)
{
  if (highbeamLight)
  {
    osiObject->mutable_vehicle_classification()->mutable_light_state()->set_high_beam(
        osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_ON);
  }
  else
  {
    osiObject->mutable_vehicle_classification()->mutable_light_state()->set_high_beam(
        osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  }
}

bool OWL::Implementation::MovingObject::GetHighBeamLight() const
{
  const auto &osiState = osiObject->vehicle_classification().light_state().high_beam();

  if (osiState == osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_ON)
  {
    return true;
  }
  if (osiState == osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF)
  {
    return false;
  }

  throw std::logic_error("HighBeamLightState is not supported");
}

void OWL::Implementation::MovingObject::SetType(mantle_api::EntityType type)
{
  osi3::MovingObject_Type osiType{static_cast<int>(type)};
  osiObject->set_type(osiType);
}

void OWL::Implementation::MovingObject::SetVehicleClassification(mantle_api::VehicleClass vehicleClassification)
{
  if (vehicleClassification == mantle_api::VehicleClass::kInvalid)
  {
    throw(std::runtime_error("VehicleClassification \"Invalid\" not supported"));
  }

  osi3::MovingObject_VehicleClassification_Type osiClassification{static_cast<int>(vehicleClassification)};
  osiObject->mutable_vehicle_classification()->set_type(osiClassification);
}

OWL::Primitive::LaneOrientation OWL::Implementation::MovingObject::GetLaneOrientation() const
{
  throw std::logic_error("MovingObject::GetLaneOrientation not implemented");
}

mantle_api::Vec3<units::velocity::meters_per_second_t> OWL::Implementation::MovingObject::GetAbsVelocity() const
{
  const osi3::Vector3d osiVelocity = osiObject->base().velocity();

  return {units::velocity::meters_per_second_t(osiVelocity.x()),
          units::velocity::meters_per_second_t(osiVelocity.y()),
          units::velocity::meters_per_second_t(osiVelocity.z())};
}

units::velocity::meters_per_second_t OWL::Implementation::MovingObject::GetAbsVelocityDouble() const
{
  const auto velocity = GetAbsVelocity();
  mantle_api::Orientation3<units::angle::radian_t> orientation = GetAbsOrientation();
  double sign = 1.0;

  auto velocityAngle = units::math::atan2(velocity.y, velocity.x);

  if (units::math::abs(velocity.x) == 0.0_mps && units::math::abs(velocity.y) == 0.0_mps)
  {
    velocityAngle = 0.0_rad;
  }

  auto angleBetween = velocityAngle - orientation.yaw;

  if (units::math::abs(angleBetween) > 1_rad * M_PI_2 && units::math::abs(angleBetween) < 3_rad * M_PI_2)
  {
    sign = -1.0;
  }

  return openpass::hypot(velocity.x, velocity.y) * sign;
}

void OWL::Implementation::MovingObject::SetAbsVelocity(
    const mantle_api::Vec3<units::velocity::meters_per_second_t> &newVelocity)
{
  osi3::Vector3d *osiVelocity = osiObject->mutable_base()->mutable_velocity();

  osiVelocity->set_x(units::unit_cast<double>(newVelocity.x));
  osiVelocity->set_y(units::unit_cast<double>(newVelocity.y));
  osiVelocity->set_z(units::unit_cast<double>(newVelocity.z));
}

void OWL::Implementation::MovingObject::SetAbsVelocity(const units::velocity::meters_per_second_t newVelocity)
{
  osi3::Vector3d *osiVelocity = osiObject->mutable_base()->mutable_velocity();

  auto yaw = GetAbsOrientation().yaw;
  auto cos_val = units::math::cos(yaw);
  auto sin_val = units::math::sin(yaw);

  osiVelocity->set_x(units::unit_cast<double>(newVelocity * cos_val));
  osiVelocity->set_y(units::unit_cast<double>(newVelocity * sin_val));
  osiVelocity->set_z(0.0);
}

OWL::Primitive::AbsAcceleration OWL::Implementation::MovingObject::GetAbsAcceleration() const
{
  const osi3::Vector3d osiAcceleration = osiObject->base().acceleration();
  OWL::Primitive::AbsAcceleration acceleration{};

  acceleration.ax = units::acceleration::meters_per_second_squared_t(osiAcceleration.x());
  acceleration.ay = units::acceleration::meters_per_second_squared_t(osiAcceleration.y());
  acceleration.az = units::acceleration::meters_per_second_squared_t(osiAcceleration.z());

  return acceleration;
}

units::acceleration::meters_per_second_squared_t OWL::Implementation::MovingObject::GetAbsAccelerationDouble() const
{
  OWL::Primitive::AbsAcceleration acceleration = GetAbsAcceleration();
  mantle_api::Orientation3<units::angle::radian_t> orientation = GetAbsOrientation();
  double sign = 1.0;

  units::angle::radian_t accAngle = units::math::atan2(acceleration.ay, acceleration.ax);

  if (units::math::abs(acceleration.ax) == 0.0_mps_sq && units::math::abs(acceleration.ay) == 0.0_mps_sq)
  {
    accAngle = 0.0_rad;
  }

  const auto angleBetween = accAngle - orientation.yaw;

  if ((units::math::abs(angleBetween) - 1_rad * M_PI_2) > 0_rad)
  {
    sign = -1.0;
  }

  return openpass::hypot(acceleration.ax, acceleration.ay) * sign;
}

void OWL::Implementation::MovingObject::SetAbsAcceleration(const OWL::Primitive::AbsAcceleration &newAcceleration)
{
  osi3::Vector3d *osiAcceleration = osiObject->mutable_base()->mutable_acceleration();

  osiAcceleration->set_x(units::unit_cast<double>(newAcceleration.ax));
  osiAcceleration->set_y(units::unit_cast<double>(newAcceleration.ay));
  osiAcceleration->set_z(units::unit_cast<double>(newAcceleration.az));
}

void OWL::Implementation::MovingObject::SetAbsAcceleration(
    const units::acceleration::meters_per_second_squared_t newAcceleration)
{
  osi3::Vector3d *osiAcceleration = osiObject->mutable_base()->mutable_acceleration();

  auto yaw = GetAbsOrientation().yaw;
  auto cos_val = units::math::cos(yaw);
  auto sin_val = units::math::sin(yaw);

  osiAcceleration->set_x(newAcceleration.value() * cos_val);
  osiAcceleration->set_y(newAcceleration.value() * sin_val);
  osiAcceleration->set_z(0.0);
}

mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>
OWL::Implementation::MovingObject::GetAbsOrientationRate() const
{
  const osi3::Orientation3d osiOrientationRate = osiObject->base().orientation_rate();

  return {units::angular_velocity::radians_per_second_t(osiOrientationRate.yaw()),
          units::angular_velocity::radians_per_second_t(osiOrientationRate.pitch()),
          units::angular_velocity::radians_per_second_t(osiOrientationRate.roll())};
}

OWL::Primitive::AbsOrientationAcceleration OWL::Implementation::MovingObject::GetAbsOrientationAcceleration() const
{
  const osi3::Orientation3d osiOrientationAcceleration = osiObject->base().orientation_acceleration();

  return OWL::Primitive::AbsOrientationAcceleration{
      units::angular_acceleration::radians_per_second_squared_t(osiOrientationAcceleration.yaw()),
      units::angular_acceleration::radians_per_second_squared_t(osiOrientationAcceleration.pitch()),
      units::angular_acceleration::radians_per_second_squared_t(osiOrientationAcceleration.roll())};
}

void OWL::Implementation::MovingObject::SetAbsOrientationRate(
    const mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> &newOrientationRate)
{
  osi3::Orientation3d *osiOrientationRate = osiObject->mutable_base()->mutable_orientation_rate();

  osiOrientationRate->set_yaw(newOrientationRate.yaw.value());
  osiOrientationRate->set_pitch(newOrientationRate.pitch.value());
  osiOrientationRate->set_roll(newOrientationRate.roll.value());
}

void OWL::Implementation::MovingObject::SetAbsOrientationAcceleration(
    const OWL::Primitive::AbsOrientationAcceleration &newOrientationAcceleration)
{
  osi3::Orientation3d *osiOrientationAcceleration = osiObject->mutable_base()->mutable_orientation_acceleration();

  osiOrientationAcceleration->set_yaw(newOrientationAcceleration.yawAcceleration.value());
  osiOrientationAcceleration->set_pitch(newOrientationAcceleration.pitchAcceleration.value());
  osiOrientationAcceleration->set_roll(newOrientationAcceleration.rollAcceleration.value());
}

void OWL::Implementation::MovingObject::AddLaneAssignment(const Interfaces::Lane &lane,
                                                          const std::optional<RoadPosition> &referencePoint)
{
  osiObject->mutable_moving_object_classification()->add_assigned_lane_id()->set_value(lane.GetId());
  auto *logicalLaneAssignment = osiObject->mutable_moving_object_classification()->add_logical_lane_assignment();
  logicalLaneAssignment->mutable_assigned_lane_id()->set_value(lane.GetLogicalLaneId());
  if (referencePoint.has_value())
  {
    logicalLaneAssignment->set_s_position(referencePoint->s.value());
    logicalLaneAssignment->set_t_position(referencePoint->t.value());
    logicalLaneAssignment->set_angle_to_lane(referencePoint->hdg.value());
  }
  assignedLanes.push_back(&lane);
}

void OWL::Implementation::MovingObject::SetSourceReference(const OWL::ExternalReference &externalReference)
{
  auto *sourceReference = osiObject->add_source_reference();
  sourceReference->set_reference(externalReference.reference);
  sourceReference->set_type(externalReference.type);
  for (const auto &id : externalReference.identifier)
  {
    sourceReference->add_identifier(id);
  }
}

void OWL::Implementation::MovingObject::AddWheel(const WheelData &wheelData)
{
  osi3::MovingObject_VehicleAttributes_WheelData newWheel{};
  newWheel.set_width(wheelData.width.value());
  newWheel.set_rim_radius(wheelData.rim_radius.value());
  newWheel.set_rotation_rate(wheelData.rotation_rate.value());
  newWheel.set_wheel_radius(wheelData.wheelRadius.value());
  newWheel.set_axle(wheelData.axle);
  newWheel.set_index(wheelData.index);
  newWheel.mutable_position()->set_x(wheelData.position.x.value());
  newWheel.mutable_position()->set_y(wheelData.position.y.value());
  newWheel.mutable_position()->set_z(wheelData.position.z.value());
  newWheel.mutable_orientation()->set_pitch(wheelData.orientation.roll.value());
  newWheel.mutable_orientation()->set_roll(wheelData.orientation.pitch.value());
  newWheel.mutable_orientation()->set_yaw(wheelData.orientation.yaw.value());
  osiObject->mutable_vehicle_attributes()->mutable_wheel_data()->Add(std::move(newWheel));
  if (osiObject->mutable_vehicle_attributes()->number_wheels() == std::numeric_limits<uint32_t>::max())
  {
    osiObject->mutable_vehicle_attributes()->set_number_wheels(0);
  }
  uint32_t s = osiObject->mutable_vehicle_attributes()->number_wheels() + 1;
  osiObject->mutable_vehicle_attributes()->set_number_wheels(s);
}

const OWL::Interfaces::Lanes &OWL::Implementation::MovingObject::GetLaneAssignments() const
{
  return assignedLanes;
}

void OWL::Implementation::MovingObject::ClearLaneAssignments()
{
  osiObject->mutable_moving_object_classification()->mutable_assigned_lane_id()->Clear();
  osiObject->mutable_moving_object_classification()->mutable_logical_lane_assignment()->Clear();
  assignedLanes.clear();
}

OWL::Angle OWL::Implementation::MovingObject::GetSteeringWheelAngle()
{
  return units::angle::radian_t(osiObject->mutable_vehicle_attributes()->steering_wheel_angle());
}

void OWL::Implementation::MovingObject::SetSteeringWheelAngle(const Angle newValue)
{
  osiObject->mutable_vehicle_attributes()->set_steering_wheel_angle(newValue.value());
}

std::optional<const OWL::WheelData> OWL::Implementation::MovingObject::GetWheelData(unsigned int axleIndex,
                                                                                    unsigned int rowIndex)
{
  //const auto predicate = ;
  auto it = std::find_if(osiObject->vehicle_attributes().wheel_data().begin(),
                         osiObject->vehicle_attributes().wheel_data().end(),
                         [axleIndex, rowIndex](const osi3::MovingObject_VehicleAttributes_WheelData &wheel)
                         { return wheel.axle() == axleIndex && wheel.index() == rowIndex; });
  if (it == osiObject->vehicle_attributes().wheel_data().end())
  {
    return std::nullopt;
  }
  WheelData foundWheel{};
  foundWheel.SetFromOsi(&(*it));
  return foundWheel;
}

void OWL::Implementation::MovingObject::SetWheelOrientation(
    const int axleIndex, const int wheelIndex, const mantle_api::Vec3<units::angle::radian_t> wheelOrientation)
{
  std::for_each(osiObject->mutable_vehicle_attributes()->mutable_wheel_data()->begin(),
                osiObject->mutable_vehicle_attributes()->mutable_wheel_data()->end(),
                [axleIndex, wheelIndex, wheelOrientation](osi3::MovingObject_VehicleAttributes_WheelData &wheel)
                {
                  if (wheel.axle() == axleIndex && wheel.index() == wheelIndex)  // assuming steering axle is in front
                  {
                    wheel.mutable_orientation()->set_roll(wheelOrientation.x.value());
                    wheel.mutable_orientation()->set_pitch(wheelOrientation.y.value());
                    wheel.mutable_orientation()->set_yaw(wheelOrientation.z.value());
                  }
                });
}

void OWL::Implementation::MovingObject::SetWheelRotationRate(const int axleIndex,
                                                             const int wheelIndex,
                                                             units::angular_velocity::radians_per_second_t rotationRate)
{
  auto it = std::find_if(osiObject->mutable_vehicle_attributes()->mutable_wheel_data()->begin(),
                         osiObject->mutable_vehicle_attributes()->mutable_wheel_data()->end(),
                         [axleIndex, wheelIndex](const osi3::MovingObject_VehicleAttributes_WheelData &wheel)
                         { return wheel.axle() == axleIndex && wheel.index() == wheelIndex; });
  if (it != osiObject->vehicle_attributes().wheel_data().end())
  {
    it->set_rotation_rate(rotationRate.value());
  }
}

void OWL::Implementation::DefaultMovingObjectFactory::AssignDefaultValues(osi3::MovingObject *_osiMovingObject)
{
  AssignDefaultTypes();
  _osiMovingObject->mutable_id()->set_value(d_identifier.value());
  AssignDefaultBase(_osiMovingObject->mutable_base());
  _osiMovingObject->set_type(d_type);
  _osiMovingObject->mutable_assigned_lane_id()->Clear();
  AssignDefaultVehicleAttributes(_osiMovingObject->mutable_vehicle_attributes());
  AssignDefaultVehicleClassification(_osiMovingObject->mutable_vehicle_classification());
  _osiMovingObject->mutable_model_reference()->assign(model_reference);
  _osiMovingObject->mutable_future_trajectory()->Clear();
  AssignDefaultMovingObjectClassification(_osiMovingObject->mutable_moving_object_classification());
  //source_reference is optional and discarded
}

void OWL::Implementation::DefaultMovingObjectFactory::AssignDefaultTypes()
{
  d_type = osi3::MovingObject_Type_TYPE_OTHER;
  d_dimension.set_height(std::numeric_limits<double>::signaling_NaN());
  d_dimension.set_width(std::numeric_limits<double>::signaling_NaN());
  d_dimension.set_length(std::numeric_limits<double>::signaling_NaN());
  d_orientation.set_pitch(std::numeric_limits<double>::signaling_NaN());
  d_orientation.set_roll(std::numeric_limits<double>::signaling_NaN());
  d_orientation.set_yaw(std::numeric_limits<double>::signaling_NaN());
  d_3d.set_x(std::numeric_limits<double>::signaling_NaN());
  d_3d.set_y(std::numeric_limits<double>::signaling_NaN());
  d_3d.set_z(std::numeric_limits<double>::signaling_NaN());
  d_identifier.set_value(std::numeric_limits<google::protobuf::uint64>::max());
  d_uint32 = std::numeric_limits<google::protobuf::uint32>::max();
  d_double = std::numeric_limits<double>::signaling_NaN();
};

void OWL::Implementation::DefaultMovingObjectFactory::AssignDefaultBase(osi3::BaseMoving *const base)
{
  base->mutable_base_polygon()->Clear();
  base->mutable_dimension()->CopyFrom(d_dimension);
  base->mutable_orientation()->CopyFrom(d_orientation);
  base->mutable_orientation_rate()->CopyFrom(d_orientation);
  base->mutable_orientation_acceleration()->CopyFrom(d_orientation);
  base->mutable_position()->CopyFrom(d_3d);
  base->mutable_velocity()->CopyFrom(d_3d);
  base->mutable_acceleration()->CopyFrom(d_3d);
}

void OWL::Implementation::DefaultMovingObjectFactory::AssignDefaultVehicleAttributes(
    osi3::MovingObject_VehicleAttributes *const vehicleAttributes)
{
  vehicleAttributes->mutable_driver_id();  //not to be set if host_vehicle is set to false
  vehicleAttributes->radius_wheel();       //deprecated and moved to wheel data
  vehicleAttributes->set_number_wheels(0);
  vehicleAttributes->mutable_bbcenter_to_front()->CopyFrom(d_3d);
  vehicleAttributes->mutable_bbcenter_to_rear()->CopyFrom(d_3d);
  vehicleAttributes->set_ground_clearance(d_double);
  vehicleAttributes->mutable_wheel_data()->Clear();
  vehicleAttributes->set_steering_wheel_angle(d_double);
}

void OWL::Implementation::DefaultMovingObjectFactory::AssignDefaultVehicleClassification(
    osi3::MovingObject_VehicleClassification *const vehicleClassifcation)
{
  vehicleClassifcation->set_type(osi3::MovingObject_VehicleClassification_Type_TYPE_OTHER);
  vehicleClassifcation->set_role(osi3::MovingObject_VehicleClassification_Role_ROLE_CIVIL);
  vehicleClassifcation->set_has_trailer(false);
  vehicleClassifcation->trailer_id();  //Not set if trailer id is false
  vehicleClassifcation->mutable_light_state()->set_front_fog_light(
      osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  vehicleClassifcation->mutable_light_state()->set_rear_fog_light(
      osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  vehicleClassifcation->mutable_light_state()->set_head_light(
      osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  vehicleClassifcation->mutable_light_state()->set_high_beam(
      osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  vehicleClassifcation->mutable_light_state()->set_reversing_light(
      osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  vehicleClassifcation->mutable_light_state()->set_brake_light_state(
      osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_OFF);
  vehicleClassifcation->mutable_light_state()->set_license_plate_illumination_rear(
      osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  vehicleClassifcation->mutable_light_state()->set_emergency_vehicle_illumination(
      osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  vehicleClassifcation->mutable_light_state()->set_service_vehicle_illumination(
      osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF);
  vehicleClassifcation->mutable_light_state()->set_brake_light_state(
      osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_OFF);
}

void OWL::Implementation::DefaultMovingObjectFactory::AssignDefaultMovingObjectClassification(
    osi3::MovingObject_MovingObjectClassification *const objectClassifcation)
{
  objectClassifcation->mutable_assigned_lane_id()->Clear();
  objectClassifcation->mutable_assigned_lane_percentage()->Clear();
}
