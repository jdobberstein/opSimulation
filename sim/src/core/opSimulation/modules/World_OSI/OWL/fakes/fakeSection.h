/********************************************************************************
 * Copyright (c) 2018-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include "OWL/DataTypes.h"

namespace OWL::Fakes
{
class Section : public OWL::Interfaces::Section
{
public:
  MOCK_CONST_METHOD0(GetId, OWL::Id(void));
  MOCK_METHOD1(AddNext, void(const OWL::Interfaces::Section& section));
  MOCK_METHOD1(AddPrevious, void(const OWL::Interfaces::Section& section));
  MOCK_METHOD1(AddLane, void(const OWL::Interfaces::Lane& lane));
  MOCK_CONST_METHOD0(GetLanes, const OWL::Interfaces::Lanes&(void));
  MOCK_CONST_METHOD0(GetLength, units::length::meter_t(void));
  MOCK_CONST_METHOD0(GetSOffset, units::length::meter_t());
  MOCK_CONST_METHOD1(GetDistance, units::length::meter_t(OWL::MeasurementPoint mp));
  MOCK_CONST_METHOD1(Covers, bool(units::length::meter_t));
  MOCK_CONST_METHOD2(CoversInterval, bool(units::length::meter_t startDistance, units::length::meter_t endDistance));
  MOCK_METHOD1(SetRoad, void(OWL::Interfaces::Road*));
  MOCK_CONST_METHOD0(GetRoad, const OWL::Interfaces::Road&(void));
  MOCK_METHOD1(SetCenterLaneBoundary, void(std::vector<OWL::Id> laneBoundaryId));
  MOCK_CONST_METHOD0(GetCenterLaneBoundary, std::vector<OWL::Id>());
  MOCK_METHOD1(SetCenterLogicalLaneBoundary, void(std::vector<Id> logicalLaneBoundaryId));
  MOCK_CONST_METHOD0(GetCenterLogicalLaneBoundary, std::vector<Id>());
  MOCK_METHOD1(SetReferenceLine, void(Id referenceLineId));
  MOCK_CONST_METHOD0(GetReferenceLine, Id());
};
}  //namespace OWL::Fakes
