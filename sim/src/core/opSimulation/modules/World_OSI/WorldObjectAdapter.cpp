/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "WorldObjectAdapter.h"

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <algorithm>
#include <vector>

#include "OWL/DataTypes.h"
#include "common/boostGeometryCommon.h"

namespace boost::geometry
{
struct radian;
}  // namespace boost::geometry

WorldObjectAdapter::WorldObjectAdapter(OWL::Interfaces::WorldObject& baseTrafficObject)
    : baseTrafficObject{baseTrafficObject}
{
}

const polygon_t& WorldObjectAdapter::GetBoundingBox2D() const
{
  if (boundingBoxNeedsUpdate)
  {
    boundingBox = CalculateBoundingBox();
    boundingBoxNeedsUpdate = false;
  }

  return boundingBox;
}

units::length::meter_t WorldObjectAdapter::GetDistanceReferencePointToLeadingEdge() const
{
  return baseTrafficObject.GetDimension().length / 2.0;
}

units::length::meter_t WorldObjectAdapter::GetHeight() const
{
  return baseTrafficObject.GetDimension().height;
}

int WorldObjectAdapter::GetId() const
{
  return static_cast<int>(baseTrafficObject.GetId());
}

units::length::meter_t WorldObjectAdapter::GetLength() const
{
  return baseTrafficObject.GetDimension().length;
}

units::length::meter_t WorldObjectAdapter::GetPositionX() const
{
  return baseTrafficObject.GetReferencePointPosition().x;
}

units::length::meter_t WorldObjectAdapter::GetPositionY() const
{
  return baseTrafficObject.GetReferencePointPosition().y;
}

units::length::meter_t WorldObjectAdapter::GetPositionZ() const
{
  return baseTrafficObject.GetReferencePointPosition().z;
}

units::length::meter_t WorldObjectAdapter::GetWidth() const
{
  return baseTrafficObject.GetDimension().width;
}

units::angle::radian_t WorldObjectAdapter::GetYaw() const
{
  return baseTrafficObject.GetAbsOrientation().yaw;
}

units::angle::radian_t WorldObjectAdapter::GetRoll() const
{
  return baseTrafficObject.GetAbsOrientation().roll;
}

const OWL::Interfaces::WorldObject& WorldObjectAdapter::GetBaseTrafficObject() const
{
  return baseTrafficObject;
}

const polygon_t WorldObjectAdapter::CalculateBoundingBox() const
{
  const auto length = GetLength();
  const auto width = GetWidth();
  const auto height = GetHeight();
  const auto rotation = GetYaw();
  const auto roll = GetRoll();

  const auto x = units::unit_cast<double>(GetPositionX());
  const auto y = units::unit_cast<double>(GetPositionY());

  const auto center = GetDistanceReferencePointToLeadingEdge();

  const auto halfWidth = width / 2.0;
  const auto widthLeft = halfWidth * units::math::cos(roll) + (roll < 0_rad ? height * units::math::sin(-roll) : 0_m);
  const auto widthRight = halfWidth * units::math::cos(roll) + (roll > 0_rad ? height * units::math::sin(roll) : 0_m);

  std::vector<point_t> boxPoints{{units::unit_cast<double>(center - length), units::unit_cast<double>(-widthRight)},
                                 {units::unit_cast<double>(center - length), units::unit_cast<double>(widthLeft)},
                                 {units::unit_cast<double>(center), units::unit_cast<double>(widthLeft)},
                                 {units::unit_cast<double>(center), units::unit_cast<double>(-widthRight)},
                                 {units::unit_cast<double>(center - length), units::unit_cast<double>(-widthRight)}};

  polygon_t box;
  polygon_t boxTemp;
  bg::append(box, boxPoints);

  bt::translate_transformer<double, 2, 2> translate(x, y);

  // rotation in mathematical negativ order (boost) -> invert to match
  bt::rotate_transformer<bg::radian, double, 2, 2> rotate(-rotation.value());

  bg::transform(box, boxTemp, rotate);
  bg::transform(boxTemp, box, translate);

  return box;
}

namespace WorldObjectCommon
{

units::length::meter_t GetFrontDeltaS(units::length::meter_t length,
                                      units::length::meter_t width,
                                      units::angle::radian_t hdg,
                                      units::length::meter_t distanceReferencePointToLeadingEdge)
{
  auto l_front = distanceReferencePointToLeadingEdge;
  auto l_rear = distanceReferencePointToLeadingEdge - length;
  auto w = width / 2;

  const auto sinHeading = units::math::sin(hdg);
  const auto cosHeading = units::math::cos(hdg);

  const units::length::meter_t s1 = l_front * cosHeading - w * sinHeading;
  const units::length::meter_t s2 = l_front * cosHeading + w * sinHeading;
  const units::length::meter_t s3 = l_rear * cosHeading - w * sinHeading;
  const units::length::meter_t s4 = l_rear * cosHeading + w * sinHeading;

  return std::max({s1, s2, s3, s4});
}

units::length::meter_t GetRearDeltaS(units::length::meter_t length,
                                     units::length::meter_t width,
                                     units::angle::radian_t hdg,
                                     units::length::meter_t distanceReferencePointToLeadingEdge)
{
  auto l_front = distanceReferencePointToLeadingEdge;
  auto l_rear = distanceReferencePointToLeadingEdge - length;
  auto w = width / 2;

  const auto sinHeading = units::math::sin(hdg);
  const auto cosHeading = units::math::cos(hdg);

  auto s1 = l_front * cosHeading - w * sinHeading;
  auto s2 = l_front * cosHeading + w * sinHeading;
  auto s3 = l_rear * cosHeading - w * sinHeading;
  auto s4 = l_rear * cosHeading + w * sinHeading;

  return std::min({s1, s2, s3, s4});
}
}  // namespace WorldObjectCommon
