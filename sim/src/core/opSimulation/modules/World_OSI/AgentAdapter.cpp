/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2020 HLRS, University of Stuttgart
 *               2016 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "AgentAdapter.h"

#include <algorithm>
#include <variant>

#include "common/globalDefinitions.h"
#include "include/agentBlueprintInterface.h"

AgentAdapter::AgentAdapter(OWL::Interfaces::MovingObject &mo,
                           WorldInterface *world,
                           const CallbackInterface *callbacks,
                           const World::Localization::Localizer &localizer)
    : WorldObjectAdapter{mo}, world{world}, callbacks{callbacks}, localizer{localizer}, egoAgent{this, world}
{
}

AgentAdapter::~AgentAdapter() = default;

void AgentAdapter::InitParameter(const AgentBuildInstructions &agentBuildInstructions)
{
  UpdateEntityModelParameter(agentBuildInstructions.entityProperties);

  this->vehicleModelType = agentBuildInstructions.entityProperties->model;
  this->driverProfileName = agentBuildInstructions.system.driverProfileName;
  this->agentCategory = agentBuildInstructions.agentCategory;
  this->agentTypeName = agentBuildInstructions.system.agentProfileName;
  this->objectName = agentBuildInstructions.name;

  // set default values
  GetBaseTrafficObject().SetPitch(0.0_rad);
  GetBaseTrafficObject().SetRoll(0.0_rad);
  GetBaseTrafficObject().SetAbsOrientationRate({0.0_rad_per_s, 0.0_rad_per_s, 0.0_rad_per_s});
  GetBaseTrafficObject().SetAbsOrientationAcceleration({0.0_rad_per_s_sq, 0.0_rad_per_s_sq, 0.0_rad_per_s_sq});

  UpdateYaw(agentBuildInstructions.spawnParameter.orientation.yaw);
  GetBaseTrafficObject().SetX(agentBuildInstructions.spawnParameter.position.x);
  GetBaseTrafficObject().SetY(agentBuildInstructions.spawnParameter.position.y);
  GetBaseTrafficObject().SetZ(0.5 * vehicleModelParameters->bounding_box.dimension.height);
  GetBaseTrafficObject().SetAbsVelocity(agentBuildInstructions.spawnParameter.velocity);
  GetBaseTrafficObject().SetAbsAcceleration(agentBuildInstructions.spawnParameter.acceleration);
  this->currentGear = static_cast<int>(agentBuildInstructions.spawnParameter.gear);

  SetSensorParameters(agentBuildInstructions.system.sensorParameters);

  // spawn tasks are executed before any other task types within current scheduling time
  // other task types will have a consistent view of the world
  // calculate initial position
  Locate();

  auto route = agentBuildInstructions.spawnParameter.route;
  egoAgent.SetRoadGraph(std::move(route.roadGraph), route.root, route.target);
}

bool AgentAdapter::Update()
{
  // currently set to constant true to correctly update BB of rotating objects (with velocity = 0)
  // and objects with an incomplete set of dynamic parameters (i. e. changing x/y with velocity = 0)
  //boundingBoxNeedsUpdate = std::abs(GetVelocity()) >= zeroBaseline;
  boundingBoxNeedsUpdate = true;
  if (!Locate())
  {
    return false;
  }
  return true;
}

void AgentAdapter::SetBrakeLight(bool brakeLightStatus)
{
  GetBaseTrafficObject().SetBrakeLightState(brakeLightStatus);
}

bool AgentAdapter::GetBrakeLight() const
{
  return GetBaseTrafficObject().GetBrakeLightState();
}

bool AgentAdapter::Locate()
{
  // reset on-demand values
  boundaryPoints.clear();

  locateResult = localizer.Locate(GetBoundingBox2D(), GetBaseTrafficObject());

  GetBaseTrafficObject().SetTouchedRoads(locateResult.touchedRoads);

  egoAgent.Update();

  return locateResult.isOnRoute;
}

void AgentAdapter::Unlocate()
{
  localizer.Unlocate(GetBaseTrafficObject());
}

AgentCategory AgentAdapter::GetAgentCategory() const
{
  return agentCategory;
}

std::string AgentAdapter::GetAgentTypeName() const
{
  return agentTypeName;
}

void AgentAdapter::SetIndicatorState(IndicatorState indicatorState)
{
  GetBaseTrafficObject().SetIndicatorState(indicatorState);
}

IndicatorState AgentAdapter::GetIndicatorState() const
{
  return GetBaseTrafficObject().GetIndicatorState();
}

bool AgentAdapter::IsAgentInWorld() const
{
  return locateResult.isOnRoute;
}

void AgentAdapter::SetPosition(Position pos)
{
  SetPositionX(pos.xPos);
  SetPositionY(pos.yPos);
  SetYaw(pos.yawAngle);
}

Common::Vector2d<units::velocity::meters_per_second_t> AgentAdapter::GetVelocity(ObjectPoint point) const
{
  auto longitudinal = GetLongitudinal(point) - GetLongitudinal(ObjectPointPredefined::Center);
  auto lateral = GetLateral(point) - GetLateral(ObjectPointPredefined::Center);
  Common::Vector2d<units::velocity::meters_per_second_t> rotation{
      0.0_mps, 0.0_mps};  //! velocity from rotation of vehicle around reference point
  if (longitudinal != 0_m || lateral != 0_m)
  {
    rotation.x = -lateral * GetYawRate() / 1_rad;
    rotation.y = longitudinal * GetYawRate() / 1_rad;
  }
  return {GetBaseTrafficObject().GetAbsVelocity().x + rotation.x,
          GetBaseTrafficObject().GetAbsVelocity().y + rotation.y};
}
Common::Vector2d<units::acceleration::meters_per_second_squared_t> AgentAdapter::GetAcceleration(
    ObjectPoint point) const
{
  auto longitudinal = GetLongitudinal(point) - GetLongitudinal(ObjectPointPredefined::Center);
  auto lateral = GetLateral(point) - GetLateral(ObjectPointPredefined::Center);
  Common::Vector2d<units::acceleration::meters_per_second_squared_t> rotationAcceleration{
      0.0_mps_sq, 0.0_mps_sq};  //! acceleration from rotation of vehicle around reference point
  if (longitudinal != 0_m || lateral != 0_m)
  {
    rotationAcceleration.x
        = (-lateral * GetYawAcceleration() - longitudinal * units::math::pow<2>(GetYawRate()) / 1_rad) / 1_rad;
    rotationAcceleration.y
        = (longitudinal * GetYawAcceleration() - lateral * units::math::pow<2>(GetYawRate()) / 1_rad) / 1_rad;
  }
  return {GetBaseTrafficObject().GetAbsAcceleration().ax + rotationAcceleration.x,
          GetBaseTrafficObject().GetAbsAcceleration().ay + rotationAcceleration.y};
}

bool AgentAdapter::IsEgoAgent() const
{
  return agentCategory == AgentCategory::Ego;
}

void AgentAdapter::UpdateCollision(std::pair<ObjectTypeOSI, int> collisionPartner)
{
  auto findIter = std::find_if(collisionPartners.begin(),
                               collisionPartners.end(),
                               [collisionPartner](const std::pair<ObjectTypeOSI, int> &storedCollisionPartner)
                               { return collisionPartner == storedCollisionPartner; });
  if (findIter == collisionPartners.end())
  {
    collisionPartners.push_back(collisionPartner);
  }
}

bool AgentAdapter::IsLeavingWorld() const
{
  return false;
}

bool AgentAdapter::GetHeadLight() const
{
  return GetBaseTrafficObject().GetHeadLight();
}

bool AgentAdapter::GetHighBeamLight() const
{
  return GetBaseTrafficObject().GetHighBeamLight();
}

std::vector<std::string> AgentAdapter::GetRoads(ObjectPoint point) const
{
  std::vector<std::string> roadIds;
  std::transform(GetRoadPosition(point).cbegin(),
                 GetRoadPosition(point).cend(),
                 std::back_inserter(roadIds),
                 [](const auto &entry) { return entry.first; });
  return roadIds;
}

units::length::meter_t AgentAdapter::GetDistanceToConnectorEntrance(std::string intersectingConnectorId,
                                                                    int intersectingLaneId,
                                                                    std::string ownConnectorId) const
{
  return world->GetDistanceToConnectorEntrance(
      /*locateResult.position,*/ intersectingConnectorId, intersectingLaneId, ownConnectorId);
}

units::length::meter_t AgentAdapter::GetDistanceToConnectorDeparture(std::string intersectingConnectorId,
                                                                     int intersectingLaneId,
                                                                     std::string ownConnectorId) const
{
  return world->GetDistanceToConnectorDeparture(
      /*locateResult.position,*/ intersectingConnectorId, intersectingLaneId, ownConnectorId);
}

LightState AgentAdapter::GetLightState() const
{
  if (GetFlasher())
  {
    return LightState::Flash;
  }
  if (GetHighBeamLight())
  {
    return LightState::HighBeam;
  }
  if (GetHeadLight())
  {
    return LightState::LowBeam;
  }

  return LightState::Off;
}

const GlobalRoadPositions &AgentAdapter::GetRoadPosition(const ObjectPoint &point) const
{
  auto position = locateResult.points.find(point);
  if (position != locateResult.points.cend())
  {
    return position->second;
  }
  const auto globalPoint = GetAbsolutePosition(point);
  auto locatedPoint = localizer.Locate(globalPoint, GetYaw());
  auto [newElement, success] = locateResult.points.insert({point, locatedPoint});
  return newElement->second;
}

units::length::meter_t AgentAdapter::GetLongitudinal(const ObjectPoint &objectPoint) const
{
  if (std::holds_alternative<ObjectPointCustom>(objectPoint))
  {
    return std::get<ObjectPointCustom>(objectPoint).longitudinal;
  }
  if (std::holds_alternative<ObjectPointPredefined>(objectPoint))
  {
    switch (std::get<ObjectPointPredefined>(objectPoint))
    {
      case ObjectPointPredefined::Reference:
        return 0_m;
      case ObjectPointPredefined::Center:
        return GetDistanceReferencePointToLeadingEdge() - 0.5 * GetLength();
      case ObjectPointPredefined::FrontCenter:
      case ObjectPointPredefined::FrontLeft:
      case ObjectPointPredefined::FrontRight:
        return GetDistanceReferencePointToLeadingEdge();
      case ObjectPointPredefined::RearCenter:
      case ObjectPointPredefined::RearLeft:
      case ObjectPointPredefined::RearRight:
        return GetDistanceReferencePointToLeadingEdge() - GetLength();
    }
    throw std::runtime_error("Invalid value for ObjectPointPredefined");
  }
  if (std::holds_alternative<ObjectPointRelative>(objectPoint))
  {
    throw std::invalid_argument("Relative ObjectPoint can not be resolved without reference");
  }
  throw std::runtime_error("Unknown type of ObjectPoint");
}

units::length::meter_t AgentAdapter::GetLateral(const ObjectPoint &objectPoint) const
{
  if (std::holds_alternative<ObjectPointCustom>(objectPoint))
  {
    return std::get<ObjectPointCustom>(objectPoint).lateral;
  }
  if (std::holds_alternative<ObjectPointPredefined>(objectPoint))
  {
    switch (std::get<ObjectPointPredefined>(objectPoint))
    {
      case ObjectPointPredefined::Reference:
      case ObjectPointPredefined::Center:
      case ObjectPointPredefined::FrontCenter:
      case ObjectPointPredefined::RearCenter:
        return 0_m;
      case ObjectPointPredefined::FrontLeft:
      case ObjectPointPredefined::RearLeft:
        return 0.5 * GetWidth();
      case ObjectPointPredefined::FrontRight:
      case ObjectPointPredefined::RearRight:
        return -0.5 * GetWidth();
    }
    throw std::runtime_error("Invalid value for ObjectPointPredefined");
  }
  if (std::holds_alternative<ObjectPointRelative>(objectPoint))
  {
    throw std::invalid_argument("Relative ObjectPoint can not be resolved without reference");
  }
  throw std::runtime_error("Unknown type of ObjectPoint");
}

Common::Vector2d<units::length::meter_t> AgentAdapter::GetAbsolutePosition(const ObjectPoint &objectPoint) const
{
  auto longitudinal = GetLongitudinal(objectPoint);
  auto lateral = GetLateral(objectPoint);
  const auto &referencePoint = baseTrafficObject.GetReferencePointPosition();
  const auto &yaw = baseTrafficObject.GetAbsOrientation().yaw;
  auto x = referencePoint.x + units::math::cos(yaw) * longitudinal - units::math::sin(yaw) * lateral;
  auto y = referencePoint.y + units::math::sin(yaw) * longitudinal + units::math::cos(yaw) * lateral;
  return {x, y};
}
