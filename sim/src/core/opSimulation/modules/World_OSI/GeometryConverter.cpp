/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2017 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "GeometryConverter.h"

#include <algorithm>
#include <cmath>
#include <deque>
#include <limits>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>

#include "LocalizationElement.h"
#include "RamerDouglasPeucker.h"
#include "WorldData.h"
#include "WorldToRoadCoordinateConverter.h"
#include "common/commonTools.h"
#include "common/vector2d.h"
#include "commonHelper.h"
#include "include/roadInterface/roadElevation.h"
#include "include/roadInterface/roadGeometryInterface.h"
#include "include/roadInterface/roadInterface.h"
#include "include/roadInterface/roadLaneInterface.h"
#include "include/roadInterface/roadLaneOffset.h"
#include "include/roadInterface/roadLaneRoadMark.h"
#include "include/roadInterface/roadLaneSectionInterface.h"
#include "include/roadInterface/roadLaneWidth.h"
#include "include/sceneryInterface.h"

namespace OWL::Primitive
{
struct LaneGeometryElement;
}  // namespace OWL::Primitive

void GeometryConverter::CalculateRoads(const SceneryInterface &scenery, OWL::Interfaces::WorldData &worldData)
{
  for (auto [_, road] : scenery.GetRoads())
  {
    const auto &roadLaneSections = road->GetLaneSections();

    for (auto roadLaneSectionIt = roadLaneSections.begin(); roadLaneSectionIt != roadLaneSections.end();
         roadLaneSectionIt++)
    {
      RoadLaneSectionInterface *roadSection = (*roadLaneSectionIt).get();
      const auto &roadLanes = roadSection->GetLanes();

      if (!roadLanes.empty())
      {
        units::length::meter_t roadSectionStart = roadSection->GetStart();
        units::length::meter_t roadSectionEnd{std::numeric_limits<double>::max()};

        if (std::next(roadLaneSectionIt) != roadLaneSections.end())  // if not a single element in the list
        {
          roadSectionEnd = (*std::next(roadLaneSectionIt))->GetStart();
        }

        // collect geometry sections
        CalculateSection(worldData, roadSectionStart, roadSectionEnd, road, roadSection);
      }  // if lanes are not empty
    }
  }
}

void GeometryConverter::CalculateSection(OWL::Interfaces::WorldData &worldData,
                                         units::length::meter_t roadSectionStart,
                                         units::length::meter_t roadSectionEnd,
                                         const RoadInterface *road,
                                         const RoadLaneSectionInterface *roadSection)
{
  units::length::meter_t roadMarkStart{0};
  SampledGeometry sampledGeometries;
  bool firstRoadMark{true};

  while (roadMarkStart + roadSectionStart < roadSectionEnd)
  {
    units::length::meter_t roadMarkEnd{std::numeric_limits<double>::max()};
    for (const auto &roadLane : roadSection->GetLanes())
    {
      for (const auto &roadMark : roadLane.second->GetRoadMarks())
      {
        const auto sOffset = roadMark->GetSOffset();
        if (sOffset > roadMarkStart && sOffset < roadMarkEnd)
        {
          roadMarkEnd = sOffset;
        }
      }
    }

    auto sampledGeometry
        = CalculateSectionBetweenRoadMarkChanges(roadSectionStart + roadMarkStart,
                                                 units::math::min(roadSectionStart + roadMarkEnd, roadSectionEnd),
                                                 road,
                                                 roadSection);
    if (firstRoadMark)
    {
      sampledGeometries = sampledGeometry;
      firstRoadMark = false;
    }
    else
    {
      sampledGeometries.Combine(sampledGeometry);
    }

    roadMarkStart = roadMarkEnd;
  }

  JointsBuilder jointsBuilder{sampledGeometries};

  jointsBuilder.CalculatePoints().CalculateHeadings().CalculateCurvatures();

  AddPointsToWorld(worldData, jointsBuilder.GetJoints());
}

SampledGeometry GeometryConverter::CalculateSectionBetweenRoadMarkChanges(units::length::meter_t roadSectionStart,
                                                                          units::length::meter_t roadSectionEnd,
                                                                          const RoadInterface *road,
                                                                          const RoadLaneSectionInterface *roadSection)
{
  SampledGeometry sampledGeometries;
  const auto &roadGeometries = road->GetGeometries();
  bool firstGeometry{true};

  for (auto roadGeometry = roadGeometries.cbegin(); roadGeometry != roadGeometries.cend(); ++roadGeometry)
  {
    auto next = std::next(roadGeometry);
    //To prevent rounding issues, the length is calculated as difference between the start s of the geometries
    auto roadGeometryLength
        = (next == roadGeometries.end()) ? (*roadGeometry)->GetLength() : (*next)->GetS() - (*roadGeometry)->GetS();
    auto sampledGeometry = CalculateGeometry(
        roadSectionStart, roadSectionEnd, road, roadSection, (*roadGeometry).get(), roadGeometryLength);
    if (sampledGeometry.borderPoints.empty())
    {
      continue;
    }
    if (firstGeometry)
    {
      sampledGeometries = sampledGeometry;
      firstGeometry = false;
    }
    else
    {
      sampledGeometries.Combine(sampledGeometry);
    }
  }
  return sampledGeometries;
}

SampledGeometry GeometryConverter::CalculateGeometry(units::length::meter_t roadSectionStart,
                                                     units::length::meter_t roadSectionEnd,
                                                     const RoadInterface *road,
                                                     const RoadLaneSectionInterface *roadSection,
                                                     const RoadGeometryInterface *roadGeometry,
                                                     units::length::meter_t roadGeometryLength)
{
  auto roadGeometryStart = roadGeometry->GetS();
  auto roadGeometryEnd = roadGeometryStart + roadGeometryLength;

  // if section is not affected by geometry
  if (roadSectionStart >= roadGeometryEnd || roadSectionEnd <= roadGeometryStart)
  {
    return {};
  }

  units::length::meter_t geometryOffsetStart = CalculateGeometryOffsetStart(roadSectionStart, roadGeometryStart);

  units::length::meter_t geometryOffsetEnd
      = CalculateGeometryOffsetEnd(roadSectionEnd, roadGeometryStart, roadGeometryEnd, roadGeometryLength);
  auto borderPoints
      = CalculateJoints(geometryOffsetStart, geometryOffsetEnd, roadSection, road, roadGeometry, roadGeometryStart);

  borderPoints = RamerDouglasPeucker::Simplify<BorderPoints>(borderPoints);

  return {borderPoints, roadGeometry->GetDir(geometryOffsetStart), roadGeometry->GetDir(geometryOffsetEnd)};
}

units::length::meter_t GeometryConverter::CalculateGeometryOffsetStart(units::length::meter_t roadSectionStart,
                                                                       units::length::meter_t roadGeometryStart)
{
  // geometry begins within section
  if (roadSectionStart <= roadGeometryStart)
  {
    return 0.0_m;
  }
  // geometry begins before section
  return roadSectionStart - roadGeometryStart;
}

units::length::meter_t GeometryConverter::CalculateGeometryOffsetEnd(units::length::meter_t roadSectionEnd,
                                                                     units::length::meter_t roadGeometryStart,
                                                                     units::length::meter_t roadGeometryEnd,
                                                                     units::length::meter_t roadGeometryLength)
{
  // geometry ends within section
  if (roadSectionEnd >= roadGeometryEnd)
  {
    return roadGeometryLength;
  }
  // geometry ends after section
  return roadSectionEnd - roadGeometryStart;
}

std::vector<BorderPoints> GeometryConverter::CalculateJoints(units::length::meter_t geometryOffsetStart,
                                                             units::length::meter_t geometryOffsetEnd,
                                                             const RoadLaneSectionInterface *roadSection,
                                                             const RoadInterface *road,
                                                             const RoadGeometryInterface *roadGeometry,
                                                             units::length::meter_t roadGeometryStart)
{
  std::vector<BorderPoints> borderPoints;
  auto geometryOffset = geometryOffsetStart;
  while (geometryOffset < geometryOffsetEnd + SAMPLING_RATE)
  {
    // account for last sample
    if (geometryOffset > geometryOffsetEnd)
    {
      geometryOffset = geometryOffsetEnd;
    }

    borderPoints.push_back(
        CalculateBorderPoints(roadSection, road, roadGeometry, geometryOffset, roadGeometryStart + geometryOffset));

    geometryOffset += SAMPLING_RATE;
  }
  return borderPoints;
}

BorderPoints GeometryConverter::CalculateBorderPoints(const RoadLaneSectionInterface *roadSection,
                                                      const RoadInterface *road,
                                                      const RoadGeometryInterface *roadGeometry,
                                                      units::length::meter_t geometryOffset,
                                                      units::length::meter_t sCoordinate)
{
  const auto &roadLanes = roadSection->GetLanes();
  const auto maxLaneId = roadLanes.crbegin()->first;
  const auto minLaneId = roadLanes.cbegin()->first;

  const auto sectionOffset = units::math::max(sCoordinate - roadSection->GetStart(),
                                              0.0_m);  //Take only positive value to prevent rounding imprecision
  const auto laneOffset = CalculateLaneOffset(road, sCoordinate);
  const auto centerPoint = roadGeometry->GetCoord(geometryOffset, laneOffset);
  const auto heading = roadGeometry->GetDir(geometryOffset);
  const auto sin_hdg = units::math::sin(heading);
  const auto cos_hdg = units::math::cos(heading);

  std::deque<BorderPoint> points;

  // start with positive lanes
  // 1, 2, 3... -> order necessary to accumulate right starting from center
  units::length::meter_t borderOffset{0.0};
  for (int laneId = 1; laneId <= maxLaneId; laneId++)
  {
    const auto laneIter = roadLanes.find(laneId);
    if (laneIter == roadLanes.end())
    {
      throw std::runtime_error("Missing lane with id " + std::to_string(laneId));
    }

    auto *lane = laneIter->second.get();
    if (!lane->GetWidths().empty())
    {
      borderOffset += CalculateLaneWidth(lane, sectionOffset);
    }
    else if (!lane->GetBorders().empty())
    {
      borderOffset = CalculateLaneBorder(lane, sectionOffset);
    }
    else
    {
      throw std::runtime_error("RoadLane requires either Width or Border definition.");
    }

    const units::length::meter_t x = units::length::meter_t(centerPoint.x) - borderOffset * sin_hdg;
    const units::length::meter_t y = units::length::meter_t(centerPoint.y) + borderOffset * cos_hdg;
    points.emplace_front(Common::Vector2d<units::length::meter_t>{x, y}, borderOffset, roadLanes.at(laneId).get());
  }

  points.emplace_back(centerPoint, 0.0_m, roadLanes.at(0).get());

  borderOffset = 0.0_m;
  for (int laneId = -1; laneId >= minLaneId; laneId--)
  {
    const auto laneIter = roadLanes.find(laneId);
    if (laneIter == roadLanes.end())
    {
      throw std::runtime_error("Missing lane with id " + std::to_string(laneId));
    }

    auto *lane = laneIter->second.get();
    if (!lane->GetWidths().empty())
    {
      borderOffset += CalculateLaneWidth(lane, sectionOffset);
    }
    else if (!lane->GetBorders().empty())
    {
      borderOffset = CalculateLaneBorder(lane, sectionOffset);
    }
    else
    {
      throw std::runtime_error("RoadLane requires either Width or Border definition.");
    }

    const units::length::meter_t x = units::length::meter_t(centerPoint.x) + borderOffset * sin_hdg;
    const units::length::meter_t y = units::length::meter_t(centerPoint.y) - borderOffset * cos_hdg;
    points.emplace_back(Common::Vector2d<units::length::meter_t>{x, y}, -borderOffset, roadLanes.at(laneId).get());
  }

  return BorderPoints{sCoordinate, heading, {points.begin(), points.end()}};
}

void GeometryConverter::AddPointsToWorld(OWL::Interfaces::WorldData &worldData, const Joints &joints)
{
  for (const auto &joint : joints)
  {
    for (const auto &[laneId, laneJoint] : joint.laneJoints)
    {
      if (laneId == 0)
      {
        worldData.AddCenterLinePoint(
            *laneJoint.lane->GetLaneSection(), laneJoint.center, joint.s, laneJoint.rawHeading);
      }
      else
      {
        worldData.AddLaneGeometryPoint(*laneJoint.lane,
                                       laneJoint.left,
                                       laneJoint.center,
                                       laneJoint.right,
                                       joint.s,
                                       laneJoint.t_left,
                                       laneJoint.t_right,
                                       laneJoint.curvature,
                                       laneJoint.heading);
      }
    }
  }
}

units::length::meter_t GeometryConverter::CalculateCoordZ(RoadInterface *road, units::length::meter_t offset)
{
  units::length::meter_t coordZ{0.0};
  const RoadElevation *roadElevation = GetRelevantRoadElevation(offset, road);

  if (roadElevation)
  {
    const auto ds = offset - roadElevation->GetS();

    coordZ = roadElevation->GetA() + roadElevation->GetB() * ds + roadElevation->GetC() * ds * ds
           + roadElevation->GetD() * ds * ds * ds;
  }

  return coordZ;
}

double GeometryConverter::CalculateSlope(RoadInterface *road, units::length::meter_t offset)
{
  double slope = 0.0;
  const RoadElevation *roadElevation = GetRelevantRoadElevation(offset, road);

  if (roadElevation)
  {
    slope = CalculateSlopeAtRoadPosition(roadElevation, offset);
  }

  return slope;
}

const RoadElevation *GeometryConverter::GetRelevantRoadElevation(units::length::meter_t roadOffset, RoadInterface *road)
{
  auto roadLaneIt = road->GetElevations().begin();
  while (road->GetElevations().end() != roadLaneIt)
  {
    if ((*roadLaneIt)->GetS() <= roadOffset)
    {
      auto roadLaneNextIt = std::next(roadLaneIt);
      if (road->GetElevations().end() == roadLaneNextIt || (*roadLaneNextIt)->GetS() > roadOffset)
      {
        break;
      }
    }

    ++roadLaneIt;
  }

  if (roadLaneIt == road->GetElevations().end())
  {
    return nullptr;
  }
  return (*roadLaneIt).get();
}

const RoadLaneWidth *GeometryConverter::GetRelevantRoadLaneWidth(
    units::length::meter_t sectionOffset, const std::vector<std::unique_ptr<RoadLaneWidth>> &widthsOrBorders)
{
  auto roadLaneIt = widthsOrBorders.begin();
  while (widthsOrBorders.end() != roadLaneIt)
  {
    if ((*roadLaneIt)->GetSOffset() <= sectionOffset)
    {
      auto roadLaneNextIt = std::next(roadLaneIt);
      if (widthsOrBorders.end() == roadLaneNextIt || (*roadLaneNextIt)->GetSOffset() > sectionOffset)
      {
        break;
      }
    }

    ++roadLaneIt;
  }

  if (roadLaneIt == widthsOrBorders.end())
  {
    return nullptr;
  }
  return (*roadLaneIt).get();
}

const RoadLaneOffset *GeometryConverter::GetRelevantRoadLaneOffset(units::length::meter_t roadOffset,
                                                                   const RoadInterface *road)
{
  auto laneOffsetIt = road->GetLaneOffsets().begin();
  while (laneOffsetIt != road->GetLaneOffsets().end())
  {
    auto laneOffsetNextIt = std::next(laneOffsetIt);

    if (road->GetLaneOffsets().end() == laneOffsetNextIt || (*laneOffsetNextIt)->GetS() > roadOffset)
    {
    }

    ++laneOffsetIt;
  }

  auto roadLaneIt = road->GetLaneOffsets().begin();
  while (road->GetLaneOffsets().end() != roadLaneIt)
  {
    if ((*roadLaneIt)->GetS() <= roadOffset)
    {
      auto roadLaneNextIt = std::next(roadLaneIt);
      if (road->GetLaneOffsets().end() == roadLaneNextIt || (*roadLaneNextIt)->GetS() > roadOffset)
      {
        break;
      }
    }

    ++roadLaneIt;
  }

  if (roadLaneIt == road->GetLaneOffsets().end())
  {
    return nullptr;
  }
  return (*roadLaneIt).get();
}

const RoadLaneRoadMark *GeometryConverter::GetRelevantRoadLaneRoadMark(units::length::meter_t sectionOffset,
                                                                       const RoadLaneInterface *roadLane)
{
  auto roadMarkIt = roadLane->GetRoadMarks().begin();

  while (roadMarkIt != roadLane->GetRoadMarks().end())
  {
    if ((*roadMarkIt)->GetSOffset() <= sectionOffset)
    {
      auto roadMarkNextIt = std::next(roadMarkIt);
      if (roadMarkNextIt == roadLane->GetRoadMarks().end() || (*roadMarkNextIt)->GetSOffset() > sectionOffset)
      {
        break;
      }
    }

    ++roadMarkIt;
  }

  if (roadMarkIt == roadLane->GetRoadMarks().end())
  {
    return nullptr;
  }
  return (*roadMarkIt).get();
}

units::length::meter_t GeometryConverter::CalculateLaneWidth(const RoadLaneInterface *roadLane,
                                                             units::length::meter_t sectionOffset)
{
  const RoadLaneWidth *roadLaneWidth = GetRelevantRoadLaneWidth(sectionOffset, roadLane->GetWidths());

  if (!roadLaneWidth)
  {
    throw std::runtime_error("No lane width given");
  }

  auto width = CalculateWidthAtSectionPosition(roadLaneWidth, sectionOffset);
  CheckLaneWidth(roadLane, sectionOffset, width);
  return width;
}

units::length::meter_t GeometryConverter::CalculateLaneBorder(const RoadLaneInterface *roadLane,
                                                              units::length::meter_t sectionOffset)
{
  const RoadLaneWidth *roadLaneBorder = GetRelevantRoadLaneWidth(sectionOffset, roadLane->GetBorders());

  if (!roadLaneBorder)
  {
    throw std::runtime_error("No lane border given");
  }

  auto width = CalculateWidthAtSectionPosition(roadLaneBorder, sectionOffset);
  CheckLaneWidth(roadLane, sectionOffset, width);
  return width;
}

void GeometryConverter::CheckLaneWidth(const RoadLaneInterface *roadLane,
                                       units::length::meter_t sectionOffset,
                                       units::length::meter_t width)
{
  if (width < 0_m && width > units::length::meter_t(-CommonHelper::EPSILON))
  {
    width = 0_m;
  }

  if (width < 0_m)
  {
    throw std::runtime_error("Negative width for road " + roadLane->GetLaneSection()->GetRoad()->GetId() + " lane "
                             + std::to_string(roadLane->GetId()) + " s = "
                             + std::to_string(roadLane->GetLaneSection()->GetStart().value() + sectionOffset.value()));
  }
}

units::length::meter_t GeometryConverter::CalculateLaneOffset(const RoadInterface *road,
                                                              units::length::meter_t roadPosition)
{
  const RoadLaneOffset *roadLaneOffset = GetRelevantRoadLaneOffset(roadPosition, road);

  if (!roadLaneOffset)
  {
    return 0.0_m;
  }

  return CalculateOffsetAtRoadPosition(roadLaneOffset, roadPosition);
}

units::length::meter_t GeometryConverter::CalculateWidthAtSectionPosition(const RoadLaneWidth *width,
                                                                          units::length::meter_t position)
{
  const auto ds = position - width->GetSOffset();

  return width->GetA() + width->GetB() * ds + width->GetC() * ds * ds + width->GetD() * ds * ds * ds;
}

double GeometryConverter::CalculateSlopeAtRoadPosition(const RoadElevation *roadElevation,
                                                       units::length::meter_t position)
{
  const auto ds = position - roadElevation->GetS();

  double deltaZ = roadElevation->GetB() + 2.0 * roadElevation->GetC() * ds + 3.0 * roadElevation->GetD() * ds * ds;

  return std::atan(deltaZ);
}

units::length::meter_t GeometryConverter::CalculateOffsetAtRoadPosition(const RoadLaneOffset *roadOffset,
                                                                        units::length::meter_t position)
{
  const auto ds = position - roadOffset->GetS();

  units::length::meter_t deltaT
      = roadOffset->GetA() + roadOffset->GetB() * ds + roadOffset->GetC() * ds * ds + roadOffset->GetD() * ds * ds * ds;

  return deltaT;
}

void GeometryConverter::Convert(const SceneryInterface &scenery, OWL::Interfaces::WorldData &worldData)
{
  CalculateRoads(scenery, worldData);
  CalculateIntersections(worldData);
}

void GeometryConverter::CalculateIntersections(OWL::Interfaces::WorldData &worldData)
{
  for (const auto &[id, junction] : worldData.GetJunctions())
  {
    JunctionPolygons junctionPolygons;
    std::transform(junction->GetConnectingRoads().begin(),
                   junction->GetConnectingRoads().end(),
                   std::inserter(junctionPolygons, junctionPolygons.begin()),
                   BuildRoadPolygons);

    CalculateJunctionIntersectionsFromRoadPolygons(junctionPolygons, junction, worldData);
  }
}

RoadPolygons GeometryConverter::BuildRoadPolygons(const OWL::Road *const road)
{
  std::vector<LaneGeometryPolygon> polygons;
  const auto &roadId = road->GetId();

  for (const auto *const section : road->GetSections())
  {
    for (const auto *const lane : section->GetLanes())
    {
      OWL::Id laneId = lane->GetId();

      const auto buildPolygonFromLaneGeometryElement = CreateBuildPolygonFromLaneGeometryFunction(roadId, laneId);
      const auto &laneGeometryElements = lane->GetLaneGeometryElements();
      std::transform(std::begin(laneGeometryElements),
                     std::end(laneGeometryElements),
                     std::back_inserter(polygons),
                     buildPolygonFromLaneGeometryElement);
    }
  }

  return std::make_pair(roadId, polygons);
}

std::function<LaneGeometryPolygon(const OWL::Primitive::LaneGeometryElement *const)>
GeometryConverter::CreateBuildPolygonFromLaneGeometryFunction(const std::string &roadId, const OWL::Id laneId)
{
  return [roadId, laneId](const auto elem) -> LaneGeometryPolygon
  {
    point_t currentLeftPoint{elem->joints.current.points.left.x.value(), elem->joints.current.points.left.y.value()};
    point_t currentRightPoint{elem->joints.current.points.right.x.value(), elem->joints.current.points.right.y.value()};
    point_t nextRightPoint{elem->joints.next.points.right.x.value(), elem->joints.next.points.right.y.value()};
    point_t nextLeftPoint{elem->joints.next.points.left.x.value(), elem->joints.next.points.left.y.value()};
    polygon_t polygon;

    bg::append(polygon, currentLeftPoint);
    bg::append(polygon, currentRightPoint);
    bg::append(polygon, nextRightPoint);
    bg::append(polygon, nextLeftPoint);
    bg::append(polygon, currentLeftPoint);
    bg::correct(polygon);

    return LaneGeometryPolygon{roadId, laneId, elem, polygon};
  };
}

void GeometryConverter::CalculateJunctionIntersectionsFromRoadPolygons(const JunctionPolygons &junctionPolygons,
                                                                       OWL::Junction *const junction,
                                                                       OWL::Interfaces::WorldData &worldData)
{
  auto roadPolygonsIter = junctionPolygons.begin();
  while (roadPolygonsIter != junctionPolygons.end())
  {
    auto roadPolygonsToCompareIter = roadPolygonsIter;
    roadPolygonsToCompareIter++;

    while (roadPolygonsToCompareIter != junctionPolygons.end())
    {
      const auto intersectionInfo
          = CalculateIntersectionInfoForRoadPolygons(*roadPolygonsIter, *roadPolygonsToCompareIter, junction);
      if (intersectionInfo)
      {
        const auto crossIntersectionInfo
            = CalculateIntersectionInfoForRoadPolygons(*roadPolygonsToCompareIter, *roadPolygonsIter, junction);

        junction->AddIntersectionInfo(roadPolygonsIter->first, intersectionInfo.value());
        junction->AddIntersectionInfo(roadPolygonsToCompareIter->first, crossIntersectionInfo.value());
        worldData.AddIntersectionInfo(intersectionInfo.value(), crossIntersectionInfo.value());
      }

      roadPolygonsToCompareIter++;
    }

    roadPolygonsIter++;
  }
}

std::optional<OWL::IntersectionInfo> GeometryConverter::CalculateIntersectionInfoForRoadPolygons(
    const RoadPolygons &roadPolygons, const RoadPolygons &roadPolygonsToCompare, const OWL::Junction *const junction)
{
  OWL::IntersectionInfo info;
  info.intersectingRoad = roadPolygonsToCompare.first;
  info.relativeRank = GetRelativeRank(roadPolygons.first, roadPolygonsToCompare.first, junction);

  for (const auto &laneGeometryPolygon : roadPolygons.second)
  {
    for (const auto &polygonToCompare : roadPolygonsToCompare.second)
    {
      const auto intersection = CommonHelper::IntersectionCalculation::GetIntersectionPoints(
          laneGeometryPolygon.polygon, polygonToCompare.polygon, false, false);

      if (intersection.size() < 3)
      {
        continue;
      }

      World::Localization::LocalizationElement localizationElement{*laneGeometryPolygon.laneGeometryElement};
      World::Localization::WorldToRoadCoordinateConverter processor{localizationElement};

      units::length::meter_t minS{std::numeric_limits<double>::max()};
      units::length::meter_t maxS{0};

      for (const auto &point : intersection)
      {
        const auto s = processor.GetS(point);
        minS = units::math::min(minS, s);
        maxS = units::math::max(maxS, s);
      }

      std::pair<OWL::Id, OWL::Id> intersectingLanesPair{laneGeometryPolygon.laneId, polygonToCompare.laneId};
      const auto intersectingLanesPairIter = info.sOffsets.find(intersectingLanesPair);

      // if these lane ids are already marked as intersecting, update the startSOffset and endSOffset to reflect new
      // intersection information
      if (intersectingLanesPairIter != info.sOffsets.end())
      {
        const auto recordedStartS = (*intersectingLanesPairIter).second.first;
        const auto recordedEndS = (*intersectingLanesPairIter).second.second;
        (*intersectingLanesPairIter).second.first = units::math::min(recordedStartS, minS);
        (*intersectingLanesPairIter).second.second = units::math::max(recordedEndS, maxS);
      }
      else
      {
        info.sOffsets.emplace(intersectingLanesPair, std::make_pair(minS, maxS));
      }
    }
  }

  return !info.sOffsets.empty() ? std::make_optional(info) : std::nullopt;
}

IntersectingConnectionRank GeometryConverter::GetRelativeRank(const std::string &roadId,
                                                              const std::string &intersectingRoadId,
                                                              const OWL::Junction *const junction)
{
  if (std::find_if(junction->GetPriorities().begin(),
                   junction->GetPriorities().end(),
                   [&](const auto &priority)
                   { return priority.first == roadId && priority.second == intersectingRoadId; })
      != junction->GetPriorities().end())
  {
    return IntersectingConnectionRank::Lower;
  }
  if (std::find_if(junction->GetPriorities().begin(),
                   junction->GetPriorities().end(),
                   [&](const auto &priority)
                   { return priority.first == intersectingRoadId && priority.second == roadId; })
      != junction->GetPriorities().end())
  {
    return IntersectingConnectionRank::Higher;
  }

  return IntersectingConnectionRank::Undefined;
}
