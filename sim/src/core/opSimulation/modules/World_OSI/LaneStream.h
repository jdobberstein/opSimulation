/********************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  LaneStream.h
//! @brief
//-----------------------------------------------------------------------------

#pragma once

#include <optional>
#include <units.h>
#include <utility>
#include <vector>

#include "OWL/DataTypes.h"
#include "common/globalDefinitions.h"
#include "common/worldDefinitions.h"
#include "include/streamInterface.h"

class WorldObjectInterface;

using namespace units::literals;

//! This class represents one element of a LaneStream.
struct LaneStreamElement
{
  const OWL::Interfaces::Lane* lane;  ///< lane represented by this object
  units::length::meter_t sOffset;     ///< S Offset of the start point of the element from the beginning of the stream
  bool
      inStreamDirection;  ///< Specifies whether the direction of the element is the same as the direction of the stream

  LaneStreamElement() = default;

  /**
   * @brief LaneStreamElement constructor
   *
   * @param[in]   lane                Lane represented by this object
   * @param[in]   sOffset             S Offset of the start point of the element from the beginning of the stream
   * @param[in]   inStreamDirection   Specifies whether the direction of the element is the same as the direction of the
   * stream
   */
  LaneStreamElement(const OWL::Interfaces::Lane* lane, units::length::meter_t sOffset, bool inStreamDirection)
      : lane(lane), sOffset(sOffset), inStreamDirection(inStreamDirection)
  {
  }

  /// @return Call operator function to return pointers to the lane
  const OWL::Interfaces::Lane& operator()() const { return *lane; }

  //! Transform the s coordinate on the element to the s coordinate on the stream
  //!
  //! \param elementPosition position relative to the start of the element
  //! \return position relative to the start of the stream
  units::length::meter_t GetStreamPosition(units::length::meter_t elementPosition) const
  {
    return sOffset + (inStreamDirection ? elementPosition : -elementPosition);
  }

  //! Transform the s coordinate on the stream to the s coordinate on the element
  //!
  //! \param streamPosition position relative to the start of the stream
  //! \return position relative to the start of the element
  units::length::meter_t GetElementPosition(units::length::meter_t streamPosition) const
  {
    return inStreamDirection ? streamPosition - sOffset : sOffset - streamPosition;
  }

  /**
   * @brief Returns the stream position of the start of the lane
   *
   * @return stream position of the start of the lane
   */
  units::length::meter_t StartS() const { return sOffset - (inStreamDirection ? 0_m : lane->GetLength()); }

  /**
   * @brief Returns the stream position of the end of the lane
   *
   * @return stream position of the end of the lane
   */
  units::length::meter_t EndS() const { return sOffset + (inStreamDirection ? lane->GetLength() : 0_m); }
};

//! This class represents a connected sequence of lanes in the road network
class LaneStream : public LaneStreamInterface
{
public:
  //! LaneStream constructor
  //!
  //! @param[in] elements List of the elements of the lane stream
  LaneStream(std::vector<LaneStreamElement> elements);

  StreamPosition GetStreamPosition(const GlobalRoadPosition& roadPosition) const override;

  GlobalRoadPosition GetRoadPosition(const StreamPosition& streamPosition) const override;

  AgentInterfaces GetAgentsInRange(const StreamPosition& start, const StreamPosition& end) const override;

  std::vector<const WorldObjectInterface*> GetObjectsInRange(const StreamPosition& start,
                                                             const StreamPosition& end) const override;

  std::optional<StreamPosition> GetStreamPosition(const WorldObjectInterface* object,
                                                  const ObjectPoint& point) const override;

  units::length::meter_t GetLength() const override;

  std::vector<std::pair<units::length::meter_t, LaneType>> GetLaneTypes() const override;

private:
  const std::vector<LaneStreamElement> elements;
};
