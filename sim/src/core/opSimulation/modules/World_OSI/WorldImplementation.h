/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2017 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <functional>
#include <map>
#include <memory>
#include <optional>
#include <stdexcept>
#include <stdint.h>
#include <string>
#include <units.h>
#include <unordered_map>
#include <utility>
#include <vector>

#include "AgentNetwork.h"
#include "EntityInfoPublisher.h"
#include "Localization.h"
#include "OWL/DataTypes.h"
#include "RadioImplementation.h"
#include "RoadStream.h"
#include "SceneryConverter.h"
#include "WorldData.h"
#include "WorldDataQuery.h"
#include "common/vector2d.h"
#include "include/callbackInterface.h"
#include "include/idManagerInterface.h"
#include "include/worldInterface.h"
#include "worldDefinitions.h"

class AgentInterface;
class DataBufferWriteInterface;
class ParameterInterface;
class RadioInterface;
class RoadStreamInterface;
class SceneryConverter;
class SceneryInterface;
class TrafficObjectInterface;
class WorldObjectInterface;
namespace OWL
{
namespace Interfaces
{
class WorldData;
}  // namespace Interfaces
}  // namespace OWL
namespace mantle_api
{
struct Weather;
}  // namespace mantle_api
struct AgentBuildInstructions;

/// Structure representing OSI parameters of the world
struct WorldParameterOSI
{
  //! Resets the parameters to their initial state
  void Reset()
  {
    timeOfDay = "";
    visibilityDistance = 0_m;
    friction = 0.0;
    weather = "";
  }

  std::string timeOfDay{""};                     //!< Time of day ranging from 1-24 [h]
  units::length::meter_t visibilityDistance{0};  //!< Defines how far a human driver can see [m]
  double friction{0.0};                          //!< Friction on the road
  std::string weather{""};                       //!< Weather as string
  TrafficRules trafficRules{};                   //!< Defines which traffic rules are in effect
};

/** \addtogroup World
* @{
* \brief implementation of a world interface
*
* The World module implements a WorldInterface which is used
* by the framework and the agents.
* It is used to set up a basic simulated world with static objects like
* roads, intersection and curves and dynamic objects like cars, bicycles and
* pedastrians.
*
* The World handles all simulated objects.
*
* \section world_input Inputs
* name | meaning
* -----|---------
* callbacks | CallbackInterface to send log information to the framework
*
* \section world_output Outputs
 name | meaning
* -----|---------
* *WoldInterface | Provides a reference to an implementation of a WorldInterface to the framework.
*
* \section world_configParameter Parameters to be specified in runConfiguration.xml
* tag | meaning
* -----|---------
* World | Name of World library. "World" by default.
*
* @see WorldInterface
*
* @} */

/*!
 * \brief Implementation of a WorldInterface
 *
 * This class implements all function of the WorldInterface. It is responsible for all
 * dynamic and static objects in a given scenery.
 *
 * \ingroup World
 */
class WorldImplementation : public WorldInterface
{
public:
  /// Name of the current module
  const std::string MODULENAME = "WORLD";

  /**
   * @brief WorldImplementation constructor
   *
   * @param[in] callbacks     Pointer to the callbacks
   * @param[in] idManager     Pointer to the IdManager
   * @param[in] dataBuffer    Pointer to the data buffer that provides write-only access to the data
   * @param[in] worldData     the data of the world
   */
  WorldImplementation(const CallbackInterface *callbacks,
                      DataBufferWriteInterface *dataBuffer,
                      IdManagerInterface *idManager,
                      OWL::Interfaces::WorldData &worldData);
  WorldImplementation(const WorldImplementation &) = delete;
  WorldImplementation(WorldImplementation &&) = delete;
  WorldImplementation &operator=(const WorldImplementation &) = delete;
  WorldImplementation &operator=(WorldImplementation &&) = delete;

  ~WorldImplementation() override;

  AgentInterface *GetAgent(int id) const override;
  const std::vector<const WorldObjectInterface *> &GetWorldObjects() const override;
  std::map<int, AgentInterface *> GetAgents() override;
  const std::vector<int> GetRemovedAgentsInPreviousTimestep() override;

  const std::vector<const TrafficObjectInterface *> &GetTrafficObjects() const override;
  const TrafficRules &GetTrafficRules() const override;

  // framework internal methods to access members without restrictions
  void ExtractParameter(ParameterInterface *parameters) override;

  void ClearAgents() override;

  // model callbacks
  std::string GetTimeOfDay() const override;

  void *GetWorldData() override;
  void *GetOsiGroundTruth() const override;

  void QueueAgentUpdate(std::function<void()> func) override;
  void QueueAgentRemove(const AgentInterface *agent) override;

  void RemoveAgents() override;

  /// @brief Remove agent from the world
  /// @param agent Agent to be removed
  void RemoveAgent(const AgentInterface *agent);

  void PublishGlobalData() override;
  void SyncGlobalData() override;

  bool CreateScenery(const SceneryInterface *scenery, const TurningRates &turningRates) override;

  void SetWeather(const mantle_api::Weather &weather) override;

  void SetTrafficSignalState(const std::string &trafficSignalName, const std::string &trafficSignalState) override;

  AgentInterface *CreateAgentAdapterForAgent() override
  {
    throw std::runtime_error(
        "WorldImplementation::CreateAgentAdapterForAgent: Deprecated method not implemented (see worldInterface.h)");
  }

  AgentInterface &CreateAgentAdapter(mantle_api::UniqueId id,
                                     const AgentBuildInstructions &agentBuildInstructions) override;

  AgentInterface *GetEgoAgent() override;

  AgentInterface *GetAgentByName(const std::string &scenarioName) override;

  RouteQueryResult<std::optional<GlobalRoadPosition>> ResolveRelativePoint(
      const RoadGraph &roadGraph,
      RoadGraphVertex startNode,
      ObjectPointRelative relativePoint,
      const WorldObjectInterface &object) const override;

  RouteQueryResult<std::vector<const WorldObjectInterface *>> GetObjectsInRange(
      const RoadGraph &roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t startDistance,
      units::length::meter_t backwardRange,
      units::length::meter_t forwardRange) const override;
  AgentInterfaces GetAgentsInRangeOfJunctionConnection(std::string connectingRoadId,
                                                       units::length::meter_t range) const override;

  units::length::meter_t GetDistanceToConnectorEntrance(
      /*const ObjectPosition position,*/ std::string intersectingConnectorId,
      int intersectingLaneId,
      std::string ownConnectorId) const override;
  units::length::meter_t GetDistanceToConnectorDeparture(
      /*const ObjectPosition position,*/ std::string intersectingConnectorId,
      int intersectingLaneId,
      std::string ownConnectorId) const override;

  std::optional<Position> LaneCoord2WorldCoord(units::length::meter_t distanceOnLane,
                                               units::length::meter_t offset,
                                               std::string roadId,
                                               OWL::OdId laneId) const override;

  GlobalRoadPositions WorldCoord2LaneCoord(units::length::meter_t x,
                                           units::length::meter_t y,
                                           units::angle::radian_t heading) const override;

  bool IsSValidOnLane(std::string roadId, int laneId, units::length::meter_t distance) override;

  bool IsDirectionalRoadExisting(const std::string &roadId, bool inOdDirection) const override;

  bool IsLaneTypeValid(const std::string &roadId,
                       const int laneId,
                       const units::length::meter_t distanceOnLane,
                       const LaneTypes &validLaneTypes) override;

  units::curvature::inverse_meter_t GetLaneCurvature(std::string roadId,
                                                     int laneId,
                                                     units::length::meter_t position) const override;

  RouteQueryResult<std::optional<units::curvature::inverse_meter_t>> GetLaneCurvature(
      const RoadGraph &roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t position,
      units::length::meter_t distance) const override;

  units::length::meter_t GetLaneWidth(std::string roadId, int laneId, units::length::meter_t position) const override;

  RouteQueryResult<std::optional<units::length::meter_t>> GetLaneWidth(const RoadGraph &roadGraph,
                                                                       RoadGraphVertex startNode,
                                                                       int laneId,
                                                                       units::length::meter_t position,
                                                                       units::length::meter_t distance) const override;

  units::angle::radian_t GetLaneDirection(std::string roadId,
                                          OWL::OdId laneId,
                                          units::length::meter_t position) const override;

  RouteQueryResult<std::optional<units::angle::radian_t>> GetLaneDirection(
      const RoadGraph &roadGraph,
      RoadGraphVertex startNode,
      OWL::OdId laneId,
      units::length::meter_t position,
      units::length::meter_t distance) const override;

  RouteQueryResult<units::length::meter_t> GetDistanceToEndOfLane(
      const RoadGraph &roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t initialSearchDistance,
      units::length::meter_t maximumSearchLength) const override;

  RouteQueryResult<units::length::meter_t> GetDistanceToEndOfLane(const RoadGraph &roadGraph,
                                                                  RoadGraphVertex startNode,
                                                                  int laneId,
                                                                  units::length::meter_t initialSearchDistance,
                                                                  units::length::meter_t maximumSearchLength,
                                                                  const LaneTypes &laneTypes) const override;

  RouteQueryResult<std::optional<units::length::meter_t>> GetDistanceBetweenObjects(
      const RoadGraph &roadGraph,
      RoadGraphVertex startNode,
      units::length::meter_t ownPosition,
      const GlobalRoadPositions &target) const override;

  LaneSections GetLaneSections(const std::string &roadId) const override;

  bool IntersectsWithAgent(units::length::meter_t x,
                           units::length::meter_t y,
                           units::angle::radian_t rotation,
                           units::length::meter_t length,
                           units::length::meter_t width,
                           units::length::meter_t center) override;

  std::optional<Position> RoadCoord2WorldCoord(RoadPosition roadCoord, std::string roadID) const override;

  units::length::meter_t GetRoadLength(const std::string &roadId) const override;

  units::length::meter_t GetVisibilityDistance() const override;

  RouteQueryResult<Obstruction> GetObstruction(
      const RoadGraph &roadGraph,
      RoadGraphVertex startNode,
      const GlobalRoadPosition &ownPosition,
      const std::map<ObjectPoint, Common::Vector2d<units::length::meter_t>> &points,
      const RoadIntervals &touchedRoads) const override;

  RouteQueryResult<std::vector<CommonTrafficSign::Entity>> GetTrafficSignsInRange(
      const RoadGraph &roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t startDistance,
      units::length::meter_t searchRange) const override;

  RouteQueryResult<std::vector<CommonTrafficSign::Entity>> GetRoadMarkingsInRange(
      const RoadGraph &roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t startDistance,
      units::length::meter_t searchRange) const override;

  RouteQueryResult<std::vector<CommonTrafficLight::Entity>> GetTrafficLightsInRange(
      const RoadGraph &roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t startDistance,
      units::length::meter_t searchRange) const override;

  RouteQueryResult<std::vector<LaneMarking::Entity>> GetLaneMarkings(const RoadGraph &roadGraph,
                                                                     RoadGraphVertex startNode,
                                                                     int laneId,
                                                                     units::length::meter_t startDistance,
                                                                     units::length::meter_t range,
                                                                     Side side) const override;

  [[deprecated]] RouteQueryResult<RelativeWorldView::Roads> GetRelativeJunctions(
      const RoadGraph &roadGraph,
      RoadGraphVertex startNode,
      units::length::meter_t startDistance,
      units::length::meter_t range) const override;

  RouteQueryResult<RelativeWorldView::Roads> GetRelativeRoads(const RoadGraph &roadGraph,
                                                              RoadGraphVertex startNode,
                                                              units::length::meter_t startDistance,
                                                              units::length::meter_t range) const override;

  RouteQueryResult<RelativeWorldView::Lanes> GetRelativeLanes(const RoadGraph &roadGraph,
                                                              RoadGraphVertex startNode,
                                                              int laneId,
                                                              units::length::meter_t distance,
                                                              units::length::meter_t range,
                                                              bool includeOncoming = true) const override;

  RouteQueryResult<std::optional<int>> GetRelativeLaneId(const RoadGraph &roadGraph,
                                                         RoadGraphVertex startNode,
                                                         int laneId,
                                                         units::length::meter_t distance,
                                                         GlobalRoadPositions targetPosition) const override;

  RouteQueryResult<AgentInterfaces> GetAgentsInRange(const RoadGraph &roadGraph,
                                                     RoadGraphVertex startNode,
                                                     int laneId,
                                                     units::length::meter_t startDistance,
                                                     units::length::meter_t backwardRange,
                                                     units::length::meter_t forwardRange) const override;

  std::vector<JunctionConnection> GetConnectionsOnJunction(std::string junctionId,
                                                           std::string incomingRoadId) const override;

  std::vector<IntersectingConnection> GetIntersectingConnections(std::string connectingRoadId) const override;

  std::vector<JunctionConnectorPriority> GetPrioritiesOnJunction(std::string junctionId) const override;

  RoadNetworkElement GetRoadSuccessor(std::string roadId) const override;

  RoadNetworkElement GetRoadPredecessor(std::string roadId) const override;

  std::pair<RoadGraph, RoadGraphVertex> GetRoadGraph(const RouteElement &start,
                                                     int maxDepth,
                                                     bool inDrivingDirection = true) const override;

  std::map<RoadGraphEdge, double> GetEdgeWeights(const RoadGraph &roadGraph) const override;

  std::unique_ptr<RoadStreamInterface> GetRoadStream(const std::vector<RouteElement> &route) const override;

  double GetFriction() const override;

  void *GetGlobalDrivingView() const override
  {
    throw std::runtime_error("WorldImplementation::GetGlobalDrivingView not implemented");
  }
  void *GetGlobalObjects() const override
  {
    throw std::runtime_error("WorldImplementation::GetGlobalObjects not implemented");
  }

  void SetTimeOfDay(int timeOfDay) override
  {
    throw std::runtime_error("WorldImplementation::SetTimeOfDay not implemented");
  }

  void SetWeekday(Weekday weekday) override
  {
    throw std::runtime_error("WorldImplementation::SetWeekday not implemented");
  }

  Weekday GetWeekday() const override { throw std::runtime_error("WorldImplementation::GetWeekday not implemented"); }

  bool CreateGlobalDrivingView() override
  {
    throw std::runtime_error("WorldImplementation::CreateGlobalDrivingView not implemented");
  }
  const AgentInterface *GetSpecialAgent() const override
  {
    throw std::runtime_error("WorldImplementation::GetSpecialAgent not implemented");
  }

  const AgentInterface *GetLastCarInlane(int laneNumber) const override
  {
    throw std::runtime_error("WorldImplementation::GetLastCarInlane not implemented");
  }
  const AgentInterface *GetBicycle() const override
  {
    throw std::runtime_error("WorldImplementation::GetBicycle not implemented");
  }

  void QueueAgentUpdate(std::function<void(double)> func, double val) override
  {
    throw std::runtime_error("WorldImplementation::QueueAgentUpdate not implemented");
  }

  bool CreateWorldScenery(const std::string &sceneryFilename) override
  {
    throw std::runtime_error("WorldImplementation::CreateWorldScenery not implemented");
  }

  bool CreateWorldScenario(const std::string &scenarioFilename) override
  {
    throw std::runtime_error("WorldImplementation::CreateWorldScenario not implemented");
  }

  RadioInterface &GetRadio() override;

  uint64_t GetUniqueLaneId(std::string roadId, OWL::OdId laneId, units::length::meter_t position) const override;

protected:
  //-----------------------------------------------------------------------------
  //! Provides callback to LOG() macro
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-----------------------------------------------------------------------------
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
  {
    if (callbacks)
    {
      callbacks->Log(logLevel, file, line, message);
    }
  }

private:
  void InitTrafficObjects();

  OWL::Interfaces::WorldData &worldData;
  WorldDataQuery worldDataQuery{worldData};
  World::Localization::Localizer localizer{worldData};

  std::vector<const TrafficObjectInterface *> trafficObjects;

  // world parameters
  WorldParameterOSI worldParameter;

  AgentNetwork agentNetwork;

  const CallbackInterface *callbacks;

  mutable std::vector<const WorldObjectInterface *> worldObjects;

  const int stepSizeLookingForValidS = 100;
  const SceneryInterface *scenery{nullptr};
  RadioImplementation radio;

  std::unordered_map<const OWL::Interfaces::MovingObject *, AgentInterface *> movingObjectMapping{{nullptr, nullptr}};
  std::unordered_map<const OWL::Interfaces::MovingObject *, TrafficObjectInterface *> stationaryObjectMapping{
      {nullptr, nullptr}};

  DataBufferWriteInterface *dataBuffer;
  IdManagerInterface *idManager;
  EntityInfoPublisher entityInfoPublisher;
  std::unique_ptr<SceneryConverter> sceneryConverter;
};
