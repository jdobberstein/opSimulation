/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "WorldToRoadCoordinateConverter.h"

#include <optional>

#include "OWL/LaneGeometryElement.h"
#include "OWL/LaneGeometryJoint.h"
#include "common/commonHelper.h"
#include "common/commonTools.h"

namespace World::Localization
{

bool WorldToRoadCoordinateConverter::IsConvertible(const Common::Vector2d<units::length::meter_t>& point) const
{
  return CommonHelper::IntersectionCalculation::IsWithin(element.laneGeometryElement.joints.current.points.left,
                                                         element.laneGeometryElement.joints.next.points.left,
                                                         element.laneGeometryElement.joints.current.points.right,
                                                         element.laneGeometryElement.joints.next.points.right,
                                                         point);
}

RoadPosition WorldToRoadCoordinateConverter::GetRoadCoordinate(const Common::Vector2d<units::length::meter_t>& point,
                                                               units::angle::radian_t hdg) const
{
  const auto intersectionPoint = GetIntersectionPoint(point);
  const auto s = CalcS(intersectionPoint);
  const auto t = CalcT(point, intersectionPoint);
  const auto yaw = CalcYaw(hdg);
  return {s, t, yaw};
}

units::length::meter_t WorldToRoadCoordinateConverter::GetS(const Common::Vector2d<units::length::meter_t>& point) const
{
  const auto intersectionPoint = GetIntersectionPoint(point);
  return CalcS(intersectionPoint);
}

Common::Vector2d<units::length::meter_t> WorldToRoadCoordinateConverter::GetIntersectionPoint(
    const Common::Vector2d<units::length::meter_t>& point) const
{
  Common::Vector2d<units::length::meter_t> intersectionPoint;
  if (element.tAxisCenter)
  {
    intersectionPoint = CommonHelper::CalculateIntersection(element.laneGeometryElement.joints.current.points.reference,
                                                            element.referenceVector,
                                                            element.tAxisCenter.value(),
                                                            point - element.tAxisCenter.value())
                            .value_or(element.laneGeometryElement.joints.current.points.reference);
  }
  else
  {
    intersectionPoint = ProjectOntoReferenceAxis(point);
  }
  return intersectionPoint;
}

units::length::meter_t WorldToRoadCoordinateConverter::CalcS(
    const Common::Vector2d<units::length::meter_t>& intersectionPoint) const
{
  Common::Vector2d<units::length::meter_t> sVector
      = intersectionPoint - element.laneGeometryElement.joints.current.points.reference;
  return element.laneGeometryElement.joints.current.sOffset
       + static_cast<units::length::meter_t>(sVector.Length() * element.referenceScale);
}

units::length::meter_t WorldToRoadCoordinateConverter::CalcT(
    const Common::Vector2d<units::length::meter_t>& point,
    const Common::Vector2d<units::length::meter_t>& intersectionPoint) const
{
  Common::Vector2d<units::length::meter_t> tVector = point - intersectionPoint;
  units::length::meter_t tCoordinate{tVector.Length()};
  return IsLeftOfReferenceAxis(tVector) ? tCoordinate : -tCoordinate;
}

units::angle::radian_t WorldToRoadCoordinateConverter::CalcYaw(units::angle::radian_t hdg) const
{
  // Updated for direction awareness - might be an issue, when cars try to turn around
  return CommonHelper::SetAngleToValidRange(hdg - element.laneGeometryElement.joints.current.sHdg);
}

Common::Vector2d<units::length::meter_t> WorldToRoadCoordinateConverter::ProjectOntoReferenceAxis(
    const Common::Vector2d<units::length::meter_t>& point) const
{
  const double lamdba = element.referenceVector.Dot(point - element.laneGeometryElement.joints.current.points.reference)
                      / element.referenceVector.Dot(element.referenceVector);
  return element.laneGeometryElement.joints.current.points.reference + element.referenceVector * lamdba;
}

bool WorldToRoadCoordinateConverter::IsLeftOfReferenceAxis(const Common::Vector2d<units::length::meter_t>& vector) const
{
  Common::Vector2d<units::length::meter_t> vectorToLeft{-element.referenceVector.y, element.referenceVector.x};
  return vector.Dot(vectorToLeft) >= 0_sq_m;
}

}  // namespace World::Localization
