/********************************************************************************
 * Copyright (c) 2016-2017 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  WorldGlobal.h
//! @brief This file contains DLL export declarations
//-----------------------------------------------------------------------------

#pragma once

#include "sim/src/common/opExport.h"

#if defined(WORLD_LIBRARY)
/// Export of the dll-functions
#define WORLD_SHARED_EXPORT OPEXPORT
#else
/// Import of the dll-functions
#define WORLD_SHARED_EXPORT OPIMPORT
#endif
