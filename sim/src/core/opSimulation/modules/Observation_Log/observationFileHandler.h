/********************************************************************************
 * Copyright (c) 2017 ITK Engineering GmbH
 *               2019-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <libxml/xmlwriter.h>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "common/openPassTypes.h"
#include "common/sensorDefinitions.h"
#include "include/dataBufferInterface.h"
#include "observationLogConstants.h"
#include "runStatistic.h"

class ObservationCyclics;
class RunResultInterface;

struct Event  ///< An openpass Event
{
  /// @brief Event constructor
  /// @param time     Time of event occurance
  /// @param dataRow  Reference to the Acyclic Row
  Event(const int& time, openpass::databuffer::AcyclicRow dataRow) : time(time), dataRow(std::move(dataRow)) {}

  /**
   * @brief Equals operator for Event matching
   *
   * @param other Another Event
   * @return True if contents of the Event are equal to other, false otherwise
   */
  bool operator==(const Event& other) const { return time == other.time && dataRow == other.dataRow; }

  int time;                                  ///< Time of event occurance
  openpass::databuffer::AcyclicRow dataRow;  ///< Acyclic row data buffer
};

using Events = std::vector<Event>;

//-----------------------------------------------------------------------------
/** \brief Provides the basic logging and observer functionality.
 *   \details Provides the basic logging functionality.
 *            Writes the framework, events, agents and samples into the output.
 *            It has two logging modes either event based or as single output.
 *            This module also acts as an observer of agent modules.
 *
 *   \ingroup ObservationLog
 */
//-----------------------------------------------------------------------------
class ObservationFileHandler
{
public:
  /// Name of the current component
  const std::string COMPONENTNAME = "ObservationFileHandler";

  /**
   * @brief ObservationFileHandler constructor
   *
   * @param[in]   dataBuffer  Pointer to the data buffer that provides read-only access to the data
   */
  ObservationFileHandler(const DataBufferReadInterface& dataBuffer);
  ObservationFileHandler(const ObservationFileHandler&) = delete;
  ObservationFileHandler(ObservationFileHandler&&) = delete;
  ObservationFileHandler& operator=(const ObservationFileHandler&) = delete;
  ObservationFileHandler& operator=(ObservationFileHandler&&) = delete;
  ~ObservationFileHandler() = default;

  /*!
   * \brief Sets the directory where the output is written into
   *
   * \param outputDir     Path of output directory
   * \param outputFile    Path of output file
   */
  void SetOutputLocation(const std::string& outputDir, const std::string& outputFile)
  {
    folder = outputDir;
    finalFilename = outputFile;
    finalPath = folder / finalFilename;
  }

  /// @brief Setter function for scenery file
  /// @param fileName File name of the scenery file
  void SetSceneryFile(const std::string& fileName) { sceneryFile = fileName; }

  /// @brief Setter function to write CSV output
  /// @param writeCsv True, if the cyclics are to be written to CSV
  void SetCsvOutput(bool writeCsv) { writeCyclicsToCsv = writeCsv; }

  /*!
   * \brief Creates the output file as simulationOutput.tmp and writes the basic header information
   *
   * \param[in] frameworkVersion  Framework version
   */
  void WriteStartOfFile(const std::string& frameworkVersion);

  /*!
   * \brief This function gets called after each run and writes all information about this run into the output file
   *
   * \param runResult
   * \param runStatistic
   * \param cyclics
   * \param cyclicsActiveComponent  cyclics containing info about active components
   * \param events
   */
  void WriteRun([[maybe_unused]] const RunResultInterface& runResult,
                RunStatistic runStatistic,
                ObservationCyclics& cyclics,
                ObservationCyclics& cyclicsActiveComponent,
                const Events& events);

  /*!
   * \brief Closes the xml tags flushes the output file, closes it and renames it to simulationOutput.xlm
   */
  void WriteEndOfFile();

protected:
  xmlTextWriterPtr xmlFileStream{};           ///< Unique pointer to the stream writer
  const DataBufferReadInterface& dataBuffer;  ///< Pointer to the data buffer that provides read-only access to the data

  int runNumber = 0;        ///< run number
  std::string sceneryFile;  ///< Scenery file name

  bool writeCyclicsToCsv{false};  ///< Boolean, if cyclics are to be written to CSV

  OutputAttributes outputAttributes;  ///< Output attributes
  OutputTags outputTags;              ///< Output tags

  std::filesystem::path folder;         ///< Output directory
  std::filesystem::path tmpFilename;    ///< File name temporary
  std::filesystem::path finalFilename;  ///< Name of output file
  std::filesystem::path tmpFilePath;    ///< Temporary path of the output directory
  std::filesystem::path finalPath;      ///< Final path of the output directory
  std::ofstream xmlFile;                ///< xml file

  //add infos to the file stream
  /*!
   * \brief Returns whether the agent has sensors or not.
   *
   * @param[in]    sensorParameters    Parameters which describe the sensor
   * @return       true if agent has sensors, otherwise false.
   */
  inline bool ContainsSensor(const openpass::sensors::Parameters& sensorParameters) const;

  /*!
   * \brief Writes the sensor information into the simulation output.
   *
   * @param[in]    agentId     id of the agent
   * @param[in]    sensorId    id of the sensor
   */
  void AddSensor(const std::string& agentId, const std::string& sensorId);

  /*!
   * \brief Writes the sensor information into the simulation output.
   *
   * @param[in]    agentId     id of the agent
   */
  void AddVehicleAttributes(const std::string& agentId);

  /*!
   * \brief Writes all sensor information of an agent into the simulation output.
   *
   * @param[in]    agentId     id of the agent
   */
  void AddComponents(const std::string& agentId);

  /*!
   * \brief Writes all sensor information of an agent into the simulation output.
   *
   * @param[in]    agentId     id of the agent
   */
  void AddSensors(const std::string& agentId);

  /*!
   * \brief Writes the content of an agent into the simulation output.
   *
   * @param[in]    agentId     id of the agent
   */
  void AddAgent(const std::string& agentId);

  /*!
   * \brief Writes the content of all agent into the simulation output.
   */
  void AddAgents();

  /*!
   * \brief Writes all events into the simulation output.
   *
   * @param[in]     events     events of the run
   * @param[in]     cyclicsActiveComponent  cyclics containing info about active components
   */
  void AddEvents(const Events& events, const ObservationCyclics& cyclicsActiveComponent);

  /*!
   * \brief Parses all changes in the active components of the agents
   *
   * @param[in]     cyclicsActiveComponent  cyclics containing info about active components
   * @return  list of active components changes
   */
  Events ParseActiveComponentChanges(const ObservationCyclics& cyclicsActiveComponent);

  /*!
   * \brief Writes the header into the simulation output during full logging.
   *
   * @param[in]     cyclics    cyclics of the run
   */
  void AddHeader(const ObservationCyclics& cyclics);

  /*!
   * \brief Writes the samples into the simulation output during full logging.
   *
   * @param[in]     cyclics    cyclics of the run
   */
  void AddSamples(const ObservationCyclics& cyclics);

  /*!
   * \brief Writes the filename for the cyclics file into the simulation output during full logging.
   *
   * @param[in]     filename           Name of the file, where cyclics are written to.
   */
  void AddReference(const std::string& filename);

  /*!
   * \brief Writes the cyclics of one run to a csv.
   *
   * @param[in]    filepath            Filepath for current run
   * @param[in]    cyclics             Cyclics of the current run
   */
  void WriteCsvCyclics(const std::filesystem::path& filepath, const ObservationCyclics& cyclics);

  /*!
   * \brief Write entities to XML
   *
   * \param tag       tag name
   * \param entities  list of entities
   * \param mandatory if set, an emtpy tag is added if entities is empty (default false)
   */
  void WriteEntities(const char* tag, const openpass::type::EntityIds& entities, bool mandatory = false);

  /*!
   * \brief Write (event) parameter to XML
   *
   * \remark Might be used for generic parameters in the future - right now, only event parameters
   *
   * \param[in]    parameters  list of parameters
   * \param[in]    mandatory   if set, an emtpy tag is added if parameters are empty (default false)
   */
  void WriteParameter(const openpass::type::FlatParameter& parameters, bool mandatory = false);

  /// Output schema version
  static constexpr const char* outputFileVersion = "0.3.1";
};
