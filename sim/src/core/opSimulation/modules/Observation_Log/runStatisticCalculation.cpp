/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "runStatisticCalculation.h"

#include <algorithm>
#include <vector>

#include "include/agentInterface.h"
#include "include/runResultInterface.h"
#include "include/worldInterface.h"
#include "runStatistic.h"

void RunStatisticCalculation::DetermineEgoCollision(RunStatistic& runStatistic,
                                                    const RunResultInterface& runResult,
                                                    WorldInterface* world)
{
  auto* egoAgent = world->GetEgoAgent();

  if (!egoAgent)
  {
    return;
  }

  const int egoId = egoAgent->GetId();
  const auto* collisionsIds = runResult.GetCollisionIds();

  if (std::find(collisionsIds->cbegin(), collisionsIds->cend(), egoId) != collisionsIds->cend())
  {
    runStatistic.EgoCollision = true;
  }
}
