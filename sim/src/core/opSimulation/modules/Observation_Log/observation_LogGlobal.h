/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * \file  observation_LogGlobal.h
 * \brief This file contains DLL export declarations
 */

#pragma once

#include "sim/src/common/opExport.h"

#if defined(OBSERVATION_LOG_LIBRARY)
#define OBSERVATION_LOG_SHARED_EXPORT OPEXPORT  ///< Export of the dll-functions
#else
#define OBSERVATION_LOG_SHARED_EXPORT OPIMPORT  ///< Import of the dll-functions
#endif
