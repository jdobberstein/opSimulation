/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <string>
#include <utility>

#include "common/logEntryBase.h"
#include "include/dataBufferInterface.h"
#include "include/publisherInterface.h"

namespace openpass::publisher
{

static inline constexpr int CORE_ID = 67798269;  /// ASCII: C O R E

/// Class for logging of acyclic events in the dataBuffer
class LogEntry : public LogEntryBase
{
public:
  /**
   * @brief LogEntry constructor
   *
   * @param[in]    name    Name of the event
   */
  LogEntry(std::string name) : LogEntryBase(std::move(name)) {}

  /// @return operator function
  operator Acyclic() const noexcept override { return Acyclic(name, triggeringEntities, affectedEntities, parameter); }

  openpass::databuffer::Parameter parameter;  //!< Generic parameter set associated with event
};

/// Interface which has to be provided by observation modules
class CoreDataPublisher : public PublisherInterface
{
public:
  /**
   * @brief CoreDataPublisher constructor
   *
   * @param[in]   dataBuffer  Pointer to the DataBufferWriteInterface in order to register the dataBuffer backend
   */
  CoreDataPublisher(DataBufferWriteInterface *const dataBuffer) : PublisherInterface(dataBuffer) {}

  void Publish(const openpass::databuffer::Key &key, const openpass::publisher::LogEntryBase &event) override;
};

}  //namespace openpass::publisher
