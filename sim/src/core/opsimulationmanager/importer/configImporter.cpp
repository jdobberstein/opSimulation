/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2019 ITK Engineering GmbH
 *               2019 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "configImporter.h"

#include <locale>
#include <optional>
#include <stdexcept>
#include <string>

#include <qbytearray.h>
#include <qcoreapplication.h>
#include <qdir.h>
#include <qfile.h>
#include <qfileinfo.h>
#include <qiodevice.h>

#include "../framework/config.h"
#include "../framework/simulationConfig.h"
#include "common/xmlParser.h"

namespace SimulationManager::Configuration
{

bool GetFirstChildElement(const QDomElement &rootElement, const std::string &tag, QDomElement &result)
{
  QDomNode node = rootElement.firstChildElement(QString::fromStdString(tag));
  if (node.isNull())
  {
    return false;
  }

  result = node.toElement();
  return !result.isNull();
}

bool ParseString(const QDomElement &rootElement, const std::string &tag, std::string &result)
{
  QDomNode node = rootElement.firstChildElement(QString::fromStdString(tag));
  if (node.isNull())
  {
    return false;
  }

  QDomElement element = node.toElement();
  if (element.isNull())
  {
    return false;
  }

  result = element.text().toStdString();

  return true;
}

bool ParseInt(const QDomElement &rootElement, const std::string &tag, int &result)
{
  QDomNode node = rootElement.firstChildElement(QString::fromStdString(tag));
  if (node.isNull())
  {
    return false;
  }
  QDomElement element = node.toElement();
  if (element.isNull())
  {
    return false;
  }
  try
  {
    result = std::stoi(element.text().toStdString());
  }
  catch (...)
  {
    return false;
  }
  return true;
}

template <typename T>
bool Parse([[maybe_unused]] const QDomElement &rootElement,
           [[maybe_unused]] const std::string &tag,
           [[maybe_unused]] T &result)
{
  throw std::runtime_error("xmlParser::Parse<T> not implemented yet");
}

template <>
bool Parse(const QDomElement &rootElement, const std::string &tag, std::string &result)
{
  return ParseString(rootElement, tag, result);
}

template <>
bool Parse(const QDomElement &rootElement, const std::string &tag, int &result)
{
  return ParseInt(rootElement, tag, result);
}

template <typename T>
///
/// \brief Gets the value of a given tag
/// \param element root element
/// \param tag     tag
/// \return value if tag was found, std::nullopt else
///
std::optional<T> GetValue(QDomElement element, const std::string &tag)
{
  QDomNode node = element.firstChildElement(QString::fromStdString(tag));
  if (!node.isNull())
  {
    T result;
    if (Parse<T>(element, tag, result))
    {
      return std::make_optional<T>(result);
    }
  }
  return std::nullopt;
}

/// \brief Helper method to dig into the xml tree
/// <a><b><c> => b = get(a), c = get(b), etc...
/// If tag is not available, a runtime_error is issued
///
/// \param[in] element root
/// \param[in] tag     tag of child
/// \return child element
QDomElement GetChildOrThrow(const QDomElement &element, const std::string &tag)
{
  QDomElement child;
  GetFirstChildElement(element, tag, child);
  if (child.isNull())
  {
    throw std::runtime_error("Missing tag " + tag);
  }
  return child;
}

Config ConfigImporter::Import(const QString &filename)
{
  std::locale::global(std::locale("C"));

  // file handling
  QString absoluteFilePath = GetAbsoluteFilePath(filename);
  QFile xmlFile(absoluteFilePath);
  QDomElement document = ReadDocument(xmlFile, absoluteFilePath.toStdString());

  return Config{GetValue<int>(document, "logLevel"),
                GetValue<std::string>(document, "logFileOpSimulationManager"),
                GetValue<std::string>(document, "simulation"),
                GetValue<std::string>(document, "libraries"),
                ParseSimulationConfigs(document)};
}

SimulationConfigs ConfigImporter::ParseSimulationConfigs(const QDomElement &element)
{
  auto simulationConfigRoot = GetChildOrThrow(element, "simulationConfigs");

  const std::string simulationConfigEntryTag{"simulationConfig"};
  auto simulationConfigEntry = GetChildOrThrow(simulationConfigRoot, simulationConfigEntryTag);

  SimulationConfigs simulationConfigs;
  while (!simulationConfigEntry.isNull())
  {
    simulationConfigs.emplace_back(ParseSimulationConfig(simulationConfigEntry));
    simulationConfigEntry = simulationConfigEntry.nextSiblingElement(QString::fromStdString(simulationConfigEntryTag));
  }

  return simulationConfigs;
}

SimulationConfig ConfigImporter::ParseSimulationConfig(const QDomElement &element)
{
  return SimulationConfig{GetValue<std::string>(element, "logFileSimulation"),
                          GetValue<std::string>(element, "configurations"),
                          GetValue<std::string>(element, "results")};
}

QDomElement ConfigImporter::ReadDocument(QFile &xmlFile, const std::string &file)
{
  if (!xmlFile.open(QIODevice::ReadOnly))
  {
    throw std::runtime_error("Could not open file " + file);
  }

  QByteArray xmlData(xmlFile.readAll());
  QDomDocument document;
  QString errorMsg;
  int errorLine = 0;

  if (!document.setContent(xmlData, &errorMsg, &errorLine))
  {
    throw std::runtime_error("Invalid xml file: " + file + "(" + std::to_string(errorLine) + ")");
  }

  QDomElement documentRoot = document.documentElement();
  if (documentRoot.isNull())
  {
    throw std::runtime_error("Could not find xml root in " + file);
  }

  return documentRoot;
}

QString ConfigImporter::GetAbsoluteFilePath(const QString &filename)
{
  QString absoluteFilePath = "";

  if (QFileInfo(filename).isRelative())
  {
    QDir baseDir = QCoreApplication::applicationDirPath();
    absoluteFilePath = baseDir.absoluteFilePath(filename);
    absoluteFilePath = baseDir.cleanPath(absoluteFilePath);
  }
  else
  {
    absoluteFilePath = filename;
  }

  if (!QFileInfo(absoluteFilePath).exists())
  {
    throw std::runtime_error(absoluteFilePath.toStdString() + " does not exist");
  }

  return absoluteFilePath;
}

}  // namespace SimulationManager::Configuration
