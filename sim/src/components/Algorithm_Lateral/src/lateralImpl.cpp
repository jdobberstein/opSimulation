/********************************************************************************
 * Copyright (c) 2018-2019 AMFD GmbH
 *               2016-2017 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  lateralImpl.cpp */
//-----------------------------------------------------------------------------

#include "lateralImpl.h"

#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <memory>
#include <new>
#include <optional>
#include <stdexcept>

#include "common/commonTools.h"
#include "common/lateralSignal.h"
#include "common/parametersVehicleSignal.h"
#include "common/steeringSignal.h"
#include "components/Algorithm_Lateral/src/steeringController.h"
#include "components/Sensor_Driver/src/Signals/sensorDriverSignal.h"
#include "components/Sensor_Driver/src/Signals/sensor_driverDefinitions.h"
#include "components/common/vehicleProperties.h"
#include "include/callbackInterface.h"
#include "include/signalInterface.h"

void AlgorithmLateralImplementation::UpdateInput(int localLinkId,
                                                 const std::shared_ptr<SignalInterface const> &data,
                                                 [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    // from PrioritizerLateralVehicleComponent or Driver
    const std::shared_ptr<ComponentStateSignalInterface const> stateSignal
        = std::dynamic_pointer_cast<ComponentStateSignalInterface const>(data);
    if (stateSignal->componentState == ComponentState::Acting)
    {
      isActive = true;
      const std::shared_ptr<LateralSignal const> signal = std::dynamic_pointer_cast<LateralSignal const>(data);
      if (!signal)
      {
        const std::string msg = COMPONENTNAME + " invalid signaltype";
        LOG(CbkLogLevel::Debug, msg);
        throw std::runtime_error(msg);
      }

      steeringController.SetLateralInput(*signal);
      source = signal->source;
    }
    else
    {
      isActive = false;
    }
  }
  else if (localLinkId == 100)
  {
    // from ParametersAgent
    const std::shared_ptr<ParametersVehicleSignal const> signal
        = std::dynamic_pointer_cast<ParametersVehicleSignal const>(data);
    if (!signal)
    {
      const std::string msg = COMPONENTNAME + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
    const auto steeringRatio
        = helper::map::query(signal->vehicleParameters.properties, Properties::Vehicle::STEERING_RATIO);
    THROWIFFALSE(steeringRatio.has_value(), "SteeringRatio was not defined in VehicleCatalog");

    steeringController.SetVehicleParameter(
        std::stod(steeringRatio.value()),
        signal->vehicleParameters.front_axle.max_steering * std::stod(steeringRatio.value()),
        signal->vehicleParameters.front_axle.bb_center_to_axle_center.x
            - signal->vehicleParameters.rear_axle.bb_center_to_axle_center.x);
  }
  else if (localLinkId == 101 || localLinkId == 102)
  {
    // from SensorDriver
    const std::shared_ptr<SensorDriverSignal const> signal = std::dynamic_pointer_cast<SensorDriverSignal const>(data);

    if (!signal)
    {
      const std::string msg = COMPONENTNAME + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }

    steeringController.SetVelocityAndSteeringWheelAngle(signal->GetOwnVehicleInformation().absoluteVelocity,
                                                        signal->GetOwnVehicleInformation().steeringWheelAngle);
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void AlgorithmLateralImplementation::UpdateOutput(int localLinkId,
                                                  std::shared_ptr<SignalInterface const> &data,
                                                  [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    try
    {
      if (isActive)
      {
        data = std::make_shared<SteeringSignal const>(ComponentState::Acting, out_desiredSteeringWheelAngle, source);
      }
      else
      {
        data = std::make_shared<SteeringSignal const>(ComponentState::Disabled, 0.0_rad);
      }
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void AlgorithmLateralImplementation::Trigger(int time)
{
  out_desiredSteeringWheelAngle = steeringController.CalculateSteeringAngle(units::time::millisecond_t(time));
}
