/********************************************************************************
 * Copyright (c) 2018-2019 AMFD GmbH
 *               2016-2017 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <units.h>

#include "common/globalDefinitions.h"
#include "common/lateralSignal.h"

/*!
 * \brief
 *
 *
 * \ingroup Algorithm_LateralDriver
 */
/// @brief Class to represent the controller for steering
class SteeringController
{
public:
  /*!
   * \brief Calculates the steering angle for a given timestep.
   *
   * @param[in]     time           Current scheduling time
   *
   * @return    steering angle
   */
  units::angle::radian_t CalculateSteeringAngle(units::time::millisecond_t time);

  /*!
   * \brief Sets the lateral input, which contains the desired lateral position.
   *
   * @param[in]     lateralSignal           LateralSignal
   */
  void SetLateralInput(const LateralSignal &lateralSignal) { in_lateralSignal = lateralSignal; }

  /*!
   * \brief Sets the vehicle parameters. This should ideally only be called once.
   *
   * @param[in]     steeringRatio                              steeringRatio
   * @param[in]     maximumSteeringWheelAngleAmplitude         maximumSteeringWheelAngleAmplitude
   * @param[in]     wheelbase                                  wheelbase
   */
  void SetVehicleParameter(const double &steeringRatio,
                           const units::angle::radian_t &maximumSteeringWheelAngleAmplitude,
                           const units::length::meter_t &wheelbase)
  {
    in_steeringRatio = steeringRatio;
    in_steeringMax = maximumSteeringWheelAngleAmplitude;
    in_wheelBase = wheelbase;
  }

  /*!
   * \brief Sets the current velocity and the current steeringwheel angle of the vehicle.
   *
   * @param[in]     velocity                              Current velocity
   * @param[in]     steeringWheelAngle         Current steeringwheel angle
   */
  void SetVelocityAndSteeringWheelAngle(const units::velocity::meters_per_second_t &velocity,
                                        const units::angle::radian_t &steeringWheelAngle)
  {
    in_velocity = velocity;
    in_steeringWheelAngle = steeringWheelAngle;
  }

protected:
  // --- module internal functions

  // --- module internal variables
  /// --- Inputs
  LateralSignal in_lateralSignal{};

  /// current velocity
  units::velocity::meters_per_second_t in_velocity{0.0};
  /// current angle of the steering wheel
  units::angle::radian_t in_steeringWheelAngle{0.0};

  /// The steering ratio of the vehicle.
  double in_steeringRatio = 10.7;
  /// The maximum steering wheel angle of the car in both directions in degree.
  units::angle::radian_t in_steeringMax{180.0};
  /// The wheelbase of the car in m.
  units::length::meter_t in_wheelBase{2.89};

  // --- Internal Parameters

  /// Time to Average regulation over
  units::time::second_t tAverage{0.};

  /// Previous scheduling time (for calculation of cycle time lenght).
  units::time::millisecond_t timeLast{-100};
  /// running average of  mean curvature up to NearPoint
  units::curvature::inverse_meter_t meanCurvatureToNearPointSmoothLast{0.};
  /// running average of  mean curvature from NearPoint up to FarPoint
  units::curvature::inverse_meter_t meanCurvatureToFarPointSmoothLast{0.};
  /// running average of kappaRoad at referencepoint
  units::curvature::inverse_meter_t curvatureRoadSmoothLast{0.};
};
