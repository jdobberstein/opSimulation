/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * @defgroup module_switch Switch
 * The Algorithm component for switch.
 * image html @cond ecu_switch.png @endcond "Overview of Switch"
 */

/**
 * @ingroup module_switch
 * @defgroup switch_group1 Initialization
 */

/**
 * @ingroup module_switch
 * @defgroup switch_group2 Calculation of outputs
 */

#include "algorithm_Switch_implementation.h"

#include <utility>

#include "components/Algorithm_Switch/switch.h"
#include "include/callbackInterface.h"

class AgentInterface;
class ParameterInterface;
class PublisherInterface;
class SignalInterface;
class StochasticsInterface;

Algorithm_Switch_Implementation::Algorithm_Switch_Implementation(std::string componentName,
                                                                 bool isInit,
                                                                 int priority,
                                                                 int offsetTime,
                                                                 int responseTime,
                                                                 int cycleTime,
                                                                 StochasticsInterface *stochastics,
                                                                 const ParameterInterface *parameters,
                                                                 PublisherInterface *const publisher,
                                                                 const CallbackInterface *callbacks,
                                                                 AgentInterface *agent)
    : AlgorithmInterface(std::move(componentName),
                         isInit,
                         priority,
                         offsetTime,
                         responseTime,
                         cycleTime,
                         stochastics,
                         parameters,
                         publisher,
                         callbacks,
                         agent),
      switchAssist(new Switch())
{
  LOGINFO("Constructing Algorithm_Switch");

  /** @addtogroup switch_group1
   * Initialize the output control signal to zero values. Set input parameters to default.
   */

  int noActivityNoCollision = -1;
  index.SetDefaultValue(noActivityNoCollision);

  ControlData defaultControl = {0.0, 0.0, 0.0, {0.0, 0.0, 0.0, 0.0}};
  driverControl.SetDefaultValue(defaultControl);
  prio1Control.SetDefaultValue(defaultControl);
  prio2Control.SetDefaultValue(defaultControl);
  prio3Control.SetDefaultValue(defaultControl);

  resultingControl.SetValue(defaultControl);

  LOGINFO("Constructing Algorithm_Switch successful");
}

Algorithm_Switch_Implementation::~Algorithm_Switch_Implementation()
{
  delete switchAssist;
  switchAssist = nullptr;
}

void Algorithm_Switch_Implementation::UpdateInput(int localLinkId,
                                                  const std::shared_ptr<SignalInterface const> &data,
                                                  [[maybe_unused]] int time)
{
  bool success = inputPorts.at(localLinkId)->SetSignalValue(data);
  if (success)
  {
    LOGDEBUG("Algorithm_Switch: Update input #" + std::to_string(localLinkId) + " successful");
  }
  else
  {
    LOGERROR("Algorithm_Switch: Update input #" + std::to_string(localLinkId) + " failed");
  }
}

void Algorithm_Switch_Implementation::UpdateOutput(int localLinkId,
                                                   std::shared_ptr<SignalInterface const> &data,
                                                   [[maybe_unused]] int time)
{
  bool success = outputPorts.at(localLinkId)->GetSignalValue(data);
  if (success)
  {
    LOGDEBUG("Algorithm_Switch: Update output #" + std::to_string(localLinkId) + " successful");
  }
  else
  {
    LOGERROR("Algorithm_Switch: Update output #" + std::to_string(localLinkId) + " failed");
  }
}

void Algorithm_Switch_Implementation::Trigger([[maybe_unused]] int time)
{
  ControlData result = switchAssist->Perform(index.GetValue(),
                                             driverControl.GetValue(),
                                             prio1Control.GetValue(),
                                             prio2Control.GetValue(),
                                             prio3Control.GetValue());

  resultingControl.SetValue(result);

  return;
}
