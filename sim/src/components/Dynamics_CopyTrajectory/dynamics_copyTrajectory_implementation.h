/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef DYNAMICS_COPYTRAJECTORY_IMPLEMENTATION_H
#define DYNAMICS_COPYTRAJECTORY_IMPLEMENTATION_H

#include <MantleAPI/Common/poly_line.h>
#include <memory>
#include <string>
#include <units.h>
#include <vector>

#include "include/modelInterface.h"

class AgentInterface;
class CallbackInterface;
class ParameterInterface;
class PublisherInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;

/** \addtogroup Components_PCM openPASS components pcm
 * @{
 * \addtogroup Dynamics_CopyTrajectory
 * \brief Dynamic component to follow a exactly a given trajectory.
 *
 * \details This module uses the given trajectory of the agent to set its position,
 * orientation and velocity at every timestep.
 *
 * @} */

/*!
 * \copydoc Dynamics_CopyTrajectory
 * \ingroup Dynamics_CopyTrajectory
 */
class Dynamics_CopyTrajectory_Implementation : public DynamicsInterface
{
public:
  /// Name of this component
  const std::string COMPONENTNAME = "Dynamics_CopyTrajectory";

  //! Constructor
  //!
  //! @param[in]     componentName  Name of the component
  //! @param[in]     isInit         Corresponds to "init" of "Component"
  //! @param[in]     priority       Corresponds to "priority" of "Component"
  //! @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
  //! @param[in]     responseTime   Corresponds to "responseTime" of "Component"
  //! @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
  //! @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
  //! @param[in]     world          Pointer to the world
  //! @param[in]     parameters     Pointer to the parameters of the module
  //! @param[in]     publisher      Pointer to the publisher instance
  //! @param[in]     callbacks      Pointer to the callbacks
  //! @param[in]     agent          Pointer to agent instance
  Dynamics_CopyTrajectory_Implementation(std::string componentName,
                                         bool isInit,
                                         int priority,
                                         int offsetTime,
                                         int responseTime,
                                         int cycleTime,
                                         StochasticsInterface *stochastics,
                                         WorldInterface *world,
                                         const ParameterInterface *parameters,
                                         PublisherInterface *const publisher,
                                         const CallbackInterface *callbacks,
                                         AgentInterface *agent);
  ~Dynamics_CopyTrajectory_Implementation() override = default;
  Dynamics_CopyTrajectory_Implementation(const Dynamics_CopyTrajectory_Implementation &) = delete;
  Dynamics_CopyTrajectory_Implementation(Dynamics_CopyTrajectory_Implementation &&) = delete;
  Dynamics_CopyTrajectory_Implementation &operator=(const Dynamics_CopyTrajectory_Implementation &) = delete;
  Dynamics_CopyTrajectory_Implementation &operator=(Dynamics_CopyTrajectory_Implementation &&) = delete;

  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;
  void Trigger(int time) override;

private:
  /** \addtogroup Dynamics_CopyTrajectory
   *  @{
   *      \name InputPorts
   *      All input ports with PortId
   *      @{
   */
  // InputPort<TrajectorySignal, openScenario::Trajectory> trajectory{0, &inputPorts}; //!< given trajectory to follow
  mantle_api::PolyLine trajectory;  //!< given trajectory to follow
  /**
   *      @}
   *  @}
   */

  void ReadWayPointData();

  /**
   *    \name Internal objects
   *    @{
   */
  //local computation objects
  units::time::second_t timeStep{0.0};  //!< Time step as double in s
  units::time::second_t timeVecNext{0.0};
  unsigned int indexVecNext = 0;
  std::vector<units::time::second_t> timeVec;                          //!< time vector of trajectory
  std::vector<units::length::meter_t> posX;                            //!< x coordinate vector of trajectory
  std::vector<units::length::meter_t> posY;                            //!< y coordinate vector of trajectory
  std::vector<units::velocity::meters_per_second_t> velX;              //!< velocity vector of trajectory
  std::vector<units::velocity::meters_per_second_t> velY;              //!< velocity vector of trajectory
  std::vector<units::angle::radian_t> angleYaw;                        //!< yaw angle vector of trajectory
  std::vector<units::angular_velocity::radians_per_second_t> rateYaw;  //!< yaw angle vector of trajectory
                                                                       /**
                                                                        *    @}
                                                                        */
};

#endif  // DYNAMICS_COPYTRAJECTORY_IMPLEMENTATION_H
