/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef ECU_H
#define ECU_H

/// @brief Class representing ECU
class Ecu
{
public:
  Ecu();

  /// @brief Resolve which assistance system to use, based on the logic of collision and system activations.
  /// @param coll
  /// @param prio1
  /// @param prio2
  /// @param prio3
  /// @return
  int Perform(bool coll, bool prio1, bool prio2, bool prio3);

private:
  bool anyPrevActivity;
  int index;
};

#endif  // ECU_H
