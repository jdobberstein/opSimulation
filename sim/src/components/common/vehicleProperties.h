/********************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <string>

namespace Properties::Vehicle
{
const std::string AIR_DRAG_COEFFICIENT("AirDragCoefficient");
const std::string AXLE_RATIO("AxleRatio");
const std::string DECELERATION_FROM_POWERTRAIN_DRAG("DecelerationFromPowertrainDrag");
const std::string FRICTION_COEFFICIENT("FrictionCoefficient");
const std::string FRONT_SURFACE("FrontSurface");
const std::string GEAR_RATIO("GearRatio");
const std::string NUMBER_OF_GEARS("NumberOfGears");
const std::string MAXIMUM_ENGINE_SPEED("MaximumEngineSpeed");
const std::string MAXIMUM_ENGINE_TORQUE("MaximumEngineTorque");
const std::string MINIMUM_ENGINE_SPEED("MinimumEngineSpeed");
const std::string STEERING_RATIO("SteeringRatio");
const std::string POSITION_COG_X("XPositionCOG");
const std::string POSITION_COG_Y("YPositionCOG");
const std::string POSITION_COG_Z("ZPositionCOG");
}  //namespace Properties::Vehicle
