/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2017-2019 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  agentUpdaterImpl.cpp
//! @brief This file contains the implementation of the header file
//-----------------------------------------------------------------------------

#include "agentUpdaterImpl.h"

#include <MantleAPI/Common/orientation.h>
#include <stdexcept>

#include "common/dynamicsSignal.h"
#include "include/publisherInterface.h"

class SignalInterface;

void AgentUpdaterImplementation::UpdateInput(int localLinkId,
                                             const std::shared_ptr<SignalInterface const> &data,
                                             [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    // from DynamicsPrioritizer
    const std::shared_ptr<DynamicsSignal const> signal = std::dynamic_pointer_cast<DynamicsSignal const>(data);
    if (!signal)
    {
      const std::string msg = COMPONENTNAME + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }

    dynamicsInformation = signal->dynamicsInformation;
    longitudinalController = signal->longitudinalController;
    lateralController = signal->lateralController;
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void AgentUpdaterImplementation::UpdateOutput([[maybe_unused]] int localLinkId,
                                              [[maybe_unused]] std::shared_ptr<SignalInterface const> &data,
                                              [[maybe_unused]] int time)
{
}

void AgentUpdaterImplementation::Trigger([[maybe_unused]] int time)
{
  AgentInterface *agent = GetAgent();

  Validate(dynamicsInformation.yaw, "yaw");
  agent->SetYaw(dynamicsInformation.yaw);
  Validate(dynamicsInformation.yawRate, "yawRate");
  agent->SetYawRate(dynamicsInformation.yawRate);
  Validate(dynamicsInformation.yawAcceleration, "yawAcceleration");
  agent->SetYawAcceleration(dynamicsInformation.yawAcceleration);

  Validate(dynamicsInformation.velocityX, "velocityX");
  Validate(dynamicsInformation.velocityY, "velocityY");
  agent->SetVelocityVector({dynamicsInformation.velocityX, dynamicsInformation.velocityY, 0.0_mps});

  if (!std::isnan(dynamicsInformation.acceleration.value()))
  {
    agent->SetAcceleration(dynamicsInformation.acceleration);
    if (std::isnan(dynamicsInformation.centripetalAcceleration.value()))
    {
      dynamicsInformation.centripetalAcceleration = 0.0_mps_sq;
    }
    agent->SetCentripetalAcceleration(dynamicsInformation.centripetalAcceleration);
  }
  if (!std::isnan(dynamicsInformation.accelerationX.value()))
  {
    Validate(dynamicsInformation.accelerationX, "accelerationX");
    Validate(dynamicsInformation.accelerationY, "accelerationY");
    agent->SetAccelerationVector({dynamicsInformation.accelerationX, dynamicsInformation.accelerationY, 0.0_mps_sq});
  }

  Validate(dynamicsInformation.positionX, "positionX");
  agent->SetPositionX(dynamicsInformation.positionX);
  Validate(dynamicsInformation.positionY, "positionY");
  agent->SetPositionY(dynamicsInformation.positionY);

  Validate(dynamicsInformation.roll, "roll");
  agent->SetRoll(dynamicsInformation.roll);
  Validate(dynamicsInformation.steeringWheelAngle, "steeringWheelAngle");
  agent->SetSteeringWheelAngle(dynamicsInformation.steeringWheelAngle);
  Validate(dynamicsInformation.travelDistance, "travelDistance");
  agent->SetDistanceTraveled(agent->GetDistanceTraveled() + dynamicsInformation.travelDistance);

  for (int idx = 0; idx < dynamicsInformation.wheelRotationRate.size(); idx++)
  {
    Validate(dynamicsInformation.wheelRotationRate[idx], std::string("wheelRotation") + std::to_string(idx));
    agent->SetWheelRotationRate(
        static_cast<int>(std::floor(idx / 2.0)), idx % 2, dynamicsInformation.wheelRotationRate[idx]);
  }

  for (int idx = 0; idx < std::max({dynamicsInformation.wheelRoll.size(),
                                    dynamicsInformation.wheelPitch.size(),
                                    dynamicsInformation.wheelYaw.size()});
       idx++)
  {
    mantle_api::Vec3<units::angle::radian_t> wheelOrientation{0.0_rad, 0.0_rad, 0.0_rad};

    if (idx < dynamicsInformation.wheelRoll.size())
    {
      Validate(dynamicsInformation.wheelRoll[idx], std::string("wheelRoll") + std::to_string(idx));
      wheelOrientation.x = dynamicsInformation.wheelRoll[idx];
    }

    if (idx < dynamicsInformation.wheelPitch.size())
    {
      Validate(dynamicsInformation.wheelPitch[idx], std::string("wheelPitch") + std::to_string(idx));
      wheelOrientation.y = dynamicsInformation.wheelPitch[idx];
    }

    if (idx < dynamicsInformation.wheelYaw.size())
    {
      Validate(dynamicsInformation.wheelYaw[idx], std::string("wheelYaw") + std::to_string(idx));
      wheelOrientation.z = dynamicsInformation.wheelYaw[idx];
    }

    agent->SetWheelOrientation(static_cast<int>(std::floor(idx / 2.0)), idx % 2, wheelOrientation);
  }

  publisher->Publish("LongitudinalController", longitudinalController);
  publisher->Publish("LateralController", lateralController);
}
