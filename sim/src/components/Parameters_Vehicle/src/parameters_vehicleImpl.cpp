/********************************************************************************
 * Copyright (c) 2016-2017 ITK Engineering GmbH
 *               2019-2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  parameters_vehicleImpl.cpp */
//-----------------------------------------------------------------------------

#include "parameters_vehicleImpl.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <memory>
#include <new>
#include <stdexcept>
#include <utility>

#include "common/parametersVehicleSignal.h"
#include "include/agentInterface.h"
#include "include/callbackInterface.h"

class ParameterInterface;
class PublisherInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;

ParametersVehicleImplementation::ParametersVehicleImplementation(std::string componentName,
                                                                 bool isInit,
                                                                 int priority,
                                                                 int offsetTime,
                                                                 int responseTime,
                                                                 int cycleTime,
                                                                 StochasticsInterface* stochastics,
                                                                 WorldInterface* world,
                                                                 const ParameterInterface* parameters,
                                                                 PublisherInterface* const publisher,
                                                                 const CallbackInterface* callbacks,
                                                                 AgentInterface* agent)
    : SensorInterface(std::move(componentName),
                      isInit,
                      priority,
                      offsetTime,
                      responseTime,
                      cycleTime,
                      stochastics,
                      world,
                      parameters,
                      publisher,
                      callbacks,
                      agent)
{
  if (agent->GetVehicleModelParameters()->type != mantle_api::EntityType::kVehicle)
  {
    throw std::runtime_error("Component " + GetComponentName()
                             + " expects an entity of type Vehicle and VehicleProperties.");
  }
}

void ParametersVehicleImplementation::UpdateInput([[maybe_unused]] int localLinkId,
                                                  [[maybe_unused]] const std::shared_ptr<SignalInterface const>& data,
                                                  [[maybe_unused]] int time)
{
}

void ParametersVehicleImplementation::UpdateOutput(int localLinkId,
                                                   std::shared_ptr<SignalInterface const>& data,
                                                   [[maybe_unused]] int time)
{
  try
  {
    switch (localLinkId)
    {
      case 1:
        // vehicle parameters
        data = std::make_shared<ParametersVehicleSignal const>(
            *(std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters())));
        break;

      default:
        const std::string msg = COMPONENTNAME + " invalid link";
        LOG(CbkLogLevel::Debug, msg);
        throw std::runtime_error(msg);
    }
  }
  catch (const std::bad_alloc&)
  {
    const std::string msg = COMPONENTNAME + " could not instantiate signal";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void ParametersVehicleImplementation::Trigger([[maybe_unused]] int time) {}
