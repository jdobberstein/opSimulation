/********************************************************************************
 * Copyright (c) 2016-2017 ITK Engineering GmbH
 *               2019-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <memory>
#include <string>

#include "include/modelInterface.h"

class AgentInterface;
class CallbackInterface;
class ParameterInterface;
class PublisherInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;

/** \addtogroup Parameters_Vehicle
 * @{
 * \brief defines and distributes parameters (constants and stochasitc variables) for all components of an agent
 *
 * This component defines parameters for all other components of an agent. It includes constants and stochastic
 * variables, which are divided into driver and vehicle parameters in the components output data structs.
 *
 * \section Parameters_Vehicle_Inputs Inputs
 * none
 *
 * \section Parameters_Vehicle_InitInputs Init Inputs
 * none
 *
 * \section ParametersAgent_Outputs Outputs
 * **Output variables:**
 *
 * Information in output data struct out_vehicleParameters
 * name | meaning
 * ---|---
 * airDragCoefficient                                  | The air drag coefficient of the vehicle
 * axleRatio                                           | The ratio of the axle gear
 * decelerationFromPowertrainDrag                      | The deceleration caused by the overall powertrain drag torque
 * in m/s2 distanceReferencePointToFrontAxle                   | The distance between the vehicle coordinate system's
 * reference point (rear axle) and the front axle in m distanceReferencePointToLeadingEdge                 | The
 * distance between the vehicle coordinate system's reference point (rear axle) and the front bumper in m frictionCoeff
 * | The friction coefficient between road and the vehicles tires frontSurface                                        |
 * The projected front surface of the vehicle in m2 gearRatios                                          | The ratios of
 * all gears in the gearbox (no reverse gear) height                                              | The maximum height
 * of the vehicle in m heightCOG                                           | The height of the center of gravity above
 * ground in m length                                              | The maximum length of the vehicle in m maxCurvature
 * | The maximum curavture the vehicle is able to drive in 1/m maximumEngineSpeed                                  | The
 * maximum engine speed in 1/min maximumEngineTorque                                 | The maximum torque of the engine
 * in Nm maximumSteeringWheelAngleAmplitude                  | The maximum amplitude of the steering wheel angle in
 * radian maxVelocity                                         | The maximum velocity of the vehicle in m/s
 * minimumEngineSpeed                                  | The idle speed of the engine in 1/min
 * minimumEngineTorque                                 | The drag torque of the engine in Nm
 * momentInertiaPitch                                  | The moment of inertia along the vehicle's lateral axis in kgm2
 * momentInertiaRoll                                   | The moment of inertia along the vehicle's longitudinal axis in
 * kgm2 momentInertiaYaw                                    | The moment of inertia along the vehicle's vertical axis in
 * kgm2 numberOfGears                                       | The number of gears in the gearbox (no reverse gear)
 * staticWheelRadius                                   | The static wheel radius in m
 * steeringRatio                                       | The ratio of the steering gear
 * trackwidth                                          | The trackwidth of the vehicle in m (for all axles)
 * vehicleType                                         | The type of the driver's vehicle
 * weight                                              | The overall mass of the vehicle in kg
 * wheelbase                                           | The wheelbase of the vehicle in m
 * width                                               | The maximum width of the vehicle in m
 *
 * **Output channel IDs:**
 * Output Id | signal class | contained variables
 * ------------|--------------|-------------
 *  1 | VehicleParameterSignal                 | out_vehicleParameters
 * @} */

/*!
 * \brief defines constants or creates stochastic values that are used in more than one module
 *
 * \ingroup Parameters_Vehicle
 */
class ParametersVehicleImplementation : public SensorInterface
{
public:
  /// Name of this component
  const std::string COMPONENTNAME = "ParametersVehicle";

  /**
   * @brief Construct a new Parameters Vehicle Implementation object
   *
   * @param[in]     componentName  Name of the component
   * @param[in]     isInit         Corresponds to "init" of "Component"
   * @param[in]     priority       Corresponds to "priority" of "Component"
   * @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
   * @param[in]     responseTime   Corresponds to "responseTime" of "Component"
   * @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
   * @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
   * @param[in]     world          Pointer to the world interface
   * @param[in]     parameters     Pointer to the parameters of the module
   * @param[in]     publisher      Pointer to the publisher instance
   * @param[in]     callbacks      Pointer to the callbacks
   * @param[in]     agent          Pointer to agent instance
   */
  ParametersVehicleImplementation(std::string componentName,
                                  bool isInit,
                                  int priority,
                                  int offsetTime,
                                  int responseTime,
                                  int cycleTime,
                                  StochasticsInterface* stochastics,
                                  WorldInterface* world,
                                  const ParameterInterface* parameters,
                                  PublisherInterface* const publisher,
                                  const CallbackInterface* callbacks,
                                  AgentInterface* agent);
  ParametersVehicleImplementation(const ParametersVehicleImplementation&) = delete;
  ParametersVehicleImplementation(ParametersVehicleImplementation&&) = delete;
  ParametersVehicleImplementation& operator=(const ParametersVehicleImplementation&) = delete;
  ParametersVehicleImplementation& operator=(ParametersVehicleImplementation&&) = delete;

  virtual ~ParametersVehicleImplementation() = default;

  /**
   * \brief Update Inputs
   *
   * Function is called by framework when another component delivers a signal over
   * a channel to this component (scheduler calls update taks of other component).
   *
   * Refer to module description for input channels and input ids.
   *
   * @param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
   * @param[in]     data           Referenced signal (copied by sending component)
   * @param[in]     time           Current scheduling time
   */
  virtual void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const>& data, int time) override;

  /**
   * \brief Update outputs.
   *
   * Function is called by framework when this component has to deliver a signal over
   * a channel to another component (scheduler calls update task of this component).
   *
   * Refer to module description for output channels and output ids.
   *
   * @param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
   * @param[out]    data           Referenced signal (copied by this component)
   * @param[in]     time           Current scheduling time
   */
  virtual void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const>& data, int time) override;

  /**
   * @brief Process data within component.
   *
   * Function is called by framework when the scheduler calls the trigger task
   * of this component
   *
   * @param time  Current scheduling time
   */
  void Trigger(int time) override;
};