/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2019-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "FmuCalculations.h"

#include <algorithm>

#include "common/worldDefinitions.h"

double FmuCalculations::CalculateSpeedLimit(double range, AgentInterface* agent)
{
  double speedLimit = 999.0;
  auto trafficSigns = agent->GetEgoAgent().GetTrafficSignsInRange(units::length::meter_t(range));
  auto trafficSignsBehind
      = agent->GetEgoAgent().GetTrafficSignsInRange(units::length::meter_t(std::numeric_limits<double>::lowest()));
  trafficSigns.insert(trafficSigns.end(), trafficSignsBehind.begin(), trafficSignsBehind.end());
  std::sort(trafficSigns.begin(),
            trafficSigns.end(),
            [](const CommonTrafficSign::Entity& lhs, const CommonTrafficSign::Entity& rhs)
            { return lhs.relativeDistance > rhs.relativeDistance; });
  for (const auto& sign : trafficSigns)
  {
    if (sign.type == CommonTrafficSign::Type::EndOfMaximumSpeedLimit
        || sign.type == CommonTrafficSign::Type::EndOffAllSpeedLimitsAndOvertakingRestrictions
        || sign.type == CommonTrafficSign::Type::SpeedLimitZoneEnd
        || sign.type == CommonTrafficSign::Type::TrafficCalmedDistrictEnd)
    {
      break;
    }
    if (sign.type == CommonTrafficSign::Type::MaximumSpeedLimit
        || sign.type == CommonTrafficSign::Type::SpeedLimitZoneBegin)
    {
      speedLimit = sign.value;
      break;
    }
    if (sign.type == CommonTrafficSign::Type::TrafficCalmedDistrictBegin)
    {
      speedLimit = 7.0 / 3.6;
      break;
    }
  }
  return speedLimit;
}

int FmuCalculations::CalculateLaneCount(Side side, AgentInterface* agent)
{
  if (!agent->GetEgoAgent().HasValidRoute())
  {
    return 0;
  }
  const auto relativeLanes = agent->GetEgoAgent().GetRelativeLanes(0_m).at(0);
  return static_cast<int>(std::count_if(relativeLanes.lanes.cbegin(),
                                        relativeLanes.lanes.cend(),
                                        [&](const auto& lane)
                                        {
                                          return (lane.type == LaneType::Driving || lane.type == LaneType::Exit
                                                  || lane.type == LaneType::Entry || lane.type == LaneType::OnRamp
                                                  || lane.type == LaneType::OffRamp)
                                              && ((side == Side::Left) ? (lane.relativeId > 0) : (lane.relativeId < 0))
                                              && lane.inDrivingDirection;
                                        }));
}

constexpr units::area::square_meter_t SquaredDistance(units::length::meter_t x, units::length::meter_t y)
{
  return x * x + y * y;
}

std::vector<SensorFusionObjectInfo> FmuCalculations::CalculateSensorFusionInfo(osi3::SensorData& sensorDataIn,
                                                                               WorldInterface* world,
                                                                               AgentInterface* agent)
{
  auto* worldData = static_cast<OWL::WorldData*>(world->GetWorldData());

  std::vector<std::pair<WorldObjectInterface*, int>> objects;
  std::transform(sensorDataIn.moving_object().cbegin(),
                 sensorDataIn.moving_object().cend(),
                 std::back_inserter(objects),
                 [&](const osi3::DetectedMovingObject& object)
                 {
                   std::uint64_t osi_id = object.header().ground_truth_id(0).value();
                   return std::pair<WorldObjectInterface*, int>(world->GetAgent(static_cast<int>(osi_id)),
                                                                object.header().sensor_id_size());
                 });
  std::transform(sensorDataIn.stationary_object().cbegin(),
                 sensorDataIn.stationary_object().cend(),
                 std::back_inserter(objects),
                 [&](const osi3::DetectedStationaryObject& object)
                 {
                   std::uint64_t osi_id = object.header().ground_truth_id(0).value();
                   return std::pair<WorldObjectInterface*, int>(
                       worldData->GetStationaryObject(osi_id).GetLink<WorldObjectInterface>(),
                       object.header().sensor_id_size());
                 });

  std::sort(objects.begin(),
            objects.end(),
            [agent](const auto& lhs, const auto& rhs)
            {
              auto lhsDistance = SquaredDistance(lhs.first->GetPositionX() - agent->GetPositionX(),
                                                 lhs.first->GetPositionY() - agent->GetPositionY());
              auto rhsDistance = SquaredDistance(rhs.first->GetPositionX() - agent->GetPositionX(),
                                                 rhs.first->GetPositionY() - agent->GetPositionY());
              return lhsDistance < rhsDistance;
            });

  std::vector<SensorFusionObjectInfo> sensorFusionInfo;

  std::transform(
      objects.cbegin(),
      objects.cend(),
      std::back_inserter(sensorFusionInfo),
      [&](std::pair<const WorldObjectInterface*, int> entry)
      {
        const auto* object = entry.first;
        const auto numberOfSensors = entry.second;
        auto obstruction = agent->GetEgoAgent().GetObstruction(
            object,
            {ObjectPointPredefined::FrontCenter, ObjectPointRelative::Leftmost, ObjectPointRelative::Rightmost});
        auto [netXDistance, netYDistance]
            = CommonHelper::GetCartesianNetDistance(agent->GetBoundingBox2D(), object->GetBoundingBox2D());
        const auto& roadPosition = object->GetRoadPosition(ObjectPointPredefined::FrontCenter);

        return SensorFusionObjectInfo{
            object->GetId(),
            numberOfSensors,
            obstruction.valid ? obstruction.lateralDistances.at(ObjectPointPredefined::FrontCenter)
                              : units::length::meter_t{NAN},
            obstruction.valid ? obstruction.lateralDistances.at(ObjectPointRelative::Leftmost)
                              : units::length::meter_t{NAN},
            obstruction.valid ? obstruction.lateralDistances.at(ObjectPointRelative::Rightmost)
                              : units::length::meter_t{NAN},
            agent->GetEgoAgent()
                .GetDistanceToObject(object, ObjectPointPredefined::Reference, ObjectPointPredefined::Reference)
                .value_or(units::length::meter_t{NAN}),
            object->GetPositionX() - agent->GetPositionX(),
            object->GetPositionY() - agent->GetPositionY(),
            agent->GetEgoAgent().GetNetDistance(object).value_or(units::length::meter_t{NAN}),
            units::length::meter_t(netXDistance),
            units::length::meter_t(netYDistance),
            roadPosition.empty() ? 0 : static_cast<int>(roadPosition.cbegin()->second.laneId),
            object->GetVelocity().Length(),
            object->GetVelocity().x,
            object->GetVelocity().y,
            object->GetYaw()};
      });

  return sensorFusionInfo;
}