/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <algorithm>

template <size_t FMI>
ChannelDefinitionParser<FMI>::ChannelDefinitionParser(const FmuVariables &fmuVariables, fmi_version_enu_t fmiVersion, const CallbackInterface* callbacks) :
    fmiVersion(fmiVersion),
    unmappedFmuVariables(fmuVariables),
    callbacks(callbacks)
{
    fmuOutputs.emplace<FMI>();
    fmuRealInputs.emplace<FMI>();
    fmuIntegerInputs.emplace<FMI>();
    fmuBooleanInputs.emplace<FMI>();
    fmuStringInputs.emplace<FMI>();
    fmuIntegerParameters.emplace<FMI>();
    fmuDoubleParameters.emplace<FMI>();
    fmuBoolParameters.emplace<FMI>();
    fmuStringParameters.emplace<FMI>();
}

template <size_t FMI>
void ChannelDefinitionParser<FMI>::AddOutputChannel(const std::string& outputType, const std::string& variableName, std::unordered_map<std::string, std::unordered_map<int, std::string>> fmuTypeDefinitions)
{
    auto unmappedVariableItem = std::get<FMI>(unmappedFmuVariables).find(variableName);
    auto fmuOutput = stringToSignalValueMap.find(outputType);

    if (unmappedVariableItem == std::get<FMI>(unmappedFmuVariables).end())
    {
        LOGERRORANDTHROW("Unable to add output channel: Variable <" + variableName + "> not defined in FMU");
    }

    if (fmuOutput == stringToSignalValueMap.end())
    {
        LOGERRORANDTHROW("Unable to add output channel: Output type <" + outputType + "> not defined in FMU");
    }

    const auto& [valueReference, variableTypeInFmu, casuality, variablility] = unmappedVariableItem->second;
    const auto& [fmuOutputType, variableTypeInWrapper] = fmuOutput->second;

    if (variableTypeInFmu != variableTypeInWrapper)
    {
        LOGERRORANDTHROW("Unable to add output channel: Variable type in FMU not the same as in Wrapper");
    }

    std::get<FMI>(fmuOutputs)[fmuOutputType] = valueReference;

    std::get<FMI>(unmappedFmuVariables).erase(unmappedVariableItem);

    if (variableTypeInFmu == VariableType::Enum)
    {
        if (fmuTypeDefinitions.find(outputType) != fmuTypeDefinitions.cend())
        {
            for (auto enumItem : fmuTypeDefinitions.at(outputType))
            {
                if (fmuOutputType == SignalValue::ComponentState)
                {
                    if (stringToComponentState.find(enumItem.second) == stringToComponentState.cend())
                    {
                        LOGERRORANDTHROW("Unable to add output channel: Enumeration value <" + enumItem.second + "> not a valid value for ComponentState");
                    }
                    fmuEnumerations.componentStates.insert(std::make_pair(enumItem.first, stringToComponentState.at(enumItem.second)));
                }  
                else if (fmuOutputType == SignalValue::CompCtrlSignal_MovementDomain)
                {
                    if (stringToMovementDomain.find(enumItem.second) == stringToMovementDomain.cend())
                    {
                        LOGERRORANDTHROW("Unable to add output channel: Enumeration value <" + enumItem.second + "> not a valid value for CompCtrlSignal_MovementDomain");
                    }
                    fmuEnumerations.movementDomains.insert(std::make_pair(enumItem.first, stringToMovementDomain.at(enumItem.second)));
                } 
                else if (fmuOutputType == SignalValue::CompCtrlSignal_WarningLevel)
                {
                    if (stringToComponentWarningLevel.find(enumItem.second) == stringToComponentWarningLevel.cend())
                    {
                        LOGERRORANDTHROW("Unable to add output channel: Enumeration value <" + enumItem.second + "> not a valid value for CompCtrlSignal_WarningLevel");
                    }
                    fmuEnumerations.warningLevels.insert(std::make_pair(enumItem.first, stringToComponentWarningLevel.at(enumItem.second)));
                }
                else if (fmuOutputType == SignalValue::CompCtrlSignal_WarningType)
                {
                    if (stringToComponentWarningType.find(enumItem.second) == stringToComponentWarningType.cend())
                    {
                        LOGERRORANDTHROW("Unable to add output channel: Enumeration value <" + enumItem.second + "> not a valid value for CompCtrlSignal_WarningType");
                    }
                    fmuEnumerations.warningTypes.insert(std::make_pair(enumItem.first, stringToComponentWarningType.at(enumItem.second)));
                }
                else if (fmuOutputType == SignalValue::CompCtrlSignal_WarningIntensity)
                {
                    if (stringToComponentWarningIntensity.find(enumItem.second) == stringToComponentWarningIntensity.cend())
                    {
                        LOGERRORANDTHROW("Unable to add output channel: Enumeration value <" + enumItem.second + "> not a valid value for CompCtrlSignal_WarningIntensity");
                    }
                    fmuEnumerations.warningIntensities.insert(std::make_pair(enumItem.first, stringToComponentWarningIntensity.at(enumItem.second)));
                }
                else if (fmuOutputType == SignalValue::CompCtrlSignal_WarningDirection)
                {
                    if (stringToWarningDirection.find(enumItem.second) == stringToWarningDirection.cend())
                    {
                        LOGERRORANDTHROW("Unable to add output channel: Enumeration value <" + enumItem.second + "> not a valid value for CompCtrlSignal_WarningDirection");
                    }
                    fmuEnumerations.warningDirections.insert(std::make_pair(enumItem.first, stringToWarningDirection.at(enumItem.second)));
                }
            }
        }
        else
        {
            LOGERRORANDTHROW("Unable to add output channel:: Enumeration is not (correctly) defined in FMU for output type <" + outputType + ">");
        }
    }
}

template <size_t FMI>
void ChannelDefinitionParser<FMI>::AddInputChannel(const std::string& inputType, const std::string& variableName)
{

    const auto pos = inputType.find('_');
    const auto variableType = inputType.substr(0, pos);
    std::variant<double, size_t> additionalParameter;
    if (variableType == "SpeedLimit" || variableType == "RoadCurvature")
    {
        const auto rangeString = inputType.substr(pos + 1);
        additionalParameter = std::stod(rangeString);
    }
    else if (variableType.substr(0,12)  == "SensorFusion")
    {
        const auto indexString = inputType.substr(pos + 1);
        additionalParameter = static_cast<size_t>(std::stoul(indexString));
    }

    auto unmappedVariableItem = std::get<FMI>(unmappedFmuVariables).find(variableName);
    auto fmuInput = stringToFmuTypeMap.find(variableType);

    if (unmappedVariableItem == std::get<FMI>(unmappedFmuVariables).end())
    {
        LOGERRORANDTHROW("Unable to add input channel: Variable <" + variableName + "> not defined in FMU");
    }

    if (fmuInput == stringToFmuTypeMap.end())
    {
        LOGERRORANDTHROW("Unable to add input channel: Input type <" + inputType + "> not defined in FMU");
    }

    const auto& [valueReference, variableTypeInFmu, casuality, variablility] = unmappedVariableItem->second;
    const auto& [fmuInputType, variableTypeInWrapper] = fmuInput->second;

    if (variableTypeInFmu != variableTypeInWrapper)
    {
        LOGERRORANDTHROW("Unable to add input channel: Variable type in FMU not the same as in Wrapper");
    }

    if (variableTypeInFmu == VariableType::Double)
    {
        std::get<FMI>(fmuRealInputs).emplace_back(fmuInputType, additionalParameter, valueReference);
    }
    else if (variableTypeInFmu == VariableType::Int)
    {
        std::get<FMI>(fmuIntegerInputs).emplace_back(fmuInputType, additionalParameter, valueReference);
    }
    else if (variableTypeInFmu == VariableType::Bool)
    {
        std::get<FMI>(fmuBooleanInputs).emplace_back(fmuInputType, additionalParameter, valueReference);
    }
    else if (variableTypeInFmu == VariableType::String)
    {
        std::get<FMI>(fmuStringInputs).emplace_back(fmuInputType, additionalParameter, valueReference);
    }

    std::get<FMI>(unmappedFmuVariables).erase(unmappedVariableItem);
}

template <size_t FMI>
void ChannelDefinitionParser<FMI>::ParseOutputSignalTypes()
{
    std::vector<SignalType> nonExistingOutputSignals;
    for (const auto& [signalType, values] : valuesOfSignalType)
    {
        for (const auto signalValue : values)
        {
            auto valueFound = std::find_if(std::get<FMI>(fmuOutputs).cbegin(), std::get<FMI>(fmuOutputs).cend(),
                                           [&](const auto& pair) {return pair.first == signalValue;}) != std::get<FMI>(fmuOutputs).cend();
            if (valueFound)
            {
                outputSignals.emplace(signalType);
                if (std::find(nonExistingOutputSignals.cbegin(), nonExistingOutputSignals.cend(), signalType) != nonExistingOutputSignals.cend())
                {
                    LOGERRORANDTHROW("Output signal for FMU incomplete");
                }
            }
            else
            {
                nonExistingOutputSignals.emplace_back(signalType);
                if (std::find(outputSignals.cbegin(), outputSignals.cend(), signalType) != outputSignals.cend())
                {
                    LOGERRORANDTHROW("Output signal for FMU incomplete");
                }
            }
        }
    }
}

template <size_t FMI>
FmuOutputs ChannelDefinitionParser<FMI>::GetFmuOutputs()
{
    return fmuOutputs;
}

template <size_t FMI>
std::set<SignalType> ChannelDefinitionParser<FMI>::GetOutputSignals()
{
    return outputSignals;
}

template <size_t FMI>
FmuInputs ChannelDefinitionParser<FMI>::GetFmuRealInputs()
{
    return fmuRealInputs;
}

template <size_t FMI>
FmuInputs ChannelDefinitionParser<FMI>::GetFmuIntegerInputs()
{
    return fmuIntegerInputs;
}

template <size_t FMI>
FmuInputs ChannelDefinitionParser<FMI>::GetFmuBooleanInputs()
{
    return fmuBooleanInputs;
}

template <size_t FMI>
FmuInputs ChannelDefinitionParser<FMI>::GetFmuStringInputs()
{
    return fmuStringInputs;
}

template <size_t FMI>
FmuParameters<std::string> ChannelDefinitionParser<FMI>::GetFmuStringParameters()
{
    return fmuStringParameters;
}

template <size_t FMI>
FmuParameters<int> ChannelDefinitionParser<FMI>::GetFmuIntegerParameters()
{
    return fmuIntegerParameters;
}

template <size_t FMI>
FmuParameters<double> ChannelDefinitionParser<FMI>::GetFmuDoubleParameters()
{
    return fmuDoubleParameters;
}

template <size_t FMI>
FmuParameters<bool> ChannelDefinitionParser<FMI>::GetFmuBoolParameters()
{
    return fmuBoolParameters;
}

template <size_t FMI>
FmuEnumerations ChannelDefinitionParser<FMI>::GetFmuEnumerations()
{
    return fmuEnumerations;
}

template <size_t FMI>
void ChannelDefinitionParser<FMI>::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message)
{
    if (callbacks)
    {
        callbacks->Log(logLevel, file, line, message);
    }
}