/*******************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once
#include <filesystem>
#include <google/protobuf/util/json_util.h>
#include <map>
#include <optional>
#include <string>
#include <string_view>

#include "common/runtimeInformation.h"

namespace FmuFileHelper
{

struct TraceEntry  ///< Parameters of trace entry
{
  std::string message;  ///< Message of the trace
  int time;             ///< Time of the trace entry
  std::string osiType;  ///< Type of OSI
};

std::string CreateAgentIdString(int agentId);
std::filesystem::path CreateOrOpenOutputFolder(const std::filesystem::path& outputDir,
                                               const std::string& componentName,
                                               std::optional<const std::string> appendedFolder);

std::string GenerateTraceFileName(const std::string_view outputType,
                                  const std::pair<const std::string, FmuFileHelper::TraceEntry>& fileToOutputTrace);

void WriteBinaryTrace(const std::string& message,
                      const std::string& fileName,
                      const std::string& componentName,
                      int time,
                      std::string osiType,
                      std::map<std::string, FmuFileHelper::TraceEntry>& targetOutputTracesMap);
void WriteTracesToFile(const std::filesystem::path& outputDir,
                       const std::map<std::string, FmuFileHelper::TraceEntry>& fileToOutputTracesMap);
void WriteJson(const google::protobuf::Message& message,
               const std::string& fileName,
               const std::filesystem::path& outputDir);

/// @brief Retrieve a unique temporary path
/// @param agentIdString agentId wrapped into the path
/// @return Path following the pattern <system_temp>/fmu_XXXXXX_<agentIdString>_<epoch_timestamp>
std::filesystem::path GetTemporaryPath(const std::string& agentIdString);
}  // namespace FmuFileHelper
