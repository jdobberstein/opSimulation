
#include "FmuCommunication.h"

#include <utility>

/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(const std::vector<fmi1_value_reference_t>& valueReferences,
                                              const std::vector<fmi_string_t>& dataVecIn)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI1, fmi_string_t>(dataVecIn);
  return fmi1_import_set_string(cdata.fmu1,
                                valueReferences.data(),  // array of value reference
                                valueReferences.size(),  // number of elements
                                dataIn.data());          // array of values
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(const std::vector<fmi2_value_reference_t>& valueReferences,
                                              const std::vector<fmi_string_t>& dataVecIn)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI2, fmi_string_t>(dataVecIn);
  return fmi2_import_set_string(cdata.fmu2,
                                valueReferences.data(),  // array of value reference
                                valueReferences.size(),  // number of elements
                                dataIn.data());          // array of values
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(const std::vector<fmi1_value_reference_t>& valueReferences,
                                              const std::vector<fmi_real_t>& dataVecIn)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI1, fmi_real_t>(dataVecIn);
  return fmi1_import_set_real(cdata.fmu1,
                              valueReferences.data(),  // array of value reference
                              valueReferences.size(),  // number of elements
                              dataIn.data());          // array of values
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(const std::vector<fmi2_value_reference_t>& valueReferences,
                                              const std::vector<fmi_real_t>& dataVecIn)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI2, fmi_real_t>(dataVecIn);
  return fmi2_import_set_real(cdata.fmu2,
                              valueReferences.data(),  // array of value reference
                              valueReferences.size(),  // number of elements
                              dataIn.data());          // array of values
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(const std::vector<fmi1_value_reference_t>& valueReferences,
                                              const std::vector<fmi_integer_t>& dataVecIn)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI1, fmi_integer_t>(dataVecIn);
  return fmi1_import_set_integer(cdata.fmu1,
                                 valueReferences.data(),  // array of value reference
                                 valueReferences.size(),  // number of elements
                                 dataIn.data());          // array of values
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(const std::vector<fmi2_value_reference_t>& valueReferences,
                                              const std::vector<fmi_integer_t>& dataVecIn)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI2, fmi_integer_t>(dataVecIn);
  return fmi2_import_set_integer(cdata.fmu2,
                                 valueReferences.data(),  // array of value reference
                                 valueReferences.size(),  // number of elements
                                 dataIn.data());          // array of values
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(const std::vector<fmi1_value_reference_t>& valueReferences,
                                              const std::vector<fmi_boolean_t>& dataVecIn)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI1, fmi_boolean_t>(dataVecIn);
  return fmi1_import_set_boolean(cdata.fmu1,
                                 valueReferences.data(),  // array of value reference
                                 valueReferences.size(),  // number of elements
                                 dataIn.data());          // array of values
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(const std::vector<fmi2_value_reference_t>& valueReferences,
                                              const std::vector<fmi_boolean_t>& dataVecIn)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI2, fmi_boolean_t>(dataVecIn);
  return fmi2_import_set_boolean(cdata.fmu2,
                                 valueReferences.data(),  // array of value reference
                                 valueReferences.size(),  // number of elements
                                 dataIn.data());          // array of values
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(const std::vector<fmi1_value_reference_t>& valueReferences,
                                              std::vector<fmi_string_t>& dataVecOut)
{
  dataVecOut.resize(valueReferences.size());
  std::vector<fmi1_string_t> dataOut(valueReferences.size());

  auto status = fmi1_import_get_string(cdata.fmu1,
                                       valueReferences.data(),  // array of value reference
                                       valueReferences.size(),  // number of elements
                                       dataOut.data());         // array of values

  for (int i = 0; i < valueReferences.size(); i++)
  {
    dataVecOut[i].emplace<FMI1>(dataOut[i]);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  }

  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(const std::vector<fmi2_value_reference_t>& valueReferences,
                                              std::vector<fmi_string_t>& dataVecOut)
{
  dataVecOut.resize(valueReferences.size());
  std::vector<fmi2_string_t> dataOut(valueReferences.size());

  auto status = fmi2_import_get_string(cdata.fmu2,
                                       valueReferences.data(),  // array of value reference
                                       valueReferences.size(),  // number of elements
                                       dataOut.data());         // array of values

  for (int i = 0; i < valueReferences.size(); i++)
  {
    dataVecOut[i].emplace<FMI2>(dataOut[i]);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  }

  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(const std::vector<fmi1_value_reference_t>& valueReferences,
                                              std::vector<fmi_real_t>& dataVecOut)
{
  dataVecOut.resize(valueReferences.size());
  std::vector<fmi1_real_t> dataOut(valueReferences.size());

  auto status = fmi1_import_get_real(cdata.fmu1,
                                     valueReferences.data(),  // array of value reference
                                     valueReferences.size(),  // number of elements
                                     dataOut.data());         // array of values

  for (int i = 0; i < valueReferences.size(); i++)
  {
    dataVecOut[i].emplace<FMI1>(dataOut[i]);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  }

  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(const std::vector<fmi2_value_reference_t>& valueReferences,
                                              std::vector<fmi_real_t>& dataVecOut)
{
  dataVecOut.resize(valueReferences.size());
  std::vector<fmi2_real_t> dataOut(valueReferences.size());

  auto status = fmi2_import_get_real(cdata.fmu2,
                                     valueReferences.data(),  // array of value reference
                                     valueReferences.size(),  // number of elements
                                     dataOut.data());         // array of values

  for (int i = 0; i < valueReferences.size(); i++)
  {
    dataVecOut[i].emplace<FMI2>(dataOut[i]);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  }

  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(const std::vector<fmi1_value_reference_t>& valueReferences,
                                              std::vector<fmi_integer_t>& dataVecOut)
{
  dataVecOut.resize(valueReferences.size());
  std::vector<fmi1_integer_t> dataOut(valueReferences.size());

  auto status = fmi1_import_get_integer(cdata.fmu1,
                                        valueReferences.data(),  // array of value reference
                                        valueReferences.size(),  // number of elements
                                        dataOut.data());         // array of values

  for (int i = 0; i < valueReferences.size(); i++)
  {
    dataVecOut[i].emplace<FMI1>(dataOut[i]);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  }

  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(const std::vector<fmi2_value_reference_t>& valueReferences,
                                              std::vector<fmi_integer_t>& dataVecOut)
{
  dataVecOut.resize(valueReferences.size());
  std::vector<fmi2_integer_t> dataOut(valueReferences.size());

  auto status = fmi2_import_get_integer(cdata.fmu2,
                                        valueReferences.data(),  // array of value reference
                                        valueReferences.size(),  // number of elements
                                        dataOut.data());         // array of values

  for (int i = 0; i < valueReferences.size(); i++)
  {
    dataVecOut[i].emplace<FMI2>(dataOut[i]);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  }

  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(const std::vector<fmi1_value_reference_t>& valueReferences,
                                              std::vector<fmi_boolean_t>& dataVecOut)
{
  dataVecOut.resize(valueReferences.size());
  std::vector<fmi1_boolean_t> dataOut(valueReferences.size());

  auto status = fmi1_import_get_boolean(cdata.fmu1,
                                        valueReferences.data(),  // array of value reference
                                        valueReferences.size(),  // number of elements
                                        dataOut.data());         // array of values

  for (int i = 0; i < valueReferences.size(); i++)
  {
    dataVecOut[i].emplace<FMI1>(dataOut[i]);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  }

  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(const std::vector<fmi2_value_reference_t>& valueReferences,
                                              std::vector<fmi_boolean_t>& dataVecOut)
{
  dataVecOut.resize(valueReferences.size());
  std::vector<fmi2_boolean_t> dataOut(valueReferences.size());

  auto status = fmi2_import_get_boolean(cdata.fmu2,
                                        valueReferences.data(),  // array of value reference
                                        valueReferences.size(),  // number of elements
                                        dataOut.data());         // array of values

  for (int i = 0; i < valueReferences.size(); i++)
  {
    dataVecOut[i].emplace<FMI2>(dataOut[i]);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  }

  return status;
}