/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2019-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Common/route_definition.h>
#include <MantleAPI/Common/trajectory.h>
#include <cstdint>
#include <google/protobuf/util/json_util.h>
#include <memory>
#include <osi3/osi_trafficcommand.pb.h>
#include <osi3/osi_trafficupdate.pb.h>
#include <sstream>
#include <string>
#include <string_view>

#include "common/accelerationSignal.h"
#include "common/commonTools.h"
#include "common/dynamicsSignal.h"
#include "common/longitudinalSignal.h"
#include "common/osiUtils.h"
#include "common/sensorDataSignal.h"
#include "common/speedActionSignal.h"
#include "common/steeringSignal.h"
#include "common/stringSignal.h"
#include "common/trajectorySignal.h"
#include "core/opSimulation/modules/World_OSI/WorldData.h"
#include "error_handler.h"
#include "include/agentInterface.h"
#include "include/egoAgentInterface.h"
#include "include/fmuHandlerInterface.h"
#include "variant_visitor.h"

extern "C"
{
#include <fmilib.h>

#include "fmuChecker.h"
}

/*
struct fmi1String{ fmi1_string_t string; };
struct fmi2String{ fmi2_string_t string; };
struct fmi1Real{ fmi1_real_t real; };
struct fmi2Real{ fmi2_real_t real; };
struct fmi1Integer{ fmi1_integer_t integer; };
struct fmi2Integer{ fmi2_integer_t integer; };
struct fmi1Boolean{ fmi1_boolean_t boolean; };
struct fmi2Boolean{ fmi2_boolean_t boolean; };
using fmi_string_t_s = std::variant<fmi1String, fmi2String>;
using fmi_real_t_s = std::variant<fmi1Real, fmi2Real>;
using fmi_integer_t_s = std::variant<fmi1Integer, fmi2Integer>;
using fmi_boolean_t_s = std::variant<fmi1Boolean, fmi2Boolean>;
*/

using fmi_string_t = std::variant<fmi1_string_t, fmi2_string_t>;
using fmi_real_t = std::variant<fmi1_real_t, fmi2_real_t>;
using fmi_integer_t = std::variant<fmi1_integer_t, fmi2_integer_t>;
using fmi_boolean_t = std::variant<fmi1_boolean_t, fmi2_boolean_t>;

/// This struct gives the correct data type for each combination of FMI version and variant type defined above (e.g.
/// fmi_string_t). Has to be specialized for the individual valid combinations.
template <size_t FMI, typename ValueType>
struct VersionedDataType
{
};

/// VersionedDataType specialization for FMI1 string type
template <>
struct VersionedDataType<FMI1, fmi_string_t>
{
  using type = fmi1_string_t;  ///< data type corresponding to FMI version and ValueType
};

/// VersionedDataType specialization for FMI2 string type
template <>
struct VersionedDataType<FMI2, fmi_string_t>
{
  using type = fmi2_string_t;  ///< data type corresponding to FMI version and ValueType
};

/// VersionedDataType specialization for FMI1 real type
template <>
struct VersionedDataType<FMI1, fmi_real_t>
{
  using type = fmi1_real_t;  ///< data type corresponding to FMI version and ValueType
};

/// VersionedDataType specialization for FMI2 real type
template <>
struct VersionedDataType<FMI2, fmi_real_t>
{
  using type = fmi2_real_t;  ///< data type corresponding to FMI version and ValueType
};

/// VersionedDataType specialization for FMI1 integer type
template <>
struct VersionedDataType<FMI1, fmi_integer_t>
{
  using type = fmi1_integer_t;  ///< data type corresponding to FMI version and ValueType
};

/// VersionedDataType specialization for FMI2 integer type
template <>
struct VersionedDataType<FMI2, fmi_integer_t>
{
  using type = fmi2_integer_t;  ///< data type corresponding to FMI version and ValueType
};

/// VersionedDataType specialization for FMI1 boolean type
template <>
struct VersionedDataType<FMI1, fmi_boolean_t>
{
  using type = fmi1_boolean_t;  ///< data type corresponding to FMI version and ValueType
};

/// VersionedDataType specialization for FMI2 boolean type
template <>
struct VersionedDataType<FMI2, fmi_boolean_t>
{
  using type = fmi2_boolean_t;  ///< data type corresponding to FMI version and ValueType
};

using fmi_t = std::variant<fmi_string_t, fmi_real_t, fmi_integer_t, fmi_boolean_t>;
using fmi_t_data = std::
    variant<std::vector<fmi_string_t>, std::vector<fmi_real_t>, std::vector<fmi_integer_t>, std::vector<fmi_boolean_t>>;
//using fmi_t_data = std::variant<fmi_string_t, fmi_real_t, fmi_integer_t, fmi_boolean_t>;

/// Structure representing an object containing information on sensor fusion
struct SensorFusionObjectInfo
{
  int id{-1};                                          ///< ID of the sensor fusion object
  int numberOfSensors{0};                              ///< Number of sensors
  units::length::meter_t t{0};                         ///< T position
  units::length::meter_t t_left{0};                    ///< T left position
  units::length::meter_t t_right{0};                   ///< T right position
  units::length::meter_t s{0};                         ///< s coordinate
  units::length::meter_t rel_x{0};                     ///< Relative x coordinate
  units::length::meter_t rel_y{0};                     ///< Relative y coordinate
  units::length::meter_t net_s{0};                     ///< Net s coordinate of all sensors
  units::length::meter_t net_x{0};                     ///< Net x coordinate
  units::length::meter_t net_y{0};                     ///< Net y coordinate
  int lane{0};                                         ///< Lane
  units::velocity::meters_per_second_t velocity{0};    ///< Velocity of the agent
  units::velocity::meters_per_second_t velocity_x{0};  ///< Velocity of the agent in x coordinate
  units::velocity::meters_per_second_t velocity_y{0};  ///< Velocity of the agent in y coordinate
  units::angle::radian_t yaw{0};                       ///< Yaw angle of the agent
};

/// @brief Class representing an FMU helper
class FmuHelper
{
public:
  /// @brief Generates the prefix for logging
  /// @param agentIdString  id of the agent as string
  /// @return
  static std::string log_prefix(const std::string& agentIdString);

  /// @brief Generates the prefix for logging
  /// @param agentIdString  id of the agent as string
  /// @param componentName  name of the component
  /// @return
  static std::string log_prefix(const std::string& agentIdString, const std::string& componentName);

  /// @brief Add a traffic command action from open scenario position
  /// @param trafficAction    Pointer to OSI traffic action
  /// @param position         OpenSCENARIO position
  /// @param worldInterface   Pointer to the world interface
  /// @param errorCallback    Function of error callback
  static void AddTrafficCommandActionFromOpenScenarioPosition(
      osi3::TrafficAction* trafficAction,
      const mantle_api::Position& position,
      WorldInterface* const worldInterface,
      const std::function<void(const std::string&)>& errorCallback);

  /// @brief Add an traffic command action from a open scenario trajectory
  /// @param trafficAction    Pointer to OSI traffic action
  /// @param trajectory       Open scenario trajectory
  static void AddTrafficCommandActionFromOpenScenarioTrajectory(osi3::TrafficAction* trafficAction,
                                                                const mantle_api::Trajectory& trajectory);

  /// @brief Add an traffic command action from mantle api route waypoints
  /// @param trafficAction        Pointer to OSI traffic action
  /// @param scenarioCommand      Reference to the scenario command assign route action
  static void AddTrafficCommandActionFromOpenScenarioRouteWayPoints(
      osi3::TrafficAction* trafficAction, const ScenarioCommand::AssignRoute* scenarioCommand);

  /// @brief Function to decode integer to pointers
  /// @tparam FMI
  /// @param hi
  /// @param lo
  /// @return
  template <size_t FMI>
  static void* decode_integer_to_pointer(fmi_integer_t hi, fmi_integer_t lo);

  /// @brief Function to encode pointer to integer
  /// @tparam FMI
  /// @param ptr
  /// @param hi
  /// @param lo
  template <size_t FMI>
  static void encode_pointer_to_integer(const void* ptr, fmi_integer_t& hi, fmi_integer_t& lo);

  /// @brief Function to append message
  /// @param appendedMessage message to append
  /// @param message         existing message
  static void AppendMessages(std::string& appendedMessage, std::string& message);

  /// @brief Function to convert an integer to bytes
  /// @param paramInt Integer value
  /// @return list of converted bytes
  static std::vector<unsigned char> intToBytes(int paramInt);

  /// @brief Generate a default sensor view configuration
  /// @return Default OSI sensor view configuration
  static osi3::SensorViewConfiguration GenerateDefaultSensorViewConfiguration();

  /// @brief Function to get references from integer vector
  /// @tparam FMI
  /// @param valueReferencesVec   List of references of the value
  /// @return FMI value reference
  template <size_t FMI>
  static fmi_value_references_t GetReferencesFromIntVector(std::vector<int> valueReferencesVec);

  /// @brief FUnction to convert FMI data from values
  /// @tparam FMI
  /// @param dataVec  list of values
  /// @return FMI integer
  template <size_t FMI, typename ValueType>
  static std::vector<typename VersionedDataType<FMI, ValueType>::type> FmiDataFromValues(
      const std::vector<ValueType>& dataVec);

  /// @brief Function to generate a string
  /// @param operation    Operation
  /// @param name         Name
  /// @param datatype
  /// @param value
  /// @return
  static std::string GenerateString(std::string_view operation,
                                    std::string_view name,
                                    VariableType datatype,
                                    FmuValue value);

  /// @brief Function to convert a given datatype to the string
  /// @param datatype Given variable data type
  /// @return data type in string
  static std::string VariableTypeToStringMap(VariableType datatype)
  {
    static std::map<VariableType, std::string> s{{VariableType::Bool, "Bool"},
                                                 {VariableType::Int, "Int"},
                                                 {VariableType::Double, "Double"},
                                                 {VariableType::String, "String"},
                                                 {VariableType::Enum, "Enum"}};

    return s[datatype];
  };

  /// @brief Function to convert the given cauality to string
  /// @param causality Causality of FMI variable
  /// @return string of the given causality
  static std::string CausalityToStringMap(fmi_causality_enu_t causality)
  {
    static std::map<fmi_causality_enu_t, std::string> s{
        {fmi1_causality_enu_input, "input"},
        {fmi1_causality_enu_output, "output"},
        {fmi2_causality_enu_input, "output"},
        {fmi2_causality_enu_output, "output"},
        {fmi2_causality_enu_parameter, "parameter"},
        {fmi2_causality_enu_calculated_parameter, "calculated parameter"}};

    return s[causality];
  };

  /// @brief Function to convet fmi variability to string
  /// @param variability FMU variablities
  /// @return string of the given variablity
  static std::string VariabilityToStringMap(fmi_variability_enu_t variability)
  {
    static std::map<fmi_variability_enu_t, std::string> s{
        {fmi1_variability_enu_discrete, "discrete"},
        {fmi1_variability_enu_parameter, "fixed"},
        {fmi1_variability_enu_continuous, "continuous"},
        {fmi1_variability_enu_constant, "constant"},
        {fmi1_variability_enu_unknown, "unknown"},

        {fmi2_variability_enu_discrete, "discrete"},
        {fmi2_variability_enu_fixed, "fixed"},
        {fmi2_variability_enu_continuous, "continuous"},
        {fmi2_variability_enu_constant, "constant"},
        {fmi2_variability_enu_tunable, "tunable"},
        {fmi2_variability_enu_unknown, "unknown"},
    };

    return s[variability];
  };
};

template <size_t FMI>
fmi_value_references_t FmuHelper::GetReferencesFromIntVector(std::vector<int> valueReferencesVec)
{
  fmi_value_references_t valueReferences;

  if constexpr (FMI == FMI1)
  {
    valueReferences.emplace<FMI>(std::vector<fmi1_value_reference_t>(valueReferencesVec.size()));
  }
  else if constexpr (FMI == FMI2)
  {
    valueReferences.emplace<FMI>(std::vector<fmi2_value_reference_t>(valueReferencesVec.size()));
  }

  for (int i = 0; i < valueReferencesVec.size(); i++)
  {
    std::get<FMI>(valueReferences)[i] = valueReferencesVec[i];
  }
  return valueReferences;
}

template <size_t FMI, typename ValueType>
std::vector<typename VersionedDataType<FMI, ValueType>::type> FmuHelper::FmiDataFromValues(
    const std::vector<ValueType>& dataVec)
{
  std::vector<typename VersionedDataType<FMI, ValueType>::type> data;
  data.resize(dataVec.size());

  for (int i = 0; i < dataVec.size(); i++)
  {
    data[i] = std::get<FMI>(dataVec[i]);
  }
  return data;
}

template <size_t FMI>
void FmuHelper::encode_pointer_to_integer(const void* ptr, fmi_integer_t& hi, fmi_integer_t& lo)
{
#if PTRDIFF_MAX == INT64_MAX
  union addrconv
  {
    struct
    {
      int lo;
      int hi;
    } base;
    unsigned long long address;
  } myaddr;
  myaddr.address = reinterpret_cast<unsigned long long>(ptr);
  hi.emplace<FMI>(myaddr.base.hi);
  lo.emplace<FMI>(myaddr.base.lo);
#elif PTRDIFF_MAX == INT32_MAX
  hi.emplace<FMI>(0);
  lo.emplace<FMI>(reinterpret_cast<int>(ptr));
#else
#error "Cannot determine 32bit or 64bit environment!"
#endif
}

template <size_t FMI>
void* FmuHelper::decode_integer_to_pointer(fmi_integer_t hi, fmi_integer_t lo)
{
#if PTRDIFF_MAX == INT64_MAX
  union addrconv
  {
    struct
    {
      int lo;
      int hi;
    } base;
    unsigned long long address;
  } myaddr;
  myaddr.base.lo = std::get<FMI>(lo);
  myaddr.base.hi = std::get<FMI>(hi);
  return reinterpret_cast<void*>(myaddr.address);
#elif PTRDIFF_MAX == INT32_MAX
  return reinterpret_cast<void*>(get<FMI>(lo));
#else
#error "Cannot determine 32bit or 64bit environment!"
#endif
}
