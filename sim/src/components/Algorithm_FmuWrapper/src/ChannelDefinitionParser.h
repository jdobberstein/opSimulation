/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <FMI1/fmi1_import_variable_list.h>
#include <set>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

#include "common/driverWarning.h"
#include "include/fmuHandlerInterface.h"

//using Fmu1Variables = std::unordered_map<std::string, ValueReferenceAndType1>;
//using Fmu2Variables = std::unordered_map<std::string, ValueReferenceAndType2>;
//using FmuVariables = std::variant<Fmu1Variables, Fmu2Variables>;

/// List of openPASS values (e.g. provided by AgentInterface) that can be assigned to FMU inputs
enum class FmuInputType
{
  VelocityEgo,
  AccelerationEgo,
  CentripetalAccelerationEgo,
  SteeringWheelEgo,
  AccelerationPedalPositionEgo,
  BrakePedalPositionEgo,
  DistanceRefToFrontEdgeEgo,
  PositionXEgo,
  PositionYEgo,
  YawEgo,
  PositionSEgo,
  PositionTEgo,
  LaneEgo,
  ExistenceFront,
  PositionXFront,
  PositionYFront,
  YawFront,
  PositionSFront,
  PositionTFront,
  RelativeDistanceFront,
  WidthFront,
  LengthFront,
  DistanceRefToFrontEdgeFront,
  VelocityFront,
  LaneFront,
  ExistenceFrontFront,
  PositionXFrontFront,
  PositionYFrontFront,
  RelativeDistanceFrontFront,
  VelocityFrontFront,
  LaneFrontFront,
  LaneCountLeft,
  LaneCountRight,
  SpeedLimit,
  RoadCurvature,
  SensorFusionObjectId,
  SensorFusionNumberOfDetectingSensors,
  SensorFusionRelativeS,
  SensorFusionRelativeNetS,
  SensorFusionRelativeT,
  SensorFusionRelativeX,
  SensorFusionRelativeY,
  SensorFusionRelativeNetLeft,
  SensorFusionRelativeNetRight,
  SensorFusionRelativeNetX,
  SensorFusionRelativeNetY,
  SensorFusionLane,
  SensorFusionVelocity,
  SensorFusionVelocityX,
  SensorFusionVelocityY,
  SensorFusionYaw
};

//! Type of output signal
enum class SignalType
{
  LateralSignal,
  AccelerationSignal,
  LongitudinalSignal,
  SteeringSignal,
  DynamicsSignal,
  CompCtrlSignal,
  CompCtrlSignalWarningDirection,
  SensorDataSignal
};

//! Value in an output signal
enum class SignalValue
{
  ComponentState,
  AccelerationSignal_Acceleration,
  LongitudinalSignal_AccPedalPos,
  LongitudinalSignal_BrakePedalPos,
  LongitudinalSignal_Gear,
  SteeringSignal_SteeringWheelAngle,
  DynamicsSignal_Acceleration,
  DynamicsSignal_Velocity,
  DynamicsSignal_PositionX,
  DynamicsSignal_PositionY,
  DynamicsSignal_Yaw,
  DynamicsSignal_YawRate,
  DynamicsSignal_YawAcceleration,
  DynamicsSignal_SteeringWheelAngle,
  DynamicsSignal_CentripetalAcceleration,
  DynamicsSignal_TravelDistance,
  CompCtrlSignal_MovementDomain,
  CompCtrlSignal_WarningActivity,
  CompCtrlSignal_WarningLevel,
  CompCtrlSignal_WarningType,
  CompCtrlSignal_WarningIntensity,
  CompCtrlSignal_WarningDirection
};

//! Assignment of a FMU input variable to a value / method of calculation in openPASS
struct Fmu1Input
{
  FmuInputType type;  //!< Corresponding value in openPASS
  std::variant<double, size_t>
      additionalParameter;  //!< Additional parameter for calculation of the value, if necessary (e.g. distance, index)
  fmi1_value_reference_t valueReference;  //!< Value reference of the FMU

  /// @brief Construct an FMU 1.0 input
  /// @param type                 Corresponding value in openPASS
  /// @param additionalParameter  Additional parameter for calculation of the value, if necessary (e.g. distance, index)
  /// @param valueReference       Value reference of the FMU
  Fmu1Input(FmuInputType type, std::variant<double, size_t> additionalParameter, fmi1_value_reference_t valueReference)
      : type(type), additionalParameter(additionalParameter), valueReference(valueReference)
  {
  }
};

//! Assignment of a FMU input variable to a value / method of calculation in openPASS
struct Fmu2Input
{
  FmuInputType type;  //!< Corresponding value in openPASS
  std::variant<double, size_t>
      additionalParameter;  //!< Additional parameter for calculation of the value, if necessary (e.g. distance, index)
  fmi2_value_reference_t valueReference;  //!< Value reference of the FMU

  /// @brief Construct an FMU 2.0 input
  /// @param type                 Corresponding value in openPASS
  /// @param additionalParameter  Additional parameter for calculation of the value, if necessary (e.g. distance, index)
  /// @param valueReference       Value reference of the FMU
  Fmu2Input(FmuInputType type, std::variant<double, size_t> additionalParameter, fmi2_value_reference_t valueReference)
      : type(type), additionalParameter(additionalParameter), valueReference(valueReference)
  {
  }
};

using Fmu1Inputs = std::vector<Fmu1Input>;
using Fmu1Outputs = std::map<SignalValue, fmi1_value_reference_t>;
using Fmu2Inputs = std::vector<Fmu2Input>;
using Fmu2Outputs = std::map<SignalValue, fmi2_value_reference_t>;
using FmuInputs = std::variant<Fmu1Inputs, Fmu2Inputs>;
using FmuOutputs = std::variant<Fmu1Outputs, Fmu2Outputs>;

template <typename T>
struct Fmu1Parameter  ///< Fmu1 parameter
{
  T value;                                ///< Value of an paramter
  fmi1_value_reference_t valueReference;  ///< Value reference of the FMU

  /// @brief Initialize a fmu 1.0 parameter
  /// @param value            Value of an parameter
  /// @param valueReference   Value reference of the FMU
  Fmu1Parameter(T value, fmi1_value_reference_t valueReference) : value(value), valueReference(valueReference) {}
};

template <typename T>
struct Fmu2Parameter  ///< Fmu2 parameter
{
  T value;                                ///< Value of an paramter
  fmi2_value_reference_t valueReference;  ///< Value reference of the FMU

  /// @brief Initialize a fmu 2.0 parameter
  /// @param value            Value of an parameter
  /// @param valueReference   Value reference of the FMU
  Fmu2Parameter(T value, fmi2_value_reference_t valueReference) : value(value), valueReference(valueReference) {}
};

template <typename T>
using Fmu1Parameters = std::vector<Fmu1Parameter<T>>;
template <typename T>
using Fmu2Parameters = std::vector<Fmu2Parameter<T>>;
template <typename T>
using FmuParameters = std::variant<Fmu1Parameters<T>, Fmu2Parameters<T>>;

//! maps the string of the parameter to the enum value for the input types
static std::map<std::string, std::pair<FmuInputType, VariableType>> stringToFmuTypeMap{
    {"VelocityEgo", {FmuInputType::VelocityEgo, VariableType::Double}},
    {"AccelerationEgo", {FmuInputType::AccelerationEgo, VariableType::Double}},
    {"CentripetalAccelerationEgo", {FmuInputType::CentripetalAccelerationEgo, VariableType::Double}},
    {"SteeringWheelEgo", {FmuInputType::SteeringWheelEgo, VariableType::Double}},
    {"AccelerationPedalPositionEgo", {FmuInputType::AccelerationPedalPositionEgo, VariableType::Double}},
    {"BrakePedalPositionEgo", {FmuInputType::BrakePedalPositionEgo, VariableType::Double}},
    {"DistanceRefToFrontEdgeEgo", {FmuInputType::DistanceRefToFrontEdgeEgo, VariableType::Double}},
    {"PositionXEgo", {FmuInputType::PositionXEgo, VariableType::Double}},
    {"PositionYEgo", {FmuInputType::PositionYEgo, VariableType::Double}},
    {"LaneEgo", {FmuInputType::LaneEgo, VariableType::Int}},
    {"YawEgo", {FmuInputType::YawEgo, VariableType::Double}},
    {"PositionSEgo", {FmuInputType::PositionSEgo, VariableType::Double}},
    {"PositionTEgo", {FmuInputType::PositionTEgo, VariableType::Double}},
    {"ExistenceFront", {FmuInputType::ExistenceFront, VariableType::Bool}},
    {"PositionXFront", {FmuInputType::PositionXFront, VariableType::Double}},
    {"PositionYFront", {FmuInputType::PositionYFront, VariableType::Double}},
    {"YawFront", {FmuInputType::YawFront, VariableType::Double}},
    {"PositionSFront", {FmuInputType::PositionSFront, VariableType::Double}},
    {"PositionTFront", {FmuInputType::PositionTFront, VariableType::Double}},
    {"RelativeDistanceFront", {FmuInputType::RelativeDistanceFront, VariableType::Double}},
    {"WidthFront", {FmuInputType::WidthFront, VariableType::Double}},
    {"LengthFront", {FmuInputType::LengthFront, VariableType::Double}},
    {"DistanceRefToFrontEdgeFront", {FmuInputType::DistanceRefToFrontEdgeFront, VariableType::Double}},
    {"VelocityFront", {FmuInputType::VelocityFront, VariableType::Double}},
    {"LaneFront", {FmuInputType::LaneFront, VariableType::Int}},
    {"ExistenceFrontFront", {FmuInputType::ExistenceFrontFront, VariableType::Bool}},
    {"PositionXFrontFront", {FmuInputType::PositionXFrontFront, VariableType::Double}},
    {"PositionYFrontFront", {FmuInputType::PositionYFrontFront, VariableType::Double}},
    {"RelativeDistanceFrontFront", {FmuInputType::RelativeDistanceFrontFront, VariableType::Double}},
    {"VelocityFrontFront", {FmuInputType::VelocityFrontFront, VariableType::Double}},
    {"LaneFrontFront", {FmuInputType::LaneFrontFront, VariableType::Int}},
    {"LaneCountLeft", {FmuInputType::LaneCountLeft, VariableType::Int}},
    {"LaneCountRight", {FmuInputType::LaneCountRight, VariableType::Int}},
    {"SpeedLimit", {FmuInputType::SpeedLimit, VariableType::Double}},
    {"RoadCurvature", {FmuInputType::RoadCurvature, VariableType::Double}},
    {"SensorFusionObjectId", {FmuInputType::SensorFusionObjectId, VariableType::Int}},
    {"SensorFusionNumberOfDetectingSensors", {FmuInputType::SensorFusionNumberOfDetectingSensors, VariableType::Int}},
    {"SensorFusionRelativeS", {FmuInputType::SensorFusionRelativeS, VariableType::Double}},
    {"SensorFusionRelativeNetS", {FmuInputType::SensorFusionRelativeNetS, VariableType::Double}},
    {"SensorFusionRelativeT", {FmuInputType::SensorFusionRelativeT, VariableType::Double}},
    {"SensorFusionRelativeX", {FmuInputType::SensorFusionRelativeX, VariableType::Double}},
    {"SensorFusionRelativeY", {FmuInputType::SensorFusionRelativeY, VariableType::Double}},
    {"SensorFusionRelativeNetLeft", {FmuInputType::SensorFusionRelativeNetLeft, VariableType::Double}},
    {"SensorFusionRelativeNetRight", {FmuInputType::SensorFusionRelativeNetRight, VariableType::Double}},
    {"SensorFusionRelativeNetX", {FmuInputType::SensorFusionRelativeNetX, VariableType::Double}},
    {"SensorFusionRelativeNetY", {FmuInputType::SensorFusionRelativeNetY, VariableType::Double}},
    {"SensorFusionLane", {FmuInputType::SensorFusionLane, VariableType::Int}},
    {"SensorFusionVelocity", {FmuInputType::SensorFusionVelocity, VariableType::Double}},
    {"SensorFusionVelocityX", {FmuInputType::SensorFusionVelocityX, VariableType::Double}},
    {"SensorFusionVelocityY", {FmuInputType::SensorFusionVelocityY, VariableType::Double}},
    {"SensorFusionYaw", {FmuInputType::SensorFusionYaw, VariableType::Double}}};

//! maps the string of the parameter to the enum value for the output types
static std::map<std::string, std::pair<SignalValue, VariableType>> stringToSignalValueMap{
    {"ComponentState", {SignalValue::ComponentState, VariableType::Enum}},
    {"AccelerationSignal_Acceleration", {SignalValue::AccelerationSignal_Acceleration, VariableType::Double}},
    {"LongitudinalSignal_AccPedalPos", {SignalValue::LongitudinalSignal_AccPedalPos, VariableType::Double}},
    {"LongitudinalSignal_BrakePedalPos", {SignalValue::LongitudinalSignal_BrakePedalPos, VariableType::Double}},
    {"LongitudinalSignal_Gear", {SignalValue::LongitudinalSignal_Gear, VariableType::Int}},
    {"SteeringSignal_SteeringWheelAngle", {SignalValue::SteeringSignal_SteeringWheelAngle, VariableType::Double}},
    {"DynamicsSignal_Acceleration", {SignalValue::DynamicsSignal_Acceleration, VariableType::Double}},
    {"DynamicsSignal_Velocity", {SignalValue::DynamicsSignal_Velocity, VariableType::Double}},
    {"DynamicsSignal_PositionX", {SignalValue::DynamicsSignal_PositionX, VariableType::Double}},
    {"DynamicsSignal_PositionY", {SignalValue::DynamicsSignal_PositionY, VariableType::Double}},
    {"DynamicsSignal_Yaw", {SignalValue::DynamicsSignal_Yaw, VariableType::Double}},
    {"DynamicsSignal_YawRate", {SignalValue::DynamicsSignal_YawRate, VariableType::Double}},
    {"DynamicsSignal_YawAcceleration", {SignalValue::DynamicsSignal_YawAcceleration, VariableType::Double}},
    {"DynamicsSignal_SteeringWheelAngle", {SignalValue::DynamicsSignal_SteeringWheelAngle, VariableType::Double}},
    {"DynamicsSignal_CentripetalAcceleration",
     {SignalValue::DynamicsSignal_CentripetalAcceleration, VariableType::Double}},
    {"DynamicsSignal_TravelDistance", {SignalValue::DynamicsSignal_TravelDistance, VariableType::Double}},
    {"CompCtrlSignal_MovementDomain", {SignalValue::CompCtrlSignal_MovementDomain, VariableType::Enum}},
    {"CompCtrlSignal_WarningActivity", {SignalValue::CompCtrlSignal_WarningActivity, VariableType::Bool}},
    {"CompCtrlSignal_WarningLevel", {SignalValue::CompCtrlSignal_WarningLevel, VariableType::Enum}},
    {"CompCtrlSignal_WarningType", {SignalValue::CompCtrlSignal_WarningType, VariableType::Enum}},
    {"CompCtrlSignal_WarningIntensity", {SignalValue::CompCtrlSignal_WarningIntensity, VariableType::Enum}},
    {"CompCtrlSignal_WarningDirection", {SignalValue::CompCtrlSignal_WarningDirection, VariableType::Enum}}};

//! defines which values are needed for each signal
static std::map<SignalType, std::vector<SignalValue>> valuesOfSignalType{
    {SignalType::AccelerationSignal, {SignalValue::AccelerationSignal_Acceleration}},
    {SignalType::LongitudinalSignal,
     {SignalValue::LongitudinalSignal_AccPedalPos,
      SignalValue::LongitudinalSignal_BrakePedalPos,
      SignalValue::LongitudinalSignal_Gear}},
    {SignalType::SteeringSignal, {SignalValue::SteeringSignal_SteeringWheelAngle}},
    {SignalType::DynamicsSignal,
     {SignalValue::DynamicsSignal_Acceleration,
      SignalValue::DynamicsSignal_Velocity,
      SignalValue::DynamicsSignal_PositionX,
      SignalValue::DynamicsSignal_PositionY,
      SignalValue::DynamicsSignal_Yaw,
      SignalValue::DynamicsSignal_YawRate,
      SignalValue::DynamicsSignal_YawAcceleration,
      SignalValue::DynamicsSignal_SteeringWheelAngle,
      SignalValue::DynamicsSignal_CentripetalAcceleration,
      SignalValue::DynamicsSignal_TravelDistance}},
    {SignalType::CompCtrlSignal,
     {SignalValue::CompCtrlSignal_MovementDomain,
      SignalValue::CompCtrlSignal_WarningActivity,
      SignalValue::CompCtrlSignal_WarningLevel,
      SignalValue::CompCtrlSignal_WarningType,
      SignalValue::CompCtrlSignal_WarningIntensity}},
    {SignalType::CompCtrlSignalWarningDirection, {SignalValue::CompCtrlSignal_WarningDirection}}};

//! This struct gives the mapping of integer values to enumeration values as defined by the fmu
struct FmuEnumerations
{
  std::unordered_map<unsigned int, ComponentState> componentStates;                //!< The map for component states
  std::unordered_map<unsigned int, MovementDomain> movementDomains;                //!< The map for movement domains
  std::unordered_map<unsigned int, ComponentWarningLevel> warningLevels;           //!< The map for warning levels
  std::unordered_map<unsigned int, ComponentWarningType> warningTypes;             //!< The map for warning types
  std::unordered_map<unsigned int, ComponentWarningIntensity> warningIntensities;  //!< The map for warning intensities
  std::unordered_map<unsigned int, AreaOfInterest> warningDirections;              //!< The map for warning directions
};

//! maps the string to the enum value for the component states
static std::map<std::string, ComponentState> stringToComponentState{{"Undefined", ComponentState::Undefined},
                                                                    {"Disabled", ComponentState::Disabled},
                                                                    {"Armed", ComponentState::Armed},
                                                                    {"Acting", ComponentState::Acting}};

//! maps the string to the enum value for the movement domains
static std::map<std::string, MovementDomain> stringToMovementDomain{{"Undefined", MovementDomain::Undefined},
                                                                    {"Lateral", MovementDomain::Lateral},
                                                                    {"Longitudinal", MovementDomain::Longitudinal},
                                                                    {"Both", MovementDomain::Both}};

//! maps the string to the enum value for the component warning levels
static std::map<std::string, ComponentWarningLevel> stringToComponentWarningLevel{
    {"Info", ComponentWarningLevel::INFO}, {"Warning", ComponentWarningLevel::WARNING}};

//! maps the string to the enum value for the component warning types
static std::map<std::string, ComponentWarningType> stringToComponentWarningType{
    {"Optic", ComponentWarningType::OPTIC},
    {"Acoustic", ComponentWarningType::ACOUSTIC},
    {"Haptic", ComponentWarningType::HAPTIC}};

//! maps the string to the enum value for the component warning intensities
static std::map<std::string, ComponentWarningIntensity> stringToComponentWarningIntensity{
    {"Low", ComponentWarningIntensity::LOW},
    {"Medium", ComponentWarningIntensity::MEDIUM},
    {"High", ComponentWarningIntensity::HIGH}};

//! maps the string to the enum value for the component warning directions
static std::map<std::string, AreaOfInterest> stringToWarningDirection{
    {"LEFT_FRONT", AreaOfInterest::LEFT_FRONT},
    {"LEFT_FRONT_FAR", AreaOfInterest::LEFT_FRONT_FAR},
    {"RIGHT_FRONT", AreaOfInterest::RIGHT_FRONT},
    {"RIGHT_FRONT_FAR", AreaOfInterest::RIGHT_FRONT_FAR},
    {"LEFT_REAR", AreaOfInterest::LEFT_REAR},
    {"RIGHT_REAR", AreaOfInterest::RIGHT_REAR},
    {"EGO_FRONT", AreaOfInterest::EGO_FRONT},
    {"EGO_FRONT_FAR", AreaOfInterest::EGO_FRONT_FAR},
    {"EGO_REAR", AreaOfInterest::EGO_REAR},
    {"LEFT_SIDE", AreaOfInterest::LEFT_SIDE},
    {"RIGHT_SIDE", AreaOfInterest::RIGHT_SIDE},
    {"INSTRUMENT_CLUSTER", AreaOfInterest::INSTRUMENT_CLUSTER},
    {"INFOTAINMENT", AreaOfInterest::INFOTAINMENT},
    {"HUD", AreaOfInterest::HUD},
    {"LEFTLEFT_FRONT", AreaOfInterest::LEFTLEFT_FRONT},
    {"RIGHTRIGHT_FRONT", AreaOfInterest::RIGHTRIGHT_FRONT},
    {"LEFTLEFT_REAR", AreaOfInterest::LEFTLEFT_REAR},
    {"RIGHTRIGHT_REAR", AreaOfInterest::RIGHTRIGHT_REAR},
    {"LEFTLEFT_SIDE", AreaOfInterest::LEFTLEFT_SIDE},
    {"RIGHTRIGHT_SIDE", AreaOfInterest::RIGHTRIGHT_SIDE},
    {"DISTRACTION", AreaOfInterest::DISTRACTION}};

template <size_t FMI>
class ChannelDefinitionParser  ///< Class representing a parser for channel definition
{
public:
  /*!
   * \brief ChannelDefinitionParser constructor
   *
   * \param[in]       fmuVariables        Map from FMU variable names to their value references
   * \param[in]       fmiVersion          Interface version of FMI
   * \param[in]       callbacks           Pointer to the callback interface
   */
  ChannelDefinitionParser(const FmuVariables& fmuVariables,
                          fmi_version_enu_t fmiVersion,
                          const CallbackInterface* callbacks);

  /*!
   * \brief Adds a FMU output channel mapping
   *
   * \param[in]       outputType          Type of the output variable
   * \param[in]       variableName        FMU variable name
   * \param[in]       fmuTypeDefinitions  FMU TypeDefinitions
   */
  void AddOutputChannel(const std::string& outputType,
                        const std::string& variableName,
                        std::unordered_map<std::string, std::unordered_map<int, std::string>> fmuTypeDefinitions);

  /*!
   * \brief Adds a FMU input channel mapping
   *
   * \param[in]       inputType       type of the input variable (e.g. "VelocityEgo")
   * \param[in]       variableName    FMU variable name
   */
  void AddInputChannel(const std::string& inputType, const std::string& variableName);

  /*!
   * \brief Adds a FMU parameter channel mapping
   *
   * \param[in]       inputValue      value of the parameter
   * \param[in]       variableName    FMU channel definition
   */
  template <typename T>
  void AddParameter(const T& inputValue, const std::string& variableName)
  {
    AddParameter<T, FMI>(inputValue, variableName);
  }

  /*!
   * \brief Adds a FMU parameter channel mapping
   *
   * \param[in]       inputValue      value of the parameter
   * \param[in]       variableName    FMU channel definition
   */
  template <typename T, size_t Version>
  void AddParameter(const T& inputValue, const std::string& variableName)
  {
    auto unmappedVariableItem = std::get<Version>(unmappedFmuVariables).find(variableName);

    if (unmappedVariableItem == std::get<Version>(unmappedFmuVariables).end())
    {
      LOGERRORANDTHROW("Unable to add parameter: Variable <" + variableName + "> not defined in FMU");
    }

    const auto& [valueReference, variableType, casuality, variablility] = unmappedVariableItem->second;

    if constexpr (std::is_same_v<T, std::string>)
    {
      if (variableType == VariableType::String)
      {
        std::get<Version>(fmuStringParameters).emplace_back(inputValue, valueReference);
        std::get<Version>(unmappedFmuVariables).erase(unmappedVariableItem);
      }
      else
      {
        LOGERRORANDTHROW("Unable to add parameter: Variable type in FMU not the same as in Wrapper");
      }
    }
    else if constexpr (std::is_same_v<T, int>)
    {
      if (variableType == VariableType::Int)
      {
        std::get<Version>(fmuIntegerParameters).emplace_back(inputValue, valueReference);
        std::get<Version>(unmappedFmuVariables).erase(unmappedVariableItem);
      }
      else
      {
        LOGERRORANDTHROW("Unable to add parameter: Variable type in FMU not the same as in Wrapper");
      }
    }
    else if constexpr (std::is_same_v<T, double>)
    {
      if (variableType == VariableType::Double)
      {
        std::get<Version>(fmuDoubleParameters).emplace_back(inputValue, valueReference);
        std::get<Version>(unmappedFmuVariables).erase(unmappedVariableItem);
      }
      else
      {
        LOGERRORANDTHROW("Unable to add parameter: Variable type in FMU not the same as in Wrapper");
      }
    }
    else if constexpr (std::is_same_v<T, bool>)
    {
      if (variableType == VariableType::Bool)
      {
        std::get<Version>(fmuBoolParameters).emplace_back(inputValue, valueReference);
        std::get<Version>(unmappedFmuVariables).erase(unmappedVariableItem);
      }
      else
      {
        LOGERRORANDTHROW("Unable to add parameter: Variable type in FMU not the same as in Wrapper");
      }
    }
    else
    {
      LOGERRORANDTHROW("Unable to add parameter: Variable type in FMU not the same as in Wrapper");
    }
  }

  /*!
   * \brief Retrieve the output link id mapping
   *
   * \return          Map from output link id to tuple of value reference with type id hash code
   */
  FmuOutputs GetFmuOutputs();

  /*!
   * \brief Retrieve the output signals
   *
   * \return          Set of signal types
   */
  std::set<SignalType> GetOutputSignals();

  /// @brief Parse output signal types
  void ParseOutputSignalTypes();

  /*!
   * \brief Retrieve the input link id mapping for real variables
   *
   * \return          Map from input type to corresponding FMU value reference
   */
  FmuInputs GetFmuRealInputs();

  /*!
   * \brief Retrieve the input link id mapping for integer variables
   *
   * \return          Map from input type to corresponding FMU value reference
   */
  FmuInputs GetFmuIntegerInputs();

  /*!
   * \brief Retrieve the input link id mapping for boolean variables
   *
   * \return          Map from input type to corresponding FMU value reference
   */
  FmuInputs GetFmuBooleanInputs();

  /*!
   * \brief Retrieve the input link id mapping for string variables
   *
   * \return          Map from input type to corresponding FMU value reference
   */
  FmuInputs GetFmuStringInputs();

  /*!
   * \brief Retrieve the input link id mapping for string parameters
   *
   * \return          List of FMU parameters and their value
   */
  FmuParameters<std::string> GetFmuStringParameters();

  /*!
   * \brief Retrieve the input link id mapping for integer parameters
   *
   * \return          List of FMU parameters and their value
   */
  FmuParameters<int> GetFmuIntegerParameters();

  /*!
   * \brief Retrieve the input link id mapping for double parameters
   *
   * \return          List of FMU parameters and their value
   */
  FmuParameters<double> GetFmuDoubleParameters();

  /*!
   * \brief Retrieve the input link id mapping for bool parameters
   *
   * \return          List of FMU parameters and their value
   */
  FmuParameters<bool> GetFmuBoolParameters();

  /*!
   * \brief Retrieve the FMU Enumerations
   *
   * \return          The maps of FMU Enumerations
   */
  FmuEnumerations GetFmuEnumerations();

  /*!
   * \brief Log the information of FMI communication
   * \param[in] logLevel Level of log
   * \param[in] file     File to log
   * \param[in] line     Line
   * \param[in] message  Message to log
   */
  void Log(CbkLogLevel logLevel, const char* file, int line, const std::string& message);

  const CallbackInterface* callbacks;  ///< Pointer to the callback interface

private:
  const fmi_version_enu_t fmiVersion;

  FmuVariables unmappedFmuVariables;  //!< FMU variables without output link mapped
  FmuOutputs fmuOutputs;              //!< output link id mapping
  std::set<SignalType> outputSignals;
  FmuInputs fmuRealInputs;
  FmuInputs fmuIntegerInputs;
  FmuInputs fmuBooleanInputs;
  FmuInputs fmuStringInputs;
  FmuParameters<int> fmuIntegerParameters;
  FmuParameters<double> fmuDoubleParameters;
  FmuParameters<bool> fmuBoolParameters;
  FmuParameters<std::string> fmuStringParameters;
  FmuEnumerations fmuEnumerations;
};

#include "ChannelDefinitionParser.tpp"
