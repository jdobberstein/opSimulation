/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LongitudinalSignalParser.h"

std::shared_ptr<const SignalInterface> LongitudinalSignalParser::Translate(
    const std::set<SignalType> &outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue)
{
  if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::LongitudinalSignal) != outputSignals.cend())
  {
    auto accPedalPos = getFmuSignalValue(SignalValue::LongitudinalSignal_AccPedalPos, VariableType::Double)
                           .realValue;  // NOLINT(cppcoreguidelines-pro-type-union-access)
    auto brakePedalPos = getFmuSignalValue(SignalValue::LongitudinalSignal_BrakePedalPos, VariableType::Double)
                             .realValue;  // NOLINT(cppcoreguidelines-pro-type-union-access)
    auto gear = getFmuSignalValue(SignalValue::LongitudinalSignal_Gear, VariableType::Int)
                    .intValue;  // NOLINT(cppcoreguidelines-pro-type-union-access)
    return std::make_shared<LongitudinalSignal const>(componentState, accPedalPos, brakePedalPos, gear, componentName);
  }
  return std::make_shared<LongitudinalSignal const>(ComponentState::Disabled, 0.0, 0.0, 0, componentName);
}