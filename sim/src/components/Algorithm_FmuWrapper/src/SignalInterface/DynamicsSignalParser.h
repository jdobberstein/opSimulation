/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>

#include "common/dynamicsSignal.h"
#include "common/sensorDataSignal.h"
#include "components/Algorithm_FmuWrapper/src/ChannelDefinitionParser.h"
#include "include/agentInterface.h"
#include "include/signalInterface.h"

//! @brief Parser for dynamics signal
struct DynamicsSignalParser
{
  //! @brief Translator function for dynamics signal
  //!
  //! @param outputSignals      Container that contains a sorted set of output signal
  //! @param componentName      Name of the component
  //! @param getFmuSignalValue  Function that creates FmuValue from value in an output signal and VariableType
  //! @return pointer to the signal interface
  static std::shared_ptr<const SignalInterface> Translate(
      const std::set<SignalType> &outputSignals,
      const std::string &componentName,
      const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue);
};