/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \brief SensorOSI.cpp */
//-----------------------------------------------------------------------------

#include "sensorOSI.h"

#include <map>
#include <memory>
#include <new>
#include <stdexcept>
#include <string>
#include <utility>

#include "include/parameterInterface.h"
#include "src/sensorCar2X.h"
#include "src/sensorGeometric2D.h"

class AgentInterface;
class PublisherInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;
class ModelInterface;
class CallbackInterface;

const std::string Version = "0.0.1";
static const CallbackInterface *Callbacks = nullptr;  // NOLINT[cppcoreguidelines-avoid-non-const-global-variables]

extern "C" SENSOR_OSI_SHARED_EXPORT const std::string &OpenPASS_GetVersion()
{
  return Version;
}

//-----------------------------------------------------------------------------
//! dll-function to create an instance of the module.
//!
//! @param[in]     componentName  Name of the component
//! @param[in]     isInit         Corresponds to "init" of "Component"
//! @param[in]     priority       Corresponds to "priority" of "Component"
//! @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
//! @param[in]     responseTime   Corresponds to "responseTime" of "Component"
//! @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
//! @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
//! @param[in]     world          Pointer to the world
//! @param[in]     parameters     Pointer to the parameters of the module
//! @param[in]     publisher      Pointer to the publisher instance
//! @param[in]     agent          Pointer to the agent in which the module is situated
//! @param[in]     callbacks      Pointer to the callbacks
//! @return                       A pointer to the created module instance.
//-----------------------------------------------------------------------------
extern "C" SENSOR_OSI_SHARED_EXPORT ModelInterface *OpenPASS_CreateInstance(std::string componentName,
                                                                            bool isInit,
                                                                            int priority,
                                                                            int offsetTime,
                                                                            int responseTime,
                                                                            int cycleTime,
                                                                            StochasticsInterface *stochastics,
                                                                            WorldInterface *world,
                                                                            const ParameterInterface *parameters,
                                                                            PublisherInterface *const publisher,
                                                                            AgentInterface *agent,
                                                                            const CallbackInterface *callbacks)
{
  Callbacks = callbacks;

  try
  {
    std::string sensorType = parameters->GetParametersString().at("Type");

    if (sensorType == "Geometric2D")
    {
      return (ModelInterface *)(new (std::nothrow) SensorGeometric2D(std::move(componentName),
                                                                     isInit,
                                                                     priority,
                                                                     offsetTime,
                                                                     responseTime,
                                                                     cycleTime,
                                                                     stochastics,
                                                                     world,
                                                                     parameters,
                                                                     publisher,
                                                                     callbacks,
                                                                     agent));
    }
    if (sensorType == "ReceiverCar2X")
    {
      return (ModelInterface *)(new (std::nothrow) SensorCar2X(componentName,
                                                               isInit,
                                                               priority,
                                                               offsetTime,
                                                               responseTime,
                                                               cycleTime,
                                                               stochastics,
                                                               world,
                                                               parameters,
                                                               publisher,
                                                               callbacks,
                                                               agent));
    }
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "No such sensor type");
    }
    return nullptr;
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return nullptr;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return nullptr;
  }
}

//-----------------------------------------------------------------------------
//! dll-function to destroy/delete an instance of the module.
//!
//! @param[in]     implementation    The instance that should be freed
//-----------------------------------------------------------------------------
extern "C" SENSOR_OSI_SHARED_EXPORT void OpenPASS_DestroyInstance(ModelInterface *implementation)
{
  delete implementation;  // NOLINT(cppcoreguidelines-owning-memory)
}

extern "C" SENSOR_OSI_SHARED_EXPORT bool OpenPASS_UpdateInput(ModelInterface *implementation,
                                                              int localLinkId,
                                                              const std::shared_ptr<SignalInterface const> &data,
                                                              int time)
{
  try
  {
    implementation->UpdateInput(localLinkId, data, time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

extern "C" SENSOR_OSI_SHARED_EXPORT bool OpenPASS_UpdateOutput(ModelInterface *implementation,
                                                               int localLinkId,
                                                               std::shared_ptr<SignalInterface const> &data,
                                                               int time)
{
  try
  {
    implementation->UpdateOutput(localLinkId, data, time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

extern "C" SENSOR_OSI_SHARED_EXPORT bool OpenPASS_Trigger(ModelInterface *implementation, int time)
{
  try
  {
    implementation->Trigger(time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}
