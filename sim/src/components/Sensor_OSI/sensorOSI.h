/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "sim/src/common/opExport.h"

#if defined(SENSOR_OSI_LIBRARY)
#define SENSOR_OSI_SHARED_EXPORT OPEXPORT
#else
#define SENSOR_OSI_SHARED_EXPORT OPIMPORT
#endif
