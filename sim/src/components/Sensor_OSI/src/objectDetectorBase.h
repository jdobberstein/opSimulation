/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2019 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** \addtogroup SensorOSI
 * @{
 * \brief This file models the SensorOSI.
 *
 * \details This file models the Sensors which can be part of an agent.
 *          The SensorOSI represent sensors which can detect other WorldObjects around the agent.
 *          Those sensors have different approaches in detecting other WorldObjects.
 *          The detected Objects can then be sent to the corresponding ADAS which uses them.
 *
 * \section SensorOSI_Outputs Outputs
 * Output variables:
 * name | meaning
 * -----|------
 * out_detectedObjects | Vector of all detected objects.
 *
 * Output channel IDs:
 * Output Id | signal class | contained variables
 * ----------|--------------|-------------
 *  0        | SensorDataSignal  | out_detectedObjects
 *
 *
 * \section SensorOSI_ExternalParameters External parameters
 * name | meaning
 * -----|---------
 * sensorParameters | All parameters which describe the sensor.
 *
 * @} */

#pragma once

#include <list>
#include <map>
#include <memory>
#include <osi3/osi_sensordata.pb.h>
#include <osi3/osi_sensorviewconfiguration.pb.h>
#include <stdint.h>
#include <string>
#include <utility>

#include "common/boostGeometryCommon.h"
#include "common/globalDefinitions.h"
#include "common/sensorDefinitions.h"
#include "include/modelInterface.h"

class AgentInterface;
class CallbackInterface;
class ParameterInterface;
class PublisherInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;
namespace osi3
{
class Dimension3d;
class MovingObject;
class Orientation3d;
class SensorView;
class StationaryObject;
class Vector3d;
}  // namespace osi3

//! Information when an object was first and last detected for calculation sensor delay
class DelayInformation
{
private:
  int timeFirstDetected;  //!< first timestep in which the object was detected
  int timeLastDetected;   //!< latest timestep in which the object was detected

public:
  //! Creates a delay information for a new object
  //! \param time current time
  DelayInformation(int time) : timeFirstDetected{time}, timeLastDetected{time} {}

  //! Checks if the object was detected for at least as long as the sensor delay and updates the DelayInformation
  //! \param delay          sensor delay
  //! \param maxDropOutTime maximum time before timer is reset
  //! \param time           current time
  //! \return wether object was detected long enough
  bool CheckDetection(int delay, int maxDropOutTime, int time)
  {
    if (time - timeLastDetected > maxDropOutTime)  //! If object was not seen for a certain time, reset timer
    {
      *this = {time};
      return false;
    }

    timeLastDetected = time;
    return (time - timeFirstDetected >= delay);
  }
};

//-----------------------------------------------------------------------------
/** \brief This class is the common base for all OSI sensors.
 *   \details This class is the common base for all OSI sensors.
 *            It provides the basic functionality for all sensors.
 *
 *   \ingroup SensorOSI
 */
//-----------------------------------------------------------------------------
class ObjectDetectorBase : public SensorInterface
{
public:
  /// Name of this component
  const std::string COMPONENTNAME = "Sensor_Model_Commonbase";

  using ObjectId = uint64_t;  ///< OSI id of an object

  /**
   * @brief Construct a new Object Detector Base object
   *
   * \param [in] componentName   Name of the component
   * \param [in] isInit          Query whether the component was just initialized
   * \param [in] priority        Priority of the component
   * \param [in] offsetTime      Offset time of the component
   * \param [in] responseTime    Response time of the component
   * \param [in] cycleTime       Cycle time of this components trigger task [ms]
   * \param [in] stochastics     Provides access to the stochastics functionality of the framework
   * \param [in] world           Provides access to world representation
   * \param [in] parameters      Interface provides access to the configuration parameters
   * \param [in] publisher       Instance  provided by the framework
   * \param [in] callbacks       Interface for callbacks to framework
   * \param [in] agent           This interface provides access to agent parameters, properties, attributes and dynamic states
   */
  ObjectDetectorBase(std::string componentName,
                     bool isInit,
                     int priority,
                     int offsetTime,
                     int responseTime,
                     int cycleTime,
                     StochasticsInterface* stochastics,
                     WorldInterface* world,
                     const ParameterInterface* parameters,
                     PublisherInterface* const publisher,
                     const CallbackInterface* callbacks,
                     AgentInterface* agent);

  ObjectDetectorBase(const ObjectDetectorBase&) = delete;
  ObjectDetectorBase(ObjectDetectorBase&&) = delete;
  ObjectDetectorBase& operator=(const ObjectDetectorBase&) = delete;
  ObjectDetectorBase& operator=(ObjectDetectorBase&&) = delete;

  virtual ~ObjectDetectorBase() = default;

  /*!
   * \brief Update Inputs
   *
   * Function is called by framework when another component delivers a signal over
   * a channel to this component (scheduler calls update taks of other component).
   *
   * Refer to module description for input channels and input ids.
   *
   * \param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
   * \param[in]     data           Referenced signal (copied by sending component)
   * \param[in]     time           Current scheduling time
   */
  virtual void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const>& data, int time) = 0;

  /*!
   * \brief Update outputs.
   *
   * Function is called by framework when this Component.has to deliver a signal over
   * a channel to another component (scheduler calls update task of this component).
   *
   * Refer to module description for output channels and output ids.
   *
   * \param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
   * \param[out]    data           Referenced signal (copied by this component)
   * \param[in]     time           Current scheduling time
   */
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const>& data, int time);

  /*!
   * \brief Process data within component.
   *
   * Function is called by framework when the scheduler calls the trigger task
   * of this component.
   *
   * Refer to module description for information about the module's task.
   *
   * \param[in]     time           Current scheduling time
   */
  virtual void Trigger(int time) = 0;

  /**
   * @brief Get the Sensor Data object (for testing)
   *
   * @return sensor data
   */
  const osi3::SensorData& getSensorData() { return sensorData; }

protected:
  /*!
   * \brief Adds the information of a detected moving object as DetectedMovingObject to the sensor data
   *
   * \param object             detected moving object
   * \param ownVelocity        velocity of own vehicle at sensor position in global coordinates
   * \param ownAcceleration    acceleration of own vehicle at sensor position in global coordinates
   * \param sensorPosition     position of sensor in global coordinates
   * \param ownYaw             yaw of sensor in global coordinates
   * \param ownYawRate         yawRate of own vehicle at sensor position in global coordinates
   * \param ownYawAcceleration yawAcceleration of own vehicle at sensor position in global coordinates
   */
  void AddMovingObjectToSensorData(const osi3::MovingObject& object,
                                   const point_t& ownVelocity,
                                   const point_t& ownAcceleration,
                                   const point_t& sensorPosition,
                                   double ownYaw,
                                   double ownYawRate,
                                   double ownYawAcceleration);

  /*!
   * \brief Adds the information of a detected stationary object as DetectedStationaryObject to the sensor data
   *
   * \param object            stationary moving object
   * \param sensorPosition    position of sensor in global coordinates
   * \param yaw               yaw of own vehicle in global coordinates
   */
  void AddStationaryObjectToSensorData(const osi3::StationaryObject& object, const point_t& sensorPosition, double yaw);

  /*!
   * \brief Returns the absolute position of the sensor
   *
   * This method calculates and returns the absolute position of the sensor by adding the agent's position
   * to the sensor's mounting position (and taking the yaw angle into account).
   *
   * \return The absolute position and yaw of the sensor with respect to world coordinates
   */
  Position GetAbsolutePosition();

  /*!
   * \brief Apply the predefined latency to the detected results
   *
   * The base implementation can be overriden in derived classes
   *
   * \param[in] time
   * \param[in] currentSensorData list of detected objects
   *
   * \return sensor data with latency applied
   */
  osi3::SensorData ApplyLatency(int time, osi3::SensorData currentSensorData);

  /*!
   * \brief Rolls for the failure chance of the sensor.
   *
   * @return	     true if the sensor failed. false otherwise.
   */
  bool HasDetectionError();

  //! Checks if an object was detected for at least as long as the sensor delay
  //!
  //! \param time     current time
  //! \param objectId id of the object
  //! \return true if object was detected long enough
  bool CheckDelay(int time, ObjectId objectId);

  /*!
   * \brief Generates the basic SensorViewConfiguration
   *
   * @return SensorViewConfiguration for parametrization of Sensorview
   */
  virtual osi3::SensorViewConfiguration GenerateSensorViewConfiguration();

  /*!
   * \brief Calculates the bounding box of an object from the osi information
   *
   * @param[in] dimension                       osi dimension
   * @param[in] position                        osi position
   * @param[in] orientation                     osi orientation
   * @return bounding box of object
   */
  static polygon_t CalculateBoundingBox(const osi3::Dimension3d& dimension,
                                        const osi3::Vector3d& position,
                                        const osi3::Orientation3d& orientation);

  /*!
   * \brief Translates the polygon from local coordinates to global coordinates
   *
   * @param[in] polygon                 polygon in local coordinates
   * @param[in] sensorPositionGlobal    position of the sensor in global coordinates
   * @param[in] yaw                     yaw of the sensor in global coordinates
   * @return polygon in global coordinates
   */
  polygon_t TransformPolygonToGlobalCoordinates(const polygon_t& polygon,
                                                point_t sensorPositionGlobal,
                                                double yaw) const;

  /*!
   * \brief Translates a point from global coordinates to local coordinates
   *
   * @param[in] point                   point in global coordinates
   * @param[in] sensorPositionGlobal    position of the sensor in global coordinates
   * @param[in] yaw                     yaw of the sensor in global coordinates
   * @return point in local coordinates
   */
  virtual point_t TransformPointToLocalCoordinates(point_t point, point_t sensorPositionGlobal, double yaw);

  /*!
   * \brief Calculates the position of the sensor in global coordinates
   *
   * @param[in] vehiclePosition     position of the vehicle in global coordinates
   * @param[in] yaw                 yaw of the vehicle in global coordinates
   * @return position of the sensor in global coordinates
   */
  point_t CalculateGlobalSensorPosition(point_t vehiclePosition, double yaw) const;

  /*!
   * \brief Calculates the relative velocity or acceleration between this vehicle and another in local vehicle coordinates
   *
   * @param[in] absolute    absolute velocity/acceleration of other vehicle in global coordinates
   * @param[in] own         absolute velocity/acceleration of own vehicle in global coordinates
   * @param[in] yaw         yaw of own vehicle in global coordinates
   * @return relative velocity/acceleration of other vehicle in vehicle coordinates
   */
  virtual point_t CalculateRelativeVector(const point_t absolute, const point_t own, double yaw);

  /**
   * @brief To find the MovingObject in the sensor view
   *
   * @param sensorView view of the sensor
   * @return Returns the MovingObject in the sensor view which was defined as host vehicle (by id)
   */
  static const osi3::MovingObject* FindHostVehicleInSensorView(const osi3::SensorView& sensorView);

  /**
   * @brief Get the Sensor Position object
   *
   * @return Returns the world coordinate position of the sensor
   */
  point_t GetSensorPosition() const;

  /// list of detected objects
  std::list<std::pair<int, osi3::SensorData>> detectedObjectsBuffer;

  /// previously detected objects with timestamps
  std::map<ObjectId, DelayInformation> delayInformations;

  /// osi sensor data
  osi3::SensorData sensorData;

  /// sensor position
  openpass::sensors::Position position;

  /// sensor id
  int id = 0;

  //! Probability object is not detected although it is visible
  double failureProbability{0.0};

  //! Sensor latency
  double latency{0.0};

  //! Sensor latency in ms
  int latencyInMs{0};

  //! Sensor delay in ms
  int detectionDelayInMs{0};

  //! time after which delay for undetected object starts anew in ms
  int maxDropOutTimeInMs{0};

private:
  static constexpr double MIN_FAILURE_PROBABILITY = 1e-12;

  /*!
   * \brief This method parses the initial parameters required by the module.
   *
   */
  void ParseBasicParameter();
};
