/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <string>
#include <units.h>

class AgentInterface;
class EgoAgentInterface;

//! Utility class for the sensor driver
class SensorDriverCalculations
{
public:
  /**
   * @brief Construct a new Sensor Driver Calculations object
   *
   * @param egoAgent Ego agent
   */
  SensorDriverCalculations(const EgoAgentInterface& egoAgent) : egoAgent(egoAgent) {}

  /**
   * @brief Get the Lateral Distance To Object object
   *
   * @param roadId        road of the object
   * @param otherObject   object to calculate distance to
   * @return lateral distance in lane
   */
  units::length::meter_t GetLateralDistanceToObject(const std::string& roadId, const AgentInterface* otherObject);

private:
  const EgoAgentInterface& egoAgent;
};
