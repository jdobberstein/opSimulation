/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <MantleAPI/Common/poly_line.h>
#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Traffic/control_strategy.h>
#include <memory>
#include <string>
#include <units.h>
#include <utility>
#include <vector>

#include "common/dynamicsSignal.h"
#include "common/globalDefinitions.h"
#include "common/vector2d.h"
#include "include/agentInterface.h"
#include "include/modelInterface.h"
#include "include/signalInterface.h"

class CallbackInterface;
class ParameterInterface;
class PublisherInterface;
class ScenarioControlInterface;
class StochasticsInterface;
class WorldInterface;

//! Class representing implementation of scenario dynamics
class DynamicsScenarioImplementation : public UnrestrictedControllStrategyModelInterface
{
public:
  //! Name of the current component
  const std::string COMPONENTNAME = "DynamicScenario";

  //! @brief Construct a new Dynamics Scenario Implementation object
  //!
  //! @param[in]     componentName     Name of the component
  //! @param[in]     isInit            Corresponds to "init" of "Component"
  //! @param[in]     priority          Corresponds to "priority" of "Component"
  //! @param[in]     offsetTime        Corresponds to "offsetTime" of "Component"
  //! @param[in]     responseTime      Corresponds to "responseTime" of "Component"
  //! @param[in]     cycleTime         Corresponds to "cycleTime" of "Component"
  //! @param[in]     stochastics       Pointer to the stochastics class loaded by the framework
  //! @param[in]     world             Pointer to the world
  //! @param[in]     parameters        Pointer to the parameters of the module
  //! @param[in]     publisher         Pointer to the publisher instance
  //! @param[in]     callbacks         Pointer to the callbacks
  //! @param[in]     agent             Pointer to agent instance
  //! @param[in]     scenarioControl   scenarioControl of entity
  DynamicsScenarioImplementation(std::string componentName,
                                 bool isInit,
                                 int priority,
                                 int offsetTime,
                                 int responseTime,
                                 int cycleTime,
                                 StochasticsInterface *stochastics,
                                 WorldInterface *world,
                                 const ParameterInterface *parameters,
                                 PublisherInterface *const publisher,
                                 const CallbackInterface *callbacks,
                                 AgentInterface *agent,
                                 std::shared_ptr<ScenarioControlInterface> scenarioControl)
      : UnrestrictedControllStrategyModelInterface(std::move(componentName),
                                                   isInit,
                                                   priority,
                                                   offsetTime,
                                                   responseTime,
                                                   cycleTime,
                                                   stochastics,
                                                   world,
                                                   parameters,
                                                   publisher,
                                                   callbacks,
                                                   agent,
                                                   std::move(scenarioControl)),
        dynamicsSignal{ComponentState::Acting},
        velocityToKeep{GetAgent()->GetVelocity().Length()}
  {
  }
  DynamicsScenarioImplementation(const DynamicsScenarioImplementation &) = delete;
  DynamicsScenarioImplementation(DynamicsScenarioImplementation &&) = delete;
  DynamicsScenarioImplementation &operator=(const DynamicsScenarioImplementation &) = delete;
  DynamicsScenarioImplementation &operator=(DynamicsScenarioImplementation &&) = delete;
  virtual ~DynamicsScenarioImplementation() = default;

  /*!
   * \brief Update Inputs
   *
   * Function is called by framework when another component delivers a signal over
   * a channel to this component (scheduler calls update taks of other component).
   *
   * Refer to module description for input channels and input ids.
   *
   * @param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
   * @param[in]     data           Referenced signal (copied by sending component)
   * @param[in]     time           Current scheduling time
   */
  virtual void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time);

  /*!
   * \brief Update outputs.
   *
   * Function is called by framework when this component has to deliver a signal over
   * a channel to another component (scheduler calls update task of this component).
   *
   * Refer to module description for output channels and output ids.
   *
   * @param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
   * @param[out]    data           Referenced signal (copied by this component)
   * @param[in]     time           Current scheduling time
   */
  virtual void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time);

  /*!
   * \brief Process data within component.
   *
   * Function is called by framework when the scheduler calls the trigger task
   * of this component
   *
   * @param[in]     time           Current scheduling time
   */
  virtual void Trigger(int time);

private:
  units::velocity::meters_per_second_t GetVelocityFromSplines();

  mantle_api::PolyLine CalculateSinusiodalLaneChange(
      const std::shared_ptr<const mantle_api::PerformLaneChangeControlStrategy> &laneChange);

  DynamicsInformation ReadWayPointData();

  DynamicsInformation CalculateTrajectoryWithExternalVelocity(units::velocity::meters_per_second_t velocity);

  //! Output Signal
  DynamicsSignal dynamicsSignal;

  mantle_api::ControlStrategyType longitudinalStrategy{mantle_api::ControlStrategyType::kKeepVelocity};
  mantle_api::ControlStrategyType lateralStrategy{mantle_api::ControlStrategyType::kKeepLaneOffset};

  units::time::second_t timeLongitudinalStrategy{0.0_s};
  units::time::second_t timeLateralStrategy{0.0_s};

  //! Parameters of the control strategies
  units::velocity::meters_per_second_t velocityToKeep;
  std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines;
  mantle_api::PolyLine trajectory;
  bool externalVelocity{false};
  std::vector<mantle_api::PolyLinePoint>::iterator currentTrajectoryPoint;
  units::dimensionless::scalar_t percentageTraveledBetweenCoordinates;
};
