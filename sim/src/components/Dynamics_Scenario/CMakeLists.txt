################################################################################
# Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_NAME Dynamics_Scenario)

add_compile_definitions(DYNAMICS_SCENARIO_LIBRARY)

add_openpass_target(
  NAME ${COMPONENT_NAME} TYPE library LINKAGE shared COMPONENT module

  HEADERS
    dynamics_scenario.h
    src/dynamics_scenario_implementation.h

  SOURCES
    dynamics_scenario.cpp
    src/dynamics_scenario_implementation.cpp

  LIBRARIES
    MantleAPI::MantleAPI
)
