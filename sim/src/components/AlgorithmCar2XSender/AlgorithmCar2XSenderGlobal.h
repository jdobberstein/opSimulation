/********************************************************************************
 * Copyright (c) 2018 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  AlgorithmCar2XSenderGlobal.h
 *	@brief This file contains DLL export declarations
 **/
//-----------------------------------------------------------------------------

#pragma once

#include "sim/src/common/opExport.h"

#if defined(ALGORITHM_CAR_TO_X_SENDER_LIBRARY)
#define ALGORITHM_CAR_TO_X_SENDER_SHARED_EXPORT OPEXPORT  //!< Export of the dll-functions
#else
#define ALGORITHM_CAR_TO_X_SENDER_SHARED_EXPORT OPIMPORT  //!< Import of the dll-functions
#endif
