/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "OSMPConnectorBase.h"
#include "components/Algorithm_FmuWrapper/src/FmuHelper.h"

namespace ssp
{

/// @brief Class representing a OSMP connector
/// @tparam OsiType
/// @tparam FMI
template <typename OsiType, size_t FMI>
class OsmpConnector : public OSMPConnectorBase
{
public:
  /**
   * @brief Construct a new Osmp Connector object
   *
   * @param connectorName Name of the connector which is relevant for the ssd system
   * @param osmpLinkName  Name in the annotation which joins the triple of connectors that form a OSI Connector
   * @param fmuWrapper    Pointer to the FMU Wrapper interface
   * @param priority      Priority of the FMU component
   */
  OsmpConnector(const std::string &connectorName,
                const std::string &osmpLinkName,
                std::shared_ptr<FmuWrapperInterface> fmuWrapper,
                int priority);
  ~OsmpConnector() override = default;

  static_assert(std::is_base_of_v<google::protobuf::Message, OsiType>);

  static_assert(is_same_as_any_of_v<OsiType,
                                    osi3::SensorView,
                                    osi3::SensorViewConfiguration,
                                    osi3::SensorData,
                                    osi3::GroundTruth,
                                    osi3::TrafficCommand,
                                    osi3::TrafficUpdate,
                                    osi3::HostVehicleData>,
                "The provided type is not an supported osi type.");

  static_assert(is_same_as_any_of_variant_v<OsiType, SupportedOsiTypes>,
                "The provided type is not an supported osi type.");

  /**
   * @brief Get the Traces Map object
   *
   * @return targetOutputTracesMap
   */
  std::optional<std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>>> GetTracesMap() const
  {
    return targetOutputTracesMap;
  }

  std::shared_ptr<google::protobuf::Message> GetMessage() final;

  void SetMessage(const google::protobuf::Message *const message) final;

  /// @brief Write output parameters
  /// @param updateOutputParameters   List of parameters to update output
  /// @param outDir                   Output directory path
  /// @param outputTraceMap           Reference to trace output map
  /// @param componentName            Name of the component
  void ApplyWriteOutputParameters(
      const std::vector<std::pair<std::string, std::string>> &updateOutputParameters,
      const std::filesystem::path &outDir,
      const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> outputTraceMap,
      const std::string &componentName);

  /// @brief Set a write json object
  /// @param outDir           Name of the output directory path
  /// @param componentName    Name of the component
  void SetWriteJson(const std::filesystem::path &outDir, const std::string &componentName);

  /// @brief Set a write binary trace object
  /// @param outDir                   Name of the output directory path
  /// @param targetOutputTracesMap    Reference to the map of target output traces
  /// @param componentName            Name of the component
  void SetWriteBinaryTrace(
      const std::filesystem::path &outDir,
      const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetOutputTracesMap,
      const std::string &componentName);

private:
  std::string serializedOsiMessage;
  std::optional<std::filesystem::path> jsonOutDir;
  std::optional<std::string> componentName;
  std::optional<std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>>> targetOutputTracesMap;

  void HandleFileWriting(int time) final;
  void HandleWriteJson(int time);
  void HandleWriteBinaryTrace(int time);
};

template <typename OsiType, size_t FMI>
void OsmpConnector<OsiType, FMI>::HandleWriteBinaryTrace(int time)
{
  if (targetOutputTracesMap.has_value())
  {
    LOGDEBUG("SSP OSMP connector: Write binary trace");

    auto message = this->GetMessage();
    auto strMessage = message->SerializeAsString();
    LOGDEBUG("SSP OSMP connector: Write binary trace messag");

    std::string osiType = StringFromOsiType<OsiType>();
    LOGDEBUG("SSP OSMP connector: Write binary trace osi type -> " + osiType);

    FmuFileHelper::WriteBinaryTrace(strMessage,
                                    GetOsmpLinkName(),
                                    componentName.value_or("unknown"),
                                    time,
                                    osiType,
                                    *(targetOutputTracesMap.value()));
  }
}
template <typename OsiType, size_t FMI>
void OsmpConnector<OsiType, FMI>::HandleWriteJson(int time)
{
  if (jsonOutDir.has_value())
  {
    LOGDEBUG("SSP OSMP connector: Write json");

    const auto message = this->GetMessage();
    LOGDEBUG("SSP OSMP connector: Write json message");

    std::string fileName = GetOsmpLinkName() + "_" + std::to_string(time) + ".json";
    LOGDEBUG("SSP OSMP connector: Write json filename -> " + fileName);

    FmuFileHelper::WriteJson(*message, fileName, this->jsonOutDir.value());
  }
}
template <typename OsiType, size_t FMI>
void OsmpConnector<OsiType, FMI>::HandleFileWriting(int time)
{
  HandleWriteJson(time);
  HandleWriteBinaryTrace(time);
}

template <typename OsiType, size_t FMI>
void OsmpConnector<OsiType, FMI>::SetWriteBinaryTrace(
    const std::filesystem::path &outDir,
    const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &pTargetOutputTracesMap,
    const std::string &traceComponentName)
{
  LOGDEBUG("SSP OSMP connector: Set WriteBinaryTrace for " + traceComponentName);

  this->componentName = traceComponentName;
  this->targetOutputTracesMap = pTargetOutputTracesMap;
}

template <typename OsiType, size_t FMI>
void OsmpConnector<OsiType, FMI>::SetWriteJson(const std::filesystem::path &outDir, const std::string &componentName)
{
  LOGDEBUG("SSP OSMP connector: Set WriteJson for " + componentName);

  this->componentName = componentName;
  jsonOutDir = FmuFileHelper::CreateOrOpenOutputFolder(outDir, componentName, "JsonFiles");
}

template <typename OsiType, size_t FMI>
OsmpConnector<OsiType, FMI>::OsmpConnector(const std::string &connectorName,
                                           const std::string &osmpLinkName,
                                           std::shared_ptr<FmuWrapperInterface> fmuWrapper,
                                           int priority)
    : OSMPConnectorBase(connectorName, osmpLinkName, fmuWrapper, priority)
{
}

template <typename OsiType, size_t FMI>
void OsmpConnector<OsiType, FMI>::ApplyWriteOutputParameters(
    const std::vector<std::pair<std::string, std::string>> &updateOutputParameters,
    const std::filesystem::path &outDir,
    const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> outputTraceMap,
    const std::string &componentName)
{
  for (auto &updateOutputParameter : updateOutputParameters)
  {
    if (updateOutputParameter.first.substr(0, updateOutputParameter.first.find('_')) == "WriteJson")
    {
      if (updateOutputParameter.second == GetOsmpLinkName())
      {
        SetWriteJson(outDir, componentName);
      }
    }
    else if (updateOutputParameter.first.substr(0, updateOutputParameter.first.find('_')) == "WriteTrace")
    {
      if (updateOutputParameter.second == GetOsmpLinkName())
      {
        SetWriteBinaryTrace(outDir, outputTraceMap, componentName);
      }
    }
  }
}

template <typename OsiType, size_t FMI>
std::shared_ptr<google::protobuf::Message> OsmpConnector<OsiType, FMI>::GetMessage()
{
  auto osiMessage = std::make_shared<OsiType>();
  auto memorySize = size->GetValue();
  auto base_high = base_hi->GetValue();
  auto base_low = base_lo->GetValue();

  fmi_integer_t fmiIntHi;
  fmiIntHi.emplace<FMI>(base_high);
  fmi_integer_t fmiIntLo;
  fmiIntLo.emplace<FMI>(base_low);

  osiMessage->ParseFromArray(FmuHelper::decode_integer_to_pointer<FMI>(fmiIntHi, fmiIntLo), memorySize);

  std::string outputString;
  google::protobuf::util::JsonPrintOptions options;
  options.add_whitespace = true;
  google::protobuf::util::MessageToJsonString(*osiMessage, &outputString, options);

  return osiMessage;
}

template <typename OsiType, size_t FMI>
void OsmpConnector<OsiType, FMI>::SetMessage(const google::protobuf::Message *const message)
{
  if (!message) return;

  if (!dynamic_cast<const OsiType *const>(message))
  {
    LOGERRORANDTHROW("Try to handle unknown message type in OSMP Connector");
  }

  message->SerializeToString(&serializedOsiMessage);
  fmi_integer_t base_low{}, base_high{};
  FmuHelper::encode_pointer_to_integer<FMI>(serializedOsiMessage.data(), base_high, base_low);

  base_lo->SetValue(std::get<FMI>(base_low));
  base_hi->SetValue(std::get<FMI>(base_high));
  size->SetValue(serializedOsiMessage.length());
}

}  //namespace ssp