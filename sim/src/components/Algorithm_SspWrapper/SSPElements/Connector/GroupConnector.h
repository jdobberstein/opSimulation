/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "Connector.h"

namespace ssp
{

/// Class representing a group connector
class GroupConnector : public Connector
{
public:
  GroupConnector() = default;
  ~GroupConnector() override = default;

  /**
   * @brief Construct a new Group Connector object
   *
   * @param connectorList List of all outer accessible connectors
   */
  explicit GroupConnector(const std::vector<std::shared_ptr<ConnectorInterface>> &connectorList);

  void Accept(ConnectorVisitorInterface &visitor) override;

  const std::string &GetConnectorName() const override;
};

}  //namespace ssp