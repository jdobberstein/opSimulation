/*******************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "SspTriggerVisitor.h"

#include "../Connector/PropagateDataVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Component.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/System.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/SspVisitorHelper.h"

void ssp::SspTriggerVisitor::Visit(const System *ssdSystem)
{
  LOGDEBUG("SSP Network Trigger Visitor: Visit System " + ssdSystem->elementName);
  SSPVisitorHelper::PriorityAcceptVisitableNetworkElements(*this, ssdSystem->elements);
}

void ssp::SspTriggerVisitor::Visit(const FmuComponent *component)
{
  LOGDEBUG("SSP Network Trigger Visitor: Visit FMU component " + component->elementName);
  component->fmuWrapperInterface->Trigger(this->time);

  ssp::PropagateDataVisitor propagateDataVisitor{this->time};
  for (const auto &connector : component->output_connectors)
  {
    if (!connector->IsParameterConnector())
    {
      connector->Accept(propagateDataVisitor);
    }
  }
}

ssp::SspTriggerVisitor::SspTriggerVisitor(int time) : time(time) {}

void ssp::SspTriggerVisitor::Visit(const SspComponent *)
{
  LOGDEBUG("SSP Network Trigger Visitor: Visit SSP Component");
  LOGWARN("SSP Network Trigger Visitor: Visit SSP Component not implemented");
}

void ssp::SspTriggerVisitor::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}