/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "../SspVisitorHelper.h"
#include "ConnectorVisitorInterface.h"

namespace ssp
{

/// Class representing a visitor to propagate data
class PropagateDataVisitor : public ConnectorVisitorInterface
{
public:
  ~PropagateDataVisitor() override = default;

  /// @brief Visitor function to update output
  /// @param scalarConnector Pointer to the scalar connector base
  void Visit(ScalarConnectorBase *scalarConnector) override;

  /// @brief Visitor function to update output
  /// @param groupConnector Reference to the group connector
  void Visit([[maybe_unused]] GroupConnector &groupConnector) override;

  /// @brief Visitor function to update output
  /// @param osmpConnector Reference to OSMP Connector base
  void Visit(OSMPConnectorBase *osmpConnectorBase) override;

  /// @param time Propagate data visitor constructor with time
  explicit PropagateDataVisitor(int time);
  PropagateDataVisitor() = delete;

  int time;  ///< time

protected:
  //-------------------------------------------------------------------------
  //! Provides callback to LOG() macro.
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-------------------------------------------------------------------------
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const override;
};
}  //namespace ssp