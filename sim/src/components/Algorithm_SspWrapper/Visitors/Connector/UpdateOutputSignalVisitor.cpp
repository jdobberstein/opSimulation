/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "UpdateOutputSignalVisitor.h"

ssp::UpdateOutputSignalVisitor::UpdateOutputSignalVisitor(const int localLinkId,
                                                          std::shared_ptr<const SignalInterface> &data,
                                                          const int time,
                                                          WorldInterface *world,
                                                          AgentInterface *agent,
                                                          const CallbackInterface *callbacks)
    : localLinkId(localLinkId), data(data), time(time), world(world), agent(agent), callbacks(callbacks)
{
}

void ssp::UpdateOutputSignalVisitor::Visit(ssp::ScalarConnectorBase *connector)
{
  LOGDEBUG("SSP Output Signal Visitor: Visit FMU connector " + connector->GetConnectorName());
  data = std::make_shared<DoubleSignal const>(ConnectorHelper::GetFmuWrapperValue<VariableTypeDouble>(
                                                  connector->fmuWrapperInterface, connector->fmuScalarVariableName),
                                              ComponentState::Acting);
}

void ssp::UpdateOutputSignalVisitor::Visit(ssp::GroupConnector &groupConnector)
{
  LOGDEBUG("SSP Output Signal Visitor: Visit system connector ");           //+ connector->GetConnectorName());
  LOGDEBUG("SSP priority queue of system connector will now be handled.");  //+ connector.GetConnectorName() + "will no
                                                                            //be handled.");
  SSPVisitorHelper::PriorityAccept(groupConnector, *this);
}

void ssp::UpdateOutputSignalVisitor::Visit(ssp::OSMPConnectorBase *connector)
{
  if (connector->IsParameterConnector())
  {
    LOGDEBUG(connector->GetConnectorName() + " is a parameter Connector, UpdateOutput Skipped");
    return;
  }

  LOGDEBUG("SSP Output Signal Visitor: Visit OSMP connector " + connector->GetConnectorName());
  connector->GetFmuWrapperInterface()->UpdateOutput(localLinkId, data, time);
  if (data)
  {
    connector->HandleFileWriting(this->time);
  }
}

void ssp::UpdateOutputSignalVisitor::Log(CbkLogLevel logLevel,
                                         const char *file,
                                         int line,
                                         const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}