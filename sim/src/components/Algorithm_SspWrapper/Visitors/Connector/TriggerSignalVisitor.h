/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "../SspVisitorHelper.h"
#include "ConnectorVisitorInterface.h"
#include "UpdateInputSignalVisitor.h"
#include "UpdateOutputSignalVisitor.h"

namespace ssp
{

///< Class representing a visitor for trigger signal
class TriggerSignalVisitor : public ConnectorVisitorInterface
{
public:
  /// TriggerSignalVisitor constructor with time
  /// @param time       current time
  /// @param world      world
  /// @param agent      ego agent
  /// @param callbacks  callbacks for logging
  explicit TriggerSignalVisitor(const int time,
                                WorldInterface *world,
                                AgentInterface *agent,
                                const CallbackInterface *callbacks);

  virtual ~TriggerSignalVisitor() = default;

  const int time;  ///< time

  /// @brief Visitor function to update output
  /// @param connector Pointer to the scalar connector base
  void Visit(ScalarConnectorBase *connector) override;

  /// @brief Visitor function to update output
  /// @param groupConnector Reference to the group connector
  void Visit(GroupConnector &groupConnector) override;

  /// @brief Visitor function to update output
  /// @param osmpConnector Reference to OSMP Connector base
  void Visit(OSMPConnectorBase *osmpConnector) override;

  std::vector<std::weak_ptr<FmuWrapperInterface>> alreadyTriggered{};  ///< List of triggered pointers

protected:
  //-------------------------------------------------------------------------
  //! Provides callback to LOG() macro.
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-------------------------------------------------------------------------
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const override;

private:
  WorldInterface *world;
  AgentInterface *agent;
  const CallbackInterface *callbacks;
};
}  //namespace ssp