/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <vector>

#include "FileElements/SsdSystem.h"

struct SsdFile  ///< Information of an SsdFile
{
  /// @brief Constructor for an SSD
  /// @param fileName     Name of the file
  /// @param ssdSystem    Reference to the Ssd System
  SsdFile(std::filesystem::path fileName, std::shared_ptr<SsdSystem> ssdSystem);
  const std::filesystem::path fileName;        ///< Name of the file
  const std::shared_ptr<SsdSystem> ssdSystem;  ///< Reference to the Ssd System
};
