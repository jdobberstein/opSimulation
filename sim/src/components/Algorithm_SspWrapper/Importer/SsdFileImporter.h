/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <filesystem>
#include <iostream>
#include <libxml/tree.h>
#include <map>
#include <memory>
#include <string>

#include "SsdFile.h"
#include "common/parameter.h"
#include "common/xmlParser.h"
#include "include/modelInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/SspLogger.h"
#include "src/core/opSimulation/modelElements/parameters.h"

/// @brief Class representing an importer of a ssd file
class SsdFileImporter
{
public:
  SsdFileImporter(const SsdFileImporter &) = delete;

  SsdFileImporter(SsdFileImporter &&) = delete;

  SsdFileImporter &operator=(const SsdFileImporter &) = delete;

  SsdFileImporter &operator=(SsdFileImporter &&) = delete;

  /// @brief Import SSD file
  /// @param filename     Name of the file
  /// @param ssdFile      Reference to the Ssd file
  /// @return true, when the import is successful
  static bool Import(const std::filesystem::path &filename, std::vector<std::shared_ptr<SsdFile>> &ssdFile);

private:
  static xmlDocPtr ImportSsdFileContent(const std::filesystem::path &filename);

  static void ImportSystemComponents(xmlNodePtr componentsElement,
                                     const std::shared_ptr<SsdSystem> &ssdSystem,
                                     const std::filesystem::path &filename);

  static SspParserTypes::Enumerations ImportSsdEnumerations(xmlNodePtr enumerationsElement);

  static void ImportComponentParameters(xmlNodePtr parameterBindingsElement,
                                        const std::shared_ptr<SsdComponent> &component,
                                        const std::filesystem::path &filename);

  static void ImportComponentAnnotations(xmlNodePtr annotationsElement, const std::shared_ptr<SsdComponent> &component);

  static void ImportComponentParameterSets(xmlNodePtr parameterBindingsElement,
                                           const std::shared_ptr<SsdComponent> &component);

  static void ImportFmuParameters(xmlNodePtr parameterBindingsElement, const std::shared_ptr<SsdComponent> &component);

  static void ImportWriteMessageParameters(xmlNodePtr parameterBindingsElement,
                                           const std::shared_ptr<SsdComponent> &component);

  static void ImportParameterConnectorInitialization(xmlNodePtr parameterBindingsElement,
                                                     const std::shared_ptr<SsdComponent> &component);

  template <class T>
  static void ImportConnectors(xmlNodePtr element,
                               const std::shared_ptr<T> &sharedPtr,
                               const std::shared_ptr<SsdSystem> &ssdSystem);

  static void ImportSystemConnections(xmlNodePtr connectionsElement, const std::shared_ptr<SsdSystem> &ssdSystem);

  static void ImportSystem(const std::filesystem::path &filename,
                           xmlNodePtr systemElement,
                           const std::shared_ptr<SsdSystem> &ssdSystem);

  static SspParserTypes::ConnectorTypeAndAttributes FetchConnectorType(xmlNodePtr connectorElement,
                                                                       const std::string &connectorName);

  static void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message);
};

//OPENPASS_SSDFILEIMPORTER_H
