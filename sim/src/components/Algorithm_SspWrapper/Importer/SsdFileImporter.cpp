/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SsdFileImporter.h"

#include <filesystem>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <regex>
#include <utility>

#include "FileElements/SsdURI.h"
#include "common/xmlParser.h"
#include "components/Algorithm_SspWrapper/OSMPConnectorFactory.h"

using namespace SimulationCommon;

SspParserTypes::ConnectorTypeAndAttributes SsdFileImporter::FetchConnectorType(xmlNodePtr connectorElement,
                                                                               const std::string &connectorName)
{
  xmlNodePtr typeElement = GetFirstChildElement(connectorElement, "Real");
  if (typeElement)
  {
    LOGDEBUG("Parse Real Connector from connector element " + connectorName);
    return {ConnectorType::Real, {}};
  }
  typeElement = GetFirstChildElement(connectorElement, "Integer");
  if (typeElement)
  {
    LOGDEBUG("Parse Integer Connector from connector element " + connectorName);
    return {ConnectorType::Integer, {}};
  }
  typeElement = GetFirstChildElement(connectorElement, "Boolean");
  if (typeElement)
  {
    LOGDEBUG("Parse Boolean Connector from connector element " + connectorName);
    return {ConnectorType::Boolean, {}};
  }
  typeElement = GetFirstChildElement(connectorElement, "Enumeration");
  if (typeElement)
  {
    std::string enumerationName{};
    if (ParseAttribute(typeElement, "name", enumerationName))
    {
      LOGDEBUG("Parse Enumeration Connector from connector element " + connectorName);
      return {ConnectorType::Enumeration, {{"name", enumerationName}}};
    }
    LogErrorAndThrow("All Enumeration Connectors require the name attribute, " + connectorName + " missing name");
  }

  LOGDEBUG("Parse Connector from connector element " + connectorName);
  return {ConnectorType::None, {}};
}

template <class T>
void SsdFileImporter::ImportConnectors(xmlNodePtr connectorsElement,
                                       const std::shared_ptr<T> &ssdComponent,
                                       const std::shared_ptr<SsdSystem> &ssdSystem)
{
  xmlNodePtr connectorElement = GetFirstChildElement(connectorsElement, "Connector");
  if (!connectorElement)
  {
    LOGERRORANDTHROW("SSP Importer: Unable to retrieve connectors.");
  }
  while (connectorElement)
  {
    if (xmlStrEqual(connectorElement->name, toXmlChar("Connector")))
    {
      bool isOSMP = false;
      std::smatch typeMatch;
      std::string connectorName, binaryVariableName, kind, role, mimeType;

      ParseAttribute(connectorElement, "name", connectorName);
      ParseAttribute(connectorElement, "kind", kind);

      xmlNodePtr annotationsElement = GetFirstChildElement(connectorElement, "Annotations");
      if (annotationsElement)
      {
        xmlNodePtr annotationElement = GetFirstChildElement(annotationsElement, "Annotation");
        if (annotationElement)
        {
          while (annotationElement)
          {
            if (xmlStrEqual(annotationElement->name, toXmlChar("Annotation")))
            {
              std::string annotationType;
              ParseAttribute(annotationElement, "type", annotationType);

              if (annotationType == "net.pmsf.osmp")
              {
                isOSMP = true;
                xmlNodePtr binaryVariable = GetFirstChildElement(annotationElement, "osmp-binary-variable");
                if (binaryVariable)
                {
                  ParseAttribute(binaryVariable, "name", binaryVariableName);
                  ParseAttribute(binaryVariable, "role", role);
                  ParseAttribute(binaryVariable, "mime-type", mimeType);
                }
                else
                {
                  LOGWARN("SSP Importer: Parse unknown annotation: ignore " + annotationType);
                }
              }
            }
            annotationElement = xmlNextElementSibling(annotationElement);
          }
        }
      }
      if (isOSMP)
      {
        std::regex typeRegex{"type=([a-zA-Z]*);", std::regex::optimize};
        if (std::regex_search(mimeType, typeMatch, typeRegex))
        {
          auto connector = SspParserTypes::OSMPSingleLink{ssdComponent->GetName(),
                                                          {connectorName, binaryVariableName, role, typeMatch[1]}};
          ssdComponent->EmplaceConnector(std::move(kind), connector);
        }
        else
        {
          LOGERRORANDTHROW("SSP Importer: Unable to parse osi type from mime-type: " + mimeType);
        }
      }
      else
      {
        //Scalar Connector, because not OSMP type

        SspParserTypes::ConnectorTypeAndAttributes connectorParams
            = FetchConnectorType(connectorElement, connectorName);
        if (connectorParams.first == ConnectorType::Enumeration)
        {
          auto enumName = connectorParams.second["name"];
          if (ssdSystem->HasEnumeration(enumName))
          {
            ssdComponent->EmplaceConnector(std::move(kind),
                                           SspParserTypes::ScalarConnector{connectorName, connectorParams});
          }
          else
          {
            std::string logMsg = "SSP Importer: Could not create connector ";
            LOGWARN(logMsg.append(connectorName)
                        .append(" because system enumerations has no enum type ")
                        .append(enumName)
                        .append("."));
          }
        }
        else
        {
          ssdComponent->EmplaceConnector(std::move(kind),
                                         SspParserTypes::ScalarConnector{connectorName, connectorParams});
        }
      }
    }
    connectorElement = xmlNextElementSibling(connectorElement);
  }
}

void SsdFileImporter::ImportComponentParameterSets(xmlNodePtr parameterSetElement,
                                                   const std::shared_ptr<SsdComponent> &component)
{
  xmlNodePtr parametersElement = GetFirstChildElement(parameterSetElement, "Parameters");
  if (!parametersElement)
  {
    LOGERRORANDTHROW("SSP Importer: Unable to retrieve parameters.");
  }
  while (parameterSetElement)
  {
    if (xmlStrEqual(parameterSetElement->name, toXmlChar("ParameterSet")))
    {
      std::string name;
      ParseAttribute(parameterSetElement, "name", name);
      if (name == "FmuParameters")
      {
        ImportFmuParameters(parameterSetElement, component);
      }
      else if (name == "WriteMessageParameters")
      {
        ImportWriteMessageParameters(parameterSetElement, component);
      }
      else if (name == "ParameterConnectorInitialization")
      {
        ImportParameterConnectorInitialization(parameterSetElement, component);
      }
      else
      {
        LOGINFO("SSP Importer: Ignoring unknown parameterSet: " + name);
      }
    }
    parameterSetElement = xmlNextElementSibling(parameterSetElement);
  }
}

void SsdFileImporter::ImportFmuParameters(xmlNodePtr parameterSetElement,
                                          const std::shared_ptr<SsdComponent> &component)
{
  xmlNodePtr parametersElement = GetFirstChildElement(parameterSetElement, "Parameters");
  if (!parametersElement)
  {
    LOGERRORANDTHROW("SSP Importer: Unable to retrieve FmuParameters.");
  }

  xmlNodePtr parameterElement = GetFirstChildElement(parametersElement, "Parameter");
  if (parameterElement)
  {
    openpass::parameter::internal::ParameterSetLevel3 parameters;

    while (parameterElement)
    {
      if (xmlStrEqual(parameterElement->name, toXmlChar("Parameter")))
      {
        std::string parameterName;
        ParseAttribute(parameterElement, "name", parameterName);

        xmlNodePtr valueElement = GetFirstChildElement(parameterElement, "String");
        std::string value;
        if (valueElement)
        {
          ParseAttribute(valueElement, "value", value);
          parameters.emplace_back(parameterName, value);
        }
        valueElement = GetFirstChildElement(parameterElement, "Real");
        if (valueElement)
        {
          ParseAttribute(valueElement, "value", value);
          parameters.emplace_back(parameterName, atof(value.c_str()));
        }
        valueElement = GetFirstChildElement(parameterElement, "Integer");
        if (valueElement)
        {
          ParseAttribute(valueElement, "value", value);
          parameters.emplace_back(parameterName, atoi(value.c_str()));
        }
        valueElement = GetFirstChildElement(parameterElement, "Boolean");
        if (valueElement)
        {
          ParseAttribute(valueElement, "value", value);

          bool valueRobust = false;
          if (value == "true")
          {
            valueRobust = true;
          }
          parameters.emplace_back(parameterName, valueRobust);
        }
      }
      parameterElement = xmlNextElementSibling(parameterElement);
    }
    component->SetParameters(parameters);
  }
}

void SsdFileImporter::ImportWriteMessageParameters(xmlNodePtr parameterSetElement,
                                                   const std::shared_ptr<SsdComponent> &component)
{
  xmlNodePtr parametersElement = GetFirstChildElement(parameterSetElement, "Parameters");

  if (!parametersElement)
  {
    LOGERRORANDTHROW("SSP Importer: Unable to retrieve WriteMessageParameters.");
  }

  xmlNodePtr parameterElement = GetFirstChildElement(parametersElement, "Parameter");
  if (parameterElement)
  {
    std::vector<std::pair<std::string, std::string>> parameters;

    while (parameterElement)
    {
      if (xmlStrEqual(parameterElement->name, toXmlChar("Parameter")))
      {
        std::string parameterName;
        ParseAttribute(parameterElement, "name", parameterName);

        xmlNodePtr valueElement = GetFirstChildElement(parameterElement, "String");
        if (valueElement)
        {
          std::string value;
          ParseAttribute(valueElement, "value", value);
          parameters.emplace_back(std::make_pair(parameterName, value));
        }
      }
      parameterElement = xmlNextElementSibling(parameterElement);
    }
    component->SetWriteMessageParameters(parameters);
  }
}

void SsdFileImporter::ImportParameterConnectorInitialization(xmlNodePtr parameterSetElement,
                                                             const std::shared_ptr<SsdComponent> &component)
{
  std::vector<ParameterConnectorInitialization> connectorInit{};
  xmlNodePtr parametersElement = GetFirstChildElement(parameterSetElement, "Parameters");
  xmlNodePtr parameterElement = GetFirstChildElement(parametersElement, "Parameter");
  xmlNodePtr enumerationsElement = GetFirstChildElement(parameterSetElement, "Enumerations");
  std::optional<SspParserTypes::Enumerations> initEnumerations;

  if (enumerationsElement)
  {
    initEnumerations = ImportSsdEnumerations(enumerationsElement);
  }

  if (parametersElement)
  {
    if (parameterElement)
    {
      while (parameterElement)
      {
        if (xmlStrEqual(parameterElement->name, toXmlChar("Parameter")))
        {
          std::string connectorName;
          ParseAttribute(parameterElement, "name", connectorName);

          xmlNodePtr valueElement = GetFirstChildElement(parameterElement, "Real");
          std::string value;
          if (valueElement)
          {
            auto connectorType = ConnectorType::Real;
            ParseAttribute(valueElement, "value", value);
            connectorInit.emplace_back(connectorName, connectorType, value, component->GetName());
          }
          valueElement = GetFirstChildElement(parameterElement, "Integer");
          if (valueElement)
          {
            auto connectorType = ConnectorType::Integer;
            ParseAttribute(valueElement, "value", value);
            connectorInit.emplace_back(connectorName, connectorType, value, component->GetName());
          }
          valueElement = GetFirstChildElement(parameterElement, "Boolean");
          if (valueElement)
          {
            auto connectorType = ConnectorType::Boolean;
            ParseAttribute(valueElement, "value", value);
            connectorInit.emplace_back(connectorName, connectorType, value, component->GetName());
          }
          valueElement = GetFirstChildElement(parameterElement, "Enumeration");
          if (valueElement)
          {
            auto connectorType = ConnectorType::Enumeration;
            xmlNodePtr nameElement{};
            std::string itemName;
            if (initEnumerations.has_value())
            {
              ParseAttribute(valueElement, "value", value);
              ParseAttribute(valueElement, "name", itemName);
              auto enumerationValue
                  = SSPParserHelper::GetValueFromEnumeration(itemName, value, initEnumerations.value());
              if (enumerationValue.has_value())
              {
                connectorInit.emplace_back(
                    connectorName, connectorType, enumerationValue.value_or(""), component->GetName());
              }
              else
              {
                std::string logMsg = "Enumeration Item: ";
                LOGWARN(
                    logMsg.append(value).append(" not found in enumeration: ").append(itemName).append(", skipping"));
              }
            }
            else
            {
              LOGWARN(
                  "Enumeration init parameter can't be processed without enumerations tag in ParameterSet, skipping");
            }
          }
        }
        parameterElement = xmlNextElementSibling(parameterElement);
      }
    }
    component->SetConnectorInitializations(std::move(connectorInit));
  }
}

void SsdFileImporter::ImportComponentParameters(xmlNodePtr parameterBindingsElement,
                                                const std::shared_ptr<SsdComponent> &component,
                                                const std::filesystem::path &filename)
{
  xmlNodePtr parameterBindingElement = GetFirstChildElement(parameterBindingsElement, "ParameterBinding");
  if (!parameterBindingElement)
  {
    LOGERRORANDTHROW("SSP Importer: Unable to retrieve parameters.");
  }
  while (parameterBindingElement)
  {
    if (xmlStrEqual(parameterBindingElement->name, toXmlChar("ParameterBinding")))
    {
      if (xmlGetProp(parameterBindingElement, toXmlChar("source")))
      {
        SsdURI ssvSourceElement;
        ParseAttribute(parameterBindingElement, "source", ssvSourceElement);
        std::filesystem::path ssvSource(filename);
        std::filesystem::path ssvSourceFolder(ssvSourceElement.Path());
        if (!ssvSourceFolder.is_absolute())
        {
          ssvSource = ssvSource.parent_path() / ssvSourceFolder;
        }
        xmlDocPtr document = ImportSsdFileContent(ssvSource);

        xmlNodePtr documentRoot = xmlDocGetRootElement(document);
        if (documentRoot == nullptr)
        {
          ImportComponentParameterSets(documentRoot, component);
        }
        xmlFreeDoc(document);
      }
      else
      {
        xmlNodePtr parameterValuesElement = GetFirstChildElement(parameterBindingElement, "ParameterValues");
        if (parameterValuesElement)
        {
          xmlNodePtr parameterSetElement = GetFirstChildElement(parameterValuesElement, "ParameterSet");
          if (parameterSetElement)
          {
            ImportComponentParameterSets(parameterSetElement, component);
          }
        }
      }
    }
    parameterBindingElement = xmlNextElementSibling(parameterBindingElement);
  }
}

void SsdFileImporter::ImportSystemConnections(xmlNodePtr connectionsElement,
                                              const std::shared_ptr<SsdSystem> &ssdSystem)
{
  xmlNodePtr connectionElement = GetFirstChildElement(connectionsElement, "Connection");
  if (!connectionElement)
  {
    LOGINFO("SSP Importer: No Connection element present.");
    return;
  }

  std::string startElement;
  std::string endElement;
  std::string startConnector;
  std::string endConnector;

  while (connectionElement)
  {
    if (xmlStrEqual(connectionElement->name, toXmlChar("Connection")))
    {
      std::map<std::string, std::string> map;

      if (!ParseAttribute(connectionElement, "startConnector", startConnector))
      {
        LOGERRORANDTHROW("SSP Importer: Unable to retrieve connection startConnector.");
      }
      map.insert(std::make_pair("startConnector", startConnector));

      if (!ParseAttribute(connectionElement, "endConnector", endConnector))
      {
        LOGERRORANDTHROW("SSP Importer: Unable to retrieve connection endConnector.");
      }
      map.insert(std::make_pair("endConnector", endConnector));

      if (ParseAttribute(connectionElement, "startElement", startElement))
      {
        map.emplace("startElement", startElement);
      }
      else
      {
        map.emplace("startElement", ssdSystem->GetName());
      }
      if (ParseAttribute(connectionElement, "endElement", endElement))
      {
        map.emplace("endElement", endElement);
      }
      else
      {
        map.emplace("endElement", ssdSystem->GetName());
      }
      ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(map));
    }
    connectionElement = xmlNextElementSibling(connectionElement);
  }
}

void SsdFileImporter::ImportSystemComponents(xmlNodePtr elementsElement,
                                             const std::shared_ptr<SsdSystem> &ssdSystem,
                                             const std::filesystem::path &filename)
{
  xmlNodePtr componentsElement = GetFirstChildElement(elementsElement, "Component");
  if (componentsElement)
  {
    while (componentsElement)
    {
      if (xmlStrEqual(componentsElement->name, toXmlChar("Component")))
      {
        std::string componentName;
        if (!ParseAttribute(componentsElement, "name", componentName))
        {
          LOGERRORANDTHROW("SSP Importer: Unable to retrieve component name: " + componentName);
        }

        std::string componentSource;
        if (!ParseAttribute(componentsElement, "source", componentSource))
        {
          LOGERRORANDTHROW("SSP Importer: Unable to retrieve component source: " + componentSource);
        }

        std::string componentType;
        if (!ParseAttribute(componentsElement, "type", componentType))
        {
          LOGINFO("SSP Importer: Unable to retrieve component type: " + componentType
                  + "\nDefauting to application/x-fmu-sharedlibrary");
          componentType = "application/x-fmu-sharedlibrary";
        }

        std::shared_ptr<SsdComponent> ssdComponent{};
        if (componentType == "application/x-fmu-sharedlibrary")
        {
          ssdComponent = std::make_shared<SsdComponent>(
              std::move(componentName), std::move(componentSource), SspComponentType::x_fmu_sharedlibrary);
        }
        else if (componentType == "application/x-ssp-definition")
        {
          ssdComponent = std::make_shared<SsdComponent>(
              std::move(componentName), std::move(componentSource), SspComponentType::x_ssp_definition);
        }

        LOGINFO("SSP Importer: import connectors");
        xmlNodePtr connectorsElement = GetFirstChildElement(componentsElement, "Connectors");
        if (connectorsElement)
        {
          ImportConnectors(connectorsElement, ssdComponent, ssdSystem);
        }

        LOGINFO("SSP Importer: import annotations");
        xmlNodePtr annotationsElement = GetFirstChildElement(componentsElement, "Annotations");
        if (annotationsElement)
        {
          ImportComponentAnnotations(annotationsElement, ssdComponent);
        }

        LOGINFO("SSP Importer: import parameters");
        xmlNodePtr parameterBindingsElement = GetFirstChildElement(componentsElement, "ParameterBindings");
        if (parameterBindingsElement)
        {
          ImportComponentParameters(parameterBindingsElement, ssdComponent, filename);
        }

        ssdSystem->AddComponent(std::move(ssdComponent));
      }
      componentsElement = xmlNextElementSibling(componentsElement);
    }
  }
}

SspParserTypes::Enumerations SsdFileImporter::ImportSsdEnumerations(xmlNodePtr enumerationsElement)
{
  xmlNodePtr enumerationElement = GetFirstChildElement(enumerationsElement, "Enumeration");
  SspParserTypes::Enumerations enumerations{};

  if (!enumerationElement)
  {
    LOGINFO("SSP Importer: No Enumeration element present.");
    return enumerations;
  }

  std::string enumName;
  std::vector<SspParserTypes::EnumerationItem> enumItems;

  while (enumerationElement)
  {
    if (xmlStrEqual(enumerationElement->name, toXmlChar("Connection")))
    {
      if (!ParseAttribute(enumerationElement, "name", enumName))
      {
        LOGERRORANDTHROW("SSP Importer: Unable to retrieve enum name.");
      }

      xmlNodePtr enumerationItem = GetFirstChildElement(enumerationElement, "Item");
      if (!enumerationItem)
      {
        LOGINFO("SSP Importer: No Enumeration element items present.");
        return enumerations;
      }
      std::string itemName;
      int itemValue{};
      while (enumerationItem)
      {
        if (xmlStrEqual(enumerationItem->name, toXmlChar("Item")))
        {
          if (!ParseAttribute(enumerationItem, "name", itemName))
          {
            LOGERRORANDTHROW("SSP Importer: Unable to retrieve enum item name.");
          }
          if (!ParseAttribute(enumerationItem, "value", itemValue))
          {
            LOGERRORANDTHROW("SSP Importer: Unable to retrieve enum item value.");
          }

          SspParserTypes::EnumerationItem enumItem;
          enumItem.first = itemName;
          enumItem.second = itemValue;
          enumItems.emplace_back(enumItem);
        }
        enumerationItem = xmlNextElementSibling(enumerationItem);
      }
      SspParserTypes::Enumeration enumeration;
      enumeration.first = enumName;
      enumeration.second = enumItems;
      enumerations.emplace_back(enumeration);
    }
    enumerationElement = xmlNextElementSibling(enumerationElement);
  }
  return enumerations;
}

xmlDocPtr SsdFileImporter::ImportSsdFileContent(const std::filesystem::path &filename)
{
  std::locale::global(std::locale("C"));

  if (!std::filesystem::exists(filename))
  {
    LOGINFO("SSP Importer: SsdFile: " + filename.string() + " does not exist.");
    return nullptr;
  }

  std::ifstream xmlFile(filename, std::ios::in);
  if (!xmlFile.is_open())
  {
    LOGERRORANDTHROW("SSP Importer: an error occurred during SsdFile import: " + filename.string());
  }

  std::string xmlDataString((std::istreambuf_iterator<char>(xmlFile)), std::istreambuf_iterator<char>());

  xmlParserCtxtPtr parser = xmlNewParserCtxt();
  if (parser == nullptr)
  {
    LOGERRORANDTHROW("Failed to create XML parser context.");
  }

  xmlDocPtr document = xmlCtxtReadMemory(parser,
                                         xmlDataString.c_str(),
                                         static_cast<int>(xmlDataString.size()),
                                         filename.string().c_str(),
                                         nullptr,
                                         XML_PARSE_NOBLANKS);
  // xmlErrorPtr error = xmlCtxtGetLastError(parser);
  if (document == nullptr)
  {
    LOGERRORANDTHROW("SSP Importer: invalid xml file format of file " + filename.string());
  }

  xmlFreeParserCtxt(parser);

  return document;
}

bool SsdFileImporter::Import(const std::filesystem::path &filename, std::vector<std::shared_ptr<SsdFile>> &ssdFiles)
{
  try
  {
    xmlDocPtr document = ImportSsdFileContent(filename);
    if (!document)
    {
      return false;
    }

    xmlNodePtr documentRoot = xmlDocGetRootElement(document);
    if (documentRoot == nullptr)
    {
      return false;
    }

    xmlNodePtr systemElement = GetFirstChildElement(documentRoot, "System");
    std::string localName = "ssd";
    xmlChar *string = xmlGetNsProp(
        documentRoot, toXmlChar(localName), toXmlChar("http://ssp-standard.org/SSP1/SystemStructureDescription"));
    if (systemElement)
    {
      std::string systemId;
      if (!ParseAttribute(systemElement, "name", systemId))
      {
        LOGERRORANDTHROW("SSP Importer: Unable to retrieve system name: " + systemId);
      }

      auto ssdSystem = std::make_shared<SsdSystem>(systemId);
      ImportSystem(filename, systemElement, ssdSystem);
      ssdFiles.emplace_back(std::make_shared<SsdFile>(filename, ssdSystem));
    }
    xmlFreeDoc(document);

    return true;
  }
  catch (const std::runtime_error &e)
  {
    LOGERRORANDTHROW("SSP Importer: SsdFile import failed.");
  }
}

void SsdFileImporter::ImportSystem(const std::filesystem::path &filename,  // NOLINT(misc-no-recursion)
                                   xmlNodePtr systemElement,
                                   const std::shared_ptr<SsdSystem> &ssdSystem)
{
  LOGINFO("SSP Importer: Import enumerations");
  xmlNodePtr enumerationsElement = GetFirstChildElement(systemElement, "Enumerations");
  if (enumerationsElement)
  {
    try
    {
      auto enumerations = ImportSsdEnumerations(enumerationsElement);
      ssdSystem->AddEnumerations(std::move(enumerations));
    }
    catch (const std::runtime_error &error)
    {
      LOGERRORANDTHROW("SSP Importer: Unable to import system enumerators.");
    }
  }

  LOGINFO("SSP Importer: Import components");
  xmlNodePtr elementsElement = GetFirstChildElement(systemElement, "Elements");
  if (elementsElement)
  {
    try
    {
      ImportSystemComponents(elementsElement, ssdSystem, filename);
    }
    catch (const std::runtime_error &error)
    {
      LOGERRORANDTHROW("SSP Importer: Unable to import system components.");
    }

    LOGINFO("SSP Importer: Import systems");
    xmlNodePtr subsystemElement = GetFirstChildElement(elementsElement, "System");
    if (subsystemElement)
    {
      while (subsystemElement)
      {
        if (xmlStrEqual(subsystemElement->name, toXmlChar("System")))
        {
          std::string systemId;
          if (!ParseAttribute(systemElement, "name", systemId))
          {
            LOGERRORANDTHROW("SSP Importer: Unable to retrieve system name: " + systemId);
          }

          auto ssdSubSystem = std::make_shared<SsdSystem>(systemId);
          ImportSystem(filename, subsystemElement, ssdSubSystem);
          ssdSystem->AddSystem(std::move(ssdSubSystem));
        }
        subsystemElement = xmlNextElementSibling(subsystemElement);
      }
    }
  }

  LOGINFO("SSP Importer: Import connectors");
  xmlNodePtr connectorsElement = GetFirstChildElement(systemElement, "Connectors");
  if (connectorsElement)
  {
    try
    {
      ImportConnectors(connectorsElement, ssdSystem, ssdSystem);
    }
    catch (const std::runtime_error &error)
    {
      LOGERRORANDTHROW("SSP Importer: Unable to import system connectors.");
    }
  }

  LOGINFO("SSP Importer: Import connections");
  xmlNodePtr connectionsElement = GetFirstChildElement(systemElement, "Connections");

  try
  {
    ImportSystemConnections(connectionsElement, ssdSystem);
  }
  catch (const std::runtime_error &error)
  {
    LOGERRORANDTHROW("SSP Importer: Unable to import system connections.");
  }
}

void SsdFileImporter::ImportComponentAnnotations(xmlNodePtr annotationsElement,
                                                 const std::shared_ptr<SsdComponent> &component)
{
  xmlNodePtr annotationElement = GetFirstChildElement(annotationsElement, "Annotation");
  if (!annotationElement)
  {
    LOGWARN("SSP Importer: Unable to retrieve component annotations.");
  }

  int priority{};

  while (annotationElement)
  {
    if (xmlStrEqual(annotationElement->name, toXmlChar("Annotation")))
    {
      std::string annotationType;
      ParseAttribute(annotationElement, "type", annotationType);

      if (annotationType == "de.setlevel.ssp.scheduling")
      {
        xmlNodePtr binaryVariable = GetFirstChildElement(annotationElement, "co-simulation");
        if (binaryVariable)
        {
          ParseAttribute(binaryVariable, "priority", priority);
        }
      }
    }
    annotationElement = xmlNextElementSibling(annotationElement);
  }

  component->SetPriority(priority);
}

void SsdFileImporter::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message)
{
  SspLogger::Log(logLevel, file, line, message);
}
