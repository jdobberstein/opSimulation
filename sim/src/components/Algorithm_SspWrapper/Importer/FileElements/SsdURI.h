/*******************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <regex>
#include <string>

#include "common/xmlParser.h"

struct SsdURI  ///< SSD URI
{
private:
  std::string uri;
  static const std::regex uriRegex;
  void VerifyUri(const std::string &);

public:
  SsdURI() = default;

  /// @brief SsdURI constructor
  /// @param  uriString
  SsdURI(std::string);

  /// @param  uriString Another SsdURI object
  /// @return copy assignment operator
  SsdURI &operator=(std::string);

  /// @param  uriString Another SsdURI object
  /// @return move assignment operator
  SsdURI &operator=(std::string &&);

  operator std::string() const;  ///< Converts signal to string

  /// @return Returns Path
  [[nodiscard]] std::string Path() const;

  /// @return Returns id of the fragment
  [[nodiscard]] std::string FragmentId() const;

  /// @return Returns if it has fragment
  [[nodiscard]] bool HasFragment() const;
};

namespace SimulationCommon
{
/// @brief Parse attributes of SSDURI type
/// @tparam SsdURI
/// @param  element
/// @param  attributeName
/// @param  result
/// @return True, if successfull
template <>
bool ParseAttribute<>(xmlNodePtr element, const std::string &attributeName, SsdURI &result);
}  // namespace SimulationCommon
