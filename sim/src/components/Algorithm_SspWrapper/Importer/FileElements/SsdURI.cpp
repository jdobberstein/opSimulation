/*******************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SsdURI.h"

void SsdURI::VerifyUri(const std::string &uriString)
{
  if (std::regex_match(uriString.cbegin(), uriString.cend(), uriRegex)) return;
  throw std::runtime_error("Invalid URI string format: " + uriString);
}

SsdURI::operator std::string() const
{
  return uri;
}

SsdURI::SsdURI(std::string uriString) : uri(std::move(uriString))
{
  VerifyUri(uri);
}

SsdURI &SsdURI::operator=(std::string uriString)
{
  VerifyUri(uriString);
  uri = std::move(uriString);
  return *this;
}

SsdURI &SsdURI::operator=(std::string &&uriString)
{
  VerifyUri(uriString);
  uri = std::move(uriString);
  return *this;
}

const std::regex SsdURI::uriRegex{
    R"(((\w+)\/)*[\w\.]+(#[\w\.]+)?)",
    std::regex_constants::icase | std::regex_constants::optimize | std::regex_constants::ECMAScript};

std::string SsdURI::Path() const
{
  return uri.substr(0, uri.find('#'));
}

std::string SsdURI::FragmentId() const
{
  return uri.substr(uri.find('#') + 1);
}

bool SsdURI::HasFragment() const
{
  return Path() != FragmentId();
}

template <>
bool SimulationCommon::ParseAttribute<>(xmlNodePtr element, const std::string &attributeName, SsdURI &result)
{
  std::string tmp{};
  if (SimulationCommon::ParseAttribute(element, attributeName, tmp))
  {
    result = tmp;
    return true;
  }
  return false;
}