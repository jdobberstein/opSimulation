/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SsdToSspNetworkParser.h"

#include <filesystem>
#include <optional>
#include <string_view>
#include <utility>

#include "OSMPConnectorFactory.h"
#include "ScalarConnectorFactory.h"
#include "SspLogger.h"
#include "components/Algorithm_FmuWrapper/src/variant_visitor.h"
#include "src/components/Algorithm_FmuWrapper/src/fmuFileHelper.h"

SsdToSspNetworkParser::SsdToSspNetworkParser(std::string componentName,
                                             bool isInit,
                                             int priority,
                                             int offsetTime,
                                             int responseTime,
                                             int cycleTime,
                                             StochasticsInterface *stochasticsInterface,
                                             WorldInterface *worldInterface,
                                             const ParameterInterface *parameterInterface,
                                             PublisherInterface *const publisherInterface,
                                             AgentInterface *agentInterface,
                                             const CallbackInterface *callbackInterface,
                                             std::shared_ptr<ScenarioControlInterface> scenarioControlInterface)
    : componentName(std::move(componentName)),
      isInit(isInit),
      priority(priority),
      offsetTime(offsetTime),
      responseTime(responseTime),
      cycleTime(cycleTime),
      stochastics(stochasticsInterface),
      world(worldInterface),
      parameters(parameterInterface),
      publisher(publisherInterface),
      agent(agentInterface),
      callbacks(callbackInterface),
      scenarioControl(std::move(scenarioControlInterface))
{
}

void SsdToSspNetworkParser::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}

std::shared_ptr<ssp::System> SsdToSspNetworkParser::GetRootSystem(const std::vector<std::shared_ptr<SsdFile>> &ssdFiles)
{
  auto rootFileIterator = std::find_if(
      ssdFiles.cbegin(),
      ssdFiles.cend(),
      [](auto ssdFile) { return std::filesystem::path{ssdFile->fileName}.filename() == "SystemStructure.ssd"; });
  if (rootFileIterator == ssdFiles.cend())
  {
    LOGERRORANDTHROW("SSP Parser: No 'SystemStructure.ssd' file found in System Structure Package.");
  }

  this->ssdFiles = &ssdFiles;
  const auto &rootFile = *rootFileIterator;
  const auto rootSystemName = rootFile->ssdSystem->GetName();

  outputDir = openpass::common::GenerateEntityOutputDir(parameters->GetRuntimeInformation(),
                                                        FmuFileHelper::CreateAgentIdString(agent->GetId()));
  auto rootSystem = ParseSsdFile(rootFile);
  LOGINFO("SSP Parser: Created root system " + rootSystemName + ".");
  rootSystem->SetOutputDir(outputDir);
  LOGINFO("SSP Parser: Created SSP root output directory: " + outputDir.string() + ".");

  return rootSystem;
}

std::shared_ptr<ssp::System> SsdToSspNetworkParser::ParseSsdFile(  // NOLINT(misc-no-recursion)
    const std::shared_ptr<SsdFile> &ssdFile)
{
  const auto ssdSystem = ssdFile->ssdSystem;

  LOGINFO("SSP Parser: Started parsing the ssd file which describes the system " + ssdSystem->GetName() + ".");

  std::vector<std::shared_ptr<ssp::ConnectorInterface>> systemInputConnectors{};
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> systemOutputConnectors{};

  auto targetToTracesMap = std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>();

  auto outputDirComplete = outputDir / ssdSystem->GetName();

  auto ssdEnumerations = ssdSystem->GetEnumerations();
  auto systemEnumerations
      = std::make_shared<std::vector<std::shared_ptr<ssp::Enumeration>>>(HandleEnumerations(ssdEnumerations));
  HandleSystemConnectors(ssdSystem, systemInputConnectors, systemOutputConnectors);

  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> system_components
      = HandleComponents(ssdFile, ssdSystem, outputDirComplete, targetToTracesMap);

  LOGINFO("SSP Parser: Initialize system " + ssdSystem->GetName() + ".");
  auto system = std::make_shared<ssp::System>(ssdSystem->GetName(),
                                              std::move(system_components),
                                              std::move(systemInputConnectors),
                                              std::move(systemOutputConnectors),
                                              targetToTracesMap);
  system->SetOutputDir(outputDirComplete);
  system->SetEnumerations(systemEnumerations);

  HandleConnections(ssdSystem, system);
  auto connectorInits = ssdSystem->GetConnectorInitializations();
  parameterInitializations.insert(parameterInitializations.end(), connectorInits.begin(), connectorInits.end());

  return system;
}

const std::vector<SspParserTypes::Connection> &SsdToSspNetworkParser::GetConnections() const
{
  return connections;
}
const std::map<SspParserTypes::Component, int> &SsdToSspNetworkParser::GetPriorities() const
{
  return priorities;
}

void SsdToSspNetworkParser::HandleSystemConnectors(
    const std::shared_ptr<SsdSystem> &ssdSystem,
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> &systemInputConnectors,
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> &systemOutputConnectors)
{
  for (const auto &systemConnector : ssdSystem->GetConnectors())
  {
    auto connectorType = ConnectorKindFromString(systemConnector.first);
    if (connectorType == ConnectorKind::Input || connectorType == ConnectorKind::Inout
        || connectorType == ConnectorKind::Parameter)
    {
      AddSystemConnector(systemInputConnectors, systemConnector, inputConnectors);
    }

    if (connectorType == ConnectorKind::Output || connectorType == ConnectorKind::Inout
        || connectorType == ConnectorKind::Calculated_Parameter)
    {
      AddSystemConnector(systemOutputConnectors, systemConnector, outputConnectors);
    }
  }
}

void SsdToSspNetworkParser::AddSystemConnector(std::vector<std::shared_ptr<ssp::ConnectorInterface>> &systemConnectors,
                                               std::pair<std::string, SspParserTypes::Connector> systemConnector,
                                               std::set<SspParserTypes::Connector> &connectors)
{
  systemConnectors.emplace_back(
      std::move(std::make_shared<ssp::Connector>(std::visit(osmpConnectorNameVisitor, systemConnector.second))));
  connectors.emplace(systemConnector.second);
}

std::vector<std::shared_ptr<ssp::VisitableNetworkElement>>
SsdToSspNetworkParser::HandleComponents(  // NOLINT(misc-no-recursion)
    const std::shared_ptr<SsdFile> &ssdFile,
    const std::shared_ptr<SsdSystem> &ssdSystem,
    const std::filesystem::path &outputDirComplete,
    std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetToTracesMap)
{
  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> _components;

  for (const auto &ssdComponent : ssdSystem->GetComponents())
  {
    LOGINFO("SSP Parser: Added new component " + ssdComponent->GetName() + ".");

    if (ssdComponent->GetPriority().has_value())
    {
      priorities.emplace(ssdComponent->GetName(), ssdComponent->GetPriority().value());
      LOGINFO("SSP Parser: " + ssdComponent->GetName() + " has a priority of "
              + std::to_string(ssdComponent->GetPriority().value()) + ".");
    }
    else
    {
      LOGWARN("SSP Parser: " + ssdComponent->GetName() + " has no priority configured.");
    }

    if (ssdComponent->GetComponentType() == SspComponentType::x_fmu_sharedlibrary)
    {
      auto *fmuWrapperParameters = GetFmuParameters(ssdFile, ssdComponent);
      auto fmuWrapperInterface = EmplaceAlgorithmFmuWrapper(ssdSystem, ssdComponent, fmuWrapperParameters);
      const auto fmiVersion = fmuWrapperInterface->getFmiVersion();
      switch (fmiVersion)
      {
        case fmi_version_enu_t::fmi_version_1_enu:
          _components.emplace_back(CreateFmuComponent<FMI1>(ssdComponent, outputDirComplete, targetToTracesMap));
          break;
        case fmi_version_enu_t::fmi_version_2_0_enu:
          _components.emplace_back(CreateFmuComponent<FMI2>(ssdComponent, outputDirComplete, targetToTracesMap));
          break;
        default:
          LOGINFO("SSP Parser: Unknown FMI version" + ssdComponent->GetName());
          _components.emplace_back(CreateFmuComponent<FMI2>(ssdComponent, outputDirComplete, targetToTracesMap));
          break;
      }
    }
    else if (ssdComponent->GetComponentType() == SspComponentType::x_ssp_definition)
    {
      auto source = ssdComponent->GetSource();
      auto path = source.Path();
      auto pathFragment = source.FragmentId();
      bool foundFile = false;
      for (const auto &file : *this->ssdFiles)
      {
        auto filename = file.get()->fileName;
        auto substrFilename = std::filesystem::path(filename).filename().string();
        if (path == substrFilename || pathFragment == substrFilename)
        {
          auto subsystem = ParseSsdFile(file);
          _components.emplace_back(subsystem);
          foundFile = true;
        }
      }
      if (!foundFile)
      {
        std::string logMsg = "SSP Parser: Could not find file described as source: ";
        LOGERRORANDTHROW(logMsg.append(path).append("#").append(pathFragment).append(" ."));
      }
    }
    else
    {
      _components.emplace_back(std::make_shared<ssp::System>(ssdComponent->GetName()));
    }
  }
  return _components;
}

std::vector<std::shared_ptr<ssp::Enumeration>> SsdToSspNetworkParser::HandleEnumerations(
    const std::vector<std::shared_ptr<SspParserTypes::Enumeration>> &ssdEnumerations)
{
  std::vector<std::shared_ptr<ssp::Enumeration>> enumerations;

  for (const auto &enumeration : ssdEnumerations)
  {
    std::shared_ptr<ssp::Enumeration> sspEnum = std::make_shared<ssp::Enumeration>();
    sspEnum->name = enumeration->first;
    sspEnum->items = std::make_shared<std::vector<ssp::EnumeratorItem>>();

    for (const auto &item : enumeration->second)
    {
      ssp::EnumeratorItem enumItem;
      enumItem.name = item.first;
      enumItem.value = item.second;
      sspEnum->items->push_back(enumItem);
    }
    enumerations.push_back(sspEnum);
  }

  return enumerations;
}

void SsdToSspNetworkParser::HandleConnections(const std::shared_ptr<SsdSystem> &ssdSystem,
                                              std::shared_ptr<ssp::System> &system)
{
  LOGINFO("SSP Parser: Read in connections of " + ssdSystem->GetName() + ".");
  for (const auto &connection : ssdSystem->GetConnections())
  {
    std::string startElement, endElement, startConnectorName, endConnectorName;
    for (const auto &connectionParameter : *connection)
    {
      if (connectionParameter.first == "startElement")
      {
        startElement = connectionParameter.second;
      }
      else if (connectionParameter.first == "endElement")
      {
        endElement = connectionParameter.second;
      }
      else if (connectionParameter.first == "startConnector")
      {
        startConnectorName = connectionParameter.second;
      }
      else if (connectionParameter.first == "endConnector")
      {
        endConnectorName = connectionParameter.second;
      }
      else
      {
        LOGERRORANDTHROW("SSP Parser: Unknown Element or Connector Type " + connectionParameter.first);
      }
    }
    if (startElement.empty() || endElement.empty() || startConnectorName.empty() || endConnectorName.empty())
    {
      LOGERRORANDTHROW("SSP Parser: Not all connection parameters provided in");
    }

    std::optional<SspParserTypes::Connector> startConnector{}, endConnector{};
    std::string osmpRole{};
    std::string osmpLinkName{};

    startConnector = GenerateConnector(outputConnectors, startConnectorName, osmpRole, osmpLinkName);
    endConnector = GenerateConnector(inputConnectors, endConnectorName, osmpRole, osmpLinkName);

    if (!startConnector.has_value() && !endConnector.has_value())
    {
      LogWarningConnectionInvalid(startElement, startConnectorName, endElement, endConnectorName);
    }
    else if (startConnector.has_value() && endConnector.has_value())
    {
      connections.emplace_back(startConnector.value(), endConnector.value());
    }

    startConnectorName = SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(startConnectorName);
    endConnectorName = SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(endConnectorName);

    if (!osmpRole.empty())
    {
      system->ManageConnection(
          {startElement, startConnectorName, endElement, endConnectorName, osmpRole, osmpLinkName});
    }
    else
    {
      system->ManageConnection({startElement, startConnectorName, endElement, endConnectorName});
    }
  }

  GenerateSubsystemConnections(system);
}

std::optional<SspParserTypes::Connector> SsdToSspNetworkParser::GenerateConnector(
    const std::set<SspParserTypes::Connector> &connectors,
    const std::string &connectorName,
    std::string &osmpRole,
    std::string &osmpLinkName)
{
  std::optional<SspParserTypes::Connector> tempConnector{};
  for (const auto &connector : connectors)
  {
    if (connectorName == std::visit(connectorNameVisitor, connector))
    {
      tempConnector = connector;
      if (std::visit(connectorIsOSMP, connector))
      {
        osmpRole = std::visit(connectorOSMPRoleVisitor, connector);
        osmpLinkName = std::visit(fmuOSMPLinkNameVisitor, connector);
        break;
      }
    }
  }
  return tempConnector;
}

void SsdToSspNetworkParser::GenerateSubsystemConnections(std::shared_ptr<ssp::System> &system)
{
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> subsystemConnectorGroupsIn;
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> subsystemConnectorGroupsOut;
  for (const auto &element : system->GetElements())
  {
    auto *subSystem = dynamic_cast<ssp::System *>(element.get());
    if (subSystem == nullptr) continue;

    GenerateSubsystemConnectors(subSystem->systemInputConnector, subsystemConnectorGroupsIn);
    GenerateSubsystemConnectors(subSystem->systemOutputConnector, subsystemConnectorGroupsOut);
  }
  ssp::GroupConnector subsSystemConnectorGroupGroupConnectorIn{subsystemConnectorGroupsIn};
  system->systemInputConnector->connectors.emplace_back(
      std::make_shared<ssp::GroupConnector>(subsSystemConnectorGroupGroupConnectorIn));
  ssp::GroupConnector subsSystemConnectorGroupGroupConnectorOut{subsystemConnectorGroupsOut};
  system->systemOutputConnector->connectors.emplace_back(
      std::make_shared<ssp::GroupConnector>(subsSystemConnectorGroupGroupConnectorOut));
}

void SsdToSspNetworkParser::GenerateSubsystemConnectors(
    std::shared_ptr<ssp::GroupConnector> &systemConnector,
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> &subsystemConnectorGroups)
{
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> subsystemConnectors;
  for (const auto &connector : systemConnector->connectors)
  {
    subsystemConnectors.emplace_back(connector);
  }
  ssp::GroupConnector subSystemConnectorGroup{subsystemConnectors};
  subsystemConnectorGroups.emplace_back(std::make_shared<ssp::GroupConnector>(subSystemConnectorGroup));
}

SimulationCommon::Parameters *SsdToSspNetworkParser::GetFmuParameters(const std::shared_ptr<SsdFile> &ssdFile,
                                                                      const std::shared_ptr<SsdComponent> &ssdComponent)
{
  auto fmuWrapperParameters = std::make_unique<SimulationCommon::Parameters>(parameters->GetRuntimeInformation());

  LOGINFO("SSP Parser: Adding parameters to " + ssdComponent->GetName());
  fmuWrapperParameters->AddParameterString(
      "FmuPath", (std::filesystem::path{ssdFile->fileName}.parent_path() / ssdComponent->GetSource().Path()).string());
  for (const auto &parameter : ssdComponent->GetParameters())
  {
    if (const auto *const stringParameter = std::get_if<std::string>(&parameter.second))
    {
      fmuWrapperParameters->AddParameterString(parameter.first, *stringParameter);
    }
    else if (const auto *const boolParameter = std::get_if<bool>(&parameter.second))
    {
      fmuWrapperParameters->AddParameterBool(parameter.first, *boolParameter);
    }
    else if (const auto *const intParameter = std::get_if<int>(&parameter.second))
    {
      fmuWrapperParameters->AddParameterInt(parameter.first, *intParameter);
    }
    else if (const auto *const doubleParameter = std::get_if<double>(&parameter.second))
    {
      fmuWrapperParameters->AddParameterDouble(parameter.first, *doubleParameter);
    }
    else
    {
      callbacks->LOGWARN("SSP Parser: Ignoring Parameter " + parameter.first + " because of unsupported type.");
    }
  }

  auto [iter, _] = parameterForComponents.emplace(ssdComponent->GetName(), std::move(fmuWrapperParameters));
  return iter->second.get();
}

std::shared_ptr<FmuWrapperInterface> SsdToSspNetworkParser::EmplaceAlgorithmFmuWrapper(
    const std::shared_ptr<SsdSystem> &ssdSystem,
    const std::shared_ptr<SsdComponent> &ssdComponent,
    SimulationCommon::Parameters *fmuWrapperParameters)
{
  auto fmuWrapperInterface = std::make_shared<AlgorithmFmuWrapperImplementation>(
      componentName + "." + ssdSystem->GetName() + "." + ssdComponent->GetName(),
      isInit,
      priority,
      offsetTime,
      responseTime,
      cycleTime,
      world,
      stochastics,
      fmuWrapperParameters,
      publisher,
      callbacks,
      agent,
      scenarioControl);
  componentToWrapperMap.emplace(ssdComponent->GetName(), fmuWrapperInterface);
  return fmuWrapperInterface;
}

template <size_t FMI>
std::shared_ptr<ssp::FmuComponent> SsdToSspNetworkParser::CreateFmuComponent(
    const std::shared_ptr<SsdComponent> &ssdComponent,
    std::filesystem::path outputDirComplete,
    std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetToTracesMap)
{
  LOGINFO("SSP Parser: Read in connectors for " + ssdComponent->GetName() + ".");
  OSMPConnectorFactory osmpConnectorFactory{ssdComponent->GetName()};
  ScalarConnectorFactory scalarConnectorFactory{ssdComponent->GetName()};
  AddConnectors(osmpConnectorFactory, scalarConnectorFactory, ssdComponent);

  std::vector<std::shared_ptr<ssp::ConnectorInterface>> input_connectors
      = osmpConnectorFactory.CreateInputOSMPConnectors<FMI>(
          ssdComponent->GetWriteMessageParameters(), outputDirComplete, targetToTracesMap);
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> output_connectors
      = osmpConnectorFactory.CreateOutputOSMPConnectors<FMI>(
          ssdComponent->GetWriteMessageParameters(), outputDirComplete, targetToTracesMap);
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> input_scalar_connectors
      = scalarConnectorFactory.CreateInputScalarConnectors(
          ssdComponent->GetWriteMessageParameters(), outputDirComplete, targetToTracesMap);
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> output_scalar_connectors
      = scalarConnectorFactory.CreateOutputScalarConnectors(
          ssdComponent->GetWriteMessageParameters(), outputDirComplete, targetToTracesMap);
  input_connectors.insert(input_connectors.end(), input_scalar_connectors.begin(), input_scalar_connectors.end());
  output_connectors.insert(output_connectors.end(), output_scalar_connectors.begin(), output_scalar_connectors.end());
  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;

  auto fmuComponent = std::make_shared<ssp::FmuComponent>(ssdComponent->GetName(),
                                                          std::move(emptyElements),
                                                          std::move(input_connectors),
                                                          std::move(output_connectors),
                                                          componentToWrapperMap.at(ssdComponent->GetName()));
  return fmuComponent;
}

void SsdToSspNetworkParser::AddConnectors(OSMPConnectorFactory &osmpConnectorFactory,
                                          ScalarConnectorFactory &scalarConnectorFactory,
                                          const std::shared_ptr<SsdComponent> &ssdComponent)
{
  RegisteredConnectorDirection direction = RegisteredConnectorDirection::None;
  for (const auto &connector : ssdComponent->GetConnectors())
  {
    auto connectorKind = ConnectorKindFromString(connector.first);
    if (std::visit(connectorIsOSMP, connector.second))
    {
      direction = osmpConnectorFactory.RegisterConnector(connector.second,
                                                         componentToWrapperMap.at(ssdComponent->GetName()),
                                                         ssdComponent->GetPriority().value_or(0),
                                                         connectorKind);
    }
    else
    {
      direction = scalarConnectorFactory.RegisterConnector(connector.second,
                                                           componentToWrapperMap.at(ssdComponent->GetName()),
                                                           ssdComponent->GetPriority().value_or(0),
                                                           connectorKind);
    }
    if (direction == RegisteredConnectorDirection::Input)
    {
      inputConnectors.emplace(connector.second);
    }
    else if (direction == RegisteredConnectorDirection::Output)
    {
      outputConnectors.emplace(connector.second);
    }
  }
}

void SsdToSspNetworkParser::LogWarningConnectionInvalid(std::string_view startElement,
                                                        std::string_view startConnectorName,
                                                        std::string_view endElement,
                                                        std::string_view endConnectorName) const
{
  std::string message = "Connection specifies nonexistent connector: (";
  message.append(startElement);
  message.append(".");
  message.append(startConnectorName);
  message.append(")->(");
  message.append(endElement);
  message.append(".");
  message.append(endConnectorName);
  message.append(")");

  LOGWARN(message);
}
ConnectorKind SsdToSspNetworkParser::ConnectorKindFromString(const std::string &fromString)
{
  if (fromString == "input") return ConnectorKind::Input;
  if (fromString == "output") return ConnectorKind::Output;
  if (fromString == "inout") return ConnectorKind::Inout;
  if (fromString == "parameter") return ConnectorKind::Parameter;
  if (fromString == "calculatedParameter") return ConnectorKind::Calculated_Parameter;
  return ConnectorKind::None;
}
