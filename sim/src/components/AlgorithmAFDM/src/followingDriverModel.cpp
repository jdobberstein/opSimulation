/********************************************************************************
 * Copyright (c) 2018-2019 AMFD GmbH
 *               2018-2019 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/**********************************************
***********************************************/
#include "followingDriverModel.h"

#include <cmath>
#include <map>
#include <new>
#include <stdexcept>
#include <units.h>
#include <utility>

#include "common/accelerationSignal.h"
#include "common/lateralSignal.h"
#include "common/secondaryDriverTasksSignal.h"
#include "components/Sensor_Driver/src/Signals/sensorDriverSignal.h"
#include "include/callbackInterface.h"
#include "include/parameterInterface.h"

class PublisherInterface;

AlgorithmAgentFollowingDriverModelImplementation::AlgorithmAgentFollowingDriverModelImplementation(
    std::string componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    StochasticsInterface *stochastics,
    WorldInterface *world,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    const CallbackInterface *callbacks,
    AgentInterface *agent)
    : SensorInterface(std::move(componentName),
                      isInit,
                      priority,
                      offsetTime,
                      responseTime,
                      cycleTime,
                      stochastics,
                      world,
                      parameters,
                      publisher,
                      callbacks,
                      agent)
{
  if (parameters->GetParametersDouble().count("VelocityWish") > 0)
  {
    vWish = units::velocity::meters_per_second_t(parameters->GetParametersDouble().at("VelocityWish"));
  }
  else if (parameters->GetParametersDouble().count("0"))
  {
    vWish = units::velocity::meters_per_second_t(parameters->GetParametersDouble().at("0"));
  }

  if (parameters->GetParametersDouble().count("Delta") > 0)
  {
    delta = parameters->GetParametersDouble().at("Delta");
  }
  else if (parameters->GetParametersDouble().count("1"))
  {
    delta = parameters->GetParametersDouble().at("1");
  }

  if (parameters->GetParametersDouble().count("TGapWish") > 0)
  {
    tGapWish = units::time::second_t(parameters->GetParametersDouble().at("TGapWish"));
  }
  else if (parameters->GetParametersDouble().count("2"))
  {
    tGapWish = units::time::second_t(parameters->GetParametersDouble().at("2"));
  }

  if (parameters->GetParametersDouble().count("MinDistance") > 0)
  {
    minDistance = units::length::meter_t(parameters->GetParametersDouble().at("MinDistance"));
  }
  else if (parameters->GetParametersDouble().count("3"))
  {
    minDistance = units::length::meter_t(parameters->GetParametersDouble().at("3"));
  }

  if (parameters->GetParametersDouble().count("MaxAcceleration") > 0)
  {
    maxAcceleration
        = units::acceleration::meters_per_second_squared_t(parameters->GetParametersDouble().at("MaxAcceleration"));
  }
  else if (parameters->GetParametersDouble().count("4"))
  {
    maxAcceleration = units::acceleration::meters_per_second_squared_t(parameters->GetParametersDouble().at("4"));
  }

  if (parameters->GetParametersDouble().count("MaxDeceleration") > 0)
  {
    decelerationWish
        = units::acceleration::meters_per_second_squared_t(parameters->GetParametersDouble().at("MaxDeceleration"));
  }
  else if (parameters->GetParametersDouble().count("5"))
  {
    decelerationWish = units::acceleration::meters_per_second_squared_t(parameters->GetParametersDouble().at("5"));
  }
}

AlgorithmAgentFollowingDriverModelImplementation::~AlgorithmAgentFollowingDriverModelImplementation() = default;

void AlgorithmAgentFollowingDriverModelImplementation::UpdateInput(int localLinkId,
                                                                   const std::shared_ptr<SignalInterface const> &data,
                                                                   [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    // from SensorDriver
    const std::shared_ptr<SensorDriverSignal const> signal = std::dynamic_pointer_cast<SensorDriverSignal const>(data);
    if (!signal)
    {
      const std::string msg = COMPONENTNAME + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }

    ownVehicleInformation = signal->GetOwnVehicleInformation();
    geometryInformation = signal->GetGeometryInformation();
    surroundingObjects = signal->GetSurroundingObjects();
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void AlgorithmAgentFollowingDriverModelImplementation::UpdateOutput(int localLinkId,
                                                                    std::shared_ptr<SignalInterface const> &data,
                                                                    [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<LateralSignal const>(componentState, out_lateralInformation, GetComponentName());
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else if (localLinkId == 1)
  {
    try
    {
      data = std::make_shared<SecondaryDriverTasksSignal const>(
          out_indicatorState, out_hornSwitch, out_headLight, out_highBeamLight, out_flasher, componentState);
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else if (localLinkId == 2)
  {
    try
    {
      data = std::make_shared<AccelerationSignal const>(componentState, out_longitudinal_acc, GetComponentName());
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void AlgorithmAgentFollowingDriverModelImplementation::Trigger([[maybe_unused]] int time)
{
  out_lateralInformation.kappaManoeuvre = geometryInformation.laneEgo.curvature;
  out_lateralInformation.laneWidth = geometryInformation.laneEgo.width;
  out_lateral_speed = 0_mps;
  out_lateral_frequency
      = units::math::sqrt(lateralDynamicConstants.lateralAcceleration / geometryInformation.laneEgo.width);
  out_lateral_damping = lateralDynamicConstants.zeta;
  out_lateralInformation.deviation = -ownVehicleInformation.lateralPosition;
  out_lateralInformation.headingError = -ownVehicleInformation.heading;
  out_lateralInformation.gainHeadingError = lateralDynamicConstants.gainHeadingError;

  const auto &frontAgent = surroundingObjects.objectFront;
  units::dimensionless::scalar_t decelerationCoeff{0.0};

  if (frontAgent.exist)
  {
    auto vDelta = units::math::abs(ownVehicleInformation.absoluteVelocity - frontAgent.absoluteVelocity);
    const units::unit_t<units::squared<units::velocity::meters_per_second>> squareVelocity
        = ownVehicleInformation.absoluteVelocity * vDelta;
    const units::acceleration::meters_per_second_squared_t acceleration
        = 2 * units::math::sqrt(maxAcceleration * decelerationWish);

    const units::length::meter_t distanceBasedOnAcceleration = squareVelocity / acceleration;
    units::length::meter_t effectiveMinimumGap
        = minDistance + ownVehicleInformation.absoluteVelocity * tGapWish + distanceBasedOnAcceleration;
    decelerationCoeff = units::math::pow<2>(effectiveMinimumGap / frontAgent.relativeLongitudinalDistance);
  }

  auto freeRoadCoeff = 1.0 - std::pow(ownVehicleInformation.absoluteVelocity / vWish, delta);
  auto intelligentDriverModelAcceleration = maxAcceleration * (freeRoadCoeff - decelerationCoeff);

  if (intelligentDriverModelAcceleration >= 0_mps_sq)
  {
    out_longitudinal_acc = units::math::min(maxAcceleration, intelligentDriverModelAcceleration);
  }
  else
  {
    auto maxDeceleration = -decelerationWish;
    out_longitudinal_acc = units::math::max(maxDeceleration, intelligentDriverModelAcceleration);
  }
}
