/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  dynamics_collision.cpp
//! @brief This file contains the implementation of of the interface for model
//!        instance construction and destruction.
//-----------------------------------------------------------------------------

#include "dynamics_collision.h"

#include <memory>
#include <new>
#include <stdexcept>
#include <string>
#include <utility>

#include "components/Dynamics_CollisionPCM/dynamics_collision_global.h"
#include "dynamics_collision_implementation.h"

class AgentInterface;
class ParameterInterface;
class PublisherInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;
class ModelInterface;
class CallbackInterface;

const std::string VERSION = "1.1.0";  //!< version of the current module - has to be incremented manually
static const CallbackInterface *Callbacks = nullptr;  // NOLINT[cppcoreguidelines-avoid-non-const-global-variables]

//-----------------------------------------------------------------------------
//! dll-function to obtain the version of the current module
//!
//! @return                       Version of the current module
//-----------------------------------------------------------------------------
extern "C" DYNAMICS_COLLISIONSHARED_EXPORT const std::string &OpenPASS_GetVersion()
{
  return VERSION;
}

//-----------------------------------------------------------------------------
//! dll-function to create an instance of the module.
//!
//! @param[in]     componentName  Name of the component
//! @param[in]     isInit         Corresponds to "init" of "Component"
//! @param[in]     priority       Corresponds to "priority" of "Component"
//! @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
//! @param[in]     responseTime   Corresponds to "responseTime" of "Component"
//! @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
//! @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
//! @param[in]     world          Pointer to the world
//! @param[in]     parameters     Pointer to the parameters of the module
//! @param[in]     publisher      Pointer to the publisher instance
//! @param[in]     agent          Pointer to the agent in which the module is situated
//! @param[in]     callbacks      Pointer to the callbacks
//! @return                       A pointer to the created module instance.
//-----------------------------------------------------------------------------
extern "C" DYNAMICS_COLLISIONSHARED_EXPORT DynamicsInterface *OpenPASS_CreateInstance(
    std::string componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    StochasticsInterface *stochastics,
    WorldInterface *world,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    AgentInterface *agent,
    const CallbackInterface *callbacks)
{
  Callbacks = callbacks;

  if (priority == 0)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Warning, __FILE__, __LINE__, "Priority 0 can lead to undefined behavior.");
    }
  }
  return (DynamicsInterface *)(new (std::nothrow) Dynamics_Collision_Implementation(std::move(componentName),
                                                                                    isInit,
                                                                                    priority,
                                                                                    offsetTime,
                                                                                    responseTime,
                                                                                    cycleTime,
                                                                                    stochastics,
                                                                                    world,
                                                                                    parameters,
                                                                                    publisher,
                                                                                    callbacks,
                                                                                    agent));
}

//-----------------------------------------------------------------------------
//! dll-function to destroy/delete an instance of the module.
//!
//! @param[in]     implementation    The instance that should be freed
//-----------------------------------------------------------------------------
extern "C" DYNAMICS_COLLISIONSHARED_EXPORT void OpenPASS_DestroyInstance(DynamicsInterface *implementation)
{
  delete implementation;  // NOLINT(cppcoreguidelines-owning-memory)
}

/// @brief dll function to update input to the model
/// @param implementation   Reference to the model interface
/// @param localLinkId      Id of the component
/// @param data             Reference to the signal interface
/// @param time             Current time
/// @return True, once the update is successfull
extern "C" DYNAMICS_COLLISIONSHARED_EXPORT bool OpenPASS_UpdateInput(ModelInterface *implementation,
                                                                     int localLinkId,
                                                                     const std::shared_ptr<SignalInterface const> &data,
                                                                     int time)
{
  try
  {
    implementation->UpdateInput(localLinkId, data, time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

/// @brief dll function to update output of the model
/// @param implementation   Reference to the model interface
/// @param localLinkId      Id of the component
/// @param data             Reference to the signal interface
/// @param time             Current time
/// @return True, once the update is successfull
extern "C" DYNAMICS_COLLISIONSHARED_EXPORT bool OpenPASS_UpdateOutput(ModelInterface *implementation,
                                                                      int localLinkId,
                                                                      std::shared_ptr<SignalInterface const> &data,
                                                                      int time)
{
  try
  {
    implementation->UpdateOutput(localLinkId, data, time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

/// @brief dll function to trigger the model
/// @param implementation Reference to the model interface
/// @param time           Time to trigger
/// @return True, if it is successfull
extern "C" DYNAMICS_COLLISIONSHARED_EXPORT bool OpenPASS_Trigger(ModelInterface *implementation, int time)
{
  try
  {
    implementation->Trigger(time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}
