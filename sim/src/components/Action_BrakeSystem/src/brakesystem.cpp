/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  brakesystem.cpp */
//-----------------------------------------------------------------------------

#include "brakesystem.h"

#include <utility>

#include "include/parameterInterface.h"

ActionBrakeSystem::ActionBrakeSystem(std::string componentName,
                                     bool isInit,
                                     int priority,
                                     int offsetTime,
                                     int responseTime,
                                     int cycleTime,
                                     StochasticsInterface *stochastics,
                                     WorldInterface *world,
                                     const ParameterInterface *parameters,
                                     PublisherInterface *const publisher,
                                     const CallbackInterface *callbacks,
                                     AgentInterface *agent)
    : ActionInterface(std::move(componentName),
                      isInit,
                      priority,
                      offsetTime,
                      responseTime,
                      cycleTime,
                      stochastics,
                      world,
                      parameters,
                      publisher,
                      callbacks,
                      agent),
      cycleTimeMs(static_cast<double>(GetCycleTime()))
{
  // Read external component parameters
  ParseParameters(parameters);
  // Get max deceleration from vehicle models parameters
  const std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties
      = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters());
  maxBrakeDeceleration = vehicleProperties->performance.max_deceleration;
  totalVehicleMass = GetAgent()->GetVehicleModelParameters()->mass;

  currentResponseTimeMs = brakeResponseTimeMs;
}

void ActionBrakeSystem::ParseParameters(const ParameterInterface *parameters)
{
  frontAxlePercentage = parameters->GetParametersDouble().count("FrontAxlePercentage") == 1
                          ? parameters->GetParametersDouble().at("FrontAxlePercentage")
                          : parameters->GetParametersDouble().at("0");
  if (frontAxlePercentage < 0.0 || frontAxlePercentage > 1.0)
  {
    throw std::runtime_error("FrontAxlePercentage out of range [0, 1]");
  }
  brakeInclineRate = parameters->GetParametersDouble().count("BrakeDecelerationInclineRate") == 1
                       ? parameters->GetParametersDouble().at("BrakeDecelerationInclineRate")
                       : parameters->GetParametersDouble().at("1");
  brakeDeclineRate = parameters->GetParametersDouble().count("BrakeDecelerationDeclineRate") == 1
                       ? parameters->GetParametersDouble().at("BrakeDecelerationDeclineRate")
                       : parameters->GetParametersDouble().at("2");
  brakeResponseTimeMs = parameters->GetParametersDouble().count("BrakeResponseTimeMs") == 1
                          ? parameters->GetParametersDouble().at("BrakeResponseTimeMs")
                          : parameters->GetParametersDouble().at("3");
}

void ActionBrakeSystem::UpdateInput(int localLinkId,
                                    const std::shared_ptr<SignalInterface const> &data,
                                    [[maybe_unused]] int time)
{
  std::stringstream log;
  log << componentname << " (component " << GetComponentName() << ", agent " << std::to_string(GetAgent()->GetId())
      << ", input data for local link " << localLinkId << ": ";
  LOG(CbkLogLevel::Debug, log.str());

  if (localLinkId == 0)
  {
    const std::shared_ptr<BoolSignal const> signal = std::dynamic_pointer_cast<BoolSignal const>(data);
    if (!signal)
    {
      const std::string msg = componentname + "_" + std::to_string(GetAgent()->GetId()) + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
    reqPrefill = signal->value;
  }
  else if (localLinkId == 1)
  {
    const std::shared_ptr<LongitudinalSignal const> signal = std::dynamic_pointer_cast<LongitudinalSignal const>(data);
    if (!signal)
    {
      const std::string msg = componentname + "_" + std::to_string(GetAgent()->GetId()) + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
    BrakePosition = signal->brakePedalPos;
  }
  else
  {
    const std::string msg = componentname + "_" + std::to_string(GetAgent()->GetId()) + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void ActionBrakeSystem::UpdateOutput(int localLinkId,
                                     std::shared_ptr<SignalInterface const> &data,
                                     [[maybe_unused]] int time)
{
  std::stringstream log;
  log << componentname << " (component " << GetComponentName() << ", agent " << std::to_string(GetAgent()->GetId())
      << ", output data for local link " << localLinkId << ": ";

  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<SignalVectorDouble const>(wheelBrakeTorque);
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg
          = componentname + "_" + std::to_string(GetAgent()->GetId()) + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else
  {
    const std::string msg = componentname + "_" + std::to_string(GetAgent()->GetId()) + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void ActionBrakeSystem::Trigger([[maybe_unused]] int time)
{
  // Calculate brake deceleration dependeing on brake pedal position
  const units::acceleration::meters_per_second_squared_t brakeDeceleration = BrakePosition * maxBrakeDeceleration;

  if (BrakePosition < std::numeric_limits<double>::epsilon())
  {
    // Increase response time if no prefill is requested, current deceleration is zero and brake pedal position is zero
    if (currentBrakeDeceleration.value() < std::numeric_limits<double>::epsilon() && !reqPrefill)
    {
      IncreaseResponseTime();
    }
    else
    {
      RequestBrakeDeceleration(0.0_mps_sq);
    }
    // reduce response time if prefill is requested
    if (reqPrefill)
    {
      ReduceResponseTime();
    }
  }
  else if (BrakePosition > std::numeric_limits<double>::epsilon())
  {
    // request whell torque/brake deceleration if brake pedal position is greater than zero
    RequestBrakeDeceleration(brakeDeceleration);
  }
  // calculate wheel brake torque as a function of possible brake deceleration and the brake distribution
  SetWheelBrakeTorque(currentBrakeDeceleration);
}

void ActionBrakeSystem::RequestBrakeDeceleration(units::acceleration::meters_per_second_squared_t deceleration)
{
  if (currentResponseTimeMs > std::numeric_limits<double>::epsilon()
      && deceleration.value() > std::numeric_limits<double>::epsilon())
  {
    // reduce response time if current response time is greater than zero an requested deceleration is greater than zero
    ReduceResponseTime();
  }
  else
  {
    // calculate current possible deceleration due to brake incline and decline rate
    if (BrakePositionLastCycle < BrakePosition)
    {
      currentBrakeDeceleration += 1_mps_sq * brakeInclineRate * cycleTimeMs / 1000.0;
      currentBrakeDeceleration = currentBrakeDeceleration > deceleration ? deceleration : currentBrakeDeceleration;
      currentBrakeDeceleration
          = currentBrakeDeceleration > maxBrakeDeceleration ? maxBrakeDeceleration : currentBrakeDeceleration;
    }
    else
    {
      currentBrakeDeceleration -= 1_mps_sq * brakeDeclineRate * cycleTimeMs / 1000.0;
      currentBrakeDeceleration = currentBrakeDeceleration < 0_mps_sq ? 0_mps_sq : currentBrakeDeceleration;
      currentBrakeDeceleration = currentBrakeDeceleration < deceleration ? deceleration : currentBrakeDeceleration;
    }
  }
}

void ActionBrakeSystem::ReduceResponseTime()
{
  currentResponseTimeMs -= cycleTimeMs;
  currentResponseTimeMs = currentResponseTimeMs < 0.0 ? 0.0 : currentResponseTimeMs;
}

void ActionBrakeSystem::IncreaseResponseTime()
{
  currentResponseTimeMs += cycleTimeMs;
  currentResponseTimeMs = currentResponseTimeMs > brakeResponseTimeMs ? brakeResponseTimeMs : currentResponseTimeMs;
}

void ActionBrakeSystem::SetWheelBrakeTorque(units::acceleration::meters_per_second_squared_t brakeAcceleration)
{
  const units::force::newton_t totalBrakeForce
      = totalVehicleMass * (-brakeAcceleration);  // BrakeForce= Mass * Acceleration ; 2. Newton's Law

  const std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties
      = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters());

  const units::torque::newton_meter_t frontAxleTorque = frontAxlePercentage
                                                      * vehicleProperties->front_axle.wheel_diameter / 2
                                                      * totalBrakeForce;  // T_front = r_front * F_front
  const units::torque::newton_meter_t rearAxleTorque = (1.0 - frontAxlePercentage)
                                                     * vehicleProperties->rear_axle.wheel_diameter / 2
                                                     * totalBrakeForce;  // T_rear = r_rear * F_rear

  wheelBrakeTorque.clear();
  wheelBrakeTorque.push_back(0.5 * frontAxleTorque.value());  // Front Left; 50% of front axle distribution
  wheelBrakeTorque.push_back(0.5 * frontAxleTorque.value());  // Front Right; 50% of front axle distribution
  wheelBrakeTorque.push_back(0.5 * rearAxleTorque.value());   // Rear Left; 50% of rear axle distribution
  wheelBrakeTorque.push_back(0.5 * rearAxleTorque.value());   // Rear Right; 50% of rear axle distribution
}