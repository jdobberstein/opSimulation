/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  action_brakesystem.h
//! @brief contains DLL export declarations
//-----------------------------------------------------------------------------

#pragma once

#include "sim/src/common/opExport.h"

#if defined(ACTION_BRAKESYSTEM_LIBRARY)
#define ACTION_BRAKESYSTEM_SHARED_EXPORT OPEXPORT  //!< Export of the dll-functions
#else
#define ACTION_BRAKESYSTEM_SHARED_EXPORT OPIMPORT  //!< Import of the dll-functions
#endif

#include "include/modelInterface.h"