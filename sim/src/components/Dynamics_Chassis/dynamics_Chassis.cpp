/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  dynamics_Chassis.cpp
//! @brief This file contains the DLL wrapper.
//-----------------------------------------------------------------------------

#include "dynamics_Chassis.h"

#include <memory>
#include <new>
#include <stdexcept>
#include <string>
#include <utility>

#include "components/Dynamics_Chassis/dynamics_Chassis_global.h"
#include "dynamics_Chassis_implementation.h"

class AgentInterface;
class ParameterInterface;
class PublisherInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;
class ModelInterface;
class CallbackInterface;

/// version of this component
const std::string VERSION = "0.0.1";
static const CallbackInterface *Callbacks = nullptr;  // NOLINT[cppcoreguidelines-avoid-non-const-global-variables]

/**
 * dll-function to obtain the version of the current module
 *
 * @return                       Version of the current module
 */
extern "C" DYNAMICS_CHASSIS_SHARED_EXPORT const std::string &OpenPASS_GetVersion()
{
  return VERSION;
}

/// @brief Create an instance of lateral algorithm
/// @param componentName    Name of the component
/// @param isInit           If instance initialzed
/// @param priority         Priority of the instance
/// @param offsetTime       Offset time
/// @param responseTime     Response time
/// @param cycleTime        Cycle time
/// @param stochastics      Reference to the stochastics interface
/// @param world            Reference to the world interface
/// @param parameters       Reference to the parameter interface
/// @param publisher        Reference to the publisher interface
/// @param agent            Reference to the agent interface
/// @param callbacks        Reference to the callback interface
/// @return  model interface
extern "C" DYNAMICS_CHASSIS_SHARED_EXPORT ModelInterface *OpenPASS_CreateInstance(std::string componentName,
                                                                                  bool isInit,
                                                                                  int priority,
                                                                                  int offsetTime,
                                                                                  int responseTime,
                                                                                  int cycleTime,
                                                                                  StochasticsInterface *stochastics,
                                                                                  WorldInterface *world,
                                                                                  const ParameterInterface *parameters,
                                                                                  PublisherInterface *const publisher,
                                                                                  AgentInterface *agent,
                                                                                  const CallbackInterface *callbacks)
{
  Callbacks = callbacks;

  if (priority == 0)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Warning, __FILE__, __LINE__, "Priority 0 can lead to undefined behavior.");
    }
  }
  try
  {
    return (DynamicsInterface *)(new (std::nothrow) DynamicsChassisImplementation(std::move(componentName),
                                                                                  isInit,
                                                                                  priority,
                                                                                  offsetTime,
                                                                                  responseTime,
                                                                                  cycleTime,
                                                                                  stochastics,
                                                                                  world,
                                                                                  parameters,
                                                                                  publisher,
                                                                                  callbacks,
                                                                                  agent));
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return nullptr;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return nullptr;
  }
}
//-----------------------------------------------------------------------------
//! dll-function to destroy/delete an instance of the module.
//!
//! @param[in]     implementation    The instance that should be freed
//-----------------------------------------------------------------------------
extern "C" DYNAMICS_CHASSIS_SHARED_EXPORT void OpenPASS_DestroyInstance(ModelInterface *implementation)
{
  delete implementation;  // NOLINT(cppcoreguidelines-owning-memory)
}

/// @brief dll function to update input to the model
/// @param implementation   Reference to the model interface
/// @param localLinkId      Id of the component
/// @param data             Reference to the signal interface
/// @param time             Current time
/// @return True, once the update is successfull
extern "C" DYNAMICS_CHASSIS_SHARED_EXPORT bool OpenPASS_UpdateInput(ModelInterface *implementation,
                                                                    int localLinkId,
                                                                    const std::shared_ptr<SignalInterface const> &data,
                                                                    int time)
{
  try
  {
    implementation->UpdateInput(localLinkId, data, time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

/// @brief dll function to update output of the model
/// @param implementation   Reference to the model interface
/// @param localLinkId      Id of the component
/// @param data             Reference to the signal interface
/// @param time             Current time
/// @return True, once the update is successfull
extern "C" DYNAMICS_CHASSIS_SHARED_EXPORT bool OpenPASS_UpdateOutput(ModelInterface *implementation,
                                                                     int localLinkId,
                                                                     std::shared_ptr<SignalInterface const> &data,
                                                                     int time)
{
  try
  {
    implementation->UpdateOutput(localLinkId, data, time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

/// @brief dll function to trigger the model
/// @param implementation Reference to the model interface
/// @param time           Time to trigger
/// @return True, if it is successfull
extern "C" DYNAMICS_CHASSIS_SHARED_EXPORT bool OpenPASS_Trigger(ModelInterface *implementation, int time)
{
  try
  {
    implementation->Trigger(time);
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}
