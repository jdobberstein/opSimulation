/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef FORCEWHEELZ_H
#define FORCEWHEELZ_H

#include <units.h>

#include "VehicleBasics.h"
#include "common/globalDefinitions.h"

//! Wheel force perpendicular.

namespace units::literals
{
class ForceWheelZ
{
public:
  ForceWheelZ() = default;
  ForceWheelZ(const ForceWheelZ &) = delete;
  ForceWheelZ(ForceWheelZ &&) = delete;
  ForceWheelZ &operator=(const ForceWheelZ &) = delete;
  ForceWheelZ &operator=(ForceWheelZ &&) = delete;

  virtual ~ForceWheelZ() = default;

  /**
   * @brief Calculate the perpendicular force on wheels against inertia
   *
   * @param fInertiaX  Inertia in x axis
   * @param fInertiaY  Inertia in y axis
   * @param pitchZ     Pitch in z direction
   * @param rollZ      Roll in Z direction
   * @param carParam   parameters of the car
   * @return TODO
   */
  bool CalForce(units::force::newton_t fInertiaX,
                units::force::newton_t fInertiaY,
                units::length::meter_t pitchZ,
                units::length::meter_t rollZ,
                VehicleBasics &carParam);

  /**
   * @brief Get the Force object
   *
   * @param i     TODO
   * @return double
   */
  [[nodiscard]] units::force::newton_t GetForce(const int idx) const { return forces.at(idx); }

private:
  std::array<units::force::newton_t, NUMBER_WHEELS> forces = {0.0_N, 0.0_N, 0.0_N, 0.0_N};
  std::array<units::force::newton_t, NUMBER_WHEELS> forcesPitch = {0.0_N, 0.0_N, 0.0_N, 0.0_N};
  std::array<units::force::newton_t, NUMBER_WHEELS> forcesRoll = {0.0_N, 0.0_N, 0.0_N, 0.0_N};

  // Calculate the perpendicular force on front and rear wheels against the inertia force in X axis
  bool CalForceInPitch(units::force::newton_t fInertiaX, units::angle::radian_t pitchAngle, VehicleBasics &carParam);

  // Calculate the perpendicular force on left and right wheels against the inertia force in Y axis
  bool CalForceInRoll(units::force::newton_t fInertiaY, units::angle::radian_t rollAngle, VehicleBasics &carParam);
};
}  //namespace units::literals
#endif  //FORCEWHEELZ_H
