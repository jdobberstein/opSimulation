/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  componentControllerImpl.cpp
//! @brief This file contains the implementation of the header file
//-----------------------------------------------------------------------------

#include "componentControllerImpl.h"

#include <utility>
#include <vector>

#include "common/agentCompToCompCtrlSignal.h"
#include "common/commonTools.h"
#include "common/globalDefinitions.h"
#include "componentStateInformation.h"
#include "components/ComponentController/src/componentStateInformation.h"
#include "components/ComponentController/src/stateManager.h"
#include "include/scenarioControlInterface.h"
#include "include/signalInterface.h"

class AgentInterface;
class ParameterInterface;
class PublisherInterface;
class StochasticsInterface;
class WorldInterface;

ComponentControllerImplementation::ComponentControllerImplementation(
    std::string componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    StochasticsInterface *stochastics,
    WorldInterface *world,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    const CallbackInterface *callbacks,
    AgentInterface *agent,
    std::shared_ptr<ScenarioControlInterface> scenarioControl)
    : UnrestrictedControllStrategyModelInterface(std::move(componentName),
                                                 isInit,
                                                 priority,
                                                 offsetTime,
                                                 responseTime,
                                                 cycleTime,
                                                 stochastics,
                                                 world,
                                                 parameters,
                                                 publisher,
                                                 callbacks,
                                                 agent,
                                                 std::move(scenarioControl)),
      stateManager(callbacks)
{
}

void ComponentControllerImplementation::UpdateInput(int localLinkId,
                                                    const std::shared_ptr<SignalInterface const> &data,
                                                    [[maybe_unused]] int time)
{
  const auto signal = SignalCast<AgentCompToCompCtrlSignal>(data, localLinkId);

  if (stateManager.LocalLinkIdIsRegistered(localLinkId))
  {
    stateManager.UpdateComponentCurrentState(localLinkId, signal->GetCurrentState());
  }
  else
  {
    std::shared_ptr<ComponentStateInformation> componentStateInformation;

    ComponentType componentType = signal->GetComponentType();
    if (componentType == ComponentType::VehicleComponent)
    {
      const auto castedSignal = SignalCast<VehicleCompToCompCtrlSignal>(signal, localLinkId);
      componentStateInformation = std::make_shared<AdasComponentStateInformation>(castedSignal->GetComponentType(),
                                                                                  castedSignal->GetAgentComponentName(),
                                                                                  castedSignal->GetCurrentState(),
                                                                                  castedSignal->GetMovementDomain(),
                                                                                  castedSignal->GetAdasType());
    }
    else
    {
      componentStateInformation = std::make_shared<ComponentStateInformation>(signal->GetComponentType(),
                                                                              signal->GetAgentComponentName(),
                                                                              signal->GetCurrentState(),
                                                                              signal->GetMovementDomain());
    }

    stateManager.AddComponent(localLinkId, componentStateInformation);
  }

  const auto warnings = signal->GetComponentWarnings();
  if (!warnings.empty())
  {
    const auto warningComponent = stateManager.GetComponent(localLinkId);

    driverWarnings.try_emplace(warningComponent->GetComponentName(), warnings);
  }
}

void ComponentControllerImplementation::UpdateOutput(int localLinkId,
                                                     std::shared_ptr<SignalInterface const> &data,
                                                     [[maybe_unused]] int time)
{
  ComponentState maxReachableState = ComponentState::Undefined;

  if (stateManager.LocalLinkIdIsRegistered(localLinkId))
  {
    const auto componentAtLocalLinkId = stateManager.GetComponent(localLinkId);
    maxReachableState = stateManager.GetComponent(localLinkId)->GetMaxReachableState();

    if (componentAtLocalLinkId->GetComponentType() == ComponentType::Driver)
    {
      data = std::make_shared<CompCtrlToDriverCompSignal const>(
          maxReachableState, stateManager.GetVehicleComponentNamesToTypeAndStateMap(), driverWarnings);
      // clear all warnings to forward to avoid unwanted repeats
      driverWarnings.clear();
    }
    else
    {
      data = std::make_shared<CompCtrlToAgentCompSignal const>(
          maxReachableState, stateManager.GetVehicleComponentNamesToTypeAndStateMap());
    }
  }
  else
  {
    data = std::make_shared<CompCtrlToAgentCompSignal const>(maxReachableState,
                                                             stateManager.GetVehicleComponentNamesToTypeAndStateMap());
  }
}

/*
 * Each trigger, pull commands for this agent and pass the list of them
 * to the stateManager for proper handling of changes of component max reachable state
 */
void ComponentControllerImplementation::Trigger([[maybe_unused]] int time)
{
  auto &customCommands = GetScenarioControl()->GetCustomCommands();
  std::vector<std::pair<std::string, ComponentState>> componentStates{};

  for (const auto &customCommand : customCommands)
  {
    auto commandTokens = CommonHelper::TokenizeString(customCommand, ' ');
    auto commandTokensSize = commandTokens.size();
    if (commandTokensSize == 3 && commandTokens.at(0) == "SetComponentState")
    {
      const auto componentName = commandTokens.at(1);
      const auto componentStateName = commandTokens.at(2);
      ComponentState componentState{ComponentState::Undefined};

      if (componentStateName == "Acting")
      {
        componentState = ComponentState::Acting;
      }
      else if (componentStateName == "Disabled")
      {
        componentState = ComponentState::Disabled;
      }
      else if (componentStateName == "Armed")
      {
        componentState = ComponentState::Armed;
      }
      else
      {
        throw std::runtime_error("Component state not valid.");
      }
      componentStates.emplace_back(componentName, componentState);
    }

    // Instruct the stateManager to updateMaxReachableStates
    // - this prioritizes the provided componentStates for command-triggered max reachable states
    // - this also uses registered conditions to determine each component's max reachable
    //   state dependent on each other component's current state
    stateManager.UpdateMaxReachableStatesForRegisteredComponents(componentStates);
  }
}
