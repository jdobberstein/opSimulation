/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <map>
#include <memory>
#include <string>
#include <utility>

#include "common/compCtrlToAgentCompSignal.h"
#include "include/signalInterface.h"

namespace ComponentControl
{

using StateMapping = std::map<std::string, VehicleComponentState>;

/*!
 * \brief This class represents a Condition based on all the information the ComponentController has gathered.
 *
 * The Statemanager uses this to decide which maxReachableState a component has. The different
 * kinds of conditions are subclasses of this class
 */
class Condition
{
public:
  Condition() = default;
  virtual ~Condition() = default;

  /*!
   * \brief Returns whether this Condition is fullfilled.
   * \param componentNameToComponentTypeAndStatesMap    Information the ComponentController has gathered
   * \return returns if it is fullfilled
   */
  virtual bool IsFullfilled(const StateMapping &componentNameToComponentTypeAndStatesMap) const = 0;
};

/*!
 * \brief This class represents an expression that can be evaluated as ComponentState
 *
 * A ComponentStateExpression is either a fixed value or the ComponentState of some
 * named component.
 */
class ComponentStateExpression
{
public:
  ComponentStateExpression() = default;
  virtual ~ComponentStateExpression() = default;

  /*!
   * \brief Evaluates this expression
   * \param componentNameToComponentTypeAndStateMap    Information the ComponentController has gathered
   * \return ComponentState this expression evaluates to
   */
  virtual ComponentState Get(const StateMapping &componentNameToComponentTypeAndStateMap) const = 0;
};

/*!
 * \brief The FixedComponentStateExpression is a ComponentStateExpression that
 * always evaluates to the same ComponentState
 */
class FixedComponentStateExpression : public ComponentStateExpression
{
public:
  /*!
   * \param value The fixed ComponentState
   */
  FixedComponentStateExpression(ComponentState value) : value(value) {}

  ComponentState Get(const StateMapping &componentNameToComponentTypeAndStateMap) const override;

private:
  ComponentState value;
};

/*!
 * \brief The VehicleComponentStateExpression is a ComponentStateExpression that
 * evaluates to the current state of some fixed component
 */
class VehicleComponentStateExpression : public ComponentStateExpression
{
public:
  /*!
   * \param component Name of the component of which the state is evaluated
   */
  VehicleComponentStateExpression(const std::string &component) : component(component) {}

  ComponentState Get(const StateMapping &componentNameToComponentTypeAndStateMap) const override;

private:
  const std::string component;
};

/*!
 * \brief This Condition is fullfilled if two ComponentStateExpressions are
 * evaluated to the same value
 *
 * The ComponentStateEquality is used either to check, whether the state of two components
 * is the same, or to check of the state of some component is equal to some fixed value (e.g. Acting).
 */
class ComponentStateEquality : public Condition
{
public:
  /**
   * @brief Construct a new Component State Equality object
   *
   * @param expression1  Pointer to 1st component state expression
   * @param expression2  Pointer to 2nd component state expression
   */
  ComponentStateEquality(std::unique_ptr<ComponentStateExpression> expression1,
                         std::unique_ptr<ComponentStateExpression> expression2)
  {
    this->expression1 = std::move(expression1);
    this->expression2 = std::move(expression2);
  }

  bool IsFullfilled(const StateMapping &componentNameToComponentTypeAndStateMap) const override;

private:
  std::unique_ptr<ComponentStateExpression> expression1;
  std::unique_ptr<ComponentStateExpression> expression2;
};

}  // namespace ComponentControl
