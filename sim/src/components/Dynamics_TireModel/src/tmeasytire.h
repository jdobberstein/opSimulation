/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef TMEASYTIRE_H
#define TMEASYTIRE_H

#include <units.h>

#include "common/commonTools.h"
#include "common/vector2d.h"

/**
 * \addtogroup Vehicle_Dynamics
 *@{
 * \addtogroup Tire_Model
 * @{
 * \addtogroup TMEasyTireModel
 * \brief TMEasy tire model
 *
 * \details TMEasy Tire Model of vehicle dynamics model. The tire model based on TMEASY by Rill et al.
 *
 * @}
 * @}
 */

/*!
 * \copydoc TMEasyTireModel
 * \ingroup TMEasyTireModel
 */
class Tire
{
public:
  //! Constructor
  //!
  //! @param[in] F_ref            Tire reference vertical force
  //! @param[in] mu_tire_max      Friction coefficient at maximum tire force at reference vertical force
  //! @param[in] mu_tire_slide    Friction coefficient at saturation tire force at reference vertical force
  //! @param[in] s_max            Slip at maximum force at reference vertical force
  //! @param[in] mu_tire_max2     Friction coefficient at maximum tire force at double reference vertical force
  //! @param[in] mu_tire_slide2   Friction coefficient at saturation tire force at double reference vertical force
  //! @param[in] s_max2           Slip at maximum force at double reference vertical force
  //! @param[in] r                Tire radius [m]
  //! @param[in] mu_scale         Scaling factor applied to the friction coefficient
  //! @param[in] s_slide          Saturation slip at reference vertical force
  //! @param[in] s_slide2         Saturation slip at double reference vertical force
  //! @param[in] F0p              Initial slope at current vertical force
  //! @param[in] F0p2             Initial slope at double vertical force
  //! @param[in] positiontire     Tire positions in car CS (Reference: Center of Gravity) [m]
  //! @param[in] rotationVelocity Tire rotation velocity [1/s]
  //! @param[in] Inertia          Tire inertia [kgm2]
  //! @param[in] PneumaticTrail   Tire pneumatic trail [m]
  Tire(units::force::newton_t fRef,
       Common::Vector2d<double> muTireMax,
       Common::Vector2d<double> muTireSlide,
       Common::Vector2d<double> sMax,
       Common::Vector2d<double> muTireMax2,
       Common::Vector2d<double> muTireSlide2,
       Common::Vector2d<double> sMax2,
       units::length::meter_t radius,
       double muScale,
       Common::Vector2d<double> sSlide,
       Common::Vector2d<double> sSlide2,
       Common::Vector2d<units::force::newton_t> f0p,
       Common::Vector2d<units::force::newton_t> f0p2,
       Common::Vector2d<units::length::meter_t> positiontire,
       units::angular_velocity::radians_per_second_t rotationVelocity,
       units::inertia inertia,
       units::length::meter_t pneumaticTrail);

  virtual ~Tire() = default;
  //! Calculation of tire Force (longitudinal & lateral) based on TMEasy tire model
  void CalcTireForce();
  //! Calculation of longitudinal tire slip depending on tire velocity and rotation velocity
  //!
  void CalcLongSlip();
  //! Calculation of lateral tire slip depending on tire velocity and rotation velocity
  //!
  void CalcLatSlip();
  //! Get tire roll friction
  //!
  //! @return                       tire roll friction
  units::force::newton_t GetRollFriction();
  //! Update tire parameters depending on the tire vertical force
  //!
  //! @param[in]     foreZ_update           Current vertical tire force
  void Rescale(units::force::newton_t forceZUpdate);
  //! Calculation of tire rotation acceleration dependung on tire torque and inertia
  //!
  void CalcRotAcc();
  //! Calculation of tire rotation speed depending on tire acceleration and timestep
  //!
  //! @param[in]     dt           integration timestep
  void CalcRotVel(units::time::second_t deltaT);
  //! Calculate tire velocity in tire coordinate system depending on vehicle velocity, yaw velocity and tire position
  //!
  //! @param[in]     yawVelocity           car yaw velocity (center of gravity)
  //! @param[in]     velocityCar           car velocity (center of gravity)
  void CalcVelTire(units::angular_velocity::radians_per_second_t yawVelocity,
                   Common::Vector2d<units::velocity::meters_per_second_t> velocityCar);
  //! Calculation of tire self aligning torquen dependung on tire lateral force and pneumatic trail
  //!
  void CalcSelfAligningTorque();
  //! Get lateral tire force
  //!
  //! @return                       tire lateral force
  [[nodiscard]] units::force::newton_t GetLateralForce() const;
  //! Get longitudinal tire force
  //!
  //! @return                       tire longitudinal force
  [[nodiscard]] units::force::newton_t GetLongitudinalForce() const;
  //! Get tire radius
  //!
  //! @return                       tire radius
  units::length::meter_t GetTireRadius();
  //! Set tire torque
  //!
  //! @param[in]   torque torque on tire
  void SetTorque(units::torque::newton_meter_t torque);
  //! Get tire velocity
  //!
  //! @return                       tire velocity
  Common::Vector2d<units::velocity::meters_per_second_t> GetVelocityTire();
  //! Get tire rotation velocity
  //!
  //! @return                       tire rotation velocity
  units::angular_velocity::radians_per_second_t GetRotationVelocity();
  //! Set tire rotation velocity
  //!
  //! @param[in]  rotVelocity tire rotation velocity
  void SetRotationVelocity(units::angular_velocity::radians_per_second_t rotVelocity);
  //! Get tire rotational acceleration
  //!
  //! @return                       tire rotational acceleration
  [[nodiscard]] units::angular_acceleration::radians_per_second_squared_t GetRotAcceleration() const;
  //! Set tire rotational acceleration
  //!
  //! @param[in]  rotAcceleration tire rotational acceleration
  void SetRotAcceleration(units::angular_acceleration::radians_per_second_squared_t rotAcceleration);
  //! Set tire angle
  //!
  //! @param[in] angle tire angle
  void SetTireAngle(units::angle::radian_t angle);
  //! Get tire self aligning torque
  //!
  //! @return                       tire self aligning torque
  units::torque::newton_meter_t GetSelfAligningTorque();

private:
  //! VerticalTireForce
  units::force::newton_t forceZ;

  /** \addtogroup TMEasyTireModel
   *  @{
   *      \name TireParameters
   *      TMEasy tire parameters
   *      @{
   */
  Common::Vector2d<double> slipPeak;                   //!< slip at maximum force at current vertical force
  Common::Vector2d<double> slipSat;                    //!< saturation slip at current vertical force
  Common::Vector2d<units::force::newton_t> forcePeak;  //!< maximum tire force [N] at current vertical force
  Common::Vector2d<units::force::newton_t> forceSat;   //!< saturation tire force [N] at current vertical force
  Common::Vector2d<double> slipSlide;                  //!< slide slip at current vertical force
  Common::Vector2d<units::force::newton_t> dF0P;       //!< initial slope at current vertical force

  Common::Vector2d<units::force::newton_t> dF0PRef;       //!< initial slope at reference vertical force
  Common::Vector2d<double> slipPeakRef;                   //!< slip at maximum force at reference vertical force
  Common::Vector2d<double> slipSatRef;                    //!< saturation slip at reference vertical force
  Common::Vector2d<units::force::newton_t> forcePeakRef;  //!< maximum tire force at reference vertical force
  Common::Vector2d<units::force::newton_t> forceSatRef;   //!< saturation tire force at reference vertical force
  Common::Vector2d<double> slipSlideRef;                  //!< slide slip at reference vertical force

  Common::Vector2d<units::force::newton_t> dF0P2Ref;       //!< initial slope at double reference vertical force
  Common::Vector2d<double> slipPeak2Ref;                   //!< slip at maximum force at double reference vertical force
  Common::Vector2d<double> slipSat2Ref;                    //!< saturation slip at double reference vertical force
  Common::Vector2d<units::force::newton_t> forcePeak2Ref;  //!< maximum tire force at double reference vertical force
  Common::Vector2d<units::force::newton_t> forceSat2Ref;   //!< saturation tire force at double reference vertical force
  Common::Vector2d<double> slipSlide2Ref;                  //!< slide slip at double reference vertical force
  /**
   *      @}
   *  @}
   */

  units::force::newton_t FRef;  //! tire reference vertical force

  units::length::meter_t radius;                                   //!< tire radius [m]
  units::inertia inertia;                                          //!< tire inertia [kgm2]
  units::length::meter_t pneumaticTrail;                           //!< tire pneumatic trail [m]
  double frictionRoll = 0.01;                                      //!< tire friction roll coefficient
  units::velocity::meters_per_second_t velocityLimit{0.27};        //!< tire velocity limit [m/s]

  units::angular_velocity::radians_per_second_t rotationVelocity;       //!< tire rotation velocity [1/s]
  Common::Vector2d<units::velocity::meters_per_second_t> velocityTire;  //!< tire velocity [m/s]
  units::angle::radian_t angleTire{};                                   //!< tire angle [rad]
  units::torque::newton_meter_t Torque{};                               //!< tire torque [Nm]

  Common::Vector2d<units::length::meter_t>
      positionTire;  //!< tire positions in car CS (Reference: Center of Gravity) [m]

  Common::Vector2d<units::force::newton_t> TireForce;  //!< tire force [N]

  units::torque::newton_meter_t SelfAligningTorque{};  //!< tire self aligning torque due to pneumatic trail

  units::angular_acceleration::radians_per_second_squared_t rotationAcceleration{
      0};  //!< tire rotational acceleration [rad/s^2]

  Common::Vector2d<double> slipNorm;  //!< tire normalized slip
  Common::Vector2d<double> slipTire;  //!< tire slip
};

#endif  // TIRE_TMeasy_H
