/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "tmeasytire.h"

#include <cmath>

Tire::Tire(const units::force::newton_t fRef,
           const Common::Vector2d<double> muTireMax,
           const Common::Vector2d<double> muTireSlide,
           const Common::Vector2d<double> sMax,
           const Common::Vector2d<double> muTireMax2,
           const Common::Vector2d<double> muTireSlide2,
           const Common::Vector2d<double> sMax2,
           const units::length::meter_t radius,
           const double muScale,
           const Common::Vector2d<double> sSlide,
           const Common::Vector2d<double> sSlide2,
           const Common::Vector2d<units::force::newton_t> f0p,
           const Common::Vector2d<units::force::newton_t> f0p2,
           const Common::Vector2d<units::length::meter_t> positiontire,
           const units::angular_velocity::radians_per_second_t rotationVelocity,
           const units::inertia inertia,
           const units::length::meter_t pneumaticTrail)
    : dF0PRef(f0p),
      slipPeakRef(sMax),
      slipSatRef(sSlide),
      dF0P2Ref(f0p2),
      slipPeak2Ref(sMax2),
      slipSat2Ref(sSlide2),
      FRef(fRef),
      radius(radius),
      inertia(inertia),
      pneumaticTrail(pneumaticTrail),
      rotationVelocity(rotationVelocity),
      positionTire(positiontire),
      forceZ(fRef)
{
  // Init Tire parameters

  forcePeakRef.x = fRef * muTireMax.x * muScale;
  forceSatRef.x = fRef * muTireSlide.x * muScale;
  forcePeakRef.y = fRef * muTireMax.y * muScale;
  forceSatRef.y = fRef * muTireSlide.y * muScale;

  forcePeak2Ref.x = 2 * fRef * muTireMax2.x * muScale;
  forceSat2Ref.x = 2 * fRef * muTireSlide2.x * muScale;
  forcePeak2Ref.y = 2 * fRef * muTireMax2.y * muScale;
  forceSat2Ref.y = 2 * fRef * muTireSlide2.y * muScale;
}

void Tire::CalcTireForce()  // Calculate tire longitudinal and lateral force
{
  units::force::newton_t force;

  // Calculate Normalized Tire Slip
  slipNorm.x = forcePeak.x / dF0P.x;
  slipNorm.y = forcePeak.y / dF0P.y;

  // Calculate Longitudinal Tire Slip
  CalcLongSlip();
  // Calculate Lateral Tire Slip
  CalcLatSlip();

  // Calculate Tire Slip
  double const slip = std::sqrt((slipTire.x / slipNorm.x) * (slipTire.x / slipNorm.x)
                                + (slipTire.y / slipNorm.y) * (slipTire.y / slipNorm.y));

  // Get absolute tire slip
  double const slipAbs = std::fabs(slip);

  double const cosPhi = slipTire.x / (slip * slipNorm.x);
  double const sinPhi = slipTire.y / (slip * slipNorm.y);
  units::force::newton_t const dF0 = units::math::sqrt(units::math::pow<2>(dF0P.x * slipNorm.x * cosPhi)
                                                       + units::math::pow<2>(dF0P.y * slipNorm.y * sinPhi));
  double const slipM
      = std::sqrt(std::pow(slipPeak.x / slipNorm.x * cosPhi, 2) + std::pow(slipPeak.y / slipNorm.y * sinPhi, 2));
  units::force::newton_t const forceM
      = units::math::sqrt(units::math::pow<2>(forcePeak.x * cosPhi) + units::math::pow<2>(forcePeak.y * sinPhi));
  double const slipG
      = std::sqrt(std::pow(slipSat.x / slipNorm.x * cosPhi, 2) + std::pow(slipSat.y / slipNorm.y * sinPhi, 2));
  units::force::newton_t const forceG
      = units::math::sqrt(units::math::pow<2>(forceSat.x * cosPhi) + units::math::pow<2>(forceSat.y * sinPhi));

  // Get absolute normalized tire slip
  double const slipAbsNorm = slipAbs / slipM;

  if (slipAbsNorm <= 1.0)
  {  // adhesion

    force = slipAbsNorm * slipM * dF0 / (1 + slipAbsNorm * (slipAbsNorm + dF0 * slipM / forceM - 2));
  }
  else if (slipAbs < slipG)  //(slipAbs < slipSat)
  {                        // adhesion/sliding

    double const sigma = (slipAbs - slipM) / (slipG - slipM);

    force = forceM - (forceM - forceG) * sigma * sigma * (3 - 2 * sigma);
  }
  else
  {  // slide
    force = forceG;
  }
  force = (slip > 0.0) ? force : -force;

  if (std::abs(slip) < std::numeric_limits<double>::epsilon())
  {
    TireForce.x = 0_N;
    TireForce.y = 0_N;
  }
  else
  {
    TireForce.x = (slipTire.x / slipNorm.x) / slip * force;
    TireForce.y = (slipTire.y / slipNorm.y) / slip * force;
  }
}
void Tire::CalcLongSlip()  // Calculate longitudinal tire slip
{
  if (std::abs(velocityTire.x.value() - radius.value() * rotationVelocity.value())
      < std::numeric_limits<double>::epsilon())
  {
    slipTire.x = 0;
  }
  else
  {
    slipTire.x = -(velocityTire.x.value() - radius.value() * rotationVelocity.value())
               / (radius.value() * units::math::abs(rotationVelocity).value() + velocityLimit.value());
  }
}
void Tire::CalcLatSlip()  // Calculate lateral tire slip
{
  slipTire.y
      = -velocityTire.y.value() / (radius.value() * units::math::abs(rotationVelocity).value() + velocityLimit.value());
}

units::force::newton_t Tire::GetRollFriction()  // Calculate tire roll friction
{
  units::force::newton_t forceFriction = forceZ * frictionRoll;

  if (velocityTire.x < 0.0_mps)
  {
    forceFriction *= -1.0;
  }
  if (units::math::abs(velocityTire.x) < velocityLimit)
  {
    forceFriction *= (velocityTire.x / velocityLimit);
  }

  return forceFriction;
}

void Tire::Rescale(const units::force::newton_t forceZUpdate)  // Rescale tire parameters
{
  double const scaling = forceZUpdate / FRef;

  forceZ = forceZUpdate;

  dF0P.x = scaling * (2 * dF0PRef.x - 0.5 * dF0P2Ref.x - (dF0PRef.x - 0.5 * dF0P2Ref.x) * scaling);
  dF0P.y = scaling * (2 * dF0PRef.y - 0.5 * dF0P2Ref.y - (dF0PRef.y - 0.5 * dF0P2Ref.y) * scaling);

  forcePeak.x
      = scaling * (2 * forcePeakRef.x - 0.5 * forcePeak2Ref.x - (forcePeakRef.x - 0.5 * forcePeak2Ref.x) * scaling);
  forcePeak.y
      = scaling * (2 * forcePeakRef.y - 0.5 * forcePeak2Ref.y - (forcePeakRef.y - 0.5 * forcePeak2Ref.y) * scaling);

  forceSat.x = scaling * (2 * forceSatRef.x - 0.5 * forceSat2Ref.x - (forceSatRef.x - 0.5 * forceSat2Ref.x) * scaling);
  forceSat.y = scaling * (2 * forceSatRef.y - 0.5 * forceSat2Ref.y - (forceSatRef.y - 0.5 * forceSat2Ref.y) * scaling);

  slipPeak.x = slipPeakRef.x + (slipPeak2Ref.x - slipPeakRef.x) * (scaling - 1);
  slipPeak.y = slipPeakRef.y + (slipPeak2Ref.y - slipPeakRef.y) * (scaling - 1);

  slipSat.x = slipSatRef.x + (slipSat2Ref.x - slipSatRef.x) * (scaling - 1);
  slipSat.y = slipSatRef.y + (slipSat2Ref.y - slipSatRef.y) * (scaling - 1);
}

void Tire::CalcRotAcc()  // Calculate tire rotational acceleration
{
  rotationAcceleration = 1.0_rad_per_s_sq * (Torque.value() - (TireForce.x.value() * radius.value())) / inertia.value();
}

void Tire::CalcRotVel(units::time::second_t deltaT)  // Calculate tire rotation velocity
{
  rotationVelocity = rotationVelocity + rotationAcceleration * deltaT;
}

void Tire::CalcVelTire(const units::angular_velocity::radians_per_second_t yawVelocity,
                       const Common::Vector2d<units::velocity::meters_per_second_t>
                           velocityCar)  // Calculate tire velocity in tire coordinate system
{
  velocityTire = velocityCar;

  velocityTire.x = velocityTire.x - yawVelocity.value() * positionTire.y.value() * 1_mps;
  velocityTire.y = velocityTire.y + yawVelocity.value() * positionTire.x.value() * 1_mps;
  velocityTire.Rotate(-angleTire);  // tire CS
}

void Tire::CalcSelfAligningTorque()  // Calculate tire self Aligning torque due to pneumatic trail
{
  SelfAligningTorque = TireForce.y * pneumaticTrail;
}

units::force::newton_t Tire::GetLateralForce() const  // Get tire lateral force
{
  return TireForce.y;
};
units::force::newton_t Tire::GetLongitudinalForce() const  // Get tire longitudinal force
{
  return TireForce.x;
};
units::length::meter_t Tire::GetTireRadius()  // Get Tire Radius
{
  return radius;
}
void Tire::SetTorque(units::torque::newton_meter_t torque)  // Set tire torque
{
  Torque = torque;
}
Common::Vector2d<units::velocity::meters_per_second_t> Tire::GetVelocityTire()  // Get tire velocity
{
  return velocityTire;
};
units::angular_velocity::radians_per_second_t Tire::GetRotationVelocity()  // Get tire rotational velocity
{
  return rotationVelocity;
}
units::angular_acceleration::radians_per_second_squared_t Tire::GetRotAcceleration()
    const  // Get tire rotational acceleration
{
  return rotationAcceleration;
}

void Tire::SetTireAngle(units::angle::radian_t angle)  // Set tire angle
{
  angleTire = angle;
}
units::torque::newton_meter_t Tire::GetSelfAligningTorque()  // Get tire self aligning torque
{
  return SelfAligningTorque;
};
void Tire::SetRotAcceleration(
    units::angular_acceleration::radians_per_second_squared_t rotAcceleration)  // Set tire rotational acceleration
{
  rotationAcceleration = rotAcceleration;
};
void Tire::SetRotationVelocity(
    units::angular_velocity::radians_per_second_t rotVelocity)  // Set tire rotational velocity
{
  rotationVelocity = rotVelocity;
};