/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  dynamics_tiremodel.h
//! @brief contains DLL export declarations
//-----------------------------------------------------------------------------

#include "sim/src/common/opExport.h"

#include "include/modelInterface.h"

#if defined(DYNAMICS_TIREMODEL_LIBRARY)
#define DYNAMICS_TIREMODELSHARED_EXPORT OPEXPORT  //!< Export of the dll-functions
#else
#define DYNAMICS_TIREMODELSHARED_EXPORT OPIMPORT  //!< Import of the dll-functions
#endif
