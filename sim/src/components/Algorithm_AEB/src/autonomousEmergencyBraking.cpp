/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2022-2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  autonomousEmergencyBraking.cpp */
//-----------------------------------------------------------------------------

#include "autonomousEmergencyBraking.h"

#include <MantleAPI/Common/orientation.h>
#include <algorithm>
#include <limits>
#include <map>
#include <memory>
#include <new>
#include <osi3/osi_common.pb.h>
#include <ostream>
#include <stdexcept>
#include <units.h>
#include <utility>

#include "common/accelerationSignal.h"
#include "common/boostGeometryCommon.h"
#include "common/commonTools.h"
#include "common/sensorDataSignal.h"
#include "common/sensorFusionQuery.h"
#include "common/vector2d.h"
#include "include/agentInterface.h"
#include "include/callbackInterface.h"
#include "include/dataBufferInterface.h"
#include "include/parameterInterface.h"
#include "include/publisherInterface.h"

class StochasticsInterface;

AlgorithmAutonomousEmergencyBrakingImplementation::AlgorithmAutonomousEmergencyBrakingImplementation(
    std::string componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    StochasticsInterface *stochastics,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    const CallbackInterface *callbacks,
    AgentInterface *agent)
    : AlgorithmInterface(std::move(componentName),
                         isInit,
                         priority,
                         offsetTime,
                         responseTime,
                         cycleTime,
                         stochastics,
                         parameters,
                         publisher,
                         callbacks,
                         agent)
{
  try
  {
    ParseParameters(parameters);
  }
  catch (...)
  {
    const std::string msg = COMPONENTNAME + " could not init parameters";
    LOG(CbkLogLevel::Error, msg);
    throw std::runtime_error(msg);
  }

  try
  {
    if (GetPublisher() == nullptr)
    {
      throw std::runtime_error("");
    }
  }
  catch (...)
  {
    const std::string msg = COMPONENTNAME + " invalid publisher module setup";
    LOG(CbkLogLevel::Error, msg);
    throw std::runtime_error(msg);
  }
}

void AlgorithmAutonomousEmergencyBrakingImplementation::ParseParameters(const ParameterInterface *parameters)
{
  collisionDetectionLongitudinalBoundary
      = units::length::meter_t(parameters->GetParametersDouble().count("CollisionDetectionLongitudinalBoundary") == 1
                                   ? parameters->GetParametersDouble().at("CollisionDetectionLongitudinalBoundary")
                                   : parameters->GetParametersDouble().at("0"));
  collisionDetectionLateralBoundary
      = units::length::meter_t(parameters->GetParametersDouble().count("CollisionDetectionLateralBoundary") == 1
                                   ? parameters->GetParametersDouble().at("CollisionDetectionLateralBoundary")
                                   : parameters->GetParametersDouble().at("1"));
  ttcBrake = units::time::second_t(parameters->GetParametersDouble().count("TTC") == 1
                                       ? parameters->GetParametersDouble().at("TTC")
                                       : parameters->GetParametersDouble().at("2"));
  brakingAcceleration
      = units::acceleration::meters_per_second_squared_t(parameters->GetParametersDouble().count("Acceleration") == 1
                                                             ? parameters->GetParametersDouble().at("Acceleration")
                                                             : parameters->GetParametersDouble().at("3"));

  if (parameters->GetParameterLists().count("SensorLinks") != 0)
  {
    const auto &sensorList = parameters->GetParameterLists().at("SensorLinks");
    for (const auto &sensorLink : sensorList)
    {
      if (sensorLink->GetParametersString().at("InputId") == "Camera")
      {
        sensors.push_back(sensorLink->GetParametersInt().at("SensorId"));
      }
    }
  }
}

void AlgorithmAutonomousEmergencyBrakingImplementation::UpdateInput(int localLinkId,
                                                                    const std::shared_ptr<SignalInterface const> &data,
                                                                    [[maybe_unused]] int time)
{
  std::stringstream log;
  log << COMPONENTNAME << " (component " << GetComponentName() << ", agent " << GetAgent()->GetId()
      << ", input data for local link " << localLinkId << ": ";
  LOG(CbkLogLevel::Debug, log.str());

  //from SensorFusion
  if (localLinkId == 0)
  {
    const std::shared_ptr<SensorDataSignal const> signal = std::dynamic_pointer_cast<SensorDataSignal const>(data);
    if (!signal)
    {
      const std::string msg = COMPONENTNAME + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }

    auto sensorData = signal->sensorData;
    detectedMovingObjects = SensorFusionHelperFunctions::RetrieveMovingObjectsBySensorId(sensors, sensorData);
    detectedStationaryObjects = SensorFusionHelperFunctions::RetrieveStationaryObjectsBySensorId(sensors, sensorData);
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void AlgorithmAutonomousEmergencyBrakingImplementation::UpdateOutput(int localLinkId,
                                                                     std::shared_ptr<SignalInterface const> &data,
                                                                     [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<AccelerationSignal const>(componentState, activeAcceleration, GetComponentName());
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }

  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void AlgorithmAutonomousEmergencyBrakingImplementation::Trigger([[maybe_unused]] int time)
{
  const auto ttc = CalculateTTC();

  if (componentState == ComponentState::Disabled && ShouldBeActivated(ttc))
  {
    componentState = ComponentState::Acting;
    UpdateAcceleration();
  }
  else if (componentState == ComponentState::Acting && ShouldBeDeactivated(ttc))
  {
    componentState = ComponentState::Disabled;
    UpdateAcceleration();
  }
}

bool AlgorithmAutonomousEmergencyBrakingImplementation::ShouldBeActivated(const units::time::second_t ttc) const
{
  return ttc < ttcBrake;
}

bool AlgorithmAutonomousEmergencyBrakingImplementation::ShouldBeDeactivated(const units::time::second_t ttc) const
{
  return ttc > (ttcBrake * 1.5);
}

units::time::second_t AlgorithmAutonomousEmergencyBrakingImplementation::CalculateObjectTTC(
    const osi3::BaseMoving &baseMoving)
{
  auto ego = GetEgoTTCParameters();

  TtcCalculations::TtcParameters opponent{};
  opponent.length = units::length::meter_t(baseMoving.dimension().length()) + collisionDetectionLongitudinalBoundary;
  units::length::meter_t width{baseMoving.dimension().width()};
  units::length::meter_t height{baseMoving.dimension().height()};
  units::angle::radian_t roll{baseMoving.orientation().roll()};
  opponent.widthLeft
      = TrafficHelperFunctions::GetWidthLeft(width, height, roll) + 0.5 * collisionDetectionLateralBoundary;
  opponent.widthRight
      = TrafficHelperFunctions::GetWidthRight(width, height, roll) + 0.5 * collisionDetectionLateralBoundary;
  opponent.frontLength = 0.5 * opponent.length;
  opponent.backLength = 0.5 * opponent.length;
  opponent.position = {baseMoving.position().x(), baseMoving.position().y()};
  opponent.velocityX = units::velocity::meters_per_second_t(baseMoving.velocity().x());
  opponent.velocityY = units::velocity::meters_per_second_t(baseMoving.velocity().y());
  opponent.accelerationX = units::acceleration::meters_per_second_squared_t(baseMoving.acceleration().x());
  opponent.accelerationY = units::acceleration::meters_per_second_squared_t(baseMoving.acceleration().y());
  opponent.yaw = units::angle::radian_t(baseMoving.orientation().yaw());
  opponent.yawRate = units::angular_velocity::radians_per_second_t(baseMoving.orientation_rate().yaw());
  opponent.yawAcceleration
      = units::angular_acceleration::radians_per_second_squared_t(baseMoving.orientation_acceleration().yaw());

  return TtcCalculations::CalculateObjectTTC(ego, opponent, ttcBrake * 1.5, GetCycleTime());
}

units::time::second_t AlgorithmAutonomousEmergencyBrakingImplementation::CalculateObjectTTC(
    const osi3::BaseStationary &baseStationary)
{
  auto ego = GetEgoTTCParameters();

  TtcCalculations::TtcParameters opponent{};
  opponent.length
      = units::length::meter_t(baseStationary.dimension().length()) + collisionDetectionLongitudinalBoundary;
  units::length::meter_t width{baseStationary.dimension().width()};
  units::length::meter_t height{baseStationary.dimension().height()};
  units::angle::radian_t roll{baseStationary.orientation().roll()};
  opponent.widthLeft
      = TrafficHelperFunctions::GetWidthLeft(width, height, roll) + 0.5 * collisionDetectionLateralBoundary;
  opponent.widthRight
      = TrafficHelperFunctions::GetWidthRight(width, height, roll) + 0.5 * collisionDetectionLateralBoundary;
  opponent.frontLength = 0.5 * opponent.length;
  opponent.backLength = 0.5 * opponent.length;
  opponent.position = {baseStationary.position().x(), baseStationary.position().y()};
  opponent.velocityX = -GetAgent()->GetVelocity().Length();
  opponent.velocityY = 0.0_mps;
  opponent.accelerationX = -GetAgent()->GetAcceleration().Projection(GetAgent()->GetYaw());
  opponent.accelerationY = 0.0_mps_sq;
  opponent.yaw = units::angle::radian_t(baseStationary.orientation().yaw());
  opponent.yawRate = 0.0_rad_per_s;
  opponent.yawAcceleration = 0.0_rad_per_s_sq;

  return TtcCalculations::CalculateObjectTTC(ego, opponent, ttcBrake * 1.5, GetCycleTime());
}

units::time::second_t AlgorithmAutonomousEmergencyBrakingImplementation::CalculateTTC()
{
  units::time::second_t ttc{std::numeric_limits<double>::max()};
  for (const auto &detectedObject : detectedMovingObjects)
  {
    auto objectTtc = CalculateObjectTTC(detectedObject.base());
    if (objectTtc < ttc)
    {
      ttc = objectTtc;
    }
  }
  for (const auto &detectedObject : detectedStationaryObjects)
  {
    auto objectTtc = CalculateObjectTTC(detectedObject.base());
    if (objectTtc < ttc)
    {
      ttc = objectTtc;
    }
  }

  return ttc;
}

void AlgorithmAutonomousEmergencyBrakingImplementation::SetAcceleration(
    units::acceleration::meters_per_second_squared_t setValue)
{
  activeAcceleration = setValue;
  GetPublisher()->Publish(COMPONENTNAME,
                          ComponentEvent({{"ComponentState", openpass::utils::to_string(componentState)}}));
}

void AlgorithmAutonomousEmergencyBrakingImplementation::UpdateAcceleration()
{
  if (componentState == ComponentState::Acting && (activeAcceleration - brakingAcceleration) != 0.0_mps_sq)
  {
    SetAcceleration(brakingAcceleration);
  }
  else if (componentState == ComponentState::Disabled && activeAcceleration != 0.0_mps_sq)
  {
    SetAcceleration(0.0_mps_sq);
  }
}

TtcCalculations::TtcParameters AlgorithmAutonomousEmergencyBrakingImplementation::GetEgoTTCParameters()
{
  TtcCalculations::TtcParameters ego{};
  ego.length = GetAgent()->GetLength() + collisionDetectionLongitudinalBoundary;
  auto width = GetAgent()->GetWidth();
  auto height = GetAgent()->GetHeight();
  auto roll = GetAgent()->GetRoll();

  ego.widthLeft = TrafficHelperFunctions::GetWidthLeft(width, height, roll) + 0.5 * collisionDetectionLateralBoundary;
  ego.widthRight = TrafficHelperFunctions::GetWidthRight(width, height, roll) + 0.5 * collisionDetectionLateralBoundary;
  ego.frontLength = GetAgent()->GetDistanceReferencePointToLeadingEdge() + 0.5 * collisionDetectionLongitudinalBoundary;
  ego.backLength = ego.length - ego.frontLength;
  ego.position = {0.0, 0.0};
  ego.velocityX = 0.0_mps;
  ego.velocityY = 0.0_mps;
  ego.accelerationX = 0.0_mps_sq;
  ego.accelerationY = 0.0_mps_sq;
  ego.yaw = 0.0_rad;
  ego.yawRate
      = GetAgent()
            ->GetYawRate();  //This is a temporary solution, since the moving object osi is filled with dummy nans here
  ego.yawAcceleration = 0.0_rad_per_s_sq;  // GetAgent()->GetYawAcceleration() not implemented yet

  return ego;
}
