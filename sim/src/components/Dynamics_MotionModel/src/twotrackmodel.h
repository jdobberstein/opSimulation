/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef TWOTRACKMODEL_H
#define TWOTRACKMODEL_H

#include <array>
#include <units.h>
#include <vector>

#include "common/commonTools.h"
#include "common/vector2d.h"

/** \addtogroup Vehicle_Dynamics
 *@{
 * \addtogroup Motion_Model
 * @{
 * \addtogroup Two track model
 * \brief  Simple two-track vehicle motion model
 *
 * \details The two track model is used to calculate the vehicle dynamics based on the input values of tire angle and
 *torque
 *
 *  @}
 * @} */

/*!
 * \copydoc Two track model
 * \ingroup Two track model
 */
class TwoTrackVehicleModel
{
public:
  TwoTrackVehicleModel();
  ~TwoTrackVehicleModel();

  /*!
   * \brief Initialize vehicle properties
   *
   * @param[in]    axles     axles of vehicle
   * @param[in]    xCog     X Position of Center of Gravity
   * @param[in]    yCog     Y Position of Center of Gravity
   * @param[in]    weight     weight
   * @param[in]    coeffDrag  coefficient of drag
   * @param[in]    areaFace   area vehicle front
   * @param[in]    bbcenter  bounding box center
   */
  void InitVehicleProperties(std::vector<mantle_api::Axle> axles,
                             units::length::meter_t xCog,
                             units::length::meter_t yCog,
                             units::mass::kilogram_t weight,
                             double coeffDrag,
                             units::area::square_meter_t areaFace,
                             units::length::meter_t bbCenter);

  /*!
   * \brief Set tire self aligning torque
   *
   * @param[in]    longitudinalTireForce     tire force X
   * @param[in]    lateralTireForce          tire force Y
   */
  void SetTireForce(std::vector<double> longitudinalTireForce, std::vector<double> lateralTireForce);
  /*!
   * \brief Set tire angle
   *
   * @param[in]    tireAngle     tire sangle
   */
  void SetTireAngle(std::vector<double> tireAngle);
  /*!
   * \brief Set tire self aligning torque
   *
   * @param[in]    selfAligningTorque     tire self aligning torque
   */
  void SetTireSelfAligningTorque(std::vector<double> selfAligningTorque);
  /*!
   * \brief Set vehicle yaw rate
   *
   * @param[in]    yawVelocity     vehicle yaw rate
   */
  void SetYawVelocity(units::angular_velocity::radians_per_second_t yawVelocity) { this->yawVelocity = yawVelocity; }
  /*!
   * \brief Set vehicle yaw acceleration
   *
   * @param[in]    yawAcceleration    vehicle yaw acceleration
   */
  void SetYawAcceleration(units::angular_acceleration::radians_per_second_squared_t yawAcceleration)
  {
    this->yawAcceleration = yawAcceleration;
  }
  /*!
   * \brief Set vehicle heading
   *
   * @param[in]    yawAngle   vehicle heading
   */
  void SetYawAngle(units::angle::radian_t yawAngle) { this->yawAngle = yawAngle; }
  /*!
   * \brief Set vehicle accleration
   *
   * @param[in]    acceleration vehicle acceleration in vehicle coordinate system
   */
  void SetAcceleration(Common::Vector2d<units::acceleration::meters_per_second_squared_t> acceleration)
  {
    this->acceleration = acceleration;  // vehicle CS
  }
  /*!
   * \brief Set vehicle velocity
   *
   * @param[in]    velocity   vehicle velocity in vehicle coordinate system
   */
  void SetVelocity(Common::Vector2d<units::velocity::meters_per_second_t> velocity)
  {
    this->velocity = velocity;  // vehicle CS
  }
  /*!
   * \brief Set vehicle position of COG
   *
   * @param[in]    position   vehicle position of COG
   */
  void SetPosition(Common::Vector2d<units::length::meter_t> position) { this->position = position; }
  /*!
   * \brief Get moment around z axis
   *
   * @return Moment Z
   */
  units::torque::newton_meter_t GetMomentZ() { return momentTotalZ; }
  /*!
   * \brief Get yaw angle
   *
   * @return Vehicle yaw angle
   */
  units::angle::radian_t GetYawAngle() { return yawAngle; }
  /*!
   * \brief Get yaw rate
   *
   * @return Vehicle yaw rate
   */
  units::angular_velocity::radians_per_second_t GetYawVelocity() { return yawVelocity; }
  /*!
   * \brief Get yaw acceleration
   *
   * @return Vehicle yaw accelertaion
   */
  units::angular_acceleration::radians_per_second_squared_t GetYawAcceleration() { return yawAcceleration; }
  /*!
   * \brief Get position
   *
   * @return vehicle position in vehicle coordinate system (COG)
   */
  Common::Vector2d<units::length::meter_t> GetPosition() { return position; }
  /*!
   * \brief Get position of vehicle COG
   *
   * @return Position COG in reference to vehicle rear axle
   */
  Common::Vector2d<units::length::meter_t> GetPositionCOG() { return PositionCOG; }
  /*!
   * \brief Get velocity
   *
   * @return Vehicle velocity
   */
  Common::Vector2d<units::velocity::meters_per_second_t> GetVelocity() { return velocity; }
  /*!
   * \brief Get acceleration
   *
   * @return Vehicle acceleration
   */
  Common::Vector2d<units::acceleration::meters_per_second_squared_t> GetAcceleration() { return acceleration; }

  /*!
   * \brief Get total force on vehicle COG
   *
   * @return Force on vehicle COG
   */
  Common::Vector2d<units::force::newton_t> GetForceTotalXY() { return forceTotalXY; }

  /*!
   * \brief calculate total force on vehicle COG
   *
   */
  void ForceGlobal();

private:
  /** \name  Vehicle Parameters
   *    @{
   */
  //! Position COG [m] in refernece to vehicle rear axle
  Common::Vector2d<units::length::meter_t> PositionCOG;

  //! Mass of the car [kg]
  units::mass::kilogram_t massTotal{};
  //! Tire positions in COG CS [m]
  std::vector<Common::Vector2d<units::length::meter_t>> positionTire;
  /**
   *  @}
   */

  /** \name  Inputs
   *    @{
   */
  //! tire forces in X & Y in local tire CS
  std::vector<Common::Vector2d<units::force::newton_t>> tireForce;
  //! tire self aligning torque
  std::vector<units::torque::newton_meter_t> wheelSelfAligningTorque;
  /**
   *  @}
   */

  /** \name Constants
   *    @{
   */
  //! Drag coefficient []
  double coeffDrag{};
  //! Face area [m^2]
  units::area::square_meter_t areaFace{};
  //! constant air density [kg/m^3]
  units::density::kilograms_per_cubic_meter_t densityAir = 1.29_kg_per_cu_m;
  /**
   *  @}
   */

  /** \name Dynamics
   *    @{
   */
  //! current vehicle yaw rate of COG [rad/s]
  units::angular_velocity::radians_per_second_t yawVelocity{};
  //! current vehicle yaw acceleration of COG [rad/s²]
  units::angular_acceleration::radians_per_second_squared_t yawAcceleration{};
  //! current vehicle yaw angle of COG [rad]
  units::angle::radian_t yawAngle{};
  //! current vehicle velocity of COG [m/s]
  Common::Vector2d<units::velocity::meters_per_second_t> velocity;
  //! current vehicle acceleration of COG [m/s²]
  Common::Vector2d<units::acceleration::meters_per_second_squared_t> acceleration;
  //! current vehicle position of COG [m]
  Common::Vector2d<units::length::meter_t> position;
  //! current tire angle [rad]
  std::vector<units::angle::radian_t> tireAngle;
  //! Total force on vehicle's COG
  Common::Vector2d<units::force::newton_t> forceTotalXY;
  //! Total momentum on the vehicle around the z-axes
  units::torque::newton_meter_t momentTotalZ{};
  /**
   *    @}
   */
};

#endif  // VEHICLESIMPLETT_H
