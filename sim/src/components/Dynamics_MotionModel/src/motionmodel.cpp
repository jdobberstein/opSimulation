/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** @addtogroup Motion_Model
 * Abbreviations:
 * - COG = center-of-gravity
 * - COS = coordinate system
 */

/**
 * @ingroup Motion_Model
 * @defgroup init_tt Initialization
 */
/**
 * @ingroup Motion_Model
 * @defgroup sim_step_00_tt Entry
 */
/**
 * @ingroup Motion_Model
 * @defgroup sim_step_10_tt Process
 */
/**
 * @ingroup Motion_Model
 * @defgroup sim_step_20_tt Output
 */

#include "motionmodel.h"

#include <boost/format.hpp>
#include <memory>
#include <utility>

#include "common/commonTools.h"
#include "components/common/vehicleProperties.h"

DynamicsMotionModelImplementation::DynamicsMotionModelImplementation(std::string componentName,
                                                                     bool isInit,
                                                                     int priority,
                                                                     int offsetTime,
                                                                     int responseTime,
                                                                     int cycleTime,
                                                                     StochasticsInterface *stochastics,
                                                                     WorldInterface *world,
                                                                     const ParameterInterface *parameters,
                                                                     PublisherInterface *const publisher,
                                                                     const CallbackInterface *callbacks,
                                                                     AgentInterface *agent)
    : UnrestrictedModelInterface(std::move(componentName),
                                 isInit,
                                 priority,
                                 offsetTime,
                                 responseTime,
                                 cycleTime,
                                 stochastics,
                                 world,
                                 parameters,
                                 publisher,
                                 callbacks,
                                 agent),
      timeStep(static_cast<double>(GetCycleTime()) / 1000.0),
      vehicle(new TwoTrackVehicleModel())
{
  LOGINFO((boost::format("Constructing Dynamics_MotionModel_TwoTrack for agent %d...") % agent->GetId()).str());

  vehicle->SetYawVelocity(0.0_rad_per_s);
  vehicle->SetYawAcceleration(0.0_rad_per_s_sq);

  const mantle_api::VehicleProperties vehicleProperties
      = *std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters());

  auto weight = GetAgent()->GetVehicleModelParameters()->mass;

  auto coeffDrag = helper::map::query((vehicleProperties.properties), Properties::Vehicle::AIR_DRAG_COEFFICIENT);
  THROWIFFALSE(coeffDrag.has_value(), "AirDragCoefficient was not defined in VehicleCatalog");

  auto areaFace = helper::map::query(vehicleProperties.properties, Properties::Vehicle::FRONT_SURFACE);
  THROWIFFALSE(areaFace.has_value(), "FrontSurface was not defined in VehicleCatalog");

  units::length::meter_t xcog;
  units::length::meter_t ycog;

  std::optional<std::string> const xPositionCog
      = helper::map::query(vehicleProperties.properties, Properties::Vehicle::POSITION_COG_X);
  std::optional<std::string> const yPositionCog
      = helper::map::query(vehicleProperties.properties, Properties::Vehicle::POSITION_COG_Y);

  if (xPositionCog.has_value())
  {
    xcog = std::stod(xPositionCog.value()) * 1_m;
  }
  else
  {
    xcog = units::math::abs(vehicleProperties.rear_axle.bb_center_to_axle_center.x) / 2;
  }
  if (yPositionCog.has_value())
  {
    ycog = std::stod(yPositionCog.value()) * 1_m;
  }
  else
  {
    ycog = 0_m;
  }

  // get front & rear axle of vehicle
  std::vector<mantle_api::Axle> axles;
  axles.resize(2);
  axles[0] = vehicleProperties.front_axle;
  axles[1] = vehicleProperties.rear_axle;

  const units::length::meter_t bbCenter = units::math::abs(vehicleProperties.bounding_box.geometric_center.x);

  /** @addtogroup init_tt
   *   Init vehicle's properties:
   *  - set the longitudinal position of the COG
   *  - set the wheelbase
   *  - set the track width
   *  - set vehicle mass
   *  - set coefficient of drag
   *  - set front surface of vehicle
   */
  vehicle->InitVehicleProperties(
      axles, xcog, ycog, weight, std::stod(coeffDrag.value()), std::stod(areaFace.value()) * 1_sq_m, bbCenter);
  ReadPreviousState();

  dynamicsSignal.lateralController = "DynamicsMotionModel";
  dynamicsSignal.longitudinalController = "DynamicsMotionModel";

  LOGINFO("Constructing Dynamics_MotionModel successful");
}

DynamicsMotionModelImplementation::~DynamicsMotionModelImplementation()
{
  delete (vehicle);
}

void DynamicsMotionModelImplementation::UpdateInput(int localLinkId,
                                                      const std::shared_ptr<SignalInterface const> &data,
                                                      [[maybe_unused]] int time)
{
  std::stringstream log;
  log << componentname << " UpdateInput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  if (localLinkId != 5)
  {
    const bool success = inputPorts.at(localLinkId)->SetSignalValue(data);

    if (success)
    {
      log << componentname << " UpdateInput successful";
      LOG(CbkLogLevel::Debug, log.str());
    }
    else
    {
      log << componentname << " UpdateInput failed";
      LOG(CbkLogLevel::Error, log.str());
    }
  }
  else
  {
    const std::shared_ptr<SteeringSignal const> signal = std::dynamic_pointer_cast<SteeringSignal const>(data);
    if (!signal)
    {
      const std::string msg = componentname + "_" + std::to_string(GetAgent()->GetId()) + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
    steeringWheelAngle = signal->steeringWheelAngle;
  }
}

void DynamicsMotionModelImplementation::UpdateOutput(int localLinkId,
                                                       std::shared_ptr<SignalInterface const> &data,
                                                       [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<SignalVectorDouble const>(forceGlobalInertia);
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = componentname + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else if (localLinkId == 1)
  {
    try
    {
      data = std::make_shared<DynamicsSignal const>(dynamicsSignal);
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = componentname + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else
  {
    const std::string msg = componentname + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void DynamicsMotionModelImplementation::Trigger([[maybe_unused]] int time)
{
  ReadPreviousState();

  /** @addtogroup sim_step_00_tt
   * Update vehicle's input data:
   *  - tire angle
   *  - tire forces
   */
  vehicle->SetTireAngle(wheelAngle.GetValue());
  vehicle->SetTireForce(longitudinalTireForces.GetValue(), lateralTireForces.GetValue());
  vehicle->SetTireSelfAligningTorque(wheelSelfAligningTorque.GetValue());

  /** @addtogroup sim_step_10_tt
   * Combine local tire forces to a global force at the vehicle's body:
   *  - total longitudinal force
   *  - total lateral force
   *  - air drag
   *  - total rotational momentum
   */
  vehicle->ForceGlobal();

  /** @addtogroup sim_step_20_tt
   * Perform a translational Euler step:
   *  - calculate next vehicle's position from prevoius velocity values
   *  - calculate new velocity from previous acceleration values
   *  - calculate new acceleration from global forces
   */
  NextStateTranslation();

  /** @addtogroup sim_step_20_tt
   * Perform a rotational Euler step:
   *  - calculate vehicle's orientation from previous rotational velocity
   *  - calculate vehicle's rotational velocity from previous rotational acceleration
   *  - calculate vehicle's rotational acceleration from the total rotational momentum
   */
  NextStateRotation();
  /** @addtogroup sim_step_20_tt
   * Write actual vehicle's state:
   *  - global position (cartesian coordinates)
   *  - direction of vehicle's longitudinal axes (angle in polar coordinates)
   *  - vehicle's longitudinal and lateral velocity in vehicle's CS
   *  - vehicle's rotational velociy
   *  - vehicle's longitudinal and lateral acceleration in vehicle's CS
   *  - vehicle's rotational acceleration
   *  - inertia forces on vehicle's COG
   */

  NextStateSet();
}

void DynamicsMotionModelImplementation::ReadPreviousState()
{
  vehicle->SetYawAngle(GetAgent()->GetYaw());

  Common::Vector2d<units::length::meter_t> positionCar;
  Common::Vector2d<units::velocity::meters_per_second_t> velocityCar;
  Common::Vector2d<units::acceleration::meters_per_second_squared_t> accelerationCar;

  positionCar.x = GetAgent()->GetPositionX()
                + ((vehicle->GetPositionCOG().x) * units::math::cos(vehicle->GetYawAngle())
                   - (vehicle->GetPositionCOG().y)
                         * units::math::sin(vehicle->GetYawAngle()));  // position of vehicle'S COG in global CS
  positionCar.y = GetAgent()->GetPositionY()
                + ((vehicle->GetPositionCOG().x) * units::math::sin(vehicle->GetYawAngle())
                   + (vehicle->GetPositionCOG().y)
                         * units::math::cos(vehicle->GetYawAngle()));  // position of vehicle's COG in global CS

  vehicle->SetPosition(positionCar);

  // global CS
  vehicle->SetYawAcceleration(GetAgent()->GetYawAcceleration());
  vehicle->SetYawVelocity(GetAgent()->GetYawRate());

  velocityCar.x = GetAgent()->GetVelocity().x
                - 1_mps * vehicle->GetYawVelocity().value() * (vehicle->GetPositionCOG().y.value());  // global
  velocityCar.y = GetAgent()->GetVelocity().y
                + 1_mps * vehicle->GetYawVelocity().value() * (vehicle->GetPositionCOG().x.value());  // global
  velocityCar.Rotate(-vehicle->GetYawAngle());                                                        // vehicle CS

  vehicle->SetVelocity(velocityCar);
  accelerationCar = GetAgent()->GetAcceleration();  // global

  accelerationCar.x = accelerationCar.x
                    + 1_mps_sq * (-vehicle->GetYawAcceleration().value() * (vehicle->GetPositionCOG().y.value()))
                    + 1_mps_sq
                          * (-vehicle->GetYawVelocity().value() * vehicle->GetYawVelocity().value()
                             * (vehicle->GetPositionCOG().x.value()));
  accelerationCar.y = accelerationCar.y
                    + 1_mps_sq * (vehicle->GetYawAcceleration().value() * (vehicle->GetPositionCOG().x.value()))
                    + 1_mps_sq
                          * (-vehicle->GetYawVelocity().value() * vehicle->GetYawVelocity().value()
                             * (vehicle->GetPositionCOG().y.value()));

  accelerationCar.Rotate(-vehicle->GetYawAngle());  // vehicle CS

  vehicle->SetAcceleration(accelerationCar);
  LOGDEBUG((boost::format("Prev Velocity for Dynamics_MotionModel_TwoTrack for agent %d: %f, %f, %f")
            % GetAgent()->GetId() % vehicle->GetVelocity().x % vehicle->GetVelocity().y % vehicle->GetYawVelocity())
               .str());
  LOGDEBUG((boost::format("Prev Acceleration for Dynamics_MotionModel_TwoTrack for agent %d: %f, %f, %f")
            % GetAgent()->GetId() % vehicle->GetAcceleration().x % vehicle->GetAcceleration().y
            % vehicle->GetYawAcceleration())
               .str());
}

void DynamicsMotionModelImplementation::NextStateTranslation()
{
  // update position COG (constant velocity step) in global COS
  Common::Vector2d<units::velocity::meters_per_second_t> velocity = vehicle->GetVelocity();
  velocity.Rotate(vehicle->GetYawAngle());
  vehicle->SetPosition({vehicle->GetPosition().x + velocity.x * (timeStep * 1_s),
                        vehicle->GetPosition().y + velocity.y * (timeStep * 1_s)});

  // update velocity
  const Common::Vector2d<units::velocity::meters_per_second_t> velocityCarNew
      = {vehicle->GetVelocity().x + vehicle->GetAcceleration().x * timeStep * 1_s,
         vehicle->GetVelocity().y + vehicle->GetAcceleration().y * timeStep * 1_s};

  vehicle->SetVelocity(velocityCarNew);

  // update acceleration
  vehicle->SetAcceleration({vehicle->GetForceTotalXY().x * (1 / GetAgent()->GetVehicleModelParameters()->mass),
                            vehicle->GetForceTotalXY().y * (1 / GetAgent()->GetVehicleModelParameters()->mass)});
}

void DynamicsMotionModelImplementation::NextStateRotation()
{
  // preserve directions of velocity and acceleration
  Common::Vector2d velocity = vehicle->GetVelocity();
  Common::Vector2d acceleration = vehicle->GetAcceleration();
  velocity.Rotate(vehicle->GetYawAngle());
  acceleration.Rotate(vehicle->GetYawAngle());

  // update yaw angle
  vehicle->SetYawAngle(
      units::math::fmod(vehicle->GetYawAngle() + timeStep * 1_s * vehicle->GetYawVelocity(), 2 * M_PI * 1_rad));

  // update yaw velocity
  const units::angular_velocity::radians_per_second_t yawVelocityNew
      = vehicle->GetYawVelocity() + vehicle->GetYawAcceleration() * timeStep * 1_s;
  vehicle->SetYawVelocity(yawVelocityNew);

  // update acceleration
  auto momentInertiaYaw = helper::map::query(GetAgent()->GetVehicleModelParameters()->properties, "MomentInertiaYaw");
  THROWIFFALSE(momentInertiaYaw.has_value(), "MomentInertiaYaw was not defined in VehicleCatalog");

  vehicle->SetYawAcceleration(1_rad_per_s_sq * vehicle->GetMomentZ().value() / std::stod(momentInertiaYaw.value()));

  // reassign directions of velocity and acceleration
  velocity.Rotate(-vehicle->GetYawAngle());
  acceleration.Rotate(-vehicle->GetYawAngle());

  vehicle->SetVelocity(velocity);
  vehicle->SetAcceleration(acceleration);
}

void DynamicsMotionModelImplementation::NextStateSet()
{
  // update position (transformation COG-> middle rear axle)
  dynamicsSignal.dynamicsInformation.positionX
      = vehicle->GetPosition().x + (-vehicle->GetPositionCOG().x) * units::math::cos(vehicle->GetYawAngle())
      - (-vehicle->GetPositionCOG().y) * units::math::sin(vehicle->GetYawAngle());
  dynamicsSignal.dynamicsInformation.positionY
      = vehicle->GetPosition().y + (-vehicle->GetPositionCOG().x) * units::math::sin(vehicle->GetYawAngle())
      + (-vehicle->GetPositionCOG().y) * units::math::cos(vehicle->GetYawAngle());

  // update global velocity
  Common::Vector2d<units::velocity::meters_per_second_t> velocityCar = vehicle->GetVelocity();
  velocityCar.Rotate(vehicle->GetYawAngle());  // global CS Reference COG

  // Transformation COG -> middle rear axle
  dynamicsSignal.dynamicsInformation.velocityX
      = velocityCar.x - vehicle->GetYawVelocity().value() * (-vehicle->GetPositionCOG().y.value() * 1_mps);
  dynamicsSignal.dynamicsInformation.velocityY
      = velocityCar.y + vehicle->GetYawVelocity().value() * (-vehicle->GetPositionCOG().x.value() * 1_mps);

  // update yaw
  dynamicsSignal.dynamicsInformation.yaw = vehicle->GetYawAngle();
  // update yawRate
  dynamicsSignal.dynamicsInformation.yawRate = vehicle->GetYawVelocity();
  // update yawAcceleration
  dynamicsSignal.dynamicsInformation.yawAcceleration = vehicle->GetYawAcceleration();

  // update global acceleration
  Common::Vector2d acceleration = vehicle->GetAcceleration();
  acceleration.Rotate(vehicle->GetYawAngle());

  dynamicsSignal.dynamicsInformation.accelerationX
      = acceleration.x + 1_mps_sq * (-vehicle->GetYawAcceleration().value() * (-vehicle->GetPositionCOG().y.value()))
      + 1_mps_sq
            * (-vehicle->GetYawVelocity().value() * vehicle->GetYawVelocity().value()
               * (-vehicle->GetPositionCOG().x.value()));

  dynamicsSignal.dynamicsInformation.accelerationY
      = acceleration.y + 1_mps_sq * (vehicle->GetYawAcceleration().value() * (-vehicle->GetPositionCOG().x.value()))
      + 1_mps_sq
            * (-vehicle->GetYawVelocity().value() * vehicle->GetYawVelocity().value()
               * (-vehicle->GetPositionCOG().y.value()));

  //set wheel yaw
  dynamicsSignal.dynamicsInformation.wheelYaw.resize(wheelAngle.GetValue().size());
  for (unsigned int idx = 0; idx < wheelAngle.GetValue().size(); idx++)
  {
    dynamicsSignal.dynamicsInformation.wheelYaw[idx] = units::angle::radian_t(wheelAngle.GetValue()[idx]);
  }

  //set wheel rotation rate
  dynamicsSignal.dynamicsInformation.wheelRotationRate.resize(wheelRotationRate.GetValue().size());
  for (unsigned int idx = 0; idx < wheelRotationRate.GetValue().size(); idx++)
  {
    dynamicsSignal.dynamicsInformation.wheelRotationRate[idx]
        = units::angular_velocity::radians_per_second_t(wheelRotationRate.GetValue()[idx]);
  }

  //set steering wheel angle
  dynamicsSignal.dynamicsInformation.steeringWheelAngle = steeringWheelAngle;

  // update forceGlobaInertia
  forceGlobalInertia = {-vehicle->GetForceTotalXY().x.value(), -vehicle->GetForceTotalXY().y.value()};

  LOGDEBUG((boost::format("Setting Acceleration by Dynamics_MotionModel_TwoTrack for agent %d: %f, %f, %f")
            % GetAgent()->GetId() % vehicle->GetAcceleration().x % vehicle->GetAcceleration().y
            % vehicle->GetYawAcceleration())
               .str());
}
