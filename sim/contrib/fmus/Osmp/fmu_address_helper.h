/*
 * Based on
 * PMSF FMU Framework for FMI 2.0 Co-Simulation FMUs
 *
 * (C) 2016 -- 2018 PMSF IT Consulting Pierre R. Mai
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "fmi2TypesPlatform.h"

void* decode_integer_to_pointer(fmi2Integer hi, fmi2Integer lo);

void encode_pointer_to_integer(const void* ptr, fmi2Integer& hi, fmi2Integer& lo);
