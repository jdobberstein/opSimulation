/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "OSMPSensorDataToTrafficUpdateStepper.h"

#include <cmath>
#include <sstream>

#include "../fmu_address_helper.h"
#include "osi3/osi_sensordata.pb.h"
#include "osi3/osi_sensorviewconfiguration.pb.h"
#include "osi3/osi_trafficcommand.pb.h"
#include "osi3/osi_trafficupdate.pb.h"

#define DEBUGBREAK()

#ifdef PRIVATE_LOG_PATH
std::ofstream OSMPSensorDataToTrafficUpdateStepper::private_log_file;
#endif

/*
 * Ctor, Dtor
 */

OSMPSensorDataToTrafficUpdateStepper::OSMPSensorDataToTrafficUpdateStepper(fmi2String theinstanceName,
                                                                           fmi2Type thefmuType,
                                                                           fmi2String thefmuGUID,
                                                                           fmi2String thefmuResourceLocation,
                                                                           const fmi2CallbackFunctions *thefunctions,
                                                                           fmi2Boolean thevisible,
                                                                           fmi2Boolean theloggingOn)
    : instanceName(theinstanceName),
      fmuType(thefmuType),
      fmuGUID(thefmuGUID),
      fmuResourceLocation(thefmuResourceLocation),
      functions(*thefunctions),
      visible(thevisible != 0),
      loggingOn(theloggingOn != 0),
      simulation_started(false)
{
}

OSMPSensorDataToTrafficUpdateStepper::~OSMPSensorDataToTrafficUpdateStepper() = default;

/* fmi interface */

fmi2Status OSMPSensorDataToTrafficUpdateStepper::SetDebugLogging(fmi2Boolean theloggingOn,
                                                                 size_t nCategories,
                                                                 const fmi2String categories[])
{
  fmi_verbose_log("fmi2SetDebugLogging(%s)", theloggingOn ? "true" : "false");
  loggingOn = theloggingOn != 0;
  if (categories && (nCategories > 0))
  {
    loggingCategories.clear();
    for (size_t i = 0; i < nCategories; i++)
    {
      if (strcmp(categories[i], "FMI") == 0)
        loggingCategories.insert("FMI");
      else if (strcmp(categories[i], "OSMP") == 0)
        loggingCategories.insert("OSMP");
      else if (strcmp(categories[i], "OSI") == 0)
        loggingCategories.insert("OSI");
    }
  }
  else
  {
    loggingCategories.clear();
    loggingCategories.insert("FMI");
    loggingCategories.insert("OSMP");
    loggingCategories.insert("OSI");
  }
  return fmi2OK;
}

fmi2Component OSMPSensorDataToTrafficUpdateStepper::Instantiate(fmi2String instanceName,
                                                                fmi2Type fmuType,
                                                                fmi2String fmuGUID,
                                                                fmi2String fmuResourceLocation,
                                                                const fmi2CallbackFunctions *functions,
                                                                fmi2Boolean visible,
                                                                fmi2Boolean loggingOn)
{
  auto *myc = new OSMPSensorDataToTrafficUpdateStepper(
      instanceName, fmuType, fmuGUID, fmuResourceLocation, functions, visible, loggingOn);

  if (myc == nullptr)
  {
    fmi_verbose_log_global("fmi2Instantiate(\"%s\",%d,\"%s\",\"%s\",\"%s\",%d,%d) = NULL (alloc failure)",
                           instanceName,
                           fmuType,
                           fmuGUID,
                           (fmuResourceLocation != NULL) ? fmuResourceLocation : "<NULL>",
                           "FUNCTIONS",
                           visible,
                           loggingOn);
    return nullptr;
  }

  if (myc->doInit() != fmi2OK)
  {
    fmi_verbose_log_global(R"(fmi2Instantiate("%s",%d,"%s","%s","%s",%d,%d) = NULL (doInit failure))",
                           instanceName,
                           fmuType,
                           fmuGUID,
                           (fmuResourceLocation != nullptr) ? fmuResourceLocation : "<nullptr>",
                           "FUNCTIONS",
                           visible,
                           loggingOn);
    delete myc;
    return nullptr;
  }
  else
  {
    fmi_verbose_log_global(R"(fmi2Instantiate("%s",%d,"%s","%s","%s",%d,%d) = %p)",
                           instanceName,
                           fmuType,
                           fmuGUID,
                           (fmuResourceLocation != nullptr) ? fmuResourceLocation : "<nullptr>",
                           "FUNCTIONS",
                           visible,
                           loggingOn,
                           myc);
    return (fmi2Component)myc;
  }
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::SetupExperiment(fmi2Boolean toleranceDefined,
                                                                 fmi2Real tolerance,
                                                                 fmi2Real startTime,
                                                                 fmi2Boolean stopTimeDefined,
                                                                 fmi2Real stopTime)
{
  fmi_verbose_log(
      "fmi2SetupExperiment(%d,%g,%g,%d,%g)", toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime);
  return doStart(toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime);
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::EnterInitializationMode()
{
  fmi_verbose_log("fmi2EnterInitializationMode()");
  return doEnterInitializationMode();
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::ExitInitializationMode()
{
  fmi_verbose_log("fmi2ExitInitializationMode()");
  simulation_started = true;
  return doExitInitializationMode();
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::DoStep(fmi2Real currentCommunicationPoint,
                                                        fmi2Real communicationStepSize,
                                                        fmi2Boolean noSetFMUStatePriorToCurrentPointfmi2Component)
{
  fmi_verbose_log("fmi2DoStep(%g,%g,%d)",
                  currentCommunicationPoint,
                  communicationStepSize,
                  noSetFMUStatePriorToCurrentPointfmi2Component);
  return doCalc(currentCommunicationPoint, communicationStepSize, noSetFMUStatePriorToCurrentPointfmi2Component);
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::Terminate()
{
  fmi_verbose_log("fmi2Terminate()");
  return doTerm();
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::Reset()
{
  fmi_verbose_log("fmi2Reset()");

  doFree();
  simulation_started = false;
  return doInit();
}

void OSMPSensorDataToTrafficUpdateStepper::FreeInstance()
{
  fmi_verbose_log("fmi2FreeInstance()");
  doFree();
}

/* FMI Getter & Setter */

fmi2Status OSMPSensorDataToTrafficUpdateStepper::GetReal(const fmi2ValueReference vr[], size_t nvr, fmi2Real value[])
{
  fmi_verbose_log("fmi2GetReal(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_REAL_VARS)
      value[i] = real_vars[vr[i]];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::GetInteger(const fmi2ValueReference vr[],
                                                            size_t nvr,
                                                            fmi2Integer value[])
{
  fmi_verbose_log("fmi2GetInteger(...)");
  bool need_refresh = !simulation_started;
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_INTEGER_VARS)
    {
      if (need_refresh
          && (vr[i] == FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX
              || vr[i] == FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX
              || vr[i] == FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX))
      {
        refresh_fmi_sensor_view_config_request();
        need_refresh = false;
      }
      value[i] = integer_vars[vr[i]];
    }
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::GetBoolean(const fmi2ValueReference vr[],
                                                            size_t nvr,
                                                            fmi2Boolean value[])
{
  fmi_verbose_log("fmi2GetBoolean(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_BOOLEAN_VARS)
      value[i] = boolean_vars[vr[i]];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::GetString(const fmi2ValueReference vr[],
                                                           size_t nvr,
                                                           fmi2String value[])
{
  fmi_verbose_log("fmi2GetString(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_STRING_VARS)
      value[i] = string_vars[vr[i]].c_str();
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::SetReal(const fmi2ValueReference vr[],
                                                         size_t nvr,
                                                         const fmi2Real value[])
{
  fmi_verbose_log("fmi2SetReal(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_REAL_VARS)
    {
      real_vars[vr[i]] = value[i];
#ifdef FMI_REAL_SENSOR_VIEW_RANGE
      if (vr[i] == FMI_REAL_SENSOR_VIEW_RANGE) sensorViewRange = value[i];
#endif
    }
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::SetInteger(const fmi2ValueReference vr[],
                                                            size_t nvr,
                                                            const fmi2Integer value[])
{
  fmi_verbose_log("fmi2SetInteger(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_INTEGER_VARS)
      integer_vars[vr[i]] = value[i];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::SetBoolean(const fmi2ValueReference vr[],
                                                            size_t nvr,
                                                            const fmi2Boolean value[])
{
  fmi_verbose_log("fmi2SetBoolean(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_BOOLEAN_VARS)
      boolean_vars[vr[i]] = value[i];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::SetString(const fmi2ValueReference vr[],
                                                           size_t nvr,
                                                           const fmi2String value[])
{
  fmi_verbose_log("fmi2SetString(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_STRING_VARS)
      string_vars[vr[i]] = value[i];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

void OSMPSensorDataToTrafficUpdateStepper::set_fmi_sensor_view_config_request(const osi3::SensorViewConfiguration &data)
{
  data.SerializeToString(&currentConfigRequestBuffer);
  encode_pointer_to_integer(currentConfigRequestBuffer.data(),
                            integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX],
                            integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX]);
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX] = (fmi2Integer)currentConfigRequestBuffer.size();
  normal_log("OSMP",
             "Providing %08X %08X, writing %d bytes from %p to set SensorViewConfiguration ...",
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX],
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX],
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX],
             currentConfigRequestBuffer.data());
  using std::swap;
  swap(currentConfigRequestBuffer, lastConfigRequestBuffer);
}

void OSMPSensorDataToTrafficUpdateStepper::reset_fmi_sensor_view_config_request()
{
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX] = 0;
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX] = 0;
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX] = 0;
}

bool OSMPSensorDataToTrafficUpdateStepper::get_fmi_sensor_view_config(osi3::SensorViewConfiguration &data)
{
  if (integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_SIZE_IDX] <= 0) return false;

  void *buffer = decode_integer_to_pointer(integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASEHI_IDX],
                                           integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASELO_IDX]);
  normal_log("OSMP",
             "Got %08X %08X, reading %d bytes from %p to get SensorViewConfiguration ...",
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASEHI_IDX],
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASELO_IDX],
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_SIZE_IDX],
             buffer);
  if (!data.ParseFromArray(buffer, integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_SIZE_IDX]))
  {
    normal_log("OSMP", "Could not deserialize SensorViewConfiguration");
    return false;
  }
  return true;
}

void OSMPSensorDataToTrafficUpdateStepper::refresh_fmi_sensor_view_config_request()
{
  osi3::SensorViewConfiguration config;
  if (get_fmi_sensor_view_config(config))
    set_fmi_sensor_view_config_request(config);
  else
  {
    config.Clear();
    config.mutable_version()->CopyFrom(
        osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version));
    // We want a full view of the environment, in all directions
    config.set_field_of_view_horizontal(2 * M_PI);
    config.set_field_of_view_vertical(2 * M_PI);
    // 200m view. This primarily needs to be far enough to react to vehicles early enough
    config.set_range(200);
    // update rate: 50Hz (20ms). Must be identical to stepSize in the
    // modelDescription.xml.
    // Note: the model doesn't care that much about the exact cycle time.
    config.mutable_update_cycle_time()->set_seconds(0);
    config.mutable_update_cycle_time()->set_nanos(20000000);
    config.mutable_update_cycle_offset()->Clear();
    // The model doesn't need any road information from the sensor view,
    // since it already has this information from the GroundTruthInit
    // message. This can greatly improve performance.
    //config.set_omit_static_information(true);
    set_fmi_sensor_view_config_request(config);
  }
}

/* Implementation proxies */

fmi2Status OSMPSensorDataToTrafficUpdateStepper::doInit()
{
  DEBUGBREAK();

  for (int &boolean_var : boolean_vars) boolean_var = fmi2False;

  for (int &integer_var : integer_vars) integer_var = 0;

  for (double &real_var : real_vars) real_var = 0.0;

  for (auto &string_var : string_vars) string_var = "";

  set_fmi_nominal_range(135.0);  // TODO: what is this doing?
  return fmi2OK;
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::doStart(fmi2Boolean toleranceDefined,
                                                         fmi2Real tolerance,
                                                         fmi2Real startTime,
                                                         fmi2Boolean stopTimeDefined,
                                                         fmi2Real stopTime)
{
  DEBUGBREAK();

  return fmi2OK;
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::doEnterInitializationMode()
{
  DEBUGBREAK();

  return fmi2OK;
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::doExitInitializationMode()
{
  DEBUGBREAK();

  // normal_log("OSMP", "SensorViewRange parameter: %f", sensorViewRange);

  osi3::SensorViewConfiguration config;
  if (false /*!get_fmi_sensor_view_config(config)*/)
    normal_log(
        "OSI",
        "Received no valid SensorViewConfiguration from Simulation Environment, assuming everything checks out.");
  else
  {
    normal_log("OSI", "Received SensorViewConfiguration for Sensor Id %llu", config.sensor_id().value());
    normal_log("OSI",
               "SVC Ground Truth FoV Horizontal %f, FoV Vertical %f, Range %f",
               config.field_of_view_horizontal(),
               config.field_of_view_vertical(),
               config.range());
    normal_log("OSI",
               "SVC Mounting Position: (%f, %f, %f)",
               config.mounting_position().position().x(),
               config.mounting_position().position().y(),
               config.mounting_position().position().z());
    normal_log("OSI",
               "SVC Mounting Orientation: (%f, %f, %f)",
               config.mounting_position().orientation().roll(),
               config.mounting_position().orientation().pitch(),
               config.mounting_position().orientation().yaw());
  }

  return fmi2OK;
}

auto calculateCollisionTimes(const osi3::MovingObject &movingObject, double xIntersection) -> std::pair<double, double>
{
  const auto halfLength = movingObject.base().dimension().length() / 2.0;
  const auto dx = movingObject.base().velocity().x();
  const auto dy = movingObject.base().velocity().y();
  const auto norm = std::sqrt(dx * dx + dy * dy);
  const auto xFront = movingObject.base().position().x() + dx / norm * halfLength;
  const auto xBack = movingObject.base().position().x() - dx / norm * halfLength;
  return std::make_pair(std::abs((xIntersection - xFront) / dx), std::abs((xIntersection - xBack) / dx));
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::doCalc(fmi2Real currentCommunicationPoint,
                                                        fmi2Real communicationStepSize,
                                                        fmi2Boolean noSetFMUStatePriorToCurrentPointfmi2Component)
{
  DEBUGBREAK();

  osi3::SensorData currentSensorDataIn;
  osi3::TrafficUpdate currentTrafficUpdateOut;
  auto time = currentCommunicationPoint;

  normal_log("OSI",
             "Calculating 'Sensor Data To Traffic Update Stepper' at %f for %f (step size %f)",
             currentCommunicationPoint,
             time,
             communicationStepSize);

  currentTrafficUpdateOut.Clear();
  currentTrafficUpdateOut.mutable_version()->CopyFrom(
      osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version));
  currentTrafficUpdateOut.mutable_timestamp()->set_seconds(static_cast<::google::protobuf::int64>(std::floor(time)));
  currentTrafficUpdateOut.mutable_timestamp()->set_nanos(
      static_cast<::google::protobuf::uint32>((time - std::floor(time)) * 1000000000.0));

  // Get sensor view (ignored in MS1)
  if (!get_fmi_sensor_data_in(currentSensorDataIn))
  {
    normal_log("OSI", "No valid sensor data input, therefore providing no valid output.");
    reset_fmi_traffic_update_out();
    set_fmi_valid(false);
    return fmi2OK;
  }

  auto sensorView = currentSensorDataIn.sensor_view(0);
  osi3::MovingObject &movingObject = *sensorView.mutable_global_ground_truth()->mutable_moving_object()->begin();
  const auto position = movingObject.base().position();
  const auto velocity = movingObject.base().velocity();

  auto positionX = position.x();
  auto positionY = position.y();
  auto newX = position.x();
  auto newY = position.y();
  normal_log("OSI", "Old position: (%f|%f)", newX, newY);
  if (time == 0.0)
  {
    newX = std::round(newX);
    newY = std::round(newY);
    positionX = std::round(positionX);
    positionY = std::round(positionY);
  }
  newX += .01;
  newY += .01;
  normal_log("OSI", "New position: (%f|%f)", newX, newY);

  currentTrafficUpdateOut.add_update()->CopyFrom(movingObject);
  const auto baseMoving = currentTrafficUpdateOut.mutable_update(0)->mutable_base();

  auto trafficUpdatePosition = baseMoving->mutable_position();
  trafficUpdatePosition->set_x(newX);
  trafficUpdatePosition->set_y(newY);

  auto orientation = baseMoving->mutable_orientation();
  orientation->set_yaw(currentSensorDataIn.host_vehicle_location().orientation().yaw());

  auto trafficUpdateVelocity = baseMoving->mutable_velocity();
  auto newVelocityX = (newX - positionX) / communicationStepSize;
  auto newVelocityY = (newY - positionY) / communicationStepSize;
  trafficUpdateVelocity->set_x(newVelocityX);
  trafficUpdateVelocity->set_y(newVelocityY);
  trafficUpdateVelocity->set_z(0);

  auto trafficUpdateAcceleration = baseMoving->mutable_acceleration();
  trafficUpdateAcceleration->set_x((newVelocityX - velocity.x()) / communicationStepSize);
  trafficUpdateAcceleration->set_y((newVelocityY - velocity.y()) / communicationStepSize);
  trafficUpdateVelocity->set_z(0);

  //orientation is copied as the same
  auto trafficUpdateOrientationRate = baseMoving->mutable_orientation_rate();
  trafficUpdateOrientationRate->set_yaw(0);

  auto trafficUpdateOrientationAcceleration = baseMoving->mutable_orientation_acceleration();
  trafficUpdateOrientationAcceleration->set_yaw(0);

  movingObject.mutable_vehicle_attributes()->set_steering_wheel_angle(0);

  set_fmi_traffic_update_out(currentTrafficUpdateOut);
  set_fmi_valid(true);
  private_log_file.flush();

  return fmi2OK;
}

fmi2Status OSMPSensorDataToTrafficUpdateStepper::doTerm()
{
  DEBUGBREAK();

  return fmi2OK;
}

void OSMPSensorDataToTrafficUpdateStepper::doFree()
{
  DEBUGBREAK();

  if (private_log_file.is_open())
  {
    private_log_file.flush();
    private_log_file.close();
  }
}

bool OSMPSensorDataToTrafficUpdateStepper::get_fmi_sensor_data_in(osi3::SensorData &sensorData)
{
  if (integer_vars[FMI_INTEGER_SENSORDATA_IN_SIZE_IDX] > 0)
  {
    void *buffer = decode_integer_to_pointer(integer_vars[FMI_INTEGER_SENSORDATA_IN_BASEHI_IDX],
                                             integer_vars[FMI_INTEGER_SENSORDATA_IN_BASELO_IDX]);
    normal_log("OSMP",
               "Got %08X %08X, reading from %p ...",
               integer_vars[FMI_INTEGER_SENSORDATA_IN_BASEHI_IDX],
               integer_vars[FMI_INTEGER_SENSORDATA_IN_BASELO_IDX],
               buffer);
    return sensorData.ParseFromArray(buffer, integer_vars[FMI_INTEGER_SENSORDATA_IN_SIZE_IDX]);
  }
  else
  {
    return false;
  }
}

void OSMPSensorDataToTrafficUpdateStepper::set_fmi_traffic_update_out(const osi3::TrafficUpdate &data)
{
  data.SerializeToString(&currentOutputBuffer);
  encode_pointer_to_integer(currentOutputBuffer.data(),
                            integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASEHI_IDX],
                            integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASELO_IDX]);
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_SIZE_IDX] = (fmi2Integer)currentOutputBuffer.length();
  normal_log("OSMP",
             "Providing %08X %08X, writing from %p ...",
             integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASEHI_IDX],
             integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASELO_IDX],
             currentOutputBuffer.data());
  swap(currentOutputBuffer, lastOutputBuffer);
}

void OSMPSensorDataToTrafficUpdateStepper::reset_fmi_traffic_update_out()
{
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_SIZE_IDX] = 0;
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASEHI_IDX] = 0;
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASELO_IDX] = 0;
}

extern "C"
{
  FMI2_Export const char *fmi2GetTypesPlatform()
  {
    return fmi2TypesPlatform;
  }

  FMI2_Export const char *fmi2GetVersion()
  {
    return fmi2Version;
  }

  FMI2_Export fmi2Status fmi2SetDebugLogging(fmi2Component c,
                                             fmi2Boolean loggingOn,
                                             size_t nCategories,
                                             const fmi2String categories[])
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->SetDebugLogging(loggingOn, nCategories, categories);
  }

  /*
   * Functions for Co-Simulation
   */
  FMI2_Export fmi2Component fmi2Instantiate(fmi2String instanceName,
                                            fmi2Type fmuType,
                                            fmi2String fmuGUID,
                                            fmi2String fmuResourceLocation,
                                            const fmi2CallbackFunctions *functions,
                                            fmi2Boolean visible,
                                            fmi2Boolean loggingOn)
  {
    return OSMPSensorDataToTrafficUpdateStepper::Instantiate(
        instanceName, fmuType, fmuGUID, fmuResourceLocation, functions, visible, loggingOn);
  }

  FMI2_Export fmi2Status fmi2SetupExperiment(fmi2Component c,
                                             fmi2Boolean toleranceDefined,
                                             fmi2Real tolerance,
                                             fmi2Real startTime,
                                             fmi2Boolean stopTimeDefined,
                                             fmi2Real stopTime)
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->SetupExperiment(toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime);
  }

  FMI2_Export fmi2Status fmi2EnterInitializationMode(fmi2Component c)
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->EnterInitializationMode();
  }

  FMI2_Export fmi2Status fmi2ExitInitializationMode(fmi2Component c)
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->ExitInitializationMode();
  }

  FMI2_Export fmi2Status fmi2DoStep(fmi2Component c,
                                    fmi2Real currentCommunicationPoint,
                                    fmi2Real communicationStepSize,
                                    fmi2Boolean noSetFMUStatePriorToCurrentPointfmi2Component)
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->DoStep(currentCommunicationPoint, communicationStepSize, noSetFMUStatePriorToCurrentPointfmi2Component);
  }

  FMI2_Export fmi2Status fmi2Terminate(fmi2Component c)
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->Terminate();
  }

  FMI2_Export fmi2Status fmi2Reset(fmi2Component c)
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->Reset();
  }

  FMI2_Export void fmi2FreeInstance(fmi2Component c)
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    myc->FreeInstance();
    delete myc;
  }

  /*
   * Data Exchange Functions
   */
  FMI2_Export fmi2Status fmi2GetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Real value[])
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->GetReal(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2GetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[])
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->GetInteger(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2GetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[])
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->GetBoolean(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2GetString(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2String value[])
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->GetString(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Real value[])
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->SetReal(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetInteger(fmi2Component c,
                                        const fmi2ValueReference vr[],
                                        size_t nvr,
                                        const fmi2Integer value[])
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->SetInteger(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetBoolean(fmi2Component c,
                                        const fmi2ValueReference vr[],
                                        size_t nvr,
                                        const fmi2Boolean value[])
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->SetBoolean(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetString(fmi2Component c,
                                       const fmi2ValueReference vr[],
                                       size_t nvr,
                                       const fmi2String value[])
  {
    OSMPSensorDataToTrafficUpdateStepper *myc = (OSMPSensorDataToTrafficUpdateStepper *)c;
    return myc->SetString(vr, nvr, value);
  }

  /*
   * Unsupported Features (FMUState, Derivatives, Async DoStep, Status Enquiries)
   */
  FMI2_Export fmi2Status fmi2GetFMUstate(fmi2Component c, fmi2FMUstate *FMUstate)
  {
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SetFMUstate(fmi2Component c, fmi2FMUstate FMUstate)
  {
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2FreeFMUstate(fmi2Component c, fmi2FMUstate *FMUstate)
  {
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SerializedFMUstateSize(fmi2Component c, fmi2FMUstate FMUstate, size_t *size)
  {
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SerializeFMUstate(fmi2Component c,
                                               fmi2FMUstate FMUstate,
                                               fmi2Byte serializedState[],
                                               size_t size)
  {
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2DeSerializeFMUstate(fmi2Component c,
                                                 const fmi2Byte serializedState[],
                                                 size_t size,
                                                 fmi2FMUstate *FMUstate)
  {
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2GetDirectionalDerivative(fmi2Component c,
                                                      const fmi2ValueReference vUnknown_ref[],
                                                      size_t nUnknown,
                                                      const fmi2ValueReference vKnown_ref[],
                                                      size_t nKnown,
                                                      const fmi2Real dvKnown[],
                                                      fmi2Real dvUnknown[])
  {
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SetRealInputDerivatives(
      fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Integer order[], const fmi2Real value[])
  {
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2GetRealOutputDerivatives(
      fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Integer order[], fmi2Real value[])
  {
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2CancelStep(fmi2Component c)
  {
    return fmi2OK;
  }

  FMI2_Export fmi2Status fmi2GetStatus(fmi2Component c, const fmi2StatusKind s, fmi2Status *value)
  {
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetRealStatus(fmi2Component c, const fmi2StatusKind s, fmi2Real *value)
  {
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetIntegerStatus(fmi2Component c, const fmi2StatusKind s, fmi2Integer *value)
  {
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetBooleanStatus(fmi2Component c, const fmi2StatusKind s, fmi2Boolean *value)
  {
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetStringStatus(fmi2Component c, const fmi2StatusKind s, fmi2String *value)
  {
    return fmi2Discard;
  }
}
