/*********************************************************************
 * This Example Content is intended to demonstrate usage of
 * Eclipse technology. It is provided to you under the terms
 * and conditions of the Eclipse Distribution License v1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php
 **********************************************************************/

//-----------------------------------------------------------------------------
//! @file  algorithm_Comp1_global.h
//! @brief This file contains DLL export declarations
//-----------------------------------------------------------------------------

#ifndef ALGORITHM_COMP1_GLOBAL_H
#define ALGORITHM_COMP1_GLOBAL_H

#include "sim/src/common/opExport.h"

#if defined(ALGORITHM_COMP1_LIBRARY)
#define ALGORITHM_COMP1_SHARED_EXPORT OPEXPORT
#else
#define ALGORITHM_COMP1_SHARED_EXPORT OPIMPORT
#endif

#endif  // ALGORITHM_COMP1_GLOBAL_H
