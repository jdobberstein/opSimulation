/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#ifndef CONNECTIONINTERFACE_H
#define CONNECTIONINTERFACE_H

#include <list>
#include <map>
#include <string>

#include "roadElementTypes.h"

/// @brief  class representing connection interface
class ConnectionInterface
{
public:
  ConnectionInterface() = default;
  ConnectionInterface(const ConnectionInterface&) = delete;
  ConnectionInterface(ConnectionInterface&&) = delete;
  ConnectionInterface& operator=(const ConnectionInterface&) = delete;
  ConnectionInterface& operator=(ConnectionInterface&&) = delete;
  virtual ~ConnectionInterface() = default;

  /**
   * Add a lane link between two roads
   *
   * @param from incoming road lane id
   * @param to   connecting road lane id
   */
  virtual void AddLink(int from, int to) = 0;

  /// @return Get Id of the connecting road
  virtual const std::string& GetConnectingRoadId() const = 0;

  /// @return Get Id of the incoming Road
  virtual const std::string& GetIncommingRoadId() const = 0;

  /// @return Get the map of links
  virtual const std::map<int, int>& GetLinks() const = 0;

  /// @return Get the type of the contact point
  virtual ContactPointType GetContactPoint() const = 0;
};

#endif  // CONNECTIONINTERFACE_H
