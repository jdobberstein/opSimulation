/********************************************************************************
 * Copyright (c) 2018-2020 in-tech GmbH
 *               2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  fmuHandlerInterface.h
//! @brief This file contains the interface for FMU interaction
//-----------------------------------------------------------------------------

#pragma once

#include <FMI1/fmi1_functions.h>
#include <FMI1/fmi1_import_variable_list.h>
#include <FMI2/fmi2_functions.h>
#include <memory>
#include <osi3/osi_trafficcommand.pb.h>
#include <unordered_map>
#include <variant>
#include <vector>

#include "components/Algorithm_FmuWrapper/src/variant_visitor.h"
#include "include/callbackInterface.h"
#include "include/signalInterface.h"
#include "include/stochasticsInterface.h"

static constexpr size_t FMI1 = 0;
static constexpr size_t FMI2 = 1;

class AgentInterface;
struct fmu_check_data_t;

namespace osi3
{
class SensorData;
class TrafficUpdate;
}  //namespace osi3

/// @brief enum class of different type of variable type
enum class VariableType
{
  Bool,
  Int,
  Double,
  String,
  Enum
};

typedef std::pair<int, VariableType> ValueReferenceAndType;  ///< Pair of variable value and its type

using fmi_status_t = std::variant<fmi1_status_t, fmi2_status_t>;  ///< Status of FMI
using fmi_value_reference_t
    = std::variant<fmi1_value_reference_t, fmi2_value_reference_t>;  ///< Value reference holding either of fmi1 or fmi2
using fmi_value_references_t
    = std::variant<std::vector<fmi1_value_reference_t>,
                   std::vector<fmi2_value_reference_t>>;  ///< value reference holding either of fmi1 or fmi2 references

using fmi_causality_enu_t
    = std::variant<fmi1_causality_enu_t, fmi2_causality_enu_t>;  ///< Causality holding either of fmi1 or fmi2 causality
using fmi_variability_enu_t
    = std::variant<fmi1_variability_enu_t, fmi2_variability_enu_t>;  ///< FMI variability enumerator holding either of
                                                                     ///< fmi1 or fmi2 variablity enumerator

using OsiSource
    = std::variant<std::monostate,
                   const osi3::SensorData*,
                   const osi3::TrafficUpdate*>;  ///< OSI message as source for a signal (monostate, if no OSI source)

/// @brief Structure to represent all FMU1 variables
struct FmuVariable1
{
  int valueReference;                  ///< Value of the reference
  VariableType variableType;           ///< Type of the variable
  fmi1_causality_enu_t causality;      ///< Causality property for variables
  fmi1_variability_enu_t variability;  ///< variability property for variables

  /// @brief Create a fmu variable2 type
  /// @param valueReference Value of the reference
  /// @param variableType   Type of the variable
  /// @param causality      Causality property for variables
  /// @param variability    variability property for variables
  FmuVariable1(int valueReference,
               VariableType variableType,
               fmi1_causality_enu_t causality,
               fmi1_variability_enu_t variability)
      : valueReference(valueReference), variableType(variableType), causality(causality), variability(variability)
  {
  }

  /// @brief Get the pair of value reference and the variable type
  /// @return Returns pair of value reference and the variable type
  std::pair<int, VariableType> getRefTypePair() { return std::make_pair(valueReference, variableType); }

  /// @brief Get the FMU value reference and its ype
  /// @return Return value reference and type
  ValueReferenceAndType GetValueReferenceAndType()
  {
    ValueReferenceAndType valueReferenceAndType;
    valueReferenceAndType.first = valueReference;
    valueReferenceAndType.second = variableType;

    return valueReferenceAndType;
  }

  /// @return True, if read at initialization point
  bool ReadAtInit()
  {
    /*
    bool read = variability != fmi1_variability_enu_constant;
    read &= (causality == fmi1_causality_enu_output ||
             causality == fmi1_causality_enu_internal);
     */

    // We have to read in everything at initialization for fmu handler to work properly
    return true;
  }

  /// @return True, if read at triggering point
  bool ReadAtTrigger()
  {
    bool read = (variability == fmi1_variability_enu_discrete || variability == fmi1_variability_enu_continuous);

    read &= (causality == fmi1_causality_enu_output || causality == fmi1_causality_enu_internal);

    return read;
  }

  /// @return True, if write at initialization point
  bool WriteAtInit()
  {
    bool write = variability != fmi1_variability_enu_constant;
    write &= (causality == fmi1_causality_enu_input);
    return write;
  }

  /// @return True, if write at triggering point
  bool WriteAtTrigger()
  {
    bool write = (variability == fmi1_variability_enu_discrete ||  //
                  variability == fmi1_variability_enu_continuous);
    write &= (causality == fmi1_causality_enu_input);
    return write;
  }

  /// @return Return if the causality is equal to the parameter
  bool IsParameter()
  {
    bool parameter = variability == fmi1_variability_enu_parameter;
    parameter &= (causality == fmi1_causality_enu_input || causality == fmi1_causality_enu_internal);
    return parameter;
  }

  /// @return Return if the causality is equal to the calculated parameter
  bool IsCalculatedParameter()
  {
    bool parameter = variability == fmi1_variability_enu_parameter;
    parameter &= (causality == fmi1_causality_enu_output || causality == fmi1_causality_enu_internal);
    return parameter;
  }
};

/// @brief Structure representing FMU2 variables
struct FmuVariable2
{
  int valueReference;                  ///< Value of the reference
  VariableType variableType;           ///< Type of the variable
  fmi2_causality_enu_t causality;      ///< Causality property for variables
  fmi2_variability_enu_t variability;  ///< variability property for variables

  /// @brief Create a fmu variable2 type
  /// @param valueReference Value of the reference
  /// @param variableType   Type of the variable
  /// @param causality      Causality property for variables
  /// @param variability    variability property for variables
  FmuVariable2(int valueReference,
               VariableType variableType,
               fmi2_causality_enu_t causality,
               fmi2_variability_enu_t variability)
      : valueReference(valueReference), variableType(variableType), causality(causality), variability(variability)
  {
  }

  /// @brief Get the pair of value reference and the variable type
  /// @return Returns pair of value reference and the variable type
  std::pair<int, VariableType> getRefTypePair() { return std::make_pair(valueReference, variableType); }

  /// @brief Get the FMU value reference and its ype
  /// @return Return value reference and type
  ValueReferenceAndType GetValueReferenceAndType()
  {
    ValueReferenceAndType valueReferenceAndType;
    valueReferenceAndType.first = valueReference;
    valueReferenceAndType.second = variableType;
    return valueReferenceAndType;
  }

  /// @return True, if read at initialization point
  bool ReadAtInit() { return true; }

  /// @return True, if read at triggering point
  bool ReadAtTrigger()
  {
    bool read = (variability == fmi2_variability_enu_discrete ||    //
                 variability == fmi2_variability_enu_continuous ||  //
                 variability == fmi2_variability_enu_tunable);
    read &= (causality == fmi2_causality_enu_output ||  //
             causality == fmi2_causality_enu_calculated_parameter);

    return read;
  }

  /// @return True, if write at initialization point
  bool WriteAtInit()
  {
    bool write = variability != fmi2_variability_enu_constant;
    write &= (causality == fmi2_causality_enu_input ||  //
              causality == fmi2_causality_enu_parameter);

    return write;
  }

  /// @return True, if write at triggering point
  bool WriteAtTrigger()
  {
    bool write = (variability == fmi2_variability_enu_discrete ||    //
                  variability == fmi2_variability_enu_continuous ||  //
                  variability == fmi2_variability_enu_tunable);
    write &= (causality == fmi2_causality_enu_input);

    return write;
  }

  /// @return Return if the causality is equal to the parameter
  bool IsParameter() { return causality == fmi2_causality_enu_parameter; }

  /// @return Return if the causality is equal to the calculated parameter
  bool IsCalculatedParameter() { return causality == fmi2_causality_enu_calculated_parameter; }
};

using Fmu1Variables
    = std::unordered_map<std::string, FmuVariable1>;  ///< FMU1 variable mapping different fmu variablie1 and its type
using Fmu2Variables
    = std::unordered_map<std::string, FmuVariable2>;  ///< FMU1 variable mapping different fmu variablie2 and its type
using FmuVariables
    = std::variant<Fmu1Variables, Fmu2Variables>;  ///< FMU variable holding either of fmu1 or fmu2 variable

//! Common storage for values from FMUs. User has to take care of accessing the correct datatype.
union FmuValue
{
  bool boolValue;           ///< FMU value of type boolean
  int intValue;             ///< FMU value of type integer
  double realValue;         ///< FMU value of type type
  const char* stringValue;  ///< FMU value of type string
};

/*!
 * \brief The FmuHandlerInterface class provides an interface for implementations of the FMU type-specific parts of the FmuWrapper
 */
class FmuHandlerInterface
{
public:
  /// @brief Create a FMU handler interface object
  /// @param componentName    Name of the FMU handler interface component
  /// @param cdata            FMU check data
  /// @param agent            Pointer to the agent interface
  /// @param stochastics      Pointer to the stochastics interface
  /// @param callbacks        Pointer to the call back interface
  FmuHandlerInterface(std::string componentName,
                      fmu_check_data_t& cdata,
                      AgentInterface* agent,
                      StochasticsInterface* stochastics,
                      const CallbackInterface* callbacks)
      : componentName(componentName), cdata(cdata), agent(agent), stochastics(stochastics), callbacks(callbacks)
  {
  }

  FmuHandlerInterface(const FmuHandlerInterface&) = delete;
  FmuHandlerInterface(FmuHandlerInterface&&) = delete;
  FmuHandlerInterface& operator=(const FmuHandlerInterface&) = delete;
  FmuHandlerInterface& operator=(FmuHandlerInterface&&) = delete;
  virtual ~FmuHandlerInterface() = default;

  /*!
   * \brief Has to be called from UpdateInput in AlgorithmFmuWrapper
   *
   * \param[in]   localLinkId     Local link id forwarded from FmuWrapper
   * \param[in]   data            Shared pointer of signal interface forwarded from FmuWrapper
   * \param[in]   time            Timestep forwarded from FmuWrapper
   */
  virtual void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const>& data, int time) = 0;

  /*!
   * \brief Has to be called from UpdateOutput in AlgorithmFmuWrapper
   *
   * \param[in]   localLinkId     Local link id forwarded from FmuWrapper
   * \param[in]   data            Shared pointer of signal interface forwarded from FmuWrapper
   * \param[in]   time            Timestep forwarded from FmuWrapper
   */
  virtual void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const>& data, int time) = 0;

  /*!
   * \brief This function is called before the initialization of the fmu
   */
  virtual void PrepareInit() = 0;

  /*!
   * \brief This function is called during the initialization if the FMU
   *  and allows to set values that are fixed after the initialization
   */
  virtual void Init() = 0;

  /*!
   * \brief Has to be called from Trigger in AlgorithmFmuWrapper
   *
   * \param[in]   time            Timestep forwarded from FmuWrapper
   */
  virtual void PreStep(int time) = 0;

  /*!
   * \brief Has to be called from Trigger in AlgorithmFmuWrapper
   *
   * \param[in]   time            Timestep forwarded from FmuWrapper
   */
  virtual void PostStep(int time) = 0;

  //-----------------------------------------------------------------------------
  //! Provides callback to LOG() macro.
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-----------------------------------------------------------------------------
  void Log(CbkLogLevel logLevel, const char* file, int line, const std::string& message)
  {
    if (callbacks)
    {
      callbacks->Log(logLevel, file, line, message);
    }
  }

  //! List of supported FMU types
  //! \note has to be instantiated in FmuWrapper implementation with all supported types
  enum class FmuType;

  //! Maps FMU type strings to enumeration values
  //! \note has to be instantiated in FmuWrapper implementation with all supported type strings
  static const std::unordered_map<std::string, FmuType> fmuTypeStrings;

  /// @brief Function to read FMU variable values
  virtual void ReadValues() = 0;

  /// @brief Function to write FMU variable values
  virtual void WriteValues() = 0;

  /// @brief Synchronize FMU variables and its paramters
  virtual void SyncFmuVariablesAndParameters() = 0;

  /// @return Function to get FMU TypeDefinitions
  virtual std::unordered_map<std::string, std::unordered_map<int, std::string>> GetFmuTypeDefinitions() = 0;

  /// @return Function to get FMU variables
  virtual FmuVariables GetFmuVariables() = 0;

  /// @brief Function to set FMU values
  /// @param valueReference   value of a reference
  /// @param fmuValue         Input FMU value
  /// @param dataType         Type of the variable
  virtual void SetFmuValue(int valueReference, FmuValue fmuValue, VariableType dataType) = 0;

  /// @brief Function to set list of FMU values
  /// @param valueReferences   list of values of a reference
  /// @param fmuValuesIn       list of Input FMU value
  /// @param dataType          Type of the variable
  virtual void SetFmuValues(std::vector<int> valueReferences, std::vector<FmuValue> fmuValuesIn, VariableType dataType)
      = 0;

  /// @brief Function to get FMU values
  /// @param valueReference   value of a reference
  /// @param fmuValueOut      Input FMU value
  /// @param dataType         Type of the variable
  virtual void GetFmuValue(int valueReference, FmuValue& fmuValueOut, VariableType dataType) = 0;

  /// @brief Function to get list of FMU values
  /// @param valueReferences   list of values of a reference
  /// @param fmuValuesOut      list of Input FMU value
  /// @param dataType          Type of the variable
  virtual void GetFmuValues(std::vector<int> valueReferences,
                            std::vector<FmuValue>& fmuValuesOut,
                            VariableType dataType)
      = 0;

  /// @brief Function to handle FMI status
  /// @param status       Status of
  /// @param logPrefix    Log prefix
  virtual void HandleFmiStatus(jm_status_enu_t status, std::string logPrefix) = 0;

  /// @brief Prepare FMU initialization
  /// @return status of FMI Library
  virtual jm_status_enu_t PrepareFmuInit() = 0;

  /// @brief End handling of FMI library
  /// @return status of FMI Library
  virtual jm_status_enu_t FmiEndHandling() = 0;

  /// @brief Simulate step of FMI library
  /// @param time Time of the simulation
  /// @return status of FMI Library
  virtual jm_status_enu_t FmiSimulateStep(double time) = 0;

  /// @brief Prepare simulation for FMI
  /// @return status of FMI Library
  virtual jm_status_enu_t FmiPrepSimulate() = 0;

protected:
  struct fmu_check_data_t& cdata;  //!< check data to be passed around between the FMIL functions

  int offsetTime = 0;    //!< after spawn  call of trigger is delayed by this value (ms)
  int responseTime = 0;  //!< call of UpdateOutput after responseTime elapsed (ms)
  int cycleTime = 100;   //!< Function Trigger of this model is called again after cycleTime  (ms)
  int startTime;         //!< Time of FMU start (simulation timebase)

  AgentInterface* agent;                    //!< References the agent interface
  const StochasticsInterface* stochastics;  //!< References the stochastics interface of the framework
  const CallbackInterface* callbacks;       //!< References the callback functions of the framework

  std::string componentName;  ///< Name of the Fmu handler interface component
};
