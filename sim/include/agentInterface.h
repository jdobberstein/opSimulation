/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2016-2021 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  agentInterface.h
//! @brief This file contains the interface for communicating between framework
//!        and world.

#pragma once

#include <MantleAPI/Traffic/entity_properties.h>
#include <map>
#include <vector>

#include "common/globalDefinitions.h"
#include "common/sensorDefinitions.h"
#include "common/worldDefinitions.h"
#include "include/profilesInterface.h"
#include "include/signalInterface.h"
#include "include/spawnItemParameterInterface.h"
#include "include/worldObjectInterface.h"

/// Using Lane types as a list of lane type
using LaneTypes = std::vector<LaneType>;
class EgoAgentInterface;

/**
 * @brief Agent Interface within the openPASS framework.
 * @details This interface provides access to agent parameters, properties, attributes and dynamic states.
 */
class AgentInterface : public virtual WorldObjectInterface
{
public:
  AgentInterface() = default;
  AgentInterface(const AgentInterface &) = delete;
  AgentInterface(AgentInterface &&) = delete;
  AgentInterface &operator=(const AgentInterface &) = delete;
  AgentInterface &operator=(AgentInterface &&) = delete;
  virtual ~AgentInterface() = default;

  /**
   * @brief Get the Ego Agent object
   *
   * @return Returns the EgoAgent corresponding to this agent
   */
  virtual EgoAgentInterface &GetEgoAgent() = 0;

  //! Retrieves the type key of an agent
  //!
  //! @return                string identifier of vehicle type
  virtual std::string GetVehicleModelType() const = 0;

  //! Retrieves all vehicle model parameters of agent
  //!
  //! @return               mantle_api::VehicleProperties of agent
  virtual std::shared_ptr<const mantle_api::EntityProperties> GetVehicleModelParameters() const = 0;

  //! Retrieves name of driver profile of agent
  //!
  //! @return               DriverProfile name of agent
  virtual std::string GetDriverProfileName() const = 0;

  //! Retrieves the name of agent specified in the scenario, xml file
  //!
  //! @return                AgentTypeName of agent
  virtual std::string GetScenarioName() const = 0;

  //! Retrieves agentTypeGroupName of agent
  //!
  //! @return                AgentTypeGroupName of agent
  virtual AgentCategory GetAgentCategory() const = 0;

  //! Retrieves agentTypeName of agent
  //!
  //! @return                AgentTypeName of agent
  virtual std::string GetAgentTypeName() const = 0;

  //! Returns true if Agent is marked as the ego agent.
  //!
  //! @return true if Agent is marked as the ego agent
  virtual bool IsEgoAgent() const = 0;

  //! Retrieves the current gear number.
  //!
  //! @return                Gear no.
  virtual int GetGear() const = 0;

  //! Retrieves list of collisions partners of agent.
  //!
  //! @return list of collisions partners of agent
  virtual std::vector<std::pair<ObjectTypeOSI, int>> GetCollisionPartners() const = 0;

  //! Retrieves velocity of agent after crash.
  //!
  //! @return                a value >0 if collided
  virtual PostCrashVelocity GetPostCrashVelocity() const = 0;

  //! Retrieves velocity of agent after crash.
  //!
  //! @param  postCrashVelocity  velocity of agent after crash
  virtual void SetPostCrashVelocity(PostCrashVelocity postCrashVelocity) = 0;

  //! Sets x-coordinate of agent
  //!
  //! @param[in]     positionX    X-coordinate
  virtual void SetPositionX(units::length::meter_t positionX) = 0;

  //! Sets y-coordinate of agent
  //!
  //! @param[in]     positionY    Y-coordinate
  virtual void SetPositionY(units::length::meter_t positionY) = 0;

  //! Sets the agents vehicle model parameter
  //!
  //! @param[in]     parameter    New vehicle model paramter
  virtual void SetVehicleModelParameter(const std::shared_ptr<const mantle_api::EntityProperties> parameter) = 0;

  //! Sets forward velocity of agent
  //!
  //! @param[in]     value velocity [Forward velocity]
  virtual void SetVelocity(units::velocity::meters_per_second_t value) = 0;

  //! Sets velocity of agent
  //!
  //! @param[in]     velocity velocity vector (Forward, Sideward, Upward)
  virtual void SetVelocityVector(const mantle_api::Vec3<units::velocity::meters_per_second_t> &velocity) = 0;

  //! Sets acceleration of agent
  //!
  //! @param[in]    acceleration acceleration vector (Forward, Sideward, Upward)
  virtual void SetAccelerationVector(
      const mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> &acceleration)
      = 0;

  //! Sets forward acceleration of agent
  //!
  //! @param[in]     value    forward acceleration
  virtual void SetAcceleration(units::acceleration::meters_per_second_squared_t value) = 0;

  //! Sets yaw angle of agent
  //!
  //! @param[in]     value yawAngle/agent orientation
  virtual void SetYaw(units::angle::radian_t value) = 0;

  //! Sets roll angle of agent
  //!
  //! @param[in]     value roll angle/agent orientation
  virtual void SetRoll(units::angle::radian_t value) = 0;

  //! Sets the total traveled distance of agent
  //!
  //! @param[in]     distanceTraveled    total traveled distance
  virtual void SetDistanceTraveled(units::length::meter_t distanceTraveled) = 0;

  //! Returns the total traveled distance of agent
  //!
  //! @return   total traveled distance
  virtual units::length::meter_t GetDistanceTraveled() const = 0;

  //! Sets gear of vehicle
  //!
  //! @param[in]     gear    current gear
  virtual void SetGear(int gear) = 0;

  //! Sets current engine speed (rpm)
  //!
  //! @param[in]     engineSpeed    current engineSpeed
  virtual void SetEngineSpeed(units::angular_velocity::revolutions_per_minute_t engineSpeed) = 0;

  //! Sets current position of acceleration pedal in percent
  //!
  //! @param[in]     percent    current percentage
  virtual void SetEffAccelPedal(double percent) = 0;

  //! Sets current position of brake pedal in percent
  //!
  //! @param[in]     percent    current percentage
  virtual void SetEffBrakePedal(double percent) = 0;

  //! Sets current angle of the steering wheel in radian
  //!
  //! @param[in]     steeringWheelAngle    current steering wheel angle
  virtual void SetSteeringWheelAngle(units::angle::radian_t steeringWheelAngle) = 0;

  //! Sets the rotation rate of selected wheel
  //!
  //! @param[in]     axleIndex	selected Axle (value 0 refers to the front axle; index increases towards the rear axle)
  //! @param[in]	   wheelIndex	selected Wheel (value 0 refers to a right wheel,index increases towards the left
  //! wheel)
  //! @param[in]	   rotationRate rotation rate of selected wheel [radians/s]
  virtual void SetWheelRotationRate(int axleIndex,
                                    int wheelIndex,
                                    units::angular_velocity::radians_per_second_t rotationRate)
      = 0;

  //! Set the orientation of selected wheel
  //!
  //! @param[in]     axleIndex	selected Axle (value 0 refers to the front axle; index increases towards the rear axle)
  //! @param[in]	 wheelIndex	selected Wheel (value 0 refers to a right wheel,index increases towards the left
  //! wheel)
  //! @param[in]	 wheelOrientation orientation of selected wheel
  virtual void SetWheelOrientation(int axleIndex,
                                   int wheelIndex,
                                   mantle_api::Vec3<units::angle::radian_t> wheelOrientation)
      = 0;

  //! Get the rotation rate of selected wheel
  //!
  //! @param[in]     axleIndex	selected Axle (value 0 refers to the front axle; index increases towards the rear axle)
  //! @param[in]	   wheelIndex	selected Wheel (value 0 refers to a right wheel,index increases towards the left
  //! wheel)
  //! @return	                    rotation rate of selected wheel [radians/s]
  virtual units::angular_velocity::radians_per_second_t GetWheelRotationRate(int axleIndex, int wheelIndex) = 0;

  //! Sets maximum acceleration of the vehicle
  //!
  //! @param[in]     maxAcceleration   maximum acceleration
  virtual void SetMaxAcceleration(units::acceleration::meters_per_second_squared_t maxAcceleration) = 0;

  //! Sets maximum deceleration of the vehicle
  //!
  //! @param[in]     maxDeceleration   maximum deceleration
  virtual void SetMaxDeceleration(units::acceleration::meters_per_second_squared_t maxDeceleration) = 0;

  //! update list with collision partners
  //!
  //! @param[in]  collisionPartner list of collision partners
  virtual void UpdateCollision(std::pair<ObjectTypeOSI, int> collisionPartner) = 0;

  //! Unlocate agent in world.
  //!
  // @return
  virtual void Unlocate() = 0;

  //! Locate agent in world.
  //!
  //! @return true, if agent could be localized
  virtual bool Locate() = 0;

  //! Update agent for in new timestep
  //!
  //! @return true, if agent could be localized
  virtual bool Update() = 0;

  //! Set the brake light on or off.
  //!
  //! @param[in]     brakeLightStatus    status of brake light
  virtual void SetBrakeLight(bool brakeLightStatus) = 0;

  /**
   * Returns the status of the brake light.
   *
   * @return true if brake light is on
   */
  virtual bool GetBrakeLight() const = 0;

  /**
   * @brief Set the Indicator State
   *
   * @param indicatorState Indicator in a specific state
   */
  virtual void SetIndicatorState(IndicatorState indicatorState) = 0;

  /**
   * @brief Get the Indicator State object
   *
   * @return Retrieve the state of the indicator
   */
  virtual IndicatorState GetIndicatorState() const = 0;

  //! Set the Horn on or off.
  //!
  //! @param[in]     hornSwitch    status of horn
  virtual void SetHorn(bool hornSwitch) = 0;

  //! Returns the status of the HornSwitch.
  //!
  //! @return        true if HornSwtich is pressed
  virtual bool GetHorn() const = 0;

  //! Set the Headlight on or off.
  //!
  //! @param[in]     headLightSwitch    status of headlight
  virtual void SetHeadLight(bool headLightSwitch) = 0;

  //! Returns the status of the Headlightswitch.
  //!
  //! @return        true if Headlightswitch is on
  virtual bool GetHeadLight() const = 0;

  //! Set the Highbeamlight on or off.
  //!
  //! @param[in]     headLightSwitch    status of headLightSwitch
  virtual void SetHighBeamLight(bool headLightSwitch) = 0;

  //! Returns the status of the Highbeamlight.
  //!
  //! @return        true if Highbeamlightswitch is on
  virtual bool GetHighBeamLight() const = 0;

  //! Returns the status of lights
  //!
  //! @return        priorized light-state of agent (lowest = off , highest=flasher)
  virtual LightState GetLightState() const = 0;

  //! Set the Flasher on or off.
  //!
  //! @param[in]     flasherSwitch    status of flasher
  virtual void SetFlasher(bool flasherSwitch) = 0;

  //! Returns the status of the Flasher.
  //!
  //! @return        true if Flasherswitch is on
  virtual bool GetFlasher() const = 0;

  //! Initializes the parameters of an agent
  //!
  //! @param[in] agentBuildInstructions    Build instructions holding parameters such as dimensions
  //!                                      of a vehicle, or its initial spawning velocity
  virtual void InitParameter(const AgentBuildInstructions &agentBuildInstructions) = 0;

  //! Returns true if agent is still in World located.
  //!
  //! @return true if agent is in world
  virtual bool IsAgentInWorld() const = 0;

  //! Set the position of an agent.
  //!
  //! @param pos position of an agent
  virtual void SetPosition(Position pos) = 0;

  /**
   * @brief If agent is partially not on the road
   *
   * @return true if agent is partially not on the road
   */
  virtual bool IsLeavingWorld() const = 0;

  //! Returns the s coordinate distance from the front of the agent to the first point where his lane intersects
  //! another. As the agent may not yet be on the junction, it has to be specified which connecting road he will take in
  //! the junction
  //!
  //! \param intersectingConnectorId  OpenDRIVE id of the connecting road that intersects with the agent
  //! \param intersectingLaneId       OpenDRIVE id of the lane on the intersecting connecting road
  //! \param ownConnectorId           OpenDRIVE id of the connecting road that this agent is assumed to drive on
  //!
  //! \return distance of front of agent to the intersecting lane
  virtual units::length::meter_t GetDistanceToConnectorEntrance(std::string intersectingConnectorId,
                                                                int intersectingLaneId,
                                                                std::string ownConnectorId) const
      = 0;

  //! Returns the s coordinate distance from the rear of the agent to the furthest point where his lane intersects
  //! another. As the agent may not yet be on the junction, it has to be specified which connecting road he will take in
  //! the junction
  //!
  //! \param intersectingConnectorId  OpenDRIVE id of the connecting road that intersects with the agent
  //! \param intersectingLaneId       OpenDRIVE id of the lane on the intersecting connecting road
  //! \param ownConnectorId           OpenDRIVE id of the connecting road that this agent is assumed to drive on
  //!
  //! \return distance of rear of agent to the farther side of the intersecting lane
  virtual units::length::meter_t GetDistanceToConnectorDeparture(std::string intersectingConnectorId,
                                                                 int intersectingLaneId,
                                                                 std::string ownConnectorId) const
      = 0;

  //! Retrieve the yaw rate of the agent.
  //!
  //! @return yaw rate
  virtual units::angular_velocity::radians_per_second_t GetYawRate() const = 0;

  //! Set the yaw rate of the agent.
  //!
  //! @param yawRate yaw rate of the agent
  virtual void SetYawRate(units::angular_velocity::radians_per_second_t yawRate) = 0;

  //! Retrieve the yaw acceleration of the agent.
  //!
  //! @return yaw acceleration
  virtual units::angular_acceleration::radians_per_second_squared_t GetYawAcceleration() const = 0;

  //! Set the yaw acceleration of the agent.
  //!
  //! @param yawAcceleration yaw acceleration of the agent
  virtual void SetYawAcceleration(units::angular_acceleration::radians_per_second_squared_t yawAcceleration) = 0;

  //! Retrieve the centripetal acceleration of the agent.
  //!
  //! @return   Centripetal acceleration [m/s^2]
  virtual units::acceleration::meters_per_second_squared_t GetCentripetalAcceleration() const = 0;

  //! Retrieve the tangential acceleration of the agent.
  //!
  //! @return   Tangential acceleration [m/s^2]
  virtual units::acceleration::meters_per_second_squared_t GetTangentialAcceleration() const = 0;

  //! Set the centripetal acceleration of the agent.
  //!
  //! @param[in]   centripetalAcceleration   The acceleration to set [m/s^2]
  virtual void SetCentripetalAcceleration(units::acceleration::meters_per_second_squared_t centripetalAcceleration) = 0;

  //! Set the tangential acceleration of the agent.
  //!
  //! @param[in]   tangentialAcceleration   The acceleration to set [m/s^2]
  virtual void SetTangentialAcceleration(units::acceleration::meters_per_second_squared_t tangentialAcceleration) = 0;

  //! Retrieve the ids of the roads the agent is on
  //!
  //! @param[in]   point   point of agent to localize
  //! @return   roads
  virtual std::vector<std::string> GetRoads(ObjectPoint point) const = 0;

  /**
   * @brief Get the Distance Reference Point To Leading Edge object
   *
   * @return the Distance Reference Point To Leading Edge object
   */
  virtual units::length::meter_t GetDistanceReferencePointToLeadingEdge() const = 0;

  /**
   * @brief Get the Engine Speed object
   *
   * @return returns engine speed
   */
  virtual units::angular_velocity::revolutions_per_minute_t GetEngineSpeed() const = 0;

  //! Gets current position of acceleration pedal in percent
  //!
  //! @return     accelPedal    current percentage
  virtual double GetEffAccelPedal() const = 0;

  //! Gets current position of brake pedal in percent
  //!
  //! @return     brakePedal    current percentage
  virtual double GetEffBrakePedal() const = 0;

  //! Gets current angle of the steering wheel in radian
  //!
  //! @return     current steering wheel angle
  virtual units::angle::radian_t GetSteeringWheelAngle() const = 0;

  /**
   * @brief Get the Max Acceleration object
   *
   * @return Returns maximum acceleration
   */
  virtual units::acceleration::meters_per_second_squared_t GetMaxAcceleration() const = 0;

  /**
   * @brief Get the Max Deceleration object
   *
   * @return Returns maximum deceleration
   */
  virtual units::acceleration::meters_per_second_squared_t GetMaxDeceleration() const = 0;

  //! Retrieves the minimum speed goal of agent
  //!
  //! @return               Speed Goal Min
  virtual units::velocity::meters_per_second_t GetSpeedGoalMin() const = 0;

  /**
   * @brief Get the Sensor Parameters object
   *
   * @return returns openpass sensors parameters
   */
  virtual const openpass::sensors::Parameters &GetSensorParameters() const = 0;

  /**
   * @brief Set the Sensor Parameters object
   *
   * @param sensorParameters Openpass sensors parameters
   */
  virtual void SetSensorParameters(openpass::sensors::Parameters sensorParameters) = 0;

  /**
   * @brief Get the wheelbase
   *
   * @return Distance between the wheels (center to center)
   */
  virtual units::length::meter_t GetWheelbase() const = 0;
};
