/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/i_identifiable.h>

/**
 * @brief Supported entity types
 *
 * Moving objects are considered to be available only for one run
 * Stationary objects and others will marked as persistent
 */
enum class EntityType
{
  kVehicle,
  kObject,
  kController,
  kOther
};

/// @brief Implementing classes of this interface shall handle unique IDs for various types of entities
class IdManagerInterface
{
public:
  virtual ~IdManagerInterface() = default;

  /// @brief Reset internally used indices for specified EntityType
  /// @param entityType   EntityType to reset
  virtual void Reset(EntityType entityType) = 0;

  /// @brief Registers a new entity without entity type (assigned to "others")
  /// @returns unique id
  [[nodiscard]] virtual mantle_api::UniqueId Generate() = 0;

  /// @brief Registers a new entity for given entity type
  /// @note  Throws if entity type is unknown
  ///
  /// @param entityType   @see EntityType
  /// @return unique id
  [[nodiscard]] virtual mantle_api::UniqueId Generate(EntityType entityType) = 0;
};