/********************************************************************************
 * Copyright (c) 2018 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OWL/DataTypes.h"

namespace OWL
{
namespace Testing
{

struct LaneGeometryElementGenerator
{
  static Primitive::LaneGeometryElement RectangularLaneGeometryElement(Common::Vector2d<units::length::meter_t> origin,
                                                                       units::length::meter_t width,
                                                                       units::length::meter_t length,
                                                                       units::angle::radian_t hdg = 0.0_rad,
                                                                       OWL::Interfaces::Lane *lane = nullptr)
  {
    Common::Vector2d<units::length::meter_t> current_left{origin.x - width / 2 * units::math::sin(hdg),
                                                          origin.y + width / 2 * units::math::cos(hdg)};

    Common::Vector2d<units::length::meter_t> current_reference = origin;

    Common::Vector2d<units::length::meter_t> current_right{origin.x + width / 2 * units::math::sin(hdg),
                                                           origin.y - width / 2 * units::math::cos(hdg)};

    Common::Vector2d<units::length::meter_t> next_left{
        origin.x + length * units::math::cos(hdg) - width / 2 * units::math::sin(hdg),
        origin.y + length * units::math::sin(hdg) + width / 2 * units::math::cos(hdg)};

    Common::Vector2d<units::length::meter_t> next_reference{origin.x + length * units::math::cos(hdg),
                                                            origin.y + length * units::math::sin(hdg)};

    Common::Vector2d<units::length::meter_t> next_right{
        origin.x + length * units::math::cos(hdg) + width / 2 * units::math::sin(hdg),
        origin.y + length * units::math::sin(hdg) - width / 2 * units::math::cos(hdg)};

    OWL::Primitive::LaneGeometryJoint current{{current_left, current_reference, current_right}, 0.0_i_m, 0.0_m, hdg};

    OWL::Primitive::LaneGeometryJoint next{{next_left, next_reference, next_right}, 0.0_i_m, length, hdg};

    return OWL::Primitive::LaneGeometryElement(current, next, lane);
  }

  static Primitive::LaneGeometryElement RectangularLaneGeometryElementWithCurvature(
      Common::Vector2d<units::length::meter_t> origin,
      units::length::meter_t width,
      units::length::meter_t length,
      units::curvature::inverse_meter_t curvatureStart,
      units::curvature::inverse_meter_t curvatureEnd,
      units::angle::radian_t hdg = 0.0_rad)
  {
    Common::Vector2d<units::length::meter_t> current_left{origin.x - width / 2 * units::math::sin(hdg),
                                                          origin.y + width / 2 * units::math::cos(hdg)};

    Common::Vector2d<units::length::meter_t> current_reference = origin;

    Common::Vector2d<units::length::meter_t> current_right{origin.x + width / 2 * units::math::sin(hdg),
                                                           origin.y - width / 2 * units::math::cos(hdg)};

    Common::Vector2d<units::length::meter_t> next_left{
        origin.x + length * units::math::cos(hdg) - width / 2 * units::math::sin(hdg),
        origin.y + length * units::math::sin(hdg) + width / 2 * units::math::cos(hdg)};

    Common::Vector2d<units::length::meter_t> next_reference{origin.x + length * units::math::cos(hdg),
                                                            origin.y + length * units::math::sin(hdg)};

    Common::Vector2d<units::length::meter_t> next_right{
        origin.x + length * units::math::cos(hdg) + width / 2 * units::math::sin(hdg),
        origin.y + length * units::math::sin(hdg) - width / 2 * units::math::cos(hdg)};

    OWL::Primitive::LaneGeometryJoint current{
        {current_left, current_reference, current_right}, curvatureStart, 0.0_m, hdg};

    OWL::Primitive::LaneGeometryJoint next{{next_left, next_reference, next_right}, curvatureEnd, length, hdg};

    return OWL::Primitive::LaneGeometryElement(current, next, nullptr);
  }

  static Primitive::LaneGeometryElement TriangularLaneGeometryElement(Common::Vector2d<units::length::meter_t> origin,
                                                                      units::length::meter_t width,
                                                                      units::length::meter_t length,
                                                                      units::angle::radian_t hdg = 0.0_rad)
  {
    Common::Vector2d<units::length::meter_t> current_left = origin;
    Common::Vector2d<units::length::meter_t> current_reference = origin;
    Common::Vector2d<units::length::meter_t> current_right = origin;

    Common::Vector2d<units::length::meter_t> next_left{
        origin.x + length * units::math::cos(hdg) - width / 2 * units::math::sin(hdg),
        origin.y + length * units::math::sin(hdg) + width / 2 * units::math::cos(hdg)};

    Common::Vector2d<units::length::meter_t> next_reference{origin.x + length * units::math::cos(hdg),
                                                            origin.y + length * units::math::sin(hdg)};

    Common::Vector2d<units::length::meter_t> next_right{
        origin.x + length * units::math::cos(hdg) + width / 2 * units::math::sin(hdg),
        origin.y + length * units::math::sin(hdg) - width / 2 * units::math::cos(hdg)};

    OWL::Primitive::LaneGeometryJoint current{{current_left, current_reference, current_right}, 0.0_i_m, 0.0_m, hdg};

    OWL::Primitive::LaneGeometryJoint next{{next_left, next_reference, next_right}, 0.0_i_m, length, hdg};

    return OWL::Primitive::LaneGeometryElement(current, next, nullptr);
  }

  static Primitive::LaneGeometryElement CurvedLaneGeometryElement(Common::Vector2d<units::length::meter_t> origin,
                                                                  units::length::meter_t width,
                                                                  units::length::meter_t length,
                                                                  units::length::meter_t sDistance,
                                                                  units::length::meter_t radius)
  {
    double openingAngle = length / radius;

    Common::Vector2d<units::length::meter_t> current_left{origin.x, origin.y + width / 2};

    Common::Vector2d<units::length::meter_t> current_reference = origin;

    Common::Vector2d<units::length::meter_t> current_right{origin.x, origin.y - width / 2};

    Common::Vector2d<units::length::meter_t> next_left{
        origin.x + radius * std::sin(openingAngle) - width / 2 * std::sin(openingAngle),
        origin.y + radius * (1 - std::cos(openingAngle)) + width / 2 * std::cos(openingAngle)};

    Common::Vector2d<units::length::meter_t> next_reference{origin.x + radius * std::sin(openingAngle),
                                                            origin.y + radius * (1 - std::cos(openingAngle))};

    Common::Vector2d<units::length::meter_t> next_right{
        origin.x + radius * std::sin(openingAngle) + width / 2 * std::sin(openingAngle),
        origin.y + radius * (1 - std::cos(openingAngle)) - width / 2 * std::cos(openingAngle)};

    OWL::Primitive::LaneGeometryJoint current{
        {current_left, current_reference, current_right}, 1.0 / radius, 0.0_m, 0.0_rad};

    OWL::Primitive::LaneGeometryJoint next{{next_left, next_reference, next_right}, 1.0 / radius, sDistance, 0.0_rad};

    return OWL::Primitive::LaneGeometryElement(current, next, nullptr);
  }
};

}  // namespace Testing
}  //namespace OWL
