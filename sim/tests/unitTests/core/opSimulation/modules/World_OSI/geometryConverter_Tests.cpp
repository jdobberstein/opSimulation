/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <algorithm>
#include <array>
#include <cstddef>
#include <map>
#include <memory>
#include <ostream>
#include <units.h>
#include <utility>
#include <vector>

#include "GeometryConverter.h"
#include "JointsBuilder.h"
#include "RamerDouglasPeucker.h"
#include "boostGeometryCommon.h"
#include "common/globalDefinitions.h"
#include "common/vector2d.h"
#include "fakeOdRoad.h"
#include "fakeRoadGeometry.h"
#include "fakeRoadLane.h"
#include "fakeRoadLaneSection.h"
#include "include/roadInterface/roadLaneInterface.h"
#include "include/roadInterface/roadLaneWidth.h"

class RoadLaneOffset;

using namespace testing;
using ::testing::_;
using ::testing::Eq;
using ::testing::Le;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::SizeIs;

TEST(GeometryConverter_UnitTests, CalculateJointOnlyRightLanes)
{
  constexpr units::length::meter_t geometryOffset{123.4};
  constexpr units::length::meter_t roadOffset{12.3};

  FakeRoadLaneSection section;
  FakeOdRoad road;
  std::vector<std::unique_ptr<RoadLaneOffset>> laneOffsets;
  ON_CALL(road, GetLaneOffsets()).WillByDefault(ReturnRef(laneOffsets));

  std::unique_ptr<FakeRoadLane> lane0 = std::make_unique<FakeRoadLane>();
  std::unique_ptr<FakeRoadLane> laneMinus1 = std::make_unique<FakeRoadLane>();
  std::unique_ptr<FakeRoadLane> laneMinus2 = std::make_unique<FakeRoadLane>();
  RoadLaneWidths widthsMinus1{};
  widthsMinus1.emplace_back(
      std::make_unique<RoadLaneWidth>(0_m,
                                      3.0_m,
                                      0.0,
                                      units::unit_t<units::inverse<units::length::meter>>{0.0},
                                      units::unit_t<units::inverse<units::squared<units::length::meter>>>{0.0}));
  ON_CALL(*laneMinus1, GetWidths()).WillByDefault(ReturnRef(widthsMinus1));
  RoadLaneWidths widthsMinus2{};
  widthsMinus2.emplace_back(
      std::make_unique<RoadLaneWidth>(0_m,
                                      4.0_m,
                                      0.0,
                                      units::unit_t<units::inverse<units::length::meter>>{0.0},
                                      units::unit_t<units::inverse<units::squared<units::length::meter>>>{0.0}));
  ON_CALL(*laneMinus2, GetWidths()).WillByDefault(ReturnRef(widthsMinus2));

  std::map<int, std::unique_ptr<RoadLaneInterface>> lanes;
  lanes.emplace(0, std::move(lane0));
  lanes.emplace(-1, std::move(laneMinus1));
  lanes.emplace(-2, std::move(laneMinus2));
  ON_CALL(section, GetLanes()).WillByDefault(ReturnRef(lanes));

  FakeRoadGeometry geometry;
  ON_CALL(geometry, GetCoord(geometryOffset, _))
      .WillByDefault(Return(Common::Vector2d<units::length::meter_t>{101.0_m, 102.0_m}));

  auto result = GeometryConverter::CalculateBorderPoints(&section, &road, &geometry, geometryOffset, roadOffset);

  ASSERT_THAT(result.s, Eq(roadOffset));
  ASSERT_THAT(result.points, SizeIs(3));
  ASSERT_THAT(result.points.at(0).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 102.0_m}));
  ASSERT_THAT(result.points.at(0).lane, Eq(lanes[0].get()));
  ASSERT_THAT(result.points.at(1).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 99.0_m}));
  ASSERT_THAT(result.points.at(1).lane, Eq(lanes[-1].get()));
  ASSERT_THAT(result.points.at(2).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 95.0_m}));
  ASSERT_THAT(result.points.at(2).lane, Eq(lanes[-2].get()));
}

TEST(GeometryConverter_UnitTests, CalculateJointOnlyRightLanesWithBorders)
{
  constexpr units::length::meter_t geometryOffset{123.4};
  constexpr units::length::meter_t roadOffset{12.3};

  FakeRoadLaneSection section;
  FakeOdRoad road;
  std::vector<std::unique_ptr<RoadLaneOffset>> laneOffsets;
  ON_CALL(road, GetLaneOffsets()).WillByDefault(ReturnRef(laneOffsets));

  std::unique_ptr<FakeRoadLane> lane0 = std::make_unique<FakeRoadLane>();
  std::unique_ptr<FakeRoadLane> laneMinus1 = std::make_unique<FakeRoadLane>();
  std::unique_ptr<FakeRoadLane> laneMinus2 = std::make_unique<FakeRoadLane>();

  RoadLaneWidths emptyWidths{};
  RoadLaneWidths widthsMinus1{};
  widthsMinus1.emplace_back(
      std::make_unique<RoadLaneWidth>(0_m,
                                      3.0_m,
                                      0.0,
                                      units::unit_t<units::inverse<units::length::meter>>{0.0},
                                      units::unit_t<units::inverse<units::squared<units::length::meter>>>{0.0}));
  ON_CALL(*laneMinus1, GetWidths()).WillByDefault(ReturnRef(emptyWidths));
  ON_CALL(*laneMinus1, GetBorders()).WillByDefault(ReturnRef(widthsMinus1));
  RoadLaneWidths widthsMinus2{};
  widthsMinus2.emplace_back(
      std::make_unique<RoadLaneWidth>(0_m,
                                      7.0_m,
                                      0.0,
                                      units::unit_t<units::inverse<units::length::meter>>{0.0},
                                      units::unit_t<units::inverse<units::squared<units::length::meter>>>{0.0}));
  ON_CALL(*laneMinus2, GetWidths()).WillByDefault(ReturnRef(emptyWidths));
  ON_CALL(*laneMinus2, GetBorders()).WillByDefault(ReturnRef(widthsMinus2));
  std::map<int, std::unique_ptr<RoadLaneInterface>> lanes;
  lanes.emplace(0, std::move(lane0));
  lanes.emplace(-1, std::move(laneMinus1));
  lanes.emplace(-2, std::move(laneMinus2));
  ON_CALL(section, GetLanes()).WillByDefault(ReturnRef(lanes));

  FakeRoadGeometry geometry;
  ON_CALL(geometry, GetCoord(geometryOffset, _))
      .WillByDefault(Return(Common::Vector2d<units::length::meter_t>{101.0_m, 102.0_m}));

  auto result = GeometryConverter::CalculateBorderPoints(&section, &road, &geometry, geometryOffset, roadOffset);

  ASSERT_THAT(result.s, Eq(roadOffset));
  ASSERT_THAT(result.points, SizeIs(3));
  ASSERT_THAT(result.points.at(0).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 102.0_m}));
  ASSERT_THAT(result.points.at(0).lane, Eq(lanes[0].get()));
  ASSERT_THAT(result.points.at(1).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 99.0_m}));
  ASSERT_THAT(result.points.at(1).lane, Eq(lanes[-1].get()));
  ASSERT_THAT(result.points.at(2).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 95.0_m}));
  ASSERT_THAT(result.points.at(2).lane, Eq(lanes[-2].get()));
}

TEST(GeometryConverter_UnitTests, CalculateJointOnlyLeftLanes)
{
  constexpr units::length::meter_t geometryOffset{123.4};
  constexpr units::length::meter_t roadOffset{12.3};

  FakeRoadLaneSection section;
  FakeOdRoad road;
  std::vector<std::unique_ptr<RoadLaneOffset>> laneOffsets;
  ON_CALL(road, GetLaneOffsets()).WillByDefault(ReturnRef(laneOffsets));

  std::unique_ptr<FakeRoadLane> lane0 = std::make_unique<FakeRoadLane>();
  std::unique_ptr<FakeRoadLane> lanePlus1 = std::make_unique<FakeRoadLane>();
  std::unique_ptr<FakeRoadLane> lanePlus2 = std::make_unique<FakeRoadLane>();
  RoadLaneWidths widthsPlus1{};
  widthsPlus1.emplace_back(
      std::make_unique<RoadLaneWidth>(0_m,
                                      3.0_m,
                                      0.0,
                                      units::unit_t<units::inverse<units::length::meter>>{0.0},
                                      units::unit_t<units::inverse<units::squared<units::length::meter>>>{0.0}));
  ON_CALL(*lanePlus1, GetWidths()).WillByDefault(ReturnRef(widthsPlus1));
  RoadLaneWidths widthsPlus2{};
  widthsPlus2.emplace_back(
      std::make_unique<RoadLaneWidth>(0_m,
                                      4.0_m,
                                      0.0,
                                      units::unit_t<units::inverse<units::length::meter>>{0.0},
                                      units::unit_t<units::inverse<units::squared<units::length::meter>>>{0.0}));
  ON_CALL(*lanePlus2, GetWidths()).WillByDefault(ReturnRef(widthsPlus2));
  std::map<int, std::unique_ptr<RoadLaneInterface>> lanes;
  lanes.emplace(0, std::move(lane0));
  lanes.emplace(1, std::move(lanePlus1));
  lanes.emplace(2, std::move(lanePlus2));
  ON_CALL(section, GetLanes()).WillByDefault(ReturnRef(lanes));

  FakeRoadGeometry geometry;
  ON_CALL(geometry, GetCoord(geometryOffset, _))
      .WillByDefault(Return(Common::Vector2d<units::length::meter_t>{101.0_m, 102.0_m}));

  auto result = GeometryConverter::CalculateBorderPoints(&section, &road, &geometry, geometryOffset, roadOffset);

  ASSERT_THAT(result.s, Eq(roadOffset));
  ASSERT_THAT(result.points, SizeIs(3));
  ASSERT_THAT(result.points.at(0).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 109.0_m}));
  ASSERT_THAT(result.points.at(0).lane, Eq(lanes[2].get()));
  ASSERT_THAT(result.points.at(1).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 105.0_m}));
  ASSERT_THAT(result.points.at(1).lane, Eq(lanes[1].get()));
  ASSERT_THAT(result.points.at(2).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 102.0_m}));
  ASSERT_THAT(result.points.at(2).lane, Eq(lanes[0].get()));
}

TEST(GeometryConverter_UnitTests, CalculateJointLeftAndRightLanes)
{
  constexpr units::length::meter_t geometryOffset{123.4};
  constexpr units::length::meter_t roadOffset{12.3};

  FakeRoadLaneSection section;
  FakeOdRoad road;
  std::vector<std::unique_ptr<RoadLaneOffset>> laneOffsets;
  ON_CALL(road, GetLaneOffsets()).WillByDefault(ReturnRef(laneOffsets));

  auto lane0 = std::make_unique<FakeRoadLane>();
  auto lanePlus1 = std::make_unique<FakeRoadLane>();
  auto laneMinus1 = std::make_unique<FakeRoadLane>();
  RoadLaneWidths widthsMinus1{};
  widthsMinus1.emplace_back(
      std::make_unique<RoadLaneWidth>(0_m,
                                      3.0_m,
                                      0.0,
                                      units::unit_t<units::inverse<units::length::meter>>{0.0},
                                      units::unit_t<units::inverse<units::squared<units::length::meter>>>{0.0}));
  ON_CALL(*laneMinus1, GetWidths()).WillByDefault(ReturnRef(widthsMinus1));
  RoadLaneWidths widthsPlus1{};
  widthsPlus1.emplace_back(
      std::make_unique<RoadLaneWidth>(0_m,
                                      4.0_m,
                                      0.0,
                                      units::unit_t<units::inverse<units::length::meter>>{0.0},
                                      units::unit_t<units::inverse<units::squared<units::length::meter>>>{0.0}));
  ON_CALL(*lanePlus1, GetWidths()).WillByDefault(ReturnRef(widthsPlus1));
  std::map<int, std::unique_ptr<RoadLaneInterface>> lanes;
  lanes.emplace(0, std::move(lane0));
  lanes.emplace(-1, std::move(laneMinus1));
  lanes.emplace(1, std::move(lanePlus1));
  ON_CALL(section, GetLanes()).WillByDefault(ReturnRef(lanes));

  FakeRoadGeometry geometry;
  ON_CALL(geometry, GetCoord(geometryOffset, _))
      .WillByDefault(Return(Common::Vector2d<units::length::meter_t>{101.0_m, 102.0_m}));

  auto result = GeometryConverter::CalculateBorderPoints(&section, &road, &geometry, geometryOffset, roadOffset);

  ASSERT_THAT(result.s, Eq(roadOffset));
  ASSERT_THAT(result.points, SizeIs(3));
  ASSERT_THAT(result.points.at(0).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 106.0_m}));
  ASSERT_THAT(result.points.at(0).lane, Eq(lanes[1].get()));
  ASSERT_THAT(result.points.at(1).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 102.0_m}));
  ASSERT_THAT(result.points.at(1).lane, Eq(lanes[0].get()));
  ASSERT_THAT(result.points.at(2).point, Eq(Common::Vector2d<units::length::meter_t>{101.0_m, 99.0_m}));
  ASSERT_THAT(result.points.at(2).lane, Eq(lanes[-1].get()));
}

// Verify that lanes to the left of the center line have positive t coordinates
// and lanes to the right have negative t coordinates
TEST(GeometryConverter_UnitTests, BorderPoint_HasCorrectTCoordinate)
{
  constexpr auto geometryOffset = 123.4_m;
  constexpr auto roadOffset = 12.3_m;

  FakeRoadLaneSection section;
  FakeOdRoad road;
  std::vector<std::unique_ptr<RoadLaneOffset>> laneOffsets;
  ON_CALL(road, GetLaneOffsets()).WillByDefault(ReturnRef(laneOffsets));

  std::array<RoadLaneWidth, 5> widths{
      RoadLaneWidth(0_m, 1.0_m, {}, {}, {}),
      RoadLaneWidth(0_m, 2.0_m, {}, {}, {}),
      RoadLaneWidth(0_m, 3.0_m, {}, {}, {}),  // this "lane" will have id 0 and thus always have width 0.
      RoadLaneWidth(0_m, 4.0_m, {}, {}, {}),
      RoadLaneWidth(0_m, 5.0_m, {}, {}, {})  //
  };

  std::array<std::vector<std::unique_ptr<RoadLaneWidth>>, 5> widthContainers;
  std::transform(widths.begin(),
                 widths.end(),
                 widthContainers.begin(),
                 [](RoadLaneWidth& width)
                 {
                   std::vector<std::unique_ptr<RoadLaneWidth>> result;
                   result.emplace_back(std::make_unique<RoadLaneWidth>(
                       width.GetSOffset(), width.GetA(), width.GetB(), width.GetC(), width.GetD()));
                   return result;
                 });

  std::array<std::unique_ptr<FakeRoadLane>, 5> lanes;
  std::map<int, std::unique_ptr<RoadLaneInterface>> laneMap;
  const int offset{lanes.size() >> 1};
  for (size_t i{0u}; i < lanes.size(); ++i)
  {
    lanes.at(i) = std::make_unique<FakeRoadLane>();
    ON_CALL(*lanes.at(i), GetWidths()).WillByDefault(ReturnRef(widthContainers.at(i)));
    laneMap.emplace_hint(laneMap.end(), i - offset, std::move(lanes.at(i)));
  }
  ON_CALL(section, GetLanes()).WillByDefault(ReturnRef(laneMap));

  FakeRoadGeometry geometry;
  ON_CALL(geometry, GetCoord(geometryOffset, _)).WillByDefault(Return(Common::Vector2d{100.0_m, 50.0_m}));
  ON_CALL(geometry, GetDir(geometryOffset)).WillByDefault(Return(0.0_rad));

  auto result = GeometryConverter::CalculateBorderPoints(&section, &road, &geometry, geometryOffset, roadOffset);

  EXPECT_THAT(result.s, Eq(roadOffset));
  ASSERT_THAT(result.points, SizeIs(5));
  // Default direction is right, thus points below (100, 50) should have negative t-coordinates:
  EXPECT_THAT(result.points.at(0).point, Eq(Common::Vector2d{100.0_m, 59.0_m}));
  EXPECT_THAT(result.points.at(0).lane, Eq(laneMap[2].get()));
  EXPECT_THAT(result.points.at(0).t, Eq(9.0_m));

  EXPECT_THAT(result.points.at(1).point, Eq(Common::Vector2d{100.0_m, 54.0_m}));
  EXPECT_THAT(result.points.at(1).lane, Eq(laneMap[1].get()));
  EXPECT_THAT(result.points.at(1).t, Eq(4.0_m));

  EXPECT_THAT(result.points.at(2).point, Eq(Common::Vector2d{100.0_m, 50.0_m}));
  EXPECT_THAT(result.points.at(2).lane, Eq(laneMap[0].get()));
  EXPECT_THAT(result.points.at(2).t, Eq(0.0_m));

  EXPECT_THAT(result.points.at(3).point, Eq(Common::Vector2d{100.0_m, 48.0_m}));
  EXPECT_THAT(result.points.at(3).lane, Eq(laneMap[-1].get()));
  EXPECT_THAT(result.points.at(3).t, Eq(-2.0_m));

  EXPECT_THAT(result.points.at(4).point, Eq(Common::Vector2d{100.0_m, 47.0_m}));
  EXPECT_THAT(result.points.at(4).lane, Eq(laneMap[-2].get()));
  EXPECT_THAT(result.points.at(4).t, Eq(-3.0_m));
}

struct RamerDouglasPeucker_UnitTests_Data
{
  std::vector<BorderPoints> input;
};

class RamerDouglasPeucker_UnitTests : public ::TestWithParam<RamerDouglasPeucker_UnitTests_Data>
{
};

double GetErrorBetweenOriginalAndSimplified(const std::vector<BorderPoints>& original,
                                            const std::vector<BorderPoints>& simplified)
{
  double maxError{0.0};
  for (auto joint = simplified.begin(); joint < simplified.end() - 1; joint++)
  {
    for (auto originalJoint : original)
    {
      if (originalJoint.s >= joint->s && originalJoint.s <= (joint + 1)->s)
      {
        for (size_t i = 0; i < originalJoint.points.size(); i++)
        //for (const auto currentPoint : originalJoint.points)
        {
          point_t currentPointBoost{originalJoint.points[i].point.x.value(), originalJoint.points[i].point.y.value()};
          bg::model::linestring<point_t> line;
          bg::append(line, point_t{joint->points[i].point.x.value(), joint->points[i].point.y.value()});
          bg::append(line, point_t{(joint + 1)->points[i].point.x.value(), (joint + 1)->points[i].point.y.value()});
          auto error = bg::distance(currentPointBoost, line);
          maxError = std::max(maxError, error);
        }
      }
    }
  }
  return maxError;
}

bool operator==(const LaneJoint& first, const LaneJoint& second)
{
  return first.lane == second.lane &&                                     //
         first.left == second.left &&                                     //
         first.center == second.center &&                                 //
         first.right == second.right &&                                   //
         units::math::abs(first.heading - second.heading) < 0.001_rad &&  //
         units::math::abs(first.rawHeading - second.rawHeading) < 0.001_rad
      && units::math::abs(first.curvature - second.curvature) < 0.0001_i_m &&  //
         units::math::abs(first.t_left - second.t_left) < 0.001_m &&           //
         units::math::abs(first.t_right - second.t_right) < 0.001_m;
}

std::ostream& operator<<(std::ostream& os, const LaneJoint& point)
{
  os << "left: " << point.left << " center: " << point.center << " right: " << point.right
     << " heading: " << point.heading << " curvature: " << point.curvature;

  return os;
}

TEST(JointsBuilder_UnitTests, CalculatePoints_CalculatesCorrectCenterAndOuterPoints)
{
  FakeRoadLane lane1;
  ON_CALL(lane1, GetId()).WillByDefault(Return(1));

  FakeRoadLane lane0;
  ON_CALL(lane0, GetId()).WillByDefault(Return(0));

  FakeRoadLane laneMinus1;
  ON_CALL(laneMinus1, GetId()).WillByDefault(Return(-1));

  FakeRoadLane laneMinus2;
  ON_CALL(laneMinus2, GetId()).WillByDefault(Return(-2));

  BorderPoints firstJoint{0.0_m,
                          0.0_rad,
                          {BorderPoint{{100_m, 100_m}, 3_m, &lane1},
                           BorderPoint{{120_m, 90_m}, 0_m, &lane0},
                           BorderPoint{{130_m, 80_m}, -3_m, &laneMinus1},
                           BorderPoint{{160_m, 60_m}, -6_m, &laneMinus2}}};
  BorderPoints secondJoint{1000.0_m,
                           0.0_rad,
                           {BorderPoint{{1100_m, 100_m}, 3_m, &lane1},
                            BorderPoint{{1120_m, 90_m}, 0_m, &lane0},
                            BorderPoint{{1130_m, 80_m}, -3_m, &laneMinus1},
                            BorderPoint{{1160_m, 60_m}, -6_m, &laneMinus2}}};
  JointsBuilder jointsBuilder({std::vector<BorderPoints>{{firstJoint, secondJoint}}, 0_rad, 0_rad});

  jointsBuilder.CalculatePoints();

  auto result = jointsBuilder.GetJoints();

  ASSERT_THAT(result, SizeIs(2));
  auto& firstResult = result.at(0);
  ASSERT_THAT(firstResult.s.value(), Eq(0.0));
  ASSERT_THAT(
      firstResult.laneJoints,
      ElementsAre(
          std::make_pair(
              -2,
              LaneJoint{
                  &laneMinus2, {130_m, 80_m}, {145_m, 70_m}, {160_m, 60_m}, 0.0_rad, 0.0_rad, 0.0_i_m, -3_m, -6_m}),
          std::make_pair(
              -1,
              LaneJoint{
                  &laneMinus1, {120_m, 90_m}, {125_m, 85_m}, {130_m, 80_m}, 0.0_rad, 0.0_rad, 0.0_i_m, 0_m, -3_m}),
          std::make_pair(
              0, LaneJoint{&lane0, {120_m, 90_m}, {120_m, 90_m}, {120_m, 90_m}, 0.0_rad, 0.0_rad, 0.0_i_m, 0_m, 0_m}),
          std::make_pair(
              1,
              LaneJoint{&lane1, {100_m, 100_m}, {110_m, 95_m}, {120_m, 90_m}, 0.0_rad, 0.0_rad, 0.0_i_m, 3_m, 0_m})));
  auto& secondResult = result.at(1);
  ASSERT_THAT(secondResult.s.value(), Eq(1000.0));
  ASSERT_THAT(
      secondResult.laneJoints,
      ElementsAre(
          std::make_pair(
              -2,
              LaneJoint{
                  &laneMinus2, {1130_m, 80_m}, {1145_m, 70_m}, {1160_m, 60_m}, 0.0_rad, 0.0_rad, 0.0_i_m, -3_m, -6_m}),
          std::make_pair(
              -1,
              LaneJoint{
                  &laneMinus1, {1120_m, 90_m}, {1125_m, 85_m}, {1130_m, 80_m}, 0.0_rad, 0.0_rad, 0.0_i_m, 0_m, -3_m}),
          std::make_pair(
              0,
              LaneJoint{&lane0, {1120_m, 90_m}, {1120_m, 90_m}, {1120_m, 90_m}, 0.0_rad, 0.0_rad, 0.0_i_m, 0_m, 0_m}),
          std::make_pair(
              1,
              LaneJoint{
                  &lane1, {1100_m, 100_m}, {1110_m, 95_m}, {1120_m, 90_m}, 0.0_rad, 0.0_rad, 0.0_i_m, 3_m, 0_m})));
}

TEST(JointsBuilder_UnitTests, CalculateHeadings)
{
  Joint firstJoint{0.0_m,
                   {{-1, LaneJoint{nullptr, {}, {0_m, 10_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}},
                    {0, LaneJoint{nullptr, {}, {0_m, 0_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}}}};
  Joint secondJoint{10.0_m,
                    {{-1, LaneJoint{nullptr, {}, {10_m, 10_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}},
                     {0, LaneJoint{nullptr, {}, {10_m, 0_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}}}};
  Joint thirdJoint{20.0_m,
                   {{-1, LaneJoint{nullptr, {}, {20_m, 0_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}},
                    {0, LaneJoint{nullptr, {}, {20_m, 10_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}}}};
  Joint forthJoint{30.0_m,
                   {{-1, LaneJoint{nullptr, {}, {20_m, -10_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}},
                    {0, LaneJoint{nullptr, {}, {10_m, 20_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}}}};

  JointsBuilder jointsBuilder({}, Joints{firstJoint, secondJoint, thirdJoint, forthJoint});

  jointsBuilder.CalculateHeadings();

  auto result = jointsBuilder.GetJoints();

  ASSERT_THAT(result, SizeIs(4));
  auto& firstResult = result.at(0);
  ASSERT_THAT(firstResult.s.value(), Eq(0.0));
  ASSERT_THAT(firstResult.laneJoints,
              ElementsAre(std::make_pair(-1, LaneJoint{nullptr, {}, {0_m, 10_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}),
                          std::make_pair(0, LaneJoint{nullptr, {}, {0_m, 0_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m})));
  auto& secondResult = result.at(1);
  ASSERT_THAT(secondResult.s.value(), Eq(10.0));
  ASSERT_THAT(secondResult.laneJoints,
              ElementsAre(std::make_pair(-1, LaneJoint{nullptr, {}, {10_m, 10_m}, {}, -45_deg, 0.0_rad, 0.0_i_m}),
                          std::make_pair(0, LaneJoint{nullptr, {}, {10_m, 0_m}, {}, 45_deg, 0.0_rad, 0.0_i_m})));
  auto& thirdResult = result.at(2);
  ASSERT_THAT(thirdResult.s.value(), Eq(20.0));
  ASSERT_THAT(thirdResult.laneJoints,
              ElementsAre(std::make_pair(-1, LaneJoint{nullptr, {}, {20_m, 0_m}, {}, -90_deg, 0.0_rad, 0.0_i_m}),
                          std::make_pair(0, LaneJoint{nullptr, {}, {20_m, 10_m}, {}, 135_deg, 0.0_rad, 0.0_i_m})));
}

TEST(JointsBuilder_UnitTests, CalculateCurvatures)
{
  Joint firstJoint{0.0_m,
                   {{-1, LaneJoint{nullptr, {}, {0_m, 0_m}, {}, 0.5_rad, 0.0_rad, 0.0_i_m}},
                    {0, LaneJoint{nullptr, {}, {0_m, 10_m}, {}, 1.0_rad, 0.0_rad, 0.0_i_m}}}};
  Joint secondJoint{10.0_m,
                    {{-1, LaneJoint{nullptr, {}, {10_m, 0_m}, {}, 1.0_rad, 0.0_rad, 0.0_i_m}},
                     {0, LaneJoint{nullptr, {}, {10_m, 10_m}, {}, 2.0_rad, 0.0_rad, 0.0_i_m}}}};
  Joint thirdJoint{20.0_m,
                   {{-1, LaneJoint{nullptr, {}, {40_m, 0_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}},
                    {0, LaneJoint{nullptr, {}, {40_m, 10_m}, {}, 1.0_rad, 0.0_rad, 0.0_i_m}}}};
  Joint forthJoint{30.0_m,
                   {{-1, LaneJoint{nullptr, {}, {50_m, 0_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}},
                    {0, LaneJoint{nullptr, {}, {50_m, 10_m}, {}, 0.0_rad, 0.0_rad, 0.0_i_m}}}};

  JointsBuilder jointsBuilder({{}, 0.5_rad, -0.5_rad}, Joints{firstJoint, secondJoint, thirdJoint, forthJoint});

  jointsBuilder.CalculateCurvatures();

  auto result = jointsBuilder.GetJoints();

  ASSERT_THAT(result, SizeIs(4));
  auto& firstResult = result.at(0);
  ASSERT_THAT(firstResult.s.value(), Eq(0.0));
  ASSERT_THAT(firstResult.laneJoints,
              ElementsAre(std::make_pair(-1, LaneJoint{nullptr, {}, {0_m, 0_m}, {}, 0.5_rad, 0.0_rad, 0.0_i_m}),
                          std::make_pair(0, LaneJoint{nullptr, {}, {0_m, 10_m}, {}, 1.0_rad, 0.0_rad, 0.1_i_m})));
  auto& secondResult = result.at(1);
  ASSERT_THAT(secondResult.s.value(), Eq(10.0));
  ASSERT_THAT(secondResult.laneJoints,
              ElementsAre(std::make_pair(-1, LaneJoint{nullptr, {}, {10_m, 0_m}, {}, 1.0_rad, 0.0_rad, 0.025_i_m}),
                          std::make_pair(0, LaneJoint{nullptr, {}, {10_m, 10_m}, {}, 2.0_rad, 0.0_rad, 0.05_i_m})));
  auto& thirdResult = result.at(2);
  ASSERT_THAT(thirdResult.s.value(), Eq(20.0));
  ASSERT_THAT(thirdResult.laneJoints,
              ElementsAre(std::make_pair(-1, LaneJoint{nullptr, {}, {40_m, 0_m}, {}, 0.0_rad, 0.0_rad, -0.05_i_m}),
                          std::make_pair(0, LaneJoint{nullptr, {}, {40_m, 10_m}, {}, 1.0_rad, 0.0_rad, -0.05_i_m})));
  auto& forthResult = result.at(3);
  ASSERT_THAT(forthResult.s.value(), Eq(30.0));
  ASSERT_THAT(forthResult.laneJoints,
              ElementsAre(std::make_pair(-1, LaneJoint{nullptr, {}, {50_m, 0_m}, {}, 0.0_rad, 0.0_rad, -0.1_i_m}),
                          std::make_pair(0, LaneJoint{nullptr, {}, {50_m, 10_m}, {}, 0.0_rad, 0.0_rad, -0.3_i_m})));
}

TEST_P(RamerDouglasPeucker_UnitTests, Simplify)
{
  auto& input = GetParam().input;
  auto result = RamerDouglasPeucker::Simplify<BorderPoints>(input);

  ASSERT_THAT(GetErrorBetweenOriginalAndSimplified(input, result), Le(RamerDouglasPeucker::ERROR_THRESHOLD.value()));
}

const BorderPoints joint0{0.0_m,
                          0.0_rad,
                          {BorderPoint{{0_m, 0_m}, 0_m, nullptr},
                           BorderPoint{{0_m, 1_m}, 0_m, nullptr},
                           BorderPoint{{0_m, 3_m}, 0_m, nullptr}}};
const BorderPoints joint1000{1000.0_m,
                             0.0_rad,
                             {BorderPoint{{1000_m, 0_m}, 0_m, nullptr},
                              BorderPoint{{1000_m, 1_m}, 0_m, nullptr},
                              BorderPoint{{1000_m, 3_m}, 0_m, nullptr}}};
const BorderPoints joint500a{500.0_m,
                             0.0_rad,
                             {BorderPoint{{500_m, 0_m}, 0_m, nullptr},
                              BorderPoint{{500_m, 1_m}, 0_m, nullptr},
                              BorderPoint{{500_m, 3_m}, 0_m, nullptr}}};
const BorderPoints joint500b{500.0_m,
                             0.0_rad,
                             {BorderPoint{{500_m, 1_m}, 0_m, nullptr},
                              BorderPoint{{500_m, 2_m}, 0_m, nullptr},
                              BorderPoint{{500_m, 4_m}, 0_m, nullptr}}};
const BorderPoints joint600b{600.0_m,
                             0.0_rad,
                             {BorderPoint{{600_m, 1_m}, 0_m, nullptr},
                              BorderPoint{{600_m, 1.99_m}, 0_m, nullptr},
                              BorderPoint{{600_m, 4_m}, 0_m, nullptr}}};
const BorderPoints joint600c{600.0_m,
                             0.0_rad,
                             {BorderPoint{{600_m, 1_m}, 0_m, nullptr},
                              BorderPoint{{600_m, 2.5_m}, 0_m, nullptr},
                              BorderPoint{{600_m, 4_m}, 0_m, nullptr}}};
const BorderPoints joint700b{700.0_m,
                             0.0_rad,
                             {BorderPoint{{700_m, 1_m}, 0_m, nullptr},
                              BorderPoint{{700_m, 2_m}, 0_m, nullptr},
                              BorderPoint{{700_m, 4_m}, 0_m, nullptr}}};

INSTANTIATE_TEST_CASE_P(RamerDouglasPeucker_UnitTests,
                        RamerDouglasPeucker_UnitTests,
                        Values(RamerDouglasPeucker_UnitTests_Data{{joint0, joint1000}},
                               RamerDouglasPeucker_UnitTests_Data{{joint0, joint500a, joint1000}},
                               RamerDouglasPeucker_UnitTests_Data{{joint0, joint500b, joint1000}},
                               RamerDouglasPeucker_UnitTests_Data{{joint0, joint500b, joint600b, joint700b, joint1000}},
                               RamerDouglasPeucker_UnitTests_Data{
                                   {joint0, joint500b, joint600c, joint700b, joint1000}}));
