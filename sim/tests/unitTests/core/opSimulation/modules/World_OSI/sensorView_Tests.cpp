/********************************************************************************
 * Copyright (c) 2018-2021 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/vector.h>
#include <algorithm>
#include <osi3/osi_common.pb.h>
#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_hostvehicledata.pb.h>
#include <osi3/osi_lane.pb.h>
#include <osi3/osi_object.pb.h>
#include <osi3/osi_sensorview.pb.h>
#include <ostream>
#include <string>
#include <units.h>
#include <vector>

#include "DataTypes.h"
#include "Primitives.h"
#include "WorldData.h"
#include "fakeLane.h"
#include "fakeMovingObject.h"

using namespace OWL;

using ::testing::_;
using ::testing::Eq;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::SizeIs;

struct SensorViewTest_Data
{
  struct Sensor
  {
    Sensor(units::length::meter_t x,
           units::length::meter_t y,
           units::angle::degree_t angle_left_abs_degree,
           units::angle::degree_t angle_right_abs_degree,
           units::length::meter_t radius)
        : x{x},
          y{y},
          angle_left_abs_rad{angle_left_abs_degree},
          angle_right_abs_rad{angle_right_abs_degree},
          radius{radius}
    {
    }

    units::length::meter_t x;
    units::length::meter_t y;
    units::angle::radian_t angle_left_abs_rad;
    units::angle::radian_t angle_right_abs_rad;
    units::length::meter_t radius;
    friend std::ostream& operator<<(std::ostream& os, const Sensor& obj)
    {
      return os << "Position: " << obj.x << ";" << obj.y << " | Opening Angles (rad): " << obj.angle_left_abs_rad << ";"
                << obj.angle_right_abs_rad << " | Radius: " << obj.radius;
    }
  };

  struct Object
  {
    units::length::meter_t x;
    units::length::meter_t y;
    units::length::meter_t length;
    units::length::meter_t width;
    units::angle::radian_t rotation;
    friend std::ostream& operator<<(std::ostream& os, const Object& obj)
    {
      return os << "Position: " << obj.x << ";" << obj.y << " | Dimension: " << obj.length << ";" << obj.width
                << " | Rotation: " << obj.rotation;
    }
  };

  std::string testcase;
  Sensor sensor;
  Object object;
  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const SensorViewTest_Data& obj)
  {
    return os << "\n[ TESTCASE ] " << obj.testcase << "\n[  SENSOR  ] " << obj.sensor << "\n[  OBJECT  ] "
              << obj.object;
  }
};

class SensorViewTestObjectDetection : public ::testing::TestWithParam<SensorViewTest_Data>
{
protected:
  OWL::WorldData worldData{nullptr};
};

TEST_P(SensorViewTestObjectDetection, TestGenerator)
{
  auto data = GetParam();

  mantle_api::Vec3<units::length::meter_t> sensorPosition = {data.sensor.x, data.sensor.y, 0_m};

  OWL::Fakes::MovingObject object;
  mantle_api::Vec3<units::length::meter_t> objectPosition = {data.object.x, data.object.y, 0_m};
  ON_CALL(object, GetReferencePointPosition()).WillByDefault(Return(objectPosition));
  mantle_api::Dimension3 objectDimension = {data.object.length, data.object.width, 0_m};
  ON_CALL(object, GetDimension()).WillByDefault(Return(objectDimension));
  std::vector<OWL::Interfaces::MovingObject*> fakeMovingObjects{&object};

  auto filteredMoving = worldData.ApplySectorFilter(fakeMovingObjects,
                                                    sensorPosition,
                                                    data.sensor.radius,
                                                    data.sensor.angle_left_abs_rad,
                                                    data.sensor.angle_right_abs_rad);
  ASSERT_THAT(filteredMoving, SizeIs(1));
}

INSTANTIATE_TEST_SUITE_P(
    ObjectsTouchingSensorView,
    SensorViewTestObjectDetection,
    ::testing::Values(SensorViewTest_Data{"Large object in backshadow or regular sensor",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, 45.0_deg, -45.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{-40.0_m, -0.50_m, 82.0_m, 2.0_m, 3.0_deg}},
                      SensorViewTest_Data{"Small object in gap of pacman-style sensor",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, 170.0_deg, -170.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{-6.0_m, 0.50_m, 4.0_m, 0.50_m, -175.0_deg}},
                      SensorViewTest_Data{"Medium object in 'negative' sensorconfig",
                                          SensorViewTest_Data::Sensor{-5.0_m, 3.0_m, -45.0_deg, 20.0_deg, 20.0_m},
                                          SensorViewTest_Data::Object{25.0_m, -8.0_m, 50.0_m, 5.0_m, -30.0_deg}}));

INSTANTIATE_TEST_SUITE_P(
    SimpleCasesWithSensor60Degree,
    SensorViewTestObjectDetection,
    ::testing::Values(SensorViewTest_Data{"Small Object touches sensor looking north",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, 120.0_deg, 60.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{-2.0_m, 10.0_m, 2.0_m, 1.0_m, 90.0_deg}},
                      SensorViewTest_Data{"Object on backshadow of sensor looking north",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, 120.0_deg, 60.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{2.0_m, 8.0_m, 2.0_m, 1.0_m, 175.0_deg}},
                      SensorViewTest_Data{"Object within cone of sensor looking north",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, 120.0_deg, 60.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{-3.50_m, 4.0_m, 2.0_m, 1.0_m, -175.0_deg}},
                      SensorViewTest_Data{"Object touches sensor looking south",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, -60.0_deg, -120.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{2.0_m, -10.0_m, 2.0_m, 1.0_m, -90.0_deg}},
                      SensorViewTest_Data{"Object on backshadow of sensor looking south",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, -60.0_deg, -120.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{-2.0_m, -8.0_m, 2.0_m, 1.0_m, -175.0_deg}},
                      SensorViewTest_Data{"Object within cone of sensor looking south",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, -60.0_deg, -120.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{3.50_m, -4.0_m, 2.0_m, 1.0_m, 175.0_deg}},
                      SensorViewTest_Data{"Object touches sensor looking west",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, -150.0_deg, 150.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{-10.0_m, 2.0_m, 2.0_m, 1.0_m, 0.0_deg}},
                      SensorViewTest_Data{"Object on backshadow of sensor looking west",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, -150.0_deg, 150.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{-8.0_m, -2.0_m, 2.0_m, 1.0_m, 85.0_deg}},
                      SensorViewTest_Data{"Object within cone of sensor looking west",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, -150.0_deg, 150.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{-4.0_m, 3.50_m, 2.0_m, 1.0_m, -265.0_deg}},
                      SensorViewTest_Data{"Object touches sensor looking east",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, 30.0_deg, -30.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{10.0_m, -2.0_m, 2.0_m, 1.0_m, 180.0_deg}},
                      SensorViewTest_Data{"Object on backshadow of sensor looking east",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, 30.0_deg, -30.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{8.0_m, 2.0_m, 2.0_m, 1.0_m, 95.0_deg}},
                      SensorViewTest_Data{"Object within cone of sensor looking east",
                                          SensorViewTest_Data::Sensor{0.0_m, 0.0_m, 30.0_deg, -30.0_deg, 10.0_m},
                                          SensorViewTest_Data::Object{4.0_m, -3.50_m, 2.0_m, 1.0_m, 445.0_deg}}));

class TestWorldData : public OWL::WorldData
{
public:
  TestWorldData() : OWL::WorldData{nullptr} {}

  MOCK_CONST_METHOD1(GetMovingObject, const Interfaces::MovingObject&(Id id));
};

TEST(SensorViewTests, AddHostVehicleToSensorView_SetsHostVehicleAndLaneAssignments)
{
  TestWorldData worldData;
  osi3::SensorView sensorView;

  OWL::Id idLane1 = 101, idLane2 = 102, idLane3 = 103;
  sensorView.mutable_global_ground_truth()->add_lane()->mutable_id()->set_value(idLane1);
  sensorView.mutable_global_ground_truth()->add_lane()->mutable_id()->set_value(idLane2);
  sensorView.mutable_global_ground_truth()->add_lane()->mutable_id()->set_value(idLane3);

  OWL::Fakes::MovingObject hostVehicle;

  ON_CALL(hostVehicle, CopyToGroundTruth(_))
      .WillByDefault(
          [](osi3::GroundTruth& groundTruth)
          {
            auto* baseMoving = groundTruth.add_moving_object()->mutable_base();
            baseMoving->mutable_position()->set_x(2.0);
            baseMoving->mutable_position()->set_y(3.0);
          });

  OWL::Fakes::Lane lane1;
  ON_CALL(lane1, GetId()).WillByDefault(Return(idLane1));
  OWL::Fakes::Lane lane3;
  ON_CALL(lane3, GetId()).WillByDefault(Return(idLane3));
  OWL::Interfaces::Lanes laneAssignments{{&lane1, &lane3}};
  ON_CALL(hostVehicle, GetLaneAssignments()).WillByDefault(ReturnRef(laneAssignments));

  const OWL::Id host_id = 11;
  ON_CALL(worldData, GetMovingObject(host_id)).WillByDefault(ReturnRef(hostVehicle));
  worldData.AddHostVehicleToSensorView(host_id, sensorView);

  EXPECT_THAT(sensorView.host_vehicle_id().value(), Eq(host_id));
  EXPECT_THAT(sensorView.global_ground_truth().host_vehicle_id().value(), Eq(host_id));
  EXPECT_THAT(sensorView.host_vehicle_data().vehicle_localization().position().x(), Eq(2.0));
  EXPECT_THAT(sensorView.host_vehicle_data().vehicle_localization().position().y(), Eq(3.0));
  for (const auto& lane : sensorView.global_ground_truth().lane())
  {
    if (lane.id().value() == idLane1 || lane.id().value() == idLane3)
    {
      EXPECT_THAT(lane.classification().is_host_vehicle_lane(), Eq(true));
    }
    else
    {
      EXPECT_THAT(lane.classification().is_host_vehicle_lane(), Eq(false));
    }
  }
}
