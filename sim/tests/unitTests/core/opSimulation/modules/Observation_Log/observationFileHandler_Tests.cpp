/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ostream>
#include <string>
#include <vector>

#include "common/openPassTypes.h"
#include "common/openPassUtils.h"
#include "fakeDataBuffer.h"
#include "include/dataBufferInterface.h"
#include "observationCyclics.h"
#include "observationFileHandler.h"

using ::testing::Eq;
using ::testing::NiceMock;
using ::testing::ReturnPointee;
using ::testing::UnorderedElementsAreArray;

struct ActiveComponentCyclicsData
{
  int time;
  std::string key;
  std::string value;
};

struct ActiveComponentEventData
{
  int time;
  int entity;
  std::string lateralController;
  std::string longitudinalController;
};

struct ActiveComponentTestData
{
  std::vector<ActiveComponentCyclicsData> cyclics;
  std::vector<ActiveComponentEventData> events;
};

class ActiveComponentObservation : public ObservationFileHandler,
                                   public ::testing::TestWithParam<ActiveComponentTestData>
{
public:
  ActiveComponentObservation() : ObservationFileHandler(buffer) {}

  // builds ObservationCyclics and expected Events from ActiveComponentTestData parametrization
  void SetUp() override
  {
    std::set<Key> uniqueAgentIds;
    const auto& testData = GetParam();

    fakeAgentIds.clear();
    expectedEvents.clear();
    cyclics.Clear();

    for (const auto& cyclic : testData.cyclics)
    {
      cyclics.Insert(cyclic.time, cyclic.key, cyclic.value);

      // get agent id from cyclic key prefix (up to ':' character), remove leading zeroes to match DataBuffer format
      auto prefix = cyclic.key.substr(0, cyclic.key.find(':'));
      prefix.erase(0, prefix.find_first_not_of('0'));

      if (prefix.empty())
      {
        // special case for prefix "00:", where all zeroes got removed in the previous step
        prefix = "0";
      }

      uniqueAgentIds.insert(prefix);
    }

    // move set of agent ids to vector to match mocked interface
    std::move(uniqueAgentIds.cbegin(), uniqueAgentIds.cend(), std::back_inserter(fakeAgentIds));

    // expected Events from test data
    for (const auto& event : testData.events)
    {
      expectedEvents.emplace_back(event.time,
                                  AcyclicRow{0,
                                             "OpenPASS",
                                             Acyclic{"ActiveComponentChange",
                                                     {},
                                                     {{event.entity}},
                                                     {{"LateralController", event.lateralController},
                                                      {"LongitudinalController", event.longitudinalController}}}});
    }
    EXPECT_CALL(buffer, GetKeys("Statics/Agents")).WillRepeatedly(ReturnPointee(&fakeAgentIds));
  }

  void ParseActiveComponentChanges() { actualEvents = ObservationFileHandler::ParseActiveComponentChanges(cyclics); }

  NiceMock<FakeDataBuffer> buffer;
  ObservationCyclics cyclics;
  Keys fakeAgentIds;
  Events actualEvents;
  Events expectedEvents;
};

TEST_P(ActiveComponentObservation, DeterminesExpectedEvents)
{
  ASSERT_NO_THROW(ParseActiveComponentChanges());

  EXPECT_THAT(actualEvents.size(), Eq(expectedEvents.size()));
  EXPECT_THAT(actualEvents, UnorderedElementsAreArray(expectedEvents));
}

// clang-format off
INSTANTIATE_TEST_SUITE_P(SingleAgentIdRanges,                  //  timestamp    cyclic header      cyclic content
                         ActiveComponentObservation,           //    |           |                  |
                         ::testing::Values(ActiveComponentTestData{{{0, "00:LateralController", "LatCtrl1"},
                                                                    {0, "00:LongitudinalController", "LongCtrl1"}},
                                         // expected values: timestamp  agentId   Event:LateralController
                                         //                          |  |         |        Event:LongitudinalController
                                                                   {{0, 0, "LatCtrl1", "LongCtrl1"}}},
                                           ActiveComponentTestData{{{0, "10:LateralController", "LatCtrl2"},
                                                                    {0, "10:LongitudinalController", "LongCtrl2"}},
                                                                   {{0, 10, "LatCtrl2", "LongCtrl2"}}},
                                           ActiveComponentTestData{{{0, "123:LateralController", "LatCtrl3"},
                                                                    {0, "123:LongitudinalController", "LongCtrl3"}},
                                                                   {{0, 123, "LatCtrl3", "LongCtrl3"}}}));

INSTANTIATE_TEST_SUITE_P(ShiftetLatLong,
                         ActiveComponentObservation,
                         ::testing::Values(ActiveComponentTestData{
                             {{0, "00:LateralController", "LatCtrl1"},
                              {0, "00:LongitudinalController", ""},
                              {1, "00:LateralController", "LatCtrl1"},
                              {1, "00:LongitudinalController", "LongCtrl1"}},
                             {{0, 0, "LatCtrl1", ""}, {1, 0, "LatCtrl1", "LongCtrl1"}}}));

INSTANTIATE_TEST_SUITE_P(MultipleAgents,
                         ActiveComponentObservation,
                         ::testing::Values(ActiveComponentTestData{{{0, "00:LateralController", "LatCtrl1"},
                                                                    {0, "00:LongitudinalController", "LongCtrl1"},
                                                                    {12, "01:LateralController", "LatCtrl2"},
                                                                    {12, "01:LongitudinalController", "LongCtrl2"},
                                                                    {122, "11:LateralController", "LatCtrl3"},
                                                                    {122, "11:LongitudinalController", "LongCtrl3"},
                                                                    {125, "222:LateralController", "LatCtrl4"},
                                                                    {125, "222:LongitudinalController", "LongCtrl4"}},
                                                                   {{0, 0, "LatCtrl1", "LongCtrl1"},
                                                                    {12, 0, "", ""},
                                                                    {12, 1, "LatCtrl2", "LongCtrl2"},
                                                                    {122, 1, "", ""},
                                                                    {122, 11, "LatCtrl3", "LongCtrl3"},
                                                                    {125, 11, "", ""},
                                                                    {125, 222, "LatCtrl4", "LongCtrl4"}}}));
// clang-format on

// code below for pretty printing Event data in case of assertion failure

std::ostream& operator<<(std::ostream& os, const openpass::databuffer::Parameter& parameter)
{
  for (const auto& entry : parameter)
  {
    std::visit(
        openpass::utils::FlatParameter::to_string([&os, &key = entry.first](const std::string& value)
                                                  { os << "      key: " << key << ", value: " << value << "\n"; }),
        entry.second);
  }

  return os;
}

std::ostream& operator<<(std::ostream& os, const Acyclic& acyclic)
{
  const auto entity
      = acyclic.affectedEntities.entities.empty() ? "-" : std::to_string(acyclic.affectedEntities.entities.front());

  return os << "    name: " << acyclic.name << "\n    1st affected entity: " << entity << "\n    parameter:\n"
            << acyclic.parameter << "(skipping triggeringEntities)";
}

std::ostream& operator<<(std::ostream& os, const AcyclicRow& row)
{
  return os << "  key: " << row.key << "\n  acyclicData:\n" << row.data;
}

std::ostream& operator<<(std::ostream& os, const Event& event)
{
  return os << "\ntime: " << event.time << "\ndataRow:\n" << event.dataRow;
}
