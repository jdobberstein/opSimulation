/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "fakeDataBuffer.h"
#include "framework/idManager.h"

TEST(IdManager, RegisterFirstMovingObject_Returns0)
{
  core::IdManager idManager;
  auto entityId = idManager.Generate(EntityType::kVehicle);
  ASSERT_THAT(entityId, 0);
}

TEST(IdManager, RegisterNextMovingObject_Returns1)
{
  core::IdManager idManager;
  auto entityId = idManager.Generate(EntityType::kVehicle);
  auto nextEntityId = idManager.Generate(EntityType::kVehicle);
  ASSERT_THAT(nextEntityId, 1);
}

TEST(IdManager, RegisterFirstStationaryObject_Returns1000000)
{
  core::IdManager idManager;
  auto entityId = idManager.Generate(EntityType::kObject);
  ASSERT_THAT(entityId, 2000000);
}

TEST(IdManager, RegisterNextStationaryObject_Returns1000001)
{
  core::IdManager idManager;
  auto entityId = idManager.Generate(EntityType::kObject);
  auto nextEntityId = idManager.Generate(EntityType::kObject);
  ASSERT_THAT(nextEntityId, 2000001);
}

TEST(IdManager, RegisterAnyObject_Returns2000000)
{
  core::IdManager idManager;
  auto entityId = idManager.Generate();
  ASSERT_THAT(entityId, 3000000);
}

TEST(IdManager, RegisteredObject_IsAddedToDatabuffer)
{
  core::IdManager idManager;
  auto entityId = idManager.Generate();
}
