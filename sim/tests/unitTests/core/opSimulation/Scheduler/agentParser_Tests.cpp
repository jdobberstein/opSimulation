/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <map>
#include <memory>
#include <string>
#include <units.h>
#include <vector>

#include "agent.h"
#include "agentParser.h"
#include "channel.h"
#include "common/vector2d.h"
#include "component.h"
#include "fakeAgent.h"
#include "fakeComponent.h"
#include "fakeWorld.h"
#include "include/agentBlueprintInterface.h"
#include "tasks.h"

using ::testing::_;
using ::testing::Contains;
using ::testing::Eq;
using ::testing::Field;
using ::testing::IsEmpty;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::SizeIs;

using namespace core;
using namespace core::scheduling;

class AgentParserTest : public ::testing::Test
{
protected:
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  std::vector<std::unique_ptr<NiceMock<FakeComponent>>> fakeComponents;
  std::unique_ptr<Agent> testAgent;
  std::unique_ptr<Component> testTargetComponent;
  AgentParser agentParser;
  Channel testChannel;
  std::map<int, Channel*> testChannels;
  AgentBuildInstructions testAgentBuildInstructions;

  AgentParserTest() : agentParser{0}, testChannel{1}, testChannels{{0, &testChannel}}, testAgentBuildInstructions{} {}

  void SetUp() override
  {
    EXPECT_CALL(fakeWorld, CreateAgentAdapter(_, _)).WillOnce(ReturnRef(fakeAgent));
    testAgent = std::make_unique<Agent>(0, &fakeWorld, testAgentBuildInstructions);
    testTargetComponent = std::make_unique<Component>("", testAgent.get());
    testChannel.AddTarget(testTargetComponent.get(), 0);
  }

  void ADD_FAKE_COMPONENT(const std::string& componentName, int cycleTime = 100, bool init = false)
  {
    fakeComponents.emplace_back(std::make_unique<NiceMock<FakeComponent>>());
    auto* fakeComponent = fakeComponents.back().get();
    // custom deleter does not delete fakeComponent, so it's okay to take the address and wrap it
    testAgent->AddComponent(componentName, std::unique_ptr<NiceMock<FakeComponent>, ComponentDeleter>(fakeComponent));

    ON_CALL(*fakeComponent, GetPriority()).WillByDefault(Return(0));
    ON_CALL(*fakeComponent, GetOffsetTime()).WillByDefault(Return(0));
    ON_CALL(*fakeComponent, GetResponseTime()).WillByDefault(Return(0));
    ON_CALL(*fakeComponent, GetOutputLinks()).WillByDefault(ReturnRef(testChannels));
    ON_CALL(*fakeComponent, ReleaseFromLibrary()).WillByDefault(Return(true));

    ON_CALL(*fakeComponent, GetCycleTime()).WillByDefault(Return(cycleTime));
    ON_CALL(*fakeComponent, GetInit()).WillByDefault(Return(init));
  }

  void INIT_AGENT_PARSER()
  {
    assert(!fakeComponents.empty());
    agentParser.Parse(*testAgent);
  }

  void TearDown() override {}
};

TEST_F(AgentParserTest, RecurringComponent_IsParsed)
{
  ADD_FAKE_COMPONENT("Component1", 100);
  INIT_AGENT_PARSER();

  auto nonRecurringTasks = agentParser.GetNonRecurringTasks();
  EXPECT_THAT(nonRecurringTasks, IsEmpty());

  std::vector<TaskItem> recurringTasks = agentParser.GetRecurringTasks();
  EXPECT_THAT(recurringTasks, Contains(Field(&TaskItem::cycletime, Eq(100)))) << "cycletime";
  EXPECT_THAT(recurringTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::Trigger)))) << "taskType Trigger";
  EXPECT_THAT(recurringTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::Update)))) << "taskType Update";

  EXPECT_EQ(recurringTasks.size(), 3);
  EXPECT_EQ(recurringTasks.front().taskType, TaskType::Trigger);
}

TEST_F(AgentParserTest, ThreeRecurringComponents_AreParsed)
{
  ADD_FAKE_COMPONENT("Compontent1", 100);
  ADD_FAKE_COMPONENT("Compontent2", 50);
  ADD_FAKE_COMPONENT("Compontent3", 250);
  INIT_AGENT_PARSER();

  auto nonRecurringTasks = agentParser.GetNonRecurringTasks();
  EXPECT_THAT(nonRecurringTasks, IsEmpty());

  auto recurringTasks = agentParser.GetRecurringTasks();

  EXPECT_THAT(recurringTasks, Contains(Field(&TaskItem::cycletime, Eq(50)))) << "cycletime";
  EXPECT_THAT(recurringTasks, Contains(Field(&TaskItem::cycletime, Eq(100)))) << "cycletime";
  EXPECT_THAT(recurringTasks, Contains(Field(&TaskItem::cycletime, Eq(250)))) << "cycletime";

  EXPECT_THAT(recurringTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::Trigger)))) << "taskType Trigger";
  EXPECT_THAT(recurringTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::Update)))) << "taskType Update";
}

TEST_F(AgentParserTest, NonRecurringComponent_IsParsed)
{
  ADD_FAKE_COMPONENT("Compontent1", 100, true);
  INIT_AGENT_PARSER();

  auto recurringTasks = agentParser.GetRecurringTasks();
  EXPECT_THAT(recurringTasks, IsEmpty());

  std::vector<TaskItem> nonRecurringTasks = agentParser.GetNonRecurringTasks();
  ASSERT_THAT(nonRecurringTasks, SizeIs(3));
  ASSERT_EQ(nonRecurringTasks.front().taskType, TaskType::Trigger);

  EXPECT_THAT(nonRecurringTasks, Contains(Field(&TaskItem::cycletime, Eq(100)))) << "cycletime";
  EXPECT_THAT(nonRecurringTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::Trigger)))) << "taskType Trigger";
  EXPECT_THAT(nonRecurringTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::Update)))) << "taskType Update";
}

TEST_F(AgentParserTest, MixedComponents_AreParsedWithRightTaskType)
{
  ADD_FAKE_COMPONENT("Compontent1", 100, false);
  ADD_FAKE_COMPONENT("Compontent2", 50, true);
  INIT_AGENT_PARSER();

  auto recurringTasks = agentParser.GetRecurringTasks();
  auto nonRecurringTasks = agentParser.GetNonRecurringTasks();

  EXPECT_THAT(recurringTasks, Contains(Field(&TaskItem::cycletime, Eq(100)))) << "cycletime";
  EXPECT_THAT(recurringTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::Trigger)))) << "taskType Trigger";
  EXPECT_THAT(recurringTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::Update)))) << "taskType Update";

  EXPECT_THAT(nonRecurringTasks, Contains(Field(&TaskItem::cycletime, Eq(50)))) << "cycletime";
  EXPECT_THAT(nonRecurringTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::Trigger)))) << "taskType Trigger";
  EXPECT_THAT(nonRecurringTasks, Contains(Field(&TaskItem::taskType, Eq(TaskType::Update)))) << "taskType Update";
}
