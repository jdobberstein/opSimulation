/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "autonomousEmergencyBraking.h"
#include "fakeAgent.h"
#include "fakeParameter.h"
#include "fakePublisher.h"
#include "fakeRadio.h"
#include "fakeStochastics.h"
#include "fakeWorld.h"
#include "fakeWorldObject.h"

using ::testing::NiceMock;

class AlgorithmAutonomousEmergencyBraking_UnitTest : public ::testing::Test
{
public:
  AlgorithmAutonomousEmergencyBraking_UnitTest();

  void SetEgoValues(units::velocity::meters_per_second_t velocity,
                    units::acceleration::meters_per_second_squared_t acceleration,
                    units::angular_velocity::radians_per_second_t yawRate);

  std::unique_ptr<AlgorithmAutonomousEmergencyBrakingImplementation> implementation;
  NiceMock<FakeWorld> fakeWorldInterface;
  NiceMock<FakeAgent> fakeEgoAgent;
  NiceMock<FakePublisher> fakePublisher;
};
