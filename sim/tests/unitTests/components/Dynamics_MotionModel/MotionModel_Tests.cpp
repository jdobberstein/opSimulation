/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <numeric>

#include "fakeAgent.h"
#include "fakeParameter.h"
#include "include/agentInterface.h"
#include "motionmodel.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

TEST(MotionModel, CheckVehicleVelocity)
{
  NiceMock<FakeAgent> fakeAgent;

  auto const fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();
  double const fakeMass = 1500;
  double const fakeXPositionCOG = 2;
  double const fakeYPositionCOG = 0.0;
  double const fakeAirDragCoefficient = 0.0;
  double const fakeFrontSurface = 0.0;
  double const fakeMomentInertiaYaw = 10.0;
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("XPositionCOG", std::to_string(fakeXPositionCOG)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("YPositionCOG", std::to_string(fakeYPositionCOG)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("AirDragCoefficient", std::to_string(fakeAirDragCoefficient)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("FrontSurface", std::to_string(fakeFrontSurface)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MomentInertiaYaw", std::to_string(fakeMomentInertiaYaw)));

  fakeVehicleModelParameters->mass = fakeMass * 1_kg;

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->front_axle.track_width = 2.0_m;
  fakeVehicleModelParameters->front_axle.bb_center_to_axle_center.x = 2.0_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.track_width = 2.0_m;
  fakeVehicleModelParameters->rear_axle.bb_center_to_axle_center.x = 0.0_m;

  const Common::Vector2d fakeVelocity = {0.0, 0.0};
  double const fakeYawVelocity = 10.0;
  const units::angle::radian_t fakeSteeringWheelAngle = 1.0_rad;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));
  int const fakeCycleTimeMs = 10;

  Common::Vector2d const fakeAcceleration = {10.0, 10.0};
  double const fakeYawAcceleration = 10.0;
  ON_CALL(fakeAgent, GetYawRate()).WillByDefault(Return(0.0_rad_per_s));
  ON_CALL(fakeAgent, GetYawAcceleration()).WillByDefault(Return((fakeYawAcceleration * 1_rad_per_s_sq)));
  ON_CALL(fakeAgent, GetYaw()).WillByDefault(Return(0.0_rad));
  ON_CALL(fakeAgent, GetVelocity(_))
      .WillByDefault(Return(Common::Vector2d{fakeVelocity.x * 1_mps, fakeVelocity.y * 1_mps}));
  ON_CALL(fakeAgent, GetAcceleration(_))
      .WillByDefault(Return(Common::Vector2d{fakeAcceleration.x * 1_mps_sq, fakeAcceleration.y * 1_mps_sq}));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));
  ON_CALL(fakeAgent, GetPositionX()).WillByDefault(Return(0_m));
  ON_CALL(fakeAgent, GetPositionY()).WillByDefault(Return(0_m));

  DynamicsMotionModelImplementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, nullptr, nullptr, &fakeAgent);

  std::vector<double> const fakeLongitudinalTireForce = {0.0, 0.0, 0.0, 0.0};
  std::vector<double> const fakeLateralTireForce = {0.0, 0.0, 0.0, 0.0};
  std::vector<double> const fakeWheelAngle = {0.0, 0.0, 0.0, 0.0};
  std::vector<double> const fakeSelfAligningTorque = {0.0, 0.0, 0.0, 0.0};

  SignalVectorDouble const fakeSignalLongitudinalTireForce(fakeLongitudinalTireForce);
  SignalVectorDouble const fakeSignalLateralTireForce(fakeLateralTireForce);
  SignalVectorDouble const fakeSignalWheelAngle(fakeWheelAngle);
  SignalVectorDouble const fakeSignalSelfAligningTorque(fakeSelfAligningTorque);

  implementation.UpdateInput(0, std::make_shared<SignalVectorDouble const>(fakeSignalLongitudinalTireForce), 0);
  implementation.UpdateInput(1, std::make_shared<SignalVectorDouble const>(fakeSignalLateralTireForce), 0);
  implementation.UpdateInput(2, std::make_shared<SignalVectorDouble const>(fakeSignalWheelAngle), 0);
  implementation.UpdateInput(3, std::make_shared<SignalVectorDouble const>(fakeSignalSelfAligningTorque), 0);
  implementation.UpdateInput(
      5, std::make_shared<SteeringSignal const>(ComponentState::Acting, fakeSteeringWheelAngle), 0);

  implementation.Trigger(0);

  std::shared_ptr<SignalInterface const> outputSignal;

  implementation.UpdateOutput(0, outputSignal, 0);
  implementation.UpdateOutput(1, outputSignal, 0);
  auto dynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(outputSignal);

  ASSERT_DOUBLE_EQ(fakeAcceleration.x * fakeCycleTimeMs / 1000.0,
                   dynamicsSignal->dynamicsInformation.velocityX.value());
  ASSERT_DOUBLE_EQ(fakeAcceleration.y * fakeCycleTimeMs / 1000.0,
                   dynamicsSignal->dynamicsInformation.velocityY.value());
  ASSERT_DOUBLE_EQ(fakeSteeringWheelAngle.value(), dynamicsSignal->dynamicsInformation.steeringWheelAngle.value());
}

TEST(MotionModel, CheckVehicleAcceleration)
{
  NiceMock<FakeAgent> fakeAgent;

  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();
  double const fakeMass = 1500;
  double const fakeXPositionCOG = 2;
  double const fakeYPositionCOG = 0.0;
  double const fakeAirDragCoefficient = 0.0;
  double const fakeFrontSurface = 0.0;
  double const fakeMomentInertiaYaw = 10.0;
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("XPositionCOG", std::to_string(fakeXPositionCOG)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("YPositionCOG", std::to_string(fakeYPositionCOG)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("AirDragCoefficient", std::to_string(fakeAirDragCoefficient)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("FrontSurface", std::to_string(fakeFrontSurface)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MomentInertiaYaw", std::to_string(fakeMomentInertiaYaw)));

  fakeVehicleModelParameters->mass = fakeMass * 1_kg;

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->front_axle.track_width = 2.0_m;
  fakeVehicleModelParameters->front_axle.bb_center_to_axle_center.x = 2.0_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.track_width = 2.0_m;
  fakeVehicleModelParameters->rear_axle.bb_center_to_axle_center.x = 0.0_m;

  const Common::Vector2d fakeAcceleration = {0.0, 0.0};
  const double fakeYawAcceleration = 0.0;
  const double fakeYawRate = 0.0;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  int const fakeCycleTimeMs = 10;

  Common::Vector2d const fakeVelocity = {0.0, 0.0};

  ON_CALL(fakeAgent, GetYawRate()).WillByDefault(Return(0.0_rad_per_s));
  ON_CALL(fakeAgent, GetYawAcceleration()).WillByDefault(Return((fakeYawAcceleration * 1_rad_per_s_sq)));
  ON_CALL(fakeAgent, GetYaw()).WillByDefault(Return(0.0_rad));
  ON_CALL(fakeAgent, GetVelocity(_))
      .WillByDefault(Return(Common::Vector2d{fakeVelocity.x * 1_mps, fakeVelocity.y * 1_mps}));
  ON_CALL(fakeAgent, GetAcceleration(_))
      .WillByDefault(Return(Common::Vector2d{fakeAcceleration.x * 1_mps_sq, fakeAcceleration.y * 1_mps_sq}));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));
  ON_CALL(fakeAgent, GetPositionX()).WillByDefault(Return(0_m));
  ON_CALL(fakeAgent, GetPositionY()).WillByDefault(Return(0_m));

  DynamicsMotionModelImplementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, nullptr, nullptr, &fakeAgent);

  std::vector<double> fakeLongitudinalTireForce = {1000.0, 1000.0, 1000.0, 1000.0};
  std::vector<double> fakeLateralTireForce = {0.0, 0.0, 0.0, 0.0};
  std::vector<double> fakeWheelAngle = {0.0, 0.0, 0.0, 0.0};
  std::vector<double> const fakeSelfAligningTorque = {0.0, 0.0, 0.0, 0.0};

  SignalVectorDouble const fakeSignalLongitudinalTireForce(fakeLongitudinalTireForce);
  SignalVectorDouble const fakeSignalLateralTireForce(fakeLateralTireForce);
  SignalVectorDouble const fakeSignalWheelAngle(fakeWheelAngle);
  SignalVectorDouble const fakeSignalSelfAligningTorque(fakeSelfAligningTorque);

  std::shared_ptr<SignalInterface const> outputSignal;

  implementation.UpdateInput(0, std::make_shared<SignalVectorDouble const>(fakeSignalLongitudinalTireForce), 0);
  implementation.UpdateInput(1, std::make_shared<SignalVectorDouble const>(fakeSignalLateralTireForce), 0);
  implementation.UpdateInput(2, std::make_shared<SignalVectorDouble const>(fakeSignalWheelAngle), 0);
  implementation.UpdateInput(3, std::make_shared<SignalVectorDouble const>(fakeSignalSelfAligningTorque), 0);

  implementation.Trigger(0);
  implementation.UpdateOutput(1, outputSignal, 0);
  auto dynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(outputSignal);

  Common::Vector2d<double> fakeTireForce;
  double const fakeMomentZ = 0.0;
  for (unsigned int idx = 0; idx < fakeLongitudinalTireForce.size(); idx++)
  {
    fakeTireForce.x = fakeLongitudinalTireForce[idx];
    fakeTireForce.y = fakeLateralTireForce[idx];
    fakeTireForce.Rotate(fakeWheelAngle[idx] * 1_rad);
    fakeLongitudinalTireForce[idx] = fakeTireForce.x;
    fakeLateralTireForce[idx] = fakeTireForce.y;
  }

  ASSERT_NEAR(
      (dynamicsSignal->dynamicsInformation.accelerationX.value()
       + (-dynamicsSignal->dynamicsInformation.yawAcceleration.value() * (fakeYPositionCOG))
       + (-dynamicsSignal->dynamicsInformation.yawRate.value() * dynamicsSignal->dynamicsInformation.yawRate.value()
          * (fakeXPositionCOG))),
      std::accumulate(fakeLongitudinalTireForce.begin(), fakeLongitudinalTireForce.end(), 0.0) / fakeMass,
      (std::accumulate(fakeLongitudinalTireForce.begin(), fakeLongitudinalTireForce.end(), 0.0) / fakeMass) * 0.01);

  ASSERT_NEAR(dynamicsSignal->dynamicsInformation.accelerationY.value()
                  + (dynamicsSignal->dynamicsInformation.yawAcceleration.value() * (fakeXPositionCOG))
                  + (-dynamicsSignal->dynamicsInformation.yawRate.value()
                     * dynamicsSignal->dynamicsInformation.yawRate.value() * (fakeYPositionCOG)),
              std::accumulate(fakeLateralTireForce.begin(), fakeLateralTireForce.end(), 0.0) / fakeMass,
              (std::accumulate(fakeLateralTireForce.begin(), fakeLateralTireForce.end(), 0.0) / fakeMass) * 0.01);
}