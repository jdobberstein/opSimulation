/**********************************************************************
 * Copyright (c) 2018-2019 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <MantleAPI/Traffic/entity_properties.h>
#include <algorithm>
#include <map>
#include <memory>
#include <osi3/osi_common.pb.h>
#include <osi3/osi_object.pb.h>
#include <osi3/osi_sensordata.pb.h>
#include <string>
#include <units.h>
#include <vector>

#include "common/vector2d.h"
#include "core/opSimulation/modules/World_OSI/RadioImplementation.h"
#include "fakeAgent.h"
#include "fakeParameter.h"
#include "fakePublisher.h"
#include "fakeRadio.h"
#include "fakeStochastics.h"
#include "fakeWorld.h"
#include "include/radioInterface.h"
#include "sensorCar2X.h"

class WorldObjectInterface;

using ::testing::_;
using ::testing::Eq;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

TEST(Radio_UnitTests, NoSendersRegistered_DetectsNothing)
{
  RadioImplementation radio{};
  std::vector<osi3::MovingObject> detectedObjects = radio.Receive(0.0_m, 0.0_m, units::sensitivity(1e-3));

  ASSERT_TRUE(detectedObjects.empty());
}

TEST(Radio_UnitTests, TwoSendersInProximityRegistered_DetectsTwoObjects)
{
  osi3::MovingObject remoteObject;
  RadioImplementation radio{};

  radio.Send(20.0_m, 0.0_m, 100.0_W, remoteObject);
  radio.Send(100.0_m, 100.0_m, 100.0_W, remoteObject);
  std::vector<osi3::MovingObject> detectedObjects = radio.Receive(0.0_m, 0.0_m, units::sensitivity(1e-7));

  ASSERT_EQ(detectedObjects.size(), 2);
}

TEST(Radio_UnitTests, ThreeSendersRegisteredOneTooFarAway_DetectsTwoObjects)
{
  osi3::MovingObject remoteObject;
  RadioImplementation radio{};

  radio.Send(20.0_m, 20.0_m, 100.0_W, remoteObject);
  radio.Send(100.0_m, 100.0_m, 100.0_W, remoteObject);
  radio.Send(1000.0_m, 1000.0_m, 1.0_W, remoteObject);
  std::vector<osi3::MovingObject> detectedObjects = radio.Receive(0.0_m, 0.0_m, units::sensitivity(1e-6));

  ASSERT_EQ(detectedObjects.size(), 2);
}

TEST(Radio_UnitTests, TwoSendersRegisteredAtSensivityThreshold_DetectsTwoObjects)
{
  osi3::MovingObject remoteObject;
  RadioImplementation radio{};

  radio.Send(0.0_m, 0.0_m, 5.0_W, remoteObject);
  radio.Send(1000.0_m, 0.0_m, 5.0_W, remoteObject);
  std::vector<osi3::MovingObject> detectedObjects = radio.Receive(500.0_m, 0.0_m, units::sensitivity(1.5e-6));

  ASSERT_EQ(detectedObjects.size(), 2);
}

TEST(Radio_UnitTests, OneSensorAtSensitivityThresholdOneOutsideThreshold_DetectsOneObject)
{
  osi3::MovingObject remoteObject;
  RadioImplementation radio{};

  radio.Send(0.0_m, 0.0_m, 5.0_W, remoteObject);
  radio.Send(1050.0_m, 0.0_m, 5.0_W, remoteObject);
  std::vector<osi3::MovingObject> detectedObjects = radio.Receive(500.0_m, 0_m, units::sensitivity(1.5e-6));

  ASSERT_EQ(detectedObjects.size(), 1);
}

TEST(Radio_UnitTests, OneMountedSensor_DetectsOneObject)
{
  NiceMock<FakeWorld> fakeWorldInterface;

  NiceMock<FakeStochastics> fakeStochastics;
  ON_CALL(fakeStochastics, GetUniformDistributed(_, _)).WillByDefault(Return(1));
  ON_CALL(fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(1));

  NiceMock<FakeParameter> fakeParameters;
  NiceMock<FakePublisher> fakePublisher;

  std::map<std::string, double> fakeDoubles = {{"FailureProbability", 0}, {"Latency", 0}, {"Sensitivity", 1e-5}};
  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeDoubles));

  std::map<std::string, int> fakeInts = {{"Id", 0}};
  ON_CALL(fakeParameters, GetParametersInt()).WillByDefault(ReturnRef(fakeInts));

  std::shared_ptr<mantle_api::VehicleProperties> fakeVehicleModelParameters
      = std::make_shared<mantle_api::VehicleProperties>();
  fakeVehicleModelParameters->properties = {{"SensorPosition/Position1/Longitudinal", "1.0"},
                                            {"SensorPosition/Position1/Lateral", "1.0"},
                                            {"SensorPosition/Position1/Height", "0.0"},
                                            {"SensorPosition/Position1/Pitch", "0.0"},
                                            {"SensorPosition/Position1/Yaw", "0.0"},
                                            {"SensorPosition/Position1/Roll", "0.0"}};

  std::map<std::string, const std::string> fakeStrings = {{"Position", "Position1"}};
  ON_CALL(fakeParameters, GetParametersString()).WillByDefault(ReturnRef(fakeStrings));

  NiceMock<FakeAgent> fakeAgentInterface;
  ON_CALL(fakeAgentInterface, GetId()).WillByDefault(Return(0));
  ON_CALL(fakeAgentInterface, GetPositionX()).WillByDefault(Return(100_m));
  ON_CALL(fakeAgentInterface, GetPositionY()).WillByDefault(Return(100_m));
  ON_CALL(fakeAgentInterface, GetYaw()).WillByDefault(Return(0_rad));
  ON_CALL(fakeAgentInterface, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  std::vector<const WorldObjectInterface*> fakeObjects;
  fakeObjects.push_back(&fakeAgentInterface);
  ON_CALL(fakeWorldInterface, GetWorldObjects()).WillByDefault(ReturnRef(fakeObjects));

  osi3::MovingObject movingObject1;
  movingObject1.mutable_id()->set_value(0);
  osi3::MovingObject movingObject2;
  movingObject2.mutable_id()->set_value(1);

  //Manipulate Radio
  std::vector<osi3::MovingObject> car2XObjects = {movingObject1, movingObject2};
  NiceMock<FakeRadio> fakeRadio;
  EXPECT_CALL(fakeRadio, Receive(Eq(101_m), Eq(101_m), Eq(units::sensitivity(1e-5)))).WillOnce(Return(car2XObjects));
  ON_CALL(fakeWorldInterface, GetRadio()).WillByDefault(ReturnRef(fakeRadio));

  SensorCar2X sensor("",
                     false,
                     0,
                     0,
                     0,
                     0,
                     &fakeStochastics,
                     &fakeWorldInterface,
                     &fakeParameters,
                     &fakePublisher,
                     nullptr,
                     &fakeAgentInterface);

  auto sensorData = sensor.DetectObjects();
  ASSERT_EQ(sensorData.moving_object_size(), 1);
}
