/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <MantleAPI/Traffic/entity_properties.h>
#include <algorithm>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeParameter.h"
#include "fakePublisher.h"
#include "objectDetectorBase.h"

class AgentInterface;
class CallbackInterface;
class ParameterInterface;
class PublisherInterface;
class SignalInterface;

using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::UnorderedElementsAreArray;

class TestSensor : public ObjectDetectorBase
{
public:
  TestSensor(int cycleTime,
             const ParameterInterface* parameters,
             PublisherInterface* const publisher,
             const CallbackInterface* callbacks,
             AgentInterface* agent)
      : ObjectDetectorBase(
          "TestSensor", false, 0, 0, 0, cycleTime, nullptr, nullptr, parameters, publisher, callbacks, agent)
  {
  }

  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const>& data, int time) override {}

  void Trigger(int time) override {}

  void NewTimeStep(int time)
  {
    this->time = time;
    detectedObjects.clear();
  }

  void DetectObject(ObjectId id)
  {
    if (CheckDelay(time, id))
    {
      detectedObjects.push_back(id);
    }
  }

  std::vector<ObjectId> GetDetectedObjects() { return detectedObjects; }

private:
  int time{0};
  std::vector<ObjectId> detectedObjects;
};

class DelayTest_Data
{
public:
  struct DetectableObject
  {
    ObjectDetectorBase::ObjectId id;
    std::vector<bool> inRange;  ///< Wether the object is in range for each timestep
  };

  double delay;
  double maxDropOutTime;
  std::vector<DetectableObject> objects;
  std::vector<std::vector<ObjectDetectorBase::ObjectId>>
      expectedObjects;  ///< List of expected detected objects for each timestep
};

class DelayTest : public ::testing::TestWithParam<DelayTest_Data>
{
public:
  DelayTest() : fakeBools({})
  {
    ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeDoubles));

    fakeInts = {{"Id", 0}};
    ON_CALL(fakeParameters, GetParametersInt()).WillByDefault(ReturnRef(fakeInts));

    ON_CALL(fakeParameters, GetParametersBool()).WillByDefault(ReturnRef(fakeBools));

    fakeStrings = {{"Position", "Position1"}};
    ON_CALL(fakeParameters, GetParametersString()).WillByDefault(ReturnRef(fakeStrings));

    fakeVehicleModelParameters->properties = {{"SensorPosition/Position1/Longitudinal", "0.0"},
                                              {"SensorPosition/Position1/Lateral", "0.0"},
                                              {"SensorPosition/Position1/Height", "0.0"},
                                              {"SensorPosition/Position1/Yaw", "0.0"},
                                              {"SensorPosition/Position1/Pitch", "0.0"},
                                              {"SensorPosition/Position1/Roll", "0.0"}};
    ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));
  }

  FakeAgent fakeAgent;
  FakeParameter fakeParameters;
  FakeCallback fakeCallbacks;
  FakePublisher fakePublisher;
  std::map<std::string, double> fakeDoubles;
  std::map<std::string, int> fakeInts;
  std::map<std::string, bool> fakeBools;
  std::map<std::string, const std::string> fakeStrings;
  std::shared_ptr<mantle_api::VehicleProperties> fakeVehicleModelParameters
      = std::make_shared<mantle_api::VehicleProperties>();
};

TEST_P(DelayTest, CheckDelayReturnsCorrectResult)
{
  auto data = GetParam();

  fakeDoubles = {{"FailureProbability", 0},
                 {"Latency", 0},
                 {"DetectionDelayTime", data.delay},
                 {"MaxDropOutTime", data.maxDropOutTime}};
  auto sensor = TestSensor{10, &fakeParameters, &fakePublisher, &fakeCallbacks, &fakeAgent};

  std::map<ObjectDetectorBase::ObjectId, std::vector<bool>::iterator> objectsInRageIterators;

  for (auto& object : data.objects)
  {
    objectsInRageIterators[object.id] = object.inRange.begin();
  }

  int time = 0;

  for (auto& expectedObjectsInTimestep : data.expectedObjects)
  {
    sensor.NewTimeStep(time);
    for (auto& [id, inRange] : objectsInRageIterators)
    {
      if (*inRange)
      {
        sensor.DetectObject(id);
      }
      ++inRange;
    }
    EXPECT_THAT(sensor.GetDetectedObjects(), UnorderedElementsAreArray(expectedObjectsInTimestep));
    time += 10;
  }
}

// clang-format off
INSTANTIATE_TEST_CASE_P(
    OneObject,
    DelayTest,
    ::testing::Values(DelayTest_Data{0., 0, {{7, {false, true, true, true, true}}}, {{}, {7}, {7}, {7}, {7}}},
                      DelayTest_Data{0.02, 0, {{7, {false, true, true, true, true}}}, {{}, {}, {}, {7}, {7}}},
                      DelayTest_Data{0.02, 0, {{7, {false, true, false, true, true}}}, {{}, {}, {}, {}, {}}},
                      DelayTest_Data{0.02, 0.02, {{7, {false, true, false, true, true}}}, {{}, {}, {}, {7}, {7}}}));

INSTANTIATE_TEST_CASE_P(
    TwoObjects,
    DelayTest,
    ::testing::Values(DelayTest_Data{0., 0, {{7, {false, true, true, true, true}}, {13, {true, false, true, false, true}}}, {{13}, {7}, {7, 13}, {7}, {7, 13}}},
                      DelayTest_Data{0.03, 0, {{7, {false, true, true, true, true}}, {13, {true, false, true, false, true}}}, {{}, {}, {}, {}, {7}}},
                      DelayTest_Data{0.03, 0.02, {{7, {false, true, false, false, true}}, {13, {true, false, true, false, true}}}, {{}, {}, {}, {}, {13}}}));
