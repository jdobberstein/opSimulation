/*******************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <osi3/osi_sensordata.pb.h>

#include "fmuFileHelper.h"

//#include "include/fmuHandlerInterface.h"
#include "FmuHandler.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeParameter.h"
#include "fakeRadio.h"
#include "fakeWorld.h"

//#include "include/parameterInterface.h"
//#include "common/sensorDataSignal.h"

#include <filesystem>
#include <sstream>
#include <vector>

std::vector<std::string> split(const std::string& string, const std::string& delimiter)
{
  size_t pos_start = 0, pos_end{}, delim_len = delimiter.length();
  std::string token;
  std::vector<std::string> res;

  while ((pos_end = string.find(delimiter, pos_start)) != std::string::npos)
  {
    token = string.substr(pos_start, pos_end - pos_start);
    pos_start = pos_end + delim_len;
    res.push_back(token);
  }

  res.push_back(string.substr(pos_start));
  return res;
}

TEST(FmuFileHelperTests, TestGenerateFilename)
{
  FmuFileHelper::TraceEntry traceEntry;
  traceEntry.time = 0;
  traceEntry.osiType = "sv";

  const std::pair<const std::string, FmuFileHelper::TraceEntry> fileToOutputTrace("test", traceEntry);
  std::string outputType = "TestOutputType";

  auto fileName = FmuFileHelper::GenerateTraceFileName(outputType, fileToOutputTrace);

  std::string delimiter = "_";
  std::vector<std::string> fileNameSplits = split(fileName, delimiter);

  ASSERT_EQ(fileNameSplits.size(), 6);
  ASSERT_EQ(fileNameSplits[1], "sv");

  const auto currentInterfaceVersion
      = osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version);
  std::stringstream osiVersion;
  osiVersion << std::to_string(currentInterfaceVersion.version_major());
  osiVersion << currentInterfaceVersion.version_minor();
  osiVersion << currentInterfaceVersion.version_patch();
  ASSERT_EQ(fileNameSplits[2], osiVersion.str());

  ASSERT_EQ(fileNameSplits[3], std::to_string(GOOGLE_PROTOBUF_VERSION));
  ASSERT_EQ(fileNameSplits[4], std::to_string(0));

  delimiter = ".";
  std::vector<std::string> fileEndingSplit = split(fileNameSplits[5], delimiter);

  ASSERT_EQ(fileEndingSplit.size(), 2);
  ASSERT_EQ(fileEndingSplit[0], outputType);
  ASSERT_EQ(fileEndingSplit[1], "osi");
}
