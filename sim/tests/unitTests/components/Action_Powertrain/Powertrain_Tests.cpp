/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <numeric>

#include "common/longitudinalSignal.h"
#include "fakeAgent.h"
#include "fakeParameter.h"
#include "powertrain.h"

using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

TEST(Powertrain, AccelerationInFirstGearFWD)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, const std::string> fakeParametersString;
  std::map<std::string, double> fakeParametersDouble;

  double const fakeFrontRatio = 0.6;
  fakeParametersString.insert(std::pair<std::string, std::string>("TypeDrivetrain", "FWD"));
  fakeParametersDouble.insert(std::pair<std::string, double>("FrontRatioAWD", fakeFrontRatio));
  int const fakeCycleTimeMs = 10;

  double const fakeMaximumEnginePower = 1000;
  double const fakeMaximumEngineSpeed = 10000;
  double const fakeMaximumEngineTorque = 60;
  double const fakeAxleRatio = 1.5;
  double const fakeGearRatio1 = 2.0;
  double const fakeGearRatio2 = 3;

  ON_CALL(fakeParameters, GetParametersString()).WillByDefault(ReturnRef(fakeParametersString));
  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  NiceMock<FakeAgent> fakeAgent;
  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();

  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEnginePower", std::to_string(fakeMaximumEnginePower)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineSpeed", std::to_string(fakeMaximumEngineSpeed)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineTorque", std::to_string(fakeMaximumEngineTorque)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("AxleRatio", std::to_string(fakeAxleRatio)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio1", std::to_string(fakeGearRatio1)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio2", std::to_string(fakeGearRatio2)));

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  ActionPowertrain implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;

  std::vector<double> fakeWheelRotationRate = {10.0, 10.0, 10.0, 10.0};

  double const fakeAcceleratorPedalPos = 1.0;
  const auto fakeSignalLongitudinalSignal
      = std::make_shared<LongitudinalSignal const>(ComponentState::Acting, fakeAcceleratorPedalPos, 0.0, 1);

  const auto fakeSignalWheelRotationRate = std::make_shared<SignalVectorDouble const>(fakeWheelRotationRate);
  implementation.UpdateInput(0, fakeSignalLongitudinalSignal, 0);
  implementation.UpdateInput(1, fakeSignalWheelRotationRate, 0);
  int const fakeTimeMs = 0;
  std::vector<double> wheelDriveTorque;

  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelDriveTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;

  ASSERT_DOUBLE_EQ(wheelDriveTorque[2], 0.0);
  ASSERT_DOUBLE_EQ(wheelDriveTorque[3], 0.0);

  ASSERT_DOUBLE_EQ(wheelDriveTorque[0], wheelDriveTorque[1]);
  ASSERT_LE(std::accumulate(wheelDriveTorque.begin(), wheelDriveTorque.end(), 0.0) / (fakeAxleRatio * fakeGearRatio1),
            fakeMaximumEngineTorque);
  ASSERT_DOUBLE_EQ(wheelDriveTorque[0] * fakeWheelRotationRate[0], 0.5 * fakeMaximumEnginePower);
}

TEST(Powertrain, AccelerationInSecondGearFWD)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, const std::string> fakeParametersString;
  std::map<std::string, double> fakeParametersDouble;

  double const fakeFrontRatio = 0.6;
  fakeParametersString.insert(std::pair<std::string, std::string>("TypeDrivetrain", "FWD"));
  fakeParametersDouble.insert(std::pair<std::string, double>("FrontRatioAWD", fakeFrontRatio));
  int const fakeCycleTimeMs = 10;

  double const fakeMaximumEnginePower = 1000;
  double const fakeMaximumEngineSpeed = 10000;
  double const fakeMaximumEngineTorque = 60;
  double const fakeAxleRatio = 1.5;
  double const fakeGearRatio1 = 2.0;
  double const fakeGearRatio2 = 3;

  ON_CALL(fakeParameters, GetParametersString()).WillByDefault(ReturnRef(fakeParametersString));
  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  NiceMock<FakeAgent> fakeAgent;
  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();

  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEnginePower", std::to_string(fakeMaximumEnginePower)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineSpeed", std::to_string(fakeMaximumEngineSpeed)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineTorque", std::to_string(fakeMaximumEngineTorque)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("AxleRatio", std::to_string(fakeAxleRatio)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio1", std::to_string(fakeGearRatio1)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio2", std::to_string(fakeGearRatio2)));

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  ActionPowertrain implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;

  std::vector<double> fakeWheelRotationRate = {10.0, 10.0, 10.0, 10.0};

  double const fakeAcceleratorPedalPos = 1.0;
  const auto fakeSignalLongitudinalSignal
      = std::make_shared<LongitudinalSignal const>(ComponentState::Acting, fakeAcceleratorPedalPos, 0.0, 2);

  const auto fakeSignalWheelRotationRate = std::make_shared<SignalVectorDouble const>(fakeWheelRotationRate);
  implementation.UpdateInput(0, fakeSignalLongitudinalSignal, 0);
  implementation.UpdateInput(1, fakeSignalWheelRotationRate, 0);
  int const fakeTimeMs = 0;
  std::vector<double> wheelDriveTorque;

  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelDriveTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;

  ASSERT_DOUBLE_EQ(wheelDriveTorque[2], 0.0);
  ASSERT_DOUBLE_EQ(wheelDriveTorque[3], 0.0);

  ASSERT_DOUBLE_EQ(wheelDriveTorque[0], wheelDriveTorque[1]);
  ASSERT_LE(std::accumulate(wheelDriveTorque.begin(), wheelDriveTorque.end(), 0.0) / (fakeAxleRatio * fakeGearRatio1),
            fakeMaximumEngineTorque);
  ASSERT_DOUBLE_EQ(wheelDriveTorque[0] * fakeWheelRotationRate[0], 0.5 * fakeMaximumEnginePower);
}

TEST(Powertrain, AccelerationInNonExistingGearFWD)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, const std::string> fakeParametersString;
  std::map<std::string, double> fakeParametersDouble;

  double const fakeFrontRatio = 0.6;
  fakeParametersString.insert(std::pair<std::string, std::string>("TypeDrivetrain", "FWD"));
  fakeParametersDouble.insert(std::pair<std::string, double>("FrontRatioAWD", fakeFrontRatio));
  int const fakeCycleTimeMs = 10;

  double const fakeMaximumEnginePower = 1000;
  double const fakeMaximumEngineSpeed = 10000;
  double const fakeMaximumEngineTorque = 60;
  double const fakeAxleRatio = 1.5;
  double const fakeGearRatio1 = 2.0;
  double const fakeGearRatio2 = 3;

  ON_CALL(fakeParameters, GetParametersString()).WillByDefault(ReturnRef(fakeParametersString));
  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  NiceMock<FakeAgent> fakeAgent;
  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();

  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEnginePower", std::to_string(fakeMaximumEnginePower)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineSpeed", std::to_string(fakeMaximumEngineSpeed)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineTorque", std::to_string(fakeMaximumEngineTorque)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("AxleRatio", std::to_string(fakeAxleRatio)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio1", std::to_string(fakeGearRatio1)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio2", std::to_string(fakeGearRatio2)));

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  ActionPowertrain implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;

  std::vector<double> const fakeWheelRotationRate = {10.0, 10.0, 10.0, 10.0};

  double const fakeAcceleratorPedalPos = 1.0;
  const auto fakeSignalLongitudinalSignal
      = std::make_shared<LongitudinalSignal const>(ComponentState::Acting, fakeAcceleratorPedalPos, 0.0, 3);

  const auto fakeSignalWheelRotationRate = std::make_shared<SignalVectorDouble const>(fakeWheelRotationRate);
  implementation.UpdateInput(0, fakeSignalLongitudinalSignal, 0);
  implementation.UpdateInput(1, fakeSignalWheelRotationRate, 0);
  int const fakeTimeMs = 0;
  std::vector<double> wheelDriveTorque;

  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelDriveTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;

  ASSERT_DOUBLE_EQ(wheelDriveTorque[0], 0.0);
  ASSERT_DOUBLE_EQ(wheelDriveTorque[1], 0.0);
  ASSERT_DOUBLE_EQ(wheelDriveTorque[2], 0.0);
  ASSERT_DOUBLE_EQ(wheelDriveTorque[3], 0.0);
}

TEST(Powertrain, AccelerationInFirstGearFromStandstillFWD)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, const std::string> fakeParametersString;
  std::map<std::string, double> fakeParametersDouble;

  double const fakeFrontRatio = 0.6;
  fakeParametersString.insert(std::pair<std::string, std::string>("TypeDrivetrain", "FWD"));
  fakeParametersDouble.insert(std::pair<std::string, double>("FrontRatioAWD", fakeFrontRatio));
  int const fakeCycleTimeMs = 10;

  double const fakeMaximumEnginePower = 1000;
  double const fakeMaximumEngineSpeed = 10000;
  double const fakeMaximumEngineTorque = 60;
  double const fakeAxleRatio = 1.5;
  double const fakeGearRatio1 = 2.0;
  double const fakeGearRatio2 = 3;

  ON_CALL(fakeParameters, GetParametersString()).WillByDefault(ReturnRef(fakeParametersString));
  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  NiceMock<FakeAgent> fakeAgent;
  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();

  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEnginePower", std::to_string(fakeMaximumEnginePower)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineSpeed", std::to_string(fakeMaximumEngineSpeed)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineTorque", std::to_string(fakeMaximumEngineTorque)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("AxleRatio", std::to_string(fakeAxleRatio)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio1", std::to_string(fakeGearRatio1)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio2", std::to_string(fakeGearRatio2)));

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  ActionPowertrain implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;

  std::vector<double> const fakeWheelRotationRate = {0.0, 0.0, 0.0, 0.0};

  double const fakeAcceleratorPedalPos = 1.0;
  const auto fakeSignalLongitudinalSignal
      = std::make_shared<LongitudinalSignal const>(ComponentState::Acting, fakeAcceleratorPedalPos, 0.0, 1);

  const auto fakeSignalWheelRotationRate = std::make_shared<SignalVectorDouble const>(fakeWheelRotationRate);
  implementation.UpdateInput(0, fakeSignalLongitudinalSignal, 0);
  implementation.UpdateInput(1, fakeSignalWheelRotationRate, 0);
  int const fakeTimeMs = 0;
  std::vector<double> wheelDriveTorque;

  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelDriveTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;

  ASSERT_DOUBLE_EQ(wheelDriveTorque[2], 0.0);
  ASSERT_DOUBLE_EQ(wheelDriveTorque[3], 0.0);

  ASSERT_DOUBLE_EQ(wheelDriveTorque[0], wheelDriveTorque[1]);
  ASSERT_LE(std::accumulate(wheelDriveTorque.begin(), wheelDriveTorque.end(), 0.0) / (fakeAxleRatio * fakeGearRatio1),
            fakeMaximumEngineTorque);
}

TEST(Powertrain, AccelerationInFirstGearRWD)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, const std::string> fakeParametersString;
  std::map<std::string, double> fakeParametersDouble;

  double const fakeFrontRatio = 0.6;
  fakeParametersString.insert(std::pair<std::string, std::string>("TypeDrivetrain", "RWD"));
  fakeParametersDouble.insert(std::pair<std::string, double>("FrontRatioAWD", fakeFrontRatio));
  int const fakeCycleTimeMs = 10;

  double const fakeMaximumEnginePower = 1000;
  double const fakeMaximumEngineSpeed = 10000;
  double const fakeMaximumEngineTorque = 60;
  double const fakeAxleRatio = 1.5;
  double const fakeGearRatio1 = 2.0;
  double const fakeGearRatio2 = 3;

  ON_CALL(fakeParameters, GetParametersString()).WillByDefault(ReturnRef(fakeParametersString));
  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  NiceMock<FakeAgent> fakeAgent;
  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();

  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEnginePower", std::to_string(fakeMaximumEnginePower)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineSpeed", std::to_string(fakeMaximumEngineSpeed)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineTorque", std::to_string(fakeMaximumEngineTorque)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("AxleRatio", std::to_string(fakeAxleRatio)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio1", std::to_string(fakeGearRatio1)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio2", std::to_string(fakeGearRatio2)));

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  ActionPowertrain implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;

  std::vector<double> fakeWheelRotationRate = {10.0, 10.0, 10.0, 10.0};

  double const fakeAcceleratorPedalPos = 1.0;
  const auto fakeSignalLongitudinalSignal
      = std::make_shared<LongitudinalSignal const>(ComponentState::Acting, fakeAcceleratorPedalPos, 0.0, 1);

  const auto fakeSignalWheelRotationRate = std::make_shared<SignalVectorDouble const>(fakeWheelRotationRate);
  implementation.UpdateInput(0, fakeSignalLongitudinalSignal, 0);
  implementation.UpdateInput(1, fakeSignalWheelRotationRate, 0);
  int const fakeTimeMs = 0;
  std::vector<double> wheelDriveTorque;

  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelDriveTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;

  ASSERT_DOUBLE_EQ(wheelDriveTorque[0], 0.0);
  ASSERT_DOUBLE_EQ(wheelDriveTorque[1], 0.0);

  ASSERT_DOUBLE_EQ(wheelDriveTorque[2], wheelDriveTorque[3]);
  ASSERT_LE(std::accumulate(wheelDriveTorque.begin(), wheelDriveTorque.end(), 0.0) / (fakeAxleRatio * fakeGearRatio1),
            fakeMaximumEngineTorque);
  ASSERT_DOUBLE_EQ(wheelDriveTorque[2] * fakeWheelRotationRate[2], 0.5 * fakeMaximumEnginePower);
}

TEST(Powertrain, AccelerationInFirstGearAWD)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, const std::string> fakeParametersString;
  std::map<std::string, double> fakeParametersDouble;

  double const fakeFrontRatio = 0.6;
  fakeParametersString.insert(std::pair<std::string, std::string>("TypeDrivetrain", "AWD"));
  fakeParametersDouble.insert(std::pair<std::string, double>("FrontRatioAWD", fakeFrontRatio));
  int const fakeCycleTimeMs = 10;

  double const fakeMaximumEnginePower = 1000;
  double const fakeMaximumEngineSpeed = 10000;
  double const fakeMaximumEngineTorque = 60;
  double const fakeAxleRatio = 1.5;
  double const fakeGearRatio1 = 2.0;
  double const fakeGearRatio2 = 3;

  ON_CALL(fakeParameters, GetParametersString()).WillByDefault(ReturnRef(fakeParametersString));
  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  NiceMock<FakeAgent> fakeAgent;
  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();

  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEnginePower", std::to_string(fakeMaximumEnginePower)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineSpeed", std::to_string(fakeMaximumEngineSpeed)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineTorque", std::to_string(fakeMaximumEngineTorque)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("AxleRatio", std::to_string(fakeAxleRatio)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio1", std::to_string(fakeGearRatio1)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio2", std::to_string(fakeGearRatio2)));

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  ActionPowertrain implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;

  std::vector<double> const fakeWheelRotationRate = {10.0, 10.0, 10.0, 10.0};

  double const fakeAcceleratorPedalPos = 1.0;
  const auto fakeSignalLongitudinalSignal
      = std::make_shared<LongitudinalSignal const>(ComponentState::Acting, fakeAcceleratorPedalPos, 0.0, 1);

  const auto fakeSignalWheelRotationRate = std::make_shared<SignalVectorDouble const>(fakeWheelRotationRate);
  implementation.UpdateInput(0, fakeSignalLongitudinalSignal, 0);
  implementation.UpdateInput(1, fakeSignalWheelRotationRate, 0);
  int const fakeTimeMs = 0;
  std::vector<double> wheelDriveTorque;

  implementation.Trigger(fakeTimeMs);
  implementation.UpdateOutput(0, outputSignal, fakeTimeMs);
  wheelDriveTorque = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;

  ASSERT_DOUBLE_EQ(wheelDriveTorque[0], wheelDriveTorque[1]);
  ASSERT_DOUBLE_EQ(wheelDriveTorque[0] / fakeFrontRatio, wheelDriveTorque[2] / (1 - fakeFrontRatio));
  ASSERT_DOUBLE_EQ(wheelDriveTorque[2], wheelDriveTorque[3]);
  ASSERT_LE(std::accumulate(wheelDriveTorque.begin(), wheelDriveTorque.end(), 0.0) / (fakeAxleRatio * fakeGearRatio1),
            fakeMaximumEngineTorque);
}

TEST(Powertrain, CheckExceptionErrors)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, const std::string> fakeParametersString;
  std::map<std::string, double> fakeParametersDouble;

  double const fakeFrontRatio = 0.6;
  fakeParametersString.insert(std::pair<std::string, std::string>("TypeDrivetrain", "FWD"));
  fakeParametersDouble.insert(std::pair<std::string, double>("FrontRatioAWD", fakeFrontRatio));
  int const fakeCycleTimeMs = 10;

  double const fakeMaximumEnginePower = 1000;
  double const fakeMaximumEngineSpeed = 10000;
  double const fakeMaximumEngineTorque = 60;
  double const fakeAxleRatio = 1.5;
  double const fakeGearRatio1 = 2.0;
  double const fakeGearRatio2 = 3;

  ON_CALL(fakeParameters, GetParametersString()).WillByDefault(ReturnRef(fakeParametersString));
  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  NiceMock<FakeAgent> fakeAgent;
  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();

  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEnginePower", std::to_string(fakeMaximumEnginePower)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineSpeed", std::to_string(fakeMaximumEngineSpeed)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEngineTorque", std::to_string(fakeMaximumEngineTorque)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("AxleRatio", std::to_string(fakeAxleRatio)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio1", std::to_string(fakeGearRatio1)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("GearRatio2", std::to_string(fakeGearRatio2)));

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  ActionPowertrain implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  ASSERT_THROW(implementation.UpdateInput(0, nullptr, 0), std::runtime_error);
  ASSERT_THROW(implementation.UpdateInput(1, nullptr, 0), std::runtime_error);
  ASSERT_THROW(implementation.UpdateInput(2, nullptr, 0), std::runtime_error);
}