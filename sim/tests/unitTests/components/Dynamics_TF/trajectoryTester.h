/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include <map>
#include <memory>
#include <string>
#include <vector>

#include "dontCare.h"
#include "fakeAgent.h"
#include "fakeParameter.h"
#include "fakePublisher.h"
#include "fakeScenarioControl.h"
#include "fakeWorld.h"
#include "tfImplementation.h"

using ::testing::DontCare;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

class TrajectoryTester
{
public:
  static const int CC_LOCAL_LINK_ID = 83;

  std::vector<int> fakeActingAgents{1};

  NiceMock<FakePublisher> fakePublisher;

  std::map<std::string, bool> fakeBools;
  NiceMock<FakeParameter> fakeParameters;

  NiceMock<FakeAgent> fakeAgent;

  std::shared_ptr<TrajectoryFollowerImplementation> trajectoryFollower;

  std::shared_ptr<FakeScenarioControl> scenarioControl = std::make_shared<FakeScenarioControl>();

  TrajectoryTester(const int cycleTime = 100);

  TrajectoryTester(const bool enforceTrajectory, const bool automaticDeactivation, const int cycleTime = 100);

  TrajectoryTester(const bool enforceTrajectory,
                   const bool automaticDeactivation,
                   FakeWorld *fakeWorld,
                   const int cycleTime = 100);

  TrajectoryTester(const bool enforceTrajectory,
                   const bool automaticDeactivation,
                   FakeWorld *fakeWorld,
                   FakeAgent *fakeAgent,
                   const int cycleTime = 100);
};
