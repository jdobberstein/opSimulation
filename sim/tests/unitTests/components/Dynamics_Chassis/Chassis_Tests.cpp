/********************************************************************************
 * Copyright (c) 2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <numeric>

#include "WheelOscillation.h"
#include "fakeAgent.h"


TEST(Dynamics, WheelOscillation)
{
  for (unsigned int run = 1; run < 10; run++)
  {
    int const fakeId = 1;
    double const fakeTimeStep = 1.0 * run;
    double const fakeCoeffSpring = 1000.0 * 2 * run;
    double const fakeCoeffDamp = 2000.0 * 3 * run;
    units::mass::kilogram_t const fakeMass = 200.0_kg * (1 + (run % 3));
    units::force::newton_t const fakeForceZ = 100.0_N * run * run;

    WheelOscillation fakeWheelOscillation;
    fakeWheelOscillation.Init(fakeId, fakeTimeStep, fakeCoeffSpring, fakeCoeffDamp);

    double fakeCurVelocity = 0.0;
    double fakeCurAcceleration = 0.0;

    double positionZ = fakeWheelOscillation.GetCurZPos();
    fakeWheelOscillation.Perform(fakeForceZ, fakeMass);

    fakeCurAcceleration
        = (fakeForceZ.value() - fakeCoeffSpring * positionZ - fakeCoeffDamp * fakeCurVelocity) / (fakeMass.value());

    positionZ += fakeCurVelocity * fakeTimeStep + 0.5 * fakeCurAcceleration * fakeTimeStep * fakeTimeStep;
    fakeCurVelocity += fakeCurAcceleration * fakeTimeStep;
    positionZ = (positionZ > 0.01) ? 0.01 : positionZ;
    positionZ = (positionZ < -0.04) ? -0.04 : positionZ;
    ASSERT_DOUBLE_EQ(fakeWheelOscillation.GetCurZPos(), positionZ);
  }
}
