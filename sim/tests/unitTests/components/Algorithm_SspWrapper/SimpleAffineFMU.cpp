/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SimpleAffineFMU.h"

#include <variant>

#include "common/primitiveSignals.h"
#include "include/signalInterface.h"

void ssp::SimpleAffineFMU::Init()
{
  if (isInitialized) return;
  fmiValues[0] = {};
  fmiValues[1] = {};
  fmiValues[2] = {};
  fmiValues[3] = {};
  isInitialized = true;
  triggerCounter = 0;
}
void ssp::SimpleAffineFMU::UpdateInput(int localLinkId, const std::shared_ptr<const SignalInterface> &data, int)
{
  if (localLinkId == 0)
  {
    fmiValues[0].realValue = std::dynamic_pointer_cast<DoubleSignal const>(data)->value;
  }
  if (localLinkId == 1)
  {
    fmiValues[1].realValue = std::dynamic_pointer_cast<DoubleSignal const>(data)->value;
  }
}

void ssp::SimpleAffineFMU::UpdateOutput(int, std::shared_ptr<const SignalInterface> &data, int)
{
  data = std::make_shared<const DoubleSignal>(fmiValues[2].realValue, ComponentState::Acting);
}

void ssp::SimpleAffineFMU::Trigger(int)
{
  fmiValues[2].realValue = fmiValues[0].realValue * factor + fmiValues[1].realValue;
  fmiValues[3].realValue = fmiValues[2].realValue * 2;
  triggerCounter++;
}
ssp::SimpleAffineFMU::SimpleAffineFMU(double factor, int priority) : factor(factor), priority(priority)
{
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("fmu" + std::to_string(static_cast<int>(factor)) + "_rate",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("fmu" + std::to_string(static_cast<int>(factor)) + "_offset",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("fmu" + std::to_string(static_cast<int>(factor)) + "_result",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, fmi2_causality_enu_output, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("fmu" + std::to_string(static_cast<int>(factor)) + "_result2",
               *std::make_shared<FmuVariable2>(
                   3, VariableType::Int, fmi2_causality_enu_output, fmi2_variability_enu_discrete));
}
void ssp::SimpleAffineFMU::UpdateInput(size_t index, double data)
{
  if (index == 0)
  {
    fmiValues[0].realValue = data;
  }
  if (index == 1)
  {
    fmiValues[1].realValue = data;
  }
}

void ssp::SimpleAffineFMU::SetFmuValues(std::vector<int> valueReferences,
                                        std::vector<FmuValue> fmuValuesIn,
                                        VariableType dataType)
{
}
void ssp::SimpleAffineFMU::GetFmuValues(std::vector<int> valueReferences,
                                        std::vector<FmuValue> &fmuValuesOut,
                                        VariableType dataType)
{
}
