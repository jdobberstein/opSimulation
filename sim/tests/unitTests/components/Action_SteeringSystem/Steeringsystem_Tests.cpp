/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "common/steeringSignal.h"
#include "fakeAgent.h"
#include "fakeParameter.h"
#include "steeringsystem.h"

using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

TEST(SteeringSystem, CalculationOfWheelAngleWithoutSelfAligningTorque)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, const std::vector<double>> fakeParametersDoubleVector;

  std::vector<double> fakeToe;
  fakeToe.resize(2);
  fakeToe[0] = 1.0;
  fakeToe[1] = 7.2;

  std::vector<double> fakeElasticity;
  fakeElasticity.resize(2);
  fakeElasticity[0] = 30.0;
  fakeElasticity[1] = 50.0;

  std::vector<double> fakeCaster;
  fakeCaster.resize(2);
  fakeCaster[0] = 0.2;
  fakeCaster[1] = 0.2;

  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("Toe", fakeToe));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("Elasticity", fakeElasticity));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("Caster", fakeCaster));

  ON_CALL(fakeParameters, GetParametersDoubleVector()).WillByDefault(ReturnRef(fakeParametersDoubleVector));

  NiceMock<FakeAgent> fakeAgent;

  double const fakeSteeringRatio = 15.0;
  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();

  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("SteeringRatio", std::to_string(fakeSteeringRatio)));

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  ActionSteeringSystem implementation(
      "", false, 0, 0, 0, 0, nullptr, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;

  std::vector<double> const fakeLateralTireForce{0.0, 0.0, 0.0, 0.0};
  double const fakeSteeringWheelAngle = 3.0;
  const auto fakeSteeringSignal
      = std::make_shared<SteeringSignal const>(ComponentState::Acting, fakeSteeringWheelAngle * 1_rad, "UnitTest");

  const auto fakeLateralTireForceSignal = std::make_shared<SignalVectorDouble const>(fakeLateralTireForce);
  implementation.UpdateInput(0, fakeSteeringSignal, 0);
  implementation.UpdateInput(1, fakeLateralTireForceSignal, 0);
  implementation.Trigger(0);
  implementation.UpdateOutput(0, outputSignal, 0);

  std::vector<double> wheelAngle = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;

  ASSERT_EQ(wheelAngle.size(), 4);
  ASSERT_DOUBLE_EQ(wheelAngle[0], -fakeToe[0] + fakeSteeringWheelAngle / fakeSteeringRatio);
  ASSERT_DOUBLE_EQ(wheelAngle[1], fakeToe[0] + fakeSteeringWheelAngle / fakeSteeringRatio);
  ASSERT_DOUBLE_EQ(wheelAngle[2], -fakeToe[1]);
  ASSERT_DOUBLE_EQ(wheelAngle[3], fakeToe[1]);
}

TEST(SteeringSystem, CalculationOfWheelAngleWithSelfAligningTorque)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, const std::vector<double>> fakeParametersDoubleVector;

  std::vector<double> fakeToe;
  fakeToe.resize(2);
  fakeToe[0] = 1.0;
  fakeToe[1] = 7.2;

  std::vector<double> fakeElasticity;
  fakeElasticity.resize(2);
  fakeElasticity[0] = 30.0;
  fakeElasticity[1] = 50.0;

  std::vector<double> fakeCaster;
  fakeCaster.resize(2);
  fakeCaster[0] = 0.2;
  fakeCaster[1] = 0.2;

  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("Toe", fakeToe));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("Elasticity", fakeElasticity));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("Caster", fakeCaster));

  ON_CALL(fakeParameters, GetParametersDoubleVector()).WillByDefault(ReturnRef(fakeParametersDoubleVector));

  NiceMock<FakeAgent> fakeAgent;

  double const fakeSteeringRatio = 15.0;
  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();

  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("SteeringRatio", std::to_string(fakeSteeringRatio)));

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  ActionSteeringSystem implementation(
      "", false, 0, 0, 0, 0, nullptr, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  std::shared_ptr<SignalInterface const> outputSignal;

  std::vector<double> fakeLateralTireForce{1.0, 2.0, 3.0, 3.0};
  double const fakeSteeringWheelAngle = 3.0;
  const auto fakeSteeringSignal
      = std::make_shared<SteeringSignal const>(ComponentState::Acting, fakeSteeringWheelAngle * 1_rad, "UnitTest");

  const auto fakeLateralTireForceSignal = std::make_shared<SignalVectorDouble const>(fakeLateralTireForce);
  implementation.UpdateInput(0, fakeSteeringSignal, 0);
  implementation.UpdateInput(1, fakeLateralTireForceSignal, 0);
  implementation.Trigger(0);
  implementation.UpdateOutput(0, outputSignal, 0);

  std::vector<double> wheelAngle = std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value;

  ASSERT_EQ(wheelAngle.size(), 4);
  ASSERT_DOUBLE_EQ(wheelAngle[0],
                   -fakeToe[0] + fakeSteeringWheelAngle / fakeSteeringRatio
                       - fakeLateralTireForce[0] * fakeCaster[0] / fakeElasticity[0]);
  ASSERT_DOUBLE_EQ(wheelAngle[1],
                   fakeToe[0] + fakeSteeringWheelAngle / fakeSteeringRatio
                       - fakeLateralTireForce[1] * fakeCaster[0] / fakeElasticity[0]);
  ASSERT_DOUBLE_EQ(wheelAngle[2], -fakeToe[1] - fakeLateralTireForce[2] * fakeCaster[1] / fakeElasticity[1]);
  ASSERT_DOUBLE_EQ(wheelAngle[3], fakeToe[1] - fakeLateralTireForce[3] * fakeCaster[1] / fakeElasticity[1]);
}