/********************************************************************************
 * Copyright (c) 2019 AMFD GmbH
 *               2019 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>

#include "testResourceManager.h"

#include <map>
#include <new>
#include <string>
#include <vector>

#include "fakeAgent.h"
#include "fakeParameter.h"
#include "fakePublisher.h"
#include "fakeStochastics.h"
#include "unitTests/components/Algorithm_Lateral/testAlgorithmLateralImplementation.h"

class CallbackInterface;

TestResourceManager::TestResourceManager()
    : fakeStochasticsInterface(new testing::NiceMock<FakeStochastics>()),
      fakeParameters(new testing::NiceMock<FakeParameter>()),
      fakeAgent(new FakeAgent),
      fakePublisher(new testing::NiceMock<FakePublisher>)
{
  CallbackInterface* callbacks = nullptr;
  std::map<std::string, int> intParameters{{"DebugLoggingType", 0}, {"SensorModelType", 0}};
  std::string componentName = "AlgorithmLateralDriverModules";

  ON_CALL(*fakeParameters, GetParametersInt()).WillByDefault(testing::ReturnRef(intParameters));

  stubLateralDriver = std::make_unique<TestAlgorithmLateralImplementation>(componentName,
                                                                           true,
                                                                           50,
                                                                           0,
                                                                           0,
                                                                           100,
                                                                           fakeStochasticsInterface,
                                                                           fakeParameters,
                                                                           fakePublisher,
                                                                           callbacks,
                                                                           fakeAgent);
}

TestResourceManager::~TestResourceManager()
{
  if (fakeStochasticsInterface)
  {
    delete fakeStochasticsInterface;
    fakeStochasticsInterface = nullptr;
  }
  if (fakeParameters)
  {
    delete fakeParameters;
    fakeParameters = nullptr;
  }
  if (fakeAgent)
  {
    delete fakeAgent;
    fakeAgent = nullptr;
  }
  if (fakePublisher)
  {
    delete fakePublisher;
    fakePublisher = nullptr;
  }
}
