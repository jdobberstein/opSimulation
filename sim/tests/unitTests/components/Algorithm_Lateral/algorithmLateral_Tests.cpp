/********************************************************************************
 * Copyright (c) 2019 AMFD GmbH
 *               2019 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <MantleAPI/Common/orientation.h>
#include <cmath>
#include <memory>
#include <ostream>
#include <string>
#include <units.h>
#include <vector>

#include "common/globalDefinitions.h"
#include "common/lateralSignal.h"
#include "common/steeringSignal.h"
#include "include/signalInterface.h"
#include "testResourceManager.h"
#include "unitTests/components/Algorithm_Lateral/testAlgorithmLateralImplementation.h"

/*******************************************
 * CHECK Trigger                           *
 *******************************************/

/// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
struct DataFor_AlgorithmLateralDriverImplementation_Trigger
{
  units::velocity::meters_per_second_t input_LongitudinalVelocity;
  units::length::meter_t input_LateralDeviation;
  units::angle::radian_t input_HeadingError;
  units::angle::degree_t input_LastSteeringWheelAngle;
  std::vector<units::curvature::inverse_meter_t> input_CurvatureSegmentsNear;
  std::vector<units::curvature::inverse_meter_t> input_CurvatureSegmentsFar;
  units::curvature::inverse_meter_t input_KappaRoad;
  units::curvature::inverse_meter_t input_KappaManeuver;
  units::angle::degree_t result_SteeringWheelAngle;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_AlgorithmLateralDriverImplementation_Trigger& obj)
  {
    return os << "  input_LongitudinalVelocity (double): " << obj.input_LongitudinalVelocity
              << "| input_LateralDeviation (double): " << obj.input_LateralDeviation
              << "| input_HeadingError (double): " << obj.input_HeadingError
              << "| input_LastSteeringWheelAngle (double): " << obj.input_LastSteeringWheelAngle
              << "| input_KappaRoad (double): " << obj.input_KappaRoad
              << "| input_KappaManeuver (double): " << obj.input_KappaManeuver
              << "| result_SteeringWheelAngle (double): " << obj.result_SteeringWheelAngle;
  }
};

class LateralDriverTrigger : public ::testing::Test,
                             public ::testing::WithParamInterface<DataFor_AlgorithmLateralDriverImplementation_Trigger>
{
};

TEST_P(LateralDriverTrigger, LateralDriver_CheckTriggerFunction)
{
  // Get Resources for testing
  DataFor_AlgorithmLateralDriverImplementation_Trigger data = GetParam();

  TestResourceManager resourceManager;
  TestAlgorithmLateralImplementation* stubLateralDriver = resourceManager.stubLateralDriver.get();

  // Set data for test
  LateralSignal lateralSignal{ComponentState::Acting,
                              {0.0_m,
                               data.input_LateralDeviation,
                               20.0_rad_per_s_sq,
                               data.input_HeadingError,
                               10._Hz,
                               data.input_KappaManeuver},
                              data.input_KappaRoad,
                              data.input_CurvatureSegmentsNear,
                              data.input_CurvatureSegmentsFar};
  stubLateralDriver->SetLateralInput(lateralSignal);
  stubLateralDriver->SetVehicleParameter(10., units::angle::radian_t(2 * M_PI), 3.0_m);
  stubLateralDriver->SetVelocityAndSteeringWheelAngle(data.input_LongitudinalVelocity,
                                                      data.input_LastSteeringWheelAngle);

  // Call test
  stubLateralDriver->Trigger(0);
  auto result = stubLateralDriver->GetDesiredSteeringWheelAngle();

  // Results must be within 1% of analytical results (since excact matches can't be guaranteed)
  bool resultLegit = units::math::fabs(data.result_SteeringWheelAngle - result)
                  <= .01 * units::math::fabs(data.result_SteeringWheelAngle);

  // Evaluate result
  ASSERT_TRUE(resultLegit) << "SteeringWheelAngle: " << result << std::endl;
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/
INSTANTIATE_TEST_CASE_P(
    Default,
    LateralDriverTrigger,
    testing::Values(
        /*
units::velocity::meters_per_second_t                  input_LongitudinalVelocity;
units::length::meter_t                  input_LateralDeviation;
units::angle::radian_t                  input_HeadingError;
units::angle::degree_t                  input_LastSteeringWheelAngle;
std::vector<units::curvature::inverse_meter_t>     input_CurvatureSegmentsNear;
std::vector<units::curvature::inverse_meter_t>     input_CurvatureSegmentsFar;
units::curvature::inverse_meter_t                  input_KappaRoad;
units::curvature::inverse_meter_t                  input_KappaManeuver;
units::angle::degree_t                  result_SteeringWheelAngle;
*/
        DataFor_AlgorithmLateralDriverImplementation_Trigger{50._mps,
                                                             0._m,
                                                             0._rad,
                                                             0._deg,
                                                             {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
                                                             {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
                                                             0._i_m,
                                                             0._i_m,
                                                             000.000000_deg},  // Driving straight
        DataFor_AlgorithmLateralDriverImplementation_Trigger{50._mps,
                                                             1._m,
                                                             0._rad,
                                                             0._deg,
                                                             {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
                                                             {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
                                                             0._i_m,
                                                             0._i_m,
                                                             013.750987_deg},  // Lateral deviation from trajectory
        DataFor_AlgorithmLateralDriverImplementation_Trigger{
            50._mps,
            0._m,
            1._rad,
            300._deg,
            {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            0._i_m,
            0._i_m,
            332.000000_deg},  // Lateral deviation from trajectory with non central steering wheel capped to 320°/s
                              // (actual 343.77467)
        DataFor_AlgorithmLateralDriverImplementation_Trigger{
            50._mps,
            0._m,
            2._rad,
            350._deg,
            {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
            0._i_m,
            0._i_m,
            360.000000_deg},  // Curvature of trajectory, 687.54935° capped at 360°
        DataFor_AlgorithmLateralDriverImplementation_Trigger{50._mps,
                                                             2._m,
                                                             1._rad,
                                                             350._deg,
                                                             {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
                                                             {0._i_m, 0._i_m, 0._i_m, 0._i_m, 0._i_m},
                                                             0._i_m,
                                                             0._i_m,
                                                             360.000000_deg}
        // Total steering wheel angle, 371.27665° capped at 360°
        ));

/*******************************************
 * CHECK Update input                      *
 *******************************************/

/// \brief Data table for definition of individual test cases for AreaOfInterest::EGO_FRONT
struct DataFor_AlgorithmLateralDriverImplementation_UpdateInput
{
  bool input_NotifyCollistion;
  double input_LateralDeviation;
  double input_GainLateralDeviation;
  double input_HeadingError;
  double input_GainHeadingError;
  double input_KappaManeuver;
  double input_KappaRoad;
  double input_SteeringRatio;
  double input_MaximumSteeringWheelAngleAmplitude;
  double input_WheelBase;
  double input_AbsoluteVelocity;
  std::vector<double> input_CurvatureOfSegmentsToNearPoint;
  std::vector<double> input_CurvatureOfSegmentsToFarPoint;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_AlgorithmLateralDriverImplementation_UpdateInput& obj)
  {
    return os << "  input_NotifyCollistion (bool): " << obj.input_NotifyCollistion
              << "| input_LateralDeviation (double): " << obj.input_LateralDeviation
              << "| input_GainLateralDeviation (double): " << obj.input_GainLateralDeviation
              << "| input_HeadingError (double): " << obj.input_HeadingError
              << "| input_GainHeadingError (double): " << obj.input_GainHeadingError
              << "| input_KappaManeuver (double): " << obj.input_KappaManeuver
              << "| input_KappaRoad (double): " << obj.input_KappaRoad
              << "| input_SteeringRatio (double): " << obj.input_SteeringRatio
              << "| input_MaximumSteeringWheelAngleAmplitude (double): " << obj.input_MaximumSteeringWheelAngleAmplitude
              << "| input_WheelBase (double): " << obj.input_WheelBase
              << "| input_AbsoluteVelocity (double): " << obj.input_AbsoluteVelocity;
  }
};

/********************************************
 * CHECK Update output                      *
 ********************************************/

struct DataFor_AlgorithmLateralDriverImplementation_UpdateOutput
{
  units::angle::radian_t input_DesiredSteeringWheelAngle;
  bool input_IsActive;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os,
                                  const DataFor_AlgorithmLateralDriverImplementation_UpdateOutput& obj)
  {
    return os << "  input_DesiredSteeringWheelAngle (double): " << obj.input_DesiredSteeringWheelAngle;
  }
};

class LateralDriverUpdateOutput
    : public ::testing::Test,
      public ::testing::WithParamInterface<DataFor_AlgorithmLateralDriverImplementation_UpdateOutput>
{
};

TEST_P(LateralDriverUpdateOutput, LateralDriver_CheckFunction_UpdateOutput)
{
  // Get Resources for testing
  DataFor_AlgorithmLateralDriverImplementation_UpdateOutput data = GetParam();

  TestResourceManager resourceManager;
  TestAlgorithmLateralImplementation* stubLateralDriver = resourceManager.stubLateralDriver.get();

  // Create Signals
  stubLateralDriver->SetDesiredSteeringWheelAngle(data.input_DesiredSteeringWheelAngle);
  stubLateralDriver->SetIsActive(data.input_IsActive);

  int localLinkId0{0};
  int time{100};
  std::shared_ptr<SignalInterface const> signal1;

  // Call tests
  stubLateralDriver->UpdateOutput(localLinkId0, signal1, time);

  const std::shared_ptr<SteeringSignal const> signal = std::dynamic_pointer_cast<SteeringSignal const>(signal1);

  // Evaluate result
  if (data.input_IsActive)
  {
    ASSERT_EQ(signal->componentState, ComponentState::Acting);
    ASSERT_EQ(signal->steeringWheelAngle, data.input_DesiredSteeringWheelAngle);
  }
  else
  {
    ASSERT_EQ(signal->componentState, ComponentState::Disabled);
    ASSERT_EQ(signal->steeringWheelAngle, 0._rad);
  }
}

/**********************************************************
 * The test data (must be defined below test)             *
 **********************************************************/
INSTANTIATE_TEST_CASE_P(
    Default,
    LateralDriverUpdateOutput,
    testing::Values(
        /*
double input_DesiredSteeringWheelAngle;
bool input_IsActive;
*/

        DataFor_AlgorithmLateralDriverImplementation_UpdateOutput{units::angle::radian_t(0.27 * M_PI / 180.), true},
        DataFor_AlgorithmLateralDriverImplementation_UpdateOutput{units::angle::radian_t(0.74 * M_PI / 180.), false}));
