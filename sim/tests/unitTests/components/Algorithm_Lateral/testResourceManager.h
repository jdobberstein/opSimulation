/********************************************************************************
 * Copyright (c) 2019 AMFD GmbH
 *               2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <memory>

class FakeAgent;
class FakeParameter;
class FakePublisher;
class FakeStochastics;
class TestAlgorithmLateralImplementation;

class TestResourceManager
{
public:
  TestResourceManager();
  virtual ~TestResourceManager();

  std::unique_ptr<TestAlgorithmLateralImplementation> stubLateralDriver{nullptr};

private:
  FakeStochastics* fakeStochasticsInterface{nullptr};
  FakeParameter* fakeParameters{nullptr};
  FakeAgent* fakeAgent;
  FakePublisher* fakePublisher{nullptr};
};
