################################################################################
# Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME AlgorithmAFDM_Tests)
set(COMPONENT_SOURCE_DIR ${OPENPASS_SIMCORE_DIR}/components/AlgorithmAFDM/src)

add_openpass_target(
  NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT module
  DEFAULT_MAIN

  SOURCES
    Afdm_Tests.cpp
    ${COMPONENT_SOURCE_DIR}/followingDriverModel.cpp

  HEADERS
    ${COMPONENT_SOURCE_DIR}/followingDriverModel.h

  INCDIRS
    ${COMPONENT_SOURCE_DIR}
    ${OPENPASS_SIMCORE_DIR}/components/Sensor_Driver/src/Signals

  LIBRARIES
    open_simulation_interface::open_simulation_interface_shared
    MantleAPI::MantleAPI
)

