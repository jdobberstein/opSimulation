/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <MantleAPI/Common/poly_line.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Common/trajectory.h>
#include <MantleAPI/Traffic/control_strategy.h>
#include <algorithm>
#include <memory>
#include <optional>
#include <string>
#include <tuple>
#include <units.h>
#include <variant>
#include <vector>

#include "common/dynamicsSignal.h"
#include "common/globalDefinitions.h"
#include "common/vector2d.h"
#include "common/worldDefinitions.h"
#include "dynamics_scenario_implementation.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeEgoAgent.h"
#include "fakeScenarioControl.h"
#include "fakeWorld.h"

class SignalInterface;

using ::testing::_;
using ::testing::DoubleEq;
using ::testing::Ne;
using ::testing::Return;
using ::testing::ReturnRef;

TEST(DynamicsScenario_UnitsTests, KeepVelocity)
{
  FakeAgent agent;
  FakeEgoAgent egoAgent;
  ON_CALL(agent, GetEgoAgent()).WillByDefault(ReturnRef(egoAgent));
  Common::Vector2d<units::velocity::meters_per_second_t> velocity0{3.0_mps, 4.0_mps};
  ON_CALL(agent, GetVelocity(_)).WillByDefault(Return(velocity0));

  Position position{1_m, 2_m, 0.0_rad, 0.0_i_m};
  ON_CALL(egoAgent, GetWorldPosition(_, _, _)).WillByDefault(Return(position));

  FakeCallback callback;

  auto scenarioControl = std::make_shared<FakeScenarioControl>();
  auto keepVelocityControlStrategy = std::make_shared<mantle_api::KeepVelocityControlStrategy>();
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> longitudinalStrategies{keepVelocityControlStrategy};
  ON_CALL(*scenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(true));
  ON_CALL(*scenarioControl, GetStrategies(mantle_api::MovementDomain::kLongitudinal))
      .WillByDefault(Return(longitudinalStrategies));

  DynamicsScenarioImplementation dynamicsScenario(
      "", false, 0, 0, 0, 100, nullptr, nullptr, nullptr, nullptr, &callback, &agent, scenarioControl);

  dynamicsScenario.Trigger(0);

  std::shared_ptr<SignalInterface const> resultSignal;
  dynamicsScenario.UpdateOutput(0, resultSignal, 0);

  auto resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  auto resultVelocity = units::math::hypot(resultDynamicsSignal->dynamicsInformation.velocityX,
                                           resultDynamicsSignal->dynamicsInformation.velocityY);
  ASSERT_THAT(resultVelocity.value(), DoubleEq(5.0));

  Common::Vector2d<units::velocity::meters_per_second_t> velocity1{6.0_mps, 8.0_mps};
  ON_CALL(agent, GetVelocity(_)).WillByDefault(Return(velocity1));
  ON_CALL(*scenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));

  dynamicsScenario.Trigger(100);

  dynamicsScenario.UpdateOutput(0, resultSignal, 100);

  resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  resultVelocity = units::math::hypot(resultDynamicsSignal->dynamicsInformation.velocityX,
                                      resultDynamicsSignal->dynamicsInformation.velocityY);
  ASSERT_THAT(resultVelocity.value(), DoubleEq(5.0));

  ON_CALL(*scenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(true));

  dynamicsScenario.Trigger(200);

  dynamicsScenario.UpdateOutput(0, resultSignal, 200);

  resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  resultVelocity = units::math::hypot(resultDynamicsSignal->dynamicsInformation.velocityX,
                                      resultDynamicsSignal->dynamicsInformation.velocityY);
  ASSERT_THAT(resultVelocity.value(), DoubleEq(10.0));
}

TEST(DynamicsScenario_UnitsTests, FollowVelocitySpline)
{
  FakeAgent agent;
  FakeEgoAgent egoAgent;
  ON_CALL(agent, GetEgoAgent()).WillByDefault(ReturnRef(egoAgent));

  Position position{1_m, 2_m, 0.0_rad, 0.0_i_m};
  ON_CALL(egoAgent, GetWorldPosition(_, _, _)).WillByDefault(Return(position));

  FakeCallback callback;

  auto scenarioControl = std::make_shared<FakeScenarioControl>();
  auto followVelocitySplineControlStrategy = std::make_shared<mantle_api::FollowVelocitySplineControlStrategy>();
  auto& splineSection = followVelocitySplineControlStrategy->velocity_splines.emplace_back();
  splineSection.start_time = 0._s;
  splineSection.end_time = 1._s;
  splineSection.polynomial = {
      units::unit_t<
          units::compound_unit<units::velocity::meters_per_second, units::inverse<units::cubed<units::time::second>>>>{
          1.},
      units::unit_t<units::compound_unit<units::velocity::meters_per_second,
                                         units::inverse<units::squared<units::time::second>>>>{-1.5},
      units::unit_t<units::compound_unit<units::velocity::meters_per_second, units::inverse<units::time::second>>>{0.},
      units::unit_t<units::velocity::meters_per_second>{10.}};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> longitudinalStrategies{followVelocitySplineControlStrategy};
  ON_CALL(*scenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(true));
  ON_CALL(*scenarioControl, GetStrategies(mantle_api::MovementDomain::kLongitudinal))
      .WillByDefault(Return(longitudinalStrategies));

  DynamicsScenarioImplementation dynamicsScenario(
      "", false, 0, 0, 0, 500, nullptr, nullptr, nullptr, nullptr, &callback, &agent, scenarioControl);

  dynamicsScenario.Trigger(0);

  std::shared_ptr<SignalInterface const> resultSignal;
  dynamicsScenario.UpdateOutput(0, resultSignal, 0);

  auto resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  auto resultVelocity = units::math::hypot(resultDynamicsSignal->dynamicsInformation.velocityX,
                                           resultDynamicsSignal->dynamicsInformation.velocityY);
  ASSERT_THAT(resultVelocity.value(), DoubleEq(9.75));

  ON_CALL(*scenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  EXPECT_CALL(*scenarioControl, SetControlStrategyGoalReached(mantle_api::ControlStrategyType::kFollowVelocitySpline));

  dynamicsScenario.Trigger(500);

  dynamicsScenario.UpdateOutput(0, resultSignal, 500);

  resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  resultVelocity = units::math::hypot(resultDynamicsSignal->dynamicsInformation.velocityX,
                                      resultDynamicsSignal->dynamicsInformation.velocityY);
  ASSERT_THAT(resultVelocity.value(), DoubleEq(9.5));
}

TEST(DynamicsScenario_UnitsTests, KeepLaneOffset)
{
  FakeAgent agent;
  FakeEgoAgent egoAgent;
  ON_CALL(agent, GetEgoAgent()).WillByDefault(ReturnRef(egoAgent));

  FakeCallback callback;

  auto scenarioControl = std::make_shared<FakeScenarioControl>();
  auto keepLaneOffsetStrategy = std::make_shared<mantle_api::KeepLaneOffsetControlStrategy>();
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> longitudinalStrategies{keepLaneOffsetStrategy};
  ON_CALL(*scenarioControl, HasNewLateralStrategy()).WillByDefault(Return(true));
  ON_CALL(*scenarioControl, GetStrategies(mantle_api::MovementDomain::kLateral))
      .WillByDefault(Return(longitudinalStrategies));

  Position position{1_m, 2_m, 0.0_rad, 0.0_i_m};
  EXPECT_CALL(egoAgent, GetWorldPosition(_, 0._m, 0._rad)).WillOnce(Return(position));

  DynamicsScenarioImplementation dynamicsScenario(
      "", false, 0, 0, 0, 100, nullptr, nullptr, nullptr, nullptr, &callback, &agent, scenarioControl);

  dynamicsScenario.Trigger(0);

  std::shared_ptr<SignalInterface const> resultSignal;
  dynamicsScenario.UpdateOutput(0, resultSignal, 0);

  auto resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(1.0));
}

TEST(DynamicsScenario_UnitsTests, PerformLaneChange_FixedTime)
{
  FakeAgent agent;
  FakeEgoAgent egoAgent;
  ON_CALL(agent, GetEgoAgent()).WillByDefault(ReturnRef(egoAgent));
  Common::Vector2d<units::velocity::meters_per_second_t> velocity0{5.0_mps, 0.0_mps};
  ON_CALL(agent, GetVelocity(_)).WillByDefault(Return(velocity0));
  const std::string roadId{"RoadA"};
  ON_CALL(egoAgent, GetRoadId()).WillByDefault(ReturnRef(roadId));
  GlobalRoadPositions position{{roadId, {roadId, -2, 10_m, 0.5_m, 0_rad}}};
  ON_CALL(agent, GetRoadPosition(_)).WillByDefault(ReturnRef(position));

  FakeWorld world;
  ON_CALL(world, GetLaneWidth(_, _, _)).WillByDefault(Return(4.0_m));
  ON_CALL(world, RoadCoord2WorldCoord(_, _))
      .WillByDefault(
          [](RoadPosition roadCoord, [[maybe_unused]] const std::string& roadID) {
            return Position{roadCoord.s, roadCoord.t, roadCoord.hdg, 0._i_m};
          });

  FakeCallback callback;

  auto scenarioControl = std::make_shared<FakeScenarioControl>();
  auto performLaneChangeStrategy = std::make_shared<mantle_api::PerformLaneChangeControlStrategy>();
  performLaneChangeStrategy->target_lane_id = -3;
  performLaneChangeStrategy->target_lane_offset = 1.0_m;
  performLaneChangeStrategy->transition_dynamics.dimension = mantle_api::Dimension::kTime;
  performLaneChangeStrategy->transition_dynamics.shape = mantle_api::Shape::kSinusoidal;
  performLaneChangeStrategy->transition_dynamics.value = 1.0;

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> lateralStrategies{performLaneChangeStrategy};
  ON_CALL(*scenarioControl, HasNewLateralStrategy()).WillByDefault(Return(true));
  ON_CALL(*scenarioControl, GetStrategies(mantle_api::MovementDomain::kLateral))
      .WillByDefault(Return(lateralStrategies));

  DynamicsScenarioImplementation dynamicsScenario(
      "", false, 0, 0, 0, 500, nullptr, &world, nullptr, nullptr, &callback, &agent, scenarioControl);

  dynamicsScenario.Trigger(0);

  std::shared_ptr<SignalInterface const> resultSignal;
  dynamicsScenario.UpdateOutput(0, resultSignal, 0);

  auto resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(12.5));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(-7.25));

  ON_CALL(*scenarioControl, HasNewLateralStrategy()).WillByDefault(Return(false));

  EXPECT_CALL(*scenarioControl, SetControlStrategyGoalReached(mantle_api::ControlStrategyType::kPerformLaneChange));
  dynamicsScenario.Trigger(500);

  dynamicsScenario.UpdateOutput(0, resultSignal, 500);

  resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(15.0));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(-9.0));
}

TEST(DynamicsScenario_UnitsTests, PerformLaneChange_FixedDistance)
{
  FakeAgent agent;
  FakeEgoAgent egoAgent;
  ON_CALL(agent, GetEgoAgent()).WillByDefault(ReturnRef(egoAgent));
  Common::Vector2d<units::velocity::meters_per_second_t> velocity0{5.0_mps, 0.0_mps};
  ON_CALL(agent, GetVelocity(_)).WillByDefault(Return(velocity0));
  const std::string roadId{"RoadA"};
  ON_CALL(egoAgent, GetRoadId()).WillByDefault(ReturnRef(roadId));
  GlobalRoadPositions position{{roadId, {roadId, -2, 10_m, 0.5_m, 0_rad}}};
  ON_CALL(agent, GetRoadPosition(_)).WillByDefault(ReturnRef(position));

  FakeWorld world;
  ON_CALL(world, GetLaneWidth(_, _, _)).WillByDefault(Return(4.0_m));
  ON_CALL(world, RoadCoord2WorldCoord(_, _))
      .WillByDefault(
          [](RoadPosition roadCoord, [[maybe_unused]] const std::string& roadID) {
            return Position{roadCoord.s, roadCoord.t, roadCoord.hdg, 0._i_m};
          });

  FakeCallback callback;

  auto scenarioControl = std::make_shared<FakeScenarioControl>();
  auto performLaneChangeStrategy = std::make_shared<mantle_api::PerformLaneChangeControlStrategy>();
  performLaneChangeStrategy->target_lane_id = -1;
  performLaneChangeStrategy->target_lane_offset = 1.0_m;
  performLaneChangeStrategy->transition_dynamics.dimension = mantle_api::Dimension::kDistance;
  performLaneChangeStrategy->transition_dynamics.shape = mantle_api::Shape::kSinusoidal;
  performLaneChangeStrategy->transition_dynamics.value = 5.0;

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> lateralStrategies{performLaneChangeStrategy};
  ON_CALL(*scenarioControl, HasNewLateralStrategy()).WillByDefault(Return(true));
  ON_CALL(*scenarioControl, GetStrategies(mantle_api::MovementDomain::kLateral))
      .WillByDefault(Return(lateralStrategies));

  DynamicsScenarioImplementation dynamicsScenario(
      "", false, 0, 0, 0, 500, nullptr, &world, nullptr, nullptr, &callback, &agent, scenarioControl);

  dynamicsScenario.Trigger(0);

  std::shared_ptr<SignalInterface const> resultSignal;
  dynamicsScenario.UpdateOutput(0, resultSignal, 0);

  auto resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(12.5));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(-3.25));

  ON_CALL(*scenarioControl, HasNewLateralStrategy()).WillByDefault(Return(false));

  EXPECT_CALL(*scenarioControl, SetControlStrategyGoalReached(mantle_api::ControlStrategyType::kPerformLaneChange));
  dynamicsScenario.Trigger(500);

  dynamicsScenario.UpdateOutput(0, resultSignal, 500);

  resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(15.0));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(-1.0));
}

TEST(DynamicsScenario_UnitsTests, FollowTrajectoryWithTime)
{
  FakeAgent agent;
  FakeEgoAgent egoAgent;
  ON_CALL(agent, GetEgoAgent()).WillByDefault(ReturnRef(egoAgent));
  Common::Vector2d<units::velocity::meters_per_second_t> velocity0{5.0_mps, 0.0_mps};
  ON_CALL(agent, GetVelocity(_)).WillByDefault(Return(velocity0));
  const std::string roadId{"RoadA"};
  ON_CALL(egoAgent, GetRoadId()).WillByDefault(ReturnRef(roadId));
  GlobalRoadPositions position{{roadId, {roadId, -2, 10_m, 0.5_m, 0_rad}}};
  ON_CALL(agent, GetRoadPosition(_)).WillByDefault(ReturnRef(position));

  FakeCallback callback;

  auto scenarioControl = std::make_shared<FakeScenarioControl>();
  auto followTrajectoryControlStrategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
  mantle_api::PolyLine trajectory;
  mantle_api::PolyLinePoint point1;
  point1.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
  point1.time = 0.0_s;
  trajectory.push_back(point1);
  mantle_api::PolyLinePoint point2;
  point2.pose = {{1.0_m, 0.5_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
  point2.time = 0.1_s;
  trajectory.push_back(point2);
  mantle_api::PolyLinePoint point3;
  point3.pose = {{5.0_m, 2.5_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
  point3.time = 0.3_s;
  trajectory.push_back(point3);
  followTrajectoryControlStrategy->trajectory.type = trajectory;

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> strategies{followTrajectoryControlStrategy};
  ON_CALL(*scenarioControl, HasNewLateralStrategy()).WillByDefault(Return(true));
  ON_CALL(*scenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(true));
  ON_CALL(*scenarioControl, GetStrategies(mantle_api::MovementDomain::kLateral)).WillByDefault(Return(strategies));
  ON_CALL(*scenarioControl, GetStrategies(mantle_api::MovementDomain::kLongitudinal)).WillByDefault(Return(strategies));

  DynamicsScenarioImplementation dynamicsScenario(
      "", false, 0, 0, 0, 100, nullptr, nullptr, nullptr, nullptr, &callback, &agent, scenarioControl);

  dynamicsScenario.Trigger(0);

  std::shared_ptr<SignalInterface const> resultSignal;
  dynamicsScenario.UpdateOutput(0, resultSignal, 0);

  auto resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(1.0));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(0.5));

  ON_CALL(*scenarioControl, HasNewLateralStrategy()).WillByDefault(Return(false));
  ON_CALL(*scenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));

  dynamicsScenario.Trigger(100);

  dynamicsScenario.UpdateOutput(0, resultSignal, 100);

  resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(3.0));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(1.5));

  EXPECT_CALL(*scenarioControl, SetControlStrategyGoalReached(mantle_api::ControlStrategyType::kFollowTrajectory));
  dynamicsScenario.Trigger(200);

  dynamicsScenario.UpdateOutput(0, resultSignal, 200);

  resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(5.0));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(2.5));
}

TEST(DynamicsScenario_UnitsTests, FollowTrajectoryWithoutTimeAndKeepVelocity)
{
  FakeAgent agent;
  FakeEgoAgent egoAgent;
  ON_CALL(agent, GetEgoAgent()).WillByDefault(ReturnRef(egoAgent));
  Common::Vector2d<units::velocity::meters_per_second_t> velocity0{40.0_mps, 0.0_mps};
  ON_CALL(agent, GetVelocity(_)).WillByDefault(Return(velocity0));
  const std::string roadId{"RoadA"};
  ON_CALL(egoAgent, GetRoadId()).WillByDefault(ReturnRef(roadId));
  GlobalRoadPositions position{{roadId, {roadId, -2, 10_m, 0.5_m, 0_rad}}};
  ON_CALL(agent, GetRoadPosition(_)).WillByDefault(ReturnRef(position));

  FakeCallback callback;

  auto scenarioControl = std::make_shared<FakeScenarioControl>();
  auto followTrajectoryControlStrategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
  mantle_api::PolyLine trajectory;
  mantle_api::PolyLinePoint point1;
  point1.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
  point1.time = std::nullopt;
  trajectory.push_back(point1);
  mantle_api::PolyLinePoint point2;
  point2.pose = {{4.0_m, 3.0_m, 0.0_m}, {0.1_rad, 0.0_rad, 0.0_rad}};
  point2.time = std::nullopt;
  trajectory.push_back(point2);
  mantle_api::PolyLinePoint point3;
  point3.pose = {{12.0_m, -3.0_m, 0.0_m}, {0.5_rad, 0.0_rad, 0.0_rad}};
  point3.time = std::nullopt;
  trajectory.push_back(point3);
  followTrajectoryControlStrategy->trajectory.type = trajectory;

  auto keepVelocityControlStrategy = std::make_shared<mantle_api::KeepVelocityControlStrategy>();
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> longitudinalStrategies{keepVelocityControlStrategy};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> lateralStrategies{followTrajectoryControlStrategy};
  ON_CALL(*scenarioControl, HasNewLateralStrategy()).WillByDefault(Return(true));
  ON_CALL(*scenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(true));
  ON_CALL(*scenarioControl, GetStrategies(mantle_api::MovementDomain::kLongitudinal))
      .WillByDefault(Return(longitudinalStrategies));
  ON_CALL(*scenarioControl, GetStrategies(mantle_api::MovementDomain::kLateral))
      .WillByDefault(Return(lateralStrategies));

  DynamicsScenarioImplementation dynamicsScenario(
      "", false, 0, 0, 0, 100, nullptr, nullptr, nullptr, nullptr, &callback, &agent, scenarioControl);

  dynamicsScenario.Trigger(0);

  std::shared_ptr<SignalInterface const> resultSignal;
  dynamicsScenario.UpdateOutput(0, resultSignal, 0);

  auto resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(3.2));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(2.4));

  ON_CALL(*scenarioControl, HasNewLateralStrategy()).WillByDefault(Return(false));
  ON_CALL(*scenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));

  dynamicsScenario.Trigger(100);

  dynamicsScenario.UpdateOutput(0, resultSignal, 100);

  resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(6.4));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(1.2));

  dynamicsScenario.Trigger(200);

  dynamicsScenario.UpdateOutput(0, resultSignal, 200);

  resultDynamicsSignal = std::dynamic_pointer_cast<DynamicsSignal const>(resultSignal);
  ASSERT_THAT(resultDynamicsSignal, Ne(nullptr));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(9.6));
  ASSERT_THAT(resultDynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(-1.2));

  EXPECT_CALL(*scenarioControl, SetControlStrategyGoalReached(mantle_api::ControlStrategyType::kFollowTrajectory));
  dynamicsScenario.Trigger(300);
}
