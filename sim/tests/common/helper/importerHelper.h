/********************************************************************************
 * Copyright (c) 2018-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "importer/importerCommon.h"

namespace
{
static xmlNodePtr documentRootFromString(std::string content)
{
  xmlDocPtr document = xmlReadMemory(content.c_str(), content.size(), nullptr, nullptr, 0);
  ThrowIfFalse(document != nullptr, "ParameterImporter_Tests: Failed to parse the XML string.");

  return xmlDocGetRootElement(document);
}
}  //namespace
