/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/poly_line.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Common/trajectory.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/control_strategy.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <cmath>
#include <filesystem>
#include <map>
#include <memory>
#include <optional>
#include <osi3/osi_common.pb.h>
#include <osi3/osi_detectedobject.pb.h>
#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_hostvehicledata.pb.h>
#include <osi3/osi_object.pb.h>
#include <osi3/osi_sensordata.pb.h>
#include <osi3/osi_sensorview.pb.h>
#include <osi3/osi_sensorviewconfiguration.pb.h>
#include <string>
#include <units.h>
#include <utility>
#include <variant>
#include <vector>

#include "OWL/DataTypes.h"
#include "OWL/fakes/fakeMovingObject.h"
#include "OWL/fakes/fakeWorldData.h"
#include "WorldData.h"
#include "common/accelerationSignal.h"
#include "common/agentCompToCompCtrlSignal.h"
#include "common/areaOfInterest.h"
#include "common/boostGeometryCommon.h"
#include "common/driverWarning.h"
#include "common/dynamicsSignal.h"
#include "common/globalDefinitions.h"
#include "common/hypot.h"
#include "common/longitudinalSignal.h"
#include "common/runtimeInformation.h"
#include "common/sensorDataSignal.h"
#include "common/steeringSignal.h"
#include "common/stochasticDefinitions.h"
#include "common/vector2d.h"
#include "common/vector3d.h"
#include "common/worldDefinitions.h"
#include "fakeAgent.h"
#include "fakeEgoAgent.h"
#include "fakeParameter.h"
#include "fakeScenarioControl.h"
#include "fakeWorld.h"
#include "fakeWorldObject.h"
#include "fmuWrapper.h"
#include "include/parameterInterface.h"
#include "include/scenarioControlInterface.h"
#include "include/signalInterface.h"

class WorldObjectInterface;

using ::testing::_;
using ::testing::DoubleEq;
using ::testing::Eq;
using ::testing::NiceMock;
using ::testing::NotNull;
using ::testing::Return;
using ::testing::ReturnRef;

#if defined(WIN32)
const std::filesystem::path resourcesFolder{std::filesystem::current_path() / ".." / ".." / ".." / "contrib" / "fmus" / "win64"};
#elif defined(unix)
const std::filesystem::path resourcesFolder{std::filesystem::current_path() / ".." / ".." / ".." / "contrib" / "fmus" / "linux64"};
#else
#error win32 or unix has to be defined
#endif

const openpass::common::RuntimeInformation fakeRti{0, 100, {std::filesystem::current_path().string(), "", ""}};

TEST(FmuWrapper_InputTests, DummyFmu1WithoutInputs_OutputIsZero)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const auto fmuPath = resourcesFolder / "Fmi1" / "DummyFmuInputTest.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()}, {"Output_dummyFMU_Output", "AccelerationSignal_Acceleration"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};

  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(1, signal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(signal);
  ASSERT_THAT(accelerationSignal->acceleration.value(), DoubleEq(0.0));
}

TEST(FmuWrapper_InputTests, DummyFmu1WithAllInputs_OutputIsSumOfInputs)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeWorldObject> fakeFrontObject;
  NiceMock<FakeWorldObject> fakeFrontFrontObject;
  NiceMock<FakeParameter> fakeParameter;
  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};

  const auto fmuPath = resourcesFolder / "Fmi1" / "DummyFmuInputTest.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()},
      {"Output_dummyFMU_Output", "AccelerationSignal_Acceleration"},
      {"Input_v_ego", "VelocityEgo"},
      {"Input_aLongitudinal_ego", "AccelerationEgo"},
      {"Input_aLateral_ego", "CentripetalAccelerationEgo"},
      {"Input_steeringWheelAngle_ego", "SteeringWheelEgo"},
      {"Input_pedalPosition_ego", "AccelerationPedalPositionEgo"},
      {"Input_brakePedalPosition_ego", "BrakePedalPositionEgo"},
      {"Input_deltaS_ego_front", "RelativeDistanceFront"},
      {"Input_x_ego", "PositionXEgo"},
      {"Input_y_ego", "PositionYEgo"},
      {"Input_yaw_ego", "YawEgo"},
      {"Input_lane_ego", "LaneEgo"},
      {"Input_s_ego", "PositionSEgo"},
      {"Input_t_ego", "PositionTEgo"},
      {"Input_presence_front", "ExistenceFront"},
      {"Input_x_front", "PositionXFront"},
      {"Input_y_front", "PositionYFront"},
      {"Input_width_front", "WidthFront"},
      {"Input_length_front", "LengthFront"},
      {"Input_yaw_front", "YawFront"},
      {"Input_s_front", "PositionSFront"},
      {"Input_t_front", "PositionTFront"},
      {"Input_v_front", "VelocityFront"},
      {"Input_lane_front", "LaneFront"},
      {"Input_presence_frontFront", "ExistenceFrontFront"},
      {"Input_x_frontFront", "PositionXFrontFront"},
      {"Input_y_frontFront", "PositionYFrontFront"},
      {"Input_v_frontFront", "VelocityFrontFront"},
      {"Input_lane_frontFront", "LaneFrontFront"},
      {"Input_deltaS_frontFront", "RelativeDistanceFrontFront"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));

  const std::string roadId{"Road"};
  std::vector<const WorldObjectInterface*> objectsInRange{&fakeFrontObject, &fakeFrontFrontObject};
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeEgoAgent, HasValidRoute()).WillByDefault(Return(true));
  ON_CALL(fakeEgoAgent, GetObjectsInRange(_, _, _)).WillByDefault(Return(objectsInRange));
  EXPECT_CALL(fakeAgent, GetVelocity(_)).WillOnce(Return(Common::Vector2d{1.0_mps, 0.0_mps}));
  EXPECT_CALL(fakeAgent, GetAcceleration(_))
      .WillOnce(
          Return(Common::Vector2d{2.0_mps_sq * units::math::cos(9.0_rad), 2.0_mps_sq * units::math::sin(9.0_rad)}));
  EXPECT_CALL(fakeAgent, GetCentripetalAcceleration()).WillOnce(Return(3.0_mps_sq));
  EXPECT_CALL(fakeAgent, GetSteeringWheelAngle()).WillOnce(Return(4.0_rad));
  EXPECT_CALL(fakeAgent, GetEffAccelPedal()).WillOnce(Return(5.0));
  EXPECT_CALL(fakeAgent, GetEffBrakePedal()).WillOnce(Return(6.0));
  EXPECT_CALL(fakeAgent, GetPositionX()).WillOnce(Return(7.0_m));
  EXPECT_CALL(fakeAgent, GetPositionY()).WillOnce(Return(8.0_m));
  EXPECT_CALL(fakeAgent, GetYaw()).WillRepeatedly(Return(9.0_rad));
  ON_CALL(fakeEgoAgent, GetRoadId()).WillByDefault(ReturnRef(roadId));
  auto mainLocatePosition = std::make_optional<GlobalRoadPosition>(roadId, 10, 11.0_m, 12.0_m, 9.0_rad);
  ON_CALL(fakeEgoAgent, GetMainLocatePosition()).WillByDefault(ReturnRef(mainLocatePosition));
  GlobalRoadPosition referencePosition{roadId, 10, 11.0_m, 12.0_m, 9.0_rad};
  ON_CALL(fakeEgoAgent, GetReferencePointPosition()).WillByDefault(Return(referencePosition));
  EXPECT_CALL(fakeEgoAgent, GetNetDistance(&fakeFrontObject)).WillOnce(Return(13.0_m));
  EXPECT_CALL(fakeEgoAgent, GetNetDistance(&fakeFrontFrontObject)).WillOnce(Return(14.0_m));
  EXPECT_CALL(fakeFrontObject, GetPositionX()).WillOnce(Return(15.0_m));
  EXPECT_CALL(fakeFrontObject, GetPositionY()).WillOnce(Return(16.0_m));
  EXPECT_CALL(fakeFrontObject, GetYaw()).WillOnce(Return(17.0_rad));
  EXPECT_CALL(fakeFrontObject, GetWidth()).WillOnce(Return(18.0_m));
  EXPECT_CALL(fakeFrontObject, GetLength()).WillOnce(Return(19.0_m));
  EXPECT_CALL(fakeFrontObject, GetVelocity(_)).WillOnce(Return(Common::Vector2d{20.0_mps, 0.0_mps}));
  GlobalRoadPositions frontPosition{{roadId, {roadId, 21, 22.0_m, 23.0_m, 17.0_rad}}};
  ON_CALL(fakeFrontObject, GetRoadPosition(_)).WillByDefault(ReturnRef(frontPosition));
  EXPECT_CALL(fakeFrontFrontObject, GetPositionX()).WillOnce(Return(24.0_m));
  EXPECT_CALL(fakeFrontFrontObject, GetPositionY()).WillOnce(Return(25.0_m));
  EXPECT_CALL(fakeFrontFrontObject, GetVelocity(_)).WillOnce(Return(Common::Vector2d{26.0_mps, 0.0_mps}));
  GlobalRoadPositions frontfrontPosition{{roadId, {roadId, 27, 1.0_m, 1.0_m, 1.0_rad}}};
  ON_CALL(fakeFrontFrontObject, GetRoadPosition(_)).WillByDefault(ReturnRef(frontfrontPosition));
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(1, signal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(signal);
  ASSERT_THAT(accelerationSignal->acceleration.value(), DoubleEq(380.0));
}

TEST(FmuWrapper_InputTests, DummyFmu1WithSensorFusionInput_OutputIsSumOfInputs)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  NiceMock<FakeAgent> agent0;
  NiceMock<FakeAgent> agent1;
  NiceMock<FakeAgent> agent2;
  NiceMock<FakeAgent> agent3;
  NiceMock<FakeAgent> agent4;
  NiceMock<FakeAgent> agent5;
  NiceMock<FakeAgent> agent6;
  NiceMock<FakeWorldObject> object0;
  NiceMock<FakeWorldObject> object1;
  NiceMock<FakeWorldObject> object2;

  const auto fmuPath = resourcesFolder / "Fmi1" / "DummyFmuObjectsList.fmu";
  std::map<std::string, const std::string> stringParameter{{"FmuPath", fmuPath.string()},
                                                           {"Output_Output_Sum", "AccelerationSignal_Acceleration"}};
  for (int i = 0; i <= 9; ++i)
  {
    auto numberStringFmu = (i > 0) ? std::to_string(i) : "10";  // Inputs in DummyFMU are numbered from 1 to 10
    auto numberString = std::to_string(i);
    stringParameter.insert({"Input_SensorFusionObjectId_" + numberStringFmu, "SensorFusionObjectId_" + numberString});
    stringParameter.insert({"Input_SensorFusionNumberOfDetectingSensors_" + numberStringFmu,
                            "SensorFusionNumberOfDetectingSensors_" + numberString});
    stringParameter.insert({"Input_SensorFusionRelativeS_" + numberStringFmu, "SensorFusionRelativeS_" + numberString});
    stringParameter.insert(
        {"Input_SensorFusionRelativeNetS_" + numberStringFmu, "SensorFusionRelativeNetS_" + numberString});
    stringParameter.insert({"Input_SensorFusionRelativeT_" + numberStringFmu, "SensorFusionRelativeT_" + numberString});
    stringParameter.insert({"Input_SensorFusionRelativeX_" + numberStringFmu, "SensorFusionRelativeX_" + numberString});
    stringParameter.insert({"Input_SensorFusionRelativeY_" + numberStringFmu, "SensorFusionRelativeY_" + numberString});
    stringParameter.insert(
        {"Input_SensorFusionRelativeNetLeft_" + numberStringFmu, "SensorFusionRelativeNetLeft_" + numberString});
    stringParameter.insert(
        {"Input_SensorFusionRelativeNetRight_" + numberStringFmu, "SensorFusionRelativeNetRight_" + numberString});
    stringParameter.insert(
        {"Input_SensorFusionRelativeNetX_" + numberStringFmu, "SensorFusionRelativeNetX_" + numberString});
    stringParameter.insert(
        {"Input_SensorFusionRelativeNetY_" + numberStringFmu, "SensorFusionRelativeNetY_" + numberString});
    stringParameter.insert({"Input_SensorFusionLane_" + numberStringFmu, "SensorFusionLane_" + numberString});
    stringParameter.insert({"Input_SensorFusionVelocity_" + numberStringFmu, "SensorFusionVelocity_" + numberString});
    stringParameter.insert({"Input_SensorFusionVelocityX_" + numberStringFmu, "SensorFusionVelocityX_" + numberString});
    stringParameter.insert({"Input_SensorFusionVelocityY_" + numberStringFmu, "SensorFusionVelocityY_" + numberString});
    stringParameter.insert({"Input_SensorFusionYaw_" + numberStringFmu, "SensorFusionYaw_" + numberString});
  }
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{{"VelocityWish", 10.0}};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, const std::string> inputId{{"InputId", "Camera"}};

  auto sensorLink = std::make_shared<NiceMock<FakeParameter>>();
  ON_CALL(*sensorLink, GetParametersString()).WillByDefault(ReturnRef(inputId));
  std::map<std::string, int> sensorId{{"SensorId", 7}};
  ON_CALL(*sensorLink, GetParametersInt()).WillByDefault(ReturnRef(sensorId));
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {sensorLink}}};
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(agent0, GetId()).WillByDefault(Return(0));
  ON_CALL(agent1, GetId()).WillByDefault(Return(1));
  ON_CALL(agent2, GetId()).WillByDefault(Return(2));
  ON_CALL(agent3, GetId()).WillByDefault(Return(3));
  ON_CALL(agent4, GetId()).WillByDefault(Return(4));
  ON_CALL(agent5, GetId()).WillByDefault(Return(5));
  ON_CALL(agent6, GetId()).WillByDefault(Return(6));
  ON_CALL(object0, GetId()).WillByDefault(Return(7));
  ON_CALL(object1, GetId()).WillByDefault(Return(8));
  ON_CALL(object2, GetId()).WillByDefault(Return(9));
  ON_CALL(fakeEgoAgent, GetDistanceToObject(&agent0, _, _)).WillByDefault(Return(1.5_m));
  ON_CALL(fakeEgoAgent, GetDistanceToObject(&agent1, _, _)).WillByDefault(Return(2.5_m));
  ON_CALL(fakeEgoAgent, GetDistanceToObject(&agent2, _, _)).WillByDefault(Return(3.5_m));
  ON_CALL(fakeEgoAgent, GetDistanceToObject(&agent3, _, _)).WillByDefault(Return(4.5_m));
  ON_CALL(fakeEgoAgent, GetDistanceToObject(&agent4, _, _)).WillByDefault(Return(5.5_m));
  ON_CALL(fakeEgoAgent, GetDistanceToObject(&agent5, _, _)).WillByDefault(Return(6.5_m));
  ON_CALL(fakeEgoAgent, GetDistanceToObject(&agent6, _, _)).WillByDefault(Return(7.5_m));
  ON_CALL(fakeEgoAgent, GetDistanceToObject(&object0, _, _)).WillByDefault(Return(8.5_m));
  ON_CALL(fakeEgoAgent, GetDistanceToObject(&object1, _, _)).WillByDefault(Return(9.5_m));
  ON_CALL(fakeEgoAgent, GetDistanceToObject(&object2, _, _)).WillByDefault(Return(10.50_m));
  ON_CALL(fakeEgoAgent, GetNetDistance(&agent0)).WillByDefault(Return(1_m));
  ON_CALL(fakeEgoAgent, GetNetDistance(&agent1)).WillByDefault(Return(2_m));
  ON_CALL(fakeEgoAgent, GetNetDistance(&agent2)).WillByDefault(Return(3_m));
  ON_CALL(fakeEgoAgent, GetNetDistance(&agent3)).WillByDefault(Return(4_m));
  ON_CALL(fakeEgoAgent, GetNetDistance(&agent4)).WillByDefault(Return(5_m));
  ON_CALL(fakeEgoAgent, GetNetDistance(&agent5)).WillByDefault(Return(6_m));
  ON_CALL(fakeEgoAgent, GetNetDistance(&agent6)).WillByDefault(Return(7_m));
  ON_CALL(fakeEgoAgent, GetNetDistance(&object0)).WillByDefault(Return(8_m));
  ON_CALL(fakeEgoAgent, GetNetDistance(&object1)).WillByDefault(Return(9_m));
  ON_CALL(fakeEgoAgent, GetNetDistance(&object2)).WillByDefault(Return(10_m));
  ON_CALL(fakeEgoAgent, GetObstruction(&agent0, _))
      .WillByDefault(Return(Obstruction{{{ObjectPointRelative::Leftmost, 1_m},
                                         {ObjectPointRelative::Rightmost, 1_m},
                                         {ObjectPointPredefined::FrontCenter, 10.0_m}}}));
  ON_CALL(fakeEgoAgent, GetObstruction(&agent1, _))
      .WillByDefault(Return(Obstruction{{{ObjectPointRelative::Leftmost, 1_m},
                                         {ObjectPointRelative::Rightmost, 1_m},
                                         {ObjectPointPredefined::FrontCenter, 11.0_m}}}));
  ON_CALL(fakeEgoAgent, GetObstruction(&agent2, _))
      .WillByDefault(Return(Obstruction{{{ObjectPointRelative::Leftmost, 1_m},
                                         {ObjectPointRelative::Rightmost, 1_m},
                                         {ObjectPointPredefined::FrontCenter, 12.0_m}}}));
  ON_CALL(fakeEgoAgent, GetObstruction(&agent3, _))
      .WillByDefault(Return(Obstruction{{{ObjectPointRelative::Leftmost, 1_m},
                                         {ObjectPointRelative::Rightmost, 1_m},
                                         {ObjectPointPredefined::FrontCenter, 13.0_m}}}));
  ON_CALL(fakeEgoAgent, GetObstruction(&agent4, _))
      .WillByDefault(Return(Obstruction{{{ObjectPointRelative::Leftmost, 1_m},
                                         {ObjectPointRelative::Rightmost, 1_m},
                                         {ObjectPointPredefined::FrontCenter, 14.0_m}}}));
  ON_CALL(fakeEgoAgent, GetObstruction(&agent5, _))
      .WillByDefault(Return(Obstruction{{{ObjectPointRelative::Leftmost, 1_m},
                                         {ObjectPointRelative::Rightmost, 1_m},
                                         {ObjectPointPredefined::FrontCenter, 15.0_m}}}));
  ON_CALL(fakeEgoAgent, GetObstruction(&agent6, _))
      .WillByDefault(Return(Obstruction{{{ObjectPointRelative::Leftmost, 1_m},
                                         {ObjectPointRelative::Rightmost, 1_m},
                                         {ObjectPointPredefined::FrontCenter, 16.0_m}}}));
  ON_CALL(fakeEgoAgent, GetObstruction(&object0, _))
      .WillByDefault(Return(Obstruction{{{ObjectPointRelative::Leftmost, 1_m},
                                         {ObjectPointRelative::Rightmost, 1_m},
                                         {ObjectPointPredefined::FrontCenter, 17.0_m}}}));
  ON_CALL(fakeEgoAgent, GetObstruction(&object1, _))
      .WillByDefault(Return(Obstruction{{{ObjectPointRelative::Leftmost, 1_m},
                                         {ObjectPointRelative::Rightmost, 1_m},
                                         {ObjectPointPredefined::FrontCenter, 18.0_m}}}));
  ON_CALL(fakeEgoAgent, GetObstruction(&object2, _))
      .WillByDefault(Return(Obstruction{{{ObjectPointRelative::Leftmost, 1_m},
                                         {ObjectPointRelative::Rightmost, 1_m},
                                         {ObjectPointPredefined::FrontCenter, 19.0_m}}}));
  GlobalRoadPositions position0{{"Road", GlobalRoadPosition{"Road", 1, 0_m, 0_m, 0_rad}}};
  GlobalRoadPositions position1{{"Road", GlobalRoadPosition{"Road", 2, 0_m, 0_m, 0_rad}}};
  GlobalRoadPositions position2{{"Road", GlobalRoadPosition{"Road", 3, 0_m, 0_m, 0_rad}}};
  GlobalRoadPositions position3{{"Road", GlobalRoadPosition{"Road", 4, 0_m, 0_m, 0_rad}}};
  GlobalRoadPositions position4{{"Road", GlobalRoadPosition{"Road", 5, 0_m, 0_m, 0_rad}}};
  GlobalRoadPositions position5{{"Road", GlobalRoadPosition{"Road", 6, 0_m, 0_m, 0_rad}}};
  GlobalRoadPositions position6{{"Road", GlobalRoadPosition{"Road", 7, 0_m, 0_m, 0_rad}}};
  GlobalRoadPositions position7{{"Road", GlobalRoadPosition{"Road", 8, 0_m, 0_m, 0_rad}}};
  GlobalRoadPositions position8{{"Road", GlobalRoadPosition{"Road", 9, 0_m, 0_m, 0_rad}}};
  GlobalRoadPositions position9{{"Road", GlobalRoadPosition{"Road", 10, 0_m, 0_m, 0_rad}}};
  ON_CALL(agent0, GetRoadPosition(_)).WillByDefault(ReturnRef(position0));
  ON_CALL(agent1, GetRoadPosition(_)).WillByDefault(ReturnRef(position1));
  ON_CALL(agent2, GetRoadPosition(_)).WillByDefault(ReturnRef(position2));
  ON_CALL(agent3, GetRoadPosition(_)).WillByDefault(ReturnRef(position3));
  ON_CALL(agent4, GetRoadPosition(_)).WillByDefault(ReturnRef(position4));
  ON_CALL(agent5, GetRoadPosition(_)).WillByDefault(ReturnRef(position5));
  ON_CALL(agent6, GetRoadPosition(_)).WillByDefault(ReturnRef(position6));
  ON_CALL(object0, GetRoadPosition(_)).WillByDefault(ReturnRef(position7));
  ON_CALL(object1, GetRoadPosition(_)).WillByDefault(ReturnRef(position8));
  ON_CALL(object2, GetRoadPosition(_)).WillByDefault(ReturnRef(position9));
  ON_CALL(agent0, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{20_mps, 0.0_mps}));
  ON_CALL(agent1, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{21_mps, 0.0_mps}));
  ON_CALL(agent2, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{22_mps, 0.0_mps}));
  ON_CALL(agent3, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{23_mps, 0.0_mps}));
  ON_CALL(agent4, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{24_mps, 0.0_mps}));
  ON_CALL(agent5, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{25_mps, 0.0_mps}));
  ON_CALL(agent6, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{26_mps, 0.0_mps}));
  ON_CALL(object0, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{27_mps, 0.0_mps}));
  ON_CALL(object1, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{28_mps, 0.0_mps}));
  ON_CALL(object2, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{29_mps, 0.0_mps}));
  polygon_t boundingBox;
  boundingBox.outer().push_back(point_t{0, 0});
  boundingBox.outer().push_back(point_t{1, 0});
  boundingBox.outer().push_back(point_t{1, 1});
  boundingBox.outer().push_back(point_t{0, 1});
  ON_CALL(fakeAgent, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(agent0, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(agent1, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(agent2, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(agent3, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(agent4, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(agent5, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(agent6, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(object0, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(object1, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(object2, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(fakeAgent, GetPositionX()).WillByDefault(Return(100.0_m));
  ON_CALL(agent0, GetPositionX()).WillByDefault(Return(101.0_m));
  ON_CALL(agent1, GetPositionX()).WillByDefault(Return(102.0_m));
  ON_CALL(agent2, GetPositionX()).WillByDefault(Return(103.0_m));
  ON_CALL(agent3, GetPositionX()).WillByDefault(Return(104.0_m));
  ON_CALL(agent4, GetPositionX()).WillByDefault(Return(105.0_m));
  ON_CALL(agent5, GetPositionX()).WillByDefault(Return(106.0_m));
  ON_CALL(agent6, GetPositionX()).WillByDefault(Return(107.0_m));
  ON_CALL(object0, GetPositionX()).WillByDefault(Return(108.0_m));
  ON_CALL(object1, GetPositionX()).WillByDefault(Return(109.0_m));
  ON_CALL(object2, GetPositionX()).WillByDefault(Return(110.0_m));
  ON_CALL(fakeAgent, GetPositionY()).WillByDefault(Return(200.0_m));
  ON_CALL(agent0, GetPositionY()).WillByDefault(Return(201.0_m));
  ON_CALL(agent1, GetPositionY()).WillByDefault(Return(202.0_m));
  ON_CALL(agent2, GetPositionY()).WillByDefault(Return(203.0_m));
  ON_CALL(agent3, GetPositionY()).WillByDefault(Return(204.0_m));
  ON_CALL(agent4, GetPositionY()).WillByDefault(Return(205.0_m));
  ON_CALL(agent5, GetPositionY()).WillByDefault(Return(206.0_m));
  ON_CALL(agent6, GetPositionY()).WillByDefault(Return(207.0_m));
  ON_CALL(object0, GetPositionY()).WillByDefault(Return(208.0_m));
  ON_CALL(object1, GetPositionY()).WillByDefault(Return(209.0_m));
  ON_CALL(object2, GetPositionY()).WillByDefault(Return(210.0_m));

  NiceMock<OWL::Fakes::WorldData> fakeWorldData;
  NiceMock<OWL::Fakes::StationaryObject> stationary0;
  NiceMock<OWL::Fakes::StationaryObject> stationary1;
  NiceMock<OWL::Fakes::StationaryObject> stationary2;
  stationary0.SetLinkedObjectForTesting(&object0);
  stationary1.SetLinkedObjectForTesting(&object1);
  stationary2.SetLinkedObjectForTesting(&object2);
  ON_CALL(fakeWorldData, GetStationaryObject(7)).WillByDefault(ReturnRef(stationary0));
  ON_CALL(fakeWorldData, GetStationaryObject(8)).WillByDefault(ReturnRef(stationary1));
  ON_CALL(fakeWorldData, GetStationaryObject(9)).WillByDefault(ReturnRef(stationary2));

  NiceMock<FakeWorld> fakeWorld;
  ON_CALL(fakeWorld, GetWorldData()).WillByDefault(Return(&fakeWorldData));
  ON_CALL(fakeWorld, GetAgent(0)).WillByDefault(Return(&agent0));
  ON_CALL(fakeWorld, GetAgent(1)).WillByDefault(Return(&agent1));
  ON_CALL(fakeWorld, GetAgent(2)).WillByDefault(Return(&agent2));
  ON_CALL(fakeWorld, GetAgent(3)).WillByDefault(Return(&agent3));
  ON_CALL(fakeWorld, GetAgent(4)).WillByDefault(Return(&agent4));
  ON_CALL(fakeWorld, GetAgent(5)).WillByDefault(Return(&agent5));
  ON_CALL(fakeWorld, GetAgent(6)).WillByDefault(Return(&agent6));

  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  osi3::SensorData sd;

  for (unsigned int i = 0; i < 7; ++i)
  {
    auto* object = sd.add_moving_object();
    object->mutable_header()->add_ground_truth_id()->set_value(i);
    for (int k = 0; k <= i; ++k)
    {
      object->mutable_header()->add_sensor_id()->set_value(100 + k);
    }
  }
  for (unsigned int i = 7; i < 10; ++i)
  {
    auto* object = sd.add_stationary_object();
    object->mutable_header()->add_ground_truth_id()->set_value(i);
    for (int k = 7; k <= i; ++k)
    {
      object->mutable_header()->add_sensor_id()->set_value(100 + k);
    }
  }

  fmuWrapper->UpdateInput(2, std::make_shared<SensorDataSignal>(sd), 0);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(1, signal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(signal);
  ASSERT_THAT(accelerationSignal->acceleration.value(), DoubleEq(1014.0));
}

TEST(FmuWrapper_InputTests, DummyFmu1WithParametersBool_OutputIsInput)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi1" / "DummyFmuParameter.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()}, {"Output_Output_ParameterSum", "AccelerationSignal_Acceleration"}};
  std::map<std::string, bool> boolParameter{
      {"Logging", true}, {"CsvOutput", true}, {"Parameter_Input_Parameter_boolean.Value", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(1, signal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(signal);
  ASSERT_THAT(accelerationSignal->acceleration.value(), DoubleEq(1));
}

TEST(FmuWrapper_InputTests, DummyFmu1WithParametersReal_OutputIsInput)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi1" / "DummyFmuParameter.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()}, {"Output_Output_ParameterSum", "AccelerationSignal_Acceleration"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{{"Parameter_Input_Parameter_integer_1.Value", 10},
                                          {"Parameter_Input_Parameter_integer_2.Value", 20}};
  std::map<std::string, double> doubleParameter{{"Parameter_Input_Parameter_double_1.Value", 1.23},
                                                {"Parameter_Input_Parameter_double_2.Value", 12.3}};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(1, signal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(signal);
  ASSERT_THAT(accelerationSignal->acceleration.value(), DoubleEq(43.53));
}

TEST(FmuWrapper_InputTests, DummyFmu1WithParametersString_OutputIsInput)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi1" / "DummyFmuParameter.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()},
      {"Parameter_Input_Parameter_string", "TestString"},
      {"Output_Output_ParameterSum", "AccelerationSignal_Acceleration"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(1, signal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(signal);
  ASSERT_THAT(accelerationSignal->acceleration.value(), DoubleEq(10));
}

TEST(FmuWrapper_OutputTests, DummyFmu1WithDynamicsSignalOutput_OutputIsAsExpected)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi1" / "DummyFmuOutputTest.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()},
      {"Output_output_1", "ComponentState"},
      {"Output_output_2", "DynamicsSignal_Acceleration"},
      {"Output_output_3", "DynamicsSignal_Velocity"},
      {"Output_output_4", "DynamicsSignal_PositionX"},
      {"Output_output_5", "DynamicsSignal_PositionY"},
      {"Output_output_6", "DynamicsSignal_Yaw"},
      {"Output_output_7", "DynamicsSignal_YawRate"},
      {"Output_output_8", "DynamicsSignal_YawAcceleration"},
      {"Output_output_9", "DynamicsSignal_SteeringWheelAngle"},
      {"Output_output_10", "DynamicsSignal_CentripetalAcceleration"},
      {"Output_output_11", "DynamicsSignal_TravelDistance"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(0, signal, 0);

  auto dynamicsSignal = std::dynamic_pointer_cast<const DynamicsSignal>(signal);
  EXPECT_THAT(dynamicsSignal->componentState, Eq(ComponentState::Acting));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.acceleration.value(), DoubleEq(2.0));
  EXPECT_THAT(openpass::hypot(dynamicsSignal->dynamicsInformation.velocityX.value(),
                              dynamicsSignal->dynamicsInformation.velocityY.value()),
              DoubleEq(3.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(4.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(5.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.yaw.value(), DoubleEq(6.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.yawRate.value(), DoubleEq(7.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.yawAcceleration.value(), DoubleEq(8.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.steeringWheelAngle.value(), DoubleEq(9.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.centripetalAcceleration.value(), DoubleEq(10.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.travelDistance.value(), DoubleEq(11.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.travelDistance.value(), DoubleEq(11.0));
}

TEST(FmuWrapper_OutputTests, DummyFmu1WithLongitudinalAndSteeringSignalOutput_OutputIsAsExpected)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi1" / "DummyFmuOutputTest.fmu";
  std::map<std::string, const std::string> stringParameter{{"FmuPath", fmuPath.string()},
                                                           {"Output_output_1", "ComponentState"},
                                                           {"Output_output_2", "LongitudinalSignal_AccPedalPos"},
                                                           {"Output_output_3", "LongitudinalSignal_BrakePedalPos"},
                                                           {"Output_output_12", "LongitudinalSignal_Gear"},
                                                           {"Output_output_4", "SteeringSignal_SteeringWheelAngle"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal1;
  fmuWrapper->UpdateOutput(2, signal1, 0);
  auto longitudinalSignal = std::dynamic_pointer_cast<const LongitudinalSignal>(signal1);
  std::shared_ptr<const SignalInterface> signal2;
  fmuWrapper->UpdateOutput(3, signal2, 0);
  auto steeringSignal = std::dynamic_pointer_cast<const SteeringSignal>(signal2);

  EXPECT_THAT(longitudinalSignal->componentState, Eq(ComponentState::Acting));
  EXPECT_THAT(longitudinalSignal->accPedalPos, DoubleEq(2.0));
  EXPECT_THAT(longitudinalSignal->brakePedalPos, DoubleEq(3.0));
  EXPECT_THAT(longitudinalSignal->gear, Eq(12));
  EXPECT_THAT(steeringSignal->componentState, Eq(ComponentState::Acting));
  EXPECT_THAT(steeringSignal->steeringWheelAngle.value(), DoubleEq(4.0));
}

TEST(FmuWrapper_OutputTests, DummyFmu1WithCompCtrlSignalOutput_OutputIsAsExpected)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi1" / "DummyFmuComCtrlsSignal.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()},
      {"Output_Output_MovementDomain", "CompCtrlSignal_MovementDomain"},
      {"Output_Output_WarningActivity", "CompCtrlSignal_WarningActivity"},
      {"Output_Output_WarningLevel", "CompCtrlSignal_WarningLevel"},
      {"Output_Output_WarningType", "CompCtrlSignal_WarningType"},
      {"Output_Output_WarningIntensity", "CompCtrlSignal_WarningIntensity"},
      {"Output_Output_WarningDirection", "CompCtrlSignal_WarningDirection"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        1000,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal0;
  fmuWrapper->UpdateOutput(4, signal0, 0);
  auto compCtrlSignal0 = std::dynamic_pointer_cast<const VehicleCompToCompCtrlSignal>(signal0);
  EXPECT_THAT(compCtrlSignal0->GetCurrentState(), Eq(ComponentState::Acting));
  EXPECT_THAT(compCtrlSignal0->GetMovementDomain(), Eq(MovementDomain::Undefined));
  auto warnings0 = compCtrlSignal0->GetComponentWarnings();
  EXPECT_THAT(warnings0.front().activity, Eq(false));
  EXPECT_THAT(warnings0.front().level, Eq(ComponentWarningLevel::INFO));
  EXPECT_THAT(warnings0.front().type, Eq(ComponentWarningType::OPTIC));
  EXPECT_THAT(warnings0.front().intensity, Eq(ComponentWarningIntensity::LOW));
  EXPECT_THAT(warnings0.front().direction, Eq(AreaOfInterest::EGO_FRONT));

  fmuWrapper->Trigger(1000);
  std::shared_ptr<const SignalInterface> signal1;
  fmuWrapper->UpdateOutput(4, signal1, 0);
  auto compCtrlSignal1 = std::dynamic_pointer_cast<const VehicleCompToCompCtrlSignal>(signal1);
  EXPECT_THAT(compCtrlSignal1->GetCurrentState(), Eq(ComponentState::Acting));
  EXPECT_THAT(compCtrlSignal1->GetMovementDomain(), Eq(MovementDomain::Lateral));
  auto warnings1 = compCtrlSignal1->GetComponentWarnings();
  EXPECT_THAT(warnings1.front().activity, Eq(true));
  EXPECT_THAT(warnings1.front().level, Eq(ComponentWarningLevel::WARNING));
  EXPECT_THAT(warnings1.front().type, Eq(ComponentWarningType::ACOUSTIC));
  EXPECT_THAT(warnings1.front().intensity, Eq(ComponentWarningIntensity::MEDIUM));
  EXPECT_THAT(warnings1.front().direction, Eq(AreaOfInterest::HUD));

  fmuWrapper->Trigger(2000);
  std::shared_ptr<const SignalInterface> signal2;
  fmuWrapper->UpdateOutput(4, signal2, 0);
  auto compCtrlSignal2 = std::dynamic_pointer_cast<const VehicleCompToCompCtrlSignal>(signal2);
  EXPECT_THAT(compCtrlSignal2->GetCurrentState(), Eq(ComponentState::Acting));
  EXPECT_THAT(compCtrlSignal2->GetMovementDomain(), Eq(MovementDomain::Longitudinal));
  auto warnings2 = compCtrlSignal2->GetComponentWarnings();
  EXPECT_THAT(warnings2.front().activity, Eq(true));
  EXPECT_THAT(warnings2.front().level, Eq(ComponentWarningLevel::WARNING));
  EXPECT_THAT(warnings2.front().type, Eq(ComponentWarningType::HAPTIC));
  EXPECT_THAT(warnings2.front().intensity, Eq(ComponentWarningIntensity::HIGH));
  EXPECT_THAT(warnings2.front().direction, Eq(AreaOfInterest::HUD));

  fmuWrapper->Trigger(3000);
  std::shared_ptr<const SignalInterface> signal3;
  fmuWrapper->UpdateOutput(4, signal3, 0);
  auto compCtrlSignal3 = std::dynamic_pointer_cast<const VehicleCompToCompCtrlSignal>(signal3);
  EXPECT_THAT(compCtrlSignal3->GetCurrentState(), Eq(ComponentState::Acting));
  EXPECT_THAT(compCtrlSignal3->GetMovementDomain(), Eq(MovementDomain::Both));
  auto warnings3 = compCtrlSignal3->GetComponentWarnings();
  EXPECT_THAT(warnings3.front().activity, Eq(true));
  EXPECT_THAT(warnings3.front().level, Eq(ComponentWarningLevel::WARNING));
  EXPECT_THAT(warnings3.front().type, Eq(ComponentWarningType::HAPTIC));
  EXPECT_THAT(warnings3.front().intensity, Eq(ComponentWarningIntensity::HIGH));
  EXPECT_THAT(warnings3.front().direction, Eq(AreaOfInterest::HUD));
}

TEST(FmuWrapper_InputTests, DummyFmu2WithoutInputs_OutputIsZero)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi2" / "DummyFmuInputTest.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()}, {"Output_dummyFMU_Output", "AccelerationSignal_Acceleration"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(1, signal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(signal);
  ASSERT_THAT(accelerationSignal->acceleration.value(), DoubleEq(0.0));
}

TEST(FmuWrapper_InputTests, DummyFmu2WithAllInputs_OutputIsSumOfInputs)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeWorldObject> fakeFrontObject;
  NiceMock<FakeWorldObject> fakeFrontFrontObject;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi2" / "DummyFmuInputTest.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()},
      {"Output_dummyFMU_Output", "AccelerationSignal_Acceleration"},
      {"Input_v_ego", "VelocityEgo"},
      {"Input_aLongitudinal_ego", "AccelerationEgo"},
      {"Input_aLateral_ego", "CentripetalAccelerationEgo"},
      {"Input_steeringWheelAngle_ego", "SteeringWheelEgo"},
      {"Input_pedalPosition_ego", "AccelerationPedalPositionEgo"},
      {"Input_brakePedalPosition_ego", "BrakePedalPositionEgo"},
      {"Input_deltaS_ego_front", "RelativeDistanceFront"},
      {"Input_x_ego", "PositionXEgo"},
      {"Input_y_ego", "PositionYEgo"},
      {"Input_yaw_ego", "YawEgo"},
      {"Input_lane_ego", "LaneEgo"},
      {"Input_s_ego", "PositionSEgo"},
      {"Input_t_ego", "PositionTEgo"},
      {"Input_presence_front", "ExistenceFront"},
      {"Input_x_front", "PositionXFront"},
      {"Input_y_front", "PositionYFront"},
      {"Input_width_front", "WidthFront"},
      {"Input_length_front", "LengthFront"},
      {"Input_yaw_front", "YawFront"},
      {"Input_s_front", "PositionSFront"},
      {"Input_t_front", "PositionTFront"},
      {"Input_v_front", "VelocityFront"},
      {"Input_lane_front", "LaneFront"},
      {"Input_presence_frontFront", "ExistenceFrontFront"},
      {"Input_x_frontFront", "PositionXFrontFront"},
      {"Input_y_frontFront", "PositionYFrontFront"},
      {"Input_v_frontFront", "VelocityFrontFront"},
      {"Input_lane_frontFront", "LaneFrontFront"},
      {"Input_deltaS_frontFront", "RelativeDistanceFrontFront"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));

  const std::string roadId{"Road"};
  std::vector<const WorldObjectInterface*> objectsInRange{&fakeFrontObject, &fakeFrontFrontObject};
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeEgoAgent, HasValidRoute()).WillByDefault(Return(true));
  ON_CALL(fakeEgoAgent, GetObjectsInRange(_, _, _)).WillByDefault(Return(objectsInRange));
  EXPECT_CALL(fakeAgent, GetVelocity(_)).WillOnce(Return(Common::Vector2d{1.0_mps, 0.0_mps}));
  EXPECT_CALL(fakeAgent, GetAcceleration(_))
      .WillOnce(
          Return(Common::Vector2d{2.0_mps_sq * units::math::cos(9.0_rad), 2.0_mps_sq * units::math::sin(9.0_rad)}));
  EXPECT_CALL(fakeAgent, GetCentripetalAcceleration()).WillOnce(Return(3.0_mps_sq));
  EXPECT_CALL(fakeAgent, GetSteeringWheelAngle()).WillOnce(Return(4.0_rad));
  EXPECT_CALL(fakeAgent, GetEffAccelPedal()).WillOnce(Return(5.0));
  EXPECT_CALL(fakeAgent, GetEffBrakePedal()).WillOnce(Return(6.0));
  EXPECT_CALL(fakeAgent, GetPositionX()).WillOnce(Return(7.0_m));
  EXPECT_CALL(fakeAgent, GetPositionY()).WillOnce(Return(8.0_m));
  EXPECT_CALL(fakeAgent, GetYaw()).WillRepeatedly(Return(9.0_rad));
  ON_CALL(fakeEgoAgent, GetRoadId()).WillByDefault(ReturnRef(roadId));
  auto mainLocatePosition = std::make_optional<GlobalRoadPosition>(roadId, 10, 11.0_m, 12.0_m, 9.0_rad);
  ON_CALL(fakeEgoAgent, GetMainLocatePosition()).WillByDefault(ReturnRef(mainLocatePosition));
  GlobalRoadPosition referencePosition{roadId, 10, 11.0_m, 12.0_m, 9.0_rad};
  ON_CALL(fakeEgoAgent, GetReferencePointPosition()).WillByDefault(Return(referencePosition));
  EXPECT_CALL(fakeEgoAgent, GetNetDistance(&fakeFrontObject)).WillOnce(Return(13.0_m));
  EXPECT_CALL(fakeEgoAgent, GetNetDistance(&fakeFrontFrontObject)).WillOnce(Return(14.0_m));
  EXPECT_CALL(fakeFrontObject, GetPositionX()).WillOnce(Return(15.0_m));
  EXPECT_CALL(fakeFrontObject, GetPositionY()).WillOnce(Return(16.0_m));
  EXPECT_CALL(fakeFrontObject, GetYaw()).WillOnce(Return(17.0_rad));
  EXPECT_CALL(fakeFrontObject, GetWidth()).WillOnce(Return(18.0_m));
  EXPECT_CALL(fakeFrontObject, GetLength()).WillOnce(Return(19.0_m));
  EXPECT_CALL(fakeFrontObject, GetVelocity(_)).WillOnce(Return(Common::Vector2d{20.0_mps, 0.0_mps}));
  GlobalRoadPositions frontPosition{{roadId, {roadId, 21, 22.0_m, 23.0_m, 17.0_rad}}};
  ON_CALL(fakeFrontObject, GetRoadPosition(_)).WillByDefault(ReturnRef(frontPosition));
  EXPECT_CALL(fakeFrontFrontObject, GetPositionX()).WillOnce(Return(24.0_m));
  EXPECT_CALL(fakeFrontFrontObject, GetPositionY()).WillOnce(Return(25.0_m));
  EXPECT_CALL(fakeFrontFrontObject, GetVelocity(_)).WillOnce(Return(Common::Vector2d{26.0_mps, 0.0_mps}));
  GlobalRoadPositions frontfrontPosition{{roadId, {roadId, 27, 1.0_m, 1.0_m, 1.0_rad}}};
  ON_CALL(fakeFrontFrontObject, GetRoadPosition(_)).WillByDefault(ReturnRef(frontfrontPosition));
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_shared<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(1, signal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(signal);
  ASSERT_THAT(accelerationSignal->acceleration.value(), DoubleEq(380.0));
}

TEST(FmuWrapper_InputTests, DummyFmu2WithParametersBool_OutputIsInput)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi2" / "DummyFmuParameter.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()}, {"Output_Output_ParameterSum", "AccelerationSignal_Acceleration"}};
  std::map<std::string, bool> boolParameter{
      {"Logging", true}, {"CsvOutput", true}, {"Parameter_Input_Parameter_boolean.Value", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(1, signal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(signal);
  ASSERT_THAT(accelerationSignal->acceleration.value(), DoubleEq(1));
}

TEST(FmuWrapper_InputTests, DummyFmu2WithParametersReal_OutputIsInput)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi2" / "DummyFmuParameter.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()}, {"Output_Output_ParameterSum", "AccelerationSignal_Acceleration"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{{"Parameter_Input_Parameter_integer_1.Value", 10},
                                          {"Parameter_Input_Parameter_integer_2.Value", 20}};
  std::map<std::string, double> doubleParameter{{"Parameter_Input_Parameter_double_1.Value", 1.23},
                                                {"Parameter_Input_Parameter_double_2.Value", 12.3}};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(1, signal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(signal);
  ASSERT_THAT(accelerationSignal->acceleration.value(), DoubleEq(43.53));
}

TEST(FmuWrapper_InputTests, DummyFmu2WithParametersString_OutputIsInput)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi2" / "DummyFmuParameter.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()},
      {"Parameter_Input_Parameter_string", "TestString"},
      {"Output_Output_ParameterSum", "AccelerationSignal_Acceleration"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(1, signal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(signal);
  ASSERT_THAT(accelerationSignal->acceleration.value(), DoubleEq(10));
}

TEST(FmuWrapper_OutputTests, DummyFmu2WithDynamicsSignalOutput_OutputIsAsExpected)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi2" / "DummyFmuOutputTest.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()},
      {"Output_output_1", "ComponentState"},
      {"Output_output_2", "DynamicsSignal_Acceleration"},
      {"Output_output_3", "DynamicsSignal_Velocity"},
      {"Output_output_4", "DynamicsSignal_PositionX"},
      {"Output_output_5", "DynamicsSignal_PositionY"},
      {"Output_output_6", "DynamicsSignal_Yaw"},
      {"Output_output_7", "DynamicsSignal_YawRate"},
      {"Output_output_8", "DynamicsSignal_YawAcceleration"},
      {"Output_output_9", "DynamicsSignal_SteeringWheelAngle"},
      {"Output_output_10", "DynamicsSignal_CentripetalAcceleration"},
      {"Output_output_11", "DynamicsSignal_TravelDistance"}};
  std::map<std::string, bool> boolParameter{{"Logging", false}, {"CsvOutput", false}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal;
  fmuWrapper->UpdateOutput(0, signal, 0);

  auto dynamicsSignal = std::dynamic_pointer_cast<const DynamicsSignal>(signal);
  EXPECT_THAT(dynamicsSignal->componentState, Eq(ComponentState::Acting));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.acceleration.value(), DoubleEq(2.0));
  EXPECT_THAT(openpass::hypot(dynamicsSignal->dynamicsInformation.velocityX.value(),
                              dynamicsSignal->dynamicsInformation.velocityY.value()),
              DoubleEq(3.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(4.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(5.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.yaw.value(), DoubleEq(6.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.yawRate.value(), DoubleEq(7.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.yawAcceleration.value(), DoubleEq(8.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.steeringWheelAngle.value(), DoubleEq(9.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.centripetalAcceleration.value(), DoubleEq(10.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.travelDistance.value(), DoubleEq(11.0));
}

TEST(FmuWrapper_OutputTests, DummyFmu2WithLongitudinalAndSteeringSignalOutput_OutputIsAsExpected)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi2" / "DummyFmuOutputTest.fmu";
  std::map<std::string, const std::string> stringParameter{{"FmuPath", fmuPath.string()},
                                                           {"Output_output_1", "ComponentState"},
                                                           {"Output_output_2", "LongitudinalSignal_AccPedalPos"},
                                                           {"Output_output_3", "LongitudinalSignal_BrakePedalPos"},
                                                           {"Output_output_12", "LongitudinalSignal_Gear"},
                                                           {"Output_output_4", "SteeringSignal_SteeringWheelAngle"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        100,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal1;
  fmuWrapper->UpdateOutput(2, signal1, 0);
  auto longitudinalSignal = std::dynamic_pointer_cast<const LongitudinalSignal>(signal1);
  std::shared_ptr<const SignalInterface> signal2;
  fmuWrapper->UpdateOutput(3, signal2, 0);
  auto steeringSignal = std::dynamic_pointer_cast<const SteeringSignal>(signal2);

  EXPECT_THAT(longitudinalSignal->componentState, Eq(ComponentState::Acting));
  EXPECT_THAT(longitudinalSignal->accPedalPos, DoubleEq(2.0));
  EXPECT_THAT(longitudinalSignal->brakePedalPos, DoubleEq(3.0));
  EXPECT_THAT(longitudinalSignal->gear, Eq(12));
  EXPECT_THAT(steeringSignal->componentState, Eq(ComponentState::Acting));
  EXPECT_THAT(steeringSignal->steeringWheelAngle.value(), DoubleEq(4.0));
}

TEST(FmuWrapper_OutputTests, DummyFmu2WithCompCtrlSignalOutput_OutputIsAsExpected)
{
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeParameter> fakeParameter;
  const auto fmuPath = resourcesFolder / "Fmi2" / "DummyFmuComCtrlsSignal.fmu";
  std::map<std::string, const std::string> stringParameter{
      {"FmuPath", fmuPath.string()},
      {"Output_Output_MovementDomain", "CompCtrlSignal_MovementDomain"},
      {"Output_Output_WarningActivity", "CompCtrlSignal_WarningActivity"},
      {"Output_Output_WarningLevel", "CompCtrlSignal_WarningLevel"},
      {"Output_Output_WarningType", "CompCtrlSignal_WarningType"},
      {"Output_Output_WarningIntensity", "CompCtrlSignal_WarningIntensity"},
      {"Output_Output_WarningDirection", "CompCtrlSignal_WarningDirection"}};
  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{{"SensorLinks", {}}};
  auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
  ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
  ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
  ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
  ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
  ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
  ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));
  ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  const std::vector<std::string> commands{};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
  ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  auto fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                        false,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        1000,
                                                                        &fakeWorld,
                                                                        nullptr,
                                                                        &fakeParameter,
                                                                        nullptr,
                                                                        nullptr,
                                                                        &fakeAgent,
                                                                        fakeScenarioControl);

  fmuWrapper->Trigger(0);
  std::shared_ptr<const SignalInterface> signal0;
  fmuWrapper->UpdateOutput(4, signal0, 0);
  auto compCtrlSignal0 = std::dynamic_pointer_cast<const VehicleCompToCompCtrlSignal>(signal0);
  EXPECT_THAT(compCtrlSignal0->GetCurrentState(), Eq(ComponentState::Acting));
  EXPECT_THAT(compCtrlSignal0->GetMovementDomain(), Eq(MovementDomain::Undefined));
  auto warnings0 = compCtrlSignal0->GetComponentWarnings();
  EXPECT_THAT(warnings0.front().activity, Eq(false));
  EXPECT_THAT(warnings0.front().level, Eq(ComponentWarningLevel::INFO));
  EXPECT_THAT(warnings0.front().type, Eq(ComponentWarningType::OPTIC));
  EXPECT_THAT(warnings0.front().intensity, Eq(ComponentWarningIntensity::LOW));
  EXPECT_THAT(warnings0.front().direction, Eq(AreaOfInterest::EGO_FRONT));

  fmuWrapper->Trigger(1000);
  std::shared_ptr<const SignalInterface> signal1;
  fmuWrapper->UpdateOutput(4, signal1, 0);
  auto compCtrlSignal1 = std::dynamic_pointer_cast<const VehicleCompToCompCtrlSignal>(signal1);
  EXPECT_THAT(compCtrlSignal1->GetCurrentState(), Eq(ComponentState::Acting));
  EXPECT_THAT(compCtrlSignal1->GetMovementDomain(), Eq(MovementDomain::Lateral));
  auto warnings1 = compCtrlSignal1->GetComponentWarnings();
  EXPECT_THAT(warnings1.front().activity, Eq(true));
  EXPECT_THAT(warnings1.front().level, Eq(ComponentWarningLevel::WARNING));
  EXPECT_THAT(warnings1.front().type, Eq(ComponentWarningType::ACOUSTIC));
  EXPECT_THAT(warnings1.front().intensity, Eq(ComponentWarningIntensity::MEDIUM));
  EXPECT_THAT(warnings1.front().direction, Eq(AreaOfInterest::HUD));

  fmuWrapper->Trigger(2000);
  std::shared_ptr<const SignalInterface> signal2;
  fmuWrapper->UpdateOutput(4, signal2, 0);
  auto compCtrlSignal2 = std::dynamic_pointer_cast<const VehicleCompToCompCtrlSignal>(signal2);
  EXPECT_THAT(compCtrlSignal2->GetCurrentState(), Eq(ComponentState::Acting));
  EXPECT_THAT(compCtrlSignal2->GetMovementDomain(), Eq(MovementDomain::Longitudinal));
  auto warnings2 = compCtrlSignal2->GetComponentWarnings();
  EXPECT_THAT(warnings2.front().activity, Eq(true));
  EXPECT_THAT(warnings2.front().level, Eq(ComponentWarningLevel::WARNING));
  EXPECT_THAT(warnings2.front().type, Eq(ComponentWarningType::HAPTIC));
  EXPECT_THAT(warnings2.front().intensity, Eq(ComponentWarningIntensity::HIGH));
  EXPECT_THAT(warnings2.front().direction, Eq(AreaOfInterest::HUD));

  fmuWrapper->Trigger(3000);
  std::shared_ptr<const SignalInterface> signal3;
  fmuWrapper->UpdateOutput(4, signal3, 0);
  auto compCtrlSignal3 = std::dynamic_pointer_cast<const VehicleCompToCompCtrlSignal>(signal3);
  EXPECT_THAT(compCtrlSignal3->GetCurrentState(), Eq(ComponentState::Acting));
  EXPECT_THAT(compCtrlSignal3->GetMovementDomain(), Eq(MovementDomain::Both));
  auto warnings3 = compCtrlSignal3->GetComponentWarnings();
  EXPECT_THAT(warnings3.front().activity, Eq(true));
  EXPECT_THAT(warnings3.front().level, Eq(ComponentWarningLevel::WARNING));
  EXPECT_THAT(warnings3.front().type, Eq(ComponentWarningType::HAPTIC));
  EXPECT_THAT(warnings3.front().intensity, Eq(ComponentWarningIntensity::HIGH));
  EXPECT_THAT(warnings3.front().direction, Eq(AreaOfInterest::HUD));
}

class FmuWrapper_OSMPDummy : public ::testing::Test
{
public:
  FmuWrapper_OSMPDummy()
  {
    zeroVector3d.set_x(0.0);
    zeroVector3d.set_y(0.0);
    zeroVector3d.set_z(0.0);

    zeroOrientation3d.set_yaw(0.0);
    zeroOrientation3d.set_pitch(0.0);
    zeroOrientation3d.set_roll(0.0);

    zeroErrorBaseMoving.mutable_position()->CopyFrom(zeroVector3d);
    zeroErrorBaseMoving.mutable_velocity()->CopyFrom(zeroVector3d);
    zeroErrorBaseMoving.mutable_acceleration()->CopyFrom(zeroVector3d);
    zeroErrorBaseMoving.mutable_orientation()->CopyFrom(zeroOrientation3d);
    zeroErrorBaseMoving.mutable_orientation_rate()->CopyFrom(zeroOrientation3d);
    zeroErrorBaseMoving.mutable_orientation_acceleration()->CopyFrom(zeroOrientation3d);

    sensorView->mutable_sensor_id()->set_value(0);
    sensorView->mutable_mounting_position()->mutable_position()->CopyFrom(zeroVector3d);
    sensorView->mutable_mounting_position()->mutable_orientation()->CopyFrom(zeroOrientation3d);
    sensorView->mutable_mounting_position_rmse()->mutable_position()->CopyFrom(zeroVector3d);
    sensorView->mutable_mounting_position_rmse()->mutable_orientation()->CopyFrom(zeroOrientation3d);

    sensorView->mutable_host_vehicle_id()->set_value(0);

    listsParameter = {{"SensorLinks", {}}};
    ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
    ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
    ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
    ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
    ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
    ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(listsParameter));
    ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRti));

    ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));

    vehicleModelParameters->bounding_box.geometric_center.x = 1.5_m;
    ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));

    ON_CALL(fakeWorld, GetWorldData()).WillByDefault(Return(&fakeWorldData));
    ON_CALL(fakeWorldData, GetOsiGroundTruth()).WillByDefault(ReturnRef(groundTruth));
    ON_CALL(fakeWorldData, GetSensorView(_, _, _))
        .WillByDefault([this](auto, auto, auto)
                       { return std::move(sensorView); });  // test::Return does not work with unique pointer

    ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
    ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
  }

  void InitFmu(std::map<std::string, const std::string> additionalStringParameter)
  {
    stringParameter.merge(additionalStringParameter);

    fmuWrapper = std::make_unique<AlgorithmFmuWrapperImplementation>("FmuWrapper",
                                                                     false,
                                                                     0,
                                                                     0,
                                                                     0,
                                                                     100,
                                                                     &fakeWorld,
                                                                     nullptr,
                                                                     &fakeParameter,
                                                                     nullptr,
                                                                     nullptr,
                                                                     &fakeAgent,
                                                                     fakeScenarioControl);
  }

  void PutEgo(const Common::Vector3d& position, const Common::Vector3d& orientation)
  {
    auto* hostVehicleData = sensorView->mutable_host_vehicle_data();
    auto* hostBaseMoving = hostVehicleData->mutable_location();
    auto* hostPosition = hostBaseMoving->mutable_position();
    auto* hostOrientation = hostBaseMoving->mutable_orientation();
    hostPosition->set_x(position.x);
    hostPosition->set_y(position.y);
    hostPosition->set_z(position.z);
    hostOrientation->set_yaw(orientation.x);
    hostOrientation->set_pitch(orientation.y);
    hostOrientation->set_roll(orientation.z);
    hostVehicleData->mutable_location_rmse()->CopyFrom(zeroErrorBaseMoving);
    auto* egoMovingObject = sensorView->mutable_global_ground_truth()->add_moving_object();
    egoMovingObject->mutable_base()->CopyFrom(*hostBaseMoving);
    egoMovingObject->mutable_vehicle_attributes()->mutable_bbcenter_to_rear()->set_x(
        -vehicleModelParameters->bounding_box.geometric_center.x.value());
    groundTruth.add_moving_object()->mutable_base()->CopyFrom(*hostBaseMoving);
  }

  void PutAgent(const Common::Vector3d& position, const Common::Vector3d& orientation)
  {
    auto* moving = sensorView->mutable_global_ground_truth()->add_moving_object();
    auto* baseMoving = moving->mutable_base();
    auto* osiPosition = baseMoving->mutable_position();
    auto* osiOrientation = baseMoving->mutable_orientation();
    osiPosition->set_x(position.x);
    osiPosition->set_y(position.y);
    osiPosition->set_z(position.z);
    osiOrientation->set_yaw(orientation.x);
    osiOrientation->set_pitch(orientation.y);
    osiOrientation->set_roll(orientation.z);
    moving->mutable_id()->set_value(uniqueId++);
    groundTruth.add_moving_object()->CopyFrom(*moving);
  }

protected:
  std::unique_ptr<AlgorithmFmuWrapperImplementation> fmuWrapper;
  NiceMock<FakeParameter> fakeParameter;
  osi3::GroundTruth groundTruth;
  OWL::SensorView_ptr sensorView{new osi3::SensorView()};
  std::uint64_t uniqueId = 1;
  osi3::Vector3d zeroVector3d;
  osi3::Orientation3d zeroOrientation3d;
  osi3::BaseMoving zeroErrorBaseMoving;
  const std::vector<std::string> commands{};

  NiceMock<FakeWorld> fakeWorld;
  NiceMock<OWL::Fakes::WorldData> fakeWorldData;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};

  std::shared_ptr<mantle_api::VehicleProperties> vehicleModelParameters
      = std::make_shared<mantle_api::VehicleProperties>();

  std::map<std::string, const std::string> stringParameter{};

  std::map<std::string, bool> boolParameter{{"Logging", true}, {"CsvOutput", true}};

  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};
  std::map<std::string, ParameterInterface::ParameterLists> listsParameter{};
};

TEST_F(FmuWrapper_OSMPDummy, WithSensorViewConfigRequest_ProvidedSensorViewConfigIsUsed)
{
  osi3::SensorViewConfiguration receivedSensorViewConfig;

  ON_CALL(fakeWorldData, GetSensorView(_, _, _))
      .WillByDefault(
          [&](auto sensorViewConfig, auto, auto)
          {
            receivedSensorViewConfig = sensorViewConfig;
            return std::move(sensorView);
          });

  const auto fmuPath = resourcesFolder / "OSMPDummySensor.fmu";
  InitFmu({{"FmuPath", fmuPath.string()},
           {"Output_OSMPSensorDataOut", "SensorData"},
           {"Input_OSMPSensorViewInConfig", "SensorViewConfig"},
           {"Output_OSMPSensorViewInConfigRequest", "SensorViewConfigRequest"},
           {"Input_OSMPSensorViewIn", "SensorView"}});

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  fmuWrapper->Trigger(0);

  EXPECT_THAT(receivedSensorViewConfig.field_of_view_horizontal(), Eq(3.14));
  EXPECT_THAT(receivedSensorViewConfig.field_of_view_vertical(), Eq(3.14));
}

TEST_F(FmuWrapper_OSMPDummy, WithNoSensorViewConfigRequest_DefaultValueIsUsed)
{
  osi3::SensorViewConfiguration receivedSensorViewConfig;

  ON_CALL(fakeWorldData, GetSensorView(_, _, _))
      .WillByDefault(
          [&](auto sensorViewConfig, auto, auto)
          {
            receivedSensorViewConfig = sensorViewConfig;
            return std::move(sensorView);
          });

  const auto fmuPath = resourcesFolder / "OSMPDummySensor.fmu";
  InitFmu({{"FmuPath", fmuPath.string()},
           {"Output_OSMPSensorDataOut", "SensorData"},
           {"Input_OSMPSensorViewInConfig", "SensorViewConfig"},
           {"Input_OSMPSensorViewIn", "SensorView"}});

  fmuWrapper->Trigger(0);

  EXPECT_THAT(receivedSensorViewConfig.field_of_view_horizontal(), Eq(M_2_PI));
  EXPECT_THAT(receivedSensorViewConfig.field_of_view_vertical(), Eq(M_1_PI / 10.0));
  EXPECT_THAT(receivedSensorViewConfig.range(), Eq(std::numeric_limits<double>::max()));
}

TEST_F(FmuWrapper_OSMPDummy, SensorViewWithEgoAndNoMovingObjects_SensorDataContainsNoObjects)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  const auto fmuPath = resourcesFolder / "OSMPDummySensor.fmu";
  InitFmu({{"FmuPath", fmuPath.string()},
           {"Output_OSMPSensorDataOut", "SensorData"},
           {"Input_OSMPSensorViewIn", "SensorView"}});

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  fmuWrapper->Trigger(0);
  fmuWrapper->UpdateOutput(6, outputSignal, 0);

  auto sensorDataSignal = std::dynamic_pointer_cast<const SensorDataSignal>(outputSignal);

  ASSERT_THAT(sensorDataSignal, NotNull());

  const auto& sensorData = sensorDataSignal->sensorData;

  ASSERT_THAT(sensorData.sensor_view_size(), Eq(1));
  EXPECT_THAT(sensorData.moving_object_size(), Eq(0));
}

TEST_F(FmuWrapper_OSMPDummy, SensorViewWithEgoAndTwoMovingObjects_SensorDataContainsObjectsAtCorrectLocation)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  PutEgo({0.0, 0.0, 0.0}, {0.0, 0.0, 0.0});
  PutAgent({30.0, 2.0, 0.0}, {0.0, 0.0, 0.0});
  PutAgent({40.0, -1.0, 0.0}, {0.0, 0.0, 0.0});

  const auto fmuPath = resourcesFolder / "OSMPDummySensor.fmu";
  InitFmu({{"FmuPath", fmuPath.string()},
           {"Output_OSMPSensorDataOut", "SensorData"},
           {"Input_OSMPSensorViewIn", "SensorView"}});

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  fmuWrapper->Trigger(0);
  fmuWrapper->UpdateOutput(6, outputSignal, 0);

  auto sensorDataSignal = std::dynamic_pointer_cast<const SensorDataSignal>(outputSignal);

  ASSERT_THAT(sensorDataSignal, NotNull());

  const auto& sensorData = sensorDataSignal->sensorData;

  ASSERT_THAT(sensorData.sensor_view_size(), Eq(1));
  EXPECT_THAT(sensorData.moving_object_size(), Eq(2));

  auto movingObject1 = sensorData.moving_object(0).base();
  auto movingObject2 = sensorData.moving_object(1).base();

  EXPECT_THAT(movingObject1.position().x(), DoubleEq(30.0));
  EXPECT_THAT(movingObject1.position().y(), DoubleEq(2.0));
  EXPECT_THAT(movingObject2.position().x(), DoubleEq(40.0));
  EXPECT_THAT(movingObject2.position().y(), DoubleEq(-1.0));
}

TEST_F(FmuWrapper_OSMPDummy, SensorViewWithRotatedEgoAndTwoMovingObjects_SensorDataContainsObjectsAtCorrectLocation)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  PutEgo({0.0, 0.0, 0.0}, {0.1, 0.0, 0.0});
  PutAgent({30.0, 2.0, 0.0}, {0.0, 0.0, 0.0});
  PutAgent({40.0, -1.0, 0.0}, {0.0, 0.0, 0.0});

  const auto fmuPath = resourcesFolder / "OSMPDummySensor.fmu";
  InitFmu({{"FmuPath", fmuPath.string()},
           {"Output_OSMPSensorDataOut", "SensorData"},
           {"Input_OSMPSensorViewIn", "SensorView"}});

  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlstrategies = {};
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlstrategies));

  fmuWrapper->Trigger(0);
  fmuWrapper->UpdateOutput(6, outputSignal, 0);

  auto sensorDataSignal = std::dynamic_pointer_cast<const SensorDataSignal>(outputSignal);

  ASSERT_THAT(sensorDataSignal, NotNull());

  const auto& sensorData = sensorDataSignal->sensorData;

  ASSERT_THAT(sensorData.sensor_view_size(), Eq(1));
  EXPECT_THAT(sensorData.moving_object_size(), Eq(2));

  auto movingObject1 = sensorData.moving_object(0).base();
  auto movingObject2 = sensorData.moving_object(1).base();

  EXPECT_THAT(movingObject1.position().x(), DoubleEq(30.0 * std::cos(-0.1) - 2.0 * std::sin(-0.1)));
  EXPECT_THAT(movingObject1.position().y(), DoubleEq(30.0 * std::sin(-0.1) + 2.0 * std::cos(-0.1)));
  EXPECT_THAT(movingObject2.position().x(), DoubleEq(40.0 * std::cos(-0.1) + 1.0 * std::sin(-0.1)));
  EXPECT_THAT(movingObject2.position().y(), DoubleEq(40.0 * std::sin(-0.1) - 1.0 * std::cos(-0.1)));
}

TEST_F(FmuWrapper_OSMPDummy, LinearTrajectorySignal_TrafficUpdateIsCorrectlyConvertedToDynamicsSignal)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  mantle_api::PolyLinePoint polyLinePoint1{{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}}, 0.0_s};
  mantle_api::PolyLinePoint polyLinePoint2{{{2.0_m, 3.0_m, 0.0_m}, {0.4_rad, 0.0_rad, 0.0_rad}}, 1.0_s};
  mantle_api::PolyLine polyLine;
  polyLine.push_back(polyLinePoint1);
  polyLine.push_back(polyLinePoint2);

  mantle_api::Trajectory fakeCoordinates = {"", polyLine};
  auto controlStrategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
  controlStrategy->trajectory = fakeCoordinates;
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> controlStrategies{controlStrategy};

  PutEgo({0.0, 0.0, 0.0}, {0.0, 0.0, 0.0});

  const auto fmuPath = resourcesFolder / "OSMPDummyTCTU.fmu";
  InitFmu({{"FmuPath", fmuPath.string()},
           {"Output_OSMPTrafficUpdateOut", "TrafficUpdate"},
           {"Input_OSMPTrafficCommandIn", "TrafficCommand"},
           {"Input_OSMPSensorViewIn", "SensorView"},
           {"Init_OSMPGroundTruthInit", "GroundTruth"}});

  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(controlStrategies));
  ScenarioCommands commands{};
  ON_CALL(*fakeScenarioControl, GetCommands()).WillByDefault(ReturnRef(commands));

  fmuWrapper->Trigger(0);
  fmuWrapper->UpdateOutput(0, outputSignal, 0);

  auto dynamicsSignal = std::dynamic_pointer_cast<const DynamicsSignal>(outputSignal);

  ASSERT_THAT(dynamicsSignal, NotNull());

  EXPECT_THAT(dynamicsSignal->dynamicsInformation.acceleration.value(),
              DoubleEq(std::cos(0.4) * 10.0 + std::sin(0.4) * 15.0));
  EXPECT_THAT(openpass::hypot(dynamicsSignal->dynamicsInformation.velocityX.value(),
                              dynamicsSignal->dynamicsInformation.velocityY.value()),
              DoubleEq(openpass::hypot(20.0, 30.0)));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.positionX.value(), DoubleEq(2.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.positionY.value(), DoubleEq(3.0));
  EXPECT_THAT(dynamicsSignal->dynamicsInformation.yaw.value(), DoubleEq(0.4));

  fmuWrapper->UpdateOutput(1, outputSignal, 0);

  auto accelerationSignal = std::dynamic_pointer_cast<const AccelerationSignal>(outputSignal);

  ASSERT_THAT(accelerationSignal, NotNull());

  EXPECT_THAT(accelerationSignal->acceleration.value(), DoubleEq(std::cos(0.4) * 10.0 + std::sin(0.4) * 15.0));

  fmuWrapper->UpdateOutput(2, outputSignal, 0);

  auto longitudinalSignal = std::dynamic_pointer_cast<const LongitudinalSignal>(outputSignal);

  ASSERT_THAT(longitudinalSignal, NotNull());

  EXPECT_THAT(longitudinalSignal->accPedalPos, DoubleEq(0.2));
  EXPECT_THAT(longitudinalSignal->brakePedalPos, DoubleEq(0.3));
  EXPECT_THAT(longitudinalSignal->gear, Eq(4));

  fmuWrapper->UpdateOutput(3, outputSignal, 0);

  auto steeringSignal = std::dynamic_pointer_cast<const SteeringSignal>(outputSignal);

  ASSERT_THAT(steeringSignal, NotNull());

  EXPECT_THAT(steeringSignal->steeringWheelAngle.value(), DoubleEq(0.5));
}
