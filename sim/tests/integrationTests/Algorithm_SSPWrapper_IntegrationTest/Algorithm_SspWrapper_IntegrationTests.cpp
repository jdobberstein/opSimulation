/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <MantleAPI/Traffic/control_strategy.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <algorithm>
#include <filesystem>
#include <map>
#include <memory>
#include <osi3/osi_common.pb.h>
#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_hostvehicledata.pb.h>
#include <osi3/osi_object.pb.h>
#include <osi3/osi_sensorview.pb.h>
#include <string>
#include <units.h>
#include <utility>
#include <vector>

#include "AlgorithmSspWrapperImplementation.h"
#include "OWL/fakes/fakeWorldData.h"
#include "SSPElements/Connector/ScalarConnector.h"
#include "SSPElements/NetworkElement.h"
#include "SSPElements/System.h"
#include "WorldData.h"
#include "common/dynamicsSignal.h"
#include "common/globalDefinitions.h"
#include "common/runtimeInformation.h"
#include "common/vector2d.h"
#include "common/vector3d.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeEgoAgent.h"
#include "fakeParameter.h"
#include "fakeScenarioControl.h"
#include "fakeWorld.h"
#include "include/callbackInterface.h"
#include "include/scenarioControlInterface.h"
#include "type_helper.h"

class SignalInterface;

using ::testing::_;
using ::testing::Eq;
using ::testing::NiceMock;
using ::testing::NotNull;
using ::testing::Return;
using ::testing::ReturnRef;

const std::filesystem::path resourcesFolder{std::filesystem::current_path() / "Resources"};
const std::filesystem::path resourcesFolderLinux{std::filesystem::current_path() / "Resources/linux"};
const std::filesystem::path resourcesFolderWin{std::filesystem::current_path() / "Resources/win64"};

class SspWrapper_OSMPConnectionTest : public ::testing::Test
{
public:
  SspWrapper_OSMPConnectionTest()
  {
    zeroVector3d.set_x(0.0);
    zeroVector3d.set_y(0.0);
    zeroVector3d.set_z(0.0);

    zeroOrientation3d.set_yaw(0.0);
    zeroOrientation3d.set_pitch(0.0);
    zeroOrientation3d.set_roll(0.0);

    zeroErrorBaseMoving.mutable_position()->CopyFrom(zeroVector3d);
    zeroErrorBaseMoving.mutable_velocity()->CopyFrom(zeroVector3d);
    zeroErrorBaseMoving.mutable_acceleration()->CopyFrom(zeroVector3d);
    zeroErrorBaseMoving.mutable_orientation()->CopyFrom(zeroOrientation3d);
    zeroErrorBaseMoving.mutable_orientation_rate()->CopyFrom(zeroOrientation3d);
    zeroErrorBaseMoving.mutable_orientation_acceleration()->CopyFrom(zeroOrientation3d);

    sensorView->mutable_sensor_id()->set_value(0);
    sensorView->mutable_mounting_position()->mutable_position()->CopyFrom(zeroVector3d);
    sensorView->mutable_mounting_position()->mutable_orientation()->CopyFrom(zeroOrientation3d);
    sensorView->mutable_mounting_position_rmse()->mutable_position()->CopyFrom(zeroVector3d);
    sensorView->mutable_mounting_position_rmse()->mutable_orientation()->CopyFrom(zeroOrientation3d);

    sensorView->mutable_host_vehicle_id()->set_value(0);

    ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
    ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
    ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
    ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
    ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(runtimeInformation));

    ON_CALL(fakeWorld, GetWorldData()).WillByDefault(Return(&fakeWorldData));
    ON_CALL(fakeWorldData, GetOsiGroundTruth()).WillByDefault(ReturnRef(groundTruth));
    ON_CALL(fakeWorldData, GetSensorView(_, _, _))
        .WillByDefault(
            [this](auto, auto, auto)
            {
              OWL::SensorView_ptr sv{new osi3::SensorView()};
              sv->CopyFrom(*sensorView);
              return std::move(sv);
            });  //test::Return does not work with unique pointer

    ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
    auto entityProperties = std::make_shared<const mantle_api::EntityProperties>();
    ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

    ON_CALL(*fakeScenarioControl, GetCustomCommands()).WillByDefault(ReturnRef(commands));
    ON_CALL(*fakeScenarioControl, GetCommands()).WillByDefault(ReturnRef(scenarioCommands));
    ON_CALL(*fakeScenarioControl, HasNewLongitudinalStrategy()).WillByDefault(Return(false));
    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies = {};
    ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
        .WillByDefault(Return(control_strategies));
  }

  void InitSsp(std::map<std::string, const std::string> additionalStringParameter)
  {
    stringParameter.merge(additionalStringParameter);

    sspWrapper = std::make_unique<AlgorithmSspWrapperImplementation>("SspWrapper",
                                                                     false,
                                                                     0,
                                                                     0,
                                                                     0,
                                                                     100,
                                                                     nullptr,
                                                                     &fakeWorld,
                                                                     &fakeParameter,
                                                                     nullptr,
                                                                     &fakeAgent,
                                                                     &fakeCallback,
                                                                     fakeScenarioControl);
  }

  void PutEgo(const Common::Vector3d& position, const Common::Vector3d& orientation)
  {
    auto* hostVehicleData = sensorView->mutable_host_vehicle_data();
    auto* hostBaseMoving = hostVehicleData->mutable_location();
    auto* hostPosition = hostBaseMoving->mutable_position();
    auto* hostOrientation = hostBaseMoving->mutable_orientation();
    hostPosition->set_x(position.x);
    hostPosition->set_y(position.y);
    hostPosition->set_z(position.z);
    hostOrientation->set_yaw(orientation.x);
    hostOrientation->set_pitch(orientation.y);
    hostOrientation->set_roll(orientation.z);
    hostVehicleData->mutable_location_rmse()->CopyFrom(zeroErrorBaseMoving);
    sensorView->mutable_global_ground_truth()->add_moving_object()->mutable_base()->CopyFrom(*hostBaseMoving);
    groundTruth.add_moving_object()->mutable_base()->CopyFrom(*hostBaseMoving);
  }

  void PutAgent(const Common::Vector3d& position, const Common::Vector3d& orientation)
  {
    auto* moving = sensorView->mutable_global_ground_truth()->add_moving_object();
    auto* baseMoving = moving->mutable_base();
    auto* osiPosition = baseMoving->mutable_position();
    auto* osiOrientation = baseMoving->mutable_orientation();
    osiPosition->set_x(position.x);
    osiPosition->set_y(position.y);
    osiPosition->set_z(position.z);
    osiOrientation->set_yaw(orientation.x);
    osiOrientation->set_pitch(orientation.y);
    osiOrientation->set_roll(orientation.z);
    moving->mutable_id()->set_value(uniqueId++);
    groundTruth.add_moving_object()->CopyFrom(*moving);
  }

protected:
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<OWL::Fakes::WorldData> fakeWorldData;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeCallback> fakeCallback;
  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  std::unique_ptr<AlgorithmSspWrapperImplementation> sspWrapper;
  NiceMock<FakeParameter> fakeParameter;
  osi3::GroundTruth groundTruth;
  OWL::SensorView_ptr sensorView{new osi3::SensorView()};
  std::uint64_t uniqueId = 1;
  osi3::Vector3d zeroVector3d;
  osi3::Orientation3d zeroOrientation3d;
  osi3::BaseMoving zeroErrorBaseMoving;

  std::map<std::string, const std::string> stringParameter{{"TargetLocation", "configs/ssp"}};
  std::map<std::string, bool> boolParameter{};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  openpass::common::RuntimeInformation runtimeInformation{0, 100, {std::filesystem::current_path().string(), "", ""}};
  const std::vector<std::string> commands{};
  const ScenarioCommands scenarioCommands{};
};

TEST_F(SspWrapper_OSMPConnectionTest,
       OSMPDummySensorAndOSMPTrafficStepperConnectionTest_MovesAgentFromDummySensorPositionByTrafficStepperUpdate)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  ON_CALL(fakeCallback, Log(_, _, _, _)).WillByDefault(Return());

  PutEgo({0, 0, 0}, {0, 0, 0});
  PutAgent({4, 0, 0}, {0, 0, 0});
  const auto sspPath = resourcesFolder / "OSMPConnectionTest.ssp";
  InitSsp({{"SspPath", sspPath.string()}});

  sspWrapper->Trigger(0);
  sspWrapper->UpdateOutput(0, outputSignal, 0);
  auto sensorDataSignal = std::dynamic_pointer_cast<const DynamicsSignal>(outputSignal);
  ASSERT_THAT(sensorDataSignal, NotNull());
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionX.value(), Eq(0.01));
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionY.value(), Eq(0.01));
}

TEST_F(SspWrapper_OSMPConnectionTest,
       OSMPDummySensorAndOSMPTrafficStepperConnectionTest_AnnotationsOnComponentAndSystem)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  ON_CALL(fakeCallback, Log(_, _, _, _)).WillByDefault(Return());

  PutEgo({0, 0, 0}, {0, 0, 0});
  PutAgent({4, 0, 0}, {0, 0, 0});
  const auto sspPath = resourcesFolder / "ConnectionTestAnnotated.ssp";
  InitSsp({{"SspPath", sspPath.string()}});

  sspWrapper->Trigger(0);
  sspWrapper->UpdateOutput(0, outputSignal, 0);
  auto sensorDataSignal = std::dynamic_pointer_cast<const DynamicsSignal>(outputSignal);
  ASSERT_THAT(sensorDataSignal, NotNull());
}

TEST_F(SspWrapper_OSMPConnectionTest, OSMPConnectionTest_UnpackingSSPFileWithinSSP)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  PutEgo({0, 0, 0}, {0, 0, 0});
  PutAgent({4, 0, 0}, {0, 0, 0});
  const auto sspPath = resourcesFolder / "OSMPConnectionTestSspFile.ssp";
  InitSsp({{"SspPath", sspPath.string()}});

  sspWrapper->Trigger(0);
  sspWrapper->UpdateOutput(0, outputSignal, 0);
  auto sensorDataSignal = std::dynamic_pointer_cast<const DynamicsSignal>(outputSignal);
  ASSERT_THAT(sensorDataSignal, NotNull());
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionX.value(), Eq(0.01));
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionY.value(), Eq(0.01));
}

TEST_F(SspWrapper_OSMPConnectionTest, OSMPConnectionTest_withinSystemStructure)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  PutEgo({0, 0, 0}, {0, 0, 0});
  PutAgent({4, 0, 0}, {0, 0, 0});
  const auto sspPath = resourcesFolder / "OSMPConnectionTestWithinSystemStructure.ssp";
  InitSsp({{"SspPath", sspPath.string()}});

  sspWrapper->Trigger(0);
  sspWrapper->UpdateOutput(0, outputSignal, 0);
  auto sensorDataSignal = std::dynamic_pointer_cast<const DynamicsSignal>(outputSignal);
  ASSERT_THAT(sensorDataSignal, NotNull());
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionX.value(), Eq(0.01));
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionY.value(), Eq(0.01));
}

TEST_F(SspWrapper_OSMPConnectionTest, OSMPConnectionTest_OnlyUseReferencedSSDs)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  PutEgo({0, 0, 0}, {0, 0, 0});
  PutAgent({4, 0, 0}, {0, 0, 0});
  const auto sspPath = resourcesFolder / "OSMPConnectionTest_OnlyUseReferencedSSDs.ssp";
  InitSsp({{"SspPath", sspPath.string()}});

  auto rootSystem = sspWrapper->GetRootSystem();
  auto elements = rootSystem->GetElements();

  ASSERT_EQ(elements.size(), 1);
  ASSERT_EQ(elements.at(0)->GetName(), "OSMPConnectionTest");

  sspWrapper->Trigger(0);
  sspWrapper->UpdateOutput(0, outputSignal, 0);
  auto sensorDataSignal = std::dynamic_pointer_cast<const DynamicsSignal>(outputSignal);
  ASSERT_THAT(sensorDataSignal, NotNull());
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionX.value(), Eq(0.01));
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionY.value(), Eq(0.01));
}

TEST_F(SspWrapper_OSMPConnectionTest, OSMPConnectionTest_SubSubSystem)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  PutEgo({0, 0, 0}, {0, 0, 0});
  PutAgent({4, 0, 0}, {0, 0, 0});
  const auto sspPath = resourcesFolder / "OSMPConnectionTest_SubSubSystem.ssp";
  InitSsp({{"SspPath", sspPath.string()}});

  auto rootSystem = sspWrapper->GetRootSystem();
  auto elements = rootSystem->GetElements();

  ASSERT_EQ(elements.size(), 1);
  ASSERT_EQ(elements.at(0)->GetName(), "SystemHolder");

  sspWrapper->Trigger(0);
  sspWrapper->UpdateOutput(0, outputSignal, 0);
  auto sensorDataSignal = std::dynamic_pointer_cast<const DynamicsSignal>(outputSignal);

  ASSERT_THAT(sensorDataSignal, NotNull());
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionX.value(), Eq(0.01));
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionY.value(), Eq(0.01));
}

TEST_F(SspWrapper_OSMPConnectionTest, OSMPConnectionTest_SubSubSystemWithConnections)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  PutEgo({0, 0, 0}, {0, 0, 0});
  PutAgent({4, 0, 0}, {0, 0, 0});
  const auto sspPath = resourcesFolder / "OSMPConnectionTest_SubSubSystemWithConnections.ssp";
  InitSsp({{"SspPath", sspPath.string()}});

  auto rootSystem = sspWrapper->GetRootSystem();
  auto elements = rootSystem->GetElements();

  ASSERT_EQ(elements.size(), 1);
  ASSERT_EQ(elements.at(0)->GetName(), "SystemHolder");

  sspWrapper->Trigger(0);
  sspWrapper->UpdateOutput(0, outputSignal, 0);
  auto sensorDataSignal = std::dynamic_pointer_cast<const DynamicsSignal>(outputSignal);

  ASSERT_THAT(sensorDataSignal, NotNull());
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionX.value(), Eq(0.01));
  ASSERT_THAT(sensorDataSignal->dynamicsInformation.positionY.value(), Eq(0.01));
}

TEST_F(SspWrapper_OSMPConnectionTest, OSMPDummySensorAndOSMPTrafficStepperConnectionTest_TrafficStepperUnconnected)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  PutEgo({0, 0, 0}, {0, 0, 0});
  PutAgent({4, 0, 0}, {0, 0, 0});
  const auto sspPath = resourcesFolder / "OSMPConnectionTestUnconnected.ssp";
  InitSsp({{"SspPath", sspPath.string()}});

  sspWrapper->Trigger(0);
  sspWrapper->UpdateOutput(0, outputSignal, 0);
  auto sensorDataSignal = std::dynamic_pointer_cast<const DynamicsSignal>(outputSignal);
  ASSERT_THAT(sensorDataSignal, nullptr);
}

TEST_F(SspWrapper_OSMPConnectionTest,
       OSMPDummySensorAndOSMPTrafficStepperConnectionTest_TrafficStepperNoOuterConnectors)
{
  std::shared_ptr<const SignalInterface> outputSignal;

  PutEgo({0, 0, 0}, {0, 0, 0});
  PutAgent({4, 0, 0}, {0, 0, 0});
  const auto sspPath = resourcesFolder / "OSMPConnectionTestNoOuterConnectors.ssp";
  InitSsp({{"SspPath", sspPath.string()}});

  sspWrapper->Trigger(0);
  sspWrapper->UpdateOutput(0, outputSignal, 0);
  auto sensorDataSignal = std::dynamic_pointer_cast<const DynamicsSignal>(outputSignal);
  ASSERT_THAT(sensorDataSignal, nullptr);
}

class SspWrapper_ScalarConnectionTest : public SspWrapper_OSMPConnectionTest
{
protected:
  void RunAssertions()
  {
    std::shared_ptr<const SignalInterface> outputSignal;

    auto rootSystem = sspWrapper->GetRootSystem();
    auto elements = rootSystem->GetElements();

    ASSERT_EQ(elements.size(), 1);
    ASSERT_EQ(elements.at(0)->GetName(), "ScalarConnectionTest");

    auto inputConnectorsOfFirstElement = rootSystem->GetElements()[0]->GetElements()[0]->GetInputConnectors();
    auto inputConnectorsOfSecondElement = rootSystem->GetElements()[0]->GetElements()[1]->GetInputConnectors();
    auto outputConnectorsOfLastElement = rootSystem->GetElements()[0]->GetElements()[1]->GetOutputConnectors();

    auto inputDoubleConnector
        = std::find_if(inputConnectorsOfFirstElement.begin(),
                       inputConnectorsOfFirstElement.end(),
                       [](auto connector) { return connector->GetConnectorName() == "InputDouble"; });

    auto inputIntegerConnector1
        = std::find_if(inputConnectorsOfFirstElement.begin(),
                       inputConnectorsOfFirstElement.end(),
                       [](auto connector) { return connector->GetConnectorName() == "InputInteger"; });

    if (inputDoubleConnector != inputConnectorsOfFirstElement.end())
    {
      if (std::shared_ptr<ssp::ScalarConnector<ssp::VariableTypeDouble>> fmuScalarConnector
          = std::dynamic_pointer_cast<ssp::ScalarConnector<ssp::VariableTypeDouble>>(*inputDoubleConnector))
      {
        fmuScalarConnector->SetValue(2.0);
      }
    }

    if (inputIntegerConnector1 != inputConnectorsOfFirstElement.end())
    {
      if (std::shared_ptr<ssp::ScalarConnector<ssp::VariableTypeInt>> fmuScalarConnector
          = std::dynamic_pointer_cast<ssp::ScalarConnector<ssp::VariableTypeInt>>(*inputIntegerConnector1))
      {
        fmuScalarConnector->SetValue(1);
      }
    }

    auto inputIntegerConnector2
        = std::find_if(inputConnectorsOfSecondElement.begin(),
                       inputConnectorsOfSecondElement.end(),
                       [](auto connector) { return connector->GetConnectorName() == "InputInteger"; });

    if (inputIntegerConnector2 != inputConnectorsOfSecondElement.end())
    {
      if (std::shared_ptr<ssp::ScalarConnector<ssp::VariableTypeInt>> fmuScalarConnector
          = std::dynamic_pointer_cast<ssp::ScalarConnector<ssp::VariableTypeInt>>(*inputIntegerConnector2))
      {
        fmuScalarConnector->SetValue(2);
      }
    }

    sspWrapper->Trigger(0);

    auto outputDoubleConnector
        = std::find_if(outputConnectorsOfLastElement.begin(),
                       outputConnectorsOfLastElement.end(),
                       [](auto connector) { return connector->GetConnectorName() == "OutputDouble"; });

    if (outputDoubleConnector != outputConnectorsOfLastElement.end())
    {
      if (std::shared_ptr<ssp::ScalarConnector<ssp::VariableTypeDouble>> fmuScalarConnector
          = std::dynamic_pointer_cast<ssp::ScalarConnector<ssp::VariableTypeDouble>>(*outputDoubleConnector))
      {
        auto result = fmuScalarConnector->GetValue();
        ASSERT_EQ(result, 16);
      }
    }
  }
};

TEST_F(SspWrapper_ScalarConnectionTest, Fmi1)
{
  const auto sspPath = resourcesFolder / "ConnectionScalarTestFmi1.ssp";
  InitSsp({{"SspPath", sspPath.string()}});
  RunAssertions();
}

TEST_F(SspWrapper_ScalarConnectionTest, Fmi2)
{
  const auto sspPath = resourcesFolder / "ConnectionScalarTestFmi2.ssp";
  InitSsp({{"SspPath", sspPath.string()}});
  RunAssertions();
}
