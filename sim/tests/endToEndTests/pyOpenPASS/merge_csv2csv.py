#!/usr/bin/env python3
################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

import re
import shutil
import pandas as pd
import logging
from pathlib import Path
from functools import reduce

logger = logging.getLogger(__file__)


def extract_columns_without_prefix(df):
    src_col_names = df.columns.tolist()
    cols_mapping = {}
    for col in src_col_names:
        # look for 00:<scope>:<column> -> we are just intersted in <column>
        # failsafe: take last part of split if there was one
        # so everything after the last colon or dot
        # or the column name iteself (eg. for Timestep)
        parts = re.split(r'[:.]', col)
        if len(parts) == 3:
            cols_mapping[col] = f'{parts[0]}:{parts[2]}'
        else:
            cols_mapping[col] = parts[-1]
    output_df = df.copy()
    output_df.rename(columns=cols_mapping, inplace=True)
    return output_df


def clear_sources(csv_file_paths):
    for file_path in csv_file_paths:
        if file_path.exists():
            file_path.unlink()

def check_columns(csv_file_path: Path):
    df = pd.read_csv(csv_file_path, nrows=0)
    entity_match = re.search(r'entity(\d+)', str(csv_file_path))
    if not entity_match:
        return False
    entity_id = int(entity_match.group(1))

    if 'Timestep' in df.columns:
        col_ids = []
        for col in [col for col in df.columns if col != 'Timestep']:
            col_id_match = re.search(r'(\d+):.*', col)
            if col_id_match:
                col_ids.append(int(col_id_match.group(1)))
        return all(entity_id == id for id in col_ids)
    return False

def collect_mergeable_data(component_paths):
    csv_files = []

    for component in component_paths:
        if component.is_dir():
            for csv_file in component.glob('**/*.csv'):
                if csv_file.is_file() and check_columns(csv_file):
                    csv_files.append(csv_file)

    return csv_files

def get_source_dataframes(csv_file_paths):
    dataframes = []

    for file_path in csv_file_paths:
        logger.info(f'Found file "{file_path}" to merge')
        df = extract_columns_without_prefix(pd.read_csv(file_path, dtype=str))
        logger.debug(
            f'Extracted {len(df.columns) - 1} columns from "{file_path}"')
        if len(df.columns) > 1:
            dataframes.append(df)

    return dataframes

def get_matching_cyclics_file(results_path: Path, run_id: str):
    for dst_file in results_path.glob('*.csv'):
        dst_file_match = re.search(r'Cyclics_Run_(\d+).csv', str(dst_file))
        run_id_match = re.search(r'run(\d+)', run_id)
        if dst_file.is_file() and dst_file_match and run_id_match:
            if int(dst_file_match.group(1)) == int(run_id_match.group(1)):
                return dst_file
    return None

def extract_agent_data_csv(results_path: Path, component_paths: list, run_id: str, backup_dst_files: bool, keep_source: bool):
    logger.debug(f'Searching "{results_path}" for cyclics files')
    if (dst_file := get_matching_cyclics_file(results_path, run_id)):
        logger.info(f'Collecting additional information for "{dst_file}"')
        if backup_dst_files:
            logger.debug(
                f'Creating backup of "{dst_file}" as "{dst_file.with_suffix(".orig")}"')
            shutil.copyfile(dst_file, dst_file.with_suffix('.orig'))

        csv_files = collect_mergeable_data(component_paths)
        src_dfs = get_source_dataframes(csv_files)

        # insert original data
        src_dfs.insert(0, pd.read_csv(dst_file, dtype=str))

        # strips all column names
        for df in src_dfs:
            df.columns = df.columns.str.strip()

        output_df = reduce(lambda left, right:
                        pd.merge(left, right, on='Timestep', how='left'), src_dfs).fillna('')

        # sort columns
        output_df = output_df.sort_index(axis=1)
        cols = ['Timestep'] + \
            [col for col in output_df.columns if col != 'Timestep']

        logger.debug(f'Writing merged data to "{dst_file}"')
        output_df[cols].to_csv(dst_file, index=False)

        if keep_source == False:
            clear_sources(csv_files)
