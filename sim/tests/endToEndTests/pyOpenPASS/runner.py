################################################################################
# Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

from pathlib import Path
import subprocess
import warnings
import psutil
from tempfile import NamedTemporaryFile
import shutil
from time import sleep



class Runner:
    def __init__(self, executable):
        self.base_path = Path(executable).parent
        self.executable = executable

    @staticmethod
    def _get_memory_usage(pid: int) -> float:
        """
        Get the memory usage of a process with the specified process ID (PID)
        in megabytes (MB).

        Parameters:
            pid (int): The process ID (PID) of the target process.

        Returns:
            float: The memory usage of the process in megabytes (MB).
        """
        memory_info = psutil.Process(pid).memory_full_info()
        return memory_info.uss / (1024.0 * 1024.0)

    @staticmethod
    def get_memory_usage(pid: int) -> float:
        """
        Memory usage of a process with specified PID in megabytes (MB).

        Parameters:
            pid (int): The process ID (PID) of the target process.

        Returns:
            float: Memory usage of the process in megabytes (MB).
        """
        try:
            children = psutil.Process(pid).children(recursive=True)
            total_memory_bytes = sum(
                psutil.Process(child.pid).memory_full_info().uss for child in children
            )
        except Exception:
            # allowed to happen as memory_usage accesses child processes
            # which might have been alrady destroyed
            # catch outside to speed up
            total_memory_bytes = 0
        return total_memory_bytes / (1024.0 * 1024.0)

    def execute_process(self, logfile, configs_path, results_path):
        memory_log = [0.0]
        stdout_tempfile = NamedTemporaryFile(delete=False)
        stderr_tempfile = NamedTemporaryFile(delete=False)

        try:
            command = f'{self.executable} --logLevel 1 --logFile "{logfile}" --configs "{configs_path}" --results "{results_path}"'
            process = subprocess.Popen(
                command,
                shell=True,
                stdout=stdout_tempfile,
                stderr=stderr_tempfile,
                text=True,
                cwd=self.base_path,
            )

            while process.poll() is None:
                memory_log.append(Runner.get_memory_usage(process.pid))
                sleep(0.1)

        except subprocess.CalledProcessError as e:
            return e.returncode, max(memory_log)
        except Exception as e:
            warnings(f"Caught exception during execution of '{command}': {e}")
            return -1, max(memory_log)
        finally:
            stdout_tempfile.close()
            stderr_tempfile.close()
            shutil.move(stdout_tempfile.name, results_path / 'stdout.txt')
            shutil.move(stderr_tempfile.name, results_path / 'stderr.txt')

        process.communicate()
        return process.returncode, max(memory_log)


    def execute(self, logfile, configs_path, results_path):
        return self.execute_process(logfile, configs_path, results_path)
