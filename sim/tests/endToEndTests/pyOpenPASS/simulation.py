################################################################################
# Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

from copy import copy
from dataclasses import dataclass, replace
import pickle
from typing import List
from merge_csv2csv import extract_agent_data_csv
from path_manager import PathManager, SimulatorBasePaths, ConfigSource
from runner import Runner
from config_modulator import ConfigModulator
from config_parser import TestItem
from path_hasher import generate_hash
from filelock import FileLock
from pathlib import Path

@dataclass(init=True, frozen=True)
class SimulationResult:
    exit_code: int
    ram_usage: float
    result_path: str

class CacheObjectAlreadyExists(Exception):
    pass

class ResultCache:
    def __init__(self, base_path):
        self.cache_path = Path(base_path / PathManager.PYOPENPASS_CACHE)

    def _get_file(self, key):
        return self.cache_path / key

    def get(self, key) -> SimulationResult:
        cache_file = self._get_file(key)
        if cache_file.exists():
            with open(cache_file, 'rb') as file:
                return pickle.load(file)

    def set(self, key, result: SimulationResult) -> None:
        cache_file = self._get_file(key)
        if cache_file.exists():
            raise CacheObjectAlreadyExists(
                f"Cannot set {cache_file} because it already exists. Clear cache before using it.")
        with open(cache_file, 'wb') as file:
            pickle.dump(result, file)

class Simulator():
    def __init__(self, simulator_base_paths: SimulatorBasePaths, mutual_config_path, resources_path):
        self.simulator_base_paths = simulator_base_paths
        self.resultCache = ResultCache(simulator_base_paths.results)
        self.runner = Runner(simulator_base_paths.executable)
        self.mutual_config_path = mutual_config_path
        self.resources_path = resources_path

    def _apply_config_and_run(self, paths: PathManager, test_item) -> SimulationResult:
        # for the hashing we need the actual configuration, but already modified
        # if it is identical to a config we already know, the result is being reused
        paths.collect_config()
        ConfigModulator.apply(test_item, paths.configs)
        config_hash = generate_hash(paths.configs)

        # for keeping reusing results, we keep them in a folder named after the hash
        # telling the pathmanager is necessary for collecting the artifacts later on
        paths.set_results_subfolder(config_hash)

        # for working with multiple threads (xdist)
        # the thread actually simulating, locks the resource
        with FileLock(paths.cache_lock(config_hash)):
            sim_result = self.resultCache.get(config_hash)
            if not sim_result:
                paths.clear_results()
                runner_result = self.runner.execute(
                    paths.logfile, paths.configs, paths.results)

                for run_id_path in paths.get_run_ids(paths.results):
                    component_paths =  paths.scan_for_additional_controller_output(run_id_path)
                    extract_agent_data_csv(paths.results, component_paths, run_id_path.name, backup_dst_files=True, keep_source=True)

                sim_result = SimulationResult(*runner_result, str(paths.results))
                self.resultCache.set(config_hash, sim_result)

        return sim_result

    def _run_determinism(self, test_item: TestItem) -> List[SimulationResult]:
        simulation_results = [self._run(test_item)]
        for run in range(test_item.invocations):
            single_run_test_item = copy(test_item)
            single_run_test_item.name = f'Rep{run:02d}'
            single_run_test_item.invocations = 1
            single_run_test_item.random_seed_offset = run
            simulation_results.append(self._run(single_run_test_item))
        return simulation_results

    def _run(self, test_item: TestItem) -> SimulationResult:
        config_source = ConfigSource(
            self.mutual_config_path, self.resources_path, test_item.config)
        paths = PathManager(self.simulator_base_paths,
                            config_source, test_item.nodeid, test_item.id)
        simulation_result = self._apply_config_and_run(paths, test_item)
        paths.collect_artifacts()
        return replace(simulation_result, result_path=str(paths.artifacts))

    def run(self, test_item: TestItem) -> List[SimulationResult]:
        if hasattr(test_item, 'determinism') and test_item.determinism:
            return self._run_determinism(test_item)
        else:
            return [self._run(test_item)]
