################################################################################
# Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

import os
import pytest

from pyopenpass import PyOpenPass, Reporter, addoption
from path_manager import clear_cache

def pytest_addoption(parser):
    addoption(parser)

def pytest_configure(config: pytest):
    config._pyopenpass = PyOpenPass(config)
    config.pluginmanager.register(config._pyopenpass, name="PyOpenPASS")
    config._pyopenpass_reporter = Reporter(config)
    config.pluginmanager.register(config._pyopenpass_reporter, name="PyOpenPASSReporter")

    worker_id = os.environ.get("PYTEST_XDIST_WORKER")
    if worker_id is None: # no xdist or xdist initialization thread
        clear_cache(
            config._pyopenpass.args.simulation_path,
            config._pyopenpass.args.output_path)

def pytest_unconfigure(config):
    pyopenpass = getattr(config, "_pyopenpass", None)
    if pyopenpass:
        del config._pyopenpass
        config.pluginmanager.unregister(pyopenpass)

    pyopenpass_reporter = getattr(config, "_pyopenpass_reporter", None)
    if pyopenpass_reporter:
        del config._pyopenpass_reporter
        config.pluginmanager.unregister(pyopenpass_reporter)

@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()

    results = item.results.asdict()
    results["type"] = "OpenPassTestItem"
    results["fixture"] = item.nodeid.split('::')[1]
    results["name"] = "::".join(item.nodeid.split('::')[2:])
    if hasattr(item, 'success_rate'): results["expected_rate"] = item.success_rate,
    if hasattr(item, 'ram_limit'): results["ram_limit"] = item.ram_limit,

    Reporter.make_report(results, rep)