..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
                2021 in-tech GmbH

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _download_and_install_openpass:

Installing openPASS
===================

This section describes how to compile and run |op|. Please make sure that all prerequisites have been properly installed according to section :ref:`Prerequisites`.
If you have strictly followed the instructions, the installed dependencies should be located on your machine under 
``C:\deps`` on **Windows** and ``~/deps`` on **Linux**. If there is a path deviation, 
the following commands must be adjusted.

.. note::
   
   If you are unfamiliar to ``CMake`` or working within a |mingw_shell|, Section :ref:`cmake` and :ref:`MSYS2 <msys2>` might give you a short introduction on these topics in the scope of building |op| itself.

#. Navigate back into repository

   .. tabs::

      .. tab:: Windows

         .. code-block:: 

            cd /C/openpass

      .. tab:: Linux

         .. code-block:: 
      
            cd ~/openpass

#. Create build directory and navigate into it

   .. code-block:: 
      
      mkdir build
      cd build

#. Prepare build

   |Op| links against shared libraries, which are located in the paths specified by ``CMAKE_PREFIX_PATH``. 
   To be able to install |Op| with resolved dependencies, all libraries found under the paths have to be copied right next to the executable during the installation step. 
   This is done by setting ``INSTALL_EXTRA_RUNTIME_DEPS=ON``. If you have followed the instructions strictly, no changes are necessary. 

   .. tabs::

      .. tab:: Windows

         .. code-block:: 

            cmake -G "MSYS Makefiles" \
            -D CMAKE_PREFIX_PATH="C:/deps/direct_deploy/fmilibrary;C:/deps/direct_deploy/open-simulation-interface;C:/deps/direct_deploy/protobuf;C:/deps/direct_deploy/protobuf-shared;C:/deps/direct_deploy/units;C:/deps/direct_deploy/mantleapi;C:/deps/direct_deploy/yase;C:/deps/direct_deploy/openscenario_api;C:/deps/direct_deploy/openscenario_engine;C:/deps/direct_deploy/gtest;C:/deps/direct_deploy/boost;C:/deps/direct_deploy/qt;C:/deps/direct_deploy/minizip;C:/deps;C:/msys64/mingw64/bin;" \
            -D CMAKE_INSTALL_PREFIX=C:/openPASS/bin/core \
            -D CMAKE_BUILD_TYPE=Release \
            -D USE_CCACHE=ON \
            -D WITH_DEBUG_POSTFIX=OFF \
            -D OPENPASS_ADJUST_OUTPUT=OFF \
            -D INSTALL_EXTRA_RUNTIME_DEPS=ON \
            ..

         .. note::

          - By specifying ``INSTALL_EXTRA_RUNTIME_DEPS=ON``, runtime dependencies will be copied to the installation directory when running ``make install``. This applies to all dependencies located in the paths specified in ``CMAKE_PREFIX_PATH``.
          - Make sure that the path ``C:/msys64/mingw64/bin`` is the last path in the CMAKE_PREFIX_PATH. Otherwise cmake might find and use local versions of required libraries instead of the ones listed in the thirdparties folder.

      .. tab:: Linux

         .. code-block:: 

            cmake -D CMAKE_PREFIX_PATH="$HOME/deps/fmilibrary;$HOME/deps/open-simulation-interface;$HOME/deps/protobuf;$HOME/deps/protobuf-shared;$HOME/deps/units;$HOME/deps/mantleapi;$HOME/deps/yase;$HOME/deps/openscenario_api;$HOME/deps/openscenario_engine;$HOME/deps/gtest;$HOME/deps/boost;$HOME/deps/qt;$HOME/deps/minizip;C:/deps" \
            -D CMAKE_INSTALL_PREFIX=/usr/local/openPASS/bin/core \
            -D CMAKE_BUILD_TYPE=Release \
            -D USE_CCACHE=ON \
            -D WITH_DEBUG_POSTFIX=OFF \
            -D OPENPASS_ADJUST_OUTPUT=OFF \
            -D INSTALL_EXTRA_RUNTIME_DEPS=ON \
            ..

         .. note:: 
         
          - To install in the mentioned CMAKE_INSTALL_PREFIX path, create ``openPASS`` folder under ``/usr/local`` using ``sudo mkdir /usr/local/openPASS``
            And for the user to access the folder, modify the permissions using ``sudo chown <uesrname>:<username> /usr/local/openPASS``
          - By specifying ``INSTALL_EXTRA_RUNTIME_DEPS=ON``, runtime dependencies will be copied to the installation directory when running ``make install``. This applies to all dependencies located in the paths specified in ``CMAKE_PREFIX_PATH``.
          - If you need to adjust paths and options based on your system and needs, you need to pay attention on quotations ``" "``, when using it in console (see :ref:`Cmake_prefix_path`).
            And also you need to be careful with it when you configuring it under VSCode (see :ref:`vscode`).
   
   .. note:: For a build that goes beyond the default settings, see :ref:`Cmake` for more available variables and options that can be set.
             

#. Optional: Build and execute unit tests

   Starting from ``openpass/build``:

   .. code-block:: 

      make test ARGS="--output-on-failure -j3"

   .. note::

      ARGS is optional, but recommended.
      Adjust parallel build flag ``-j3`` based on your system.

#. Build documentation

   Starting from ``openpass/build``:

   .. code-block:: 

      make doc

   .. note:: Currently the documentation must be built before |op| can be successfully installed if the CMake variable `WITH_DOC=ON` (default).

#. Build simulation core

   Starting from ``openpass/build``:

   .. code-block:: 

      make -j3 install

   .. note:: Again, adjust parallel build flag ``-j3`` based on your system.
