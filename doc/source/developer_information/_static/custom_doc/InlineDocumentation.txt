/// @file main.c
#include<stdio.h>

/// Mathematical constant PI 
#define PI       3.1415 

/// Radius in meters  
#define RADIUS_M 7.82
 
//! Calculates the area of the circle.
//! 
//! @param[in]  radius Radius of the circle
//! @param[out] area   Area of the circle
float CalculateArea(float radius)
{
    float area;
    area = PI * radius * radius;
    return area;
}