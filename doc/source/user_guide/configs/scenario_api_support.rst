..
  *******************************************************************************
                2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _scenario_api_support:

Scenario API Support
====================

.. include:: @OP_REL_ROOT@/scenario_api_support.md
   :parser: myst_parser.sphinx_