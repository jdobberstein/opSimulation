<!---***********************************************************************
Copyright (c) 2021 in-tech GmbH
              2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
**************************************************************************-->

# openPASS Documentation

Build this documentation with Sphinx, a python based documentation generator based on [reStructuredText](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html) as primary import format.

Sphinx
======

## Resources

- [reStructuredText Primer](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html)
- [A "How to" Guide for Sphinx + ReadTheDocs](https://source-rtd-tutorial.readthedocs.io/en/latest/index.html)
- [Sphinx Documentation](https://www.sphinx-doc.org)
- [OSI Sphinx Config](https://github.com/OpenSimulationInterface/osi-documentation/blob/master/conf.py)


## Building the documentation (Windows)

1. Get MSYS2 from https://www.msys2.org/
2. Start MSYS2 MSYS and update the system packages:

```
pacman -Syuu
```

If the upgrade requires a restart of MSYS2, resume the upgrade by re-opening the shell and call:

```
pacman -Suu
```

3. Install required packages: Start MSYS2 MinGW 64bit and install the [required packages](installation_guide/_static/msys2_packages.txt)

4. Create a directory named `build` inside your checked out repository and navigate to it in the MSYS2 MinGW 64bit shell
5. Execute

```
cmake -G "MSYS Makefiles" -DWITH_DOC=ON -DWITH_SIMCORE=OFF -DWITH_TESTS=OFF ..
make doc
```

## Building the documentation (Debian)

```
# install python, pip, spellchecker, ...
sudo apt install doxygen python3 python3-pip dvipng texlive-latex-extra

# install sphinx and its extensions
pip3 install sphinx sphinx-rtd-theme sphinx-tabs myst-parser breath exhale

# build doc (only)
mkdir build
cd build
cmake -DWITH_SIMCORE=OFF -DWITH_TESTS=OFF -DWITH_DOC=ON ..
make doc
```
