################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# conanfile for building opsimulation with Conan
################################################################################

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain, cmake_layout
from conan.tools.files import update_conandata
from conan.tools.scm import Git
import os

required_conan_version = ">=1.53"

class opsimulationConan(ConanFile):
    name = "opsimulation"
    version= "0.0.1"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
    }
    short_paths = Truegenerators = ['CMakeDeps', 'VirtualBuildEnv']


    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def requirements(self):
        self.requires("b2/4.10.1", options={"toolset":"gcc"})
        self.requires("boost/1.72.0", options={"shared":True,
                                       "without_atomic":True,
                                       "without_chrono":True,
                                       "without_container":True,
                                       "without_context":True,
                                       "without_contract":True,
                                       "without_coroutine":True,
                                       "without_date_time":True,
                                       "without_exception":True,
                                       "without_fiber":True,
                                       "without_graph":True,
                                       "without_graph_parallel":True,
                                       "without_iostreams":True,
                                       "without_locale":True,
                                       "without_log":True,
                                       "without_math":True,
                                       "without_mpi":True,
                                       "without_python":True,
                                       "without_random":True,
                                       "without_regex":True,
                                       "without_serialization":True,
                                       "without_stacktrace":True,
                                       "without_test":True,
                                       "without_thread":True,
                                       "without_timer":True,
                                       "without_type_erasure":True,
                                       "without_wave":True })
        self.requires("msys2/cci.latest", options={"no_kill":True})
        self.requires("qt/5.15.7", options={"with_pq":False,
                                            "with_libpng":False,
                                            "openssl":False,
                                            "opengl":"no",
                                            "qtxmlpatterns":True})
        self.requires("zlib/1.2.12@")
        self.requires("minizip/1.2.13@")
        # self.requires("gtest/1.14.0@")
        self.requires("fmilibrary/2.0.3@openpass/testing")
        self.requires("protobuf/3.20.0@", options={"shared":True})
        self.requires("units/2.3.3@openpass/testing")
        self.requires("open-simulation-interface/3.5.0@openpass/testing")
        self.requires("mantleapi/0.0.4@openpass/testing")
        # self.requires("yase/d0c0e58d17358044cc9018c74308b45f6097ecfb@openpass/testing")
        self.requires("openscenario_api/v1.3.1@openpass/testing")
        self.requires("openscenario_engine/82e2c1c349ee69d28cd301abfb520e8b210aee7b@openpass/testing", options={
            'MantleAPI_version': "0.0.4",
            'Yase_version': "d0c0e58d17358044cc9018c74308b45f6097ecfb"
        }
)

    def export(self):
        git = Git(self, self.recipe_folder)
        scm_url, scm_commit = git.get_url_and_commit()
        update_conandata(
            self, {"sources": {"commit": scm_commit, "url": scm_url}})

    def generate(self):
        _cmake_prefix_paths = []
        for _, dependency in self.dependencies.items():
            _cmake_prefix_paths.append(dependency.package_folder)
        _cmake_prefix_paths = ';'.join(str(_cmake_prefix_path) for _cmake_prefix_path in _cmake_prefix_paths)
        tc = CMakeToolchain(self)
        tc.cache_variables["CMAKE_PREFIX_PATH"] = _cmake_prefix_paths
        tc.generate()

    def source(self):
        git = Git(self)
        sources = self.conan_data["sources"]
        git.clone(url=sources["url"], target=".")
        git.checkout(commit=sources["commit"])

    def layout(self):
        cmake_layout(self)
        self.cpp.build.libdirs.append("src")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        cmake.test()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        rmdir(self, os.path.join(self.package_folder, "cmake"))